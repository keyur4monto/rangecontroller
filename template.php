<?php
include 'includes/session.php';
?>

<!DOCTYPE HTML>
<html>
<head>
<title>Range Controller</title>
<?php
include 'includes/headerTags.php';
?>
</head>

<body>
<?php
include 'includes/header.php';
?>
<?php
include 'includes/navBar.php';
?>
<style>
.pageName{
    background-color: #ffffff;
    color: green;
    margin: 0 auto;
    overflow: hidden;
    padding-bottom: 10px;
    padding-top: 10px;
    width: 950px;
    text-align: center;
}
</style>
<?php 
if(isset($_GET['msg']) && $_GET['msg']==1){?>
    <div id="pageName" class="pageName">Thank you for your message! we will be contact you soon!</div></div>
    <?php    
}
include 'includes/content.php';
?>
<?php
include 'includes/footer.php';
?>
</body>
</html>