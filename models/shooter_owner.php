<?php

namespace Rc\Models {
    use Rc\Services\DbHelper;
    use Rc\Services\StringHelper;
        
    class shooter_owner {
        /**
         *
         * @var \mysqli
         */
        protected $link;     
        /**
         *
         * @var \Rc\Models\range_owner_id 
         */
        protected $shooter_owner_id;

        protected $accHashKey;
        // also known as range number
        protected $accNo;

        protected $companyName;
        protected $companyLogo;
        protected $phone;
        protected $email;

        // company's address 
        protected $addressStreet;
        protected $addressCity;
        protected $addressState;
        protected $addressZip;

        // name of the person's who registered for this range company
        protected $firstName;
        protected $lastName;
        
        // membership numbers start from this number
        protected $memberNumberStartCount;


        //private constructor will prevent this class from being initialized 
        // like new range_owner(). We want it to be initialized by using
        // range_owner::initWithAccount(...)
        private function __construct() {               
        }

        public function getCompanyName(){ 
            return $this->companyName;
        }
        public function getCompanyLogo(){
            return $this->companyLogo;
        }
        
        public function getPhoneNumber(){ 
            return $this->phone;
        }
        public function getEmail(){ 
            return $this->email;
        }
        /**
         * returns account number
         * @alias getAccountNumber
         * @return string
         */
        public function getRangeNumber(){
            return $this->accNo;
        }

        public function getAccountNumber(){
            return $this->accNo;
        }

        public function getAccountHashKey(){
            return $this->accHashKey;
        }

        /**
         * Returns the address parts as an array so you can join them with \n
         *  or <br /> depending on your environment
         * @return array
         */
        public function getCompanyAddress(){
            return array(
                $this->addressStreet,
                $this->addressCity . (empty($this->addressCity) ? '' : ', ') 
                . $this->addressState . 
                ' ' . $this->addressZip
            );
        }

        public function getOwnerFirstname(){
            return $this->firstName;
        }

        public function getOwnerLastname(){
            return $this->lastName;
        }
        
        public function getMemberNumberStartCount(){
            return $this->memberNumberStartCount;
        }

        /**
         * Returns a new range_owner object populated for this account
         * @param mysqli $link
         * @param string $accHashKey
         * @param string|int $accNo
         * @return \range_owner
         */
        public static function initWithAccount($link, $accHashKey, $accNo) {
            $ro = new shooter_owner();
            $ro->link  = $link;       
            $ro->accHashKey = $accHashKey;
            $ro->accNo = $accNo;
            $result = $ro->populate();
            if ($result) {
                $ro->shooter_owner_id = new \Rc\Models\shooter_owner_id($accHashKey, $accNo);
                return $ro;
            }
            return null;
        }

        protected function populate(){
            $ctrlCfgRow = $this->getRecordFromControllerConfig();
            if (empty($ctrlCfgRow)) {
                return false;
            }
            $signupInfo = $this->getRecodFromSignupContactInfo();
            if (empty($signupInfo)) {
                return false;
            }

            $this->companyName = $ctrlCfgRow['company_name'];
            $this->companyLogo = $ctrlCfgRow['company_logo'];
            $this->email = $ctrlCfgRow['registered_email'];
            
            $this->memberNumberStartCount = $ctrlCfgRow['member_number_start_count'];

            $this->phone = $signupInfo['phone_number'];
            $this->firstName = $signupInfo['first_name'];
            $this->lastName = $signupInfo['last_name'];
            $this->addressStreet = trim($signupInfo['street_address']);
            $this->addressCity = trim($signupInfo['city']);
            $this->addressState = trim($signupInfo['state']);
            $this->addressZip = trim($signupInfo['zipcode']);

            return true;        
        }

        protected function getVal($bag, $key, $default = null, $trim = true){
            return isset($bag[$key]) ? $bag[$key] : $default;
        }
        
        public function getBillingInfo($memberNo = null, $id=null){
            $sql = "select payment_for, employee_name, acctnumberhashed, accountnumber, "
                    . "membership_number, id , "
                    . "payment_method, payment_amount, `date` from "
                    . " billing_info where " . 
                    $this->range_owner_id->getSqlCond($this->link) 
                    . (empty($memberNo) ? '' : 
                            ' and membership_number = ' . (int)$memberNo) 
                    . (empty($id) ? '' : ' and id = ' . (int)$id) 
                    . " order by id desc, str_to_date(`date`, '%m/%e/%Y') desc  ";
            
            return DbHelper::dbFetchAllAssoc($this->link, $sql);
        }    
        
        
        
        /**
         * 
         * @param array $filter
         * @param \Rc\Services\Pager $pager
         * @return \Rc\Services\PagedResults
         */
        public function getMembers($filter, \Rc\Services\Pager $pager){
            $cond = '';
            $ordby = ''; 
            if ($fn = $this->getVal($filter, 'firstName','', 1)) {                        
                $cond = " and first_name like '" . 
                        $this->link->real_escape_string($fn) . "%' ";
                $ordby = ' first_name, last_name ';
            } else if ($ln = $this->getVal($filter, 'lastName','', 1)) {
                $cond = " and last_name like '" . 
                        $this->link->real_escape_string($ln) . "%' ";
                $ordby = ' last_name, first_name ';
            } else if ($memno = $this->getVal($filter, 'membershipNumber','', 1)) {
                $cond = " and membership_number like '" . 
                        $this->link->real_escape_string($memno) . "%' ";
                $ordby = ' membership_number ';
            } else if ($phone = $this->getVal($filter, 'phoneNumber','', 1)) {
                $ph_part = StringHelper::formatPhonePart($phone);
                 //or cell_phone like '" . 
                 //       $this->link->real_escape_string($phone) . "%' "
                 //       . " or cell_phone like  '". 
                 //       $this->link->real_escape_string($ph_part) . "%'
                $cond = " and (home_phone like '" . 
                        $this->link->real_escape_string($phone) . "%' "
                        . " or home_phone like  '". 
                        $this->link->real_escape_string($ph_part) . "%'"
                        . " ) ";
                $ordby = ' home_phone ';
            } 

            $ordby = empty($ordby) ? ' order by membership_number' : 
                    ' order by ' . $ordby . ' '; 

            $sql_common = " from customers where " . 
                    $this->range_owner_id->getSqlCond($this->link) 
                    . " $cond ";
            $sql_check_login = " select count(*) as c   "
                    . " FROM range_time_tracker rtt "
                    . " WHERE date_stamp='".date('n/j/y')."' "
                    . " and start_time <> '' and end_time = '0' "
                    . " and " . $this->range_owner_id->getSqlCond($this->link) 
                    .  " and rtt.membership_number = "
                    . " customers.membership_number ";
            $sql_paged = " select first_name, last_name, middle_name, "
                    . "membership_number, "
                    . "($sql_check_login) as _is_logged_in, " 
                    . "street_address, city, state, zipcode, home_phone "
                    . $sql_common 
                    . $ordby 
                    . " limit " . $pager->getOffset() . ", " . $pager->getPageSize();
            $sql_count = "select count(*) as c " . $sql_common; 

            $countrow = DbHelper::dbFetchRowAssoc($this->link, $sql_count);
            $pagedResults = new \Rc\Services\PagedResults(
                    DbHelper::dbFetchAllAssoc($this->link, $sql_paged), $countrow['c'], 
                    $pager);
            return $pagedResults;
        }

        protected function getRecordFromControllerConfig() {
            $sql = "select * from controller_config "
                    . " where  acctnumberhashed = '" . 
                    $this->link->real_escape_string($this->accHashKey) . "' "
                    . " and accountnumber = '" . 
                    $this->link->real_escape_string($this->accNo) . "' ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }

        protected function getRecodFromSignupContactInfo(){
            $sql = "select * from signup_contact_info "
                    . " where  acctnumberhashed = '" . 
                    $this->link->real_escape_string($this->accHashKey) . "' "
                    . " and accountnumber = '" . 
                    $this->link->real_escape_string($this->accNo) . "' ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
        
        public  function generateMembershipNumber(){
            $sql = " select member_number from control_member_number "
                    . " where " . $this->shooter_owner_id->getSqlCond($this->link) . 
                    " for update ";
            $this->link->query(' START TRANSACTION ');
            $rec = DbHelper::dbFetchRowAssoc($this->link, $sql);
            if (!$rec) {
                $this->link->query('ROLLBACK');
                $this->link->query(' START TRANSACTION ');
                
                $data = $this->shooter_owner_id->getIdArray();
                $data['member_number'] = (int)trim($this->getMemberNumberStartCount());
                $sql_ins = "insert into control_member_number (" . 
                        implode(', ', array_keys($data)) . ") values ('" .
                        implode("', '", array_values($data)) . "' ) "; 
                $this->link->query($sql_ins);
                $rec = DbHelper::dbFetchRowAssoc($this->link, $sql);
                if (empty($rec)) {
                    $this->link->query('ROLLBACK');
                    return false;
                }                
            }
            $memno = $rec['member_number'] + 1;
            $sql = " update control_member_number set member_number = " . 
                    $memno . " where " . $this->shooter_owner_id->getSqlCond($this->link);
            $this->link->query($sql);
            $this->link->query(' COMMIT');
            return $memno;            
            
        }
        
		function getRemainingDays(){
			$sql="SELECT * FROM trial_version WHERE accountnumber = '" . $this->getRangeNumber() . "'";
			$row=DbHelper::dbFetchRowAssoc($this->link, $sql);
			  $trial_start = $row['trial_start'];
				$trial_end = $row['trial_end'];
				$trial_paid = $row['trial_paid'];
				$date = strtotime($trial_end);
			$remaining = $date - time();
			$days_remaining = floor($remaining / 86400);
            return $days_remaining;            
		}
		
        public static function generateAccountNumber(){
            throw new Exception('not implemented');
        }

    }
}