<?php

namespace Rc\Models {
    use Rc\Models\range_owner_id;
    use Doctrine\DBAL\Connection as DbalConn;
    use Rc\Services\DbHelper; 
    class UniversalId {
        
        /**
         *
         * @var \mysqli 
         */
        protected $link;
        /**
         *
         * @var \Rc\Models\range_owner_id 
         */
        protected $range_owner_id; 
        
        /**
         *
         * @var DbalConn 
         */
        protected $dbalConn;
        function __construct(range_owner_id $range_owner_id, DbalConn $conn) {
            $this->dbalConn = $conn;
            $this->link = DbHelper::getWrappedHandle($conn);
            $this->range_owner_id = $range_owner_id;
        }
        
        /**
         * Some tables are using a unique id per range owner account.
         *  That id is coming from this universal_id table's universal_id column. 
         *  The column in db shows the last id 
         *  that is used.. so this function increments it by one and returns that
         *  for next usage
         *  I KNOW THIS IS STUPID  TO NTH DEGREE BUT IT IS HOW THE CODE IS 
         *   SET UP BY THE PREVIOUS PROGRAMMER. 
         *  (AN AUTO INCREMENETED COLUMN WOULD BE GREAT FOR THIS)
         *  This is a make-it-work plan 
         *    to ensure that a unique id is generated PER RANGER ACCOUNT
         * @return int|null on failure
         */
        public static function getNewId(){            
            $sql = " select universal_id from universal_id "
                    . " where " . $this->range_owner_id->getSqlCond($this->link) . 
                    " for update ";
            $this->dbalConn->beginTransaction();            
            $rec = $this->dbalConn->fetchArray($sql);
            $id = 1;
            if (empty($rec)) {
                $this->dbalConn->rollback();
                $this->dbalConn->beginTransaction();
                $data = $this->range_owner_id->getIdArray();
                $data['universal_id'] = $id;
                $this->dbalConn->insert('universal_id', $data);
                $rec = $this->dbalConn->fetchArray($sql);
                if (empty($rec)) {
                    $this->dbalConn->rollback();
                    return null;
                }
            }
            $memno = $rec['universal_id'] +1 ;
            $sql = " update universal_id set universal_id = " . 
                    $memno . " where " . $this->range_owner_id->getSqlCond($this->link);
            $this->dbalConn->executeUpdate($sql);
            $this->dbalConn->commit();
            return $memno;            
            
        }

    }
}
