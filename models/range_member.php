<?php
namespace Rc\Models { 
    
    use Rc\Services\DbHelper;
    use Rc\Services\ArrayHelper;
    use Doctrine\DBAL\Connection as DbalConn;    
    use Rc\Services\StringHelper;
	use Silex\Application;
    
    
    class range_member {
        /**
         *
         * @var mysqli
         */
        protected $link;
        
        protected $dbalConn;
        
        protected $accHashKey;
        protected $accNo;
        protected $memberNo;

        protected $membershipExpirationDate;
        protected $membershipType;
        protected $membershipDate;

        /**
         *
         * @var string m/d/y format 
         */
        protected $membershipRenewalEmailSentDateStr;

        protected $firstName;
        protected $lastName;
        protected $middleName;
        protected $email;
		protected $email_status;
        protected $tempPassword;
        
        /**
         *
         * @var \Rc\Models\Address
         */
        protected $address; 
        
        protected $ssn; 
        protected $phone1;
        protected $phone2;
        
        protected $emergencyContactName; 
        protected $emergencyContactPhone;
        
        /**
         *
         * @var \Rc\Models\DriversLicense 
         */
        protected $driversLicense;
        
        protected $certsSafetyVideoCompleted;
        protected $certsSafetyRangeFormCompleted;
        protected $certsComments;

        //private constructor will prevent this class from being initialized 
        // like new range_member(). We want it to be initialized by using
        // range_member::initForMember(...)
        private function __construct() {               
        }

        public function getMembershipNumber(){
            return $this->memberNo;
        }

        /**
         * 
         * @return string
         */
        public function getMembershipExpirationDate(){
            return $this->membershipExpirationDate;
        }

        /**
         * 
         * @return string
         */
        public function getMembershipDate(){
            return $this->membershipDate;
        }

        /**
         * 
         * @return string
         */
        public function getMembershipType(){
            return$this->membershipType;
        }

        public function  getFirstName(){
            return $this->firstName;
        }

        public function getLastName() {
            return $this->lastName;
        }

        public function getMiddleName(){
            return $this->middleName;
        }

        public function getFullName() {
            return $this->firstName . 
                    ($this->middleName ? " " . $this->middleName : '') .
                    " " . $this->lastName;
        }

        public function getEmail() {
            return $this->email;
        }
		public function getEmailStatus(){ 
            return $this->email_status;
        }
        public function getTemporaryPassword(){
            return $this->tempPassword;
        }

        public function getMembershipRenewalEmailSentDateStr(){
            return $this->membershipRenewalEmailSentDateStr;
        }
        
        public function getComments(){
            if ($this->driversLicense) {
                return $this->driversLicense->comments;
            } 
            return '';
        }
        
        public function getCertsSafetyRangeFormCompleted(){
            return $this->certsSafetyRangeFormCompleted;
        }
        
        public function getCertsSafetyVideoCompleted(){
            return $this->certsSafetyVideoCompleted;
        }
        
        public function getCertsComments(){
            return $this->certsComments;
        }
        
        /**
         * returns drivers license info.. set getEmpty to true 
         *  if u want an object with empty values in case 
         *  drivers license is empty (so u can read data from it 
         *  in forms / views etc)
         * @param bool $getEmpty
         * @return type
         */
        public function getDriversLicense($getEmpty = false){
            if ($getEmpty && empty($this->driversLicense)) {
                return new DriversLicense();
            }
            return $this->driversLicense;
        }
        
        /**
         * 
         * @return \Rc\Models\Address
         */
        public function getAddress(){
            return $this->address; 
        }
        
        public function getSsn(){
            return $this->ssn;
        }
        
        public function getPhone1(){
            return StringHelper::formatPhone($this->phone1);
        }
        
        public function getPhone2(){
            return StringHelper::formatPhone($this->phone2);
        }
        
        public function getEmergencyContactName(){
            return $this->emergencyContactName;
        }
        
        public function getEmergencyContactPhone(){
            return StringHelper::formatPhone($this->emergencyContactPhone);
        }
        public function getIdFieldMap(){
            return [
                'acctnumberhashed' => $this->accHashKey,
                'accountnumber' => $this->accNo,
                'membership_number' => $this->memberNo
            ];
        }
        /**
         * 
         * @param \mysqli $link
         * @param \string $accHashKey
         * @param \string $accNo
         * @param \string $memberNo
         * @return \Rc\Models\range_member
         */
        public static function initForMember($link, $accHashKey, $accNo, $memberNo) {            
            $range_member = new range_member($link);
            
            $range_member->link = $link;
            $range_member->accHashKey = $accHashKey;
            $range_member->accNo = $accNo;
            $range_member->memberNo = $memberNo;
            $res = $range_member->populate();
            if (!$res) {
                return null;
            }
            return $range_member;
        }

        public function updateOrInsertMembershipRenewalDate(\DateTime $renewalDate){
            $memRenewal = $this->getRecordFromMembershipRenewal();
            if ( $memRenewal) {            
                $sql = "update membership_renewal set date_sent = '" . 
                        $renewalDate->format('n/j/Y'). "' "
                        . "where " . $this->getUniqueMemberSqlCond() ;
            } else {
                $data = [
                    'acctnumberhashed' => $this->accHashKey, 
                    'accountnumber' => $this->accNo,
                    'membership_number' => $this->memberNo,
                    'email' => $this->getEmail(),
                    'expiration_date' => $this->membershipExpirationDate,
                    'date_sent' => $renewalDate->format('n/j/Y')
                ];
                $sql = "insert into membership_renewal (" . 
                        implode(",", array_keys($data)) . " ) values ( '" . 
                        implode("','", array_values($data)) . "' ) ";

            }
            //print $sql;
            return $this->link->query($sql);
        }
        
        public static function memberUniqueFields(){
            return ['first_name' =>'First Name', 
                'last_name' =>"Last Name", 
                'middle_name' => 'Middle Name', 
                'home_phone' => 'Phone 1#', 
                'email' => 'Email'];
        }
        public static function doesMemberExist($data, range_owner_id $roid, 
                DbalConn $conn){
            $req = self::memberUniqueFields();
            $datakeys = array_keys($data);
            foreach($req as $k => $v){
                if (!in_array($k, $datakeys)) {
                    throw new Exception('You must pass all required fields in data argument');
                }
            }
            
            $sql = "select count(*) as c from customers where "
                    . " first_name like ? "
                    . "and last_name like ? "
                    . "and middle_name like ? "
                    . "and home_phone like ? "
                    . "and email like ? "
                    . "and acctnumberhashed = ? "
                    . "and accountnumber  = ? ";
            $vals = [
               $data['first_name'],$data['last_name'],
                $data['middle_name'], $data['home_phone'],
                $data['email'],
                $roid->getAccountHashKey(),
                $roid->getAccountNo()
            ];
            $arr = $conn->fetchAssoc($sql, $vals);
            if ($arr && $arr['c'] > 0) {
                return true;
            }
            return false;
            
        }
        
        public function isLoggedIn(){
            $sql = " SELECT start_time, end_time "
                    . " FROM range_time_tracker "
                    . " WHERE date_stamp='".date('n/j/y')."' "
                    . " and start_time <> '' and end_time = '0' "
                    . " and " . $this->getUniqueMemberSqlCond();
           //print $sql;
            $row = DbHelper::dbFetchRowAssoc($this->link, $sql);
            if (empty($row)) {
                return false;
            }
            
            return true;
        }
        
        public function isBlocked(DbalConn $conn){
            $sql = "SELECT count(*) as c  FROM block_unblock "
                    . "WHERE " . $this->getUniqueMemberSqlCond();
            $row = $conn->fetchAssoc($sql);
            if (empty($row) || !$row['c']) {
                return false;
            }
            return true;
        }
        
        public function blockMember(DbalConn $conn) {
            $accCols = $this->getIdFieldMap();
            $this->unBlockMember($conn);
            $accCols['start_block'] = date("D M j g:i:s Y");
            $accCols['end_block'] = '';
            return $conn->insert('block_unblock', $accCols);
        }        
        
        public function unBlockMember(DbalConn $conn) {
            $accCols = $this->getIdFieldMap();
            return $conn->delete('block_unblock', $accCols);
        }
        
        public function getVisitCount(){
            $sql = " SELECT count(*) as c "
                    . "FROM range_time_tracker "
                    . "WHERE " . $this->getUniqueMemberSqlCond(); 
            $res = DbHelper::dbFetchRowAssoc($this->link, $sql);
            return empty($res) ? 0 : $res['c'];
        }
        
        public static  function getRequiredFieldsForTable($table){
            $reqs = [
                'customers'=> [
                    'first_name','last_name', 'street_address',
                    'city','state', 'zipcode', 'email', 'home_phone'
                ]
            ];
            return ArrayHelper::getIfExists($reqs, $table, null);
        }
                
        public function updateCustomers($vals, DbalConn $conn){
            $cust = $this->getRecordFromCustomers();
            if (empty($cust) || empty($vals)) {
                return 0;
            }              
            $accCols = $this->getIdFieldMap();
            $reqs = self::getRequiredFieldsForTable('customers');            
            $vals = array_merge($cust, $vals);
            if (ArrayHelper::hasEmptyValuesForKeys($vals, $reqs)) {
                throw new Exception('Contact data missing some required values ');
            }
            return $conn->update('customers', $vals,  $accCols);
            
        }
        
        /**
         * 
         * @param type $table can be oneof member tables like 
         *  memberships, driver_license_info, emergency_contact,
         *  billing_info, certs_and_safety
         * @param type $vals
         * @param DbalConn $conn
         * @return int
         */
        public function updateOrInsertCommon($table, $vals, DbalConn $conn){
            if (strtolower(trim($table)) == 'billing_info') {
                throw new Exception("You cannot update $table via this method "
                        . "because it has multiple records per account ");
            }
            $rec = $this->getRecordFromTable($table);
                        
            if (empty($vals)) {
                return 0;
            }
            $accCols = $this->getIdFieldMap();
            if ($rec) {
                $vals = array_merge($rec, $vals);
                return $conn->update($table, $vals,  $accCols);
            } else {
                $vals = array_merge($vals, $accCols);
                return $conn->insert($table, $vals);
            }
        }
        /**
         * 
         * @param array $cond key value for conditions
         * @param \Rc\Models\DbalConn $conn
         * @return boolean
         */
        public function deleteBilling($id, DbalConn $conn) {           
            $accCols = $this->getIdFieldMap();
            $accCols['id'] = $id;
            return $conn->delete('billing_info', $accCols);
            
        }
        
        public function addBilling($vals, DbalConn $conn){
            if (empty($vals)) {
                return 0;
            }
            $accCols = $this->getIdFieldMap();
            $vals = array_merge($vals, $accCols);
            return $conn->insert('billing_info', $vals);            
        }
        
        public function updateBilling($vals, $id, DbalConn $conn){
            $fields = ['payment_method', 'payment_amount', 'date','source',
                'membership_type'];
            $data = [];
            foreach($fields as $k) {
                if (isset($vals[$k])) {
                    $data[$k] = $vals[$k];
                }
            }
            if (empty($data)) {
                return 0;
            }
            $acc = $this->getIdFieldMap();
            $acc['id'] = $id;
            return $conn->update('billing_info', $data, $acc);
            
        }
        
        public static function createNewMember($argData, 
                \Doctrine\DBAL\Connection $conn, 
                range_owner $range_owner) {
            $fields = ['firstName', 'lastName', 'middleName', 
                'street', 'city', 'state', 'zipcode', 
                'ssn', 'phone1', 'phone2', 'email', 'email_status',
                'emergencyName', 'emergencyPhone', 
                'membershipType', 'memRegistrationDate', 
                'memExpirationDate', 'emp_id'];
            $data =  [];
            foreach($fields as $field) {
                $data[$field] = trim(
                        ArrayHelper::getValForKeyWithDefault($argData, $field, ''));
            }
                        
            $newMemberNo = $range_owner->generateMembershipNumber();            
            $conn->beginTransaction();
            
            $inserted = $conn->insert('customers', [
                'acctnumberhashed'=> $range_owner->getAccountHashKey(),
                'accountnumber' => $range_owner->getAccountNumber(),
                'membership_number' => $newMemberNo,
                'first_name' => $data['firstName'],
                'last_name' => $data['lastName'],
                'middle_name' => $data['middleName'],
                'street_address' => $data['street'],
                'city' => $data['city'], 
                'state' => $data['state'], 
                'zipcode' => $data['zipcode'],
                'ssn' => $data['ssn'],
                'home_phone' => StringHelper::formatPhone($data['phone1']),
                'cell_phone' => StringHelper::formatPhone($data['phone2']),
                'email' => $data['email'],
				'email_status' => $data['email_status'],
                'emp_id' => $data['emp_id']
            ]);
            if (!$inserted) {
                $conn->rollback();
                return false;
            }
            
            if ($data['emergencyName'] || $data['emergencyPhone']) {
                $conn->insert('emergency_contact', [
                    'acctnumberhashed'=> $range_owner->getAccountHashKey(),
                    'accountnumber' => $range_owner->getAccountNumber(),
                    'membership_number' => $newMemberNo,
                    'contact_name' => $data['emergencyName'],
                    'contact_phone' => $data['emergencyPhone']
                ]);
            }
            
            if ($data['membershipType'] || $data['memRegistrationDate']
                    || $data['memExpirationDate']) {
                $conn->insert('memberships', [
                    'acctnumberhashed'=> $range_owner->getAccountHashKey(),
                    'accountnumber' => $range_owner->getAccountNumber(),
                    'membership_number' => $newMemberNo,
                    'membership_type' => $data['membershipType'],
                    'membership_date' => $data['memRegistrationDate'],
                    'membership_expires' => $data['memExpirationDate']
                ]);
            }
            
            $pwd = StringHelper::generateRandomString();
            $inserted = $conn->insert('member_login', [
                'acctnumberhashed'=> $range_owner->getAccountHashKey(),
                'accountnumber' => $range_owner->getAccountNumber(),
                'membership_number' => $newMemberNo,
                'email' => $data['email'],
                'temp_pw' => $pwd,
                'current_pw' => $pwd
            ]);
            if (!$inserted) {
                $conn->rollback();
                return false;
            }
            
            $conn->commit();
            return $newMemberNo;            
        }
        
        
        
        
        
        private function populate(){
            $customersRow = $this->getRecordFromCustomers();
            //print_r($customersRow);
            if (!$customersRow) {
                return false;
            }

            $memLogin = $this->getRecordFromMemberLogin();
            //print_r($memLogin);
            if (!$memLogin) {
                return false;
            }

            $membership = $this->getRecordFromMemberships();
            //print_r($membership);
            
            $emergency = $this->getRecordFromEmergencyContact();
            
            $dl = $this->getRecordFromDriversLicense();
            
            $certs = $this->getRecordFromCertificates();
            
            if ($certs) {
                $this->certsComments = $certs['certs_certificates_comments'];
                $this->certsSafetyVideoCompleted = $certs['certs_video_complete'];
                $this->certsSafetyRangeFormCompleted =$certs['certs_safety_form_complete'];
            }
            
            if ($dl) {
                $this->driversLicense = DriversLicense::initWithTableRowArray($dl);
            }

            $this->firstName = trim($customersRow['first_name']);
            $this->lastName = trim($customersRow['last_name']);
            $this->middleName = trim($customersRow['middle_name']);
            $this->email = trim($customersRow['email']);
            $this->email_status = trim($customersRow['email_status']);
            $this->address = new Address($customersRow['street_address'], 
                    $customersRow['city'], $customersRow['state'], 
                    $customersRow['zipcode']);
            
            $this->ssn = $customersRow['ssn'];
            $this->phone1 = $customersRow['home_phone'];
            $this->phone2 = $customersRow['cell_phone'];
            
            if ($emergency) {
                $this->emergencyContactName = $emergency['contact_name'];
                $this->emergencyContactPhone = $emergency['contact_phone'];
            }
            
            if ($membership) {
                $this->membershipExpirationDate = trim($membership['membership_expires']);
                $this->membershipType = trim($membership['membership_type']);
                $this->membershipDate = trim($membership['membership_date']);
            }

            $memRenewal = $this->getRecordFromMembershipRenewal();
            if ($memRenewal) {
                $this->membershipRenewalEmailSentDateStr = $memRenewal['date_sent'];
            }

            $this->tempPassword = $memLogin['temp_pw'];
            return true;
        }
		

		public static function getAllMembersByType (Application $app, $query_condition) {
		
		$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
		
		$sql = "SELECT customers.visitcount,customers.membership_number, memberships.accountnumber, customers.email_status, customers.first_name,customers.middle_name, customers.last_name, customers.street_address, customers.city, customers.state, memberships.membership_date, memberships.membership_type, customers.zipcode FROM memberships INNER JOIN customers ON customers.membership_number = memberships.membership_number and  customers.accountnumber=memberships.accountnumber and customers.is_delete=1 " . $query_condition . " ORDER BY customers.membership_number ASC";		
		
		$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
		
		return $assoc_array;
		
		
		 
		//return "faFA";
		
		}
		public static function getAllMembersByTypeShortNo (Application $app, $query_condition, $shortno) {
	
		$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
		if($shortno == "all")
		{
		$sql = "SELECT customers.visitcount,customers.membership_number, memberships.accountnumber, customers.email_status, customers.first_name,customers.middle_name, customers.last_name, customers.street_address, customers.city, customers.state, memberships.membership_date, memberships.membership_type, customers.zipcode FROM memberships INNER JOIN customers ON customers.membership_number = memberships.membership_number" . $query_condition . " ORDER BY customers.membership_number ASC";
		}
		else
		{
		$sql = "SELECT customers.visitcount,customers.membership_number, memberships.accountnumber, customers.email_status, customers.first_name,customers.middle_name, customers.last_name, customers.street_address, customers.city, customers.state, memberships.membership_date, memberships.membership_type, customers.zipcode FROM memberships INNER JOIN customers ON customers.membership_number = memberships.membership_number" . $query_condition . " ORDER BY customers.membership_number ASC LIMIT " . $shortno;
		}
		$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
		
		return $assoc_array;
		
		
		 
		//return "faFA";
		
		}
		public static function getAllMembersByShortNo (Application $app, $query_condition, $shortno) {
		
		$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
		if($shortno == 'all')
		{
		$sql = "SELECT customers.visitcount,customers.membership_number, memberships.accountnumber, customers.email_status, customers.first_name,customers.middle_name, customers.last_name, customers.street_address, customers.city, customers.state, memberships.membership_date, memberships.membership_type, customers.zipcode FROM memberships INNER JOIN customers ON customers.membership_number = memberships.membership_number" . $query_condition . " ORDER BY customers.membership_number ASC";
		}
		else
		{ 
		$sql = "SELECT customers.visitcount,customers.membership_number, memberships.accountnumber, customers.email_status, customers.first_name,customers.middle_name, customers.last_name, customers.street_address, customers.city, customers.state, memberships.membership_date, memberships.membership_type, customers.zipcode FROM memberships INNER JOIN customers ON customers.membership_number = memberships.membership_number" . $query_condition . " ORDER BY customers.membership_number ASC LIMIT " . $shortno;
		}
		$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
		
		return $assoc_array;
		
		
		 
		//return "faFA";
		
		}
		
        private function getRecordFromCustomers() {
            $sql = "select * from customers "
                    . " where  " . $this->getUniqueMemberSqlCond() .  " limit 1 ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }

        private function getRecordFromMemberships() {
            $sql = "select * from memberships "
                    . " where " . $this->getUniqueMemberSqlCond() .  " limit 1";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }

        private function getRecordFromMemberLogin() {
            $sql = "select * from member_login "
                    . " where " . $this->getUniqueMemberSqlCond() .  "  limit 1";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }

        private function getRecordFromMembershipRenewal() {
            $sql = "select * from membership_renewal  "
                    . " where " . $this->getUniqueMemberSqlCond() .  "  limit 1";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
        
        private function getRecordFromEmergencyContact() {
            $sql = "select * from emergency_contact  "
                    . " where " . $this->getUniqueMemberSqlCond() .  "  limit 1";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
        
        private  function getRecordFromDriversLicense(){
            $sql = " select * from driver_license_info  where " . 
                    $this->getUniqueMemberSqlCond() . " limit 1";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
        
        private function getRecordFromCertificates() {
            $sql = "select * from certs_and_safety "
                    . " where  " . $this->getUniqueMemberSqlCond() .  " limit 1 ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
        
        private function getRecordFromTable($table){
            $sql = "select * from `$table` "
                    . " where  " . $this->getUniqueMemberSqlCond() .  " limit 1 ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }

        private function getUniqueMemberSqlCond(){
            return " acctnumberhashed = '" . 
                    $this->link->real_escape_string($this->accHashKey) . "' "
                    . " and accountnumber = '" . 
                    $this->link->real_escape_string($this->accNo) . "' "
                    . " and membership_number = '" . 
                    $this->link->real_escape_string($this->memberNo) . "'";
        }
        public static function get_id_card_creator(Application $app, $hash, $accno) {
			
			$sql = "SELECT * FROM id_card_creator WHERE acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' ";
		
			$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
		
			return $assoc_array;
			
		}
		public static function get_customer(Application $app, $hash, $accno, $membershipNo) {
			
			$sql = "SELECT * FROM customers WHERE membership_number = '".$membershipNo."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."'";
			
			$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
			
			return $assoc_array;
		}
		public static function get_emergency_contact(Application $app, $hash, $accno, $membershipNo) {
			
			$sql = "SELECT * FROM emergency_contact WHERE membership_number = '".$membershipNo."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."'";
			
			$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
			
			return $assoc_array;
		}
		public static function get_driver_license_info(Application $app, $hash, $accno, $membershipNo) {
			
			$sql = "SELECT * FROM driver_license_info WHERE membership_number = '".$membershipNo."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."'";
			
			$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
			
			return $assoc_array;
		}
		public static function get_memberships(Application $app, $hash, $accno, $membershipNo) {
			
			$sql = "SELECT * FROM memberships WHERE membership_number = '".$membershipNo."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."'";
			
			$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
			
			return $assoc_array;
		}
		public static function get_certs_and_safety(Application $app, $hash, $accno, $membershipNo) {
			
			$sql = "SELECT * FROM certs_and_safety WHERE membership_number = '".$membershipNo."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."'";
			
			$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
			
			return $assoc_array;
		}
		public static function get_controller_config(Application $app, $hash, $accno, $membershipNo) {
			
			$sql = "SELECT * FROM controller_config WHERE acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."'";
			
			$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
			
			return $assoc_array;
		}
		public static function get_member_login(Application $app, $hash, $accno, $membershipNo) {
			
			$sql = "SELECT * FROM member_login WHERE membership_number = '".$membershipNo."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."'";
			
			$thislink = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$assoc_array = DbHelper::dbFetchAllAssoc($thislink, $sql);
			
			return $assoc_array;
		}
        

    }
}


