<?php

namespace Rc\Models {
    
    class Address {

        public $street = '';
        public $city = '';
        public $state = ''; 
        public $zipcode = '';
        function __construct($street, $city, $state, $zip) {
            $this->street = $street;
            $this->city = $city;
            $this->state = $state; 
            $this->zipcode = $zip;
        }
        
        public function getFullAddress($lineSep = ' '){
            return $this->street . 
                    (empty($this->street) ? '' : $lineSep) . 
                    $this->city . 
                    (!empty($this->city) && !empty($this->state) ? ', ' : '') . 
                    $this->state . ' ' . $this->zipcode;
        }

    }
}
