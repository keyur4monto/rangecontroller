<?php
namespace Rc\Models;

class admin_owner_id {
    protected $customer_number;
	
    /**
     * 
     * @param type $accHashKey
     * @param type $accNo
     */
    public function __construct($customer_number) {
        $this->customer_number = $customer_number;
     
    }
    
    public function getcustomer_number(){
        return $this->customer_number;
    }
    public function getSqlCond(
            \mysqli $link, 
            $customer_number = 'customer_number', 
            $tblPrefix = ''){
        
        $tblPrefix = !empty($tblPrefix) ? $tblPrefix . '.' : '';
        return " $tblPrefix`$customer_number` = '" . 
                $link->real_escape_string($this->customer_number) . "' "
                . "  ";
    }
    
    public function getIdArray(
            $hashKeyColumnName = 'customer_number')
			{
            return array(
                $hashKeyColumnName => $this->customer_number,
            );
        }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

