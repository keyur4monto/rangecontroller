<?php
namespace Rc\Models {
    use Rc\Services\DbHelper;
    use Rc\Services\StringHelper;
        
    class billing {
        /**
         *
         * @var \mysqli
         */
        protected $link;     
        /**
         *
         * @var \Rc\Models\range_owner_id 
         */
        protected $shooter_owner_id;

        protected $accHashKey;
        // also known as range number
        protected $accNo;

        protected $subject;
        protected $content;
        protected $timestamp;
        


        //private constructor will prevent this class from being initialized 
        // like new range_owner(). We want it to be initialized by using
        // range_owner::initWithAccount(...)
        private function __construct() {               
        }

        
        /**
         * returns account number
         * @alias getAccountNumber
         * @return string
         */
        public function getRangeNumber(){
            return $this->accNo;
        }

        public function getAccountNumber(){
            return $this->accNo;
        }

        public function getAccountHashKey(){
            return $this->accHashKey;
        }

        /**
         * Returns the address parts as an array so you can join them with \n
         *  or <br /> depending on your environment
         * @return array
         */
        
        
        public function getMemberNumberStartCount(){
            return $this->memberNumberStartCount;
        }

        /**
         * Returns a new range_owner object populated for this account
         * @param mysqli $link
         * @param string $accHashKey
         * @param string|int $accNo
         * @return \range_owner
         */
		 
        public static function initWithAccount($link, $accHashKey, $accNo) {
            $ro = new billing();
            $ro->link  = $link;       
            $ro->accHashKey = $accHashKey;
            $ro->accNo = $accNo;
            $result = $ro->populate();
            if ($result) {
                $ro->shooter_owner_id = new \Rc\Models\shooter_owner_id($accHashKey, $accNo);
                return $ro;
            }
            return null;
		}
		protected function populate(){
            $ctrlCfgRow = $this->getRecordFromControllerConfig();
            if (empty($ctrlCfgRow)) {
                return false;
            }
            $signupInfo = $this->getRecodFromSignupContactInfo();
            if (empty($signupInfo)) {
                return false;
            }

            $this->companyName = $ctrlCfgRow['company_name'];
            $this->companyLogo = $ctrlCfgRow['company_logo'];
            $this->email = $ctrlCfgRow['registered_email'];
            
            $this->memberNumberStartCount = $ctrlCfgRow['member_number_start_count'];

            $this->phone = $signupInfo['phone_number'];
            $this->firstName = $signupInfo['first_name'];
            $this->lastName = $signupInfo['last_name'];
            $this->addressStreet = trim($signupInfo['street_address']);
            $this->addressCity = trim($signupInfo['city']);
            $this->addressState = trim($signupInfo['state']);
            $this->addressZip = trim($signupInfo['zipcode']);

            return true;        
        }
		protected function getRecordFromControllerConfig() {
            $sql = "select * from controller_config "
                    . " where  acctnumberhashed = '" . 
                    $this->link->real_escape_string($this->accHashKey) . "' "
                    . " and accountnumber = '" . 
                    $this->link->real_escape_string($this->accNo) . "' ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
		protected function getRecodFromSignupContactInfo(){
            $sql = "select * from signup_contact_info "
                    . " where  acctnumberhashed = '" . 
                    $this->link->real_escape_string($this->accHashKey) . "' "
                    . " and accountnumber = '" . 
                    $this->link->real_escape_string($this->accNo) . "' ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
		public function getbilling($member_number, $hash, $accno) {
			$sel_billing_sql = "SELECT * FROM billing_info WHERE membership_number = ".$member_number." AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' ORDER BY `date` DESC"; 
			return $this->link->query($sel_billing_sql);
		}
		public function getselpoint($member_number, $hash, $accno) {
			$sel_pos_sql = "SELECT * FROM pos_new_sale WHERE membership_number=".$member_number." AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' ORDER BY date_stamp DESC";
			return $this->link->query($sel_pos_sql);
		}
    }
}