<?php

namespace Rc\Models {
    
    use Rc\Services\DbHelper;    
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    
    class server_email_template_types {
        /**
         *
         * @var \mysqli
         */
        protected $link; 

        // These are the id values from the server_email_template_types table 
        // corresponding to each template type 
        const MEMBERSHIP_SIGN_UP_ID = 1;
        const SCHEDULER_EMAIL_ID = 2;
        const MEMBERSHIP_RENEWAL_ID = 3;

        public function __construct($link) {
            $this->link  = $link;
        }

        public function getTemplateTypes(){
            $sql = " select sett_id, sett_name, sett_description "
                    . " from server_email_template_types order by sett_id ";
            return DbHelper::dbFetchAllAssoc($this->link, $sql);
        }

        public static function getTemplateVariables(){
            $vars = array(
            '[company-name]' => array(
                'desc' => 'Displays the name of your company', 
                'example' => 'Test Range Co.'),
            '[company-phone]' => array(
                'desc' => 'Displays the phone number of your company', 
                'example' => '(123)-456-7890'),
            '[shooter-login-link]' => array(
                'desc' => 'Displays the shooter login link', 
                'example' => 'http://www.rangecontroller.com'),
            '[range-number]' => array(
                'desc' => 'Displays your company\'s range number', 
                'example' => '10000'),
            '[company-address]' => array(
                'desc' => 'Displays the address of your company', 
                'example' => '101 Main Street <br />Nashville, TN 12345'),
            '[company-email]' => array(
                'desc' => 'Displays your company\'s email address', 
                'example' => 'hello@testRangeCo.com'),
            '[scheduled-lanes-times]' => array(
                'desc' => 'Displays the schedules lanes and times for each lane',
                'example' => 'Lane 1: 7:30, 8:00, 9:00<br />Lane 2: 8:30, 9:00'
                ),
            '[scheduled-date]' => array(
                'desc' => 'Displays the date of the scheduled shooting', 
                'example' => 'Tuesday Nov. 4, 2014'),
                /*
            '[scheduled-time-slot]' => array(
                'desc' => 'Displays the time slot(s) for the scheduled shooting', 
                'example' => '7pm - 8:30pm'),

            '[scheduled-lane]' => array(
                'desc' => 'Displays the lane(s) of the scheduled shooting', 
                'example' => '1'),*/
            '[membership-expiration-date]' => array(
                'desc' => 'Displays the membership expiration date of '
            . 'the receiver of this email', 
                'example' => '8/5/2014'),
            '[membership-type]' => array(
                'desc' => 'Displays the type of membership such as Bronze, Silver, etc', 
                'example' => 'Bronze'),
            '[member-first-name]' => array(
                'desc' => 'Displays the FIRST name of the receiver of this email such as John', 
                'example' => 'John'),
            '[member-last-name]' => array(
                'desc' => 'Displays the LAST name of the receiver of this email such as Doe', 
                'example' => 'Doe'),
            '[member-full-name]' => array(
                'desc' => 'Displays the full name of the receiver of this email such as John Doe', 
                'example' => 'John Doe'),        
            '[membership-number]' => array(
                'desc' => 'Displays the membership number of the receiver of this email', 
                'example' => '123'),
            '[temporary-password]' => array(
                'desc' => 'Displays the temporary password of the receiver of this email', 
                'example' => 'agWetgy'),
            '[member-email]' => array(
                'desc' => 'Displays the email address of the receiver of this email', 
                'example' => 'john@doe.com')

            );
            ksort($vars);
            return $vars;
        }

        public static function getVariablesForType($typeId){
            $vars = self::getTemplateVariables();
            $pick = null;
            if ($typeId == self::MEMBERSHIP_RENEWAL_ID) {
                $pick = array(
                    '[company-name]', 
                    '[membership-expiration-date]', 
                    '[shooter-login-link]',
                    '[member-first-name]',
					'[membership-type]'
                );            
            } else if ($typeId == self::SCHEDULER_EMAIL_ID) {
                $pick = array(
                    '[company-name]', 
                    '[company-phone]', 
                    '[shooter-login-link]', 
                    //'[scheduled-time-slot]', 
                    '[scheduled-date]',
                    //'[scheduled-lane]',
                    '[scheduled-lanes-times]',
                    '[member-first-name]',
                    '[member-last-name]',
                    '[member-full-name]');
            } else if ($typeId == self::MEMBERSHIP_SIGN_UP_ID) {
                $pick = array(
                    '[member-full-name]',
                    '[member-first-name]',
                    '[member-last-name]',
                    '[range-number]',
                    '[membership-number]',
                    '[temporary-password]',
                    '[member-email]',
                    '[membership-type]',
                    '[company-name]',
                    '[company-phone]', 
                    '[company-email]',
                    '[shooter-login-link]',
                );
            }

            $result = array();
            if (!empty($pick)) {
                sort($pick);
                foreach($pick as $i => $v) {
                    $result[$v] = $vars[$v];
                }
            }
            return $result;
        }

        public static function getTemplateVarValuesForSite(){
            return array(
                '[shooter-login-link]' => 'https://rangecontroller.com'
            );
        }

        /**
         * returns key value pairs for company related template placeholders 
         * @param range_owner $range_owner
         * @return array 
         */
        public static function getTemplateVarValuesForRangeOwner(range_owner $range_owner){
            $data = array();
            $data['[company-name]'] = $range_owner->getCompanyName();
            $data['[company-phone]'] = $range_owner->getPhoneNumber();
            //$data['$shooter_login_link']
            $data['[range-number]'] = $range_owner->getRangeNumber();
            $data['[company-address]'] = implode('<br />', $range_owner->getCompanyAddress());
            $data['[company-email]'] = $range_owner->getEmail();
            return $data;
        }

        /**
         * Returns member related placeholders with EXCEPTION of schedule info
         *  they are not recorded in db
         * @param range_member $range_member
         * @return array
         */
        public static function getTemplateVarValuesForMember(range_member $range_member){
            return array(
                '[member-full-name]' => $range_member->getFullName(),
                '[member-first-name]' => $range_member->getFirstName(),
                '[member-last-name]' => $range_member->getLastName(),           
                '[membership-number]' => $range_member->getMembershipNumber(),
                '[temporary-password]' => $range_member->getTemporaryPassword(),
                '[member-email]' => $range_member->getEmail(),
                '[membership-type]' =>$range_member->getMembershipType(),
                '[membership-expiration-date]' => $range_member->getMembershipExpirationDate()        );
        }

        /**
         * Returns key => value pairs for schedule related template placeholders 
         * @param string $timeSlot
         * @param string $lane
         * @param string $date
         * @return array
         */
        public static function getTemplateVarValuesForSchedule($schedule, $date){
            $schedule_str = '';
            foreach($schedule as $lane => $times_arr) {
                $schedule_str .= 'Lane ' . $lane . ': ' . implode(', ', $times_arr) . '<br />';
            }
            return array(
                '[scheduled-lanes-times]' => $schedule_str,

                '[scheduled-date]' => $date
            );
        }



    }
}
