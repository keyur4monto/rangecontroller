<?php
namespace Rc\Models {
    use Rc\Services\DbHelper;
    use Rc\Services\StringHelper;
        
    class admin_newsletter {
        /**
         *
         * @var \mysqli
         */
        protected $link;     
        /**
         *
         * @var \Rc\Models\range_owner_id 
         */
        protected $admin_owner_id;

        protected $customer_number;
        // also known as range number

        protected $subject;
        protected $content;
        protected $timestamp;
        


        //private constructor will prevent this class from being initialized 
        // like new range_owner(). We want it to be initialized by using
        // range_owner::initWithAccount(...)
        private function __construct() {               
        }

        
        /**
         * returns account number
         * @alias getAccountNumber
         * @return string
         */
       
        public function getcustomer_number(){
            return $this->customer_number;
        }

        /**
         * Returns the address parts as an array so you can join them with \n
         *  or <br /> depending on your environment
         * @return array
         */
        
        
        public function getMemberNumberStartCount(){
            return $this->memberNumberStartCount;
        }

        /**
         * Returns a new range_owner object populated for this account
         * @param mysqli $link
         * @param string $accHashKey
         * @param string|int $accNo
         * @return \range_owner
         */
		 
        public static function initWithAccount($link, $customer_number) {
            $ro = new admin_newsletter();
            $ro->link  = $link;       
            $ro->customer_number = $customer_number;
            return $ro;
		}
		
		public function getNewsletter($customer_number) 
		{
		   $sel_newsletter_sql = "SELECT * FROM admin_newsletter";
		   return DbHelper::dbFetchAllAssoc($this->link, $sel_newsletter_sql);
		}
		public function addNewsletter($company_name, $subject, $message_body) 
		{
			$sel_newsletter_sql = "INSERT INTO admin_newsletter (`timestamp`,`subject`,`content`) VALUES ('".date("y-m-d h:i:s")."','".addslashes($subject)."','".addslashes($message_body)."')";
			return $this->link->query($sel_newsletter_sql);
		}
		public function getNewsletterLastId() 
		{
		   $sel_newsletter_sql = "SELECT MAX( newsletter_id ) as newsletter_id FROM admin_newsletter";
		   return DbHelper::dbFetchRowAssoc($this->link, $sel_newsletter_sql);
		}
		public function addEmail($email, $newsletter_id) 
		{
			$sel_newsletter_sql = "INSERT INTO newsletter_email_list (`date`,`email`,`newsletter_id`) VALUES ('".date("y-m-d h:i:s")."','".$email."','".$newsletter_id."')";
			return $this->link->query($sel_newsletter_sql);
		}
		public function updateEmail($email, $newsletter_id) 
		{
			$sel_newsletter_sql = "UPDATE newsletter_email_list SET date = '".date("y-m-d h:i:s")."', email = '".$email."' WHERE newsletter_id = '".$newsletter_id."' ";
			return $this->link->query($sel_newsletter_sql);
		}
		public function getNewsletterId($newsletter_id) 
		{
		   $sel_newsletter_sql = "SELECT * FROM admin_newsletter where newsletter_id = '".$newsletter_id."'";
		   return DbHelper::dbFetchRowAssoc($this->link, $sel_newsletter_sql);
		}
		public function getEmailLists($newsletter_id) 
		{
		   $sel_newsletter_sql = "SELECT * FROM newsletter_email_list where newsletter_id = '".$newsletter_id."'";
		   return DbHelper::dbFetchAllAssoc($this->link, $sel_newsletter_sql);
		}
		public function getEmailName($e_id) 
		{
			if($e_id['email'] == 'everyone')
			{
				$sel_newsletter_sql = "SELECT registered_email,accountnumber,company_name FROM controller_config";
				return DbHelper::dbFetchAllAssoc($this->link, $sel_newsletter_sql);
			}
			else
			{
				$a = trim($e_id['email'],'[ ]');
				$sel_newsletter_sql = "SELECT registered_email,accountnumber,company_name FROM controller_config where username in ($a)";
				return DbHelper::dbFetchAllAssoc($this->link, $sel_newsletter_sql);
			}
		}
		public function editNewsletter($newsletter_id, $subject, $message_body) 
		{
		   $sel_newsletter_sql = "UPDATE admin_newsletter SET timestamp = '".date("D M j g:i:s Y")."', subject = '".addslashes($subject)."', content = '".addslashes($message_body)."' WHERE newsletter_id = '".$newsletter_id."' ";
		   return $this->link->query($sel_newsletter_sql);
		}
		public function deleteNewsletter($newsletter_id) 
		{
		   $sel_newsletter_sql = "DELETE FROM `admin_newsletter` WHERE newsletter_id = $newsletter_id";
		   return $this->link->query($sel_newsletter_sql);
		}
		public function getNewslettersjson($filter){            
			$result = array();
			$membershiptype = "SELECT * FROM admin_newsletter WHERE 1=1";
			//where condition
	
			if(!empty($filter['timestamp']))
				$membershiptype .= " and `timestamp` <= '".$filter['timestamp']." 59:59:00' and `timestamp` >= '".$filter['timestamp']." 00:00:00'";
			if(!empty($filter['month']))
				$membershiptype .= " and MONTH( timestamp )=".$filter['month'];
			if(!empty($filter['year']))
				$membershiptype .= " and YEAR( `timestamp` )=".$filter['year'];
			$membershiptype.=" order by `timestamp` desc ";
		
			//$membershiptype .=" orderby `timestamp` desc";						
			$memtype_list = $this->link->query($membershiptype);
			//return DbHelper::dbFetchRowAssoc($this->link, $sql);
			$i = 0;
			while($row = $memtype_list->fetch_assoc()){
				$result[$i]['timestamp']       = date("l, F j, Y",strtotime($row['timestamp']));
				$result[$i]['subject']         = $row['subject'];
				$result[$i]['content']         = $row['content'];
				$result[$i]['newsletter_id']         = $row['newsletter_id'];
				$i++;								
			}		
			
			header('Content-type: application/json');
			echo json_encode($result);
			die;
        }
		public function getEmailById($selEmailAddresses) 
		{
		   $sel_email_sql = "SELECT * FROM controller_config where accountnumber = '".$selEmailAddresses."'";
		   return DbHelper::dbFetchRowAssoc($this->link, $sel_email_sql);
		}
		public function getEmail() 
		{
		   $sel_email_sql = "SELECT * FROM controller_config";
		   return DbHelper::dbFetchAllAssoc($this->link, $sel_email_sql);
		}
		public function getgrouomemberslist($pos){        	
			
			$mode=(isset($pos['task'])) ? $pos['task'] : 'default';
			if($mode=='searchmember')
			{
				//get member list
				$q=$pos['q'];

				$result=array();
				
				$sql = "SELECT * FROM controller_config where company_name like '$q%'";
				
				$members = $this->link->query($sql);
				
				$i=0;
				while($row=$members->fetch_assoc()){
						$result[$i]['id']=$row['accountnumber'];
						$result[$i]['label']=$row['company_name'];
						$i++;
				}
				//
				header('Content-type: application/json');
				echo json_encode($result);
				die;
			}
                	
        }
    }
}