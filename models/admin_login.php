<?php
namespace Rc\Models {
    use Rc\Services\DbHelper;
    use Rc\Services\StringHelper;
        
    class admin_login {
        /**
         *
         * @var \mysqli
         */
        protected $link;     
        /**
         *
         * @var \Rc\Models\range_owner_id 
         */
        protected $admin_owner_id;

        protected $customer_number;
        // also known as range number

        protected $subject;
        protected $content;
        protected $timestamp;
        


        //private constructor will prevent this class from being initialized 
        // like new range_owner(). We want it to be initialized by using
        // range_owner::initWithAccount(...)
        private function __construct() {               
        }

        
        /**
         * returns account number
         * @alias getAccountNumber
         * @return string
         */
       
        public function getcustomer_number(){
            return $this->customer_number;
        }

        /**
         * Returns the address parts as an array so you can join them with \n
         *  or <br /> depending on your environment
         * @return array
         */
        
        
        public function getMemberNumberStartCount(){
            return $this->memberNumberStartCount;
        }

        /**
         * Returns a new range_owner object populated for this account
         * @param mysqli $link
         * @param string $accHashKey
         * @param string|int $accNo
         * @return \range_owner
         */
		 
        public static function initWithAccount($link, $customer_number) {
            $ro = new admin_login();
            $ro->link  = $link;       
            $ro->customer_number = $customer_number;
            return $ro;
		}
		
		public function checklogin($txtAccountNumber, $txtAdminPassword) 
		{
		   $sel_sql = "SELECT customer_number, current_pw FROM admin_login WHERE customer_number='".$txtAccountNumber."'  AND current_pw='".$txtAdminPassword. "'";		
		   return DbHelper::dbFetchRowAssoc($this->link, $sel_sql);
		}
    }
}