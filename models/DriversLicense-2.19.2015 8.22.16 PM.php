<?php

namespace Rc\Models {
    use Rc\Services\StringHelper;
    use Rc\Services\ArrayHelper;
    class DriversLicense {

        public $state ;
        public $number;
        public $expirationDate;
        public $heightInches;
        public $heightFeet;
        public $weight;
        public $hairColor;
        public $eyeColor;
        public $race; 
        public $gender; 
        public $birthDate;
        public $birthCity;
        public $birthState;        
        public $birthCountry = '';
        public $comments; 
        function __construct() {
            
        }
        
        /**
         * 
         * @param array $arr
         * @return \Rc\Models\DriversLicense
         */
        public static function initWithTableRowArray($arr) {
            $dl = new DriversLicense();
            $dl->state = $arr['license_state'];
            $dl->number = $arr['license_number'];
            $expdate = trim($arr['license_expires']);
            $dl->expirationDate = str_replace('-','/', $expdate);
            $height = StringHelper::parseHeightToFeetAndInches($arr['license_height']);
            if(empty($height)) {
                $dl->heightFeet = '';
                $dl->heightInches = '';
            } else {
                $dl->heightFeet = $height['feet'];
                $dl->heightInches = $height['inches'];
            }
            $dl->weight = $arr['license_weight'];
            $dl->hairColor = $arr['license_hair'];
            $dl->eyeColor = $arr['license_eyes'];
            $dl->race = $arr['license_race'];
            $dl->gender = $arr['license_sex'];
            $dl->birthDate = str_replace('-','/',trim($arr['license_birthdate']));
            $dl->birthCity = $arr['license_birth_city'];
            $dl->birthState = $arr['license_birth_state'];
            $dl->birthCountry = ArrayHelper::getValForKeyWithDefault($arr, 'license_birth_country', '');
            $dl->comments = $arr['license_comments'];
            return $dl;
        }
        
        public function getHeightText(){
            if (empty($this->heightFeet)) {
                return '';
            }
            return $this->heightFeet . ' ft ' . 
                    round((float)$this->heightInches,1) . ' in';
        }
        
        public function getTableRowArray(){
            return [
                'license_state'=>  trim($this->state),
                'license_expires'=>  trim($this->expirationDate),
                'license_number'=>  trim($this->number),
                'license_height'=>  trim($this->getHeightText() ),
                'license_weight'=>  trim($this->weight),
                'license_hair'=> trim($this->hairColor ),
                'license_eyes'=>  trim($this->eyeColor),
                'license_race'=>  trim($this->race),
                'license_sex'=>  trim($this->gender),
                'license_birthdate'=>  trim($this->birthDate),
                'license_birth_city'=>  trim($this->birthCity),
                'license_birth_state'=>  trim($this->birthState),
                'license_birth_country'=> trim($this->birthCountry ),
                'license_comments'=>  trim($this->comments)
            ];
        }

    }
}