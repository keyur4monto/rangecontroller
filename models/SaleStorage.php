<?php

namespace Rc\Models {

	use Rc\Services\DbHelper;
    use Silex\Application;
    
    class SaleStorage {

        function __construct() {
            }
        
        public static function setSale(Application $app, $transaction_number){
			
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$membership_number = $app['request']->get('membership_number', '');
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			//get member name first
			$sql = "SELECT * FROM customers WHERE membership_number='" . $membership_number . "' AND accountnumber='" . $accno . "'";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			$member_name = $row['first_name'] . " " . $row['last_name'];
			
			$discount = $app['request']->get('discount', '');
			$discount = ((float) $discount) * -1;
			$discount = "" . $discount . "";
			
			$_SESSION['sale'.$transaction_number][] = array(
				'inventory_id' => $app['request']->get('inventory_id', ''),
				'transaction_number' => $app['request']->get('transaction_number', ''),
				'item_number' => $app['request']->get('item_number', ''),
				'brand' => $app['request']->get('brand', ''),
				'model' => $app['request']->get('model', ''),
				'description' => $app['request']->get('description', ''),
				'price' => $app['request']->get('price', ''),
				'extended_price' => $app['request']->get('extended_price', ''),
				'quantity' => $app['request']->get('quantity', ''),
				'total' => $app['request']->get('total', ''),
				'discount' => $discount,
				'member_name' => $member_name,
				'hash' => $hash,
				'accno' => $accno,
				'membership_number' => $membership_number,
				'employee_id' => $app['request']->get('employee_id', '')
			);
			
			/*$app['session']->set('sale', array(
				'inventory_id' => $app['request']->get('inventory_id', ''),
				'transaction_number' => $app['request']->get('transaction_number', ''),
				'item_number' => $app['request']->get('item_number', ''),
				'brand' => $app['request']->get('brand', ''),
				'model' => $app['request']->get('model', ''),
				'description' => $app['request']->get('description', ''),
				'price' => $app['request']->get('price', ''),
				'extended_price' => $app['request']->get('extended_price', ''),
				'quantity' => $app['request']->get('quantity', ''),
				'total' => $app['request']->get('total', ''),
				'discount' => $discount,
				'member_name' => $member_name,
				'hash' => $hash,
				'accno' => $accno,
				'membership_number' => $membership_number,
				'employee_id' => $app['request']->get('employee_id', '')
			));
*/
				
        return;
		}
		
		
		public static function storeSale(Application $app, $transaction_number){
			ini_set("display_errors", 1);
  error_reporting("1+2+4");

			//print_r($_SESSION['sale'.$transaction_number]);
			//die();
			//$sale = $app['session']->get('sale');
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			foreach($_SESSION['sale'.$transaction_number] as $sale)
			{
			
			$sql = "INSERT INTO pos_new_sale (inventory_id, acctnumberhashed, accountnumber, transaction_number, membership_number, member_name, date_stamp, item_number, brand, model, description, price, extended_price, discount, total, quantity, employee_id, payment1_method, payment1_amount, payment2_method, payment2_amount, change_due, void, return_item)
			VALUES ('" . $sale['inventory_id'] . "', '" . $sale['hash'] . "', '" . $sale['accno'] . "', '" . $sale['transaction_number'] . "', '" . $sale['membership_number'] . "',
			'" . $sale['member_name'] . "', '" . date("m/d/Y") . "', '" . $sale['item_number'] . "', '" . $sale['brand'] . "', '" . $sale['model'] . "', 
			'" . $sale['description'] . "', '" . $sale['price'] . "', '" . $sale['extended_price'] . "', '" . $sale['discount'] . "', '" . $sale['total'] . "', 
			'" . $sale['quantity'] . "', '" . $sale['employee_id'] . "', '0', '0', '0', '0', '0', 'empty', 'empty')";
			$link->query($sql);
			}
			unset($_SESSION['sale'.$transaction_number]) ;
		return;
		}

		
    }
}
