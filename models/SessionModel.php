<?php

namespace Rc\Models {
    use Silex\Application;
    class SessionModel {

        function __construct() {
            
        }
        
        public static function getAccHashKey(Application $app){
            return $app['session']->get('account_number_hashed', '');            
        }

        public static function getAccNo(Application $app){
            return $app['session']->get('account_number', ''); 
            
        }

    }
}