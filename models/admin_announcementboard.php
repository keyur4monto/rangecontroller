<?php
namespace Rc\Models {
    use Rc\Services\DbHelper;
    use Rc\Services\StringHelper;
        
    class admin_announcementboard {
        /**
         *
         * @var \mysqli
         */
        protected $link;     
        /**
         *
         * @var \Rc\Models\range_owner_id 
         */
        protected $admin_owner_id;

        protected $customer_number;
        // also known as range number

        protected $subject;
        protected $content;
        protected $timestamp;
        


        //private constructor will prevent this class from being initialized 
        // like new range_owner(). We want it to be initialized by using
        // range_owner::initWithAccount(...)
        private function __construct() {               
        }

        
        /**
         * returns account number
         * @alias getAccountNumber
         * @return string
         */
       
        public function getcustomer_number(){
            return $this->customer_number;
        }

        /**
         * Returns the address parts as an array so you can join them with \n
         *  or <br /> depending on your environment
         * @return array
         */
        
        
        public function getMemberNumberStartCount(){
            return $this->memberNumberStartCount;
        }

        /**
         * Returns a new range_owner object populated for this account
         * @param mysqli $link
         * @param string $accHashKey
         * @param string|int $accNo
         * @return \range_owner
         */
		 
        public static function initWithAccount($link, $customer_number) {
            $ro = new admin_announcementboard();
            $ro->link  = $link;       
            $ro->customer_number = $customer_number;
            return $ro;
		}
		
		public function getAnnouncementboard($customer_number) 
		{
		   $sel_blog_sql = "SELECT * FROM announcement_board order by announcementboard_id DESC";
		   return DbHelper::dbFetchAllAssoc($this->link, $sel_blog_sql);
		}
		public function addAnnouncementboard($message_body) 
		{
			$sel_announcement_sql = "INSERT INTO announcement_board (`timestamp`,`description`) VALUES ('".date("n/j/Y")."','".mysql_escape_string($message_body)."')";
			return $this->link->query($sel_announcement_sql);
		}
		public function getAnnouncementboards($announcementboard_id) 
		{
		   $sel_announcement_sql = "SELECT * FROM announcement_board where announcementboard_id = '".$announcementboard_id."'";
		   return DbHelper::dbFetchRowAssoc($this->link, $sel_announcement_sql);
		}
		public function editAnnouncementboard($announcementboard_id, $message_body) 
		{
		   $sel_announcement_sql = "UPDATE announcement_board SET description = '".mysql_escape_string($message_body)."' WHERE announcementboard_id = '".$announcementboard_id."' ";
		   return $this->link->query($sel_announcement_sql);
		}
		public function deleteAnnouncementboard($announcementboard_id) 
		{
		   $sel_announcement_sql = "DELETE FROM `announcement_board` WHERE announcementboard_id = $announcementboard_id";
		   return $this->link->query($sel_announcement_sql);
		}
    }
}