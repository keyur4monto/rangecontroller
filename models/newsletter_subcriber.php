<?php
namespace Rc\Models {
    use Rc\Services\DbHelper;
    use Rc\Services\StringHelper;
        
    class newsletter_subcriber {
        /**
         *
         * @var \mysqli
         */
        protected $link;     
        /**
         *
         * @var \Rc\Models\range_owner_id 
         */
        protected $newsletter_id;

        protected $accHashKey;
        // also known as range number
        protected $accNo;

        protected $subject;
        protected $content;
        protected $timestamp;
        


        //private constructor will prevent this class from being initialized 
        // like new range_owner(). We want it to be initialized by using
        // range_owner::initWithAccount(...)
        private function __construct() {               
        }

        
        /**
         * returns account number
         * @alias getAccountNumber
         * @return string
         */
        public function getRangeNumber(){
            return $this->accNo;
        }

        public function getAccountNumber(){
            return $this->accNo;
        }

        public function getAccountHashKey(){
            return $this->accHashKey;
        }

        /**
         * Returns the address parts as an array so you can join them with \n
         *  or <br /> depending on your environment
         * @return array
         */
        
        
        public function getMemberNumberStartCount(){
            return $this->memberNumberStartCount;
        }

        /**
         * Returns a new range_owner object populated for this account
         * @param mysqli $link
         * @param string $accHashKey
         * @param string|int $accNo
         * @return \range_owner
         */
        public static function initWithAccount($link, $accHashKey, $accNo) {
            $ro = new newsletter_subcriber();
            $ro->link  = $link;       
            $ro->accHashKey = $accHashKey;
            $ro->accNo = $accNo;
            $result = $ro->populate();
            if ($result) {
                $ro->newsletter_id = new \Rc\Models\newsletter_id($accHashKey, $accNo);
                return $ro;
            }
            return null;
        }

        protected function populate(){
            $ctrlCfgRow = $this->getRecordFromControllerConfig();
            if (empty($ctrlCfgRow)) {
                return false;
            }
            $signupInfo = $this->getRecodFromSignupContactInfo();
            if (empty($signupInfo)) {
                return false;
            }

            $this->companyName = $ctrlCfgRow['company_name'];
            $this->companyLogo = $ctrlCfgRow['company_logo'];
            $this->email = $ctrlCfgRow['registered_email'];
            
            $this->memberNumberStartCount = $ctrlCfgRow['member_number_start_count'];

            $this->phone = $signupInfo['phone_number'];
            $this->firstName = $signupInfo['first_name'];
            $this->lastName = $signupInfo['last_name'];
            $this->addressStreet = trim($signupInfo['street_address']);
            $this->addressCity = trim($signupInfo['city']);
            $this->addressState = trim($signupInfo['state']);
            $this->addressZip = trim($signupInfo['zipcode']);

            return true;        
        }

        protected function getVal($bag, $key, $default = null, $trim = true){
            return isset($bag[$key]) ? $bag[$key] : $default;
        }
        
        public function getBillingInfo($memberNo = null, $id=null){
            $sql = "select payment_for, employee_name, acctnumberhashed, accountnumber, "
                    . "membership_number, id , "
                    . "payment_method, payment_amount, `date` from "
                    . " billing_info where " . 
                    $this->newsletter_id->getSqlCond($this->link) 
                    . (empty($memberNo) ? '' : 
                            ' and membership_number = ' . (int)$memberNo) 
                    . (empty($id) ? '' : ' and id = ' . (int)$id) 
                    . " order by id desc, str_to_date(`date`, '%m/%e/%Y') desc  ";
            
            return DbHelper::dbFetchAllAssoc($this->link, $sql);
        }    
        
        
        
        /**
         * 
         * @param array $filter
         * @param \Rc\Services\Pager $pager
         * @return \Rc\Services\PagedResults
         */
        public function getNewsletters($filter, \Rc\Services\Pager $pager){
			$membershiptype = "SELECT *,(select fname from employees where id=new_newsletter.emp_id) as emp_name FROM new_newsletter WHERE accountnumber='".$_SESSION['account_number']."' ";
			//where condition
			if(!empty($filter['employee_id']))
				$membershiptype .= " and emp_id='".$filter['employee_id']."' ";
			if(!empty($filter['timestamp']))
				$membershiptype .= " and `timestamp` <= '".$filter['timestamp']." 59:59:00' and `timestamp` >= '".$filter['timestamp']." 00:00:00'";
			if(!empty($filter['month']))
				$membershiptype .= " and EXTRACT(MONTH FROM `timestamp`)=".$filter['month'];
			if(!empty($filter['year']))
				$membershiptype .= " and EXTRACT(YEAR FROM `timestamp`)=".$filter['year'];
			$membershiptype.=" order by `timestamp` desc ";
			//$membershiptype .=" orderby `timestamp` desc";
			$memtype_list = $this->link->query($membershiptype);
			//return DbHelper::dbFetchRowAssoc($this->link, $sql);
			return $memtype_list;
        }
        
        public function getNewslettersjson($filter){            
			//$filter['timestamp'];
			//die;
			$result = array();
			$membershiptype = "SELECT *,(select fname from employees where id=new_newsletter.emp_id) as emp_name FROM new_newsletter WHERE 1=1 ";
			//where condition
			if(!empty($filter['employee_id']))
				$membershiptype .= " and emp_id='".$filter['employee_id']."' ";
			if(!empty($filter['timestamp']))
				$membershiptype .= " and `timestamp` <= '".$filter['timestamp']." 59:59:00' and `timestamp` >= '".$filter['timestamp']." 00:00:00'";
			if(!empty($filter['month']))
				$membershiptype .= " and MONTH( timestamp )=".$filter['month'];
			if(!empty($filter['year']))
				$membershiptype .= " and YEAR( `timestamp` )=".$filter['year'];
			$membershiptype.=" order by `timestamp` desc ";
			//$membershiptype .=" orderby `timestamp` desc";						
			$memtype_list = $this->link->query($membershiptype);
			//return DbHelper::dbFetchRowAssoc($this->link, $sql);
			$i = 0;
			while($row = $memtype_list->fetch_assoc()){
				$result[$i]['newsletter_id']   = $row['newsletter_id'];
				$result[$i]['emp_id']          = $row['emp_id'];
				$result[$i]['timestamp']       = date("D M j g:i:s Y",strtotime($row['timestamp']));
				$result[$i]['membership_type'] = $row['membership_type'];
				$result[$i]['subject']         = $row['subject'];
				$result[$i]['content']         = $row['content'];
				$result[$i]['emp_name']        = $row['emp_name'];
				$i++;								
			}						
			header('Content-type: application/json');
			echo json_encode($result);
			die;
			
			//return $memtype_list;
        }
		
        protected function getRecordFromControllerConfig() {
            $sql = "select * from controller_config "
                    . " where  acctnumberhashed = '" . 
                    $this->link->real_escape_string($this->accHashKey) . "' "
                    . " and accountnumber = '" . 
                    $this->link->real_escape_string($this->accNo) . "' ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }

        protected function getRecodFromSignupContactInfo(){
            $sql = "select * from signup_contact_info "
                    . " where  acctnumberhashed = '" . 
                    $this->link->real_escape_string($this->accHashKey) . "' "
                    . " and accountnumber = '" . 
                    $this->link->real_escape_string($this->accNo) . "' ";
            return DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
        
        public  function generateMembershipNumber(){
            $sql = " select member_number from control_member_number "
                    . " where " . $this->newsletter_id->getSqlCond($this->link) . 
                    " for update ";
            $this->link->query(' START TRANSACTION ');
            $rec = DbHelper::dbFetchRowAssoc($this->link, $sql);
            if (!$rec) {
                $this->link->query('ROLLBACK');
                $this->link->query(' START TRANSACTION ');
                
                $data = $this->newsletter_id->getIdArray();
                $data['member_number'] = (int)trim($this->getMemberNumberStartCount());
                $sql_ins = "insert into control_member_number (" . 
                        implode(', ', array_keys($data)) . ") values ('" .
                        implode("', '", array_values($data)) . "' ) "; 
                $this->link->query($sql_ins);
                $rec = DbHelper::dbFetchRowAssoc($this->link, $sql);
                if (empty($rec)) {
                    $this->link->query('ROLLBACK');
                    return false;
                }                
            }
            $memno = $rec['member_number'] + 1;
            $sql = " update control_member_number set member_number = " . 
                    $memno . " where " . $this->newsletter_id->getSqlCond($this->link);
            $this->link->query($sql);
            $this->link->query(' COMMIT');
            return $memno;            
            
        }
        
        public static function generateAccountNumber(){
            throw new Exception('not implemented');
        }
        
        public function getemplist(){
        	$employees = "SELECT * FROM employees WHERE accountnumber = '".$_SESSION['account_number']."' ORDER BY fname";
			$emp_list = $this->link->query($employees);
			return $emp_list;
        }
        
		public function getmemberslist(){
        	$membershiptype = "SELECT distinct(membership_type) type FROM memberships WHERE membership_type <> '' and accountnumber='".$_SESSION['account_number']."'";
			$memtype_list = $this->link->query($membershiptype);
			return $memtype_list;
        }
        
        public function getgrouomemberslist($pos){        	
			
			$mode=(isset($pos['task'])) ? $pos['task'] : 'default';
			if($mode=='searchmember')
			{
				//get member list
				$q=$pos['q'];
				$result=array();
				
				//$WHERE="where customers.membership_number=memberships.membership_number and memberships.accountnumber='".$_SESSION['account_number']."'  and customers.accountnumber='".$_SESSION['account_number']."' and ( CONCAT_WS(' ',memberships.accountnumber,customers.first_name,customers.last_name) like '%$q%' or customers.membership_number like '$q%'";
				$WHERE="where customers.is_delete=1 and customers.membership_number=memberships.membership_number and memberships.accountnumber='".$_SESSION['account_number']."'  and customers.accountnumber='".$_SESSION['account_number']."' and CONCAT_WS(' ',memberships.membership_number,customers.first_name,customers.last_name) like '%$q%'";
				//check membership type
				/*if(!empty($pos['m']))
					$WHERE.=" and memberships.membership_type = '".$pos['m']."' ";*/
				
				//$WHERE.=' ) ';
				$sql = "SELECT CONCAT_WS(' ',memberships.membership_number,customers.first_name,customers.last_name) as membername,customers.membership_number FROM customers,memberships $WHERE  limit 0,50";
				$members = $this->link->query($sql);
				$i=0;
				while($row=$members->fetch_assoc()){
						$result[$i]['id']=$row['membership_number'];
						$result[$i]['label']=$row['membername'];
						$i++;
				}
				//
				header('Content-type: application/json');
				echo json_encode($result);
				die;
			}
                	
        }
        
        public function getemaillist($id){
			
        	$membershiptype = "SELECT * FROM new_newsletter WHERE newsletter_id = $id";
			$memtype_list = $this->link->query($membershiptype);
			$gdata = $memtype_list->fetch_assoc();			
			$em = array();
			$em = json_decode($gdata['email']);				
			$newarray = implode(", ", $em);			
			if($gdata['email']!='null')
				$qur = "SELECT distinct(CONCAT_WS(' ',first_name,last_name)) as membername, customers.membership_number, new_newsletter.membership_type, new_newsletter.everyone FROM customers, new_newsletter WHERE  customers.is_delete=1 and new_newsletter.accountnumber=customers.accountnumber and new_newsletter.newsletter_id = $id and customers.accountnumber='".$_SESSION['account_number']."' and membership_number IN ($newarray)";
			else
				$qur = "SELECT new_newsletter.membership_type, new_newsletter.everyone FROM new_newsletter where new_newsletter.newsletter_id = $id and new_newsletter.accountnumber='".$_SESSION['account_number']."'";
			$emaillist = $this->link->query($qur);
			$result=array();
			$i=0;
			while($row=$emaillist->fetch_assoc()){					
					$result[$i]['id']=$row['membership_number'];
					$result[$i]['label']=$row['membername'];
					$result[$i]['m_type']=$row['membership_type'];
					$result[$i]['everyone']=$row['everyone'];
					$i++;
			}
			return $result;	
        }
        
        public function addnewnewsletter($pos){
       		$member_type = '';
			$member_type = $pos['member_type'];
       		$timestamp = date("D M j g:i:s Y");
			$receivers = json_encode($pos['receivers']);
			$subject = $pos['subject'];
			$message_body = $pos['message_body'];
			$sql_ins = "INSERT INTO new_newsletter(timestamp, subject, email, content) VALUES ('".$timestamp."','".$subject."','".$receivers."','".$message_body."')";  			
   			
   			$this->link->query($sql_ins);
						   
       	}

    }
}