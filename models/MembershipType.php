<?php

namespace Rc\Models {
    
    use Rc\Models\range_owner_id;   
    use Rc\Services\DbHelper;
    
    class MembershipType {
        protected $key; 
        protected $name;
        function __construct($membershipName, $membershipKey = '') {
            $this->name = $membershipName;
            $this->key = empty($membershipKey) ? $membershipName : $membershipKey;
        }
        
        public function getKey(){
            return $this->key;
        }
        public function getName(){
            return $this->name;
        }
        
        /**
         * 
         * @param \mysqli $link
         * @param \Rc\Models\range_owner_id $roid
         * @return array of \Rc\Models\MembershipType
         */
        public static function getMembershipTypesForAccount(\mysqli $link, 
                range_owner_id $roid){
            $sql = "select membership_name from editable_memberships where " . 
                    $roid->getSqlCond($link) . " order by membership_name ";
            $records = DbHelper::dbFetchAllAssoc($link, $sql);
            
            $results = [];
            foreach($records as $row) {
                $mt = new MembershipType($row['membership_name']);
                $results[] = $mt;
            }
            return $results;
        }
    }
}

