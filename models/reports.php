<?php
namespace Rc\Models {

    use Rc\Services\DbHelper;
    use Rc\Services\DateTimeHelper;
    use Rc\Services\ArrayHelper;
    use Rc\Services\CommonData;
    class reports {
        /**
         *
         * @var \mysqli 
         */
        protected $link;

        const REPORT_DAILY = 'daily';
        const REPORT_WEEKLY = 'weekly';
        const REPORT_MONTHLY = 'monthly';
        const REPORT_YEARLY = 'yearly';
        const REPORT_CUSOM = 'custom';

        public function __construct(\mysqli $link) {
            $this->link = $link;        
        }

        public static function getReportTypes(){
            return array(
                self::REPORT_DAILY => 'Daily Report',
                self::REPORT_WEEKLY => 'Weekly Report',
                self::REPORT_MONTHLY => 'Monthly Report',
                self::REPORT_YEARLY => 'Yearly Report',
                self::REPORT_CUSOM => 'Custom Report'
            );
        }


        /**
         * 
         * @param string $reportType
         * @return \DateTime
         */
        public static function getDefaultFromDateFor($reportType){

            switch ($reportType) {
                case self::REPORT_DAILY:
                    return new \DateTime();
                case self::REPORT_WEEKLY:
                    return new \DateTime('-6 days');
                case self::REPORT_MONTHLY:
                    return new \DateTime('-1 month');
                case self::REPORT_YEARLY:
                    return new \DateTime('-1 year');
                case self::REPORT_CUSOM:
                default:
                    break;
            }
            return new \DateTime;
        }

        /**
         * 
         * @param type $reportType
         * @return \DateTime
         */
        public static function getDefaultToDateFor($reportType){
            return new \DateTime;
        }

        /**
         * related date column in db is defined as string so i cannot do 
         *  date check on sql and thus i am returning 1=1
         * @param \DateTime $from
         * @param \DateTime $toExclusive
         * @param string $columnName
         * @return string
         */
        private function getDateCond(\DateTime $from, \DateTime $toExclusive, 
                $columnName, $tblPrefix = ''){
            $from_date = DateTimeHelper::dateToMysqlDateStr($from);
            $to_date = DateTimeHelper::dateToMysqlDateStr($toExclusive);
            $tblPrefix = empty($tblPrefix) ? '' :  $tblPrefix . '.';

            return " str_to_date($tblPrefix`$columnName`, '%m/%e/%Y') >= "
                    . "cast('$from_date' as date) "
                    . "and str_to_date($tblPrefix`$columnName`, '%m/%e/%Y') < "
                    . "cast('$to_date' as date)  ";
        }

        /**
         * converts a date in m/d/Y format such as 1/1/2014 to normal datetime
         * @param \DateTime $mdyDate
         * @param \DateTime $sep
         * @return \DateTime
         */
        public static function mdyToDatetime($mdyDate, $sep = '/'){
            $mydate = explode($sep, trim($mdyDate));
            if (count($mydate) != 3) {
                return null;
            }
            $y = (int)$mydate[2];
            $m = (int)$mydate[0];
            $d = (int)$mydate[1];
            if (strlen($y.'') == 4 && $m <= 12 && $d <= 31) {
                return new \DateTime("$y-$m-$d");
            }
            return null;

        }

        /**
         * filtering records to inclode those ?= from date and < toexclusive date 
         * @param type $records
         * @param \DateTime $from
         * @param \DateTime $toExclusive
         * @param type $dateColumnName
         * @return type
         */
        public function getRecordsWithinDateRange(
                $records, \DateTime $from, \DateTime $toExclusive, $dateColumnName){
            $results = [];
            foreach($records as $rec) {
                $dateTime = $this->mdyToDatetime($rec[$dateColumnName]);
                if (empty($dateTime)) {
                    continue;
                }
                if ($dateTime >= $from  && $dateTime < $toExclusive) {
                    $results[] = $rec;
                }            
            }
            return $results;
        }

        public function getPointOfSaleData(
        \Rc\Models\range_owner_id $rangeOwnerId, \DateTime $from, \DateTime $toExclusive)
        {        
            $sql = " SELECT * FROM pos_new_sale "
                    . "WHERE  " . $this->getDateCond($from, $toExclusive, 'date_stamp')
                    . " AND acctnumberhashed = '".$rangeOwnerId->getAccountHashKey()."' "
                    . "AND accountnumber = '".$rangeOwnerId->getAccountNo()."' "
                    . "ORDER BY transaction_number ";
            //print $sql;

            $data = DbHelper::dbFetchAllAssoc($this->link, $sql);
            //$data = $this->getRecordsWithinDateRange($data, $from, $toExclusive, 'date_stamp');
            return $data;
        }


        public function getPointOfSaleTotal($records){
            return \Rc\Services\ArrayHelper::arraySumColumn($records, 'total', true);
        }

        public function getFirearmRentalsData(
                \Rc\Models\range_owner_id $rangeOwnerId, \DateTime $from, \DateTime $toExclusive){        

            $sql = " select * from firearm_current_rentals "
                    . "WHERE acctnumberhashed = '".$rangeOwnerId->getAccountHashKey()."' "
                    . "AND accountnumber = '".$rangeOwnerId->getAccountNo()."' "
                    . "and " . $this->getDateCond($from, $toExclusive, 'date');
            //print $sql;
            $data = DbHelper::dbFetchAllAssoc($this->link, $sql);
            //$data = $this->getRecordsWithinDateRange($data, $from, $toExclusive, 'date');
            return $data;
        }

        public function getFirearmRentalsTotal($records){
            return ArrayHelper::arraySumColumn($records, 'total', true);
        }

        public function getMembershipNamesForAcct(\Rc\Models\range_owner_id $range_owner_id){
            $sql = "select distinct membership_name from editable_memberships "
                    . " where " . $range_owner_id->getSqlCond($this->link) . " "
                    . " order by membership_name ";
            $data = DbHelper::dbFetchAllAssoc($this->link, $sql);
            return ArrayHelper::arrayColumn($data, 'membership_name');
        }

        public function getMembershipsSoldData(
                \Rc\Models\range_owner_id $rangeOwnerId, \DateTime $from, 
                \DateTime $toExclusive){

            $sql = "select trim(bilin.membership_type) as membershipType, 
                sum(payment_amount) totalPayment, 
                count(*) as totalMembershipsSold 
                from billing_info bilin where " . 
                    $rangeOwnerId->getSqlCond($this->link). " 
                and " . $this->getDateCond($from, $toExclusive, 'date') ." 
                    group by membership_type ";
            $data_total = DbHelper::dbFetchAllAssoc($this->link, $sql);
            $data_total_indexed = array_change_key_case(
                    ArrayHelper::arrayIndexBy($data_total, 'membershipType'), CASE_LOWER);
            /*
            $sql2 = "select trim(membership_type) as membershipType, 
                count(*) as totalMembershipsSold 
                from billing_info  where " . 
                    $rangeOwnerId->getSqlCond($this->link). " 
                and " . $this->getDateCond($from, $toExclusive, 'membership_date') ." 
                    group by membership_type ";

            $data_count = DbHelper::dbFetchAllAssoc($this->link, $sql2);
            $data_count_indexed = array_change_key_case(
                    ArrayHelper::arrayIndexBy($data_count, 'membershipType'), CASE_LOWER);

            foreach($data_total_indexed as $k => &$v) {
                if (isset($data_count_indexed[$k])) {
                    $v['totalMembershipsSold'] = 
                            $data_count_indexed[$k]['totalMembershipsSold'];
                } else {
                    $v['totalMembershipsSold'] = 0;
                }
            }
            */

            //print_r($data_grouped);
            $results =  $data_total_indexed;        
            $memnames = $this->getMembershipNamesForAcct($rangeOwnerId);
            foreach($memnames as $mn) {
                $mn_ = strtolower(trim($mn));
                if (!isset($results[$mn_])) {
                    $results[$mn_] = [
                        'membershipType' => $mn,
                        'totalMembershipsSold' => 0,
                        'totalPayment' => 0
                    ];
                }
            }
            ksort($results);
            //print_r($results);

            return $results;

        }

        public function getMembershipsSoldTotal($records){
            return ArrayHelper::arraySumColumn($records, 'totalPayment', true);
        }

        public function getTransactionsByTypeData(
                \Rc\Models\range_owner_id $rangeOwnerId, \DateTime $from, \DateTime $toExclusive){


	    $data_proto = [];
	    $sql1 = "select trim(k.pm) as paymentMethod,sum(totalPayment_) as totalPayment from ( "
                    . "SELECT trim(payment_method) as pm , sum(payment_amount) as totalPayment_ "
                    . "FROM billing_info WHERE " .  $rangeOwnerId->getSqlCond($this->link) . " " . " and " . $this->getDateCond($from, $toExclusive, 'date') . " "
                    . " group by payment_method) k group by k.pm order by k.pm";


	    $sql2 = "select * from pos_new_sale " . "where " . $rangeOwnerId->getSqlCond($this->link) . " "
                    . " and " . $this->getDateCond($from, $toExclusive, 'date_stamp') . "";

		//echo $sql2;


            $data__ = DbHelper::dbFetchAllAssoc($this->link, $sql2);      
            $data_ = DbHelper::dbFetchAllAssoc($this->link, $sql1);      
	    foreach ($data_ as $dat) {
			$data_proto[$dat['paymentMethod']] += number_format($dat['totalPayment'], 2);
		}

	   $seen = [];
	   foreach ($data__ as $dat) {
			if (in_array($dat['transaction_number'], $seen)) {
				continue;
			}
			$data_proto[$dat['payment1_method']] += (double) number_format($dat['payment1_amount'], 2);
			$data_proto[$dat['payment2_method']] += (double) number_format($dat['payment2_amount'], 2);

			$seen[] = $dat['transaction_number'];
		}


            $sql = " 
                select trim(k.pm) as paymentMethod,sum(totalPayment_) as totalPayment from ( "
                    . "SELECT trim(payment_method) as pm , sum(payment_amount) as totalPayment_ "
                    . "FROM billing_info WHERE " . 
                    $rangeOwnerId->getSqlCond($this->link) . " "
                    . " and " . $this->getDateCond($from, $toExclusive, 'date') . " "
                    . " group by payment_method "
                . " union "
                    . " select trim(payment1_method) as pm, sum(total) as totalPayment_"
                    . " from pos_new_sale "
                    . "where " . $rangeOwnerId->getSqlCond($this->link) . " "
                    . " and " . $this->getDateCond($from, $toExclusive, 'date_stamp') . " " 
                    . " group by payment1_method "
                . " ) k group by k.pm order by k.pm ";

            //print $sql;
            $data = DbHelper::dbFetchAllAssoc($this->link, $sql);      
            $data_indexed = ArrayHelper::arrayIndexBy($data, 'paymentMethod');
            $data_indexed_lc = array_change_key_case($data_indexed, CASE_LOWER);
            $keys = array_keys($data_indexed_lc);

            $totals = [];
            foreach($data_indexed_lc as $k => $v) {
		if (!in_array($v['totalPayment'], $seen))
                	$totals[$v['paymentMethod']] = $v['totalPayment'];

            }

		$totals = $data_proto;

            $ttypes = self::getTransactionTypesWithZeroFills();
            foreach ($ttypes as $k => $v) {
                $kk = trim(strtolower($k));
                if (!in_array($kk, $keys) && !isset($totals[$k])) {
                    $totals[$k] = 0;

                }
            }
            ksort($totals);
	    unset($totals['n/a']);

           return $totals;


        }

        public static function getTransactionTypesWithZeroFills(){
            $transactionType = array_values(CommonData::getPaymentTypes());

            $res = array_combine($transactionType, 
                    array_fill(0, count($transactionType), 0));
            return $res;
        }

        public function getTransactionsByTypeTotal($records){
            return ArrayHelper::arraySumColumn($records, 'totalPayment', true);
        }


    }
}
