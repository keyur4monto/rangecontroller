<?php
namespace Rc\Models; 

use Rc\Models\range_owner;
use Rc\Models\range_member;
use Rc\Models\server_email_templates;
use Rc\Models\server_email_template_types;
use Rc\Services\DbHelper;
use Rc\Services\ArrayHelper;
use Rc\Services\MailHelper;

class server_email_templates {
    /**
     *
     * @var mysqli
     */
    protected $link; 
    public function __construct($link) {
        $this->link  = $link;        
    }
    
    /**
     * Sends the email with given info
     * @param string $email_from
     * @param string $email_to
     * @param string $subject
     * @param string $message
     * @return bool
     */
    public static function sendMail($email_from, $email_to, $subject, $message){
        return MailHelper::sendEmailCommon($email_from, $email_to, $subject, $message, 1);        
    }
    
    // going from owner's email to owner's email or shooter's email 
    public function sendTemplateMailForAccount(
            $accHashKey, $accNo, $tplTypeId, $data){ 
        
        $setpl = new server_email_templates($this->link);
        $tpl_raw = $setpl->getTemplateForAccount($accHashKey, $accNo,$tplTypeId);
        //print_r($tpl_raw);
        if(empty($tpl_raw)) {
            return false;
        }
        $tpl_filled = $setpl->getFilledTemplateForAccount($tpl_raw, $tplTypeId, $data);
        

        $range_owner = range_owner::initWithAccount($this->link, $accHashKey, $accNo);
        
        $res1 = false;
        $res2 = false;
        //print_r($tpl_filled);
        if ($tpl_raw['notify_shooter']) {            
            $res1 = server_email_templates::sendMail(
                $range_owner->getEmail(), $data['shooter_email'], 
                $tpl_filled['template_subject'], $tpl_filled['template_body']);    
        } 
        if ($tpl_raw['notify_admin']) {            
            $res2 = server_email_templates::sendMail(
                $range_owner->getEmail(), $range_owner->getEmail(), 
                $tpl_filled['template_subject'], $tpl_filled['template_body']); 
        }
        return $res1 || $res2;
    }
    
    /**
    * 
    * @param string $lanes comma separated values like 1,1,1,2,3
    * @param type $times comma separated times like 7:30,8:00, that are on same 
    *  index as lane number (if u were made an array with comma separation)
     * @return array indexed by lane numbers ex: array('1'=>array('7:30','8:00',..),'2'=>..)
    */
    public static function getScheduesPerLane($lanes, $times){
        $lanes_arr = explode(',', trim($lanes));
        $schedule_arr = explode(',', trim($times));
        //print_r($lanes_arr);
        //print_r($schedule_arr);

        $scount = count($schedule_arr);
        $result = array();
        foreach($lanes_arr as $ind => $laneNo) {
            $laneNo = trim($laneNo);
            if ($laneNo == ''){
                continue;
            }
            $k = $laneNo . '';
            if (!array_key_exists($k, $result)) {
                $result[$k] = array();
            }
            if ($ind < $scount) {
                $result[$k][] = $schedule_arr[$ind];
            }        
        }
        return $result;
    }
    
    
    function sendScheduleMail($accHashKey, $accNo, $membershipNo, $schedule, $date){
        
        $range_owner = range_owner::initWithAccount($this->link, $accHashKey, $accNo);
        $range_member = range_member::initForMember(
                $this->link, $accHashKey, $accNo, $membershipNo);
        //print_r($range_owner);
        //print_r($range_member); 
        if (!$range_owner || !$range_member) {
            return false;
        }
        
        $data = array_merge(
            server_email_template_types::getTemplateVarValuesForSite(),
            server_email_template_types::getTemplateVarValuesForRangeOwner($range_owner),
            server_email_template_types::getTemplateVarValuesForMember($range_member),
            server_email_template_types::getTemplateVarValuesForSchedule($schedule, $date)
        );
        $data['shooter_email'] = $range_member->getEmail();
        //print_r($data);

        //print_r($data);
        $tplTypeId = server_email_template_types::SCHEDULER_EMAIL_ID;

        $set = new server_email_templates($this->link);
        $res =  $set->sendTemplateMailForAccount(
                $accHashKey, $accNo, $tplTypeId, $data);      
        //print 'send result' . $res;
        return $res;
    }

        
    /**
     * Returns template for this account for this type as 
     *  array('template_subject'=>..., 'template_body'=> ...) with filled values 
     * @param array $tpl - array having template_subject and template_body keys and vals
     * @param int $templateTypeId
     * @param array $data contains values for variables for this template type
     * @return type
     */
    public function getFilledTemplateForAccount($tpl, $templateTypeId, $data)
    {
        $reqs = array('template_subject', 'template_body');
        if (!ArrayHelper::arrayContainsAllTheseKeys($tpl, $reqs)) {
            throw new Exception('$tplData argument must have values for keys: ' . 
                    implode(',', $reqs) . '');
        }
        
        /*
        $tpl_vars = server_email_template_types::getVariablesForType($templateTypeId);
        if (!arrayContainsAllTheseKeys($data, array_keys($tpl_vars))) {
            throw new Exception('$data array must have values for these keys: ' . 
                    implode(', ', array_keys($tpl_vars)));
        } */
        
        $subject = $tpl['template_subject'];
        $body = $tpl['template_body'];
        
        foreach($data as $placeholder => $value) {
            $placeholder = str_replace('[', '\[', $placeholder);
            $placeholder = str_replace(']', '\]', $placeholder);
            $placeholder = str_replace('-', '\-', $placeholder);
            $subject = preg_replace("/". $placeholder . '/im', $value, $subject);
            $body = preg_replace("/". $placeholder . '/im', $value, $body);
        }
        $pre = $this->getPrependTemplate($templateTypeId);
        $post = $this->getAppendTemplate($templateTypeId);
        return array(
            'template_subject' => $subject,
            'template_body' => $pre . 
                '<div style="width:685px;">' . $body . '</div>'. 
            $post
        );
    }   
    
    /**
     * Restores the templates for the given account to defaults
     * @param type $accHashKey
     * @param type $accNo
     * @return boolean
     */
    public function restoreToDefaults($accHashKey, $accNo) {
        $sql_clean = " delete from server_email_templates "
            . "where acctnumberhashed = '" . 
                $this->link->real_escape_string($accHashKey) . "' "
                . " and accountnumber = '" . 
                $this->link->real_escape_string($accNo) . "' ";
        
        $sql_restore = " insert into server_email_templates "
            . "(acctnumberhashed, accountnumber, se_templatetype_fk, "
            . " template_subject, template_body, notify_admin, notify_shooter) "
            . " select '" . $this->link->real_escape_string($accHashKey) . "', "
            . " '" . $this->link->real_escape_string($accNo) . "', "
            . " se_templatetype_fk, "
            . " template_subject, template_body, notify_admin, notify_shooter "
            . " from server_email_templates "
            . "where acctnumberhashed='default' ";
        $res1 = $this->link->query($sql_clean);
        $res2 = $this->link->query($sql_restore);
        return $res1 && $res2;
    }
    
    /**
     * Sets the missing templates for this account to default templates
     * @param string $accHashKey
     * @param string $accNo
     * @return bool
     */
    public function setMissingTemplatesToDefaults($accHashKey, $accNo){        
        
        $sql = " insert into server_email_templates "
            . "(acctnumberhashed, accountnumber, se_templatetype_fk, "
            . " template_subject, template_body, notify_admin, notify_shooter) "
            . " select '" . $this->link->real_escape_string($accHashKey) . "', "
            . " '" . $this->link->real_escape_string($accNo) . "', "
            . " se_templatetype_fk, "
            . " template_subject, template_body, notify_admin, notify_shooter "
            . " from server_email_templates "
            . "where acctnumberhashed='default' "
            . "and se_templatetype_fk not in ( "
                . " select se_templatetype_fk from server_email_templates "
                . " where acctnumberhashed = '" . 
                    $this->link->real_escape_string($accHashKey) . "' "
                . " and accountnumber = '" . 
                    $this->link->real_escape_string($accNo) . "' "
            . " )";
        //echo $sql;
        return $this->link->query($sql);        
    }
    
    /**
     * Returns all of the default templates 
     * @return array
     */
    public function getDeafultTemplates(){
        $sql = " select id, template_subject, template_body, "
                . " notify_admin, notify_shooter, se_templatetype_fk "            
            . " from server_email_templates st " 
            . " where acctnumberhashed =  'default'"; 
        return DbHelper::dbFetchAllAssoc($this->link, $sql);        
    }
    
    /**
     * Returns a single row containing the template for the given type
     * @param int $templateTypeId
     * @return array
     */
    public function getDefaultTemplateForType($templateTypeId){
        $sql = " select id, template_subject, template_body, "
                . " notify_admin, notify_shooter, se_templatetype_fk "            
            . " from server_email_templates st " 
            . " where acctnumberhashed =  'default' "
                . " and se_templatetype_fk = " . (int)$templateTypeId; 
        return DbHelper::dbFetchRowAssoc($this->link, $sql);      
    }
    
    /**
     * Returns all of the templates for this particular account
     * @param string $accHashKey
     * @param string $accNo
     * @return array
     */
    public function getTemplatesForAccount($accHashKey, $accNo){
        // first ensure that templates for this account are inited to defaults
        $this->setMissingTemplatesToDefaults($accHashKey, $accNo);
        
        $sql = " select id, template_subject, template_body, "
                . " notify_admin, notify_shooter,se_templatetype_fk "
            . " from server_email_templates st "  
            . " where acctnumberhashed = '" . 
                $this->link->real_escape_string($accHashKey) . "' "
            . " and accountnumber = '" . 
                $this->link->real_escape_string($accNo) . "'";

        return DbHelper::dbFetchAllAssoc($this->link, $sql);        
    }
    
    /**
     * Returns a single row containing the template for the given account and given template type
     * @param type $accHashKey
     * @param type $accNo
     * @param type $templateTypeId
     * @return type
     */
    public function getTemplateForAccount($accHashKey, $accNo, $templateTypeId) {
        $sql = " select id, template_subject, template_body, "
                . " notify_admin, notify_shooter,se_templatetype_fk "
            . " from server_email_templates st "  
            . " where acctnumberhashed = '" . 
                $this->link->real_escape_string($accHashKey) . "' "
            . " and accountnumber = '" . 
                $this->link->real_escape_string($accNo) . "' "
                . " and se_templatetype_fk = " . (int)($templateTypeId);

        $res = DbHelper::dbFetchRowAssoc($this->link, $sql);    
        if (empty($res)) { // if empty, default template is probably not set yet
            $this->setMissingTemplatesToDefaults($accHashKey, $accNo);
            $res = DbHelper::dbFetchRowAssoc($this->link, $sql);
        }
        return $res;
    }
    
    /**
     * 
     * @param int $id
     * @param string $tplSubject
     * @param string $tplBody
     * @return boolean
     */
    function updateTemplateText($id, $tplSubject, $tplBody){
        
        $sql = " update server_email_templates "
                . " set template_subject = '" . 
                    $this->link->real_escape_string($tplSubject) . "' "
                . ", template_body = '" . 
                    $this->link->real_escape_string($tplBody) . "' "
                . " where id = " . (int)($id);
        return $this->link->query($sql);
    }
    
    /**
     * 
     * @param int $id
     * @param string $notifColName
     * @param int $val
     * @return boolean
     * @throws Exception
     */
    function updateTemplateNotif($id, $notifColName, $val){
        $notifyCols = array('notify_admin', 'notify_shooter');
        if (!in_array($notifColName, $notifyCols)) {
            throw new \Exception('$notifColName must be one of ' . implode(',', $notifyCols) );
        }
        $sql = " update server_email_templates set `$notifColName` = " . 
                ($val ? 1 : 0) . " where id = " . (int)($id);
        return $this->link->query($sql);
    }
    
    /**
     * 
     * @param string $accHashKey
     * @param string $accNo
     * @param array $data
     * @return boolean
     * @throws Exception
     */
    function addNewTemplate($accHashKey, $accNo, $data){        
        
        $required = array('se_templatetype_fk', 'template_subject', 'template_body');
        
        if (ArrayHelper::arrayContainsAllTheseKeys($data, $required)) {
            throw new Exception(' $data array must have values for ' + implode(', ', $required) + ' keys');
        }
        
        $notify_admin = isset($data['notify_admin']) ? ($data['notify_admin'] ? 1 : 0) : 1;
        $notify_shooter = isset($data['notify_shooter']) ? ($data['notify_shooter'] ? 1 : 0) : 1;
        $vals = array(
            'notify_admin' => $notify_admin,
            'notify_shooter' => $notify_shooter,
            'template_subject' => "'" . $this->link->real_escape_string($data['template_subject']) . "'",
            'template_body' => "'" . $this->link->real_escape_string($data['template_body']) . "'",
            'se_templatetype_fk' => (int)($data['se_templatetype_fk'])
        );
        
        $sql = " insert into server_email_templates (" . implode(',', array_keys($vals)) . ") "
                . " values (" . implode(',', array_values($vals)) . ") ";
        return $this->link->query($sql);
    }
    
    /**
     * Returns an html template that is prepended to email
     * @param int $templateTypeId
     * @return string
     */
    public function getPrependTemplate($templateTypeId){
        $str = '';
        $dir = dirname(__FILE__) . '/mail-template-parts';
        if ($templateTypeId == server_email_template_types::MEMBERSHIP_RENEWAL_ID) {
            $str = file_get_contents($dir . '/mem_renewal_pre.html');
        } else if ($templateTypeId == server_email_template_types::SCHEDULER_EMAIL_ID) {
            $str = file_get_contents($dir . '/scheduler_pre.html');
        }
        return $str;
    }
    
    /**
     * Returns an html template that is appended to email
     * @param int $templateTypeId
     * @return string
     */
    private function getAppendTemplate($templateTypeId) {
        return ''; // stuff below appends logo and it is supposed to be
         // customized per range owner.. skipping for now
        $str = '';
        $dir = dirname(__FILE__) . '/mail-template-parts';
        if ($templateTypeId == server_email_template_types::MEMBERSHIP_RENEWAL_ID) {
            $str = file_get_contents($dir . '/mem_renewal_post.html');
        } else if ($templateTypeId == server_email_template_types::SCHEDULER_EMAIL_ID) {
            $str = file_get_contents($dir . '/scheduler_post.html');
        }
        return $str;
    }
    
}

