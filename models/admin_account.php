<?php
namespace Rc\Models {
    use Rc\Services\DbHelper;
    use Rc\Services\StringHelper;
        
    class admin_account {
        /**
         *
         * @var \mysqli
         */
        protected $link;     
        /**
         *
         * @var \Rc\Models\range_owner_id 
         */
        protected $admin_owner_id;

        protected $customer_number;
        // also known as range number

        protected $subject;
        protected $content;
        protected $timestamp;
        


        //private constructor will prevent this class from being initialized 
        // like new range_owner(). We want it to be initialized by using
        // range_owner::initWithAccount(...)
        private function __construct() {               
        }

        
        /**
         * returns account number
         * @alias getAccountNumber
         * @return string
         */
       
        public function getcustomer_number(){
            return $this->customer_number;
        }

        /**
         * Returns the address parts as an array so you can join them with \n
         *  or <br /> depending on your environment
         * @return array
         */
        
        
        public function getMemberNumberStartCount(){
            return $this->memberNumberStartCount;
        }

        /**
         * Returns a new range_owner object populated for this account
         * @param mysqli $link
         * @param string $accHashKey
         * @param string|int $accNo
         * @return \range_owner
         */
		 
        public static function initWithAccount($link, $customer_number) {
            $ro = new admin_account();
            $ro->link  = $link;       
            $ro->customer_number = $customer_number;
            return $ro;
		}
		
		public function get_account_data($customer_number) 
		{
		   $sel_account_sql = "SELECT * FROM controller_config";
		   return DbHelper::dbFetchAllAssoc($this->link, $sel_account_sql);
		}
		public function get_trial_data() 
		{
		   $sel_account_sql = "SELECT accountnumber FROM trial_version";
		   return DbHelper::dbFetchAllAssoc($this->link, $sel_account_sql);
		}
		public function getAccountDetail($accountnumber) 
		{
			$sel_account_sql = "SELECT * FROM controller_config ";
			$sel_account_sql .= "JOIN signup_contact_info ON controller_config.accountnumber = signup_contact_info.accountnumber ";
			$sel_account_sql .= "WHERE controller_config.accountnumber = '".$accountnumber."' ";
		    return DbHelper::dbFetchRowAssoc($this->link, $sel_account_sql);
		}
		public function editAccount($accountnumber, $company_name, $contact_name, $email_address, $notes) 
		{
		  $sel_account_sql = "UPDATE controller_config SET company_name = '".$company_name."', registered_email = '".$email_address."', notes = '".$notes."' WHERE accountnumber = '".$accountnumber."' ";
		  
		   return $this->link->query($sel_account_sql);
		}
		public function editAccounts($accountnumber, $address, $city, $state, $zip_code, $phone_number) 
		{
		  $sel_account_sql = "UPDATE signup_contact_info SET street_address = '".$address."', city = '".$city."', state = '".$state."', zipcode = '".$zip_code."', phone_number = '".$phone_number."' WHERE accountnumber = '".$accountnumber."' ";
		  
		   return $this->link->query($sel_account_sql);
		}
		public function signup_contact_info_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `signup_contact_info` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function controller_config_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `controller_config` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function hours_of_operation_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `hours_of_operation` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function admin_login_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `admin_login` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function editable_memberships_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `editable_memberships` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function control_member_number_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `control_member_number` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function universal_id_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `universal_id` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function show_hide_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `show_hide` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function pos_register_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `pos_register` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function pos_transaction_number_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `pos_transaction_number` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function id_card_creator_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `id_card_creator` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function trial_version_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `trial_version` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function emergency_contact_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `emergency_contact` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function benefits_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `benefits` WHERE account_number = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function bug_report_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `bug_report` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function driver_license_info_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `driver_license_info` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function employees_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `employees` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function member_login_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `member_login` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function memberships_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `memberships` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function new_newsletter_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `new_newsletter` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function new_scheduler_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `new_scheduler` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function payment_for_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `payment_for` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function payment_types_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `payment_types` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function pos_new_inventory_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `pos_new_inventory` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function pos_new_inventory_types_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `pos_new_inventory_types` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function pos_new_sale_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `pos_new_sale` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function range_lanes_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `range_lanes` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function range_time_tracker_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `range_time_tracker` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function server_email_templates_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `server_email_templates` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function special_events_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `special_events` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function certs_and_safety_delete($accountnumber) 
		{
		   $sel_account_sql = "DELETE FROM `certs_and_safety` WHERE accountnumber = '".$accountnumber."'";
		   return $this->link->query($sel_account_sql);
		}
		public function getNewslettersjson($filter){  
		    
			$result2 = array();
			$sql_trial_data = "SELECT accountnumber FROM trial_version";
		    $sql_trials = DbHelper::dbFetchAllAssoc($this->link, $sql_trial_data);
		   
		    foreach($sql_trials as $sql_trial)
			{
				$result2[] = $sql_trial['accountnumber'];
			}
			     
			$result = array();
			$membershiptype = "SELECT * FROM controller_config WHERE 1=1";
			//where condition
			
			if(!empty($filter['company_name']))
				$membershiptype .= " and company_name LIKE '".$filter['company_name']."%' ";
			if(!empty($filter['account_status']))
				$membershiptype .= " and account_status ='".$filter['account_status']."' ";	
			$membershiptype.=" order by `accountnumber` desc ";
	
			//$membershiptype .=" orderby `timestamp` desc";						
			$memtype_list = $this->link->query($membershiptype);
			//return DbHelper::dbFetchRowAssoc($this->link, $sql);
			$i = 0;
			while($row = $memtype_list->fetch_assoc()){
				
				$result[$i]['company_name']         = $row['company_name'];
				$result[$i]['accountnumber']         = $row['accountnumber'];
				$result[$i]['account_status']         = $row['account_status'];
				$result[$i]['accountnumber_array']         = $result2;
				$i++;								
			}		
			
			
			header('Content-type: application/json');
			echo json_encode($result);
			die;
        }
		public function suspendAccount($accountnumber) 
		{
		   $sel_account_sql = "UPDATE controller_config  SET account_status='Suspended' WHERE accountnumber= '".$accountnumber."' ";
		   return $this->link->query($sel_account_sql);
		}
		public function activeAccount($accountnumber) 
		{
		   $sel_account_sql = "UPDATE controller_config  SET account_status='Active' WHERE accountnumber= '".$accountnumber."' ";
		   return $this->link->query($sel_account_sql);
		}
		public function trialAccount($accountnumber) 
		{
		   $sel_account_sql = "UPDATE controller_config  SET account_status='Trial' WHERE accountnumber= '".$accountnumber."' ";
		   return $this->link->query($sel_account_sql);
		}
    }
}