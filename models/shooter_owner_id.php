<?php
namespace Rc\Models;

class shooter_owner_id {
    protected $accHashKey;
    protected $accNo;
	
    /**
     * 
     * @param type $accHashKey
     * @param type $accNo
     */
    public function __construct($accHashKey, $accNo) {
        $this->accHashKey = $accHashKey;
        $this->accNo = $accNo;
    }
    
    public function getAccountHashKey(){
        return $this->accHashKey;
    }
    
    public function getAccountNo(){
        return $this->accNo;
       
    } 
	
	public function member_number(){
        return $this->member_number;
       
    }    
    
    /**
     * returns the sql condition for range account like 
     *  acctnumberhashed = '...' and  accountnumber = '..'
     * @param mysqli $link
     * @param type $hashKeyColumnName default acctnumberhashed
     * @param type $accNoColumnName default accountnumber
     * @param type $tblPrefix default empty string
     * @return string 
     */
    public function getSqlCond(
            \mysqli $link, 
            $hashKeyColumnName = 'acctnumberhashed', 
            $accNoColumnName = 'accountnumber',
            $tblPrefix = ''){
        
        $tblPrefix = !empty($tblPrefix) ? $tblPrefix . '.' : '';
        return " $tblPrefix`$hashKeyColumnName` = '" . 
                $link->real_escape_string($this->accHashKey) . "' "
                . " and $tblPrefix`$accNoColumnName` = '" . 
                $link->real_escape_string($this->accNo) . "' ";
    }
    
    public function getIdArray(
            $hashKeyColumnName = 'acctnumberhashed', 
            $accNoColumnName = 'accountnumber'){
            return array(
                $hashKeyColumnName => $this->accHashKey,
                $accNoColumnName => $this->accNo
            );
        }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

