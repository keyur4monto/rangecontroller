<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Range Controller - Payment</title>
<?php
include 'includesOLD/meta.php';
?>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<style>
#welcome_table3 {
	display: none;
}
#headercol2 {
	display: none;
}
.table1 {
	color:white;
}
.table2 {
	color:white;
}
.table3 {
	color:white;
}
</style>
</head>
<body>
<div align="center"><img src="images/background.jpg" class="bg" alt=""/></div>
<?php
include 'includesOLD/header.php';
?>
<table id="content_table" border="0">
    <tr>
  
    <td id="contentcol">
	 <table id="billing_info_table" width="700">
      <tr>
        <td width="100%" class="table1" style="color: white;">
			<div id="checkstatus">
				Loading...Please Wait...We are waiting for your payment Confirmation.
			</div>
		</td>
      </tr>
    </table>
	</td>
    </td>
	
    </tr>
  
</table>
<?php
include 'includesOLD/footer.php';
?>
</div>
<script src="/js/jquery.js"></script>
<script>
$(document).ready(function(){
	///check ajax
	setInterval(function(){ 
	$.ajax({
		url:'/checkpaypalpayment.php',
		data:{tx:'<?php echo $_GET['tx']; ?>'},
		type:'post',
		success:function(response){
			console.log(response);
			if(response.success)
				window.location.href=response.redirectUrl;
		}
	});
	}, 5000);
});
</script>
</body>
</html>
<style>
.recaptchatable * {
	border: 0 none;
	bottom: auto;
	color: red;
	font-family: helvetica, sans-serif;
	font-size: 8pt;
	left: auto;
	margin: 0;
	padding: 0;
	position: static;
	right: auto;
	top: auto;
}
</style>
