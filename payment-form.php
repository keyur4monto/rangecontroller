<?php
include 'includes/session.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Range Controller - Payment Form</title><?php
        include 'includes/headerTags.php';
        ?>
    <style>
        #headerRight {
            display: none;
        }

        .recaptchatable * {
        	border: 0 none;
        	bottom: auto;
        	color: red;
        	font-family: helvetica, sans-serif;
        	font-size: 8pt;
        	left: auto;
        	margin: 0;
        	padding: 0;
        	position: static;
        	right: auto;
        	top: auto;
        }

    </style>
</head>
<body>
    <?php
        include 'includes/header.php';
        ?>
    <div class="padTop" id="paymentForm">
        <form id="frmPaymentForm" name="frmPaymentForm" action="/common/process_payment_for_testing.php" method="POST">
            
            <table>
                <tr>
                    <th class="greyRow" colspan="2">Enter Billing Information</th>
                </tr>
                <tr>
                    <td class="right">First Name:</td>
                    <td><input type="text" name="txtFirstName" id="txtFirstName" value=""></td>
                </tr>
                <tr>
                    <td class="right">Last Name:</td>
                    <td><input type="text" name="txtLastName" id="txtLastName" value=""></td>
                </tr>
                <tr>
                    <td class="right">Street Address:</td>
                    <td><input type="text" name="txtStreetAddress" id="txtStreetAddress" value=""></td>
                </tr>
                <tr>
                    <td class="right">City:</td>
                    <td><input type="text" name="txtCity" id="txtCity" value=""></td>
                </tr>
                <tr>
                    <td class="right">State:</td>
                    <td>
                        <select name="selState" id="selState" size="1" onchange="setTheState(this.value);" onblur="selStateWithKey(this.value);">
                            <option></option>
                            <option value="AK">AK</option>
                            <option value="AL">AL</option>
                            <option value="AR">AR</option>
                            <option value="AZ">AZ</option>
                            <option value="CA">CA</option>
                            <option value="CO">CO</option>
                            <option value="CT">CT</option>
                            <option value="DC">DC</option>
                            <option value="DE">DE</option>
                            <option value="FL">FL</option>
                            <option value="GA">GA</option>
                            <option value="HI">HI</option>
                            <option value="IA">IA</option>
                            <option value="ID">ID</option>
                            <option value="IL">IL</option>
                            <option value="IN">IN</option>
                            <option value="KS">KS</option>
                            <option value="KY">KY</option>
                            <option value="LA">LA</option>
                            <option value="MA">MA</option>
                            <option value="MD">MD</option>
                            <option value="ME">ME</option>
                            <option value="MI">MI</option>
                            <option value="MN">MN</option>
                            <option value="MO">MO</option>
                            <option value="MS">MS</option>
                            <option value="MT">MT</option>
                            <option value="NC">NC</option>
                            <option value="ND">ND</option>
                            <option value="NE">NE</option>
                            <option value="NH">NH</option>
                            <option value="NJ">NJ</option>
                            <option value="NM">NM</option>
                            <option value="NV">NV</option>
                            <option value="NY">NY</option>
                            <option value="OH">OH</option>
                            <option value="OK">OK</option>
                            <option value="OR">OR</option>
                            <option value="PA">PA</option>
                            <option value="RI">RI</option>
                            <option value="SC">SC</option>
                            <option value="SD">SD</option>
                            <option value="TN">TN</option>
                            <option value="TX">TX</option>
                            <option value="UT">UT</option>
                            <option value="VA">VA</option>
                            <option value="VT">VT</option>
                            <option value="WA">WA</option>
                            <option value="WI">WI</option>
                            <option value="WV">WV</option>
                            <option value="WY">WY</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="right">Zip Code:</td>
                    <td><input type="text" name="txtZipcode" id="txtZipcode" value=""></td>
                </tr>
                <tr>
                    <td class="center smallText" colspan="2">All credit card
                    information is encrypted and complies with state and federal
                    laws for compliance. Once your information is input, press the
                    ”Submit” button at the bottom of the page only once.</td>
                </tr>
                <tr>
                    <th class="greyRow" colspan="2">Enter Credit Card
                    Information</th>
                </tr>
                <tr>
                    <td class="right">Card Number:</td>
                    <td><input type="text" name="txtCardNumber" id="txtCardNumber" value=""></td>
                </tr>
                <tr>
                    <td class="right">Expiration Month:</td>
                    <td>
                        <select id="selMonthExpired" name="selMonthExpired" onchange="setMonthExpired(this.value);">
                            <option></option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                          </select>
                    </td>
                </tr>
                <tr>
                    <td class="right">Expiration Year:</td>
                    <td>
                        <select id="selYearExpired" name="selYearExpired" onchange="setYearExpired(this.value);" style="width: 85px;">
                            <option></option>
                            <option value="2013">2013</option>
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2016">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                          </select>
                    </td>
                </tr>
                <tr>
                    <td class="right">3 Digit Code(CVV):</td>
                    <td><input type="text" name="txtCVV2" id="txtCVV2" value="" style="width: 55px;"></td>
                </tr>
                <tr>
                    <td class="right">Payment Sum:</td>
                    <td>$339.00</td>
                </tr>
                <tr>
                    <td class="center gold smallText" colspan="2">($49 monthly
                    charge + $290 yearly maintenance fee)</td>
                </tr>
                <tr>
                    <td class="right">Captcha:</td>
                    <td>
                        <!--script src='https://www.google.com/recaptcha/api.js'></script-->
                        
                        <?php
                        /*$switch_value = rand(1,5);
                        switch($switch_value){            
                            case 1: echo '&nbsp;5 + 5 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="10" />';
                            break;
                            case 2: echo '&nbsp;10 + 5 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="15" />';
                            break;
                            case 3: echo '&nbsp;1 + 1 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="2" />';
                            break;
                            case 4: echo '&nbsp;2 + 1 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="3" />';
                            break;
                            case 5: echo '&nbsp;3 + 3 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="6" />';           
                            break;
                        }*/
                                        
                        require_once('recaptchalib.php');                
                        // Get a key from https://www.google.com/recaptcha/admin/create
                        $publickey = "6LdL_hUTAAAAAHcjXxUMt58rAhUguAkQV2Tz7sS3";
                        $privatekey = "6LdL_hUTAAAAAHBkts1AXKwdw5nu9T9ALvj_LpT5";                
                        # the response from reCAPTCHA
                        $resp = null;
                        # the error code from reCAPTCHA, if any
                        $error = null;                
                        # was there a reCAPTCHA response?                
                        /*$resp = recaptcha_check_answer ($privatekey,$_SERVER["REMOTE_ADDR"],
                                                        $_POST["recaptcha_challenge_field"],
                                                        $_POST["recaptcha_response_field"]);*/
                        /*$che = $resp->is_valid;
                        if ($resp->is_valid) {
                                echo "You got it!";
                        } else {
                                # set the error code so that we can display it
                                $error = $resp->error;
                        
                        }*/
                        echo recaptcha_get_html($publickey,'',$error);
                        ?>
                        <!--div class="g-recaptcha" data-sitekey="6LdL_hUTAAAAAHcjXxUMt58rAhUguAkQV2Tz7sS3"></div-->
                    </td>
                </tr>
            </table>        
            <div class="center">
                <br>
                <input type="hidden" id="RecurMethodID" name="RecurMethodID" value="1"/>
                <input type="hidden" id="h_state" name="h_state"/>
                <input type="hidden" id="h_MonthExpired" name="h_MonthExpired" />
                <input type="hidden" id="h_YearExpired" name="h_YearExpired" />
                <input type="hidden" id="h_payment_sum" name="h_payment_sum" value="339.00" />
                <input type="hidden" id="h_submit" name="h_submit" />
                <input type="hidden" name="ePNAccount" value="1011371" />
                <!-- # for testing 080880                        original # 1011371-->
                <input type="hidden" name="ID" />
                <!-- RE:CVV2Type value for required cvv code is 1 sent to ePN -->
                <input type="hidden" name="CVV2Type" value="1" />
                <input type="hidden" name="RestrictKey" value="O8y9gcnP4Lh9Vgy"/>
                <!--# for testing yFqqXJh9Pqnugfr                   original# O8y9gcnP4Lh9Vgy-->
                <input type="hidden" name="ReturnApprovedURL" value="" />
                <input type="hidden" name="ReturnDeclinedURL" value="" />
                <input type="hidden" name="BackgroundColor" />
                <input type="hidden" name="TextColor" />
                <input type="button" value="Submit" onclick="return jsValidateForm(this)">
            </div>
        </form>
    </div>
    <?php
    include 'includes/footer.php';
    ?>
<script type="text/javascript">     
function jsValidateForm(t) {
      
    var validationStr = "";
    	var textArray = [ // id|error message|type	// 0 | 1           | 2
		"txtFirstName| - First Name\n|text",
        "txtLastName| - Last Name\n|text",
        "txtStreetAddress| - Street Address\n|text",
        "txtCity| - City\n|text",
        "h_state| - State\n|text",
        "txtZipcode| - Zipcode\n|text",
        "txtCardNumber| - Card Number\n|text",
        "h_MonthExpired| - Month Expired\n|text",
        "h_YearExpired| - Year Expired\n|text",
        "txtCVV2| - CVV2\n|text",
        "recaptcha_response_field| - Captcha\n|text"     
        ];
    // Validate fields.    
	for (var n = 0; n < textArray.length; n++) {
		str = textArray[n];
		var res = str.split("|");		
        e = document.getElementById(res[0]);  
        //console.log(str);
        if (e) {
			e.style.backgroundColor = "#ffffff";			
            
            if (e.value == "") {                
                validationStr += res[1];                                  
				e.style.backgroundColor = "#fdeae8";
			}
            /*if (e.value != "") {  
                if (res[0] == "email") {
			     // Check email first. Shortest possible e@co.cc 
				 
				var varTestExp=/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
                var varEmail = e.value; //[a-zA-Z0-9\-\.]
				if (varEmail.search(varTestExp) == -1) {
					validationStr += " - Email Address (valid format)\n";
					e.style.backgroundColor = "#fdeae8";
				  }
               }
            } */
        }  
    }
    
    if (validationStr) {
		validationStr = "Please check these fields: \n\n" + validationStr;
		if (1) alert(validationStr);    		
		return false;
	}
    document.getElementById('frmPaymentForm').submit();
    return true;
}


function selStateWithKey(state){
    document.getElementById("h_state").value = state;
    //alert("set with key: "+document.getElementById("h_state").value);
}//end function

function setTheState(state){
    document.getElementById("h_state").value = state;
//alert("set with mouse: "+document.getElementById("h_state").value);
}//end function

function setMonthExpired(month){
    document.getElementById("h_MonthExpired").value = month;
}//end function

function setYearExpired(year){
    document.getElementById("h_YearExpired").value = year;
}//end function

/*document.oncontextmenu=RightMouseDown;
function RightMouseDown() { return false; }*/

history.pushState({ page: 1 }, "title 1", "#nbb");

window.onhashchange = function (event) {
    window.location.hash = "nbb";
};
</script>
</body>
</html>