<?php
include 'includes/session.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Range Controller - Checkout</title><?php
    include 'includes/headerTags.php';
    ?>
    <style>
    #headerRight {
    display: none;
    }
    </style>
    <script src=
    "/xapp/tools/jquery-1.11.0.min.js">
    </script>
    <script>
    $(document).ready(function(){
        $("#pay_with_paypal").click(function(){
             $("#submit_btn").click(); 
        });
    });
    </script>
</head>
<body>
    <?php
    include 'includes/header.php';
    ?>
    <div id="checkout">
        <table id="checkoutTable">
            <tr>
                <th colspan="2" id="greyTableRow">Checkout</th>
            </tr>
            <tr>
                <td style="width: 75%;">Recurring Monthly charge</td>
                <td class="right">$49.00</td>
            </tr>
            <tr>
                <td>Yearly Maintenance Fee</td>
                <td class="right">$290.00</td>
            </tr>
            <tr>
                <td class="right" style="width: 75%;">Total:</td>
                <td class="right">$339.00</td>
            </tr>
            <tr>
                <td class="right padding" colspan="2"><br>
                <button id="creditCardButton" onclick=
                "location.href='payment_form.php';">Pay with Credit
                Card</button><br>
                <img id="pay_with_paypal" src="images/paypal.png" style=
                "width: 143px; cursor: pointer;"></td>
            </tr>
            <tr style="display:none;">
                <td class="table1" style="color: white;" width="100%">
                    <form action="https://www.paypal.com/cgi-bin/webscr"
                    method="post" target="_top">
                        <input name="cmd" type="hidden" value="_s-xclick">
                        <input name="hosted_button_id" type="hidden" value=
                        "9YFWF72CP4AGA"> <input alt=
                        "PayPal - The safer, easier way to pay online!" id=
                        "submit_btn" name="submit" src=
                        "https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif"
                        type="image"> <img alt="" border="0" height="1" src=
                        "https://www.paypalobjects.com/en_US/i/scr/pixel.gif"
                        width="1">
                    </form>
                </td>
            </tr>
        </table>
    </div><?php
    include 'includes/footer.php';
    ?>
</body>
</html>