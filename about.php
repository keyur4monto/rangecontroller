<?php
include 'includes/session.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Range Controller - About</title><?php
    include 'includes/headerTags.php';
    ?>
</head>
<body>
    <?php
    include 'includes/header.php';
    ?><?php
    include 'includes/navBar.php';
    ?>
    <div id="about">
        <h1>About</h1>
        <p>RangeController.com was a venture introduced in Late 2012 by Dave's
        Pawn Shop in Crossville, TN. We started construction on our new 10 lane
        indoor shooting range in late 2011 slated to be completed by late 2012.
        As the venture started, we began searching the country for the best
        shooting range software we could find. To our dismay, and after weeks
        of searching, we found that there were NO software offerings for
        running a traditional shooting range. This was a devastating find. The
        one thing we did not want to do was keep a drawer full of paperwork and
        try to keep up with a schedule that was pretty much the heart of a
        successful shooting range! After speaking to many heavy hitters in the
        shooting industry, we found that we would have to build what we wanted
        ourselves. I stated that even bowling alleys have software to run their
        business, why didn't shooting ranges' How were we going to keep up with
        whether a client had watched our safety video or completed our legal
        paperwork' How would we know the shooter's name on lane 4 and schedule
        him/her with a time that didn't bother another shooter' With no other
        options, we began working on our own program to fill this need,
        RangeController.</p>
        <p>We began by contacting range owners all over the Eastern United
        States and interviewing them to see what most they would want in a
        complete program to run their range. With this information in hand, we
        began constructing what would be a huge endeavor both in time and money
        invested. We copyrighted the scripts and original art from the
        RangeController site and trade marked the logo for RangeController. We
        were on our way!</p>
        <p>A team of programmers began in early 2012 building the world's most
        comprehensive and dedicated shooting range software to date. With
        thousands of lines of custom code, this was going to take a while.
        After Seven months of hard work, we finally had a working beta
        prototype. Our range opened in May of 2013 and we immediately began
        using RangeController to run the range from day to day. It was super
        easy to learn and kept up with all of our shooters. This kept us on top
        of legal issues, the cash flow and scheduling so we could focus on what
        we were here for, having fun in our new range!</p>
        <p>RangeController version 1 was introduced in July 2013 after 3 months
        of testing at a real range. After we seen what the program could do for
        our industry, we decided to offer it to everyone! Once you purchase
        your RangeController ticket, you simply enter some specifics about your
        range and you are assigned a dedicated URL (I.E.
        www.rangecontroller.com/YOURRANGE) and are ready to begin using it
        right away! We have One low monthly rate and you pick the options you
        want to use at no extra charge! As we introduce new features, you will
        get those for free as well. We offer technical support to help with any
        questions you may have and we know that RangeController will help you
        run your range to the best of your ability. Imagine if you could, by
        the click of a mouse, see how much your range had grossed this month,
        examined how many shooters utilized your range this week and your
        clients could log in from home and book time without having to bother
        your RSO! It's all possible now, with RangeController!</p>
        <p>We are a small company located in East Tennessee and staying on top
        of our budget is important to us. We still believe in treating folks
        the way you would want to be treated and getting the most for your
        dollar in todays weak economy and competitive marketplace.
        RangeController will help you run your range in a efficient and
        effective manner from day to day, we guarantee it!</p>
        <p>Your input is important to us! If you see something that would be a
        good addition to RangeController, please let us know! Real range owners
        built RangeController and as good ideas come in, real range owners will
        continue to drive and change RangeController for the better making it
        the ONLY choice for range owners in this market. If you have any
        questions, please EMAIL us here and we'll get back to you as soon as we
        can. We're probably shooting at the range!</p>
    </div><?php
    include 'includes/content.php';
    ?><?php
    include 'includes/footer.php';
    ?>
</body>
</html>