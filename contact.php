<?php
include 'includes/session.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Range Controller - Contact Us</title><?php
    include 'includes/headerTags.php';
    ?>
</head>
<body>
    <?php
    include 'includes/header.php';
    ?><?php
    include 'includes/navBar.php';
    
    if(isset($_POST['submit'])){
        $name='';$phone='';$email='';$message='';
        $name = trim($_POST['name']);
        $phone = trim($_POST['phone']);
        $email = trim($_POST['email']);
        $message = $_POST['message'];                
        $body = '<!DOCTYPE HTML><html><head></head><body><div>';
        $body .= 'Hello Admin,<br /><br />';            
        $body .= 'We have got this new message from contact us page please check<br />';
        $body .= '<div>';
        $body .= '        <span style="width: 100px; font-weight: bold;">Name : </span>';
        $body .= '        <span>'.$name.'</span>';
        $body .= '</div>';
        $body .= '<div>';
        $body .= '        <span style="width: 100px; font-weight: bold;">Phone : </span>';
        $body .= '        <span>'.$phone.'</span>';
        $body .= '</div>';
        $body .= '<div>';
        $body .= '  <span style="width: 100px; font-weight: bold;">Email : </span>';
        $body .= '  <span>'.$email.'</span>';
        $body .= '</div>';
        $body .= '<div>';
        $body .= '  <span style="width: 100px; font-weight: bold;">Message : </span><br />';
        $body .= '  <span style="word-wrap: break-word;">'.$message.'</span>';
        $body .= '</div>';
        $body .= '</div></body></html>';        
        $body = htmlentities($body);                               
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset: utf8"." \r\n";			
        $headers .= "From: Range Controlle <admin@rangecontrolle.com>"."\r\n";			
        $subject = 'Contact Us email alert!';
        
        if(!mail("sales@rangecontroller.com",$subject,$body,$headers)){									   
			echo "<div class='mail_error'>For some reason, the message could not be sent. Pleasy try again later.</div>";
		}        
        header('Location: /template.php?msg=1');                            
    }
    
    ?>
    <style>
    .mail_error{
        background-color: #ffffff;
        color: red;
        margin: 0 auto;
        overflow: hidden;
        padding-bottom: 10px;
        padding-top: 10px;
        width: 950px;
        text-align: center;
    }
    </style>
    <div id="contact">
        <div id="contactLeft">
            <h1>Contact Us</h1>
            <p>
                <a href="mailto:sales@rangecontroller.com?subject=RangeController%20Inquiry">sales@rangecontroller.com</a>
            </p>
            <p style="text-align: justify; margin-right: 40px;">* If you would
            like a call back from one of our representatives, please fill out
            the form to the right. Be sure to include your phone number in the
            message section. Note: we know your time is valuable so we limit
            call backs to no more than 15 minutes. Thank you!</p>
        </div>
        <div id="contactRight">
            <form accept-charset='UTF-8' action='' id='contactus' method='post' name="contactus">
                <fieldset>
                    <legend>Send us a message</legend>
                    <input type='hidden' name='submitted' id='submitted' value='1'> 
                    <input type='hidden' name='id2a18055813d11123aa80' value='1186320c6b9569ec35140c5fe3aa5828'> 
                    <input type='text' name='sp3023dcaa4c1378cc7e888c03b19a291b' class='spmhidip' >
                    <div class='short_explanation'>
                        * required fields
                    </div>
                    <div>
                        <span class='error'></span>
                    </div>
                    <div class='container'>
                        <label for='name'>Your Full Name*:</label><br>
                        <input type='text' name='name' id='name' maxlength="50" value=''><br>
                        <span class='error' id='contactus_name_errorloc'></span>
                    </div>
                    <div class='container'>
                        <label for='name'>Phone Number*:</label><br>
                        <input type='text' name='phone' id='phone' maxlength="15" value=''><br>
                        <span class='error' id='contactus_phone_errorloc'></span>
                    </div>
                    <div class='container'>
                        <label for='email'>Email Address*:</label><br>
                        <input type='text' name='email' id='email' maxlength="50" value=''><br>
                        <span class='error' id='contactus_email_errorloc'></span>
                    </div>
                    <div class='container'>
                        <label for='message'>Message:</label><br>
                        <span class='error' id='contactus_message_errorloc'></span> 
                        <textarea cols="50" id='message' name='message' rows="10"></textarea>
                    </div>
                    <div class='container'>
                        <input name='submit' type='submit' value='Submit' onclick="return jsValidateForm(this)">
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <script type="text/javascript">        
    var formIsSubmitted = false;     
    function jsValidateForm(t) {
      
        var validationStr = "";
        	var textArray = [ // id|error message|type	// 0 | 1           | 2
    		"name| - Name\n|text",
            "email| - Email\n|text",
            "phone| - Phone\n|text"
            ];
        // Validate fields.    
    	for (var n = 0; n < textArray.length; n++) {
    		str = textArray[n];
    		var res = str.split("|");		
            e = document.getElementById(res[0]);  
            
            if (e) {
    			e.style.backgroundColor = "#ffffff";			
                
                if (e.value == "") {                
                    validationStr += res[1];                                  
    				e.style.backgroundColor = "#fdeae8";
    			}
                if (e.value != "") {  
                    if (res[0] == "email") {
    			     // Check email first. Shortest possible e@co.cc 
    				 
    				var varTestExp=/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            
                    var varEmail = e.value; //[a-zA-Z0-9\-\.]
    				if (varEmail.search(varTestExp) == -1) {
    					validationStr += " - Email Address (valid format)\n";
    					e.style.backgroundColor = "#fdeae8";
    				  }
                   }
                } 
            }  
        }
        
        if (validationStr) {
    		validationStr = "Please check these fields: \n\n" + validationStr;
    		if (1) alert(validationStr);    		
    		return false;
    	}
        return true;
    }
    </script>
    <?php
    include 'includes/content.php';    
    include 'includes/footer.php';
    ?>
</body>
</html>