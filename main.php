<?php
include 'includes/session.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Range Controller - Main</title><?php
    include 'includes/headerTags.php';
    ?>
</head>
<body>
    <?php
    include 'includes/header.php';
    ?><?php
    include 'includes/navBar.php';
    ?>
    <div id="main">
        <p>FINALLY! A software platform for the indoor/outdoor shooting range
        owner! Over a year ago, we were in the middle of an indoor shooting
        range build and we began calling around leaders in our industry to get
        a jump start on a complete software system for the range when it opened
        for business. After weeks of investigation and many calls, we found out
        there were no programs we could use to run our range. We were
        dumbfounded!</p>
        <p>NO REAL ENTREPRENEUR can run a business without the correct tools
        for the job. So, myself and the range owners decided to take this
        challenge on, head on! With input from other range owners in Tennessee
        and North Georgia, we set out to build a software platform to keep us
        in control of our shooting range and keep the range from controlling
        us. Our name was a no brainer, RangeController.</p>
        <p>Welcome to the world's first ever software that keeps you, the range
        owner, in control of your business! Our software helps the range owner
        stay on top of:</p>
        <ul>
            <li>The sales data of classes, sales and all day to day
            income.</li>
            <li>Who's shooting and where.</li>
            <li>Who are your most active shooters and what do they spend cash
            on?</li>
            <li>Daily, monthly and yearly range maintenance.</li>
            <li>Custom reports, rentals and internal Point Of Sale system.</li>
            <li>Automated email system to keep in touch with your clients.</li>
            <li>Client/user interface so you can reach them at home.</li>
            <li>Much more function added with each update, FREE!</li>
        </ul>
        <p>That's right, for one low monthly fee, your subscription to
        RangeController along with all updates and module additions are
        included.</p>
        <p>Your Range Controller software is super easy to use and your range
        can be up and running in less than 15 minutes! Range Controller is
        completely web based and SSL encrypted for your protection. We offer
        on-line and telephone technical support and automatically bill your
        payment method monthly so you can literally get your range set up for
        LESS THAN $50! Please look around and hopefully, we will answer any
        questions you may have. Thank you for your interest in
        RangeController!</p>
    </div><?php
    include 'includes/content.php';
    ?><?php
    include 'includes/footer.php';
    ?>
</body>
</html>