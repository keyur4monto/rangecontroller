(function($, ctx, rcobj, valida, numeral){

    ctx.getLargeLoadingIndicator =  rcobj.getLargeLoadingIndicator;
    ctx.showWaiting = rcobj.showWaiting;
    ctx.showSpinner = rcobj.showSpinner;
    ctx.clearSpinner = rcobj.clearSpinner;
    ctx.appendDivAndGetId = rcobj.appendDivAndGetId;
    ctx.showMessage = rcobj.showMessage;
    ctx.showSuccess = rcobj.showSuccess;
    ctx.showError = rcobj.showError;
    
    ctx.getFields = function(){
        return {
            'membership': ['membershipType', 'memRegistrationDate', 
                'memExpirationDate'],
            'contact': ['firstName', 'middleName', 'lastName', 
                'street', 'city', 'state', 'zipcode','ssn',
                'phone1', 'phone2', 'email', 'emergencyName', 
                'emergencyPhone'],
            'dl': ['dlState', 'dlNumber', 'dlExpiration',
                'dlHeightFeet', 'dlHeightInches', 'dlWeight',
                'dlHair', 'dlEyes', 'dlRace', 'dlGender', 
                'dlBirthDate', 'dlBirthCity', 'dlBirthState'],
            'billing': ['billingDate', 'billingPaymentType', 'billingAmount'],
            'additional': ['safetyRangeFormCompleted', 'safetyVideoCompleted',
                'certsComments'], 
            'comments':['commentsText']
        };
    };

    ctx.collectFields = function(){
        var fields = ctx.getFields();
        var results = {};
        for (var grp in fields) {
            var group = fields[grp];
            results[grp] = {};
            for (var i = 0; i < group.length; i++) {
                var k = group[i];
                results[grp][k] = $.trim($('#' + k).val());
            }
        }
        return results;
    };

    ctx.verifyContact = function(values){
        var req = ['firstName', 'lastName', 
                'street', 'city', 'state', 'zipcode',
                'phone1', 'email'];
        for (var i in req) {
            if (values[req[i]] === '') {
                ctx.showError('Please fill out all the fields marked *');
                return false;
            }
        }
        if (!valida.isEmail(values['email'])) {
            ctx.showError('Email address is invalid; please re-enter');
            return false;
        }
        return true;
    };

    ctx.verifyBilling = function(values){
        //alert(values.toSource());
        if (values.billingDate === '') {
            ctx.showError('Please select a date');
            return false;
        }
        if (values.billingPaymentType === '') {
            ctx.showError('Please select a payment type');
            return false;
        }
        if (values.billingAmount.toString().match(/[^0-9\$\,\.\s]/)) {
            ctx.showError('Please enter a valid amount');
            return false;
        }


        values.billingAmount = values.billingAmount.toString().replace(
                /[^0-9\.]/g, '');
        //alert(values.billingAmount);
        if (values.billingAmount === '') {
            ctx.showError('Please enter a valid amount');
            return false;
        };
        $('#billingAmount').val(numeral(values.billingAmount).format('0,0.00'));
        return true;
    };
    ctx.saveData = function(section, elmClicked){
        var values = ctx.collectFields();
        var membershipNo = $('#membershipNo').val();
        var isFormOk = true;
        var cmd = '';
        if (section === 'membership') {
            cmd = 'updateMembership';

        } else if (section === 'contact') {
            isFormOk = ctx.verifyContact(values.contact);
            cmd = "updateContact";

        } else if (section === 'dl') {
            cmd = 'updateDriversLicense';

        } else if (section === 'billing') {
            cmd = 'addBilling';
            isFormOk = ctx.verifyBilling(values.billing);

        } else if (section === 'additional') {
            cmd = 'updateAdditionalInfo';

        } else if (section === 'comments') {
            cmd = "updateComments";                
        } else {
            ctx.showError('Invalid form');
            return false;
        }
        if (!isFormOk){
            return;
        }
        var data = values[section];
        data.cmd = cmd;
        data.membershipNo = membershipNo;

        ctx.showSpinner(elmClicked);
        $(elmClicked).prop('disabled',1);
        $.ajax({
            url: ctx.bag.urlPrefix + 'updateMemberJson',
            type: 'POST',
            dataType: 'json',
            data: data
        }).done(function(data){

            ctx.showSuccess("Successfully saved");
            if (section === 'billing') {
                $('#billingPaymentType').val('');
                $('#billingAmount').val('');
                data.payload.recent_ = true;
                ctx.bag.billingList.unshift(data.payload);
                ctx.bag.listRactive.set('list', 
                ctx.bag.billingList);
                ctx.bag.addBillListRactive.set('list', 
                        ctx.bag.billingList);
            } else {
                ctx.bag.isFormUpdated = true;
            }
        }).fail(function(result){
            //$('#dlg').html(result.responseText);
            //$('#dlg').dialog({modal:true,minWidth:1200});
            ctx.showError('Error occured while saving the data; please try again');
        }).always(function(){
            $(elmClicked).prop('disabled',0);
            ctx.clearSpinner(elmClicked);
        });            
    };

    ctx.deleteBilling = function(memNo, id, elmClicked, ind) {
        if (!confirm('Are you sure you want to delete this billing record?')) {
            return false;
        }
        ctx.showSpinner(elmClicked);   

        var data = {
            cmd: 'deleteBilling',
            'membershipNo': memNo,
            'id': id+''
        };
        $.ajax({
            url: ctx.bag.urlPrefix + 'updateMemberJson',
            type: 'POST',
            dataType: 'json',
            data: data
        }).done(function(result){
            //ctx.bag.isFormUpdated = true;
            ctx.showSuccess("Successfully deleted"); 
            ctx.bag.billingList = result.payload;
            ctx.bag.listRactive.set('list', 
            result.payload);
            ctx.bag.addBillListRactive.set('list', 
            result.payload);

            //ctx.bag.billingList.splice(ind,1);
            //$(elmClicked).parent().parent().remove(380);
        }).fail(function(result){                
            ctx.showError('Error occured while deleting the billing; please try again');
        }).always(function(){                
            ctx.clearSpinner(elmClicked);
        });            
    };

    ctx.editBilling = function(memNo, id, elmClicked, ind, isFromOverview) {
        var billing = $.extend(true, {}, ctx.bag.billingList[ind]);
        billing.paymentTypes_ = rc.kvObjToKeyValue(ctx.bag.paymentTypes);
        var elmid = ctx.appendDivAndGetId();
        var ract = new Ractive({
            el: '#' +elmid,
            template: '#billingEdit',
            data: billing
        });
        $('#'+elmid).dialog({
            modal:true, 
            buttons: {
                Cancel: function() {
                    $( this ).dialog( "close" );
                },
                Update: function(){
                    delete billing.paymentTypes_;
                    ctx.editBillingSave(billing, function(result){
                        //alert(result.payload.toSource());
                        ctx.bag.billingList = result.payload;
                        ctx.bag.listRactive.set('list', 
                        result.payload);
                        ctx.bag.addBillListRactive.set('list', 
                        result.payload);
                        //ctx.bag.listRactive.updateModel();
                        $('#'+elmid).dialog('destroy');
                        $('#'+elmid).remove();
                    });

                }
            }
        });
        $('#'+elmid + ' #billingDate').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true});
        $('#'+elmid+' #billingPaymentType').val(billing.payment_method);
    };

    ctx.editBillingSave = function(billing, successcb){
        billing.cmd = 'updateBilling';      
        billing.membershipNo = billing.membership_number;
        var dlgid = ctx.showWaiting();
        $.ajax({
            url: ctx.bag.urlPrefix + 'updateMemberJson',
            type: 'POST',
            dataType: 'json',
            data: billing
        }).done(function(data){
            //alert(data.toSource());
            //ctx.bag.isFormUpdated = true;
            ctx.showSuccess("Successfully updated"); 
            successcb(data);
        }).fail(function(result){                
            ctx.showError('Error occured while updating the billing; please try again');
        }).always(function(){      
            $('#'+dlgid).dialog('close');
            $('#'+dlgid).remove();
            //ctx.clearSpinner(elmClicked);
        });            
    };
    
    ctx.blockUnblock = function(isBlock, elm) {
        
        var data = {};
        data.isBlock = isBlock;
        data.membershipNo = $('#membershipNo').val();
        ctx.showSpinner(elm);
        var successCb = function(){
            $('.overview #blockMemberLink, .overview #unblockMemberLink')
                        .hide(380);
            if (isBlock) {
                $('.overview #unblockMemberLink').show(380);
            } else {
                $('.overview #blockMemberLink').show(380);
            }
        };
        $.ajax({
            url: ctx.bag.urlPrefix + 'blockUnblock',
            type: 'POST',
            dataType: 'json',
            data: data
        }).done(function(data){
            if (data.error) {
                ctx.showError(data.error);
            } else {
                ctx.showSuccess("Successfully " + 
                    (isBlock ? 'blocked' : 'unblocked'), 
                {okCb: successCb}); 
                    
                
                
            }
        }).fail(function(result){                
            ctx.showError('Error occured');
        }).always(function(){      
            ctx.clearSpinner(elm);
        });         
        return false;
    };
    
    ctx.showPrintBtn = function(){
         $('#breadcrumbsRight').html($('#overviewPrint').html());
    };
    ctx.tabClicked = function(elm){
        var href = $(elm).attr('href');
        //alert(href);
        $('#breadcrumbsRight').html('');
        
        if (href=== '#overview') {
           ctx.showPrintBtn();
        }
        if (href=== '#overview' && ctx.bag.isFormUpdated) {
            
            $.ajax({
                url: ctx.bag.urlPrefix + 'updateMemberJson',
                type: 'POST',
                dataType: 'html',
                data: {cmd:'getOverview', 'membershipNo':$('#membershipNo').val()}
            }).done(function(data){
                $('#overviewWrap').replaceWith(data);
            }).fail(function(result){
            }).always(function(){ 
            });            
        } else if (href === '#printid') {

            var elmid = '#printIdPdf';
            $(elmid).html(ctx.getLargeLoadingIndicator("Please wait..."));
            var url = "/xapp/tools/idcreator/createpdffile.php?cn="+
                    +$('#membershipNo').val() + "&b=0&rand="+Math.random();
            
            var htmlObject = '<object width="600" height="320" type="application/pdf" '+
                'data="'+url+'" >'+
                '<param name="controller" value="false" >'+
                '<p style="color: red;">PDF cannot be displayed. '+ 
                'Please use Chrome or Safari for best results.</p>'+
                '</object>';

		    var html = '<iframe id="gframe" name="gframe" width="600" height="320" " '+
                'src="http://docs.google.com/viewer?url='+encodeURIComponent("http://rangecontroller.com/xapp/tools/idcreator/pdf/idcard"+$('#membershipNo').val()+".pdf")+'&embedded=true" ><script>function printMe(){window.print();}</script>'+
                '</iframe><br/><button data-id="'+$('#membershipNo').val()+'" id="printgframe">PRINT</button>';
            

            var dialog = $('<p>Choose an action </p>');
            dialog.dialog({
                modal : true ,
                buttons : {
                    "Download PDF" : function(){
                        $(this).dialog("close"); 
                        window.location = url + "&download=1"             
                                         
                    },
                    "View PDF" : function(){

                        $.ajax({
                            url: url,
                            cache:false,
                            mimeType: 'application/pdf', 
                            success: function (output) {
                                    $(elmid).html(htmlObject);
                            }
                        });     

                         $(this).dialog("close");         
                    }
                } ,
                title : "PRINT ID CARD" ,
                width : "500px"
            });

            /*$.ajax({
                url: url,
                cache:false,
                mimeType: 'application/pdf', 
                success: function (output) {
                    	// display cached data
                    	$(elmid).html(html);
                    	// hide spinner
			/*
			 PDFJS.workerSrc = '/js/pdfjs/build/pdf.worker.js';
			 PDFJS.disableWorker = true;
			 PDFJS.getDocument(url).then(function getPdfHelloWorld(pdf) {
			   
			    pdf.getPage(1).then(function getPageHelloWorld(page) {
			      var scale = 1.5;
			      var viewport = page.getViewport(scale);

			      var canvas = document.getElementById('the-canvas');
			      var context = canvas.getContext('2d');
			     

			      var renderContext = {
				canvasContext: context,
				viewport: viewport
			      };
			      page.render(renderContext);			
				

			    });

	             });*/

                //}
            //});
        }
    };
    
    ctx.imageUpload = function(elmSel, elmProgressSel, imgElm){
        $(elmSel).fileupload({
            dataType: 'json',
            done: function(e, data) {
                ctx.bag.isFormUpdated = true;
                //alert(data.result.toSource());
                if (data.result.error) {
                    ctx.showError(data.result.error);
                } else {
                    
                    $(imgElm).attr('src', data.result.url.toString() + '?rand='+Math.random());
                    ctx.showSuccess("Successfully uploaded");
                }
                $(elmProgressSel).parent().hide();
            },
            start: function(e, data){
                $(elmProgressSel).css( 'width',  '0%' );
                $(elmProgressSel).parent().show();
            },
            fail: function(e, data){
                
                ctx.showError("Error occured while uploading, please try again");
                
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(elmProgressSel).css( 'width',  progress + '%' );
            }
        });
    };
    
    ctx.webcamLoad = function(elmId, swfFile){
        
        var width  = 320, height =240;
        var pos = 0, ctx1 = null, saveCB, 
                image = [];

	var canvas = document.createElement("canvas");
	canvas.setAttribute('width', width);
	canvas.setAttribute('height', height);
        
        var captureBtn = $('.captureBtn', $(elmId).parent());
        
        var beforeUpl = function(){
            ctx.showSpinner(captureBtn);
            captureBtn.prop('disabled',true);
        };
        var afterUpl = function(){
            ctx.clearSpinner(captureBtn);
            captureBtn.prop('disabled',false);
        };
        
        var uplOk = function(data){
            if (data.error) {
                ctx.showError(data.error);
            } else {
                $('#customer-image-preview').attr('src', data.url.toString() + '?'+Math.random());
                ctx.showSuccess("Successfully captured and uploaded");  
            }
        };
        
        var uplError = function(){
            ctx.showError("Error occured while uploading the capture");
        };
        
       
	if ( false && canvas.toDataURL) {
            
            ctx1 = canvas.getContext("2d");		
            image = ctx1.getImageData(0, 0, width, height);	
            saveCB = function(data) {			
                var col = data.split(";");
                var img = image;
                for(var i = 0; i < 320; i++) {
                        var tmp = parseInt(col[i]);
                        img.data[pos + 0] = (tmp >> 16) & 0xff;
                        img.data[pos + 1] = (tmp >> 8) & 0xff;
                        img.data[pos + 2] = tmp & 0xff;
                        img.data[pos + 3] = 0xff;
                        pos+= 4;
                }
                if (pos >= 4 * width * height) {
                    ctx1.putImageData(img, 0, 0);
                    beforeUpl();
                    $.post(ctx.bag.webcamSaveUrl, {type: "data", image: canvas.toDataURL("image/png")})
                            .done(function(data){
                                uplOk(data);
                            })
                                    .fail(uplError)
                            .always(function(){
                                afterUpl();
                            });
                    pos = 0;
                }
            };
	} else {
            saveCB = function(data) {
                image.push(data);			
                pos+= 4 * width;

                if (pos >= 4 * width * height) {
                    beforeUpl();
                    $.post(ctx.bag.webcamSaveUrl, {type: "pixel", image: image.join('|')})
                            .done(function(data){
                                uplOk(data);
                            }).fail(uplError)
                            .always(function(){
                                afterUpl();
                    });
                    pos = 0;
                }
            };
	}
        
        $(elmId).webcam({
		width: width,
		height: height,
		mode: "callback",
		swffile: swfFile,
		onSave: saveCB,
		onCapture: function () {
                    ctx.bag.isFormUpdated = true;
			webcam.save();
		},
                
                onLoad: function(){
                    //$('.captureBtn', $(elmId).parent()).prop('disabled',0);
                },

		debug: function (type, string) {
			//console.log(type + ": " + string);
		}
	});        
    };
    
    ctx.webcamCaptureSave = function(){        
        document.getElementById('XwebcamXobjectX').capture();
        //document.getElementById('XwebcamXobjectX').save();
    };
    
    ctx.logoutClicked = function(elm, memberData){
        var msg = " Are you sure you want to logout member " + 
                " \n\n"+ memberData + "\n\n" + 
                " This action is immediate and permanent ";
        if (confirm(msg)) {
            return true;
        }
        return false;
    };
    
    


})(window.jQuery, ranger.members.edit, rc, window.validator, window.numeral);

   
