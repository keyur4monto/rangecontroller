function setType(login_type) {
        document.getElementById("h_login_type").value = login_type;

        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

//alert(xmlhttp.responseText);
                if (xmlhttp.responseText == "Range") {
                    document.getElementById("divShooterLogin").style.display = "none";
                    document.getElementById("divRangeLogin").style.display = "block";
                } else if (xmlhttp.responseText == "Shooter") {
                    document.getElementById("divRangeLogin").style.display = "none";
                    document.getElementById("divShooterLogin").style.display = "block";
                }//end if response
            }//end if
        }//end onreadystatechange
        xmlhttp.open("POST", "ajax/login_type.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("login_type=" + login_type);
    }//END AJAX setType

    function rangeLogin() {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var resptext = xmlhttp.responseText;
                if (resptext != "success") {
                    if (resptext == 'Suspended') {
                        alert("Your account is suspended, please contact us.");
                    } else {
                        alert("Something's not right, Please try again.");
                    }
                    return;
                }
                window.location = "/xapp/dashboard";
            }//end if
        }//end onreadystatechange
//var un = document.getElementById('txtUsername').value;
        var rn = document.getElementById('txtRangeNumber').value;
        var pw = document.getElementById('txtPassword').value;
        xmlhttp.open("POST", "ajax/range_login.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("rn=" + rn + "&pw=" + pw);
    }//END AJAX rangeLogin

    function shooterLogin() {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
//if(xmlhttp.responseText=="blocked"){alert("You are blocked...");return;}
                if (xmlhttp.responseText != "success") {
                    alert("Something's not right, Please try again.");
                    return;
                }
                
				else if (xmlhttp.responseText === "success") {
				
				//alert("Successful Login!");
				
				window.location = "xapp/members/dashboard";
				
				}
            }//end if
        }//end onreadystatechange
        var srn = document.getElementById('txtShooterRangeNumber').value;
        var smn = document.getElementById('txtShooterNumber').value;
        var spw = document.getElementById('txtShooterPassword').value;
        xmlhttp.open("POST", "ajax/shooter_login.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("srn=" + srn + "&smn=" + smn + "&spw=" + spw);
    }//END AJAX shooterLogin

    function searchKeyPress1(e) {
        if (typeof e == 'undefined' && window.event) {
            e = window.event;
        }
        if (e.keyCode == 13) {
            rangeLogin();
        }//end if
    }//end function

    function searchKeyPress2(e) {
        if (typeof e == 'undefined' && window.event) {
            e = window.event;
        }
        if (e.keyCode == 13) {
            shooterLogin();
        }//end if
    }//end function