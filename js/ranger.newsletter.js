$(document).ready(function(){
	 
	 $( "#member" ).autocomplete({
      source: function( request, response ) {
			$.ajax({
			  url: urlPrefix + 'getGroupMembersJson',
			  type:'post',
			  dataType: "json",
			  data: {
				q: request.term,
				m: $("#member_type").val(),
				e:$("#employeename").val(),
				task:'searchmember'
			  },
			  success: function( data ) {
					response($.map(data, function(v,i){
						var text = v.label;
						if ( text ) {
							return {
									label: v.label,
									value: v.id,
									id:v.id,
								   };
						}
					}));
			  }
			});
      },
      minLength: 2,
      select: function( event, ui ) {
			$("#members span").append("<div class='memberlist' style='float:left'>&nbsp;"+ui.item.label+"<input type='hidden' name='receivers[]' value='"+ui.item.id+"'/>&nbsp;<input type='button'  class='removefromlist' value='X'></div>");
			$( "#member" ).val("");
			return false;
      },
	   open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
    });
	$("#members").on("click","input.removefromlist",function(){
			$(this).parent().remove();
	});
	function getSearch(){
		
	}
	
	$('#member_type').on('change', function(){
		mtype = $('#member_type').val();
		if(mtype != ''){
			$('.snd').append("<div class='memberlist' style='float:left'>&nbsp;"+mtype+"&nbsp;<input type='button'  class='removefromlist' value='X'>");
		}	
	});
	$('#everyone').on('click', function(){
		everyone = $('#everyone').val();
		if(everyone != ''){
			$('.snd').append("<div class='memberlist' style='float:left'>&nbsp;"+everyone+"<input type='hidden' name='everyone' value='yes'/>&nbsp;<input type='button'  class='removefromlist' value='X'>");
		}	
	});
});