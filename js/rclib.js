var rc  = rc || {};

(function($,ctx){
    /**
    * makes an element whose id elmid to tabs (same element structure as jquery tabs)
    * @param {string} elmid
    * @returns {undefined}
    */
   ctx.makeTabs = function(elmid, cb){
       $('#'+ elmid + " .tablist li a ").click(function(event) {
           event.preventDefault();
           $(this).parent().addClass("activetab");
           $(this).parent().siblings().removeClass("activetab");
           var tab = $(this).attr("href");
           $("#" + elmid + ' .tabitem').not(tab).css("display", "none");
           $(tab).fadeIn(380);
           cb(this);
       });
   };
   ctx.kvObjToKeyValue = function(obj){
       var results  = [];
       for (var k in obj) {
           results.push({key: k, value: obj[k]});
       }
       return results;
   };
   
    ctx.getLargeLoadingIndicator = function(message) {
        return '<div style="text-align:center">'+
            '<i class="fa fa-spinner fa-spin fa-3x"></i><br />' + 
                (message ? message : '') + '</div>';
    };
    ctx.showWaiting = function(message ){
        var msg = ctx.getLargeLoadingIndicator(message);
        var elmid = ctx.appendDivAndGetId();
         $('#'+elmid).html(msg).dialog({
            modal:true });
        return elmid;
    };

    ctx.showSpinner = function(elm, cfg){            
        $('.waitspinner ', $(elm).parent()).remove();
        var spinhtml = '<span class="waitspinner">&nbsp;' + 
            '<i class="fa fa-spinner fa-spin fa-lg"></i>&nbsp;</span>';

        if (cfg && cfg.isBefore) {
            $(elm).before(spinhtml);
        } else {
            $(elm).after(spinhtml);
        }            
    };

    ctx.clearSpinner = function(elm){           
        $('.waitspinner ', $(elm).parent()).remove();
    };

    ctx.appendDivAndGetId = function(toWhat){
        toWhat = toWhat ? toWhat : 'body';
        var id = 'elm' + (Math.random()+'').replace(/[^0-9]/, '');
        $(toWhat).append('<div id="'+id+'"></div>');
        return id;
    };
    ctx.showMessage = function(message,cfg){
        var elmid = ctx.appendDivAndGetId();
        return $('#'+elmid).html(message).dialog({
            modal:true, 
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                    if (cfg && cfg.okCb) {
                        cfg.okCb();
                    }
                }
            }
        });
    };
    ctx.showSuccess = function(message, cfg) {
        var check = '<span style="color:#0f0">'+ 
            '<i class="fa fa-check fa-3x"></i></span> &nbsp;';
        ctx.showMessage(check + message, cfg);
    };
    ctx.showError = function(message) {
        var check = '<span style="color:#f00">'+ 
            '<i class="fa fa-warning fa-2x"></i></span> &nbsp;';
        ctx.showMessage(check + message);
    };
    
    ctx.disableNavigateBackOnBackspace = function(){
        $(document).unbind('keydown').bind('keydown', function (event) {
            var doPrevent = false;
            if (event.keyCode === 8) {
                var d = event.srcElement || event.target;
                if ($(d).is(':input')) {
                    doPrevent = d.readOnly || d.disabled;
                }
                else {
                    doPrevent = true;
                }
            }

            if (doPrevent) {
                //alert('back disabled');
                event.preventDefault();
            }
        });
    };
    
})(jQuery, rc);


