<?php
session_start(); 
//require('common_security.php');

//require('common_db.php');
date_default_timezone_set('America/New_York');

//echo date("D M j g:i:s T Y");  

function formatDate($date){
    $formatted_date = date("n/j/Y",strtotime($date));
return $formatted_date;
}//END FORMAT DATE

function validateDate($test_date){
    $test_arr  = explode('/', $test_date);
    if (count($test_arr) == 3) {
        if (checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
            // valid date ...        
        } else {
            // problem with dates ...
            echo "problem with dates ...";
            echo "<script>alert('Problem with date...".$test_date." Please check the date is valid. The date format is m/d/yyyy. No leading zeros are needed for month or day.');</script>";
        }
    } else {
        // problem with input ... such as - instead of / 
        echo "problem with input ...";
    }
}//END VALIDATE DATE 

function formatCurrency($dollar_amount){
    return number_format($dollar_amount,2,'.',',');
}//END FORMAT CURRENCY

function formatEmail($email){

}//END FORMAT EMAIL

function validateEmail($email){

}//END VALIDATE EMAIL

function showMonth($month){
    $m="";
    switch($month){
        case 1:{$m="January";}break;
        case 2:{$m="February";}break;
        case 3:{$m="March";}break;
        case 4:{$m="April";}break;
        case 5:{$m="May";}break;
        case 6:{$m="June";}break;
        case 7:{$m="July";}break;
        case 8:{$m="August";}break;
        case 9:{$m="September";}break;
        case 10:{$m="October";}break;
        case 11:{$m="November";}break;
        case 12:{$m="December";}break;
    }//END SWITCH
    return $m;

}//END FUNCTION SHOW MONTH
    function getStateArray(){
        $stateList = ARRAY(
            'AL'=>"Alabama",
            'AK'=>"Alaska", 
            'AZ'=>"Arizona", 
            'AR'=>"Arkansas", 
            'CA'=>"California", 
            'CO'=>"Colorado", 
            'CT'=>"Connecticut", 
            'DE'=>"Delaware", 
            'DC'=>"District Of Columbia", 
            'FL'=>"Florida", 
            'GA'=>"Georgia", 
            'HI'=>"Hawaii", 
            'ID'=>"Idaho", 
            'IL'=>"Illinois", 
            'IN'=>"Indiana", 
            'IA'=>"Iowa", 
            'KS'=>"Kansas", 
            'KY'=>"Kentucky", 
            'LA'=>"Louisiana", 
            'ME'=>"Maine", 
            'MD'=>"Maryland", 
            'MA'=>"Massachusetts", 
            'MI'=>"Michigan", 
            'MN'=>"Minnesota", 
            'MS'=>"Mississippi", 
            'MO'=>"Missouri", 
            'MT'=>"Montana",
            'NE'=>"Nebraska",
            'NV'=>"Nevada",
            'NH'=>"New Hampshire",
            'NJ'=>"New Jersey",
            'NM'=>"New Mexico",
            'NY'=>"New York",
            'NC'=>"North Carolina",
            'ND'=>"North Dakota",
            'OH'=>"Ohio", 
            'OK'=>"Oklahoma", 
            'OR'=>"Oregon", 
            'PA'=>"Pennsylvania", 
            'RI'=>"Rhode Island", 
            'SC'=>"South Carolina", 
            'SD'=>"South Dakota",
            'TN'=>"Tennessee", 
            'TX'=>"Texas", 
            'UT'=>"Utah", 
            'VT'=>"Vermont", 
            'VA'=>"Virginia", 
            'WA'=>"Washington", 
            'WV'=>"West Virginia", 
            'WI'=>"Wisconsin", 
            'WY'=>"Wyoming");
        return $stateList;
    }
    function showOptionsDrop($array, $active, $echo){
        $string = '';
        foreach($array as $key => $value){
            $s = ($active == $k)? ' selected="selected"' : '';
            $string .= '<option value="'.$key.'"'.$s.'>'.$value.'</option>'."\n";
        }
        if($echo){
            echo $string;
        }else{
            return $string;
        }
      
    }

?>
