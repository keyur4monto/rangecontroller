<?php
// Send order confirmation and load successful order view
$subject = $memtype.' Membership Type';

$message = '<h3><b><u>'.$memtype.' Membership Sign Up is Complete - '.$_SESSION['company_name'].'</u></b></h3><br />
<b>Name: '.$_POST['first_name']." ".$_POST['last_name'].'</b><br /><br />
<b>Your range number is: <font color="red"><b>'.$_SESSION['account_number'].'</b></font></b><br /><br />
<b>Your membership number is: <font color="red"><b>'.$mn.'</b></font></b><br /><br />
<b>Your temporary password is: '.$temp_pw.'</b><br /><br />
<b>Your email: '.$_POST['email'].'</b><br /><br />
<b>Registration Date: '.formatDate(date('n/j/Y')).'</b><br /><br />

<b>Your membership type is: '.$memtype.'</b><br /><br />

'.$student_of.'

<p>
Keep this email for your records.  Use the login information above to:
</p>

<ul>
<li>Reserve time at the range</li>
<li>Schedule time in the future to shoot with our "Range Scheduler" online</li>
<li>See benefits used or available on your membership</li>
<li>Stay on top of future events at the range</li>
<li>Get helpful information on shooting strategies and useful tools</li>
<li>Renew or upgrade your membership</li>
<li>And much more!</li>
</ul>

<p>
When you&#39;re ready, simply go to <a href="https://www.rangecontroller.com">rangecontroller.com</a> and type your range number, membership number, and password in the 
login box on the upper right hand side to access your personal Shooter&#39;s Panel at The Range at '.strtoupper($_SESSION['company_name']).' and reap your 
benefits! It&#39;s totally FREE and comes with your membership or visitor pass. We hope to see you there!
</p>

<p style="font-weight: bold;">
REMEMBER - SHOOTIN&#39;S A BLAST AT '.strtoupper($_SESSION['company_name']).'!
</p>

<p>
We thank you '.$_POST['first_name'].' for signing up with us. We look forward to seeing you, and wish you a good day.<br /><br /> 
Thank you!
</p>

<p>
The gang at '.ucwords($_SESSION['company_name']).'<br />
'.$_SESSION['contact_phone_number'].'<br />
email - <a href="mailto:'.$_SESSION['registered_email'].'">'.$_SESSION['registered_email'].'</a><br />
</p>
';

$email_from = $_SESSION['registered_email'];

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'Content-Type: text/html; charset=ISO-8859-1\r\n';

mail($_POST['email'], $subject, $message, $headers);
mail("terry@davespawnshop.com", "MEMBER SIGNUP DETECTED - ".$subject, $message, $headers);//'terry@davespawnshop.com'
mail("c.r.garcia68@gmail.com", "MEMBER SIGNUP DETECTED - ".$subject, $message, $headers);//'c.r.garcia68@gmail.com'

?>

<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="c.r.garcia68@gmail.com" />

	<title></title>
</head>

<body>



</body>
</html>