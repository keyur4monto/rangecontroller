<?php
session_start();

$filename = $_SESSION['account_number'].'/webcam/'.$_SESSION['member_number'].".jpg";

$result = file_put_contents( $filename, file_get_contents('php://input') );
if (!$result) {
	print "ERROR: Failed to write data to $filename, check permissions\n";
	exit();
}
$url = 'https://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . '/' . $filename;

print "$url\n";
?>