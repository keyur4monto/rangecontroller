<?php
session_start();
require('../common/db.php');
require('../common/security.php');

$addir = "".$_SESSION['account_number']."/webcam/";

$file = $addir.trim($_GET['filename']);

if(!file_exists($file)) die("I'm sorry, the file doesn't seem to exist.");

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    set_time_limit(0); 
    readfile($file);
    exit;
}
?>