<?php
ssession_start();
require('db.php');
require('security.php');

function generatePassword($length = 8){
  $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
            '0123456789@#$%';

  $str = '';
  $max = strlen($chars) - 1;

  for ($i=0; $i < $length; $i++)
    $str .= $chars[rand(0, $max)];

  return $str;
}

function sendTemporaryPassword(){

$subject = 'Your temporary password';

$message = '<br />
<b>This is your temporary password. </b><br /><br />
'.generatePassword();

//$email_to = "c.r.garcia68@gmail.com";
$email_to = $_POST['txt_email'];
$email_from = PRIMARY_EMAIL_FROM;//swat@davespawnshop.com

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'Content-Type: text/html; charset=ISO-8859-1\r\n';

mail($email_to, $subject, $message, $headers);
}//END FUNCTION

if(isset($_POST['submit'])){
    
if($_POST['txt_email']==""){
    echo "<br /><br />Please enter an email address<br /><br /><a href='javascript:history.go(-1)'>BACK</a>";
    return;
}//end if
  
echo "<br /><br />Check your email ( ".$_POST['txt_email']." ) for the temporary password...";  
sendTemporaryPassword();    
    
}//END IF



?>

<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="carlos r garcia" />

	<title>Temporary Password</title>
</head>

<body>
<br /><br /><br /><br />

<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
<input type="text" id="txt_email" name="txt_email" style="width: 250px;" />
<br /><br />
<input type="submit" name="submit" value="Send Temporary Password" />
</form>

</body>
</html>