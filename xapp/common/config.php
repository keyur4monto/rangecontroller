<?php
session_start();
require('db.php');
//require('common_security.php');

$blnUpdateComplete = false;

if($_POST['h_update']==true){

$upd_admin_pw = "";
$upd_admin_pw = "UPDATE admin_login SET current_pw='".$_POST['txtPassword']."' ";
$upd_admin_pw .= "WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' ";

$upd_range_settings = "";
$upd_range_settings = "UPDATE controller_config SET registered_email='".$_POST['txtEmail']."', username='".$_POST['txtUsername']."', password='".$_POST['txtPassword']."', ";
$upd_range_settings .= "company_name='".$_POST['txtCompanyName']."', company_logo='".$_POST['txtCompanyLogo']."', tax_rate='".$_POST['txtTaxRate']."', number_of_lanes='".$_POST['txtNumberOfLanes']."', ";
$upd_range_settings .= "has_memberships='".$_POST['h_membership_management']."', member_number_start_count='".$_POST['txtStartCount']."', timestamp='".date("D M j g:i:s Y")."' ";
$upd_range_settings .= "WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' ";

$upd_biz_hours = "";
$upd_biz_hours = "UPDATE hours_of_operation SET sunday_from='".$_POST['txtSundayFrom']."', sunday_to='".$_POST['txtSundayTo']."', monday_from='".$_POST['txtMondayFrom']."', ";
$upd_biz_hours .= "monday_to='".$_POST['txtMondayTo']."', tuesday_from='".$_POST['txtTuesdayFrom']."', tuesday_to='".$_POST['txtTuesdayTo']."', wednesday_from='".$_POST['txtWednesdayFrom']."', ";
$upd_biz_hours .= "wednesday_to='".$_POST['txtWednesdayTo']."', thursday_from='".$_POST['txtThursdayFrom']."', thursday_to='".$_POST['txtThursdayTo']."', friday_from='".$_POST['txtFridayFrom']."', ";
$upd_biz_hours .= "friday_to='".$_POST['txtFridayTo']."', saturday_from='".$_POST['txtSaturdayFrom']."', saturday_to='".$_POST['txtSaturdayTo']."', num_of_lanes='".$_POST['txtNumberOfLanes']."' ";
$upd_biz_hours .= "WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' ";

$link->query($upd_admin_pw);
$link->query($upd_range_settings);
$link->query($upd_biz_hours);
//$link->close();

//sleep(1);

$blnUpdateComplete = true;
require_once('../includes/subjects.php');
header("Location: ../common/config.php");
}//END IF

$show_hide_sql = "";
$show_hide_sql = "SELECT point_of_sale, firearm_rentals FROM show_hide WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' "; 
if ($result = $link->query($show_hide_sql)) {   
while ($row = $result->fetch_assoc()) { 
$point_of_sale = $row['point_of_sale']; 
$firearm_rentals = $row['firearm_rentals']; 
}//END WHILE LOOP 
}//END IF
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html" />
<meta name="author" content="carlos r garcia" />
<meta name="contact" content="c.r.garcia68@gmail.com" />
<title>Range Controller - Configuration</title>
<link rel="stylesheet" type="text/css" href="../css/site_layout.css"/>
<?php
include('../includes/browser.php');

$b = new Browser;

switch($b->getBrowser()){
case "Safari":{
print '<link rel="stylesheet" type="text/css" href="../css/safari.css"/>';
}break;

case "Chrome":{
print '<link rel="stylesheet" type="text/css" href="../css/chrome.css"/>';
}break;

case "Internet Explorer":{
print '<link rel="stylesheet" type="text/css" href="../css/internet_explorer.css"/>';
}break;

case "Firefox":{
print '<link rel="stylesheet" type="text/css" href="../css/fire_fox.css"/>';
}break;
}//END SWITCH

?>
<style>
.table_header {
	background-color: #333333;
	color: white;
	font-weight: bold;
	height: 25px;
}
.table_header {
	background-color: #333333;
	color: white;
	font-weight: bold;
	height: 25px;
}
/*input{ 
border: 1px #000000 solid; 
/*
font-family: Arial, Helvetica, sans-serif; 
font-size: 11px; 

margin-top: 0px; 
margin-right: 0px; 
margin-bottom: 0px; 
margin-left: 0px; 
padding-top: 1px; 
padding-right: 0px; 
padding-bottom: 1px; 
padding-left: 2px; 
height: 18px
}*/

select {
	visibility: hidden;
}
#blanket {
	background-color: #111;
	opacity: 1.0;
*background:none;/*leave the asterisk at the beginning of this attribute*/
	position: absolute;
	z-index: 9001;
	top:0px;
	left:0px;
	width: 100%;
	height: 100%;
}
/*.maindiv{ width:640px; margin-left: -225px; margin-top: -25px; padding:20px; background:#CCC;}*/


.innerbg {
	padding:6px;
	background:#FFF;
}
.result {
	border:solid 1px #CCC;
	margin:10px 2px;
	padding:4px 2px;
}
.title a {
	font-weight:bold;
	color:#c24f00;
	text-decoration:none;
	font-size:14px;
}
.discptn a {
	text-decoration:none;
	color:#999;
}
.link a {
	color:#008000;
	text-decoration:none;
}

/*.upload_button{
width: 75px;
height: 25px;
cursor: pointer;
/*round corners*/
/*-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}
*/
/*.upload_button:hover{
background-color: green;
color: white;
}*/
</style>
<!--<style>
.table_header{
background-color: #666666; 
color: red;
font-weight: bold;
height: 25px;
}

input{ 
border: 1px #000000 solid; 
/*
font-family: Arial, Helvetica, sans-serif; 
font-size: 11px; 
*/
margin-top: 0px; 
margin-right: 0px; 
margin-bottom: 0px; 
margin-left: 0px; 
padding-top: 1px; 
padding-right: 0px; 
padding-bottom: 1px; 
padding-left: 2px; 
height: 18px
}

select{
visibility: hidden;
}

#blanket {
background-color: #111;
opacity: 1.0;
*background:none;/*leave the asterisk at the beginning of this attribute*/
position: absolute;
z-index: 9001;
top:0px;
left:0px;
width: 100%;
height: 100%;
}

/*.maindiv{ width:640px; margin-left: -225px; margin-top: -25px; padding:20px; background:#CCC;}*/


.innerbg{ padding:6px; background:#FFF;}
.result{ border:solid 1px #CCC; margin:10px 2px; padding:4px 2px;}
.title a{ font-weight:bold; color:#c24f00; text-decoration:none; font-size:14px;}
.discptn a{ text-decoration:none; color:#999;}
.link a{ color:#008000; text-decoration:none;}

.upload_button{
width: 75px;
height: 25px;
cursor: pointer;
/*round corners*/
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.upload_button:hover{
background-color: green;
color: white;
}
</style>-->
<script>
function closed(day){  
var from = "txt"+day+"From";
var to = "txt"+day+"To";
if(document.getElementById(from).value == "closed"){
document.getElementById(from).value = ""; 
document.getElementById(to).value = ""; 
//document.getElementById("txt"+day+"From").style.backgroundColor = "white";
//document.getElementById("txt"+day+"From").style.color = "black";
//document.getElementById("txt"+day+"To").style.backgroundColor = "white";
//document.getElementById("txt"+day+"To").style.color = "black";
}else{    
document.getElementById("txt"+day+"From").value = "closed"; 
document.getElementById("txt"+day+"To").value = "closed"; 
//document.getElementById("txt"+day+"From").style.backgroundColor = "red";
//document.getElementById("txt"+day+"From").style.color = "white";
//document.getElementById("txt"+day+"To").style.backgroundColor = "red";
//document.getElementById("txt"+day+"To").style.color = "white";
}//end if
document.getElementById("sel"+day+"From").style.visibility = "hidden";
document.getElementById("sel"+day+"To").style.visibility = "hidden";
}//end function

function showSelect(day){
//alert(day);
//day = the day of the week ex: SundayFrom, MondayTo, etc...
var from = "txt"+day;
var to = "txt"+day;
if(document.getElementById(from).value == "closed"){
document.getElementById(from).value = ""; 
document.getElementById(to).value = ""; 
document.getElementById("chk"+day).checked = false;
}//end if
var inputs = document.getElementsByTagName('input'); 
var select = document.getElementsByTagName('select'); 
document.getElementById("sel"+day).style.visibility = "visible";
//document.getElementById("sel"+day).style.visibility = "visible";
for(var i = 0; i < inputs.length; i++){
if(select[i].id == "sel"+day || select[i].id == "sel"+day){
//continue;
}else{
select[i].style.visibility = "hidden";
}//end if/else
}//end for loop
//document.getElementById("selMembershipLength").style.visibility = "visible";
}//end function

function setAMPM(data,day){
//data = AM or PM
//day = the day plus From or To ex: SundayFrom
var val = document.getElementById("txt"+day).value;
if(val==""){
alert("Input a time");
document.getElementById("sel"+day).selectedIndex = -1;
document.getElementById("txt"+day).focus();
return;
}
var t = val.slice(-2,val.length);
if(t=="AM"||t=="PM"){
var str = document.getElementById("txt"+day).value.substring(0,document.getElementById("txt"+day).value.length-2);
document.getElementById("txt"+day).value = str+data;
}else{
document.getElementById("txt"+day).value += data;
}//end if
document.getElementById("sel"+day).style.visibility = "hidden";
}//end function

function validateEmail(){ 
var e = document.getElementById("txtEmail").value; 
newVal = '';  
if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e)){
    
for(var c=0; c < e.length; c++) {    
newVal += e[c].substring(0,e[c].length).toLowerCase();
}

document.getElementById("txtEmail").value = newVal;
document.getElementById("txtEmail").style.color = "black";
document.getElementById("txtEmail").style.backgroundColor = "white"; 
document.getElementById("lblInvalidEmail").innerHTML = "";
return (true);   
}  
alert("You have entered an invalid email address!");
document.getElementById("lblInvalidEmail").innerHTML = "Invalid Email";
document.getElementById("lblInvalidEmail").style.color = "red";
document.getElementById("lblInvalidEmail").style.fontWeight = "bold";
//document.getElementById("txtEmail").focus(); 
//document.getElementById("email").value = "Invalid Email Address";
//document.getElementById("email").style.backgroundColor = "orange";
return (false);  
}//end function

function confirmEmail(){
var email1 = document.getElementById("txtEmail").value;
var email2 = document.getElementById("txtConfirmEmail").value;
if(email1!=email2){
//alert("Your emails do not match, Please re-enter");
document.getElementById("lblMatch").innerHTML = "No Match";
document.getElementById("lblMatch").style.color = "red";
}else{
document.getElementById("lblMatch").innerHTML = "Emails Match";
document.getElementById("lblMatch").style.color = "green";
}//end if
document.getElementById("lblMatch").style.fontWeight = "bold";
}//end function

function checkConfirm(){
if(document.getElementById("txtConfirmEmail").value==""){
document.getElementById("txtConfirmEmail").value = "confirm email...";
}
}//end function

function clearConfirm(){
document.getElementById("txtConfirmEmail").value = "";
}//end function

function typePW(){
if(document.getElementById("btnPassword").value == "Hide"){
document.getElementById("lblShowPassword").innerHTML = document.getElementById("txtPassword").value;
}else if(document.getElementById("btnPassword").value == "Show"){
document.getElementById("lblShowPassword").innerHTML = "";
}//end if
}//end function

function showPW(){
if(document.getElementById("btnPassword").value == "Show"){
document.getElementById("btnPassword").value = "Hide";
document.getElementById("lblShowPassword").innerHTML = document.getElementById("txtPassword").value;
}else if(document.getElementById("btnPassword").value == "Hide"){
document.getElementById("btnPassword").value = "Show";
document.getElementById("lblShowPassword").innerHTML = "";
}//end if
}//end function

function checkNumberOfLanes(){
if(document.getElementById("txtNumberOfLanes").value > 25){
document.getElementById("txtNumberOfLanes").value = "";
alert("The maximum number of lanes is 25, Please re-enter");
}//end if
}//end function

function GiveCorrectTimeFormat(strInputTime){
var strReturnValue = "";
if(strInputTime.length <= 5){
strInputTime = strInputTime.replace(":","");
//alert(strInputTime);
if(strInputTime.length ==1){
//strInputTime = "0" + strInputTime + ":" + "00";
strInputTime = strInputTime + ":" + "00";
}
else if (strInputTime.length ==2){
//alert("in 2");
if (strInputTime <= 23){
strInputTime =  strInputTime + ":" + "00";
}
else if (strInputTime <= 59){
strInputTime =  "00" + ":" + strInputTime;
}
else{
//strInputTime =  "00:00"
}
}
else if (strInputTime.length ==3){
//alert("in 3");
if (strInputTime < 959)
{
//alert(strInputTime.substring(1,3));
if( strInputTime.substring(1,2) <= 5){
//strInputTime =  "0" + strInputTime.substring(0,1) + ":" + strInputTime.substring(1,3);
strInputTime =  strInputTime.substring(0,1) + ":" + strInputTime.substring(1,3);

}
else{
//strInputTime =  "0" + strInputTime.substring(0,1) + ":" + "00";
strInputTime =  strInputTime.substring(0,1) + ":" + "00";
}
}
else {
//strInputTime =  "0" + strInputTime.substring(0,1) + ":" + "00";
strInputTime =  strInputTime.substring(0,1) + ":" + "00";
}
}
else if (strInputTime.length ==4){
//alert("in 4");
if (strInputTime < 2359)
{
//alert(strInputTime.substring(0,2));
if (strInputTime.substring(0,2) <= 23){

if( strInputTime.substring(2,3) <= 5){
strInputTime = strInputTime.substring(0,2) + ":" + strInputTime.substring(2,4);
}
else{
strInputTime = strInputTime.substring(0,2) + ":" + "00";
}
}
else{
//strInputTime =  "00:00"
}

}
else{
if (strInputTime.substring(0,2) <= 23)
{
if( strInputTime.substring(2,3) <= 5){
strInputTime = strInputTime.substring(0,2) + ":" + strInputTime.substring(2,4);
}
else{
strInputTime = strInputTime.substring(0,2) + ":" + "00";
}
}
else{
//strInputTime =  "00:00"
}
}
}
else{
//strInputTime =  "00:00"
}
//alert(strInputTime);
strReturnValue = strInputTime;
}
return strReturnValue;
}//end function

function MaskTimeFormat(day){
var objStartTime = document.getElementById(day);
var strStartTimeValue = objStartTime.value;
objStartTime.value = GiveCorrectTimeFormat(strStartTimeValue);
}//end function

function setEnabled(){
var option = "";
document.getElementById("h_membership_management").value = "";
if(document.getElementById("chkDisable").checked){
document.getElementById("chkDisable").checked = false;
}//end if
if(document.getElementById("chkEnable").checked){
option = "Yes";//enable membership management
}else{
option = "n/a";
}//end if
document.getElementById("h_membership_management").value = option;
}//end function

function setDisabled(){
var option = "";
document.getElementById("h_membership_management").value = "";
if(document.getElementById("chkEnable").checked){
document.getElementById("chkEnable").checked = false;
}//end if
if(document.getElementById("chkDisable").checked){
option = "No";//disable membership management
}else{
option = "n/a";
}//end if
document.getElementById("h_membership_management").value = option;
}//end function

function setNumberCount(){
if(document.getElementById("txtStartCount").value.length > 5){
alert("Maximum number of digits is 5\n\nExample: 10000 or 50000\n\nThe maximum number that can generate is 99999\n\nPlease re-enter a value");
document.getElementById("txtStartCount").value = "";
document.getElementById("txtStartCount").focus();
}//end if 
}//end function

function setEnableBenefits(){
var option = "";
document.getElementById("h_benefits_management").value = "";
if(document.getElementById("chkDisableBenefits").checked){
document.getElementById("chkDisableBenefits").checked = false;
}//end if
if(document.getElementById("chkEnableBenefits").checked){
option = "Yes";//disable membership management
}else{
option = "n/a";
}//end if
document.getElementById("h_benefits_management").value = option;
}//end function

function setDisableBenefits(){
var option = "";
document.getElementById("h_benefits_management").value = "";
if(document.getElementById("chkEnableBenefits").checked){
document.getElementById("chkEnableBenefits").checked = false;
}//end if
if(document.getElementById("chkDisableBenefits").checked){
option = "No";//disable membership management
}else{
option = "n/a";
}//end if
document.getElementById("h_benefits_management").value = option;
}//end function

function updateControllerSettings(){
var email         = document.getElementById("txtEmail").value;
var confirm_email = document.getElementById("txtConfirmEmail").value;
var username      = document.getElementById("txtUsername").value;
var password      = document.getElementById("txtPassword").value;

var company_name  = document.getElementById("txtCompanyName").value;
var company_logo  = document.getElementById("txtCompanyLogo").value;
var tax_rate      = document.getElementById("txtTaxRate").value;
var num_of_lanes  = document.getElementById("txtNumberOfLanes").value;

var sun_from      = document.getElementById("txtSundayFrom").value;
var sun_to        = document.getElementById("txtSundayTo").value;
var mon_from      = document.getElementById("txtMondayFrom").value;
var mon_to        = document.getElementById("txtMondayTo").value;
var tue_from      = document.getElementById("txtTuesdayFrom").value;
var tue_to        = document.getElementById("txtTuesdayTo").value;
var wed_from      = document.getElementById("txtWednesdayFrom").value;
var wed_to        = document.getElementById("txtWednesdayTo").value;
var thur_from     = document.getElementById("txtThursdayFrom").value;
var thur_to       = document.getElementById("txtThursdayTo").value;
var fri_from      = document.getElementById("txtFridayFrom").value;
var fri_to        = document.getElementById("txtFridayTo").value;
var sat_from      = document.getElementById("txtSaturdayFrom").value;
var sat_to        = document.getElementById("txtSaturdayTo").value;

if(!checkAMPM(sun_from)){alert("AM PM error...");return;}
if(!checkAMPM(sun_to))  {alert("AM PM error...");return;}

if(!checkAMPM(mon_from)){alert("AM PM error...");return;}
if(!checkAMPM(mon_to))  {alert("AM PM error...");return;}

if(!checkAMPM(tue_from)){alert("AM PM error...");return;}
if(!checkAMPM(tue_to))  {alert("AM PM error...");return;}

if(!checkAMPM(wed_from)){alert("AM PM error...");return;}
if(!checkAMPM(wed_to))  {alert("AM PM error...");return;}

if(!checkAMPM(thur_from)){alert("AM PM error...");return;}
if(!checkAMPM(thur_to))  {alert("AM PM error...");return;}

if(!checkAMPM(fri_from)) {alert("AM PM error...");return;}
if(!checkAMPM(fri_to))   {alert("AM PM error...");return;}

if(!checkAMPM(sat_from)) {alert("AM PM error...");return;}
if(!checkAMPM(sat_to))   {alert("AM PM error...");return;}

//var memb_management = document.getElementById("h_membership_management").value;
//var start_count     = document.getElementById("txtStartCount").value;
//var benf_management = document.getElementById("h_benefits_management").value;
var email_match = document.getElementById("lblMatch").innerHTML;

//create message if something is missing   
var msg = "";
if(email == ""){msg += "Required registered email\n";}
if(confirm_email == "confirm email..."){msg += "Required to confirm email\n";}
if(username == ""){msg += "Required username\n";}
if(password == ""){msg += "Required password\n";}
if(company_name == ""){msg += "Required company name\n";} 
if(company_logo == ""){msg += "Required to upload company logo file\n";} 
if(tax_rate == ""){msg += "Required tax rate\n";} 
if(num_of_lanes == ""){msg += "Required number of lanes\n";}
if(sun_from == ""){msg += "Required Sunday from hours\n";} 
if(sun_to == ""){msg += "Required Sunday to hours\n";} 
if(mon_from == ""){msg += "Required Monday from hours\n";} 
if(mon_to == ""){msg += "Required Monday to hours\n";} 
if(tue_from == ""){msg += "Required Tuesday from hours\n";} 
if(tue_to == ""){msg += "Required Tuesday to hours\n";} 
if(wed_from == ""){msg += "Required Wednesday from hours\n";} 
if(wed_to == ""){msg += "Required Wednesday to hours\n";} 
if(thur_from == ""){msg += "Required Thursday from hours\n";} 
if(thur_to == ""){msg += "Required Thursday to hours\n";} 
if(fri_from == ""){msg += "Required Friday from hours\n";} 
if(fri_to == ""){msg += "Required Friday to hours\n";} 
if(sat_from == ""){msg += "Required Saturday from hours\n";} 
if(sat_to == ""){msg += "Required Saturday to hours\n";} 
//if(memb_management == ""){msg += "Required to enable or disable membership management\n";} 
//if(start_count == ""){msg += "Required to set a member number start count\n";} 
//if(benf_management == ""){msg += "Required to enable or disable benefits management\n";} 
if(email_match == "No Match"){msg += "Required to confirm an email match\n";} 

if(msg != ""){alert(msg);return;}

//if all is good, submit form
document.getElementById("h_update").value = true;
document.getElementById("frmControllerConfig").submit();
}//end function

function toUSD() {
var ctrl = document.getElementById("txtMembershipPrice").value;
var number = ctrl.toString(), 
dollars = number.split('.')[0], 
cents = (number.split('.')[1] || '') +'00';
dollars = dollars.split('').reverse().join('')
.replace(/(\d{3}(?!$))/g, '$1,')
.split('').reverse().join('');
document.getElementById("txtMembershipPrice").value = dollars + '.' + cents.slice(0, 2);
//return '$' + dollars + '.' + cents.slice(0, 2);
}//end function

</script>
<script type="text/javascript" src="../js/blanket_popup.js" ></script>
<script type="text/javascript" src="../upload/Ajaxfileupload-jquery-1.3.2.js" ></script>
<script type="text/javascript" src="../upload/ajaxupload.3.5.js" ></script>
<link rel="stylesheet" type="text/css" href="../upload/Ajaxfile-upload.css" />
<script type="text/javascript" >
function open_upload(url){
popup('uploadDiv'); 
}//end function


$(function(){
var btnUpload=$('#me');
var mestatus=$('#mestatus');
var files=$('#files');
new AjaxUpload(btnUpload, {
action: '../upload/uploadPhoto.php',
name: 'uploadfile',
onSubmit: function(file, ext){
if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
// extension is not allowed 
mestatus.text('Only JPG, PNG or GIF files are allowed');
return false;
}
mestatus.html('<img src="../upload/ajax-loader.gif" height="16" width="16">');
},
onComplete: function(file, response){
//On completion clear the status
mestatus.text('Photo Uploaded Sucessfully!');

var file_ext = file.split('.').pop();
var new_file = "logo."+file_ext;
document.getElementById("txtCompanyLogo").value = new_file;

//On completion clear the status
files.html('');
//Add uploaded file to list
if(response==="success"){//https://rangecontroller.com/var/www/vhosts/rangecontroller.com/logos/free-thank-you.png
$('<li></li>').appendTo('#files').html('<img src="../img/'+<?=$_SESSION['account_number']?>+'/'+new_file+'" alt="" height="120" width="130" /><br />').addClass('success');
} else{
$('<li></li>').appendTo('#files').text(file).addClass('error');
}
}
});

});

function isNumberKey(evt){
// . is key code 46
var charCode = (evt.which) ? evt.which : event.keyCode;
//alert(charCode);
if(charCode == 46){return true;}
if (charCode > 31 && (charCode < 48 || charCode > 57)){return false;}
return true;
}//end function

function toggleMemberships() {
document.getElementById("selMembershipLength").style.visibility = "visible";
var str = "hidethis";
if( document.getElementById(str).style.display=='none' ){
document.getElementById(str).style.display = '';
//document.getElementById("sign-up-slider").style.height = "1500px";
}else{
document.getElementById(str).style.display = 'none';
//document.getElementById("sign-up-slider").style.height = "1375px";
}//end if/else
}//end function

function insertMembership(hn,an){
var msg = "";
var txtname = document.getElementById("txtMembershipName").value;
var txtprice = document.getElementById("txtMembershipPrice").value;
var txtlength = document.getElementById("h_membership_length").value;
var txtrequired = document.getElementById("h_payment_required").value;

if(txtname==""){msg+="Required membership name.\n";}
if(txtprice==""){msg+="Required membership price.\n";}
if(txtlength==""){msg+="Required membership length.\n";}
if(txtrequired==""){msg+="Required if payment is required for membership.\n";}

if(msg!=""){alert(msg);return;}

if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}
else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
//document.getElementById("divInventorySearchResults").innerHTML=xmlhttp.responseText;
//document.getElementById("divInventorySearchResults").style.border="1px solid #A5ACB2";
}
}
var name = document.getElementById("txtMembershipName").value;
var price = document.getElementById("txtMembershipPrice").value;
var length = document.getElementById("h_membership_length").value;
var required = document.getElementById("h_payment_required").value;

var m = "<table width='100%'><tr><td >"+name+'</td><td >'+price+'</td><td >'+length+'</td><td >'+required+'</td></tr><table>';

xmlhttp.open("POST","../ajax/editable_memberships.php",false);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send("hn="+hn+"&an="+an+"&name="+name+"&price="+price+"&length="+length+"&required="+required);

document.getElementById("lblEditableMemberships").innerHTML += m;
document.getElementById("txtMembershipName").value = "";
document.getElementById("txtMembershipPrice").value = "";
document.getElementById("h_membership_length").value = "";
document.getElementById("selMembershipLength").selectedIndex = "0";

//document.getElementById("sign-up-slider").style.height = "1650px";
window.setTimeout(function(){location.reload();},700);
}//end function

function setPaymentRequired(option){
document.getElementById("h_payment_required").value = option;
}//end function

function setMembershipLength(length){
document.getElementById("h_membership_length").value = length;
}//end function

function disableMembership(){
var str = "hidethis";
document.getElementById(str).style.display = 'none';
document.getElementById("sign-up-slider").style.height = "1375px";
}//end function

function deleteMembership(hn,an,mn){

//alert(hn+' '+an+' '+mn);return;
var msg = "Press OK to delete "+mn+ " from your membership list or Cancel";
var result = confirm(msg);
if(!result){return;}

if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}
else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
//document.getElementById("divInventorySearchResults").innerHTML=xmlhttp.responseText;
//document.getElementById("divInventorySearchResults").style.border="1px solid #A5ACB2";
}
}
xmlhttp.open("POST","../ajax/delete_membership.php",false);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send("hn="+hn+"&an="+an+"&mn="+mn);

window.setTimeout(function(){location.reload();},500);

}//end function

function lower(str,id){
var outString = str.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s+]/gi,'');
document.getElementById(id).value = outString.toLowerCase();
}//end function

function setPOS(option){
//alert(option);
document.getElementById("h_show_pos").value = option;
}//end function

function setFirearmRentals(option){
//alert(option);
document.getElementById("h_show_firearm_rentals").value = option;
}//end function

function showSelectX(){
document.getElementById("selShowPOS").style.visibility = "visible";
document.getElementById("selShowFirearmRentals").style.visibility = "visible";
document.getElementById("selMembershipLength").style.visibility = "visible";
document.getElementById("selPaymentRequired").style.visibility = "visible";
}//end function

function lower(str,id){//removes spaces also
var outString = str.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s+]/gi,'');
//document.getElementById(id).value = outString.toLowerCase();
document.getElementById(id).value = outString;
}//end function

function checkAMPM(str){
if ( str.indexOf( "closed" ) > -1){return true;}
if ( str.indexOf( "AM" ) > -1 || str.indexOf( "PM" ) > -1){return true;}
else{return false;}
}//end function

function showHide(field,show_or_hide){
var element = "";
var element_value = show_or_hide;
var msg = "";
switch(field){
case "point_of_sale":{msg = element_value+" POS module.";}break;
case "firearm_rentals":{msg = element_value+" Firearm Rentals module.";}break;
}//end switch
if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}
else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){

if(xmlhttp.responseText=="success"){alert(msg);location.reload();}
//else{alert("show message...");}

}//end if
}//end onreadystatechange
xmlhttp.open("POST","../ajax/show_hide.php",false);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send("field="+field+"&show_or_hide="+show_or_hide);
}//end showHide
</script>
</head>
<body onLoad="toggleMemberships();showSelectX();" bgcolor="#000000" style="color: white;" >
<div align="center"><img src="../img/background.jpg" class="bg" /></div>
<div id="blanket" style="display:none;"></div>
<!--used for popup upload-->
<table width="950" align="center">
  <tr>
    <td width="950" bgcolor="#000000"><img src="../img/logo.png" width="400"></td>
  </tr>
</table>
<table width="950" bgcolor="#1E1E1E" align="center">
  <tr>
    <td width="15%">&nbsp;</td>
    <td width="70%" valign="top" align="center"><font color="#fbb91f" size="6"><b>
      <?=$_SESSION['company_name']?>
      </b></font></td>
    <td width="15%">&nbsp;</td>
  </tr>
</table>
<!--<table width="950" align="center" bgcolor="#1E1E1E">
  <tr>
    <td width="30%" align="center"><input type="button" id="home" name="home" value="HOME" onClick="window.location = '../configurations.php'"/>
      &nbsp; 
      <!--<input type="button" id="clear" name="clear" style="width: 90px; height: 25px;" value="CLEAR ALL" onclick="window.location.reload();"/></td>-->
<!--<td width="70%" align="right"><font size="5"> 
      <!--<label style="text-align: left;">?=date("D M j Y")?></label>-->
<!--&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<b>RANGE CONTROLLER SETTINGS</b></font> <br />
      <font color="red"><b>* current range settings</b></font> <br />
      Customer Number:&nbsp;<font color="red"><b>
      
      </b></font></td>
  </tr>
</table>-->
<table width="950" align="center" bgcolor="#1E1E1E">
  <tr>
    <td width="100%"><div class="headermargb">
        <div class="breadcrumb"><a href="../point.php">Home </a> / <a href="../configurations.php">Module Configurations </a> / <span>Range Controller Settings </span></div>
        <div class="header_right">Range Controller Settings<br>
          <span style="color: red">
          Range # <?=$_SESSION['account_number']?>
          </span></div>
      </div></td>
  </tr>
</table>
<form  id="frmControllerConfig" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="2" class="table_header">REGISTERED EMAIL ADDRESS</td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Email Address:</td>
            <td><input type="text" id="txtEmail" name="txtEmail" onBlur="validateEmail();" style="width: 200px" autocomplete="off" value="<?=$_SESSION['registered_email']?>"/>
              &emsp;
              <label id="lblInvalidEmail"></label></td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Confirm Email Address: </td>
            <td><input type="text" id="txtConfirmEmail" name="txtConfirmEmail" value="confirm email..." onKeyUp="confirmEmail();" onClick="clearConfirm();" onBlur="checkConfirm();" style="width: 200px" autocomplete="off"/>
              &emsp;
              <label id="lblMatch"></label></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="2" class="table_header">ACCOUNT NUMBER AND PASSWORD</td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Account Number:</td>
            <td><input type="text" id="txtUsername" name="txtUsername" style="width: 200px;" autocomplete="off" value="<?=$_SESSION['account_number']?>" readonly="readonly"/>
              (edit disabled) </td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Password:</td>
            <td><input type="hidden" id="h_pw" name="h_pw" value="<?=$_SESSION['password']?>"/>
              <input type="password" id="txtPassword" name="txtPassword" onKeyUp="typePW();" autocomplete="off" value="<?=$_SESSION['password']?>"/>
              <input type="button" id="btnPassword" name="btnPassword" value="Show" onClick="showPW();" />
              <label id="lblShowPassword"></label></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <!--
<table width="950" align="center" bgcolor="#1E1E1E">
<tr>
<td>

<table width="75%" align="center">
<tr><td colspan="2" class="table_header">URL / FOLDER NAME</td></tr>
<tr>
<td width="50%">&nbsp;URL:&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
https://</td>
<td>
<input type="text" id="txtURL" name="txtURL" value="www.rangecontroller.com" style="width: 200px;" disabled="disabled"/>
(edit disabled)
</td>
</tr>
<tr>
<td width="50%">&nbsp;Folder Name:&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
</td>
<td>
<input type="text" id="txtFolderName" name="txtFolderName" style="width: 200px;" autocomplete="off" value="<?=$url_folder_name?>" readonly="readonly"/>
(edit disabled)
</td></tr>
</table>

</td>
</tr>
</table>
-->
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="2" class="table_header">COMPANY NAME AND LOGO</td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Company Name:</td>
            <td><input type="text" id="txtCompanyName" name="txtCompanyName" style="width: 200px;" autocomplete="off" value="<?=$_SESSION['company_name']?>" onKeyUp="lower_companyname(this.value,this.id);" /></td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Company Logo File Name:</td>
            <td><!--START UPLOAD CODE-->
              <table width="75%">
                <tr>
                  <!--<td class="table2">Company Logo:&nbsp;&nbsp;</td>-->
                  <td class="table3"><input type="text" id="txtCompanyLogo" name="txtCompanyLogo" style="width: 145px;" autocomplete="off" value="<?=$_SESSION['company_logo']?>" />
                    <input type="button" id="btnCompanyLogo" name="btnCompanyLogo" value="Upload" onClick="open_upload('process_upload_file.php')" class="upload_button" />
                    <!--START POPUP-->
                    <!--<div id="blanket" style="display:none;"></div> this div is in header.php to be able to cover entire background-->
                    <div id="uploadDiv" style="display:none;" >
                      <!--BEGIN POPUP HTML UPLOAD CODE-->
                      <p style="text-align: right; padding: 20px;"><a onClick="popup('uploadDiv');" style="cursor: pointer; font-size: larger; color: black; ">Done</a></p>
                      <center>
                        <div class="maindiv">
                          <div class="innerbg">
                            <table  width="50%" cellpadding="2" cellspacing="2">
                              <tr>
                                <td  colspan="3" align="left" valign="middle" bgcolor="#008000"><div id="title" style="margin:0px 10px; font-weight:bold; color: black; font-size:16px;">Upload Company Logo</div></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="left" valign="middle"><font color="red">Supported file types:&nbsp; jpg * png * jpeg * gif&emsp;&emsp;2KB file size</font>
                                  <div id="flash"></div>
                                  <div id="ajaxresult"></div></td>
                              </tr>
                              <tr>
                                <td colspan="3"><table width="630" border="0">
                                    <tr>
                                      <td width="194" align="center" valign="middle"><div id="me" class="styleall" style=" cursor:pointer;"> <span style=" cursor:pointer; font-family:Verdana, Geneva, sans-serif; font-size:9px;"> <span style=" cursor:pointer;">Click Here To Upload Your Logo</span> </span> </div>
                                        <span id="mestatus" style="color: red;"></span></td>
                                      <td width="208" align="center" valign="middle"><div id="files">
                                          <li class="success"></li>
                                        </div></td>
                                      <td width="228" align="right" valign="center">&nbsp;</td>
                                    </tr>
                                  </table></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="left" valign="middle"></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </center>
                      <!--END POPUP HTML UPLOAD CODE-->
                    </div>
                    <!--END POPUP--></td>
                </tr>
              </table>
              <!--END UPLOAD CODE--></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="2" class="table_header">TAX RATE</td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Tax Rate:&emsp;(in decimal form) example: .0975</td>
            <td><input type="text" id="txtTaxRate" name="txtTaxRate" value="<?=$_SESSION['tax_rate']?>" /></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="2" class="table_header">NUMBER OF LANES FOR RANGE (Maximum of 25)</td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Number of Lanes:</td>
            <td><input type="text" id="txtNumberOfLanes" name="txtNumberOfLanes" onKeyUp="checkNumberOfLanes();" value="<?=$_SESSION['number_of_lanes']?>"/></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <!--
12 hour time
/^(0?[1-9]|1[012]):[0-5][0-9]$/

24 hour time
/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/
-->
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td class="table_header">HOURS OF OPERATION (For Scheduler)</td>
            <td width="20%" style="text-align: center;" class="table_header">FROM</td>
            <td width="20%" style="text-align: center;" class="table_header">TO</td>
            <td width="20%" style="text-align: center;" class="table_header">CLOSED</td>
          </tr>
          <tr>
            <td width="25%">&nbsp;Sunday</td>
            <td style="text-align: center;"><input type="text" id="txtSundayFrom" name="txtSundayFrom" style="width: 75px; text-align: center;" onClick="showSelect('SundayFrom');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['sunday_from']?>" autocomplete="off"/>
              <select id="selSundayFrom" name="selSundayFrom" style="width: 50px;" onChange="setAMPM(this.value,'SundayFrom');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="text" id="txtSundayTo" name="txtSundayTo" style="width: 75px; text-align: center;" onClick="showSelect('SundayTo');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['sunday_to']?>" autocomplete="off"/>
              <select id="selSundayTo" name="selSundayTo" style="width: 50px;" onChange="setAMPM(this.value,'SundayTo');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="checkbox" id="chkSunday" name="chkSunday" onClick="closed('Sunday');"/></td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Monday</td>
            <td style="text-align: center;"><input type="text" id="txtMondayFrom" name="txtMondayFrom" style="width: 75px; text-align: center;" onClick="showSelect('MondayFrom');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['monday_from']?>" autocomplete="off"/>
              <select id="selMondayFrom" style="width: 50px;" onChange="setAMPM(this.value,'MondayFrom');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="text" id="txtMondayTo" name="txtMondayTo" style="width: 75px; text-align: center;" onClick="showSelect('MondayTo');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['monday_to']?>" autocomplete="off"/>
              <select id="selMondayTo" style="width: 50px;" onChange="setAMPM(this.value,'MondayTo');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="checkbox" id="chkMonday" name="chkMonday" onClick="closed('Monday');"/></td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Tuesday</td>
            <td style="text-align: center;"><input type="text" id="txtTuesdayFrom" name="txtTuesdayFrom" style="width: 75px; text-align: center;" onClick="showSelect('TuesdayFrom');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['tuesday_from']?>" autocomplete="off"/>
              <select id="selTuesdayFrom" style="width: 50px;" onChange="setAMPM(this.value,'TuesdayFrom');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="text" id="txtTuesdayTo" name="txtTuesdayTo" style="width: 75px; text-align: center;" onClick="showSelect('TuesdayTo');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['tuesday_to']?>" autocomplete="off"/>
              <select id="selTuesdayTo" style="width: 50px;" onChange="setAMPM(this.value,'TuesdayTo');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="checkbox" id="chkTuesday" name="chkTuesday" onClick="closed('Tuesday');"/></td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Wednesday</td>
            <td style="text-align: center;"><input type="text" id="txtWednesdayFrom" name="txtWednesdayFrom" style="width: 75px; text-align: center;" onClick="showSelect('WednesdayFrom');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['wednesday_from']?>" autocomplete="off"/>
              <select id="selWednesdayFrom" style="width: 50px;" onChange="setAMPM(this.value,'WednesdayFrom');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="text" id="txtWednesdayTo" name="txtWednesdayTo" style="width: 75px; text-align: center;" onClick="showSelect('WednesdayTo');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['wednesday_to']?>" autocomplete="off"/>
              <select id="selWednesdayTo" style="width: 50px;" onChange="setAMPM(this.value,'WednesdayTo');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="checkbox" id="chkWednesday" name="chkWednesday" onClick="closed('Wednesday');"/></td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Thursday</td>
            <td style="text-align: center;"><input type="text" id="txtThursdayFrom" name="txtThursdayFrom" style="width: 75px; text-align: center;" onClick="showSelect('ThursdayFrom');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['thursday_from']?>" autocomplete="off"/>
              <select id="selThursdayFrom" style="width: 50px;" onChange="setAMPM(this.value,'ThursdayFrom');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="text" id="txtThursdayTo" name="txtThursdayTo" style="width: 75px; text-align: center;" onClick="showSelect('ThursdayTo');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['thursday_to']?>" autocomplete="off"/>
              <select id="selThursdayTo" style="width: 50px;" onChange="setAMPM(this.value,'ThursdayTo');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="checkbox" id="chkThursday" name="chkThursday" onClick="closed('Thursday');"/></td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Friday</td>
            <td style="text-align: center;"><input type="text" id="txtFridayFrom" name="txtFridayFrom" style="width: 75px; text-align: center;" onClick="showSelect('FridayFrom');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['friday_from']?>" autocomplete="off"/>
              <select id="selFridayFrom" style="width: 50px;" onChange="setAMPM(this.value,'FridayFrom');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="text" id="txtFridayTo" name="txtFridayTo" style="width: 75px; text-align: center;" onClick="showSelect('FridayTo');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['friday_to']?>" autocomplete="off"/>
              <select id="selFridayTo" style="width: 50px;" onChange="setAMPM(this.value,'FridayTo');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="checkbox" id="chkFriday" name="chkFriday" onClick="closed('Friday');"/></td>
          </tr>
          <tr>
            <td width="50%">&nbsp;Saturday</td>
            <td style="text-align: center;"><input type="text" id="txtSaturdayFrom" name="txtSaturdayFrom" style="width: 75px; text-align: center;" onClick="showSelect('SaturdayFrom');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['saturday_from']?>" autocomplete="off"/>
              <select id="selSaturdayFrom" style="width: 50px;" onChange="setAMPM(this.value,'SaturdayFrom');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="text" id="txtSaturdayTo" name="txtSaturdayTo" style="width: 75px; text-align: center;" onClick="showSelect('SaturdayTo');" onBlur="MaskTimeFormat(this.id);" value="<?=$_SESSION['saturday_to']?>" autocomplete="off"/>
              <select id="selSaturdayTo" style="width: 50px;" onChange="setAMPM(this.value,'SaturdayTo');">
                <option></option>
                <option>AM</option>
                <option>PM</option>
              </select></td>
            <td style="text-align: center;"><input type="checkbox" id="chkSaturday" name="chkSaturday" onClick="closed('Saturday');"/></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="3" class="table_header"><a name="createnow">MEMBERSHIP MANAGEMENT</a></td>
          </tr>
          <tr>
            <td width="50%" >&nbsp;Memberships Enabled ?: <font color="yellow"><b>
              <?=$_SESSION['has_memberships']?>
              </b></font></td>
            <input type="hidden" id="h_membership_management" name="h_membership_management" value="<?=$_SESSION['has_memberships']?>"/>
            <td >&nbsp;Enable:&nbsp;
              <input type="checkbox" id="chkEnable" name="chkEnable" onClick="setEnabled();"/></td>
            <td>&nbsp;Disable:&nbsp;
              <input type="checkbox" id="chkDisable" name="chkDisable" onClick="setDisabled();"/></td>
          </tr>
        </table>
        <!--START EDITABLE MEMBERSHIPS -->
        <table id="hidethis" width="75%" align="center" bgcolor="#1E1E1E" >
          <tr>
            <td colspan="4" style="text-align: left;"> Enter the values for a membership then press Add. You can only add one at a time. When you have finished, simply exit
              this page. <font color="yellow">No need to press update at the bottom of page, Membership management section writes directly to your database when you press Add.</font></td>
          </tr>
          <tr style="background-color:#33333;">
            <td style="text-align: center;font-size: small;">Membership Name</td>
            <td style="text-align: center;font-size: small;">Membership Price</td>
            <td style="text-align: center;font-size: small;">Membership Length</td>
            <td style="text-align: center;font-size: small;">Payment<br />
              Required</td>
          </tr>
          <tr>
            <td style="text-align: center;">&emsp;
              <input type="text" id="txtMembershipName" name="txtMembershipName" onKeyUp="lower(this.value,this.id);"/></td>
            <td style="text-align: center;">&emsp;
              <input type="text" id="txtMembershipPrice" name="txtMembershipPrice" style="width: 50px;"/>
              <input type="button" value="$" style="width: 25px; height: 25px;" onClick="toUSD()" title="Press to format to dollar amount"/></td>
            <td style="text-align: center;">&emsp;
              <input type="hidden" id="h_membership_length" name="h_membership_length"/>
              <select id="selMembershipLength" onChange="setMembershipLength(this.value);" style="visibility: visible; width: 100px;">
                <option></option>
                <option>1 week</option>
                <option>1 month</option>
                <option>6 months</option>
                <option>1 year</option>
                <option>2 year</option>
                <option>3 year</option>
              </select></td>
            <td style="text-align: center;">&emsp;
              <input type="hidden" id="h_payment_required" name="h_payment_required"/>
              <select id="selPaymentRequired" onChange="setPaymentRequired(this.value);" style="visibility: visible; width: 50px;">
                <option></option>
                <option>Yes</option>
                <option>No</option>
              </select></td>
          </tr>
          <tr>
            <td style="text-align: center;"><input type="button" value="Add" style="width: 100px; height: 20px" onClick="insertMembership('<?=$_SESSION['account_number_hashed']?>','<?=$_SESSION['account_number']?>');"/></td>
            <td colspan="3"  style="text-align: center;"><label id="lblEditableMemberships" style="color: red;"></label></td>
          </tr>
          <tr>
            <td colspan="4"><b>CURRENT MEMBERSHIPS:</b><br />
              <br />
              <table width="100%">
                <tr style="background-color:#333333;">
                  <td style="text-align: center;font-size: small;background-color:#1e1e1e;"></td>
                  <td style="text-align: center;font-size: small;">Membership Name</td>
                  <td style="text-align: center;font-size: small;">Membership Price</td>
                  <td style="text-align: center;font-size: small;">Membership Length</td>
                  <td style="text-align: center;font-size: small;">Payment<br />
                    Required</td>
                  <td style="text-align: center;background-color:#1e1e1e;"></td>
                </tr>
                <?php
$editable_memberships_sql = "";
$editable_memberships_sql = "SELECT * FROM editable_memberships ";
$editable_memberships_sql .= "WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' ";
if ($result = $link->query($editable_memberships_sql)) {  
$i=0;
$mhn = $_SESSION['account_number_hashed'];//hash number
$man = $_SESSION['account_number'];//account number
while ($row = $result->fetch_assoc()) { 
$mna = $row['membership_name'];
$i++;
echo '<tr><td style="text-align: center;">'.$i.'.</td><td style="text-align: center;">'.$row['membership_name'].'</td><td style="text-align: center;">'.$row['membership_price'].'</td><td style="text-align: center;">'.$row['membership_length']."</td><td style='text-align: center;'>".$row['payment_required']."</td><td style='text-align: center;'><input type='button' value='Delete' onclick='deleteMembership(\"$mhn\",\"$man\",\"$mna\");' title='Delete: ".$row['membership_name']."'/></td></tr>"; 
}//END WHILE LOOP 
if($i==0){echo "<center><font color='red'>There are no memberships available or created.</font></center>";}
}//END IF
?>
              </table></td>
          </tr>
        </table>
        <!--END EDITABLE MEMBERSHIPS--></td>
    </tr>
  </table>
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="3" class="table_header">SET MEMBER NUMBER START COUNT</td>
          </tr>
          <tr>
            <td width="50%" >&nbsp;Enter Start Count: (Maximum of 5 digits long)</td>
            <td ><input type="text" id="txtStartCount" name="txtStartCount" onKeyUp="setNumberCount();" value="<?=$_SESSION['member_number_start_count']?>" readonly="readonly"/>
              (edit disabled) </td>
          </tr>
        </table></td>
    </tr>
  </table>
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="3" class="table_header"><a name="possection">SHOW POS (Point Of Sale)</a></td>
          </tr>
          <tr>
            <td width="50%" >&nbsp;Show POS ?&nbsp;
              <label id="lblShowPOS" style="color: yellow; font-weight: bold;">
                <?=$point_of_sale?>
              </label></td>
            <td><input type="hidden" id="h_show_pos" name="h_show_pos" value="<?=$point_of_sale?>"/>
              <select id="selShowPOS" onChange="showHide('point_of_sale',this.value);" style="visibility: visible; width: 75px;">
                <option></option>
                <option>Show</option>
                <option>Hide</option>
              </select></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="3" class="table_header"><a name="firearmsection">SHOW FIREARM RENTALS</a></td>
          </tr>
          <tr>
            <td width="50%" >&nbsp;Show Firearm Rentals ?&nbsp;
              <label id="lblShowPOS" style="color: yellow; font-weight: bold;">
                <?=$firearm_rentals?>
              </label></td>
            <td ><input type="hidden" id="h_show_firearm_rentals" name="h_show_firearm_rentals" value="<?=$firearm_rentals?>"/>
              <select id="selShowFirearmRentals" onChange="showHide('firearm_rentals',this.value);" style="visibility: visible; width: 75px;">
                <option></option>
                <option>Show</option>
                <option>Hide</option>
              </select></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <!--
<table width="950" align="center" bgcolor="#1E1E1E">
<tr>
<td>
<table width="75%" align="center">
<tr><td colspan="3" class="table_header">MEMBER BENEFITS MANAGEMENT</td></tr>
<tr>
<td width="50%">&nbsp;Benefits Enabled: <font color="red"><b><$has_benefits_section?></b></font></td>
<input type="hidden" id="h_benefits_management" name="h_benefits_management"/>
<td >&nbsp;Enable:&nbsp;<input type="checkbox" id="chkEnableBenefits" name="chkEnableBenefits" onclick="setEnableBenefits();"/></td>
<td>&nbsp;Disable:&nbsp;<input type="checkbox" id="chkDisableBenefits" name="chkDisableBenefits" onclick="setDisableBenefits();"/></td>
</tr>
</table>
</td>
</tr>
</table>
-->
  <table width="950" align="center" bgcolor="#1E1E1E">
    <tr>
      <td><table width="75%" align="center">
          <tr>
            <td colspan="3" class="table_header">UPDATE CONTROLLER SETTINGS</td>
          </tr>
          <tr>
            <td colspan="4" style="text-align: right; height: 35px;"><input type="hidden" id="h_update" name="h_update" />
              <input type="button" id="btnUpdate" name="btnUpdate" onClick="updateControllerSettings();" value="Update" style="width: 125px; height: 25px;"/>
              &emsp; </td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
<?php
include("../includes/footer.php");
?>
</body>
</html>
<?php
if($_SESSION['has_memberships']=="Yes"){
echo
'
<script>
toggleMemberships();
</script>
';
}//END TOGGLE IF

if($blnUpdateComplete){
echo "
<script>
alert('Update Complete');
window.location = '../common/config.php';
</script>
";
//header("Location: common_config.php");
}//END UPDATE

error_reporting(E_ALL);
ini_set('display_errors',1);
?>
