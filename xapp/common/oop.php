<?php
    //session_start();
    //require('security.php');

    //require('db.php');
    //require('common_security.php');
    /**
     * @author Terry Frymire
     * //tfrymire@juno.com  
     */
    class customer{

        function __construct($member_number,$db_object){     

            $selectCustomerSql = "SELECT * FROM customers "
                               .     "LEFT JOIN driver_license_info "
                               .       "ON driver_license_info.membership_number = customers.membership_number "
                               .     " AND driver_license_info.acctnumberhashed = customers.acctnumberhashed "
                               .     " AND driver_license_info.accountnumber = customers.accountnumber "
                               .     "LEFT JOIN emergency_contact "
                               .       "ON emergency_contact.membership_number = customers.membership_number "
                               .     " AND emergency_contact.acctnumberhashed = customers.acctnumberhashed "
                               .     " AND emergency_contact.accountnumber = customers.accountnumber "
                               .     "LEFT JOIN certs_and_safety "
                               .       "ON certs_and_safety.membership_number = customers.membership_number "
                               .     " AND certs_and_safety.acctnumberhashed = customers.acctnumberhashed "
                               .     " AND certs_and_safety.accountnumber = customers.accountnumber "
                               .     "LEFT JOIN memberships "
                               .       "ON memberships.membership_number = customers.membership_number  "
                               .     " AND memberships.acctnumberhashed = customers.acctnumberhashed "
                               .     " AND memberships.accountnumber = customers.accountnumber "
                              .     "WHERE customers.membership_number = '".$member_number
                               .     "' AND customers.acctnumberhashed = '".$_SESSION['account_number_hashed']
                               .     "' AND customers.accountnumber = '".$_SESSION['account_number']."' ";
            $result = $db_object->query($selectCustomerSql);
            if ($result) {

                while ($row = $result->fetch_assoc()) {
                    $this->membership_number = $row['membership_number'];
                    $this->membership_type = $row['membership_type'];
                    $this->membership_date = $row['membership_date'];
                    $this->membership_expires = $row['membership_expires'];

                    $this->first_name = $row['first_name'];
                    $this->middle_name = $row['middle_name'];
                    $this->last_name = $row['last_name'];

                    $this->street_address = $row['street_address'];
                    $this->city = $row['city'];
                    $this->state = $row['state'];

                    $this->zipcode = $row['zipcode'];
                    $this->ssn = $row['ssn'];
                    $this->home_phone = $row['home_phone'];
                    $this->cell_phone = $row['cell_phone'];
                    $this->email = $row['email'];

                    $this->license_state = $row['license_state'];
                    $this->license_number = $row['license_number'];
                    $this->license_expires = $row['license_expires'];
                    $this->license_height = $row['license_height'];
                    $this->license_weight = $row['license_weight'];
                    $this->license_hair = $row['license_hair'];
                    $this->license_eyes = $row['license_eyes'];
                    $this->license_race = $row['license_race'];
                    $this->license_sex = $row['license_sex'];
                    $this->license_birthdate = $row['license_birthdate'];
                    $this->license_birth_city = $row['license_birth_city'];
                    $this->license_birth_state = $row['license_birth_state'];
                    $this->license_birth_country = $row['license_birth_country'];
                    $this->license_comments = $row['license_comments'];

                    $this->contact_name = $row['contact_name'];
                    $this->contact_phone = $row['contact_phone'];

                    $this->certs_certificates_comments = $row['certs_certificates_comments'];
                    $this->certs_video_complete = $row['certs_video_complete'];
                    $this->certs_safety_form_complete = $row['certs_safety_form_complete'];

                    //$this->range_total_hours = $row['range_total_hours'];       NOT USED
                    //$this->range_time_timestamp = $row['range_time_timestamp']; NOT USED

                    //this info is pulled directly from within the benefits.php page
                    //$this->extended_range_hours = $row['extended_range_hours'];
                    //$this->guest_passes = $row['guest_passes'];
                    //$this->firearm_transfers = $row['firearm_transfers'];
                    //$this->firearm_rentals = $row['firearm_rentals'];
                    //$this->machinegun_rentals = $row['machinegun_rentals'];
                    //$this->benefits_timestamp = $row['timestamp'];//you have to know this belongs to the used_membership_benefits table, i left name as is

                    //$result->free(); 

                }//END WHILE LOOP
            } else{
                //echo $member_number . " does not exist in the database.";return;        
            }
        //return $this;//**RETURN VALUE
        }//END CONSTRUCTOR

        public $membership_number;
        public $membership_type;
        public $membership_date;
        public $membership_expires;

        public $first_name;
        public $middle_name;
        public $last_name;

        public $street_address;
        public $city;
        public $state;

        public $zipcode;
        public $ssn;
        public $home_phone;

        public $cell_phone;
        public $email;

        public $license_state;
        public $license_number;
        public $license_expires;

        public $license_height;
        public $license_weight;
        public $license_hair;

        public $license_eyes;
        public $license_race;
        public $license_sex;

        public $license_birthdate;
        public $license_birth_city;
        public $license_birth_state;

        public $license_birth_country; 
        public $license_comments; 

        public $contact_name; 
        public $contact_phone;

        public $certs_certificates_comments;  
        public $certs_video_complete;
        public $certs_safety_form_complete; 

        public $range_total_hours; 
        //public $range_time_timestamp; NOT USED

        public $extended_range_hours;
        public $guest_passes;
        public $firearm_transfers;
        public $firearm_rentals;
        public $machinegun_rentals;
        public $benefits_timestamp; 

        function __destruct() {
               //echo "Destroying Customer Object " . $this->membership_number;
               //unset($this);
        }//END DESTRUCTOR 

    }//END CUSTOMER CLASS
?>