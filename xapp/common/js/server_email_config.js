
function showDialog() {
    var dlg_content = "";
    dlg_content = "<table style='background-color: white;' border='1'>";
    dlg_content += "<tr><td style='background-color: #666666; color: white; font-weight: bold;'>Send Reminder&emsp;</td><td style='background-color: #666666; color: white; text-align: center;'><input type='button' value='X' onclick='Dialog.close();'></td></tr>";

    dlg_content += "<tr><td style='vertical-align: top; text-align: left;'><input type='text' id='txtSubject' name='txtSubject' /><input type='button' value='Set Subject' onclick='setSubject();' /><br /></td></tr>";

    dlg_content += "<tr><td style='text-align: center; vertical-align: top;'></td></tr>";
    dlg_content += "</table>";

    new Dialog.HTML(dlg_content).show();
}//end showDialog

var seco = seco || {};
(function($, seco){
    
    seco.loadTpl = function(tpltypeid){
        var atpl = seco.active_template = seco.custom_templates[''+tpltypeid];
        if (!atpl) {
            alert('Template is not found for the selected email');
            return;
        }
        //$('#testTemplateType').val(atpl.se_templatetype_fk);
        var ttypeid = atpl.se_templatetype_fk;
        $('#activeTplName').html(seco.template_types['' + ttypeid].sett_name);
        $('#activeTplSubject').val(atpl.template_subject);        
        tinymce.activeEditor.setContent(atpl.template_body);
        $('#tplGetStarted').hide();
        $('#tplControls').show();
        
        seco.hidePlaceholders(0);
    };
    
    seco.saveTpl = function(){
        var data = {
            template_subject: $.trim($('#activeTplSubject').val()),
            template_body: $.trim(tinymce.activeEditor.getContent()),
            id: seco.active_template.id,            
            cmd: 'updateTemplate'
        };
        //alert(data.toSource());
        if (data.template_subject == '' || data.template_body == '') {
            alert('Please fill out both the subject and message fields');
            return;
        }
        
        $.ajax({
            url: '../ajax/update_server_email_template.php',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(data),
            error: function(req, status, err){
                alert('Error occured while saving, please try again');
            },
            success: function(data, status, err){
                if (data.status == 'success') {
                    alert('Successfully saved');
                } else if (data.status == 'error'){
                    alert(data.message);
                }
            }
        });
    };
    seco.updateNotifs = function(){
        var data = [];
        $('.notifyCb').each(function(ind, elm){
            var arr = $(elm).attr('id').toString().split('-');
            data.push({            
                id: arr[1],
                notifyWhat: arr[0],            
                val: $(elm).prop('checked') ? 1 : 0            
            });            
        });
        //alert(data.toSource());
        $.ajax({
            url: '../ajax/update_server_email_template.php',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({cmd:'updateNotifyMulti', data:data}),
            error: function(req, status, err){
                alert('Error occured, please try again');
            },
            success: function(data, status, err){
                if (data.status != 'success') {
                    alert('Error occured, please try again');
                } else {
                    alert('Notification settings are successfully updated');
                }
            }
        });
    };
    
    seco.sendTestEmail = function(){       
       
        var data = {
            templateTypeId: seco.active_template.se_templatetype_fk,
            email: $('#testEmail').val(),
            subject: $('#activeTplSubject').val(),
            body: tinymce.activeEditor.getContent()
        };
        if (!data.templateTypeId) {
            alert('Please select a template from dropdown list to test');
            return;
        }
        if (!data.email) {
            alert('Please enter the email address to sent this test mail');
            return;
        }
        
        $.ajax({
            url: '../ajax/send_server_email_template_test_mails.php',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(data),
            error: function(req, status, err){
                alert('Error occured, please try again');
            },
            success: function(data, status, err){
                if (data.status != 'success') {
                    alert(data.message);                    
                } else {
                    alert('Mail is sent');
                }
            }
        });        
    };
    
    seco.showHidePlaceholders = function(elm){
        var el = $(elm);
        if (seco.placeholders_visible) {
            seco.hidePlaceholders(620);
        } else {
            el.html('Hide placeholders');
            var id = '#placeholdersList'+seco.active_template.se_templatetype_fk;
            
            $(id).show(620);
            seco.placeholders_visible = true;
        }        
    };
    
    seco.hidePlaceholders = function(speed){
        $('#placeholderCtrl').html('Show placeholders');
        seco.placeholders_visible = false;
        $('.placeholders').hide(speed);
    };
    
    seco.restoreToDeafults = function(){
        if (confirm("You are about to restore all of your templates to defaults.\n" + 
                "Are you sure you want to do that?")) {
        document.getElementById('frmRestoreDefaults').submit();
                }
    };
    
})(jQuery, seco);


