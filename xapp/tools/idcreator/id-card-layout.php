<?php
session_start();
/**
* @author carlos r garcia
* @copyright 2013
*/
require __DIR__ .'/../../../bootstrap.php';
use Rc\Services\DbHelper;
use Rc\Services\ArrayHelper;
use Rc\Models\range_owner; 

$range_owner = $app['range_owner'];
if (!$range_owner) {
    exit('Range owner could not be instantiated');
}

require_once('barcode.php'); 
//require_once('alpha_class.php'); 
//require_once('fpdf.php'); 
require_once('fpdi/fpdi.php'); 


//include_once("../../common/db.php");
$link = DbHelper::getWrappedHandle($app['dbs']['main']);
//include("../../common/constants.php");

$sql_settings = "";
$sql_settings = "SELECT * FROM id_card_creator WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' ";//PULL SETTINGS FROM DB, MOSTLY YES OR NO MEANING SHOW OR HIDE 
if ($result = $link->query($sql_settings)) {  
while ($row1 = $result->fetch_assoc()) { 
$gender = $row1['gender'];
$client_name = $row1['client_name'];
$client_picture = $row1['client_picture'];
$client_address = $row1['client_address'];
$client_dob = $row1['client_dob'];
$client_certs = $row1['client_certs'];
$client_emergency_contact = $row1['client_emergency_contact'];
$client_membership_type = $row1['client_membership_type'];
$client_membership_number = $row1['membership_number'];
$barcode = $row1['barcode'];
$company_name = $row1['company_name'];
$company_logo = $row1['company_logo'];
$use_logo_for_background = $row1['use_logo_for_background'];
$company_address = $row1['company_address'];
$company_phone_number = $row1['company_phone_number'];
$use_company_slogan = $row1['use_company_slogan'];
$company_slogan = $row1['company_slogan'];
$membership_border_banner = $row1['membership_border_banner'];
$use_special_instructions = $row1['use_special_instructions'];
$special_instructions = $row1['special_instructions'];
$shooter_portal_text = $row1['shooter_portal_text'];
$shooter_login_and_password = $row1['shooter_login_and_password'];
$use_background_image = $row1['use_background_image'];
}//END WHILE LOOP 
}//END IF
 
//-------------------------------------->START HELPER VARIABLES  
$customer_number = ArrayHelper::getIfExists($_GET, 'cn', 0);
//$customer_number = 10002;
//$blnBorders = $_GET['b'];
$blnBorders = 0;
$banner_color = "0,0,0";//204,204,204
$font_family = "arial";
$bold = 1;

if($gender=='male'){
$gender_name = "John Doe";
$image_path = "male-missing.jpg";
}else{
$gender_name = "Jane Doe";
$image_path = "female-missing.jpg";
}

if($membership_border_banner=="Yes"){
$solid_cell_for_thumbnail = 1;//CUSTOMER PICTURE
$solid_cell_for_membership_banner = 1;
$default_text_color = "255,255,255";
}else{
$solid_cell_for_thumbnail = 0;//CUSTOMER PICTURE
$solid_cell_for_membership_banner = 0;
$default_text_color = "0,0,0";
}

//1576 North Main St. Crossville, TN
$business_street_address = "1576 North Main St.";
$business_citystatezipcode = "Crossville, TN 38555";
$business_phone = "931-484-8947";
//-------------------------------------->END HELPER VARIABLES

$pdf=new PDF_EAN13();
$pdf->AddPage();
$pdf->EAN13(80,40,'123456789012');

$pdfi = new FPDI('L','in',array(3.38,2.13));
$pdfi->SetAutoPageBreak(false);
$pdfi->SetDisplayMode('real','default');

$pdfi->SetFont($font_family,'B',9);        

$pagecount = $pdfi->setSourceFile('id-card-template.pdf'); 
$tplidx = $pdfi->importPage(1, '/MediaBox'); 

$pdfi->addPage(); 

$pdfi->useTemplate($tplidx, 0, 0,null,null,true);        

//*************************start code
$sql = "";
//$sql = "SELECT * FROM customers WHERE membership_number = '$customer_number' ";

$sql = "SELECT * FROM customers ";
$sql .= "JOIN emergency_contact ON customers.membership_number = emergency_contact.membership_number "; 
$sql .= "JOIN driver_license_info ON emergency_contact.membership_number = driver_license_info.membership_number ";
$sql .= "JOIN memberships ON driver_license_info.membership_number = memberships.membership_number "; 
$sql .= "JOIN certs_and_safety ON memberships.membership_number = certs_and_safety.membership_number "; 
$sql .= "WHERE customers.membership_number = '$customer_number' AND customers.acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND customers.accountnumber = '".$_SESSION['account_number']."'
";

if ($result = $link->query($sql)) {  
while ($row = $result->fetch_assoc()) { 
$membership_number = $row['membership_number'];
$fn = $row['first_name'];
$ln = $row['last_name'];
$street_address = $row['street_address'];
$city = $row['city'];
$state = $row['state'];
$zipcode = $row['zipcode'];
$home_phone = $row['home_phone'];
$contact_name = $row['contact_name'];
$contact_phone = $row['contact_phone'];
$birth_date = $row['license_birthdate'];
$membership_type = $row['membership_type'];
}//END WHILE LOOP 

$customer_name = $fn.' '.$ln;
$citystatezipcode = $city.' '.$state.' '.$zipcode;
 
/*   
$filename = "./../webcam/photos/$membership_number.jpg";
if (file_exists($filename)) {//exists    
$image_path = "./../webcam/photos/$membership_number.jpg";
} else {//does not exist    
$image_path = "./../webcam/image_missing.png";
}
*/

$image_logo_path = "logo_for_layout.png";
$background_image = "background_for_layout.png";//IMAGE NO LONGER AVAIABLE, WILL CAUSE AN ERROR

if($use_logo_for_background=="Yes"){
$background_image = "../". $range_owner->getCompanyLogo();////PATH TO COMPANY LOGO THATS USED FOR RANGE
}

/*
if($use_background_image=="Yes"){
$background_uploaded_image = "logo.png";////PATH TO COMPANY LOGO THATS USED FOR RANGE
$pdfi->Image($background_uploaded_image,0,0,3.38,2.13);//CARD BACKGROUND WATERMARK
}
*/

$pdfi->SetXY(.4,.06);
$pdfi->Cell(-.35);//move cell left or right
if($client_picture=="Yes"){
$pdfi->Cell(1.1,1.08, "" ,$blnBorders, 0, 'C', $solid_cell_for_thumbnail);//BLACK COLOR BEHIND IMAGE TO CREATE BORDER
}

if($client_picture=="Yes"){
$pdfi->Image($image_path,.1,.1,1,1);//LEFT TOP CORNER (IMAGE OF CUSTOMER)
}

if($company_logo=="Yes"){
$pdfi->Image($image_logo_path,1.3,.25,.35,.35);//BUSINESS or COMPANY LOGO * *
}//END IF $company_logo=="Yes"

$pdfi->SetFont($font_family,'B',12);   
$pdfi->SetX(1);//X
$pdfi->SetY(1.19);//Y 
$pdfi->Cell(-.3);//move cell left or right
if($client_membership_number=="Yes"){
$pdfi->Cell(1,.23, "12565", $blnBorders, 0, 'C', 0);//MEMBERSHIP NUMBER
}

$pdfi->SetX(1);//X
$pdfi->SetY(1.4);//Y 
if($barcode=="Yes"){
$pdfi->SetFont($font_family,'B',9); 
$pdfi->EAN13(0.8,1.95,"12565",.15,.02);//BARCODE
}

$pdfi->SetFont($font_family,'B',9); 
$pdfi->SetX(.30);//X
$pdfi->SetY(.06);//Y 
$pdfi->Cell(.73);//move cell left or right
if($company_name=="Yes"){
$pdfi->Cell(2.2,.14, "Your Company Name Here", $blnBorders, 0, 'C', 0);//BUSINESS or COMPANY NAME
}

$pdfi->SetFont($font_family,'B',6); 
$pdfi->SetX(.30);//X
$pdfi->SetY(.2);//Y 
$pdfi->Cell(1.43);//move cell left or right
if($use_company_slogan=="Yes"){
$pdfi->Cell(1.5,.11, "Company Slogan" ,0, 0, 'L', 0);//BUSINESS SLOGAN
}

$pdfi->SetFont($font_family,'B',7); 
$pdfi->SetX(.30);//X
$pdfi->SetY(.31);//Y 
$pdfi->Cell(1.43);//move cell left or right
if($company_address=="Yes"){
$pdfi->Cell(1.5,.11, "Street Address", $blnBorders, 0, 'L', 0);//BUSINESS ADDRESS PART 1
}

$pdfi->SetFont($font_family,'B',7); 
$pdfi->SetX(.30);//X
$pdfi->SetY(.42);//Y 
$pdfi->Cell(1.43);//move cell left or right
if($company_address=="Yes"){
$pdfi->Cell(1.5,.11, "City, State Zipcode", $blnBorders, 0, 'L', 0);//BUSINESS ADDRESS PART 2
}

$pdfi->SetFont($font_family,'B',7); 
$pdfi->SetX(.30);//X
$pdfi->SetY(.53);//Y 
$pdfi->Cell(1.43);//move cell left or right
if($company_phone_number=="Yes"){
$pdfi->Cell(1.5,.11, "Phone Number", $blnBorders, 0, 'L', 0);//BUSINESS ADDRESS PART 3 (PHONE NUMBER)
}

$pdfi->SetFont($font_family,'B',10);
//$pdfi->SetFillColor($banner_color);
$pdfi->SetX(.30);//X
$pdfi->SetY(.65);//Y 
$pdfi->Cell(.75);//move cell left or right

if($client_membership_type=="Yes"){
$pdfi->SetTextColor($default_text_color);
$pdfi->Cell(2.25,.20, "Membership Type Here" ,$blnBorders, 0, 'C', $solid_cell_for_membership_banner);//MEMBERSHIP COLOR (BLACK) BANNER 
}

$pdfi->SetTextColor(0,0,0);

$pdfi->SetFont($font_family,'B',8); 
$pdfi->SetX(.30);//X
$pdfi->SetY(.9);//Y 
$pdfi->Cell(.85);//move cell left or right
if($client_name=="Yes"){
$pdfi->Cell(1.4,.14, $gender_name, $blnBorders, 0, 'L', 0);//CUSTOMER FIRST/LAST NAME
}
if($client_dob=="Yes"){
$pdfi->Cell(0.68,.14, "12/12/1966", $blnBorders, 0, 'L', 0);//CUSTOMER BIRTHDATE
}

$pdfi->SetX(.30);//X
$pdfi->SetY(1.04);//Y 
$pdfi->Cell(.85);//move cell left or right
if($client_address=="Yes"){
$pdfi->Cell(1.8,.14, "4321 Cypress Grove", $blnBorders, 0, 'L', 0);//CUSTOMER STREET ADDRESS
}

$pdfi->SetX(.30);//X
$pdfi->SetY(1.18);//Y 
$pdfi->Cell(.85);//move cell left or right
if($client_address=="Yes"){
$pdfi->Cell(1.8,.14, "Chattanooga, TN 37123", $blnBorders, 0, 'L', 0);//CUSTOMER CITY STATE ZIPCODE
}

$pdfi->SetFont($font_family,'B',6); 
$pdfi->SetX(.30);//X
$pdfi->SetY(1.32);//Y 
$pdfi->Cell(.85);//move cell left or right 85
if($client_emergency_contact=="Yes"){
$pdfi->Cell(2.05,.11, "Emergency: Name & Phone Number", $blnBorders, 0, 'L', 0);//CUSTOMER EMERGENCY CONTACT
}

$pdfi->SetFont($font_family,'B',6); 
$pdfi->SetX(.30);//X
$pdfi->SetY(1.47);//Y 
$pdfi->Cell(-.3);//move cell left or right
if($use_special_instructions=="Yes"){
$msg = "Special Instructions: ".$special_instructions;
$pdfi->Cell(3.2,.11, $msg,$blnBorders, 0, 'C', 0);//SPECIAL INSTRUCTIONS
}

$pdfi->SetFont($font_family,'B',8); 
$pdfi->SetX(.30);//X
$pdfi->SetY(1.58);//Y 
$pdfi->Cell(-.3);//move cell left or right
if($client_certs=="Yes"){
$pdfi->Cell(3.2,.12, "Other Certifications: TN Handgun Permit, NRA IH,NRA OH" ,$blnBorders, 0, 'C', 0);//OTHER CERTIFICATES
}

$pdfi->SetFont($font_family,'B',7); 
$pdfi->SetX(.30);//X
$pdfi->SetY(1.7);//Y 
$pdfi->Cell(-.3);//move cell left or right
if($shooter_portal_text=="Yes"){
$portal_msg = "Shooter portal online! www.rangecontroller.com";
$pdfi->Cell(3.2,.11,$portal_msg ,$blnBorders, 0, 'C', 0);//SHOOTER PORTAL
}

$pdfi->SetX(.30);//X
$pdfi->SetY(1.81);//Y 
$pdfi->Cell(-.3);//move cell left or right
if($shooter_login_and_password=="Yes"){
$pdfi->Cell(3.2,.11, "RANGE#: 10000 SHOOTER#: 4409 PASSWORD FDS44D5#" ,$blnBorders, 0, 'C', 0);//USERNAME AND PASSWORD
}
}
//END IF

//*************************end code
$pdfi->Close();
$pdfi->Output('idcard.pdf', 'I'); 
//*********************************************************************************************************
?>