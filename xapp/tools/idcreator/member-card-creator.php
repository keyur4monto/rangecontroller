<?php
session_start();
require('../../common/db.php');
require('../../common/security.php');

$sql_settings = "";
$sql_settings = "SELECT * FROM id_card_creator WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' "; 
if ($result = $link->query($sql_settings)) {  
while ($row1 = $result->fetch_assoc()) { 
$gender = $row1['gender'];
$client_name = $row1['client_name'];
$client_picture = $row1['client_picture'];
$client_address = $row1['client_address'];
$client_dob = $row1['client_dob'];
$client_certs = $row1['client_certs'];
$client_emergency_contact = $row1['client_emergency_contact'];
$client_membership_type = $row1['client_membership_type'];
$client_membership_number = $row1['membership_number'];
$barcode = $row1['barcode'];
$company_name = $row1['company_name'];
$company_logo = $row1['company_logo'];
$use_logo_as_background = $row1['use_logo_for_background'];
$company_address = $row1['company_address'];
$company_phone_number = $row1['company_phone_number'];
$use_company_slogan = $row1['use_company_slogan'];
$company_slogan = $row1['company_slogan'];
$membership_border_banner = $row1['membership_border_banner'];
$use_special_instructions = $row1['use_special_instructions'];
$special_instructions = $row1['special_instructions'];
$shooter_portal_text = $row1['shooter_portal_text'];
$shooter_login_and_password = $row1['shooter_login_and_password'];
$use_background_image = $row1['use_background_image'];
}//END WHILE LOOP 
}//END IF
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html" />
<meta name="author" content="carlos r garcia" />
<meta name="contact" content="c.r.garcia68@gmail.com" />
<title>Range Controller - Preferences - Member Card Creator</title>
<link rel="stylesheet" type="text/css" href="../../css/site_layout.css"/>
<link rel="stylesheet" type="text/css" href="../../tools/protoplasm_full.css"/>
<script lang="javascript" src="../../tools/protoplasm.js"></script>
<script lang="javascript">
Protoplasm.use('upload').transform('input.upload', {
'inline': true
});
</script>
<script>
// <![CDATA[  
function overlaySearch(){
el1 = document.getElementById("overlay");
el1.style.visibility = (el1.style.visibility == "visible") ? "hidden" : "visible";
el2 = document.getElementById("div_search");
el2.style.visibility = (el2.style.visibility == "visible") ? "hidden" : "visible";
if(el1.style.visibility=="hidden"||el2.style.visibility=="hidden"){}//location.reload();
}//end overlaySearch

function overlayPreview(){
var g = document.getElementById("h_gender").value;
document.getElementById("embedObject").src = "id-card-layout.php?g="+g;
//document.getElementById("embedObject").data = "id-card-layout.php?g="+g;

el1 = document.getElementById("overlay");
el1.style.visibility = (el1.style.visibility == "visible") ? "hidden" : "visible";
el2 = document.getElementById("div_preview");
el2.style.visibility = (el2.style.visibility == "visible") ? "hidden" : "visible";
el3 = document.getElementById("embedObject");
el3.style.display = (el3.style.display == "none") ? "none" : "block";

el4 = document.getElementById("lblLoadingPreview");
window.setTimeout(function(){
el4.style.visibility = "hidden";
},2005);
if(el4.style.visibility=="hidden"){location.reload();}//location.reload();
}//end overlayPreview

//START AJAX TO FILL SELECT WITH LIVE SEARCH
function populateSelect(ctrl_id,str){
if (str==""){document.getElementById("divSearchResults").innerHTML="";
return;
}
if (window.XMLHttpRequest){
// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else{
// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
document.getElementById("divSearchResults").innerHTML = xmlhttp.responseText;
}
}//end onreadystatechange
xmlhttp.open("GET","ajax-search-cust.php?ctrl_id="+ctrl_id+"&q="+str,true);
xmlhttp.send();
}//END AJAX TO FILL SELECT WITH LIVE SEARCH

function setCheckedPreferences(id,field_name){
if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}
else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
if(xmlhttp.responseText!="success"){alert("Error: setting "+field_name+" value. Please contact tech support.");}
else{autoSave(id);}
}//end if
}//end onreadystatechange
var chk = document.getElementById(id).checked;
if(chk==true){value="Yes";}else{value="No";}
xmlhttp.open("POST","ajax-set-preferences.php",false);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send("field_name="+field_name+"&value="+value);

autoRefreshPreview();
}//END AJAX setCheckedPreferences

function setText(id,field_name){
var ctrl = document.getElementById(id).value;
if(ctrl==""){alert("Required Input.");return;}
if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}
else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
if(xmlhttp.responseText!="success"){alert("Error: setting "+field_name+" value. Please contact tech support.");}
}//end if
}//end onreadystatechange
var value = document.getElementById(id).value;

switch(id){
case "rdMaleGender":{value = "male";}break;
case "rdFemaleGender":{value = "female";}break;
}//end switch

xmlhttp.open("POST","ajax-set-preferences.php",false);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send("field_name="+field_name+"&value="+value);

autoRefreshPreview();
}//end AJAX setSlogan

function autoSave(id){

document.getElementById("lblAutoSave").innerHTML = "Saving...";

window.setTimeout(function(){
document.getElementById("lblAutoSave").innerHTML = "All data is now auto saved...";
},2000);

window.setTimeout(function(){
switch(id){
case "txtCompanySlogan":{setText('txtCompanySlogan','company_slogan');}break;
case "txtSpecialInstructions":{setText('txtSpecialInstructions','special_instructions');}break;
}//end switch
},2000);

}//end auto save

function setGender(id,field_name){
//document.getElementById("h_gender").value = gender;
setText(id,field_name);
autoSave(id);
}//end set gender

function autoRefreshPreview(){
//document.getElementById('ifrmPreview').contentWindow.location.reload(true);//for an iframe  
document.getElementById("ifrmPreview").innerHTML = document.getElementById("ifrmPreview").innerHTML;  
}
// ]]>
</script>
<style>
#overlay {
	visibility: hidden;
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background-color: rgba(0, 0, 0, 0.5);
	z-index: 10;
}
#div_search {
	visibility: hidden;
	width: 500px;
	height: 425px;
	position: fixed;
	top: 15%;
	left: 20%;
	background-color: #1e1e1e;
	border-radius: 5px;
	text-align: center;
	z-index: 11; /* 1px higher than the overlay layer */
	border-style: solid;
	border-color: #808080;
}
#div_preview {
	visibility: hidden;
	width: 400px;
	height: 365px;
	position: fixed;
	top: 20%;
	left: 35%;
	margin-top:50px;
	background-color: #1e1e1e;
	border-radius: 5px;
	text-align: center;
	z-index: 11; /* 1px higher than the overlay layer */
	border-style: solid;
	border-color: #808080;
}
#div_preview table tr td label {
	position: relative;
	top:90px;
}
#embedObject {
	display: none;
}
#aClose {
	position: relative;
	top:-10px;
}
</style>
<?php
include('../../includes/browser.php');
$b = new Browser;

switch($b->getBrowser()){
case "Safari":{
print '
<style>
#div_preview{
visibility: hidden;
width: 400px;
height: 360px;
position: fixed;
top: 20%; 
left: 40%;
margin-top:50px;
background-color: #1e1e1e;
border-radius: 5px;
text-align: center;
z-index: 11; /* 1px higher than the overlay layer */
border-style: solid;
border-color: #808080;
}

#div_preview table tr td label{
position: relative; top:100px;
}

#aClose{
position: relative; top:-20px;
}
</style>
';
}break;

case "Chrome":{

}break;

case "Internet Explorer":{
//print '<link rel="stylesheet" type="text/css" href="../../css/internet_explorer.css"/>';
}break;

case "Firefox":{
//print '<link rel="stylesheet" type="text/css" href="../../css/fire_fox.css"/>';
}break;

}//END SWITCH
?>
</head>
<body>
<div align="center"><img src="../../img/background.jpg" class="bg" /></div>
<table width="950" align="center">
  <tr>
    <td width="950" bgcolor="#000000"><img src="../../img/logo.png" width="400" /></td>
  </tr>
</table>
<table width="950" bgcolor="#1E1E1E" align="center">
  <tr>
    <td width="15%">&nbsp;</td>
    <td width="70%" valign="top" align="center"><font color="#fbb91f" size="6"><b>
      <?=$_SESSION['company_name'] ?>
      </b></font></td>
    <td width="15%">&nbsp;</td>
  </tr>
</table>
<!--<table width="950" align="center" bgcolor="#1E1E1E">
  <tr>
    <td width="30%" align="center"><input type="button" id="home" name="home" value="HOME" onClick="window.location = '../../configurations.php';"/></td>
    <td width="70%" align="right"><font size="5"><b>Preferences - Member Card Creator</b></font></td>
  </tr>
</table>-->
<table width="950" align="center" bgcolor="#1E1E1E">
  <tr>
    <td width="100%" align="center" valign="top"><div class="headermargb">
        <div class="breadcrumb"><a href="../../dashboard">Home </a> / <a href="../../configurations">Module Configurations </a> / <span>Member ID Card Creator </span></div>
        <div class="header_right">Member ID Card Creator</div>
      </div>
      <table width="100%">
        <tr>
          <td width="100%" align="center"><label id="lblAutoSave" style="color: red;">All data is auto saved, no need to press a save button</label></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="100%" bgcolor="#333333" align="center"><b>Step 1: Choose a layout</b></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="30%" align="center"><img src="cardexample1.jpg"/></td>
          <td width="30%" align="center"></td>
          <td width="30%" align="center"></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="100%" bgcolor="#333333" align="center"><b>Step 2: Insert data from client file</b></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="30%"><?php
                if($client_name=="Yes"){
                print '<input type="checkbox" id="chkClientName" onclick="setCheckedPreferences(this.id,\'client_name\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientName" onclick="setCheckedPreferences(this.id,\'client_name\');"/>';
                }
                ?>
            &nbsp;Client Name (First, Mi, Last) </td>
          <td width="30%"><?php
                if($client_picture=="Yes"){
                print '<input type="checkbox" id="chkClientPicture" onclick="setCheckedPreferences(this.id,\'client_picture\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientPicture" onclick="setCheckedPreferences(this.id,\'client_picture\');"/>';
                }
                ?>
            &nbsp;Client Picture </td>
          <td width="30%"><?php
                if($client_address=="Yes"){
                print '<input type="checkbox" id="chkClientAddress" onclick="setCheckedPreferences(this.id,\'client_address\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientAddress" onclick="setCheckedPreferences(this.id,\'client_address\');"/>';
                }
                ?>
            &nbsp;Client Address</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="30%"><?php
                if($client_dob=="Yes"){
                print '<input type="checkbox" id="chkClientDOB" onclick="setCheckedPreferences(this.id,\'client_dob\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientDOB" onclick="setCheckedPreferences(this.id,\'client_dob\');"/>';
                }
                ?>
            &nbsp;Client Date of Birth </td>
          <td width="30%"><?php
                if($client_certs=="Yes"){
                print '<input type="checkbox" id="chkClientCerts" onclick="setCheckedPreferences(this.id,\'client_certs\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientCerts" onclick="setCheckedPreferences(this.id,\'client_certs\');"/>';
                }
                ?>
            &nbsp;Client Certifications</td>
          <td width="30%"><?php
                if($client_emergency_contact=="Yes"){
                print '<input type="checkbox" id="chkClientEmergencyContact" onclick="setCheckedPreferences(this.id,\'client_emergency_contact\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientEmergencyContact" onclick="setCheckedPreferences(this.id,\'client_emergency_contact\');"/>';
                }
                ?>
            &nbsp;Client Emergency Contact</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="30%"><?php
                if($client_membership_type=="Yes"){
                print '<input type="checkbox" id="chkClientMembershipType" onclick="setCheckedPreferences(this.id,\'client_membership_type\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientMembershipType" onclick="setCheckedPreferences(this.id,\'client_membership_type\');"/>';
                }
                ?>
            &nbsp;Membership Type </td>
          <td width="30%">&nbsp;</td>
          <td width="30%">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="100%" bgcolor="#333333" align="center"><b>Step 3: Insert data from company file</b></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="30%"><?php
                if($client_membership_number=="Yes"){
                print '<input type="checkbox" id="chkClientMembershipNumber" onclick="setCheckedPreferences(this.id,\'membership_number\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientMembershipNumber" onclick="setCheckedPreferences(this.id,\'membership_number\');"/>';
                }
                ?>
            &nbsp;Show Membership Number</td>
          <td width="30%"><?php
                if($barcode=="Yes"){
                print '<input type="checkbox" id="chkBarcode" onclick="setCheckedPreferences(this.id,\'barcode\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkBarcode" onclick="setCheckedPreferences(this.id,\'barcode\');"/>';
                }
                ?>
            &nbsp;Add Generated Barcode</td>
          <td width="30%"><?php
                if($company_name=="Yes"){
                print '<input type="checkbox" id="chkCompanyName" onclick="setCheckedPreferences(this.id,\'company_name\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkCompanyName" onclick="setCheckedPreferences(this.id,\'company_name\');"/>';
                }
                ?>
            &nbsp;Company Name</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="30%"><?php
                if($company_logo=="Yes"){
                print '<input type="checkbox" id="chkCompanyLogo" onclick="setCheckedPreferences(this.id,\'company_logo\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkCompanyLogo" onclick="setCheckedPreferences(this.id,\'company_logo\');"/>';
                }
                ?>
            &nbsp;Company Logo
            <!-- 
                  <ul>                    
                <?php
                
                if($use_logo_as_background=="Yes"){
                print '<input type="checkbox" id="chkBlnUseCompanyLogo" onclick="setCheckedPreferences(this.id,\'use_logo_for_background\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkBlnUseCompanyLogo" onclick="setCheckedPreferences(this.id,\'use_logo_for_background\');"/>';
                }
                
                ?>
                  &nbsp;Use logo as background
                  </ul>
               --></td>
          <td width="30%"><?php
                if($company_address=="Yes"){
                print '<input type="checkbox" id="chkCompanyAddress" onclick="setCheckedPreferences(this.id,\'company_address\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkCompanyAddress" onclick="setCheckedPreferences(this.id,\'company_address\');"/>';
                }
                ?>
            &nbsp;Company Address</td>
          <td width="30%"><?php
                if($company_phone_number=="Yes"){
                print '<input type="checkbox" id="chkCompanyPhoneNumber" onclick="setCheckedPreferences(this.id,\'company_phone_number\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkCompanyPhoneNumber" onclick="setCheckedPreferences(this.id,\'company_phone_number\');"/>';
                }
                ?>
            &nbsp;Company Phone Number</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="30%"><?php
                if($use_company_slogan=="Yes"){
                print '<input type="checkbox" id="chkUseCompanySlogan" onclick="setCheckedPreferences(this.id,\'use_company_slogan\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkUseCompanySlogan" onclick="setCheckedPreferences(this.id,\'use_company_slogan\');"/>';
                }
                ?>
            &nbsp;Use Company Slogan<br />
            <textarea id="txtCompanySlogan" cols="30" onKeyUp="autoSave('txtCompanySlogan');"><?=$company_slogan?>
</textarea></td>
          <td width="30%"><?php
                if($use_special_instructions=="Yes"){
                print '<input type="checkbox" id="chkUseSpecialInstructions" onclick="setCheckedPreferences(this.id,\'use_special_instructions\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkUseSpecialInstructions" onclick="setCheckedPreferences(this.id,\'use_special_instructions\');"/>';
                }
                ?>
            &nbsp;Use Special Instructions<br />
            <textarea id="txtSpecialInstructions" cols="30" onKeyUp="autoSave('txtSpecialInstructions');"><?=$special_instructions?>
</textarea></td>
          <td width="30%"><?php
                if($membership_border_banner=="Yes"){
                print '<input type="checkbox" id="chkMembershipBorderBanner" onclick="setCheckedPreferences(this.id,\'membership_border_banner\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkMembershipBorderBanner" onclick="setCheckedPreferences(this.id,\'membership_border_banner\');"/>';
                }
                ?>
            &nbsp;Add Border to Membership rank and client image</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="30%"><?php
                if($shooter_portal_text=="Yes"){
                print '<input type="checkbox" id="chkShooterPortalText" onclick="setCheckedPreferences(this.id,\'shooter_portal_text\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkShooterPortalText" onclick="setCheckedPreferences(this.id,\'shooter_portal_text\');"/>';
                }
                ?>
            &nbsp;Use Rangecontroller Shooter Portal</td>
          <td width="30%"><?php
                if($shooter_login_and_password=="Yes"){
                print '<input type="checkbox" id="chkShooterLoginAndPassword" onclick="setCheckedPreferences(this.id,\'shooter_login_and_password\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkShooterLoginAndPassword" onclick="setCheckedPreferences(this.id,\'shooter_login_and_password\');"/>';
                }
                ?>
            &nbsp;Print Shooter Portal login and password at base of the card</td>
          <td width="30%">&nbsp;</td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="100%" bgcolor="#333333" align="center"><b>Step 4: ID Card Preview</b></td>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="100%" align="center"><div id="ifrmPreview">
              <object width="500" height="300" type="application/pdf" data="id-card-layout.php" id="ifrmPreview_obj">
                <p style="color: red;">PDF cannot be displayed. Please use Chrome or Safari for best results.</p>
              </object>
            </div></td>
        </tr>
      </table>
      <br></td>
  </tr>
</table>
<?php
include("../../includes/footer.php");
?>
</body>
</html>
