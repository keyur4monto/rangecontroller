<?php
session_start();
// upload.php - File upload handler for "upload" control
//$filestore = dirname(__FILE__).'/filestore';
$filestore = "/var/www/vhosts/rangecontroller.com/xapp/tools/idcreator";

if ($_SERVER['HTTP_CONTENT_DISPOSITION']) {
$disposition = explode('; filename=', $_SERVER['HTTP_CONTENT_DISPOSITION']);

$postin = fopen('php://input', 'r');
$fileout = fopen($filestore.'/'.$disposition[1], 'w');

while ($chunk = fread($postin, 4096)) {
fwrite($fileout, $chunk);
}

fclose($fileout);
fclose($postin);

} else {

// Regular file post
$tmp = $_FILES['files']['tmp_name'][0];
$file = $_FILES['files']['name'][0];
rename($tmp, $filestore.'/'.$file);
}
?>