function formatFirstName(ctrl) {
val = document.getElementById(ctrl).value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length) + ' ';
}
document.getElementById(ctrl).value = newVal;
}//end function

function formatMiddleName(ctrl) {
val = document.getElementById(ctrl).value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length) + ' ';
}
document.getElementById(ctrl).value = newVal;
}//end function

function formatLastName(ctrl) {
val = document.getElementById(ctrl).value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length) + ' ';
}
document.getElementById(ctrl).value = newVal;
}//end function

function formatStreetAddress(ctrl) {
val = document.getElementById(ctrl).value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length) + ' ';
}
document.getElementById(ctrl).value = newVal;
}//end function

function formatCity(ctrl) {
val = document.getElementById(ctrl).value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length) + ' ';
}
document.getElementById(ctrl).value = newVal;
}//end function

function formatZipcode(ctrl){
var zip = document.getElementById(ctrl).value;  
if (zip.match(/^[0-9]{5}$/)) {
document.getElementById(ctrl).style.backgroundColor = "white";
document.getElementById(ctrl).style.color = "black";
return true;
}
zip = zip.toUpperCase();
if (zip.match(/^[A-Z][0-9][A-Z][0-9][A-Z][0-9]$/)) {
document.getElementById(ctrl).style.color = "black";
document.getElementById(ctrl).style.backgroundColor = "white";

return true;
}
if (zip.match(/^[A-Z][0-9][A-Z].[0-9][A-Z][0-9]$/)) {
document.getElementById(ctrl).style.color = "black";
document.getElementById(ctrl).style.backgroundColor = "white";
return true;
}
document.getElementById(ctrl).value = "Invalid Zipcode";
document.getElementById(ctrl).style.backgroundColor = "orange";
return false;
}//end function

function formatPhoneNumber(input,ctrl_name){
if (input == null) {return null}
//
// Strip all non-numeric characters and check for '\'...
//
var plain = "";
for(var i = 0;i < input.length; i++){
   var c = input.substring(i, i+1);
   if(!isNaN(c) && c != " " || !isNaN(c) && c != "\\" )
      {
      plain += c;
      }
}
//
// Make sure we have 10 numeric digits
//
var tenNumeric = /^[0-9]{10}$/;
if(tenNumeric.test(plain))
   {
   document.getElementById(ctrl_name).value =  "("
   + plain.substring(0, 3) 
   + ") "
   + plain.substring(3, 6) 
   + "-"
   + plain.substring(6, 10);
   document.getElementById(ctrl_name).style.color = "black";
   document.getElementById(ctrl_name).style.backgroundColor = "white";
    }
else
   {   
   //alert("'" + input + "'" + " ~ Invalid Format\n\nJust type the number with no symbols with area code.\n\nMake sure you type in 10 numbers.\n\nex, 2108539255");
   document.getElementById(ctrl_name).value = "Invalid Phone Number";
   document.getElementById(ctrl_name).style.backgroundColor = "orange";
   }   
}//end function

function validateEmail(ctrl){ 
var e = document.getElementById(ctrl).value;   
if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e)){
document.getElementById(ctrl).style.color = "black";
document.getElementById(ctrl).style.backgroundColor = "white"; 
return (true);   
}  
//alert("You have entered an invalid email address!") 
document.getElementById(ctrl).value = "Invalid Email Address";
document.getElementById(ctrl).style.backgroundColor = "orange";
return (false);  
}//end function