//wed may 29, 2013

/*
june 1
-working on a method to clear div contents while in add customer mode
-right now the div does not work properly, as in for example, i coose gold, then choose
-platinum, the div doesn't clear out gold to show the proper info of platinum
ele.innerHTML="";
lblBenefitType
*/

function checkBenefit(benefit_type){
hideBenefits();
alert("Regarding the "+benefit_type+" Benefits Section\n\nThe "+benefit_type+" benefits section will show up upon saving this customer first.");
}//end function

function enableRequiredFields(type){
//var h_membership_type = document.getElementById("h_membership_type").value;
if(type=="Law Enforcement"||type=="Visitor"){
document.getElementById("lblBillingRequired").innerHTML = '';
}else{
document.getElementById("lblBillingRequired").innerHTML = '* Billing Info. Required';
}
return;
}//end function

function checkIfAddingNewCust(benefit_type){
document.getElementById("h_membership_type").value = "";
document.getElementById("h_membership_type").value = benefit_type;
enableRequiredFields(benefit_type);
//clear variable first
//document.getElementById("membership_type").value = "";
//set hidden variable
//document.getElementById("membership_type").value = benefit_type;
var doc_url = document.URL;
//operation=1 (means: add new customer) is what we're looking for
//substring customized for the following url only
//https://rangecontroller.com/".RANGE_FOLDER(3 letter folder name)."/customer.php?s=1&mn=&operation=1
var chk_operation_type = doc_url.substring(53,64);//operation=1 is 11 characters long
//alert(chk_operation_type);
if(chk_operation_type=="operation=1"){

switch(benefit_type){
case "Gold":{checkBenefit("Gold");}break;  
case "Platinum":{checkBenefit("Platinum");}break;  
case "Keystone Platinum":{checkBenefit("Keystone Platinum");}break;   
}//end switch

}else{
showBenefits(benefit_type);
}//end if
}//end function

function hideBenefits(){ 
document.getElementById("divBenefitsTable").style.display = "none";    
}//end function

function showBenefits(benefit_type){     
var ele = document.getElementById("divBenefitsTable");
if(benefit_type=="Gold"||benefit_type=="Platinum"||benefit_type=="Keystone Platinum") {
ele.style.display = "block";
}else{
ele.style.display = "none";
}//end if
switch(benefit_type){
case "Gold":{populateGoldBenefits();}break;  
case "Platinum":{populatePlatinumBenefits();}break;  
case "Keystone Platinum":{populateKeystonePlatinumBenefits();}break;   
}//end switch
}//end function 

function dropDownLabels(){
//dropdown control labels
document.getElementById('lblExtendedRangeHours').innerHTML = "Extended Range Hours";
document.getElementById('lblGuestPasses').innerHTML = "Guest Passes";
document.getElementById('lblFirearmTransfers').innerHTML = "Firearm Transfers";
document.getElementById('lblFirearmRentals').innerHTML = "Firearm Rentals";
document.getElementById('lblMachineGunRentals').innerHTML = "Machine Gun Rentals";
}//end function

function populateGoldBenefits() { 
var goldExtendedRangeHours = "0"; 
var goldGuestPasses        = ["", "1", "2"];
var goldFirearmTransfers   = ["", "1"];  
var goldFirearmRentals     = ["", "1"]; 
var goldMachineGunRentals  = ["", "1"]; 

var arLen2=goldGuestPasses.length;
var arLen3=goldFirearmTransfers.length;
var arLen4=goldFirearmRentals.length;
var arLen5=goldMachineGunRentals.length;

document.getElementById('h_hasBenefits').value = true;
dropDownLabels();

//number of benefits for each benefit 
document.getElementById('lblRemoveForGold').innerHTML = "";
document.getElementById('lblNumOfExtendedRangehoursBenefits').innerHTML = "Not Available for Gold";
document.getElementById('lblNumOfExtendedRangehoursBenefits').style.color = "red";

document.getElementById('selExtendedRangeHours').value = goldExtendedRangeHours;
document.getElementById('selExtendedRangeHours').disabled = true;

for(var i=1; i<arLen2; i++){
document.getElementById('selGuestPasses').options[i]= new Option(goldGuestPasses[i], i);
}//end for loop
for(var i=1; i<arLen3; i++){
document.getElementById('selFirearmTransfers').options[i]= new Option(goldFirearmTransfers[i], i);
}//end for loop
for(var i=1; i<arLen4; i++){
document.getElementById('selFirearmRentals').options[i]= new Option(goldFirearmRentals[i], i);
}//end for loop
for(var i=1; i<arLen5; i++){
document.getElementById('selMachineGunRentals').options[i]= new Option(goldMachineGunRentals[i], i);
}//end for loop
}//end function 

//****************************************************************

function populateKeystonePlatinumBenefits() { 
var keystonePlatinumExtendedRangeHours = ["", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"]; 
var keystonePlatinumGuestPasses        = ["", "1", "2", "3", "4", "5", "6", "7", "8"];
var keystonePlatinumFirearmTransfers   = ["", "1", "2", "3", "4"];  
var keystonePlatinumFirearmRentals     = ["", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]; 
var keystonePlatinumMachineGunRentals  = ["", "1", "2", "3", "4"]; 

var arLen1=keystonePlatinumExtendedRangeHours.length;
var arLen2=keystonePlatinumGuestPasses.length;
var arLen3=keystonePlatinumFirearmTransfers.length;
var arLen4=keystonePlatinumFirearmRentals.length;
var arLen5=keystonePlatinumMachineGunRentals.length;

document.getElementById('h_hasBenefits').value = true;
dropDownLabels();
document.getElementById('lblNumOfExtendedRangehoursBenefits').style.color = "white";
//gold disables this ctrl, so here we ensure its enabled
document.getElementById('selExtendedRangeHours').disabled = false;

for(var i=1; i<arLen1; i++){
document.getElementById('selExtendedRangeHours').options[i]= new Option(keystonePlatinumExtendedRangeHours[i], i);
}//end for loop
for(var i=1; i<arLen2; i++){
document.getElementById('selGuestPasses').options[i]= new Option(keystonePlatinumGuestPasses[i], i);
}//end for loop
for(var i=1; i<arLen3; i++){
document.getElementById('selFirearmTransfers').options[i]= new Option(keystonePlatinumFirearmTransfers[i], i);
}//end for loop
for(var i=1; i<arLen4; i++){
document.getElementById('selFirearmRentals').options[i]= new Option(keystonePlatinumFirearmRentals[i], i);
}//end for loop
for(var i=1; i<arLen5; i++){
document.getElementById('selMachineGunRentals').options[i]= new Option(keystonePlatinumMachineGunRentals[i], i);
}//end for loop
}//end function 

//****************************************************************

function populatePlatinumBenefits() {  
var platinumExtendedRangeHours = ["", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]; 
var platinumGuestPasses        = ["", "1", "2", "3", "4", "5", "6"];
var platinumFirearmTransfers   = ["", "1", "2"];  
var platinumFirearmRentals     = ["", "1", "2", "3", "4"]; 
var platinumMachineGunRentals  = ["", "1", "2"]; 

var arLen1=platinumExtendedRangeHours.length;
var arLen2=platinumGuestPasses.length;
var arLen3=platinumFirearmTransfers.length;
var arLen4=platinumFirearmRentals.length;
var arLen5=platinumMachineGunRentals.length;

document.getElementById('h_hasBenefits').value = true;
dropDownLabels();

document.getElementById('lblNumOfExtendedRangehoursBenefits').style.color = "white";
document.getElementById('selExtendedRangeHours').disabled = false;

for(var i=1; i<arLen1; i++){
document.getElementById('selExtendedRangeHours').options[i]= new Option(platinumExtendedRangeHours[i], i);
}//end for loop
for(var i=1; i<arLen2; i++){
document.getElementById('selGuestPasses').options[i]= new Option(platinumGuestPasses[i], i);
}//end for loop
for(var i=1; i<arLen3; i++){
document.getElementById('selFirearmTransfers').options[i]= new Option(platinumFirearmTransfers[i], i);
}//end for loop
for(var i=1; i<arLen4; i++){
document.getElementById('selFirearmRentals').options[i]= new Option(platinumFirearmRentals[i], i);
}//end for loop
for(var i=1; i<arLen5; i++){
document.getElementById('selMachineGunRentals').options[i]= new Option(platinumMachineGunRentals[i], i);
}//end for loop
}//end function 

//****************************************************************

function setExtendedRangeHours(extended_hours){      
var userInput = (parseInt(extended_hours) + parseInt( document.getElementById("lblUsedExtendedRangeHours").innerHTML));
if(userInput > document.getElementById("h_extendedRangeHours_total").value){
document.getElementById('lblStatusMessage').innerHTML ="----";
document.getElementById('lblStatusMessage2').innerHTML ="----";
alert("You are exceeding the limit for your extended range hours, Please try again.");
return;
}else{
document.getElementById('lblStatusMessage').innerHTML = "You chose "+extended_hours+" extended range hours, Press save when your ready to confirm your selection.";
document.getElementById('lblStatusMessage2').innerHTML = document.getElementById('lblStatusMessage').innerHTML;
} 
document.getElementById('h_ExtendedRangeHours').value = extended_hours;
document.getElementById('h_extendedRangeHours_changed').value = true;
document.getElementById('h_hasBenefits').value = true;
}//end function

function setGuestPasses(guest_passes){
var userInput = (parseInt(guest_passes) + parseInt( document.getElementById("lblUsedGuestPasses").innerHTML));
if(userInput > document.getElementById("h_guestPasses_total").value){
document.getElementById('lblStatusMessage').innerHTML ="----";
document.getElementById('lblStatusMessage2').innerHTML ="----";
alert("You are exceeding the limit for your guest passes, Please try again.");
return;
}else{
document.getElementById('lblStatusMessage').innerHTML = "You chose "+guest_passes+" guest passes, Press save when your ready to confirm your selection.";
document.getElementById('lblStatusMessage2').innerHTML = document.getElementById('lblStatusMessage').innerHTML;
}//end if 
document.getElementById('h_GuestPasses').value = guest_passes; 
document.getElementById('h_guestPasses_changed').value = true;
document.getElementById('h_hasBenefits').value = true;
}//end function

function setFirearmTransfers(transfers){
var userInput = (parseInt(transfers) + parseInt( document.getElementById("lblUsedFirearmTransfers").innerHTML));
if(userInput > document.getElementById("h_firearmTransfers_total").value){
document.getElementById('lblStatusMessage').innerHTML ="----";
document.getElementById('lblStatusMessage2').innerHTML ="----";
alert("You are exceeding the limit for your firearm transfers, Please try again.");
return;
}else{
document.getElementById('lblStatusMessage').innerHTML = "You chose "+transfers+" firearm transfers, Press save when your ready to confirm your selection.";
document.getElementById('lblStatusMessage2').innerHTML = document.getElementById('lblStatusMessage').innerHTML;
}
document.getElementById('h_FirearmTransfers').value = transfers;
document.getElementById('h_firearmTransfers_changed').value = true;
document.getElementById('h_hasBenefits').value = true;
}//end function

function setFirearmRentals(rentals){
var userInput = (parseInt(rentals) + parseInt( document.getElementById("lblUsedFirearmRentals").innerHTML));
if(userInput > document.getElementById("h_firearmRentals_total").value){
document.getElementById('lblStatusMessage').innerHTML ="----";
document.getElementById('lblStatusMessage2').innerHTML ="----";
alert("You are exceeding the limit for your firearm rentals, Please try again.");
return;
}else{
document.getElementById('lblStatusMessage').innerHTML = "You chose "+rentals+" firearm rentals, Press save when your ready to confirm your selection.";
document.getElementById('lblStatusMessage2').innerHTML = document.getElementById('lblStatusMessage').innerHTML;
}    
document.getElementById('h_FirearmRentals').value = rentals; 
document.getElementById('h_firearmRentals_changed').value = true;
document.getElementById('h_hasBenefits').value = true;
}//end function

function setMachineGunRentals(rentals){
var userInput = (parseInt(rentals) + parseInt( document.getElementById("lblUsedMachineGunRentals").innerHTML));
if(userInput > document.getElementById("h_machineGunRentals_total").value){
document.getElementById('lblStatusMessage').innerHTML ="----";
document.getElementById('lblStatusMessage2').innerHTML ="----";
alert("You are exceeding the limit for your machine gun rentals, Please try again.");
return;
}else{
document.getElementById('lblStatusMessage').innerHTML = "You chose "+rentals+" machine gun rentals, Press save when your ready to confirm your selection.";
document.getElementById('lblStatusMessage2').innerHTML = document.getElementById('lblStatusMessage').innerHTML;
}      
document.getElementById('h_MachineGunRentals').value = rentals;
document.getElementById('h_machineGunRentals_changed').value = true;
document.getElementById('h_hasBenefits').value = true;
}//end function

function submitBenefitsPopup(){
document.getElementById("frmBenefitInterfacePopup").submit();   
}//end function

function addExtendedRangeHourBenefits(total){   
document.getElementById("lblUsedExtendedRangeHours").innerHTML = total;   
}//end function

function addGuestPassesBenefits(total){   
document.getElementById("lblUsedGuestPasses").innerHTML = total;   
}//end function

function addFirearmTransfersBenefits(total){   
document.getElementById("lblUsedFirearmTransfers").innerHTML = total;   
}//end function

function addFirearmRentalsBenefits(total){   
document.getElementById("lblUsedFirearmRentals").innerHTML = total;   
}//end function

function addMachineGunRentalsBenefits(total){   
document.getElementById("lblUsedMachineGunRentals").innerHTML = total;   
}//end function