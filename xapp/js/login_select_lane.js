//////////////////////////////////////////START SELECT LANE PAGE FUNCTIONS**************************

function logOut(member_number){
//prompt the user
var result = confirm('Are you sure you want to logout member number '+member_number+' ?\n\nThis action is immediate and permanent ');
if ( result ){ //if user clicked ok
//redirect to url 
window.location = 'login_select_lane.php?s=1&eteled=logout_member&mn=' + member_number;
}     
}//end function

function logOut_CurrentUsage(member_number,member){
//prompt the user
var result = confirm('Are you sure you want to logout member\n\n'+member+'\n\nThis action is immediate and permanent ');
if ( result ){ //if user clicked ok
//redirect to url 
//window.location = 'current_range_usage.php';
window.location = 'current_range_usage.php?s=1&eteled=logout_member&mn=' + member_number;
}     
}//end function

//disable the return key and browser back button
function stopRKey(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) { return false; }
}//end function



//////////////////////////////////////////END SELECT LANE PAGE FUNCTIONS**************************
//////////////////////////////////////////START CONFIG PAGE FUNCTIONS**************************
function confirmSettings(){
var result = confirm("[ " + document.getElementById("lbl_number_of_lanes").innerHTML + " ] lanes will be set for the range.");    
var h_number_of_lanes = document.getElementById("h_number_of_lanes").value;
//if(result){window.location = 'https://rangecontroller.com/".RANGE_FOLDER."/login_select_lane.php?nol='+h_number_of_lanes;}
//else{return;}
}//end function

function refreshSettings(){//onblur
var lbl = document.getElementById("lbl_number_of_lanes"); 
var lanes = document.getElementById("number_of_lanes");//setLanes
var h_number_of_lanes = document.getElementById("h_number_of_lanes");

if(lanes.value < 0 || lanes.value == 0){alert("Enter valid numbers 1-10");return;}

if(lanes.value == lbl.innerHTML){alert("Input value and current number of lanes is the same.");return;}
if(lanes.value > 10 || lanes.value == ""){
    //document.getElementById("lbl_number_of_lanes").innerHTML = 0;
    document.getElementById("number_of_lanes").value = "";
    document.getElementById("h_number_of_lanes").value = "";    
    alert("10 lanes are the limit, Please ckeck your values");    
    return;
}
h_number_of_lanes.value = document.getElementById("number_of_lanes").value;
document.getElementById("lbl_number_of_lanes").innerHTML = h_number_of_lanes.value;
//document.getElementById("h_number_of_lanes").value = h_number_of_lanes.value;  
confirmBtnStatus(false);
refreshBtnStatus(true); 
}//end function

function confirmBtnStatus(val){
document.getElementById("btn_confirm").disabled = val;   
}//end function

function refreshBtnStatus(val){
document.getElementById("btn_refresh_settings").disabled = val;   
}//end function
//////////////////////////////////////////END CONFIG PAGE FUNCTIONS**************************



