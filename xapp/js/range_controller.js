//////////////////////////////////////////START CUSTOMER INFO PAGE FUNCTIONS**************************
//alert("in js file...");
//alert(document.URL);

function openFileOption(){document.getElementById("file1").click();}//end function

function initCalendarNO_LONGER_USED() {
calendar.set("membership_expires");
calendar.set("driver_license_expires");
calendar.set("driver_license_birthdate");
calendar.set("billing_date");
}

function deleteServerFile(val_file,val_mn){
if(val_file==""){return;}
var result = confirm("CAUTION: Operation is immediate and permanent.\n\nPress OK to delete [  "+val_file+"  ] or press Cancel."); 
if(result==true){
document.getElementById("sel_delete_file").value="";    
window.location = "webcam/delete_server_file.php?fn="+val_file+"&m="+val_mn;}
else{document.getElementById("sel_delete_file").value="";return;} 
}//end function

function memberLoggedIn(lane){
if(lane!=0){
document.getElementById("LoggedIntoRange").innerHTML = "<center><font color='red'><b>Member is logged in on Lane "+lane+"</b></font></center>";    
document.getElementById("logout_range").disabled = false;
document.getElementById("login_range").disabled = true;
return;
}
document.getElementById("LoggedIntoRange").innerHTML = "";
document.getElementById("logout_range").disabled = true;
//document.getElementById("login_range").disabled = false;  
}//end function

function getRangeTime(){//
var v = sel_range_time_by.options[sel_range_time_by.selectedIndex].value;
switch(v){
case '1':{document.getElementById('total_range_time').value = document.getElementById('h_today').value;document.getElementById('lblLabel').innerHTML="Total Range Time Today ";}break; 
case '2':{document.getElementById('total_range_time').value = document.getElementById('h_week').value;document.getElementById('lblLabel').innerHTML="Total Range Time for Week to Date";}break; 
case '3':{document.getElementById('total_range_time').value = document.getElementById('h_month').value;document.getElementById('lblLabel').innerHTML="Total Range Time for Month to Date";}break; 
case '4':{document.getElementById('total_range_time').value = document.getElementById('h_year').value;document.getElementById('lblLabel').innerHTML="Total Range Time for Year to Date";}break;   
}//end switch   
}//end getRangeTime   


function getRangeTimeSecondVersion(val_mn){//this function is not being used, delete 
var rtb = sel_range_time_by.options[sel_range_time_by.selectedIndex].value;
var rt = document.getElementById("total_range_time");
var label = "";
switch(rtb){
case '1':{label = "Today";}break; 
case '2':{label = "This Week";}break; 
case '3':{label = "This Month";}break; 
case '4':{label = "This Year";}break;    
}//end switch
rt.value = document.getElementById("h_range_time_by").value;
window.location = 'https://rangecontroller.com/xapp/customer.php?s=1&operation=2&label='+label+'&sortby='+rtb+'&mn='+val_mn;
}//end function

function enterMsg(){//onclick
var dlc = document.getElementById("driver_license_comments");
if(dlc.value == "Enter any comments here..."){dlc.value = "";}
}//end function

function chkMsg(){//onblur  
var dlc = document.getElementById("driver_license_comments");
if(dlc.value == ""){
document.getElementById("driver_license_comments").value = "Enter any comments here...";     
}//end if    
}//end function

function custSearch(){
var cs = cust_search.options[cust_search.selectedIndex].value;
document.getElementById("h_cust_search").value = cs;
if(h_cust_search == ""){alert("Please select a customer to search for.");return;}
//alert(cs);
}//end function

function disableCustCtrls(){ 
var inputs = document.getElementsByTagName("input");
//this clears the membership_number ctrl, but value is still in h_unique_member
//val variable is only for this purpose

//document.getElementById("membership_number").value = "";
//document.getElementById("membership_date").value = "";

//document.getElementById("membership_expires").value = "";
for(var i = 0; i < inputs.length; i++){
if(inputs[i].type == 'text')
inputs[i].disabled = true;        
};
var selects = document.getElementsByTagName("select");
for (var i = 0; i < selects.length; i++) {
selects[i].disabled = true;
}
var textareas = document.getElementsByTagName("textarea");
for (var i = 0; i < textareas.length; i++) {       
textareas[i].disabled = true;
}
document.getElementById("membership_expires").disabled = false;
document.getElementById("membership_expires").disabled = false;

document.getElementById("add").disabled = false;

//re-enable after fixing edit issue...
//document.getElementById("edit").disabled = true;
//document.getElementById("delete").disabled = true;
//document.getElementById("save").disabled = true;
//document.getElementById("cancel").disabled = true;
     
}//end function

function enableLogin(){
document.getElementById("login_range").disabled = true;
}//end function

function enableCustCtrls(op){    
var inputs = document.getElementsByTagName("input"); 
for(var i = 0; i < inputs.length; i++){
//if(inputs[i].id == "login_range"){continue;} 
//if(inputs[i].id == "logout_range"){continue;} 
if(inputs[i].type == 'text') 
       
inputs[i].value = "";
inputs[i].disabled = false;
}
var selects = document.getElementsByTagName("select");
for (var i = 0; i < selects.length; i++) 
{   
selects[i].value = ""; 
selects[i].disabled = false; 
}
var textareas = document.getElementsByTagName("textarea");
for (var i = 0; i < textareas.length; i++) 
{ 
textareas[i].value = "";
textareas[i].disabled = false;
}    
document.getElementById("cust_search").disabled = true;
document.getElementById("search").disabled = true;
document.getElementById("edit").disabled = true;     
document.getElementById("add").disabled = true;
document.getElementById("delete").disabled = true;    
document.getElementById("save").disabled = false; 
//document.getElementById("logout_range").disabled = true; 
if(op == "add"){
/*
//this code has leading zeros    
var currentTime1 = new Date();
var month1 = currentTime1.getMonth() + 1;
var day1 = currentTime1.getDate();
var year1 = currentTime1.getFullYear();
var dt_str1 = month1 + "/" + day1 + "/" + year1;
*/
var myDate = new Date();
var year = myDate.getFullYear();
var month = myDate.getMonth();
if(month <= 9){month = '0'+month;}
var day= myDate.getDate();
if(day <= 9){day = '0'+day;}
var dt_str1 = day +'/'+ month +'/'+ year;
document.getElementById("membership_date").value = dt_str1;

/*
var currentTime2 = new Date();
var month2 = currentTime2.getMonth() + 1;
var day2 = currentTime2.getDate() + 1;
var year2 = currentTime2.getFullYear() + 1;
var dt_str2 = month2 + "/" + day2 + "/" + year2;
document.getElementById("membership_expires").value = dt_str2;
*/

document.getElementById("membership_number").disabled = true;

//here we generate the unique membership number for each customer 
//document.getElementById("membership_number").value = document.getElementById("h_unique_member").value; 

}//end if    
}//end function

function validateCustForm(payment_required){//required fields
var h_membership_type = document.getElementById("h_membership_type").value;
//this function is for saving and updating data to the db
if(document.getElementById("save").value == "UPDATE"){

if(requiredFields()){return;}//this is for updating, saving would continue on
document.getElementById("h_update_data").value = "TRUE";
confirmToAddStudent();
document.getElementById("frmCustomer").submit(); 
return;
}else{document.getElementById("h_update_data").value = "FALSE";}
//hidden field value is set to false
document.getElementById("h_saving_data").value = "FALSE";
//check required fields
var mst = document.getElementById("membership_type").value;
var fn = document.getElementById("first_name").value;
var ln = document.getElementById("last_name").value;
var sa = document.getElementById("street_address").value;
var ci = document.getElementById("city").value;
var st = document.getElementById("state").value;
var zi = document.getElementById("zipcode").value;
var p1 = document.getElementById("home_phone").value;
var em = document.getElementById("email").value;

var blnShow = false;//
//if(h_membership_type=="Law Enforcement"||h_membership_type=="Visitor"||document.getElementById("save").value=="UPDATE"){;
if(payment_required==false || document.getElementById("save").value=="UPDATE"){;
}else{
var BDate = document.getElementById("billing_date").value;
var BPaymentMethod = document.getElementById("h_billing_payment_method").value;
var BPaymentAmount = document.getElementById("billing_payment_amount").value;
blnShow = true;
}

//create message if something is missing   
var msg = "";
if(mst == ""){msg += "Required Membership Type\n";}
if(fn == ""){msg += "Required First Name\n";}
if(ln == ""){msg += "Required Last Name\n";}
if(sa == ""){msg += "Required Street Address\n";}
if(ci == ""){msg += "Required City\n";}  
if(st == ""){msg += "Required State\n";} 
if(zi == ""){msg += "Required Zipcode\n";} 
if(p1 == ""){msg += "Required Phone 1\n";} 
if(em == ""){msg += "Required Email\n";} 

if(blnShow){
if(BDate == ""){msg += "Required Billing Date\n";} 
if(BPaymentMethod == ""){msg += "Required Payment Method\n";} 
if(BPaymentAmount == ""){msg += "Required Payment Amount\n";} 
}//end if

if(msg != ""){alert(msg);return;}

if(document.getElementById("h_class_id").value!=""){
var driver_license = document.getElementById("driver_license_number").value;
var emergency_name = document.getElementById("emergency_contact_name").value;
var emergency_phone = document.getElementById("emergency_contact_phone").value;
var student_msg = "";

if(driver_license=="") {student_msg = "Required Driver License (student section)\n";}
if(emergency_name==""){student_msg += "Required Emeregency Contact Name (student section)\n";}
if(emergency_phone==""){student_msg += "Required Emeregency Phone (student section)\n";}

if(student_msg != ""){alert(student_msg);return;}
}//end if

//if(document.getElementById("chk_add_as_student").checked == true){//end if

document.getElementById("h_saving_data").value = "TRUE";
confirmToAddStudent();
document.getElementById("frmCustomer").submit(); 
//}//end if 
//}//VALIDATE STUDENT REQUIRED DATA


}//end function

function status(val){
//used for html label tag, to change its data between the label
//edit,add,view customer data etc...
val = "**** "+val+" ****";
document.getElementById("lblStatus").innerHTML = val;
document.getElementById("lblStatus").style.color = "red";   
document.getElementById("lblStatus").style.fontWeight = "bold";  
document.getElementById("lblStatus").style.fontSize = "large";

document.getElementById("lblStatus2").innerHTML = val;
document.getElementById("lblStatus2").style.color = "red";   
document.getElementById("lblStatus2").style.fontWeight = "bold";  
document.getElementById("lblStatus2").style.fontSize = "large";
//return;
}//end function

function formatPhoneNumber(input,ctrl_name){
if (input == null) {return null}
//
// Strip all non-numeric characters and check for '\'...
//
var plain = "";
for(var i = 0;i < input.length; i++){
   var c = input.substring(i, i+1);
   if(!isNaN(c) && c != " " || !isNaN(c) && c != "\\" )
      {
      plain += c;
      }
}
//
// Make sure we have 10 numeric digits
//
var tenNumeric = /^[0-9]{10}$/;
if(tenNumeric.test(plain))
   {
   //document.getElementById(ctrl_name).value =  "("
   document.getElementById(ctrl_name).value =  "("
   + plain.substring(0, 3)
   + ")" 
   + "-"
   + plain.substring(3, 6) 
   + "-"
   + plain.substring(6, 10);
   document.getElementById(ctrl_name).style.color = "black";
   document.getElementById(ctrl_name).style.backgroundColor = "white";
    }
else
   {   
   //alert("'" + input + "'" + " ~ Invalid Format\n\nJust type the number with no symbols with area code.\n\nMake sure you type in 10 numbers.\n\nex, 2108539255");
   //document.getElementById(ctrl_name).value = "Invalid Phone Number";
   //document.getElementById(ctrl_name).style.backgroundColor = "orange";
   }   
}//end function

function formatHomePhone(){
var hp = document.getElementById("home_phone");
 formatPhoneNumber(hp.value,hp.name);   
}//end function

function formatCellPhone(){
var cp = document.getElementById("cell_phone");
if(cp.value != ""){
formatPhoneNumber(cp.value,cp.name); 
}  
}//end function  

function formatEmergencyContactPhone(){
var ecp = document.getElementById("emergency_contact_phone");
 formatPhoneNumber(ecp.value,ecp.name);   
}//end function

function formatEmergencyContactName(){
val = document.getElementById("emergency_contact_name").value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length).toLowerCase() + ' ';
}
document.getElementById("emergency_contact_name").value = newVal;    
}//end function

function formatFirstName() {
val = document.getElementById("first_name").value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {    
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length).toLowerCase() + ' ';}
document.getElementById("first_name").value = newVal;
}//end function

function formatMiddleName() {
val = document.getElementById("middle_name").value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length).toLowerCase() + ' ';
}
document.getElementById("middle_name").value = newVal;
}//end function

function formatLastName() {
val = document.getElementById("last_name").value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length).toLowerCase() + ' ';
}
document.getElementById("last_name").value = newVal;
}//end function

function formatStreetAddress() {
val = document.getElementById("street_address").value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length).toLowerCase() + ' ';
}
document.getElementById("street_address").value = newVal;
}//end function

function formatCity() {
val = document.getElementById("city").value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length).toLowerCase() + ' ';
}
document.getElementById("city").value = newVal;
}//end function

function formatDLCity() {
val = document.getElementById("driver_license_birth_city").value;
newVal = '';
val = val.split(' ');
for(var c=0; c < val.length; c++) {
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length).toLowerCase() + ' ';
}
document.getElementById("driver_license_birth_city").value = newVal;
}//end function

function formatZipcode(){
var zip = document.getElementById("zipcode").value;  
if (zip.match(/^[0-9]{5}$/)) {
document.getElementById("zipcode").style.backgroundColor = "white";
document.getElementById("zipcode").style.color = "black";
return true;
}
zip = zip.toUpperCase();
if (zip.match(/^[A-Z][0-9][A-Z][0-9][A-Z][0-9]$/)) {
document.getElementById("zipcode").style.color = "black";
document.getElementById("zipcode").style.backgroundColor = "white";

return true;
}
if (zip.match(/^[A-Z][0-9][A-Z].[0-9][A-Z][0-9]$/)) {
document.getElementById("zipcode").style.color = "black";
document.getElementById("zipcode").style.backgroundColor = "white";
return true;
}
document.getElementById("zipcode").value = "Invalid Zipcode";
document.getElementById("zipcode").style.backgroundColor = "orange";
return false;
}//end function

function formatSSN(){
var ssn = document.getElementById("ssn").value;
if (ssn.length == 9) { 
document.getElementById("ssn").value = ssn.substring(0,3) + "-" + ssn.substring(3,5) + "-" + ssn.substring(5,9);
}
}//end function

function formatDate(date_ctrl){
var fd = document.getElementById(date_ctrl).value;
if (fd.length == 8) { 
document.getElementById(date_ctrl).value = fd.substring(0,2) + "/" + fd.substring(2,4) + "/" + fd.substring(4,8);
document.getElementById(date_ctrl).style.color = "black";
document.getElementById(date_ctrl).style.backgroundColor = "white";
}
validateDate(date_ctrl);
}//end function

function validateDate(date_ctrl) {
var d = document.getElementById(date_ctrl).value;
var pattern = /(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d/;
if (!d.match(pattern)){
//alert("Invalid Date for [ "+date_ctrl+" ]\n\nex,12/31/2013");
//document.getElementById(date_ctrl).value = "Invalid Date";
document.getElementById(date_ctrl).value = "";
document.getElementById(date_ctrl).style.backgroundColor = "orange";
return;
}
document.getElementById(date_ctrl).style.backgroundColor = "white";
}//end function


function validateSSN() {
var ssn = document.getElementById("ssn").value;
var pattern = /^\d{3}-?\d\d-?\d{4}$/;
//var pattern = /^\d{2}-\d{1}-\d{3}$/;
if (!ssn.match(pattern)){
//alert("Invalid Social Security Number.\n\nex,3 digits - 2 digits - 4 digits");
//document.getElementById("ssn").value = "Invalid SSN";
document.getElementById("ssn").value = "";
document.getElementById("ssn").style.backgroundColor = "orange";
}
document.getElementById("ssn").style.color = "black";
document.getElementById("ssn").style.backgroundColor = "white";
}//end function


function formatWeight(date_ctrl){
var fw = document.getElementById(date_ctrl).value;
document.getElementById(date_ctrl).value = fw + " lbs";
}//end function

function addCustomer(){ 

/* exist is the variable passed into the function
if(exist=="true"){
var result = confirm("Check if customer exist?\n\nPress OK to search or Cancel");
if(result){
window.location = "customer_check.php";
}//end if
}//end if
*/

status('ADD NEW MEMBER');
var selects = document.getElementsByTagName("select");
for (var i = 0; i < selects.length; i++) 
{
selects[i].value = ""; 
selects[i].disabled = false; 
//if(selects[i].name == "sel_range_time_by"){selects[i].disabled = true;}
}
var textareas = document.getElementsByTagName("textarea");
for (var i = 0; i < textareas.length; i++) 
{  
textareas[i].value = "";
textareas[i].disabled = false;
}
var inputs = document.getElementsByTagName("input"); 
for(var i = 0; i < inputs.length; i++)
{
if(inputs[i].type == 'text')
//if(inputs[i].id == "membership_number"){continue;}  
inputs[i].value = "";
inputs[i].disabled = false;
}
document.getElementById("search").disabled = false;
document.getElementById("save").disabled = false; 
document.getElementById("edit").disabled = true;     
document.getElementById("add").disabled = true;
document.getElementById("delete").disabled = true; 
document.getElementById("login_range").disabled = true;    
//document.getElementById("logout_range").disabled = true; 
document.getElementById("sel_range_time_by").disabled = true;
document.getElementById("total_range_time").disabled = true;
document.getElementById("scheduler").disabled = true;

document.getElementById("btn_take_photo").disabled = true;
document.getElementById("btn_upload_photo").disabled = true;
document.getElementById("btn_download_photo").disabled = true;

document.getElementById("home").disabled = true; 
document.getElementById("search").disabled = true; 

/*
document.getElementById("btn_take_photo").disabled = true;
document.getElementById("btn_take_photo").title = "Must save customer first, before adding their photo";

document.getElementById("btn_upload_photo").disabled = true;
document.getElementById("btn_upload_photo").title = "Must save customer first, before able to upload";

document.getElementById("btn_download_photo").disabled = true;
document.getElementById("btn_download_photo").title = "Must save customer first, before able to download";
*/

var currentTime1 = new Date();
var month1 = currentTime1.getMonth() + 1;
var day1 = currentTime1.getDate();
var year1 = currentTime1.getFullYear();
var dt_str1 = month1 + "/" + day1 + "/" + year1;
document.getElementById("membership_date").value = dt_str1;

/*
var currentTime2 = new Date();
var month2 = currentTime2.getMonth() + 1;
var day2 = currentTime2.getDate() + 1;
var year2 = currentTime2.getFullYear() + 1;
var dt_str2 = month2 + "/" + day2 + "/" + year2;
document.getElementById("membership_expires").value = dt_str2;
*/

document.getElementById("membership_number").disabled = true;
//here we generate the unique membership number for each customer 


//document.getElementById("membership_number").value = document.getElementById("h_unique_member").value; 


}//end function

function viewCustomer(){//logging out
status('VIEW MEMBER INFO (read only)');
var selects = document.getElementsByTagName("select");
for (var i = 0; i < selects.length; i++) 
{
if(selects[i].name == "sel_range_time_by"){continue;}
if(selects[i].name == "sel_by"){continue;}
if(selects[i].id == "sel_delete_file"){continue;}
//selects[i].disabled = true; 
selects[i].disabled = true; 
}
var textareas = document.getElementsByTagName("textarea");
for (var i = 0; i < textareas.length; i++) 
{  
textareas[i].disabled = true;
textareas[i].onblur = null;
}
var inputs = document.getElementsByTagName("input"); 
for(var i = 0; i < inputs.length; i++){
if(inputs[i].type == 'text'){ 
if(inputs[i].id=="billing_date"){document.getElementById("billing_date").disabled = false;}
if(inputs[i].id=="selPaymentMethod"){document.getElementById("selPaymentMethod").disabled = false;}
if(inputs[i].id=="billing_payment_amount"){document.getElementById("billing_payment_amount").disabled = false;}
   
inputs[i].disabled = true;
inputs[i].onblur = null;
}//end if
}//end for loop

//document.getElementById("billing_date").disabled = false;
//document.getElementById("selPaymentMethod").disabled = false;
//document.getElementById("billing_payment_amount").disabled = false;

document.getElementById("membership_expires").disabled = false;
document.getElementById("cancel").disabled = true; 

document.getElementById("save").value = "UPDATE";

if(document.getElementById("save").value == "UPDATE"){
document.getElementById("save").disabled = true;    
} 

document.getElementById("btn_take_photo").disabled = true;
document.getElementById("btn_upload_photo").disabled = true;
document.getElementById("chk_add_as_student").disabled = true;
                            //checkImage();//if missing_image.png remove any click event
}//end function

function editCustomer(){
status('EDIT MEMBER INFO');
document.getElementById("search").disabled = false;
document.getElementById("login_range").disabled = true;     
//document.getElementById("logout_range").disabled = true;
document.getElementById("edit").disabled = true;     
document.getElementById("add").disabled = true;
document.getElementById("delete").disabled = true; 

document.getElementById("save").value = "UPDATE"; 
//document.getElementById("save").id = "update"; 
//document.getElementById("save").name = "update"; 

var inputs = document.getElementsByTagName("input"); 
for(var i = 0; i < inputs.length; i++){
//if(inputs[i].id == "login_range"){continue;} 
//if(inputs[i].id == "logout_range"){continue;} 
if(inputs[i].type == 'text') 
       
//inputs[i].value = "";
inputs[i].disabled = false;
}
var selects = document.getElementsByTagName("select");
for (var i = 0; i < selects.length; i++) 
{   
//selects[i].value = ""; 
selects[i].disabled = false; 
}
var textareas = document.getElementsByTagName("textarea");
for (var i = 0; i < textareas.length; i++) 
{ 
//textareas[i].value = "";
textareas[i].disabled = false;
}  

document.getElementById("btn_download_photo").disabled = true;  
document.getElementById("btn_print_id_card").disabled = true; 
document.getElementById("scheduler").disabled = true; 

document.getElementById("home").disabled = true; 
document.getElementById("search").disabled = true; 
    
}//end function

/*
val = document.getElementById("first_name").value;
newVal = '';
val = val.split(' ');

for(var c=0; c < val.length; c++) {    
newVal += val[c].substring(0,1).toUpperCase() +
val[c].substring(1,val[c].length).toLowerCase() + ' ';
}

document.getElementById("first_name").value = newVal;
*/

function validateEmail(){ 
var e = document.getElementById("email").value; 
newVal = '';  
if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e)){
    
for(var c=0; c < e.length; c++) {    
newVal += e[c].substring(0,e[c].length).toLowerCase();
}

document.getElementById("email").value = newVal;
document.getElementById("email").style.color = "black";
document.getElementById("email").style.backgroundColor = "white"; 
return (true);   
}  
alert("You have entered an invalid email address!");
document.getElementById("email").focus(); 
//document.getElementById("email").value = "Invalid Email Address";
//document.getElementById("email").style.backgroundColor = "orange";
return (false);  
}//end function

function validateDriverLicenseNumber(){
var strFilter = /^[0-3][0-9]{7}$/;
var obj = document.getElementById("driver_license_number");
if (!strFilter.test(obj.value)) {
alert("Invalid license number\n\nUse only 8 digits.");
obj.value = "Invalid Number";
document.getElementById("driver_license_number").style.backgroundColor = "orange";
return false;
}else{
document.getElementById("driver_license_number").style.color = "black";
document.getElementById("driver_license_number").style.backgroundColor = "white";
return true;
}
}//end function  

function validateHeight(){
var h = document.getElementById("driver_license_height").value;
var g = h.length;
if (h.length == g) { 
document.getElementById("driver_license_height").value = h.substring(0,1) + " ft " + h.substring(2,g) + " in";
}
}//end function 

function deleteCustomer(member_number){
//prompt the user
var answer = confirm('Are you sure you want to delete member number '+member_number+' ?\n\nThis action is immediate and permanent ');
if ( answer ){ //if user clicked ok
//redirect to url with action as delete and id of the record to be deleted
window.location = 'customer.php?s=1&eteled=52051354&mn=' + member_number;
} 
}//end function

function logOut(member_number){
//prompt the user
var result = confirm('Are you sure you want to logout member number '+member_number+' ?\n\nThis action is immediate and permanent ');
if ( result ){ //if user clicked ok
//redirect to url with action as delete and id of the record to be deleted
window.location = 'customer.php?s=1&eteled=logout_member&mn=' + member_number;
//window.location = 'index.php';
}     
}//end function

function requiredFields(){
//check required fields
//var mst = document.getElementById("membership_type").value;
//var msd = document.getElementById("membership_date").value;
//var msde = document.getElementById("membership_expires").value;
var fn = document.getElementById("first_name").value;
var ln = document.getElementById("last_name").value;
var sa = document.getElementById("street_address").value;
var ci = document.getElementById("city").value;
var st = document.getElementById("state").value;
var zi = document.getElementById("zipcode").value;
var p1 = document.getElementById("home_phone").value;
var em = document.getElementById("email").value;
var BDate = document.getElementById("billing_date").value;
var BPaymentMethod = document.getElementById("h_billing_payment_method").value;
var BPaymentAmount = document.getElementById("billing_payment_amount").value;
//create message if something is missing   
var msg = "";
if(fn == ""){msg += "Required First Name\n";}
if(ln == ""){msg += "Required Last Name\n";}
if(sa == ""){msg += "Required Street Address\n";}
if(ci == ""){msg += "Required City\n";}  
if(st == ""){msg += "Required State\n";} 
if(zi == ""){msg += "Required Zipcode\n";} 
if(p1 == ""){msg += "Required Phone 1\n";} 
if(em == ""){msg += "Required Email\n";} 

if(document.getElementById("save").value=="UPDATE"){;}
else{
if(BDate == ""){msg += "Required Billing Date\n";} 
if(BPaymentMethod == ""){msg += "Required Payment Method\n";} 
if(BPaymentAmount == ""){msg += "Required Payment Amount\n";} 
}//end if

if(msg != ""){alert(msg);return true;}else{return false;}    
}//end function

//makeUnselectable(node) function not being used, but it may come into play
function makeUnselectable(node) {
if (node.nodeType == 1) {
node.setAttribute("unselectable", "on");
}
var child = node.firstChild;
while (child) {
makeUnselectable(child);
child = child.nextSibling;
}
}//end function

function makePayment(){
var doc_url = document.URL;
//operation=1 (means: add new customer) is what we're looking for
var chk_operation_type = doc_url.substring(54,doc_url.length);
if(chk_operation_type=="operation=1"){
alert("You must save this customer first, then you'll be able to process a payment.");
return;
}//end if chk_operation_type

document.getElementById("billing_date").disabled = false;
document.getElementById("selPaymentMethod").disabled = false;
document.getElementById("billing_payment_amount").disabled = false;
if(document.getElementById("billing_date").value==""||document.getElementById("h_billing_payment_method").value==""||document.getElementById("billing_payment_amount").value==""){
alert("Check your billing input values for making a payment. Then press again to complete payment transaction.");
return;
}
else{
var result = confirm("Press OK to submit this payment or Cancel.");
//alert(result);
if(result==true){document.getElementById("frmBilling").submit();}else{return;}//end confirm if/else
}//end else
}//end function

//////////////////////////////////////////END CUSTOMER INFO PAGE FUNCTIONS**************************









//////////////////////////////////////////START SEARCH PAGE FUNCTIONS**************************
function resetOptions() {
    document.getElementById('customer_list').options.length = 0;
    return true;
}

function clearMN(){
document.getElementById("search_by_first_name").value = "";  
document.getElementById("search_by_last_name").value = "";  
document.getElementById("search_by_home_phone").value = ""; 
document.getElementById("h_by_what_method").value = 1;
document.getElementById("h_val_for_where").value = document.getElementById("search_by_membership_number").value;
}//end function

function clearLN(){
//document.getElementById("search_by_first_name"),disabled = false;
document.getElementById("h_last_name").value = document.getElementById("search_by_last_name").value;
document.getElementById("search_by_membership_number").value = "";  
document.getElementById("search_by_first_name").value = ""; 
document.getElementById("search_by_home_phone").value = ""; 
document.getElementById("h_by_what_method").value = 3; 
document.getElementById("h_val_for_where").value = document.getElementById("search_by_last_name").value;  
}//end function

function clearFN(){
document.getElementById("h_first_name").value = document.getElementById("search_by_first_name").value;
document.getElementById("search_by_membership_number").value = "";  
document.getElementById("search_by_last_name").value = ""; 
document.getElementById("search_by_home_phone").value = ""; 
document.getElementById("h_by_what_method").value = 2; 
document.getElementById("h_val_for_where").value = document.getElementById("search_by_first_name").value;   
}//end function

function clearHP(){
formatPhoneNumber(document.getElementById("search_by_home_phone").value,"search_by_home_phone");
document.getElementById("search_by_membership_number").value = "";  
document.getElementById("search_by_first_name").value = "";  
document.getElementById("search_by_last_name").value = ""; 
document.getElementById("h_by_what_method").value = 4; 
document.getElementById("h_val_for_where").value = document.getElementById("search_by_home_phone").value;    
}//end function

function selectCustomer(){
if(document.getElementById("customer_list").value == ""){alert("Please start a search process.");return;}
var select_id = document.getElementById("customer_list");
var val = select_id.options[select_id.selectedIndex].value; 
location.href = "customer.php?login=true&s=1&mn=&operation=2&mn="+val; 
}//end function

function clickButton(e){
if (e.keyCode == 13){     
    alert(e.keyCode);
//document.getElementById("frmSearchBy").submit();
return false;
} 
}//end function 

function searchCancel(){
resetOptions();
document.getElementById("search_by_membership_number").value = "";  
document.getElementById("search_by_first_name").value = "";  
document.getElementById("search_by_last_name").value = ""; 
document.getElementById("search_by_home_phone").value = "";
document.getElementById("lbl_search_by").value = "";
location.reload();
}//end function

function enableFN(){
document.getElementById("search_by_first_name").disabled = false;    
}//end function

function disableFN(){
//document.getElementById("search_by_first_name").disabled = true;    
}//end function

//disable the return key and browser back button
function stopRKey(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) { return false; }
}//end function

function lastCustomer(val){
if(val!=""){ 
window.location = 'customer.php?s=1&operation=2&sort_by=1&mn='+val;
}
else{return;}    
}//end function


//////////////////////////////////////////END SEARCH PAGE FUNCTIONS**************************