<!DOCTYPE html>
<html>

<head>
	<script type="text/javascript">
		window.NREUM || (NREUM = {}), __nr_require = function(e, n, t) {
			function r(t) {
				if (!n[t]) {
					var o = n[t] = {
						exports: {}
					};
					e[t][0].call(o.exports, function(n) {
						var o = e[t][1][n];
						return r(o || n)
					}, o, o.exports)
				}
				return n[t].exports
			}
			if ("function" == typeof __nr_require) return __nr_require;
			for (var o = 0; o < t.length; o++) r(t[o]);
			return r
		}({
			QJf3ax: [function(e, n) {
				function t(e) {
					function n(n, t, a) {
						e && e(n, t, a), a || (a = {});
						for (var u = c(n), f = u.length, s = i(a, o, r), p = 0; f > p; p++) u[p].apply(s, t);
						return s
					}

					function a(e, n) {
						f[e] = c(e).concat(n)
					}

					function c(e) {
						return f[e] || []
					}

					function u() {
						return t(n)
					}
					var f = {};
					return {
						on: a,
						emit: n,
						create: u,
						listeners: c,
						_events: f
					}
				}

				function r() {
					return {}
				}
				var o = "nr@context",
					i = e("gos");
				n.exports = t()
			}, {
				gos: "7eSDFh"
			}],
			ee: [function(e, n) {
				n.exports = e("QJf3ax")
			}, {}],
			3: [function(e, n) {
				function t(e) {
					return function() {
						r(e, [(new Date).getTime()].concat(i(arguments)))
					}
				}
				var r = e("handle"),
					o = e(1),
					i = e(2);
				"undefined" == typeof window.newrelic && (newrelic = window.NREUM);
				var a = ["setPageViewName", "addPageAction", "setCustomAttribute", "finished", "addToTrace", "inlineHit", "noticeError"];
				o(a, function(e, n) {
					window.NREUM[n] = t("api-" + n)
				}), n.exports = window.NREUM
			}, {
				1: 12,
				2: 13,
				handle: "D5DuLP"
			}],
			gos: [function(e, n) {
				n.exports = e("7eSDFh")
			}, {}],
			"7eSDFh": [function(e, n) {
				function t(e, n, t) {
					if (r.call(e, n)) return e[n];
					var o = t();
					if (Object.defineProperty && Object.keys) try {
						return Object.defineProperty(e, n, {
							value: o,
							writable: !0,
							enumerable: !1
						}), o
					} catch (i) {}
					return e[n] = o, o
				}
				var r = Object.prototype.hasOwnProperty;
				n.exports = t
			}, {}],
			D5DuLP: [function(e, n) {
				function t(e, n, t) {
					return r.listeners(e).length ? r.emit(e, n, t) : void(r.q && (r.q[e] || (r.q[e] = []), r.q[e].push(n)))
				}
				var r = e("ee").create();
				n.exports = t, t.ee = r, r.q = {}
			}, {
				ee: "QJf3ax"
			}],
			handle: [function(e, n) {
				n.exports = e("D5DuLP")
			}, {}],
			XL7HBI: [function(e, n) {
				function t(e) {
					var n = typeof e;
					return !e || "object" !== n && "function" !== n ? -1 : e === window ? 0 : i(e, o, function() {
						return r++
					})
				}
				var r = 1,
					o = "nr@id",
					i = e("gos");
				n.exports = t
			}, {
				gos: "7eSDFh"
			}],
			id: [function(e, n) {
				n.exports = e("XL7HBI")
			}, {}],
			G9z0Bl: [function(e, n) {
				function t() {
					var e = d.info = NREUM.info,
						n = f.getElementsByTagName("script")[0];
					if (e && e.licenseKey && e.applicationID && n) {
						c(p, function(n, t) {
							n in e || (e[n] = t)
						});
						var t = "https" === s.split(":")[0] || e.sslForHttp;
						d.proto = t ? "https://" : "http://", a("mark", ["onload", i()]);
						var r = f.createElement("script");
						r.src = d.proto + e.agent, n.parentNode.insertBefore(r, n)
					}
				}

				function r() {
					"complete" === f.readyState && o()
				}

				function o() {
					a("mark", ["domContent", i()])
				}

				function i() {
					return (new Date).getTime()
				}
				var a = e("handle"),
					c = e(1),
					u = window,
					f = u.document;
				e(2);
				var s = ("" + location).split("?")[0],
					p = {
						beacon: "bam.nr-data.net",
						errorBeacon: "bam.nr-data.net",
						agent: "js-agent.newrelic.com/nr-768.min.js"
					},
					d = n.exports = {
						offset: i(),
						origin: s,
						features: {}
					};
				f.addEventListener ? (f.addEventListener("DOMContentLoaded", o, !1), u.addEventListener("load", t, !1)) : (f.attachEvent("onreadystatechange", r), u.attachEvent("onload", t)), a("mark", ["firstbyte", i()])
			}, {
				1: 12,
				2: 3,
				handle: "D5DuLP"
			}],
			loader: [function(e, n) {
				n.exports = e("G9z0Bl")
			}, {}],
			12: [function(e, n) {
				function t(e, n) {
					var t = [],
						o = "",
						i = 0;
					for (o in e) r.call(e, o) && (t[i] = n(o, e[o]), i += 1);
					return t
				}
				var r = Object.prototype.hasOwnProperty;
				n.exports = t
			}, {}],
			13: [function(e, n) {
				function t(e, n, t) {
					n || (n = 0), "undefined" == typeof t && (t = e ? e.length : 0);
					for (var r = -1, o = t - n || 0, i = Array(0 > o ? 0 : o); ++r < o;) i[r] = e[n + r];
					return i
				}
				n.exports = t
			}, {}]
		}, {}, ["G9z0Bl"]);
	</script>
	<meta http-equiv="content-type" content="text/html" />
	<title>Range Controller - Module Configurations - Range Config</title>
	<link rel="stylesheet" type="text/css" media="all" href="/xapp/css/site_layout.css" />
	<link rel="stylesheet" type="text/css" href="/xapp/css/grid-fluid.css" />
	<link rel="stylesheet" type="text/css" href="/xapp/css/silex.css" />
	<link rel="stylesheet" type='text/css' href='/xapp/tools/jquery-ui-1.11.2.custom/jquery-ui.min.css' />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="/xapp/tools/jquery-1.11.0.min.js">
	</script>
	<script src='/js/vendor/ractive.min.js'></script>
	<script src='/js/rclib.js'></script>
	<script src='/js/vendor/jquery.mask.js'></script>
	<script src="/xapp/tools/jquery.simplePagination.js">
	</script>
	<script src="/js/vendor/ejs/ejs_production.js"></script>
	<script src="/js/vendor/ejs/view.js"></script>
	<script src="/js/vendor/validator.min.js"></script>
	<script src="/js/vendor/numeral.min.js"></script>
	<script src="/xapp/tools/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
	<script src="/js/vendor/jquery.fileupload.js"></script>
	<script src="/xapp/js/angular.js"></script>
	<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
	<script>
		// global namespace for ranger functions 
		var ranger = ranger || {};
		// global namespace for ranger members/customers functions
		ranger.members = ranger.members || {};
		rc.disableNavigateBackOnBackspace();
	</script>
	<script src="https://www.rangecontroller.com/js/Chart.min.js"></script>
</head>

<body>
	<div class="hideFromPrint marginCenter"><img class="bg" src="/xapp/img/background.jpg" /> </div>
	<table id="headerTable" class="hideFromPrint marginCenter">
		<tr>
			<td class="blackHeaderCell">
				<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
				<div class="floatLeft">

					<img src="/xapp/img/logoNew.png" id="logo" style="width: 60px; height: 60px;">&nbsp;&nbsp;</div>
				<div class="floatLeft padTop"><span class="rcLogoText"><span style="color: #fbb91f;">Range</span> <span style="color: white;">Controller</span></span>
				</div>
			</td>
		</tr>
	</table>
	<table id="companyTable" class="hideFromPrint marginCenter">
		<tr>

			<td id="companyTableCell" class="textCenter borderTop"><span class="companyName"> Southeast Weapons And Training </span></td>
			<td id="companyLogoTableCell" class="textCenter borderTop">

				<img class="logoimg" src="/xapp/img/4420/swat_logo.png" />
			</td>
		</tr>
	</table>
	<table id="contentTable">
		<tbody>
			<tr>
				<td id="contentTableCell">
					<div class="container_12 customer">
						<div class="grid_9">
							<div class="breadcrumbs hideFromPrint">
								<a href='/xapp/dashboard'>Home </a> / <a href="/xapp/configurations">Module Configurations </a> / <span>Range Config </span> </div>
						</div>
						<div class="grid_3" style="z-index: 1; display:none;height:18px;" id='breadcrumbsRigh'>
							<select name="employee_id" id="employee_id" style="float:right;">
								<option value="">Choose an employee : </option>
								<option>Williams</option>
							</select><span style="float:right;" class="requiredf">* </span></div>
						<div class="clear">&nbsp;</div>

						<script>
							function getEmployees() {

								//alert("FDADSA");

								var myurlstring = "/xapp/getEmployeeList";

								var xmlHttp = null;

								xmlHttp = new XMLHttpRequest();
								xmlHttp.open("GET", myurlstring, false);
								xmlHttp.send(null);

								document.getElementById("breadcrumbsRigh").innerHTML = xmlHttp.responseText;

							}

							function addEmployee(id) {

								//alert("SDS");

								if (id == "-1") window.location = "/xapp/config/employees/add";

							}
						</script>

						<div class="grid_12">
							<div class="text-right">
								<input type="button" value="Add" onclick="window.location='addRange.php'">
							</div>
						</div>
						<div class="grid_3">&nbsp;</div>
						<div class="grid_6 margt">
							<br>


							<table>
								<tr>
									<th>Range</th>
									<th colspan="2" style="width: 35%;">Action</th>
								</tr>
								<tr>
									<td>Default Range</td>
									<td><a href="manageRange.php">Manage</a></td>
									<td></td>
								</tr>
								<tr>
									<td>Range 2</td>
									<td><a href="manageRange.php">Manage</a></td>
									<td><a href="#">Delete</a></td>
								</tr>
							</table>


						</div>
						<div class="grid_3">&nbsp;</div>

						<div class="clear">&nbsp;</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table id="footer1Table" class="hideFromPrint textCenter">

		<tbody>
			<tr>
				<td style="border-top: 0px;">
					<div class="floatLeft padLeft"> <a href="/xapp/common/logout.php" class="text-nodeco">LOGOUT</a> </div>
					<div class="floatRight padRight"> <a href="/xapp/techSupport" class="text-nodeco">Tech Support</a> </div>
				</td>
			</tr>
		</tbody>
	</table>
	<table id="footerTable" class="hideFromPrint textCenter">
		<tr>
			<td id="footerCell" class="textColor noBorder textCenter"> Range Controller v2.2 by Rockin' Guns
				<br /> &copy;
				<script>
					document.write(new Date().getFullYear());
				</script>
			</td>
		</tr>
	</table>




	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
		window.$zopim || (function(d, s) {
			var z = $zopim = function(c) {
					z._.push(c)
				},
				$ = z.s =
				d.createElement(s),
				e = d.getElementsByTagName(s)[0];
			z.set = function(o) {
				z.set.
				_.push(o)
			};
			z._ = [];
			z.set._ = [];
			$.async = !0;
			$.setAttribute("charset", "utf-8");
			$.src = "//v2.zopim.com/?3IFxEbj4zan46kXwkOFigy4PiCeK4ycL";
			z.t = +new Date;
			$.
			type = "text/javascript";
			e.parentNode.insertBefore($, e)
		})(document, "script");
	</script>
	<!--End of Zopim Live Chat Script-->


	<script type="text/javascript">
		window.NREUM || (NREUM = {});
		NREUM.info = {
			"beacon": "bam.nr-data.net",
			"licenseKey": "2a236f4391",
			"applicationID": "11068291",
			"transactionName": "ZF1WY0UEXUNTWkZfVl0XdVRDDFxeHWZKV0lDZ1dYWQNaV0dLU0JQXFZH",
			"queueTime": 0,
			"applicationTime": 28,
			"atts": "SBpVFQ0eTk0=",
			"errorBeacon": "bam.nr-data.net",
			"agent": "js-agent.newrelic.com\/nr-768.min.js"
		}
	</script>
</body>

</html>