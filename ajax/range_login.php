<?php

require_once __DIR__ . '/../bootstrap.php';
use Rc\Services\DbHelper;
//require('../common/db.php');
$link = DbHelper::getWrappedHandle($app['dbs']['main']);

$sql = "SELECT alo.acctnumberhashed,alo.accountnumber,alo.current_pw,alo.username, cc.account_status "
        . " FROM admin_login alo inner join controller_config cc "
        . " on (alo.accountnumber = cc.accountnumber) "
        . " WHERE alo.username = '" . trim($link->real_escape_string($_POST['rn'])) . 
        "' AND alo.current_pw = '" . trim($link->real_escape_string($_POST['pw'])) . "'";


$rowcount = 0;

$account_status= '';
if ($result = $link->query($sql)) {
    while ($row = $result->fetch_assoc()) {
        $rowcount++;
        $hn = $row['acctnumberhashed'];
        $an = $row['accountnumber'];
        $account_status = trim($row['account_status']);
    }//END WHILE LOOP 
}//END IF

if ($rowcount > 0) {
    if ($account_status == 'Suspended') {
        echo 'Suspended';
    } else {
        $_SESSION['account_number'] = $an;
        $_SESSION['account_number_hashed'] = $hn;
        $_SESSION['hn'] = $hn;
        $_SESSION['an'] = $an;
        echo "success";
    }
}
?>