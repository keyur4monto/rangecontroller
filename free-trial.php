<?php
include 'includes/session.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Range Controller - Free Trial</title><?php
        include 'includes/headerTags.php';
        ?>
    <style>
                #headerRight {
                    display: none;
                }
    </style>
</head>
<body>
    <?php
        include 'includes/header.php';
        ?>
    <div class="padTop" id="freeTrial">
        <div id="freeTrialBox">
            <h2 class="gold">Your 30 day FREE TRIAL starts here!</h2>
            <p>You will need to provide some basic information about your range
            to customize your RangeController.<img src=
            "https://www.rangecontroller.com/images/trial_right.jpg" style=
            "float:right; padding: 10px;"></p>
            <p>A credit card is NOT REQUIRED for this free trial and you will
            full access to the software for the next 30 days.</p>
            <p><img src="https://www.rangecontroller.com/images/trial_left.jpg"
            style="float: left; padding: 10px;">If you decide to keep
            RangeController, you can easily start a monthly recurring
            subscription by entering your payment details on our secure
            server.</p>
            <p>If RangeController didn't work for you, no cancellation is
            necessary, your data will be purged and deleted from our database
            shortly after your trial ends.</p>
            <p class="center">Captcha</p>
            <p class="center"><input id="freeTrialStart" type="button" value=
            "Let's Get Started"></p>
        </div>
    </div><?php
        include 'includes/footer.php';
        ?>
</body>
</html>