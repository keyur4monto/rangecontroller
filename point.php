<?php
error_reporting(E_ERROR);
include 'includes/session.php';
//this is just a comment
?>
<!DOCTYPE html>
<html>
<head>
    <title>Range Controller</title><?php
    include 'includes/headerTags.php';
    ?>
</head>
<body>
    <?php
    include 'includes/header.php';
    ?><?php
    include 'includes/navBar.php';
    ?>
    <div id="welcome">
        <div id="welcomeLeft">
            <h2>Welcome</h2>
            <p>FINALLY! A software platform for the indoor/outdoor shooting
            range owner! Over a year ago, we were in the middle of an indoor
            shooting range build and we began calling around leaders in our
            industry to get a jump start on a complete software system for the
            range when it opened for business. After weeks of investigation and
            many calls, we found out there were no programs we could use to run
            our range. We were dumbfounded!</p>
            <p><a href="main.php"><img src=
            "/images/redmore.png"></a></p>
        </div>
        <div id="welcomeRight">
            <iframe allowfullscreen="" frameborder="0" height="265" src=
            "https://www.youtube.com/embed/E2slR6ZpOFc" width="400"></iframe>
        </div>
    </div><?php
    include 'includes/content.php';
    ?><?php
    include 'includes/footer.php';
    ?>
</body>
</html>