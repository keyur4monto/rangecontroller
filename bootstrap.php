<?php
function getRangeUrlPrefix(){ 
    $uri = $_SERVER['REQUEST_URI'];
    if(stripos($uri, '/dev1') === 0) {
       return '/dev1';
    } else if (stripos($uri, '/xapp') === 0) {
        return '/xapp';
    }
    return '';
}
// 0 - setup auto loader for vendor libraries and your own code 
/* @var $autoLoader ClassLoader */
$autoLoader = require_once __DIR__ . '/vendor/autoload.php';

$autoLoader->addPsr4("Rc\\Models\\", __DIR__ . '/models');
$autoLoader->addPsr4("Rc\\Controllers\\", __DIR__ . '/controllers');
$autoLoader->addPsr4("Rc\\Services\\", __DIR__ . '/services');

if (!session_id()) {
    session_start();
}


class RcApplication extends \Silex\Application {
    //use \Silex\Application\UrlGeneratorTrait;
}
// 1 intialize app and get config
$app = new \RcApplication();

$app['config'] = require_once __DIR__ . '/config/config.php';

// 2 set session service
$app['session'] = $app->share(function(){
    return new \Rc\Services\NSession();
});
// use like $app['session']->get('session.key.name') or ->set('key', val)

// 3 - set up dbs
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'main' => $app['config']['dbs']['main'],
        'rcadmin' => $app['config']['dbs']['rcadmin'],
    ),
));
// use like $app['dbs']['main|rcadmin']->fetchAll

// 4 - set up templating 
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\Asset\PathPackage;
use Symfony\Component\Templating\Helper\AssetsHelper;


$app['templating.template_name_parser'] = $app->share(function() {
    return new TemplateNameParser();
});

$app['templating.loader'] = $app->share(function() use ($app) {
    return new FilesystemLoader($app['config']['views']['template_path'] .'/%name%');
});

$app['templating'] = $app->share(function() use($app){
    $engine = new PhpEngine($app['templating.template_name_parser'], 
            $app['templating.loader']);
    $engine->set(new AssetsHelper());    
    $liveDevPrefix = $app['range_url_prefix'];
    $engine->get('assets')->addPackage('rangepath', 
            new PathPackage($liveDevPrefix. '/'));
    $engine->addGlobal('app', $app);
    return $engine;
});
// echo $app['templating']->render('hello.html.php', array('name' => $name));

// 5 - set other services 
$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());

// returns the prefix for range pages depending on environment (/dev1 or /xapp)
$app['range_url_prefix'] = function() use ($app){
    return \getRangeUrlPrefix();
};
$app['range_img_path'] = $app->share(function() use ($app){
    $ro = $app['range_owner'];    
    return $app['config']['base_path'] . 
            $app['range_url_prefix'] . '/img/' .$ro->getAccountNumber();
});
$app['shooter_img_path'] = $app->share(function() use ($app){
    $ro = $app['shooter_owner'];    
    return $app['config']['base_path'] . 
            $app['range_url_prefix'] . '/img/' .$ro->getAccountNumber();
});
$app['range_logo_url'] = $app->share(function() use ($app){
    $ro = $app['range_owner'];
    $logo = $ro->getCompanyLogo();
    if (trim(strtolower($logo)) === 'generic-logo.png') {
        $path = $app['range_img_path'] . '/../' . $logo;
    } else { 
        $path = $app['range_img_path'] . '/' . $logo;   
    }
    
    if (file_exists($path) && !is_dir($path)) {
        return str_replace($app['config']['base_path'], '', $path);
    }
    return null;
});
$app['shooter_logo_url'] = $app->share(function() use ($app){
    $ro = $app['shooter_owner'];
    $logo = $ro->getCompanyLogo();
    if (trim(strtolower($logo)) === 'generic-logo.png') {
        $path = $app['shooter_img_path'] . '/../' . $logo;
    } else { 
        $path = $app['shooter_img_path'] . '/' . $logo;   
    }
    
    if (file_exists($path) && !is_dir($path)) {
        return str_replace($app['config']['base_path'], '', $path);
    }
    return null;
});
$app['get_range_url'] = $app->protect(function($path) use ($app) {
    return $app['range_url_prefix'] . '/' . $path;
});
$app['get_admin_url'] = $app->protect(function($path) use ($app) {
    return $app['range_url_prefix'] . '/' . $path;
});
$app['debug'] = $app['config']['debug'];


$app['range_owner_id'] = $app->share(function() use ($app){
    $hashKey = $app['session']->get('account_number_hashed');
    $accNo = $app['session']->get('account_number');
    if (!empty($hashKey) && !empty($accNo)) {
        return new \Rc\Models\range_owner_id($hashKey, $accNo);
    }
    return null;
});

$app['range_owner'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['range_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\RangeOwner::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['shooter_owner_id'] = $app->share(function() use ($app){
    $hashKey = $app['session']->get('hn');
    $accNo = $app['session']->get('an');
    if (!empty($hashKey) && !empty($accNo)) {
        return new \Rc\Models\shooter_owner_id($hashKey, $accNo);
    }
    return null;
});
$app['admin_owner_id'] = $app->share(function() use ($app){
    $customer_number = $app['session']->get('customer_number');
   
    if (!empty($customer_number)) {
        return new \Rc\Models\admin_owner_id($customer_number);
    }
    return null;
});
$app['shooter_owner'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\ShooterOwner::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
/** Tushar ********************/
$app['newsletter_id'] = $app->share(function() use ($app){
    $hashKey = $app['session']->get('account_number_hashed');
    $accNo = $app['session']->get('account_number');
    if (!empty($hashKey) && !empty($accNo)) {
        return new \Rc\Models\newsletter_id($hashKey, $accNo);
    }
    return null;
});

$app['newsletter_subcriber'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['newsletter_id'];
    if ($conn && $acc) {
        return \Rc\Models\NewsletterSubcriber::initWithAccount($conn, $acc->getAccountHashKey(),$acc->getAccountNo());
    }
    return null;
});
/** ********************/

$app['special_event'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['newsletter_id'];
    if ($conn && $acc) {
        return \Rc\Models\Specialevent::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['changepassword'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\ChangePassword::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});

$app['contact_info'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\contactinfo::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['edit_contact_info'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\editcontactinfo::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['billing'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\billing::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['range_usages'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\rangeuses::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['member'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\member::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['upcoming_events'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\upcomingevents::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['upcoming_events_detail'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\upcomingeventsdetail::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['upcoming_classes'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\upcomingclasses::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['upcoming_classes_detail'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\upcomingclassesdetail::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['id_card'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['range_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\idcard::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['scheduler_shooter'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['shooter_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\scheduler::initWithAccount($conn, $acc->getAccountHashKey(), 
            $acc->getAccountNo());
    }
    return null;
});
$app['range_member_no'] = $app->share(function() use ($app){
    return $app['session']->get('member_number',null);
});

$app['range_member_default_image'] = $app->share(function() use($app){
    return  $app['config']['base_path'] . $app['range_url_prefix'] . 
            '/img/image_missing.png';
});
$app['admin_newsletter'] = $app->share(function() use($app){    
    $conn = $app['dbs']['rcadmin']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['admin_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\admin_newsletter::initWithAccount($conn, $acc->getcustomer_number());
    }
    return null;
});
$app['admin_newsletter_email'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['admin_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\admin_newsletter::initWithAccount($conn, $acc->getcustomer_number());
    }
    return null;
});
$app['admin_blog'] = $app->share(function() use($app){    
    $conn = $app['dbs']['rcadmin']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['admin_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\admin_blog::initWithAccount($conn, $acc->getcustomer_number());
    }
    return null;
});
$app['admin_announcementboard'] = $app->share(function() use($app){    
    $conn = $app['dbs']['rcadmin']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['admin_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\admin_announcementboard::initWithAccount($conn, $acc->getcustomer_number());
    }
    return null;
});
$app['admin_login'] = $app->share(function() use($app){    
    $conn = $app['dbs']['rcadmin']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['admin_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\admin_login::initWithAccount($conn, $acc->getcustomer_number());
    }
    return null;
});
$app['admin_account'] = $app->share(function() use($app){    
    $conn = $app['dbs']['main']->getWrappedConnection()->getWrappedResourceHandle();
    $acc= $app['admin_owner_id'];
    if ($conn && $acc) {
        return \Rc\Models\admin_account::initWithAccount($conn, $acc->getcustomer_number());
    }
    return null;
});

use Symfony\Component\Finder\Finder;
$app['range_member_image_dir'] = $app->share(function() use ($app){
    $ro = $app['range_owner_id'];
    $accno = 0;
    if ($ro) {
        $accno = $ro->getAccountNo();
    }
    $dir = $app['config']['base_path'] . $app['range_url_prefix'] . 
                    '/img/' . $accno . '/webcam';
    if (!file_exists($dir)) {
        mkdir($dir,0775,true);
    }
    return $dir;
});

$app['range_member_image_path'] = $app->protect(function($memberNo) use($app){
    
    $path = $app['range_member_image_dir'];
    
    $finder = new Finder();
    $files = $finder->files()->name($memberNo . '.*')
            ->in($path)->sortByModifiedTime();    
    $result = null;
    // get last modified file
    foreach($files as $file) {
        $result =  $path . '/' . $file->getFileName();        
    }
    return $result;    
});

$app['range_member_image_url'] = $app->protect(function($memberNo) use($app){
    
    $fpath = $app['range_member_image_path']($memberNo);    
    $imagefile = $app['range_member_default_image'];
    if (!empty($fpath)) {
        $imagefile = $fpath;
    }
    return str_replace($app['config']['base_path'], '', $imagefile);    
});

if(substr($_SERVER['REQUEST_URI'],6,5) == 'admin')
{
if (!isset($_SESSION['customer_number'])) {
		// is this a ranger or shooter site? 
		$_reqUri = $_SERVER['REQUEST_URI'];
		if (stripos($_reqUri, '/dev1') === 0
				|| stripos($_reqUri, '/xapp/admin') === 0) {
			//header('Location: /');
			header('Location: /xapp/admin/login');
		}
	}	
}

else
{
	if (!isset($_SESSION['account_number_hashed'])
			|| empty($_SESSION['account_number_hashed'] )
			|| !isset($_SESSION['account_number'])
			|| empty($_SESSION['account_number']) ) {
		// is this a ranger or shooter site? 
		$_reqUri = $_SERVER['REQUEST_URI'];
		if (stripos($_reqUri, '/dev1') === 0
				|| stripos($_reqUri, '/xapp') === 0) {
			//header('Location: /');
			header('Location: https://www.rangecontroller.com');
		}
	}
}
