<?php
namespace Rc\Controllers\Admin {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class BlogController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-admin.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app)
		{
			$newsletter = $app['admin_blog'];
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			
			$get_blog = "";
			$get_blog = $newsletter->getBlog($customer_number);
			
			$data = array(                
			'get_blog' => $get_blog   
            );
			
			$breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Admin - Blog';
			$content = $app['templating']->render('admin/adminblog/list.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
			return array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
                array('title'=>'Blog')
            );
        }
		private function wrapInLayout(Application $app, $data){
           
            $breadcrumbsContent = $app['templating']->render(
                    'admin/adminblog/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content']				
            );
            return $app['templating']->render('admin/layout-admin.php', $vars);
        } 
		public function addBlog(Application $app)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			
			$errorMessage = '';
			if ($app['request']->get('cmd', '') === 'add') {
                
				$subject=$app['request']->get('subject', '');
				$message_body=$app['request']->get('txtContent', '');
				
				if(empty($subject) || empty($message_body))
					$errorMessage="Please fill out all of the required fields marked with *";
                if (!$errorMessage) {	
				$admin_blog = $app['admin_blog'];
				$get_admin_blog = $admin_blog->addBlog($subject, $message_body);
				return $app->redirect($app['get_admin_url']('admin/blog'));
				}
			}
			
			$data = array(
                'errorMessage' => $errorMessage
            );
			
			$breadcrumbs = array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
				array('link' => $app['get_admin_url']('').'admin/blog', 'title'=>'Blog'),
                array('title'=>'Add')
            );
            $pageTitle = 'Range Controller - Admin - Blog - Add';
			$content = $app['templating']->render('admin/adminblog/add.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }  
		public function manageBlog(Application $app, $blog_id)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			
			$blog = $app['admin_blog'];
			$errorMessage = '';
			
			if ($app['request']->get('cmd', '') === 'edit') 
			{
				$subject=$app['request']->get('subject', '');
				$message_body=$app['request']->get('txtContent', '');
				$blog_id=$app['request']->get('blog_id', '');
				if(empty($subject) || empty($message_body))
					$errorMessage="Please fill out all of the required fields marked with *";
				if (!$errorMessage) {	
				$blog = $app['admin_blog'];
				$get_blog = $blog->editBlog($blog_id, $subject, $message_body);
				return $app->redirect($app['get_admin_url']('admin/blog'));
				}
			}
			$get_blog = "";
			$get_blog = $blog->getBlogId($blog_id);
			
			$data = array(                
			'get_blog' => $get_blog,
			'errorMessage' => $errorMessage   
            );
			
			$breadcrumbs = array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
				array('link' => $app['get_admin_url']('').'admin/blog', 'title'=>'Blog'),
                array('title'=>'Manage')
            );
            $pageTitle = 'Range Controller - Admin - Blog - Manage';
			$content = $app['templating']->render('admin/adminblog/manage.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		 public function deleteBlog (Application $app, $blog_id)
		 {		
		 		$roid = $app['admin_owner_id'];
				$customer_number = $roid->getcustomer_number();	
			
				$blog = $app['admin_blog'];
				$get_blog = $blog->deleteBlog($blog_id);
				return $app->redirect($app['get_admin_url']('admin/blog'));			
		 }   
    }
}
