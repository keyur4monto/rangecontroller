<?php
namespace Rc\Controllers\Admin {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    //use Rc\Models\range_owner;
    //use Rc\Models\range_member;
    //use Rc\Models\browser;
   // use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class DashboardController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-admin.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();
			$breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Admin - Dashboard';
			$content = $app['templating']->render('admin/admindashboard/list.php');
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
			return array(
                array('link' => $app['get_admin_url']('').'common/logout.php', 'title'=>'LOGOUT'),
                array('title'=>'Admin')
            );
        }
		private function wrapInLayout(Application $app, $data){
           
            $breadcrumbsContent = $app['templating']->render(
                    'admin/admindashboard/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content']				
            );
            return $app['templating']->render('admin/layout-admin.php', $vars);
        }   
    }
}
