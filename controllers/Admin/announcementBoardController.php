<?php
namespace Rc\Controllers\Admin {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class announcementBoardController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-admin.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app)
		{
			$announcementboard = $app['admin_announcementboard'];
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			
			$get_announcementboard = "";
			$get_announcementboard = $announcementboard->getAnnouncementboard($customer_number);
			
			$data = array(                
			'get_announcementboard' => $get_announcementboard   
            );
			
			
			$breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Admin - Announcement Board';
			$content = $app['templating']->render('admin/adminAnnouncementBoard/list.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
			return array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
                array('title'=>'Announcement Board')
            );
        }
		private function wrapInLayout(Application $app, $data){
           
            $breadcrumbsContent = $app['templating']->render(
                    'admin/adminAnnouncementBoard/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content']				
            );
            return $app['templating']->render('admin/layout-admin.php', $vars);
        } 
		public function addAnnouncementBoard(Application $app)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();
				
			$errorMessage = '';
			if ($app['request']->get('cmd', '') === 'add') {
                
				$message_body=$app['request']->get('txtContent', '');
				$message_body=htmlspecialchars_decode($message_body);
				if(empty($message_body))
					$errorMessage="Please fill out all of the required fields marked with *";
                if (!$errorMessage) {	
				$announcementboard = $app['admin_announcementboard'];
				$get_announcementboard = $announcementboard->addAnnouncementboard($message_body);
				return $app->redirect($app['get_admin_url']('admin/announcementBoard'));
				}
			}
			
			$data = array(
                'errorMessage' => $errorMessage
            );
			
			$breadcrumbs = array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
				 array('link' => $app['get_admin_url']('').'admin/announcementBoard', 'title'=>'Announcement Board'),
                array('title'=>'Add ')
            );
            $pageTitle = 'Range Controller - Admin - Announcement Board - Add';
			$content = $app['templating']->render('admin/adminAnnouncementBoard/add.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }  
		public function manageAnnouncementBoard(Application $app, $announcementboard_id)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			
			$announcementboard = $app['admin_announcementboard'];
			$errorMessage = '';
			
			if ($app['request']->get('cmd', '') === 'edit') 
			{
				$message_body=$app['request']->get('txtContent', '');
				$announcementboard_id=$app['request']->get('announcementboard_id', '');
				if(empty($message_body))
					$errorMessage="Please fill out all of the required fields marked with *";
				if (!$errorMessage) {	
				$get_announcementboards = $announcementboard->editAnnouncementboard($announcementboard_id, $message_body);
				return $app->redirect($app['get_admin_url']('admin/announcementBoard'));
				}
			}
			$get_announcementboard = "";
			$get_announcementboard = $announcementboard->getAnnouncementboards($announcementboard_id);
			
			$data = array(                
			'get_announcementboard' => $get_announcementboard,
			'errorMessage' => $errorMessage   
            );
			
			$breadcrumbs = array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
				 array('link' => $app['get_admin_url']('').'admin/announcementBoard', 'title'=>'Announcement Board'),
                array('title'=>'Manage ')
            );
            $pageTitle = 'Range Controller - Admin - Announcement Board - Manage';
			$content = $app['templating']->render('admin/adminAnnouncementBoard/manage.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function deleteAnnouncementBoard (Application $app, $announcementboard_id)
		{	
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
				
			$announcementboard = $app['admin_announcementboard'];
			$get_announcementboard = $announcementboard->deleteAnnouncementboard($announcementboard_id);
			return $app->redirect($app['get_admin_url']('admin/announcementBoard'));				
		} 
    }
}
