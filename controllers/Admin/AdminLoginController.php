<?php
namespace Rc\Controllers\Admin {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class AdminLoginController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-admin.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app)
		{
			$breadcrumbsContent = $app['templating']->render('admin/adminlogin/breadcrumbs.php');
			$breadcrumbs[1]['link'] = '';
			$pageTitle = 'Range Controller - Admin - Login';
			$content = $app['templating']->render('admin/adminlogin/list.php');
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
           return array(
                array('link' => $app['get_range_url']('').'common/logout.php', 'title'=>'LOGOUT'),
                array('title'=>'Admin')
            );
        }
		private function wrapInLayout(Application $app, $data){
           
            $breadcrumbsContent = $app['templating']->render(
                    'admin/adminlogin/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $data['content']				
            );
            return $app['templating']->render('admin/layout-admin.php', $vars);
        } 
		public function loginCheck(Application $app)
		{
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$txtAccountNumber = $_POST['txtAccountNumber'];
			$txtAdminPassword = $_POST['txtAdminPassword'];
			$sql_ins = "SELECT customer_number, current_pw FROM admin_admin_login WHERE customer_number='".$txtAccountNumber."'  AND current_pw='".$txtAdminPassword. "'";			
			$result = DbHelper::dbFetchRowAssoc($link, $sql_ins);
		   
            if ($result['customer_number'] == $txtAccountNumber && $result['current_pw'] == $txtAdminPassword)
			{
				$_SESSION['customer_number'] = $result['customer_number'];
                return $app->redirect($app['get_admin_url']('admin/dashboard'));
            } 
			else 
			{
                $blnAttempt = "'Something is not right, Please try again.'";
				$data = array(    
				'blnAttempt' => $blnAttempt            
           		);
				
				$breadcrumbsContent = $app['templating']->render('admin/adminlogin/breadcrumbs.php');
				$breadcrumbs[1]['link'] = '';
				$pageTitle = 'Admin - Login';
				$content = $app['templating']->render('admin/adminlogin/list.php', $data);
				return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
            }
        }  
    }
}
