<?php
namespace Rc\Controllers\Admin {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class AccountController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-admin.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app)
		{
			$account = $app['admin_account'];
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			
			$get_account_data = "";
			$get_account_data = $account->get_account_data($customer_number);
			$get_trial_data = $account->get_trial_data();
			foreach($get_trial_data as $get_trial_datas)
			{
				$get_trial[] = $get_trial_datas['accountnumber'];
			}
		
			$data = array(                
			'get_accountdata' => $get_account_data,
			'get_trial' => $get_trial  
            );
			
			$breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Admin - Account';
			$content = $app['templating']->render('admin/adminaccount/list.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
			return array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
                array('title'=>'Accounts')
            );
        }
		private function wrapInLayout(Application $app, $data){
           
            $breadcrumbsContent = $app['templating']->render(
                    'admin/adminaccount/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content']				
            );
            return $app['templating']->render('admin/layout-admin.php', $vars);
        } 
		public function manageAccount(Application $app, $accountnumber)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			
			$account = $app['admin_account'];
			$errorMessage = '';
			if ($app['request']->get('cmd', '') === 'edit') 
			{
				$company_name=$app['request']->get('company_name', '');
				$contact_name=$app['request']->get('contact_name', '');
				$address=$app['request']->get('address', '');
				$city=$app['request']->get('city', '');
				$state=$app['request']->get('state', '');
				$zip_code=$app['request']->get('zip_code', '');
				$phone_number=$app['request']->get('phone_number', '');
				$email_address=$app['request']->get('email_address', '');
				$accountnumber=$app['request']->get('accountnumber', '');
				$notes=$app['request']->get('notes', '');
				if(empty($company_name) || empty($contact_name) || empty($address) || empty($city) || empty($state) || empty($zip_code) || empty($phone_number) || empty($email_address))
					$errorMessage="Please fill out all of the required fields marked with *";
				if (!$errorMessage) {	
				$account = $app['admin_account'];
				$edit_account = $account->editAccount($accountnumber, $company_name, $contact_name, $email_address, $notes);
				$edit_accounts = $account->editAccounts($accountnumber, $address, $city, $state, $zip_code, $phone_number);
				return $app->redirect($app['get_admin_url']('admin/account'));
				}
			}
			
			$get_account_detail = "";
			$get_account_detail = $account->getAccountDetail($accountnumber);
			
			$data = array(                
			'get_account_detail' => $get_account_detail,
			'errorMessage' => $errorMessage   
            );
			
			$breadcrumbs = array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
				array('link' => $app['get_admin_url']('').'admin/account', 'title'=>'Account'),
                array('title'=>'Manage')
            );
            $pageTitle = 'Range Controller - Admin - Accounts - Manage';
			$content = $app['templating']->render('admin/adminaccount/manage.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        } 
		public function deleteAccount(Application $app, $accountnumber)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
				
			$account = $app['admin_account'];
			$signup_contact_info_delete = $account->signup_contact_info_delete($accountnumber);
			$controller_config_delete = $account->controller_config_delete($accountnumber);
			$hours_of_operation_delete = $account->hours_of_operation_delete($accountnumber);
			$admin_login_delete = $account->admin_login_delete($accountnumber);
			$editable_memberships_delete = $account->editable_memberships_delete($accountnumber);
			$control_member_number_delete = $account->control_member_number_delete($accountnumber);
			$universal_id_delete = $account->universal_id_delete($accountnumber);
			$show_hide_delete = $account->show_hide_delete($accountnumber);
			$pos_register_delete = $account->pos_register_delete($accountnumber);
			$pos_transaction_number_delete = $account->pos_transaction_number_delete($accountnumber);
			$id_card_creator_delete = $account->id_card_creator_delete($accountnumber);
			$trial_version_delete = $account->trial_version_delete($accountnumber);
			
			$emergency_contact_delete = $account->emergency_contact_delete($accountnumber);
			$benefits_delete = $account->benefits_delete($accountnumber);
			$bug_report = $account->bug_report_delete($accountnumber);
			$driver_license_info = $account->driver_license_info_delete($accountnumber);
			$employees = $account->employees_delete($accountnumber);
			$member_login = $account->member_login_delete($accountnumber);
			$memberships = $account->memberships_delete($accountnumber);
			$new_newsletter = $account->new_newsletter_delete($accountnumber);
			$new_scheduler = $account->new_scheduler_delete($accountnumber);
			$payment_for = $account->payment_for_delete($accountnumber);
			$payment_types = $account->payment_types_delete($accountnumber);
			$pos_new_inventory = $account->pos_new_inventory_delete($accountnumber);
			$pos_new_inventory_types = $account->pos_new_inventory_types_delete($accountnumber);
			$pos_new_sale = $account->pos_new_sale_delete($accountnumber);
			$range_lanes = $account->range_lanes_delete($accountnumber);
			$range_time_tracker = $account->range_time_tracker_delete($accountnumber);
			$server_email_templates = $account->server_email_templates_delete($accountnumber);
			$special_events = $account->special_events_delete($accountnumber);
			$certs_and_safety = $account->certs_and_safety_delete($accountnumber);
			
			return $app->redirect($app['get_admin_url']('admin/account'));				
		}  
		public function getfilterDataJson(Application $app){        	
			
				$fcn=$app['request']->get('filter_company_name');
				$fcn=(!empty($fcn)) ? $fcn : false;
				
				$fas=$app['request']->get('account_status');
				$fas=(!empty($fas)) ? $fas : false;
		
				$data =	array(
						'company_name'=>$fcn,
						'account_status'=>$fas,
			    );		
				
			  $account = $app['admin_account'];
			  $records =  $account->getNewslettersjson($data);			
        }
		public function suspendAccount(Application $app, $accountnumber)
		{	
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
				
			$account = $app['admin_account'];
			$get_blog = $account->suspendAccount($accountnumber);
			return $app->redirect($app['get_admin_url']('admin/account'));				
		}
		public function activeAccount(Application $app, $accountnumber)
		{	
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
				
			$account = $app['admin_account'];
			$get_blog = $account->activeAccount($accountnumber);
			return $app->redirect($app['get_admin_url']('admin/account'));				
		} 
		public function trialAccount(Application $app, $accountnumber)
		{	
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
				
			$account = $app['admin_account'];
			$get_blog = $account->trialAccount($accountnumber);
			return $app->redirect($app['get_admin_url']('admin/account'));				
		}  
    }
}
