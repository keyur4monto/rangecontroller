<?php
namespace Rc\Controllers\Admin {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
	use Rc\Models\admin_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class newslettersController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-admin.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app)
		{
			$newsletter = $app['admin_newsletter'];
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			
			$get_newsletter = "";
			$get_newsletter = $newsletter->getNewsletter($customer_number);
			
			$data = array(                
			'get_newsletter' => $get_newsletter   
            );
			
			$breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Admin - Newsletters';
			$content = $app['templating']->render('admin/adminnewsletters/list.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
			return array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
                array('title'=>'Newsletters')
            );
        }
		private function wrapInLayout(Application $app, $data){
           
            $breadcrumbsContent = $app['templating']->render(
                    'admin/adminnewsletters/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content']				
            );
            return $app['templating']->render('admin/layout-admin.php', $vars);
        }
		public function addNewsletters(Application $app)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			$errorMessage = '';
			if ($app['request']->get('cmd', '') === 'add') {
                
				$company_name=$app['request']->get('company_name', '');
				$subject=$app['request']->get('subject', '');
				$message_body=$app['request']->get('txtContent', '');
				$selEmailAddresses=$app['request']->get('selEmailAddresses', '');
				$receivers  = $app['request']->get('receivers', '');
				$receiver = json_encode($receivers);
				$everyone=$app['request']->get('everyone', '');
				
				if(empty($subject) || empty($message_body))
					$errorMessage="Please fill out all of the required fields marked with *";
                if (!$errorMessage) {	
				$newsletter = $app['admin_newsletter'];
				$admin_newsletter_email = $app['admin_newsletter_email'];
				
				$get_newsletter = $newsletter->addNewsletter($company_name, $subject, $message_body);
				$email_from = "sales@rangecontroller.com";
				$headers = 'From: '.$email_from."\r\n".
				'Reply-To: '.$email_from."\r\n".
				'MIME-Version: 1.0'."\r\n".
				'Content-Type: text/html; charset=ISO-8859-1'."\r\n";
				if($everyone == "everyone"){
					$get_email = $admin_newsletter_email->getEmail();
					foreach($get_email as $row)
					{
						$emailids[] = $row['username'];
					}
					
					$everyone = json_encode($emailids);
					$get_last_id = $newsletter->getNewsletterLastId();
					$set_emails = $newsletter->addEmail($everyone, $get_last_id['newsletter_id']);
					foreach($get_email as $row)
					{
						mail($row['registered_email'], $subject, $message_body, $headers);
						//mail('chintanh4monto@gmail.com', $subject, $message_body, $headers);
					}
					echo "<script>alert('Newsletter sent to email(s) successfully.');window.location='http://www.shooter.dev/xapp/admin/newsletters';</script>";
				}
				else
				{
					$get_last_id = $newsletter->getNewsletterLastId();
					$set_emails = $newsletter->addEmail($receiver, $get_last_id['newsletter_id']);
					foreach($receivers as $row)
					{
						$get_email = $admin_newsletter_email->getEmailById($row);
						mail($get_email['registered_email'], $subject, $message_body, $headers);
						//mail('chintanh4monto@gmail.com', $subject, $message_body, $headers);
					}
					echo "<script>alert('Newsletter sent to email(s) successfully.');window.location='http://www.shooter.dev/xapp/admin/newsletters';</script>";
				}
				return $app->redirect($app['get_admin_url']('admin/newsletters'));
				}
			}
			$breadcrumbs = array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
				array('link' => $app['get_admin_url']('').'admin/newsletters', 'title'=>' Newsletters'),
                array('title'=>'Add')
            );
			$data = array(
                'errorMessage' => $errorMessage,
				'allmaillist' => '' 
            );
            $pageTitle = 'Range Controller - Admin - Newsletters - Add';
			$content = $app['templating']->render('admin/adminnewsletters/add.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        } 
		public function manageNewsletters(Application $app, $newsletter_id)
		{
			$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
			$newsletter = $app['admin_newsletter'];
			$errorMessage = '';
			
			if ($app['request']->get('cmd', '') === 'edit') 
			{
				$company_name=$app['request']->get('company_name', '');
				$subject=$app['request']->get('subject', '');
				$message_body=$app['request']->get('txtContent', '');
				$newsletter_id=$app['request']->get('newsletter_id', '');
				$receivers  = $app['request']->get('receivers', '');
				$receiver = json_encode($receivers);
				$everyone=$app['request']->get('everyone', '');
				
				if(empty($subject) || empty($message_body))
					$errorMessage="Please fill out all of the required fields marked with *";
				if (!$errorMessage) {	
				$newsletter = $app['admin_newsletter'];
				$admin_newsletter_email = $app['admin_newsletter_email'];
				$get_newsletter = $newsletter->editNewsletter($newsletter_id, $subject, $message_body);
				$email_from = "sales@rangecontroller.com";
				$headers = 'From: '.$email_from."\r\n".
				'Reply-To: '.$email_from."\r\n".
				'MIME-Version: 1.0'."\r\n".
				'Content-Type: text/html; charset=ISO-8859-1'."\r\n";
				if($everyone == "everyone"){
					$get_email = $admin_newsletter_email->getEmail();
					foreach($get_email as $row)
					{
						$emailids[] = $row['username'];
					}
					
					$everyone = json_encode($emailids);
					$set_emails = $newsletter->updateEmail($everyone, $newsletter_id);
					foreach($get_email as $row)
					{
						mail($row['registered_email'], $subject, $message_body, $headers);
						//mail('chintanh4monto@gmail.com', $subject, $message_body, $headers);
					}
					echo "<script>alert('Newsletter sent to email(s) successfully.');window.location='newsletters';</script>";
				}
				else
				{
					$set_emails = $newsletter->updateEmail($receiver, $newsletter_id);
					foreach($receivers as $row)
					{
						$get_email = $admin_newsletter_email->getEmailById($row);
						mail($get_email['registered_email'], $subject, $message_body, $headers);
						//mail('chintanh4monto@gmail.com', $subject, $message_body, $headers);
					}
					echo "<script>alert('Newsletter sent to email(s) successfully.');window.location='newsletters';</script>";
				}
				return $app->redirect($app['get_admin_url']('admin/newsletters'));
				}
			}
			$get_newsletter = "";
			$get_newsletter = $newsletter->getNewsletterId($newsletter_id);
			$get_email_lists = $newsletter->getEmailLists($newsletter_id);
			
			foreach($get_email_lists as $e_id)
			{	
				$email_name = $newsletter->getEmailName($e_id);	
			}
	
			$data = array(                
			'get_email_lists' => $get_email_lists,
			'get_newsletter' => $get_newsletter,
			'email_name' => $email_name,
			'errorMessage' => $errorMessage   
            );
			
			$breadcrumbs = array(
                array('link' => $app['get_admin_url']('').'admin/dashboard', 'title'=>'Admin'),
				array('link' => $app['get_admin_url']('').'admin/newsletters', 'title'=>' Newsletters'),
                array('title'=>'Manage')
            );
            $pageTitle = 'Range Controller - Admin - Newsletters - Manage';
			$content = $app['templating']->render('admin/adminnewsletters/manage.php', $data);
			return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
      }
	  
	  public function deleteNewsletter (Application $app, $newsletter_id)
	  {		
	  		$roid = $app['admin_owner_id'];
			$customer_number = $roid->getcustomer_number();	
	  		$newsletter = $app['admin_newsletter'];
			$get_newsletter = $newsletter->deleteNewsletter($newsletter_id);
   			return $app->redirect($app['get_admin_url']('admin/newsletters'));				
	 }  
	 public function getfilterDataJson(Application $app){        	
			
				$dt=$app['request']->get('filter_date');				
				$ndt=(!empty($dt)) ? date( 'Y-m-d', strtotime($dt)) : false;
				$fm=$app['request']->get('filter_month');
				$fm=(!empty($fm)) ? $fm : false;
				$fy=$app['request']->get('filter_year');
				$fy=(!empty($fy)) ? $fy : false;
				$data =	array(
						'timestamp'=>$ndt,
						'month'=>$fm,
						'year'=>$fy
					);				
			//
			$records =  $app['admin_newsletter']->getNewslettersjson($data);			
        }	
		
	 public function getGroupMembersJson(Application $app){
			$pos = array();
			$pos = $_POST;
			$pagedResults =  $app['admin_newsletter_email']->getgrouomemberslist($pos);
     }
		 
    }
}
