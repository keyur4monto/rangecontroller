<?php
namespace Rc\Controllers\Shooter {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\shooter_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\shooter_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class MembersController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-shooter.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
			$memberModel = $app['member'];
			$roid = $app['shooter_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();	
			$member_number = $_SESSION['member_number'];
			
			$getupcomingevents = $memberModel->getupcomingevents($hash, $accno);
			$getupcomingclasses = $memberModel->getupcomingclasses($hash, $accno);
			
			$data = array(                
			'getupcomingevents' => $getupcomingevents,
			'getupcomingclasses' => $getupcomingclasses,
			'shooterLogoUrl' => $app['shooter_logo_url']
            );
			
			$content = $app['templating']->render('shooter/members/list.php', $data);
			$breadcrumbs = $this->getBreadcrumbs($app);
			//$breadcrumbs = '';
            $pageTitle = 'Range Controller - Members Landing Page';
			return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('').'common/logout.php', 'title'=>'LOGOUT'),
                array('title'=>'Member Landing Page')
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['shooter_owner'];
            
            $breadcrumbsContent = $app['templating']->render('shooter/members/breadcrumbs.php',  array('breadcrumbs' => $data['breadcrumbs']));
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                //'content' => $data['content'],
				'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['shooter_logo_url']
            );
            return $app['templating']->render('shooter/layout-shooter2.php', $vars);
        }   
    }
}
