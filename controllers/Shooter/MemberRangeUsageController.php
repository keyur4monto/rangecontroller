<?php
namespace Rc\Controllers\Shooter {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\shooter_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\shooter_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class MemberRangeUsageController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-members.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
			$rangeuses = $app['range_usages'];
			$roid = $app['shooter_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();	
			$member_number = $_SESSION['member_number'];
			$getmembertype = "";
			$getmembertype = $rangeuses->getmembertype($member_number, $hash, $accno);
			while ($rowtype = $getmembertype->fetch_assoc()) {
					$membershiptype=$rowtype['membership_type'];
			}
			$getmemberid = $rangeuses->getmembershiptypeid($membershiptype, $hash, $accno);
			while ($rowtype = $getmemberid->fetch_assoc()) {
					$typeid=$rowtype['membership_type_id'];
			}
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			//today
			$sql = "SELECT * from benefits WHERE account_number='$accno' AND acct_number_hashed='$hash' and membership_type_id='$typeid'";
			$benefits = DbHelper::dbFetchAllAssoc($link, $sql);
			$i=0;
			foreach($benefits as $bef){
				$sql="select sum(amount_used) as used from benefits_used where benefit_id='".$bef['id']."' and customer_id='".$member_number."'";
				$res = DbHelper::dbFetchRowAssoc($link, $sql);
				$benefits[$i]['used']=$res['used'];
				$i++;
			}
			
			//range usages
			$usages=array();
			//today
				$sql = "SELECT start_time, end_time, date_stamp FROM range_time_tracker WHERE membership_number='".$member_number."' AND str_to_date(`date_stamp`,'%m/%d/%y')='".date('n/j/y')."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' ORDER BY date_stamp DESC ";
				$results = DbHelper::dbFetchAllAssoc($link, $sql);
				$starttime=0;
				$endtime=0;
				foreach($results as $result){
					if(empty($result['end_time']) || $result['end_time']=='')
						continue;
					$starttime+=strtotime($result['date_stamp'].' '.$result['start_time']);
					$endtime+=strtotime($result['date_stamp'].' '.$result['end_time']);
				}
				$todaydiff=$endtime-$starttime;
			//week
				$sql = "SELECT start_time, end_time, date_stamp FROM range_time_tracker WHERE membership_number='".$member_number."' AND str_to_date(`date_stamp`,'%m/%d/%y') >= '".date('Y-m-d', time()-604800)."' AND str_to_date(`date_stamp`,'%m/%d/%y') <= '".date('Y-m-d')."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' ORDER BY str_to_date(`date_stamp`,'%m/%d/%y') DESC ";
				$results = DbHelper::dbFetchAllAssoc($link, $sql);
				$starttime=0;
				$endtime=0;
				foreach($results as $result){
					if(empty($result['end_time']) || $result['end_time']=='')
						continue;
					$starttime+=strtotime($result['date_stamp'].' '.$result['start_time']);
					$endtime+=strtotime($result['date_stamp'].' '.$result['end_time']);
				}
				$weekdiff=$endtime-$starttime;
			//month
				$sql = "SELECT start_time, end_time, date_stamp FROM range_time_tracker WHERE membership_number='".$member_number."' AND str_to_date(`date_stamp`,'%m/%d/%y') >= '".date('Y-m-d', time()-(2.63e+6))."' AND str_to_date(`date_stamp`,'%m/%d/%y') <= '".date('Y-m-d')."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' ORDER BY str_to_date(`date_stamp`,'%m/%d/%y') DESC ";
				$results = DbHelper::dbFetchAllAssoc($link, $sql);
				
				$starttime=0;
				$endtime=0;
				foreach($results as $result){
					if(empty($result['end_time']) || $result['end_time']=='')
						continue;
					$starttime+=strtotime($result['date_stamp'].' '.$result['start_time']);
					$endtime+=strtotime($result['date_stamp'].' '.$result['end_time']);
				}
				$monthdiff=$endtime-$starttime;
			//year
				$sql = "SELECT start_time, end_time, date_stamp FROM range_time_tracker WHERE membership_number='".$member_number."' AND str_to_date(`date_stamp`,'%m/%d/%y') >= '".date('Y-m-d', time()-(3.156e+7))."' AND str_to_date(`date_stamp`,'%m/%d/%y') <= '".date('Y-m-d')."' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' ORDER BY str_to_date(`date_stamp`,'%m/%d/%y') DESC ";
				$results = DbHelper::dbFetchAllAssoc($link, $sql);
				
				$starttime=0;
				$endtime=0;
				foreach($results as $result){
					if(empty($result['end_time']) || $result['end_time']=='')
						continue;
						$starttime+=strtotime($result['date_stamp'].' '.$result['start_time']);
						$endtime+=strtotime($result['date_stamp'].' '.$result['end_time']);
				}
				$yeardiff=$endtime-$starttime;
				
				
				$todaybit = array('hour'=>round($todaydiff/3600),'minute'=>$todaydiff/60%60);
				$weekbit = array('hour'=>round($weekdiff/3600),'minute'=>$weekdiff/60%60);
				$monthbit = array('hour'=>round($monthdiff/3600),'minute'=>$monthdiff/60%60);
				$yearbit = array('hour'=>round($yeardiff/3600),'minute'=>$yeardiff/60%60);
			//
			$sql = "SELECT * from benefits WHERE account_number='$accno' AND acct_number_hashed='$hash' and membership_type_id='$typeid'";
			$benefits = DbHelper::dbFetchAllAssoc($link, $sql);
			$i=0;
			foreach($benefits as $bef){
				$sql="select sum(amount_used) as used from benefits_used where benefit_id='".$bef['id']."' and customer_id='".$member_number."'";
				$res = DbHelper::dbFetchRowAssoc($link, $sql);
				$benefits[$i]['used']=$res['used'];
				$i++;
			}
            
            
            //Total Visits
              $sql_visits = "SELECT count(id) as total_visits 
                FROM range_time_tracker 
                WHERE membership_number='$member_number'  
                AND acctnumberhashed = '$hash' 
                AND accountnumber = '$accno' ORDER BY str_to_date(`date_stamp`,'%m/%d/%y') DESC";  
              $results_visits = DbHelper::dbFetchAllAssoc($link, $sql_visits); 
			
			$data = array(                
				'membershiptype' => $membershiptype,
				'benifits' => $benefits,
				'today'=>$todaybit,
                'total_visits'=>$results_visits[0],
				'week'=>$weekbit,
				'month'=>$monthbit,
				'year'=>$yearbit,
            );
			$content = $app['templating']->render('shooter/memberrangeusage/list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Shooter Portal - Range Usage';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('').'members/dashboard', 'title'=>'Home'),
                array('title'=>'Range Usage')
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['shooter_owner'];
            
            $breadcrumbsContent = $app['templating']->render(
                    'shooter/memberrangeusage/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['shooter_logo_url']
            );
            return $app['templating']->render('shooter/layout-shooter2.php', $vars);
        }    
    }
}
