<?php
namespace Rc\Controllers\Shooter {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\shooter_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\shooter_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class UpcomingEventController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-members.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
			$upcomingevents = $app['upcoming_events'];
			$roid = $app['shooter_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();	
			$member_number = $_SESSION['member_number'];
			$getupcomingevents = '';
			$getupcomingevents = $upcomingevents->getupcomingevents($hash, $accno);
			
			$data = array(                
			'getupcomingevents' => $getupcomingevents  
            );
			
			$content = $app['templating']->render('shooter/upcomingevent/list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Shooter Portal - Upcoming Events';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('').'members/dashboard', 'title'=>'Home'),
                array('title'=>'Upcoming Events')
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['shooter_owner'];
            
            $breadcrumbsContent = $app['templating']->render(
                    'shooter/upcomingevent/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['shooter_logo_url']
            );
            return $app['templating']->render('shooter/layout-shooter2.php', $vars);
        }    
    }
}
