<?php
namespace Rc\Controllers\Shooter {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\shooter_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\shooter_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class EditContactInfoController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-members.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$edit_contact_info = $app['edit_contact_info'];
			$roid = $app['shooter_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$member_number = $_SESSION['member_number'];
			$getemplists = $edit_contact_info->getcustomer($hash, $accno, $member_number);
			$getdonotcall = $edit_contact_info->getdonotcall($member_number, $hash, $accno);
			
			$errorMessage = '';
			if ($app['request']->get('cmd', '') === 'change') 
			{
				$email  = $_POST['email'];
				$street_address  = $_POST['street_address'];
				$city     = $_POST['city'];		
				$state     = $_POST['state'];			
				$zipcode    = $_POST['zipcode'];
				$home_phone  = $_POST['home_phone'];
				$call_status  = $_POST['call_status'];
			
				$edit_contact_info->edit_contact_info($email, $street_address, $city, $state, $zipcode, $home_phone, $hash, $accno, $member_number);
				
				if($call_status == "yes")
				{
					$edit_contact_info->insert_do_not_call($hash, $accno, $member_number);
				}
				else
				{
					$edit_contact_info->delete_do_not_call($member_number, $hash, $accno);
				}
				return $app->redirect($app['get_range_url']('members/editContactInfo'));
			}
			
			$data = array(                
			'member' => $getemplists,  
			'do_not_call' => $getdonotcall,
			'errorMessage' => $errorMessage  
            );
			$content = $app['templating']->render('shooter/editcontactinfo/list.php',$data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Shooter Portal - Edit Contact Info';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('').'members/dashboard', 'title'=>'Home'),
                array('title'=>'Edit Contact Info')
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['shooter_owner'];
            
            $breadcrumbsContent = $app['templating']->render(
                    'shooter/editcontactinfo/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['shooter_logo_url']
            );
            return $app['templating']->render('shooter/layout-shooter2.php', $vars);
        }    
    }
}
