<?php
namespace Rc\Controllers\Shooter {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\shooter_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\shooter_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class EventDetailsController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-members.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app, $eventNo){
			$upcomingeventsdetail = $app['upcoming_events_detail'];
			$roid = $app['shooter_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();	
			$member_number = $_SESSION['member_number'];
			$getupcomingeventsdetail = '';
			$getupcomingeventsdetail = $upcomingeventsdetail->getupcomingeventsdetail($eventNo, $hash, $accno);
			
			$data = array(                
			'getupcomingeventsdetail' => $getupcomingeventsdetail  
            );
			
			$content = $app['templating']->render('shooter/eventdetails/list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app, $data);
            $pageTitle = 'Range Controller - Shooter Portal - Upcoming Events - Details';
            return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app, $data){
			
            return array(
                array('link' => $app['get_range_url']('').'members/dashboard', 'title'=>'Home'),
                array('link' => $app['get_range_url']('').'members/upcomingEvents','title'=>'Upcoming Events'),
				array('title'=> $getupcomingeventsdetail.'Date - Time - Event Name')
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['shooter_owner'];
            
           $breadcrumbsContent = $app['templating']->render('shooter/eventdetails/breadcrumbs.php', array('breadcrumbs' => $data['breadcrumbs']));
           
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['shooter_logo_url']
            );
            return $app['templating']->render('shooter/layout-shooter2.php', $vars);
        }    
    }
}
