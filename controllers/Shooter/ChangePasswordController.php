<?php
namespace Rc\Controllers\Shooter {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\shooter_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\shooter_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class ChangePasswordController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-members.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$changepassword = $app['changepassword'];
			$roid = $app['shooter_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$member_number = $_SESSION['member_number'];
			$member_current_pw = $_SESSION['member_current_pw'];
			
			$Message = '';
			if ($app['request']->get('cmd', '') === 'change') 
			{
				$curr_password  = $_POST['curr_password'];
				$new_password     = $_POST['new_password'];			
				$repeat_password    = $_POST['repeat_password'];
			
				if(empty($curr_password) || empty($new_password) || empty($repeat_password))
					$Message="Please fill out all of the required fields marked with *";
					if (!$Message)
					{	
						if($curr_password != $member_current_pw)
						{
							$Message="Old password does not match";
						}
						else if($new_password != $repeat_password)
						{
							$Message="New password and repeat password not match";
						}
						else
						{
							$records= $changepassword->change_pass($repeat_password, $hash, $accno, $member_number);
							$Message="Password Change Successfully";
						}
					}
			 
			}
			$data = array('Message' => $Message);
			$content = $app['templating']->render('shooter/changepassword/list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Shooter Portal - Change Password';
            return $this->wrapInLayout($app,compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('').'members/dashboard', 'title'=>'Home'),
                array('title'=>'Change Password')
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['shooter_owner'];
            
            $breadcrumbsContent = $app['templating']->render(
                    'shooter/changepassword/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['shooter_logo_url']
            );
            return $app['templating']->render('shooter/layout-shooter2.php', $vars);
		}
    }
}
