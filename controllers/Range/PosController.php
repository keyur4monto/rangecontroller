<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
	use Rc\Models\SaleStorage;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class PosController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';;
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
            //$pf = $app['range_url_prefix'];
            //return $app['request']->get('cmd', 'aa') .print_r($pf,1) ;
            
            return $this->indexPage($app);
        }
		
		private function getBreadcrumbs(Application $app){
            return array(
                ['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('').'maintenance-tasks.php', 'title'=>'Point of Sale'],
            );
        }
		
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner; 
            
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $data['breadcrumbs']]);
            
            
            $vars = [
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
        }
        
        private function wrapInMemberLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $data['breadcrumbs']]);
            
            
            $vars = [
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-shooter.php', $vars);
        }
    
        function posMain (Application $app, $customer_id) {
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT tax_rate FROM controller_config WHERE accountnumber='" . $accno . "'";
			        	
        	$arr = DbHelper::dbFetchRowAssoc($link, $sql);
        	
        	$sql = "SELECT current_amount FROM pos_register WHERE accountnumber='" . $accno . "'";
			        	
        	$arr2 = DbHelper::dbFetchRowAssoc($link, $sql);
        	
        	$extraRows = '';
        	
        	for ($i=6;$i<=25;$i++) {
        	
        	$extraRows = $extraRows . '
              <tr class="itemclass" id="row-' . $i . '" style="display:none;">
                <td style="text-align:center;" id="item-' . $i . '-number" >&nbsp;</td>
                <td style="text-align:center;" id="item-' . $i . '-brand" >&nbsp;</td>
                <td style="text-align:center;" id="item-' . $i . '-model" >&nbsp;</td>
                <td style="text-align:center;" id="item-' . $i . '-description" >&nbsp;</td>
                <td style="text-align:center;"><input id="item-' . $i . '-price" style="width:90%;" type="text" class="price"/></td>
                <td style="text-align:center;" style="width:50px;"><input id="item-' . $i . '-qty" style="width:90%;" type="text" class="qty"/><input id="item-' . $i . '-inventory" style="width:90%;" type="hidden"/></td>
                <td style="text-align:center;" id="item-' . $i . '-extended" >$0.00</td>
                <td style="text-align:center;"><input type="checkbox" name="item-' . $i . '-tax-exempt" id="item-' . $i . '-tax-exempt"/></td>
                <td style="text-align:center;" class="text-center" style="font-size: 14px;"><a class="remove" href="#asd">X</a></td>
              </tr>';
              
              }
			  
			  //calculation for the register amount
			  	$sql = "SELECT * FROM pos_register WHERE accountnumber='" . $accno . "'";
				$row = DbHelper::dbFetchRowAssoc($link, $sql);
				$REGISTER=$row['base_amount'];
				/*$sql="SELECT sum(total) as pos_total from pos_new_sale where accountnumber='". $accno . "' and date_stamp='".date('m/d/Y')."'";
				$row = DbHelper::dbFetchRowAssoc($link, $sql);
				$REGISTER+=$row['pos_total'];*/
				/*$sql="SELECT sum(total) as pos_total from pos_new_sale where accountnumber='". $accno . "' and date_stamp='".date('m/d/Y')."'";
				$row = DbHelper::dbFetchRowAssoc($link, $sql);
				$REGISTER+=$row['pos_total'];
				$sql="SELECT sum(payment_amount) as billing_total from billing_info where accountnumber='". $accno . "' and date_stamp='".date('m/d/Y')."'";
				$row = DbHelper::dbFetchRowAssoc($link, $sql);
				$REGISTER+=$row['billing_total'];*/
				//12-3-2015
				$rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' and date_stamp='".date('m/d/Y')."' ORDER BY transaction_number";
				$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
				$pos_transaction_rows = "";
				$pos_grand_total = 0;
				$lastTransactionNo = 0;
				$thisTransactionNo = 0;
				foreach ($assoc_array as $transaction) {
					$thisTransactionNo = $transaction['transaction_number'];
					
					$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $thisTransactionNo . " AND accountnumber='" . $accno . "'";
					$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
					$rowspan = $resultsArr['mycount'];
					
					if ($thisTransactionNo != $lastTransactionNo) $payment_types_acc[$transaction['payment1_method']] += $transaction['total']; 
					
					if ($thisTransactionNo != $lastTransactionNo) $pos_grand_total = $pos_grand_total + ((double) $transaction['total']);
						$lastTransactionNo = $thisTransactionNo;
					
				}
						
				$mem_types_query = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
				$assoc_array = DbHelper::dbFetchAllAssoc($link, $mem_types_query);
				$membership_types = array();
				$membership_types_sub = array();
				$membership_type_names = array('', '', '', '', '', '', '', '', '', '');
				$arr_index = 0;
				
				foreach ($assoc_array as $mem_type) {
					$keyname = $mem_type['membership_name'];
					$membership_type_names[$arr_index] = $keyname;
					$membership_types[$keyname] = 0;//will hold number of memberships sold  for this type		
					$membership_types_sub[$keyname] = 0;
					$arr_index++;
				
				}
				$rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'  and `date`='".date('m/d/Y')."'";
				
				$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
				
				foreach ($assoc_array as $billing) {
				
					if ($billing['payment_for'] != "Class" && $billing['payment_for'] != "Visit") $this_mem = $billing['membership_type'];
					
					else if ($billing['payment_for'] == "Class") $this_mem = 'Student';
					
					else if ($billing['payment_for'] == "Visit") $this_mem = 'Visitor';
				
					$payment_types_acc[$billing['payment_method']] += $billing['payment_amount'];
				
					foreach ($membership_type_names as $memtype) {
				
						if ($this_mem == $memtype) {

							$membership_types[$memtype]++;//add to number of this type sold

							$membership_types_sub[$memtype] = $membership_types_sub[$memtype] + $billing['payment_amount'];

						}

					}
				
				}
						
				$memberships_transaction_rows = "";
				
				$memberships_grand_total = 0;
				
				$memberships_total_count;
				foreach ($membership_type_names as $memtype) {
					$memberships_grand_total = $memberships_grand_total + $membership_types_sub[$memtype];
				}
			
				$drawer_end=number_format(($pos_grand_total + $memberships_grand_total), 2);
				//end
				$REGISTER+=$drawer_end;
			  //
        	
            $data = [
            	'member_no' => $customer_id,
            	'tax_rate' => $arr['tax_rate'],
            	//'register' => $arr2['current_amount'],
				'register' => $REGISTER,
            	'extraRows' => $extraRows,
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/pos/pos.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
			$breadcrumbs[1]['title'] = 'Point of Sale';
			//$breadcrumbs[2]['title'] = 'Login Select Lane';
            $pageTitle = 'Range Controller - Point of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function getTransactionId (Application $app) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT MAX(transaction_number) AS highest FROM pos_new_sale WHERE accountnumber='" . $accno . "'";
			
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$highest = (int) $row['highest'];
			
			$new_transaction_no = (int) $highest + 1;
			
			return "" . $new_transaction_no . ""; 
		
		}
		
		function posPost (Application $app) {
		
		ini_set("display_errors", 1);
  error_reporting("1+2+4");
		
			$inventory_id = $app['request']->get('inventory_id', '');
		
			$transaction_number = $app['request']->get('transaction_number', '');
			
			$item_number = $app['request']->get('item_number', '');
			
			$brand = $app['request']->get('brand', '');
			
			$model = $app['request']->get('model', '');
			
			$description = $app['request']->get('description', '');
			
			$price = $app['request']->get('price', '');
			
			$extended_price = $app['request']->get('extended_price', '');
			
			$quantity = $app['request']->get('quantity', '');
			
			$total = $app['request']->get('total', '');
			
			$discount = $app['request']->get('discount', '');
			
			$discount = ((float) $discount) * -1;
			
			$discount = "" . $discount . "";
			
			$membership_number = $app['request']->get('membership_number', '');
			
			$employee_id = $app['request']->get('employee_id', '');
			
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			//get member name first
			
			$sql = "SELECT * FROM customers WHERE membership_number='" . $membership_number . "' AND accountnumber='" . $accno . "'";
			
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$member_name = $row['first_name'] . " " . $row['last_name'];
			
			
			try {
    			$ss1 = SaleStorage::setSale($app, $transaction_number);
				} catch (Exception $e) {
					echo 'Exception: ',  $e->getMessage(), "\n";
			}
		
			$sql = "INSERT INTO pos_new_sale (inventory_id, acctnumberhashed, accountnumber, transaction_number, membership_number, member_name, date_stamp, item_number, brand, model, description, price, extended_price, discount, total, quantity, employee_id, payment1_method, payment1_amount, payment2_method, payment2_amount, change_due, void, return_item) VALUES ('" . $inventory_id . "', '" . $hash . "', '" . $accno . "', '" . $transaction_number . "', '" . $membership_number . "', '" . $member_name . "', '" . date("m/d/Y") . "', '" . $item_number . "', '" . $brand . "', '" . $model . "', '" . $description . "', '" . $price . "', '" . $extended_price . "', '" . $discount . "', '" . $total . "', '" . $quantity . "', '" . $employee_id . "', '0', '0', '0', '0', '0', 'empty', 'empty')";
			
			//$link->query($sql);
			
			return $sql;
			
		}
		
		function posReceipt (Application $app, $transaction_id) {
		
		ini_set("display_errors", 1);
  error_reporting("1+2+4");
		
			
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM pos_new_sale INNER JOIN employees ON pos_new_sale.employee_id = employees.id INNER JOIN customers ON pos_new_sale.membership_number = customers.membership_number WHERE pos_new_sale.accountnumber='" . $accno . "' AND transaction_number='" . $transaction_id . "' GROUP BY pos_new_sale.id";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$receiptRows = "";
			
			$subtotal = (float) 0.00;
			
			foreach ($results as $row) {
			
			if ($row['processed'] == 'NO') {
				
				$sql = "UPDATE pos_new_inventory SET quantity=quantity - " . $row['quantity'] . " WHERE id=" . $row['inventory_id'] . "";
			
				$link->query($sql);
			
				$sql = "UPDATE pos_register SET current_amount = current_amount + " . $row['total'] . " WHERE accountnumber='" . $accno . "'";
			
				$link->query($sql);
				
				$sql = "UPDATE pos_new_sale SET processed = 'YES' WHERE transaction_number='" . $transaction_id . "' AND accountnumber='" . $accno . "'";
			
				$link->query($sql);
				
			}
			
				$date_stamp = $row['date_stamp'];
				
				$employee_name = $row['fname'] . " " . $row['lname'];
				
				$customer_name = $row['first_name'] . " " . $row['last_name'];

				$street_address = $row['street_address'];
				
				$city = $row['city'];
				
				$state = $row['state'];
				
				$zipcode = $row['zipcode'];
				
				$subtotal = $subtotal + (float) $row['extended_price'];
				
				$discount = number_format($row['discount'], 2);
			
				$total = number_format(floatval($row['total']) , 2);
			
				$change = number_format(floatval($row['change_due']) , 2);
			
				$payment1_method = $row['payment1_method'];
				
				$payment2_method = $row['payment2_method'];
			
				$receiptRows = $receiptRows . '<tr>
                  	<td>' . $row['brand'] . '</td>
                  	<td>' . $row['model'] . '</td>
                  	<td>' . $row['description'] . '</td>
                  	<td>' . $row['price'] . '</td>
                  	<td>' . $row['quantity'] . '</td>
                  	<td>' . $row['extended_price'] . '</td>
                	</tr>';
                	
                
			
			}
			
			$subtotal2 = $subtotal - $discount;
			
			$subtotal = number_format($subtotal, 2);
			
			$subtotal2 = number_format($subtotal2, 2);
			
			$tax = $total - $subtotal2;
			
			$tax = number_format($tax, 2);
			
            $data = [
            	'date_stamp' => $date_stamp,
            	'employee_name' => $employee_name,
            	'customer_name' => $customer_name,
            	'street_address' => $street_address,
            	'city' => $city,
            	'state' => $state,
            	'zipcode' => $zipcode,
            	'subtotal' => $subtotal,
            	'subtotal2' => $subtotal2,
            	'discount' => $discount,
            	'total' => $total,
            	'change' => $change,
            	'payment1_method' => $payment1_method,
            	'payment2_method' => $payment2_method,
            	'receiptRows' => $receiptRows,
            	'transaction_number' => $transaction_id,
            	'tax' => $tax,
            ];
            
            $content = $app['templating']->render('range/pos/receipt.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/pointOfSale/0';
			$breadcrumbs[1]['title'] = 'Point of Sale';
			$breadcrumbs[2]['title'] = 'Receipt';
            $pageTitle = 'Range Controller - Point of Sale - Receipt';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function posReceiptPrint (Application $app, $transaction_id) {
			ini_set("display_errors", 1);
  error_reporting("1+2+4");
		
		
	
		
		$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$msql = "SELECT * FROM controller_config WHERE accountnumber='" . $accno . "'";
			
			$configs = DbHelper::dbFetchRowAssoc($link, $msql);
			
			$email = $configs['registered_email'];
			$companyname = $configs['company_name'];
			$company_address = $configs['company_address'];
			$company_city = $configs['company_city'];
			$company_state = $configs['company_state'];
			$company_zipcode = $configs['company_zipcode'];
			$company_phone = $configs['company_phone'];
			
			$statesall = array('alabama'=>'AL','alaska'=>'AK','arizona'=>'AZ','arkansas'=>'AR','california'=>'CA','colorado'=>'CO','connecticut'=>'CT','delaware'=>'DE','florida'=>'FL', 'georgia'=>'GA','hawaii'=>'HI','idaho'=>'ID','illinois'=>'IL','indiana'=>'IN','iowa'=>'IA','kansas'=>'KS','kentucky'=>'KY','louisiana'=>'LA','maine'=>'ME','maryland'=>'MD','massachusetts'=>'MA', 'michigan'=>'MI','minnesota'=>'MN','mississippi'=>'MS','missouri'=>'MO','montana'=>'MT','nebraska'=>'NE','nevada'=>'NV','new hampshire'=>'NH','new jersey'=>'NJ','new mexico'=>'NM','new york'=>'NY','north carolina'=>'NC','north dakota'=>'ND','ohio'=>'OH','oklahoma'=>'OK','oregon'=>'OR','pennsylvania'=>'PA','rhode island'=>'RI','south carolina'=>'SC','south dakota'=>'SD','tennessee'=>'TN','texas'=>'TX','utah'=>'UT','vermont'=>'VT','virginia'=>'VA','washington'=>'WA','west virginia'=>'WV','wisconsin'=>'WI','wyoming'=>'WY');
			foreach ($statesall as $key => $value) { if($company_state == $key) $company_state = $value;} 
						
			$csql = "SELECT * FROM pos_register WHERE accountnumber='" . $accno . "'";

			$rowcomm = DbHelper::dbFetchRowAssoc($link, $csql);			
						
			$sql = "SELECT * FROM pos_new_sale INNER JOIN employees ON pos_new_sale.employee_id = employees.id INNER JOIN customers ON pos_new_sale.membership_number = customers.membership_number WHERE pos_new_sale.accountnumber='" . $accno . "' AND transaction_number='" . $transaction_id . "' GROUP BY pos_new_sale.id";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$receiptRows = "";
			
			$subtotal = (float) 0.00;
			
			foreach ($results as $row) {
							
				$date_stamp = $row['date_stamp'];
				
				$employee_name = $row['fname'] . " " . $row['lname'];
				
				$customer_name = $row['first_name'] . " " . $row['last_name'];

				$street_address = $row['street_address'];
				
				$city = $row['city'];
				
				$state = $row['state'];
				
				$zipcode = $row['zipcode'];
				
				$subtotal = $subtotal + (float) $row['extended_price'];
				
				$discount = number_format($row['discount'], 2);
			
				$total = number_format(floatval($row['total']) , 2);
			
				$change = number_format(floatval($row['change_due']) , 2);
			
				$payment1_method = $row['payment1_method'];
				
				$payment2_method = $row['payment2_method'];
			
				/*$receiptRows = $receiptRows . '<tr>
                  	<td>' . $row['brand'] . '</td>
                  	<td>' . $row['model'] . '</td>
                  	<td>' . $row['description'] . '</td>
                  	<td>' . $row['price'] . '</td>
                  	<td>' . $row['quantity'] . '</td>
                  	<td>' . $row['extended_price'] . '</td>
                	</tr>';
                	*/
				$receiptRows = $receiptRows . '<tr>
      <td><!--Inventory #-->'.$row['item_number'].'</td>
      <td style="text-align: right;"><!--Qty-->'.$row['quantity'].' @</td>
      <td style="text-align: right;"><!--Price-->'.$row['price'].'</td>
    </tr>
    <tr>
      <td colspan="3"><!--Brand + Model-->'.$row['brand'].' '.$row['model'].'</td>
    </tr>
    <tr>
      <td colspan="3"><!--Serial #-->'.$row['serial_number'].'</td>
    </tr>
    <tr>
      <td colspan="3"><!--Description-->'.$row['description'].'</td>
    </tr>
    <tr>
      <td style="padding-bottom: 15px;">&nbsp;</td>
    </tr>';	
					
                
			
			}
			
			$subtotal2 = $subtotal - $discount;
			
			$subtotal = number_format($subtotal, 2);
			
			$subtotal2 = number_format($subtotal2, 2);
			
			$tax = $total - $subtotal2;
			
			$tax = number_format($tax, 2);
			
		echo '<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Untitled Document</title>
<style type="text/css">
body {
	margin: 0 auto;
}
table, th, td {
	/*border: 1px solid black;*/
font-size: 19px;
}
@media print {
body {
	margin: 0;
}
div {
	width: 100%;
	margin: 0;
}
}
</style>
</head>

<body>
<div style="width: 300px; /*border: 1px solid black;*/">
  <table style="width: 100%;">
    <tr>
      <th colspan="3" style="padding-bottom: 15px;"><!--Company Name-->'.$companyname.'<br>
        
        <!--Company Address-->'.$company_address.'<br>
        
        <!--Company City, State, Zip-->'.$company_city.', '.$company_state.' '.$company_zipcode.'<br>
        
        <!--Company Phone-->'.$company_phone.'</th>
    </tr>
    <tr>
      <td colspan="2" style="padding-bottom: 15px;"><!--Transaction #-->Transaction #'.$transaction_id.'</td>
      <td style="text-align: right; padding-bottom: 15px;"><!--Date Stamp-->'.$date_stamp.'</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3"><!--Employee-->Employee: '.$employee_name.' </td>
    </tr>
    <tr>
      <td colspan="3"><!--Customer-->Customer: '.$customer_name.' </td>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>'.$receiptRows.'
     </table>
  <table style="width: 100%;">
    <tr>
      <td style="text-align: right; width: 70%;">Subtotal: </td>
      <td style="padding-left: 10px;">$ '.$subtotal.'</td>
    </tr>
    <tr>
      <td style="text-align: right;">Discount: </td>
      <td style="padding-left: 10px;">- $ '.$discount.'</td>
    </tr>
    <tr>
      <td style="text-align: right;">Subtotal 2: </td>
      <td style="padding-left: 10px;">$ '.$subtotal2.'</td>
    </tr>
    <tr>
      <td style="text-align: right;">Tax: </td>
      <td style="padding-left: 10px;">$ '.$tax.'</td>
    </tr>
    <tr>
      <td style="text-align: right;">Total: </td>
      <td style="padding-left: 10px;">$ '.$total.'</td>
    </tr>
    <tr>
      <td style="text-align: right;">Change Due: </td>
      <td style="padding-left: 10px;">$ '.$change.'</td>
    </tr>
    <tr>
      <td style="text-align: right;">Payment 1: </td>
      <td style="padding-left: 10px;">'.$payment1_method.'</td>
    </tr>
    <tr>
      <td style="text-align: right;">Payment 2: </td>
      <td style="padding-left: 10px;">'.$payment2_method.'</td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><!--POS Receipt Comments-->'.$rowcomm['comments'].'</td>
    </tr>
  </table>
</div>
</body>
</html>';
          die();
		
		}
		
		
		
		function posPayment (Application $app, $transaction_id) {
		ini_set("display_errors", 1);
  error_reporting("1+2+4");
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$sale = $app['session']->get('sale');
					
			//$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			//$sql = "SELECT * FROM pos_new_sale WHERE transaction_number='" . $transaction_id . "' AND accountnumber='" . $accno . "'";
			
			//$row = DbHelper::dbFetchRowAssoc($link, $sql);
			
            $data = [
			//'temp' => $range_owner,
				'sale_amount' => $_SESSION['sale'.$transaction_id][0]['total'],
				'transaction_number' => $transaction_id,
            ];
            
            $content = $app['templating']->render('range/pos/payment.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/pointOfSale/0';
			$breadcrumbs[1]['title'] = 'Point of Sale';
			$breadcrumbs[2]['title'] = 'Payment';
            $pageTitle = 'Range Controller - Point of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		
		function postPayment (Application $app) {
		ini_set("display_errors", 1);
  error_reporting("1+2+4");
        
        	$transaction_number = $app['request']->get('transaction_number', '');
		
			$payment1type = $app['request']->get('payment1type', '');
			
			$payment1amount = $app['request']->get('payment1amount', '');
			
			$payment2type = $app['request']->get('payment2type', '');
			
			$payment2amount = $app['request']->get('payment2amount', '');
			
			$amountpaid = $app['request']->get('amountpaid', '');
			
			$change = $app['request']->get('change', '');
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$ss1 = SaleStorage::storeSale($app, $transaction_number);
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "UPDATE pos_new_sale SET processed='NO', payment1_method='" . $payment1type . "', payment1_amount = '" . $payment1amount . "', payment2_method='" . $payment2type . "', payment2_amount='" . $payment2amount . "', change_due='" . $change . "' WHERE transaction_number='" . $transaction_number . "'";
			
			$link->query($sql);
			
            return $sql;
					
		
		}
		
		
		
		function inventoryMain (Application $app) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			//
			
			$sql = "SELECT * FROM pos_new_inventory_types WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$inventory_types = "";
			
			foreach ($results as $row) {
			
				$inventory_types = $inventory_types . '<option value="' . $row['inventory_type'] . '">' . $row['inventory_type'] . '</option>';
			
			}
			
			//
			
			$sql = "SELECT * FROM pos_new_inventory WHERE accountnumber='" . $accno . "' ORDER BY inventory_number";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$inventoryRows = "";
			
			foreach ($results as $row) {
			
			if($row['serial_number'] == '' || $row['caliber_gauge'] == ' ') $row['serial_number'] = 'NA';
			if($row['color'] == '' || $row['caliber_gauge'] == ' ') $row['color'] = 'NA';
			if($row['barrels_length'] == '' || $row['caliber_gauge'] == ' ') $row['barrels_length'] = 'NA';
			if($row['caliber_gauge'] == '' || $row['caliber_gauge'] == ' ') $row['caliber_gauge'] = 'NA';
			//echo "<!--".var_dump($row['caliber_gauge'])."-->";
			
				$inventoryRows = $inventoryRows . '<tr style="font-size: 14px;">
                <td>' . $row['type'] . '</td>
                <td nowrap="0" style="font-size:14px;">' . $row['item_number'] . '</td>
                <td>' . $row['brand'] . '</td>
                <td>' . $row['model'] . '</td>
                <td>' . $row['description'] . '</td>
                <td>' . $row['quantity'] . '</td>
                <td>' . $row['resale'] . '</td>';
				$description = $row['description'];
				$description= str_replace("/", "%252F", $description);
				$inventoryRows .='
                <td style="width: 7%;"><a href="inventory/manage/' . $row['id'] . '">Manage</a></td>
                <td style="width: 6%;"><a href="inventory/printbarMain/' . $row['item_number'] . '/' . htmlspecialchars ($row['type']) . '/' . htmlspecialchars ($row['brand']) . '/' . htmlspecialchars ($row['model']) . '/' . 
					htmlspecialchars ($description) . '/' . htmlspecialchars ($row['serial_number']) . '/'.htmlspecialchars ($row['color']) .'/'.htmlspecialchars ($row['resale']).'/'.htmlspecialchars ($row['barrels_length']).'/'.
					htmlspecialchars ($row['caliber_gauge']).'" target="_blank">Label</a></td>
                <td style="width: 7%;"><a href="#none" onclick="deleteItem(' . $row['id'] . ')">Delete</a></td>
              </tr>';
			
			}
			
            $data = [
            	'inventoryRows' => $inventoryRows,
            	'inventory_types' => $inventory_types,
            ];
            
            $content = $app['templating']->render('range/pos/inventory.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/pointOfSale/0';
            $breadcrumbs[1]['title'] = 'Point Of Sale';
            $breadcrumbs[2]['title'] = 'Inventory';
			
			//$breadcrumbs[2]['title'] = 'Login Select Lane';
            $pageTitle = 'Range Controller - Point of Sale - Inventory';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		
		function inventoryBulkPost (Application $app) {
			ini_set("display_errors", 1);
			error_reporting("1+2+4");
		
			
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			//print_r($app['request']);
			//phpinfo();
			//die();
			//print_r($_POST);
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT inventory_number FROM pos_new_inventory WHERE accountnumber='" . $accno . "' ORDER BY inventory_number";
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			foreach ($results as $row) {
			
			$postFlag = $row['inventory_number']."_";
			
			try {
			
			$rowtemp = array();
			
			$rowtemp['type'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'inventory_type']);
			$rowtemp['item_number'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'item_number']);
			$rowtemp['brand'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'brand']);
			$rowtemp['model'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'model']);
			$rowtemp['description'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'description']);
			$rowtemp['upc'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'upc']);
			$rowtemp['min_quantity'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'min_quantity']);
			$rowtemp['quantity'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'quantity']);
			$rowtemp['cost'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'cost']);
			$rowtemp['resale'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'resale']);
			$rowtemp['profit_margin'] = DbHelper::dbEscapeStr($link, $_POST[$postFlag.'profit_margin']);
			/*
			$rowtemp['type'] = $app['request']->get($postFlag.'inventory_type');
			$rowtemp['item_number'] = $app['request']->get($postFlag.'item_number');
			$rowtemp['brand'] = $app['request']->get($postFlag.'brand');
			$rowtemp['model'] = $app['request']->get($postFlag.'model');
			$rowtemp['description'] = $app['request']->get($postFlag.'description');
			$rowtemp['quantity'] = $app['request']->get($postFlag.'quantity');
			$rowtemp['cost'] = $app['request']->get($postFlag.'cost');
			$rowtemp['resale'] = $app['request']->get($postFlag.'resale');
			$rowtemp['profit_margin'] = $app['request']->get($postFlag.'profit_margin');
			*/
			//print_r($rowtemp);
			
		
			$sql = "UPDATE pos_new_inventory SET 
				type = '".$rowtemp['type']."', 
				cost='" . $rowtemp['cost'] . "', 
				min_quantity='" . $rowtemp['min_quantity'] . "', 
				resale='" . $rowtemp['resale'] . "', 
				profit_margin = '" . $rowtemp['profit_margin'] . "', 
				quantity = '" . $rowtemp['quantity'] . "'
			WHERE accountnumber='" . $accno . "' AND inventory_number=" . $row['inventory_number'];
			
			$link->query($sql);
			//echo $sql."\r\n";
			//echo $postFlag.'inventory_type',$postFlag.'item_number',"\r\n";
			} catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";
				}
			
						
			}
			
			return "Called!";
			
		}
		
		
		function inventoryBulkMain (Application $app) {
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$sql = "SELECT * FROM pos_new_inventory_types WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$inventory_types = "";
			
			foreach ($results as $row) {
			
				$inventory_types = $inventory_types . '<option value="' . $row['inventory_type'] . '">' . $row['inventory_type'] . '</option>';
			}
			
			$typeslist = $results;
			
			$sql = "SELECT * FROM pos_new_inventory WHERE accountnumber='" . $accno . "' ORDER BY inventory_number";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$inventoryRows = "";
			
			foreach ($results as $row) {
			
				$inventory_types_temp = "";
				foreach ($typeslist as $typerow){
					if($typerow['inventory_type'] == $row['type']){
					$inventory_types_temp = $inventory_types_temp . '<option value="' . $typerow['inventory_type'] . '" selected>' . $typerow['inventory_type'] . '</option>';
					}
					else {
					$inventory_types_temp = $inventory_types_temp . '<option value="' . $typerow['inventory_type'] . '">' . $typerow['inventory_type'] . '</option>';
					}
				}
				
				$costv = (is_numeric($row['cost'])) ? number_format($row['cost'],2,'.','') : $row['cost'];
				$resalev = (is_numeric($row['resale'])) ? number_format($row['resale'],2,'.','') : $row['resale'];
			    //$costv = $row['cost'];
				//$resalev = $row['resale'];
			
				$inventoryRows = $inventoryRows . '
				<tr id ="inventory_number_'.$row['inventory_number'].'" style="font-size: 13px; text-align: center;">
                <td><select name="'.$row['inventory_number'].'_inventory_type" style="width: 70px;">
                 	'.$inventory_types_temp.'
                  </select></td>
                <td nowrap="">'.$row['item_number'].'</td>
                <td>'.$row['brand'].'</td>
                <td>'.$row['model'].'</td>
                <td>'.$row['description'].'</td>
				<td>'.$row['upc'].'</td>
				<td><input name="'.$row['inventory_number'].'_min_quantity" type="text" value="'.$row['min_quantity'].'" style="width: 35px;"></td>
                <td><input name="'.$row['inventory_number'].'_quantity" type="text" value="'.$row['quantity'].'" style="width: 35px;"></td>
                <td><input name="'.$row['inventory_number'].'_cost" type="text" value="'.$costv.'" style="width: 40px;"></td>
                <td><input name="'.$row['inventory_number'].'_resale" type="text" value="'.$resalev.'" style="width: 40px;">
				<input name="'.$row['inventory_number'].'_profit_margin" type = "hidden" value ="'.$row['profit_margin'].'">
				</td>
				<td><input name="inventory_delete" id="inventory_delete" class="inventory_delete" type="checkbox" value="'.$row['id'].'" style="width: 10px;"></td>
              </tr>';
			
			}
			
            $data = [
            	'inventoryRows' => $inventoryRows,
            	'inventory_types' => $inventory_types,
            ];
            
            $content = $app['templating']->render('range/pos/inventory_bulk.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/pointOfSale/0';
            $breadcrumbs[1]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/inventory';
            $breadcrumbs[2]['title'] = 'Inventory';
			$breadcrumbs[3]['title'] = 'Inventory Bulk Edit';
			
			//$breadcrumbs[2]['title'] = 'Login Select Lane';
            $pageTitle = 'Range Controller - Point of Sale -  Inventory - Bulk Edit ';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function inventoryManage (Application $app, $inventory_id) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			//
			
			$sql = "SELECT * FROM pos_new_inventory_types WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$inventory_types = "";
			
			foreach ($results as $row) {
			
				$inventory_types = $inventory_types . '<option value="' . $row['inventory_type'] . '">' . $row['inventory_type'] . '</option>';
			
			}
			
			//
			
			$sql = "SELECT * FROM pos_new_inventory INNER JOIN employees ON employees.id = pos_new_inventory.employee_id WHERE pos_new_inventory.accountnumber='" . $accno . "' AND pos_new_inventory.id='" . $inventory_id . "'";
			
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			
            $data = [
				'brand' => $row['brand'],
				'model' => $row['model'],
				'serial' => $row['serial_number'],
				'color' => $row['color'],
				'cost' => $row['cost'],
				'resale' => $row['resale'],
				'profit' => $row['profit_margin'],
				'action' => $row['action'],
				'finish' => $row['finish'],
				'length' => $row['barrels_length'],
				'calibre' => $row['caliber_gauge'],
				'upc' => $row['upc'],
				'description' => $row['description'],
				'item_number' => $row['item_number'],
				'inventory_number' => $row['inventory_number'],
				'quantity' => $row['quantity'],
				'minquantity' => $row['min_quantity'],
				'new' => $row['new_item'],
				'used' => $row['used_item'],
				'consignment' => $row['consignment'],
				'rental' => $row['rental'],
				'type' => $row['type'],
				'timestamp' => $row['timestamp'],
				'added_by' => $row['fname'],
				'inventory_types' => $inventory_types,
				'id' => $inventory_id,
            ];
            
            $content = $app['templating']->render('range/pos/inventory_manage.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/pointOfSale/0';
            $breadcrumbs[1]['title'] = 'Point Of Sale';
            $breadcrumbs[2]['link'] = '/xapp/inventory';
            $breadcrumbs[2]['title'] = 'Inventory';
			$breadcrumbs[3]['title'] = 'Manage';
			//$breadcrumbs[2]['title'] = 'Login Select Lane';
            $pageTitle = 'Range Controller - Point of Sale - Manage Inventory';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function inventoryAdd (Application $app) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM pos_new_inventory_types WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$inventory_types = "";
			
			foreach ($results as $row) {
			
				$inventory_types = $inventory_types . '<option value="' . $row['inventory_type'] . '">' . $row['inventory_type'] . '</option>';
			
			}
			
            $data = [
            	'inventory_types' => $inventory_types,
            ];
            
            $content = $app['templating']->render('range/pos/inventory_add.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/pointOfSale/0';
            $breadcrumbs[1]['title'] = 'Point Of Sale';
            $breadcrumbs[2]['link'] = '/xapp/inventory';
            $breadcrumbs[2]['title'] = 'Inventory';
			$breadcrumbs[3]['title'] = 'Add';
			//$breadcrumbs[2]['title'] = 'Login Select Lane';
            $pageTitle = 'Range Controller - Point of Sale - Add Inventory';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));		
		
		}
		
		function inventoryAddPost (Application $app) {
		
			//return "Called!";
		
			$item_number = $app['request']->get('item_number', '');
			
			$minquantity = $app['request']->get('minquantity', '');
			
			$brand = $app['request']->get('brand', '');
			
			$model = $app['request']->get('model', '');
			
			$serial = $app['request']->get('serial', '');
			
			$color = $app['request']->get('color', '');
			
			$cost = $app['request']->get('cost', '');
			
			$resale = $app['request']->get('resale', '');
			
			$profit = $app['request']->get('profit', '');
			
			$action = $app['request']->get('action', '');
			
			$finish = $app['request']->get('finish', '');
			
			$calibre = $app['request']->get('calibre', '');
			
			$upc = $app['request']->get('upc', '');
			
			$description = $app['request']->get('description', '');
			
			$quantity = $app['request']->get('quantity', '');
						
			$inventory_type = $app['request']->get('inventory_type', '');
			
			$new = $app['request']->get('new', '');
			
			$used = $app['request']->get('used', '');
			
			$consignment = $app['request']->get('consignment', '');
			
			$rental = $app['request']->get('rental', '');
			
			$employee_id = $app['request']->get('employee_id', '');
			
			if ($new == 1) $new = "Yes";
			
			else $new = "No";
			
			if ($used == 1) $used = "Yes";
			
			else $used = "No";
			
			if ($consignment == 1) $consignment = "Yes";
			
			else $consignment = "No";
			
			if ($rental == 1) $rental = "Yes";
			
			else $rental = "No";
			
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT MAX(inventory_number) as highest FROM pos_new_inventory WHERE accountnumber='" . $accno . "'";
			
			$result = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$new_inventory_id = (int) $result['highest'];
			
			$new_inventory_id++;
			
			$sql = "INSERT INTO pos_new_inventory (accountnumber, acctnumberhashed, item_number, inventory_number, brand, model, serial_number, color, cost, resale, profit_margin, action, finish, caliber_gauge, upc, description, quantity, min_quantity, type, new_item, used_item, consignment, rental, timestamp, employee_id) VALUES ('" . $accno . "', '" . $hash . "', '" . $item_number . "', '" . $new_inventory_id . "', '" . $brand . "', '" . $model . "', '" . $serial . "', '" . $color . "', '" . $cost . "', '" . $resale . "', '" . $profit . "', '" . $action . "', '" . $finish . "', '" . $calibre . "', '" . $upc . "','" . $description . "', '" . $quantity . "', '" . $minquantity . "','" . $inventory_type . "', '" . $new . "', '" . $used . "', '" . $consignment . "', '" . $rental . "', '" . date("m/d/Y \a\\t h:i:s A") . "', " . $employee_id . ")";
			
			$link->query($sql);
			
			//$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			return $sql;
		
		}
		
		function inventoryManagePost (Application $app) {
		
			//return "Called!";
			
			$id = $app['request']->get('id', '');
			
			$inventory_number = $app['request']->get('inventory_number', '');
					
			$item_number = $app['request']->get('item_number', '');
		
		    $minquantity = $app['request']->get('minquantity', '');
			
			$brand = $app['request']->get('brand', '');
			
			$model = $app['request']->get('model', '');
			
			$serial = $app['request']->get('serial', '');
			
			$color = $app['request']->get('color', '');
			
			$cost = $app['request']->get('cost', '');
			
			$resale = $app['request']->get('resale', '');
			
			$profit = $app['request']->get('profit', '');
			
			$action = $app['request']->get('action', '');
			
			$finish = $app['request']->get('finish', '');
			
			$calibre = $app['request']->get('calibre', '');
			
			$upc = $app['request']->get('upc', '');
			
			$description = $app['request']->get('description', '');
			
			$quantity = $app['request']->get('quantity', '');
			
			$inventory_type = $app['request']->get('inventory_type', '');
			
			$new = $app['request']->get('new', '');
			
			$used = $app['request']->get('used', '');
			
			$consignment = $app['request']->get('consignment', '');
			
			$rental = $app['request']->get('rental', '');
			
			$length = $app['request']->get('length', '');
			
			//$employee_id = $app['request']->get('employee_id', '');
			
			if ($new == 1) $new = "Yes";
			
			else $new = "No";
			
			if ($used == 1) $used = "Yes";
			
			else $used = "No";
			
			if ($consignment == 1) $consignment = "Yes";
			
			else $consignment = "No";
			
			if ($rental == 1) $rental = "Yes";
			
			else $rental = "No";
			
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "UPDATE pos_new_inventory SET barrels_length = '" . $length . "', item_number = '" . $item_number . "', brand = '" . $brand . "', model = '" . $model . "', serial_number='" . $serial . "', color='" . $color . "', cost='" . $cost . "', resale='" . $resale . "', profit_margin = '" . $profit . "', action = '" . $action . "', finish = '" . $finish . "', caliber_gauge = '" . $calibre . "', upc='" . $upc . "', description='" . $description . "', type = '" . $inventory_type . "', new_item = '" . $new . "', used_item='" . $used . "', consignment='" . $consignment . "', rental='" . $rental . "', min_quantity='" . $minquantity . "', quantity = '" . $quantity . "' WHERE id=" . $id;
			
			$link->query($sql);
			
			//$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			return $sql;
		
		}
		
		function inventoryDelete (Application $app, $inventory_id) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "DELETE FROM pos_new_inventory WHERE id='" . $inventory_id . "'";
		
			$link->query($sql);
			
			return "<script>window.location='/xapp/inventory'</script>";
		
		}
		function inventoryDeletes (Application $app, $inventory_id) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "DELETE FROM pos_new_inventory WHERE id='" . $inventory_id . "'";
		
			$link->query($sql);
			
			return "<script>window.location='/xapp/inventory/bulk'</script>";
		
		}
		function posConfig (Application $app) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM pos_new_inventory_types WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$inventoryTypes = "";
			
			foreach ($results as $row) {
			
				$inventoryTypes = $inventoryTypes . '<tr>
                <td  style="width:50%;"class="text-right">' . $row['inventory_type'] . '</td>
                <td><input class="checkboxes" value="' . $row['type_id'] . '" type="checkbox">
                  Delete </td>
              </tr>';
			
			}
			
			$sql = "SELECT * FROM pos_register WHERE accountnumber='" . $accno . "'";

			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			
			
			
			/* 
              <tr>
                <td class="text-right">TARGETS</td>
                <td><input type="checkbox">
                  Delete </td>
              </tr>
              
            */
			
            $data = [
            	'inventoryTypes' => $inventoryTypes,
            	'registerAmount' => $row['base_amount'],
				'comments' => $row['comments'],
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/pos/config.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/pointOfSale/0';
			$breadcrumbs[1]['title'] = 'Point of Sale';
			$breadcrumbs[2]['title'] = 'Configuration';
            $pageTitle = 'Range Controller - Point of Sale - Configuration';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function posManageSale (Application $app) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' GROUP BY transaction_number DESC";
			
			$results = DbHelper::dbFetchAllAssoc($link, $sql);

			$saleRows = "";

			foreach ($results as $row) {
			
				$saleRows = $saleRows . '<tr>
                    <td>' . $row['date_stamp'] . '</td>
                    <td>' . $row['member_name'] . '</td>
                    <td>$' . $row['total'] . '</td>
                    <td>' . $row['transaction_number'] . '</td>
                    <td><a href="/xapp/pointOfSaleReceipt/' . $row['transaction_number'] . '">View</a></td>
                    <td><a href="#none" onclick=\'deleteItem(' . $row['transaction_number'] . ')\'>Void</a></td>
                    <td><a href="" onclick=\'alert("Coming Soon")\'>Return</a></td>
                  </tr>';
			
			}
		
            $data = [
			//'temp' => $range_owner,
				'saleRows' => $saleRows,
				
            ];
            
            $content = $app['templating']->render('range/pos/manage_sale.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/pointOfSale/0';
			$breadcrumbs[1]['title'] = 'Point of Sale';
			$breadcrumbs[2]['title'] = 'Manage Sale';
            $pageTitle = 'Range Controller - Point of Sale - Manage Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function saleDelete (Application $app, $sale_id) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql1  = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' AND transaction_number ='".$sale_id."'";
			
			$arrtmp = DbHelper::dbFetchAllAssoc($link, $sql1);
			
			foreach ($arrtmp as $arr1) {
			$quant = $arr1['quantity'];
			$invid = $arr1['inventory_id'];
			//print_r($arr1);
			$sql2 = "UPDATE pos_new_inventory SET quantity=quantity + " . $quant . " WHERE id=" . $invid . "";
			$link->query($sql2);
			//echo $sql2;
			}
			$sql = "DELETE FROM pos_new_sale WHERE transaction_number='" . $sale_id . "' AND accountnumber='" . $accno . "'";
			//echo $sql;
			//die();
			$link->query($sql);
			
			
			return "<script>window.location='/xapp/inventory/manageSale'</script>";
		
		}
		
		function addInventoryType (Application $app) {
		
			$inventory_type = $app['request']->get('inventory_type', '');
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "INSERT INTO pos_new_inventory_types (acctnumberhashed, accountnumber, inventory_type) VALUES ('" . $hash . "', '" . $accno . "', '" . $inventory_type . "')";
		
			$link->query($sql);
			
			return "<script>window.location='/xapp/inventory/manageSale'</script>";
		
			//doesn't matter what this returns anymore
		
		}
		
		function deleteInventoryType (Application $app) {
		
			$type_id = $app['request']->get('type_id', '');
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "DELETE FROM pos_new_inventory_types WHERE type_id='" . $type_id . "' AND accountnumber='" . $accno . "'";
		
			$link->query($sql);
			
			return "<script>window.location='/xapp/inventory/manageSale'</script>";
		
			//doesn't matter what this returns
		
		}
		
		function updateRegisterBase (Application $app) {
		
			$amount = $app['request']->get('amount', '');
			
			$comments = $app['request']->get('comments', '');
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "UPDATE pos_register SET base_amount='" . $amount . "', comments = '".$comments."'  WHERE accountnumber='" . $accno ."'";
		
			$link->query($sql);
			
			return "<script>window.location='/xapp/inventory/manageSale'</script>";
		
			//doesn't matter what this returns anymore
		
		}
		
		function printbarMain (Application $app, $id, $type, $brand, $model, $description, $serial_number, $color, $resale, $barrels_length, $caliber_gauge) {
		
		/*echo '
		<html>
		<head>
		<style>
		@media print {  
			@page {
				width:3.2in;
				height:1.4in;
				border:1px solid black;
			}}
		</style>
		</head>
		<body>
		<table border = "0" cols ="2" cellspacing="0" cellpadding="0">
		<tr><td colspan = "2" style = "font-size:28px;"><b>$'.$resale.'</b></td></tr>
		<tr><td colspan = "2" style="font-size: 12px;">'.$type.'</td></tr>
		<tr><td colspan = "2" style="font-size: 12px;">'.$brand.'     '.$model.'</td></tr>
		<tr><td colspan = "2" style="font-size: 12px;">'.$description.'</td></tr>
		<tr><td style = "font-size:24px;"> I-'.$id.'</td><td><img src="/xapp/inventory/printbarImage/'.$id.'"></td></tr>
		</table>
		</body>
		</html>
		';*/
		if(!is_numeric($id)) {
			$numId = substr($id, 2);
		} else {
			$numId = $id;
		}
		
		echo '
		<html>
		<head>
		<style>
		@media print {  
			@page {
				width:3.2in;
				height:1.4in;
				margin: 0px;
				border:1px solid black;
			}}
		
		div {
			padding: 0px;
			margin: 0px;
			font-family: Times New Roman;
		}
		</style>
		</head>
		<body>
		<div style="width:3.2in;height:0.9in;">
		<div style = "font-size:18pt;"><b>$'.$resale.'</b></div>
		<div style="font-size: 8pt;">'.$type.'</div>
		<div style="font-size: 8pt;">'.$brand.'     '.$model.'</div>
		<div style="font-size: 8pt; white-space: nowrap;">'.str_replace("%2F", "/",$description).'</div>
		<div style = "font-size:14pt;"> '.$id.'<img src="/xapp/inventory/printbarImage/'.$numId.'" align="right"></div>
		</div>
		</body>
		</html>
		';
		
	

		die();
		
		}
		
		function printbarImage (Application $app, $id)
		{
		ini_set("display_errors", 1);
		error_reporting("1+2+4");
			  include('php-barcode.php');

			  // -------------------------------------------------- //
			  //                  PROPERTIES
			  // -------------------------------------------------- //
			  
			  // download a ttf font here for example : http://www.dafont.com/fr/nottke.font
			  //$font     = './NOTTB___.TTF';
			  // - -
			  
			  $fontSize = 10;   // GD1 in px ; GD2 in point
			  $marge    = 10;   // between barcode and hri in pixel
			  $x        = 100;  // barcode center
			  $y        = 12;  // barcode center
			  $height   = 25;   // barcode height in 1D ; module size in 2D
			  $width    = 2;    // barcode height in 1D ; not use in 2D
			  $angle    = 0;   // rotation in degrees : nb : non horizontable barcode might not be usable because of pixelisation
			  
			  //zero fill till 12 symbols total
			  $cpart = '';
			  for($i=0; $i< (12-strlen($id)); $i++) $cpart.='0';
			 			  
			  $code     = $cpart.$id; // barcode, of course ;)
			 
			  $type     = 'ean13';
			  
			  // -------------------------------------------------- //
			  //            ALLOCATE GD RESSOURCE
			  // -------------------------------------------------- //
			  $im     = imagecreatetruecolor(200, 25);
			  $black  = ImageColorAllocate($im,0x00,0x00,0x00);
			  $white  = ImageColorAllocate($im,0xff,0xff,0xff);
			  $red    = ImageColorAllocate($im,0xff,0x00,0x00);
			  $blue   = ImageColorAllocate($im,0x00,0x00,0xff);
			  imagefilledrectangle($im, 0, 0, 200, 25, $white);
			  
			  // -------------------------------------------------- //
			  //                      BARCODE
			  // -------------------------------------------------- //
			  
		
			  $data = \Barcode::gd($im, $black, $x, $y, $angle, $type, array('code'=>$code), $width, $height);
		
			  // -------------------------------------------------- //
			  //                        HRI
			  // -------------------------------------------------- //
			  if ( isset($font) ){
				$box = imagettfbbox($fontSize, 0, $font, $data['hri']);
				$len = $box[2] - $box[0];
				Barcode::rotate(-$len / 2, ($data['height'] / 2) + $fontSize + $marge, $angle, $xt, $yt);
				imagettftext($im, $fontSize, $angle, $x + $xt, $y + $yt, $blue, $font, $data['hri']);
			  }
			  header('Content-type: image/gif');
			  imagegif($im);
			  imagedestroy($im);
				return false;
		}
	}
	
}
