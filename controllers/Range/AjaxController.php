<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class AjaxController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';;
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
            //$pf = $app['range_url_prefix'];
            //return $app['request']->get('cmd', 'aa') .print_r($pf,1) ;
            
            return $this->indexPage($app);
        }
        
        function getMembersRewrite (Application $app, $search) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM customers WHERE (first_name LIKE '%".$search."%' OR last_name LIKE '%".$search."%' OR membership_number LIKE '%" . $search . "%' OR CONCAT(first_name, '', last_name) LIKE '%" . $search . "%' OR CONCAT(first_name, ' ', last_name) LIKE '%" . $search . "%') AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' and is_delete=1 ORDER BY membership_number ASC LIMIT 5";
            $assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
            
            $searchResults = "";
            
            foreach ($assoc_array as $row) {
            
            	$member = $row['membership_number']." ".$row['first_name']." ".$row['last_name']; 
				
				$searchResults = $searchResults . "<a class='search_results' id='search_results' onclick='setStudent(this.innerHTML);'>".ucwords($member)."</a><br/>";
                        
            }
            
            return $searchResults;		
		
		}
		
		function getPosItems (Application $app, $search) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM pos_new_inventory WHERE (inventory_number LIKE '%".$search."%' OR brand LIKE '%".$search."%' OR model LIKE '%" . $search . "%' OR description LIKE '%" . $search . "%' OR item_number LIKE '%" . $search . "%') AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' ORDER BY description ASC LIMIT 10";
        	
            $assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
            
            $searchResults = "";
            
            foreach ($assoc_array as $row) {
            
            	$item = $row['item_number']." ".$row['brand']." ".$row['model']." ".$row['description']; 
				
				$searchResults = $searchResults . "<a class='search_results' id='search_results' onclick=\"addItem('" . $row['item_number'] . "','" . $row['brand'] . "','" . $row['model'] . "','" . $row['description'] . "','" . $row['resale'] . "','" . $row['id'] . "');\">".ucwords($item)."</a><br/>";
                        //array push for setStudent
            }
            
            return $searchResults;		
		
		}
		
		function getCustomersForPos (Application $app, $search) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM customers WHERE (first_name LIKE '%".$search."%' OR last_name LIKE '%".$search."%' OR membership_number LIKE '%" . $search . "%' OR CONCAT(first_name, '', last_name) LIKE '%" . $search . "%' OR CONCAT(first_name, ' ', last_name) LIKE '%" . $search . "%') AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' and is_delete=1 ORDER BY membership_number ASC LIMIT 5";
        	
            $assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
            
            $searchResults = "";
            
            foreach ($assoc_array as $row) {
            
            	$member = $row['membership_number']." ".$row['first_name']." ".$row['last_name']; 
				
				$searchResults = $searchResults . "<a class='search_results' id='search_results' onclick='setCustomer(".$row['membership_number'].");'>".ucwords($member)."</a><br/>";
                        
            }
            
            return $searchResults;		
		
		}
		
		function getCustomerForPos (Application $app, $customer_id) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM customers WHERE membership_number='" . $customer_id . "' AND acctnumberhashed = '".$hash."' AND accountnumber = '".$accno."' and is_delete=1";
        	
            $row = DbHelper::dbFetchRowAssoc($link, $sql);
            
            $customerData = $row['first_name']." ".$row['last_name'] . '<br>
                    '.$row['street_address'].'<br>
                    '.$row['city'].', '.$row['state'].' '.$row['zipcode'].'';
            
            return $customerData;		
		
		}
		
	}
	
}



