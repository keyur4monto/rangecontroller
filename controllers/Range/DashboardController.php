<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class DashboardController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
			
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link2 = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$select_announcement_board = "SELECT * from announcement_board order by announcementboard_id DESC";
			$announcement_board = DbHelper::dbFetchAllAssoc($link2, $select_announcement_board);
					
           $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbsContent = $app['templating']->render('range/dashboard/breadcrumbs.php', array('breadcrumbs' => $breadcrumbs));
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Dashboard';
			$range_owner->getRemainingDays();
			$days_remaining=$range_owner->getRemainingDays();
			 $data = array(
				'accno'=>$accno,
				'days_remaining'=>$days_remaining,
				'announcement_board'=>$announcement_board,
            );
			$content = $app['templating']->render('range/dashboard/list.php',$data);
			return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
            //return $app['templating']->render('range/layout-range.php', $vars);
			//return false;
            //return $this->wrapInLayout($app,compact('content', 'breadcrumbs', 'pageTitle',$accno,$days_remaining));
        }
		private function getBreadcrumbs(Application $app){
           return array(
                array('link' => $app['get_range_url']('').'common/logout.php', 'title'=>'LOGOUT'),
                array('link' => $app['get_range_url']('configurations'), 'title'=>'Admin Landing Page')
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            
            $breadcrumbsContent = $app['templating']->render(
                    'range/dashboard/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],				
            );
            return $app['templating']->render('range/layout-range.php', $vars);
        }   
    }
}
