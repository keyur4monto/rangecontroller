<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class IdCardController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
			$idcard = $app['id_card'];
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();	
			$getidcarddetail = '';
			$getidcarddetail = $idcard->getidcarddetail();
			
			$data = array(                
			'getidcarddetail' => $getidcarddetail  
            );
			
			$content = $app['templating']->render('range/idcard/list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $pageTitle = 'Range Controller - Module Configurations   - Id Card Config';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'),
				array('link' => $app['get_range_url']('').'configurations', 'title'=>'Module Configurations'),
                array('title'=>'ID Card Config')
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            
            $breadcrumbsContent = $app['templating']->render(
                    'range/idcard/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            $vars = array(
                'pageTitle' => $data['pageTitle'],
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            );
            return $app['templating']->render('range/layout-range.php', $vars);
        }  
		public function ajaxSetPreferences(Application $app)
		{
			$field_name = addslashes($_POST['field_name']);
			$value = addslashes($_POST['value']);			
            
			$idcard = $app['id_card'];
			
			$updateidcarddetail = $idcard->updateidcarddetail($field_name, $value);
			
			$breadcrumbsContent = $app['templating']->render(
                    'range/idcard/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
					
			$vars = array(
                'pageTitle' => 'Range Controller - Module Configurations   - Id Card Config',
                'content' => $breadcrumbsContent . $app['templating']->render('range/idcard/list.php'),
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            );
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		
    }
}
