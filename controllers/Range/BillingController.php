<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    include('//includes/browser.php');
    class BillingcobfigController {
        protected $layoutPath;
        protected $pageSize;
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        /*public function landing(Application $app){
			$content = $app['templating']->render('range/billing/list.php');
            //$breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs=array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('billing'), 'title'=>'Reports']
            );
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Reports';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}*/
        public function index(Application $app){
            die('dsfdasf');
            //$pf = $app['range_url_prefix'];
            //return $app['request']->get('cmd', 'aa') .print_r($pf,1) ;
           /* $content = $app['templating']->render('range/reports/list.php');
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Reports';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));*/
            return $this->listMembers($app);
        }		
		 public function landing_billingcobfig(Application $app){
			
            die('sdsds');
            $content = $app['templating']->render('range/billing/list.php');
            $breadcrumbs = $this->getBreadcrumbs_config($app);
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Module Billing';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		/*public function updateImage(Application $app, $ext){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qry = "UPDATE controller_config SET company_logo='logo." . $ext . "' WHERE accountnumber='" . $accno . "'";
			$link->query($qry);
			}
		public function getEmployeeList (Application $app) {
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qry = "SELECT * FROM employees WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $qry);
			$selects = '<select onChange="addEmployee(this.value)" name="employee_id" id="employee_id" style="float:right;"><option value="">Choose an employee : </option>';
			foreach ($assoc_array as $employee_record) {
				$selects = $selects . '<option value="' . $employee_record['id'] . '">' . $employee_record['fname'] . ' ' . $employee_record['lname'] . '</option>';
			}
			$selects = $selects . '<option value="-1">Or Add An Employee...</option></select><span style="float:right;" class="requiredf">* </span>';
			return $selects;
		}
		public function setEmployeeId (Application $app, $section, $emp_id) {
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qry = "SELECT * FROM employees WHERE id=" . $emp_id;
			$results = DbHelper::dbFetchRowAssoc($link, $qry);
			//$emp_name = $results['fname'] . " " . $results['lname'];
			$emp_name = $results['initials']; 
			if ($section == 'billing') {
				$sqll = "SELECT * FROM billing_info ORDER BY id DESC LIMIT 1";
				$results = DbHelper::dbFetchRowAssoc($link, $sqll);
				$highest_id = $results['id'];
				$qry = "UPDATE billing_info SET employee_id=" . $emp_id . ", employee_name='" . $emp_name . "' WHERE id = " . $highest_id;
				$link->query($qry);
			}
		}
		public function configRC(Application $app){
            $range_owner = $app['range_owner'];  // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM controller_config WHERE accountnumber='" . $accno . "'";
			$configs = DbHelper::dbFetchRowAssoc($link, $msql);
			$nsql = "SELECT * FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			$hoo = DbHelper::dbFetchRowAssoc($link, $nsql);
			$count = 0;
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);	
            $data = [
				//'employeeRows' => $employeeRows,
				'errmsg' => $err,
				'email' => $configs['registered_email'],
				'companyname' => $configs['company_name'],
				'company_address' => $configs['company_address'],
				'company_city' => $configs['company_city'],
				'company_state' => $configs['company_state'],
				'company_zipcode' => $configs['company_zipcode'],
				'company_phone' => $configs['company_phone'],
				'accountno' => $accno,
				'password' => $configs['password'],
				'companylogo' => $configs['company_logo'],
				'taxrate' => $configs['tax_rate'],
				'numberoflanes' => $configs['number_of_lanes'],
				'hasmemberships' => $configs['has_memberships'],
				'startcount' => $configs['member_number_start_count'],
				'time_zone' => $configs['time_zone'],
				'sunday_from' => $hoo['sunday_from'],
				'sunday_to' => $hoo['sunday_to'],
				'monday_from' => $hoo['monday_from'],
				'monday_to' => $hoo['monday_to'],
				'tuesday_from' => $hoo['tuesday_from'],
				'tuesday_to' => $hoo['tuesday_to'],
				'wednesday_from' => $hoo['wednesday_from'],
				'wednesday_to' => $hoo['wednesday_to'],
				'thursday_from' => $hoo['thursday_from'],
				'thursday_to' => $hoo['thursday_to'],
				'friday_from' => $hoo['friday_from'],
				'friday_to' => $hoo['friday_to'],
				'saturday_from' => $hoo['saturday_from'],
				'saturday_to' => $hoo['saturday_to'],
            ];            
            $content = $app['templating']->render('range/config/configuration.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Range Controller Settings']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Range';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function updateRC(Application $app, $email, $password, $companyname, $company_address, $company_city, $company_state, $company_zipcode, $company_phone, $taxrate, $numberoflanes, $sunday_from, $sunday_to, $monday_from, $monday_to, $tuesday_from, $tuesday_to, $wednesday_from, $wednesday_to, $thursday_from, $thursday_to, $friday_from, $friday_to, $saturday_from, $saturday_to, $time_zone){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "UPDATE controller_config SET registered_email='" . $email . "', company_name='" . $companyname .  "', company_address='".$company_address."', company_city='".$company_city."', company_state= '".$company_state."', company_zipcode= '".$company_zipcode."', company_phone= '".$company_phone."', password='" . $password . "',  tax_rate='" . $taxrate . "', number_of_lanes='" . $numberoflanes . "', time_zone='" . $time_zone . "' WHERE accountnumber='" . $accno . "'";
			$link->query($msql);
			$nsql = "UPDATE hours_of_operation SET sunday_from='" . $sunday_from . "', sunday_to='" . $sunday_to . "', monday_from='" . $monday_from . "', monday_to='" . $monday_to . "', tuesday_from='" . $tuesday_from . "', tuesday_to='" . $tuesday_to . "', wednesday_from='" . $wednesday_from . "', wednesday_to='" . $wednesday_to . "', thursday_from='" . $thursday_from . "', thursday_to='" . $thursday_to ."', friday_from='" . $friday_from . "', friday_to='" . $friday_to . "', saturday_from='" . $saturday_from . "', saturday_to='" . $saturday_to . "', num_of_lanes='" . $numberoflanes . "' WHERE accountnumber='" . $accno . "'";
			$link->query($nsql);
			$pwsql = "UPDATE admin_login SET  current_pw='" . $password . "' WHERE accountnumber='" . $accno . "'";
			$link->query($pwsql);
			$count = 0;
			return $msql . " " . $nsql;
        }
        public function memberAddressList (Application $app) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
            //$roid =  $app['range_owner_id'];
            //$range_member = range_member::initForMember($link, 
              //      $roid->getAccountHashKey(), $roid->getAccountNo(), $membershipNo);
            //$pager = new \Rc\Services\Pager();
			$sql="SELECT memberships.membership_type,memberships.membership_number,concat(customers.first_name,customers.middle_name,customers.last_name) as cname,customers.street_address,customers.city,customers.state,customers.zipcode,(select count(*) from range_time_tracker where range_time_tracker.accountnumber=$accno and range_time_tracker.membership_number=memberships.membership_number) as visits, memberships.membership_date from memberships,customers where memberships.membership_number=customers.membership_number and customers.accountnumber=memberships.accountnumber and customers.accountnumber=$accno and memberships.membership_number <> '' ";
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			$output = "";
			foreach($results as $individual)
			{
				$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['cname'] ."</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
				$output = $output . $individual['visits'] . "</td><td>" . $individual['membership_date'] . "</td></tr>";
			}
		/*	$query_condition = " WHERE memberships.accountnumber='" . $accno . "'";
			$allMembers = range_member::getAllMembersByType($app, $query_condition);
			//return var_dump($allMembers);
			$output = "";
			foreach ($allMembers as $individual) {
			$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['first_name'] . " " . $individual['middle_name'] . " " . $individual['last_name'] . "</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
//			function numOfVisits($membership_number, $link) {
    $visits_sql = "";
    $visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $individual['membership_number'] . "'";
    if ($result = $link->query($visits_sql)) {
        $count_visits = 0;
        while ($row = $result->fetch_assoc()) {
            $count_visits++;
        }//END WHILE LOOP  
    }//END IF
    //return $count_visits;
	$output = $output . $count_visits . "</td><td>" . $individual['membership_date'] . "</td></tr>";
//}
			//$output = $output . " ASDD ";
			}
			$output = $output . "";*/
			$vars = [
                'rows' => $output,
            ];
			//return var_dump($vars); 
			//return $app['templating']->render('../xapp/reports/member_address.php', $vars);
			//return $output;
			$content = $app['templating']->render('range/reports/member_address.php', $vars);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Member Address List';
            $pageTitle = 'Range Controller - Member Address List';
			$ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $breadcrumbs]);
            $vars = [
                'pageTitle' => isset($pageTitle) 
                    ? $pageTitle : 'Range Controller',
                'content' => $breadcrumbsContent . $content,
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		public function memberAddressListByType (Application $app, $type) {
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
			$query_condition = " WHERE memberships.accountnumber='" . $accno . "' AND memberships.membership_type = '" . $type . "'";
			$allMembers = range_member::getAllMembersByType($app, $query_condition);
			$output = "";
			foreach ($allMembers as $individual) {
			$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['first_name'] . " " . $individual['middle_name'] . " " . $individual['last_name'] . "</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
//			function numOfVisits($membership_number, $link) {
				$visits_sql = "";
				$visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $individual['membership_number'] . "'";
				if ($result = $link->query($visits_sql)) {
					$count_visits = 0;
					while ($row = $result->fetch_assoc()) {
						$count_visits++;
					}//END WHILE LOOP  
				}//END IF
    //return $count_visits;
	$output = $output . $count_visits . "</td><td>" . $individual['membership_date'] . "</td></tr>";
			}
			$output = $output . "";
			$vars = [
                'rows' => $output,
            ];
			//return $app['templating']->render('range/reports/member_address.php', $vars);
			//return $output;
			$content = $app['templating']->render('range/reports/member_address.php', $vars);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Member Address List';
            $pageTitle = 'Range Controller - Reports';
			$ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $breadcrumbs]);
            $vars = [
                'pageTitle' => isset($pageTitle) 
                    ? $pageTitle : 'Range Controller',
                'content' => $breadcrumbsContent . $content,
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		public function memberAddressListByDate (Application $app, $month, $year) {
			$roid = $app['range_owner_id'];
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
			//$link = DbHelper::getWrappedHandle($app['dbs']['main']);
            //$roid =  $app['range_owner_id'];
            //$range_member = range_member::initForMember($link, 
              //      $roid->getAccountHashKey(), $roid->getAccountNo(), $membershipNo);
            //$pager = new \Rc\Services\Pager();
			$query_condition = " WHERE memberships.accountnumber='" . $accno . "'";
			$allMembers = range_member::getAllMembersByType($app, $query_condition);
			$output = "";
			foreach ($allMembers as $individual) {
			$dateArr = explode("/",$individual['membership_date']);
			if ((int) $month == (int) $dateArr[0] && (int) $year == (int) $dateArr[2]) {
				$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['first_name'] . " " . $individual['middle_name'] . " " . $individual['last_name'] . "</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
//			function numOfVisits($membership_number, $link) {
    $visits_sql = "";
    $visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $individual['membership_number'] . "'";
    if ($result = $link->query($visits_sql)) {
        $count_visits = 0;
        while ($row = $result->fetch_assoc()) {
            $count_visits++;
        }//END WHILE LOOP  
    }//END IF
    //return $count_visits;
	$output = $output . $count_visits . "</td><td>" . $individual['membership_date'] . "</td></tr>";
			}			
			//$output = $output . " ASDD ";
			} 
			$output = $output . "";
			$vars = [
                'rows' => $output,
            ];
			//return $app['templating']->render('range/reports/member_address.php', $vars);
			//return $output;
			$content = $app['templating']->render('range/reports/member_address.php', $vars);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Member Address List';
            $pageTitle = 'Range Controller - Reports';
			$ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $breadcrumbs]);
            $vars = [
                'pageTitle' => isset($pageTitle) 
                    ? $pageTitle : 'Range Controller',
                'content' => $breadcrumbsContent . $content,
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		public function listEmployees(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM employees WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);
			$count = 0;
			foreach ($assoc_array as $employeeRow) {
				$count++;
				$employeeRows = $employeeRows . '<tr><td>' . $count . '</td>
									<td>' . $employeeRow['fname'] . ' ' . $employeeRow['lname'] . '</td>
									<td>' . $employeeRow['initials'] . '</td>
									<td><a href="/xapp/config/employees/manage/' . $employeeRow['id'] . '">Manage</a></td>
									<td><a href="/xapp/config/employees/delete/' . $employeeRow['id'] . '" onclick="return myConfirm(\'Are you sure you want to delete this employee?\', ' . $employeeRow['id'] . ')">Delete</a></td>
								</tr>';	
			}
			/* 
									<td>1</td>
									<td>Jared Blair</td>
									<td>JB</td>
									<td><a href="#" onClick="window.location='manage_employee.html'">Manage</a></td>
									<td><a href="#">Delete</a></td> 
								</tr>
			*/
            $data = [
				'employeeRows' => $employeeRows,
                /*'lastManagedMember' => $this->getLastManagedMember($app),
                'totalMembers' => $records->getTotalRecords(),
                'members' =>  $records->getRecords(),
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'scheduleUrlPrefix' => $this->getScheduleUrlPrefix($app),
                'loginRangeUrlPrefix' => $this->getLoginRangeUrlPrefix($app),
                'logoutRangeUrlPrefix' => $this->getLogoutRangeUrlPrefix($app)*/
            ];            
            $content = $app['templating']->render('range/config/employees.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Employees']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Employees';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function listMemberships(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$membershipRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);
			/*  <tr>
                  <td>Employee</td>
                  <td>0.00</td>
                  <td>NA</td>
                  <td>No</td>
                  <td><a href="manage_membership.html">Manage</a></td>
                  <td><a href="#">Delete</a></td>
                </tr>
			*/
			foreach ($assoc_array as $membership_row) {
				$membershipRows = $membershipRows . ' <tr>
                  <td>' . $membership_row['membership_name'] . '</td>
                  <td>$' . $membership_row['membership_price'] . '</td>
                  <td>' . $membership_row['membership_length'] . '</td>
                  <td>' . $membership_row['payment_required'] . '</td>
                  <td><a href="/xapp/config/memberships/manage/' . $membership_row['membership_type_id'] . '">Manage</a></td>
				  <td><a href="/xapp/config/memberships/delete/' . $membership_row['membership_type_id'] . '" onclick="return myConfirm(\'Are you sure you want to delete this membership?\', ' . $membership_row['membership_type_id'] . ')">Delete</a></td>
                </tr>';		
			}
			$count = 0;
			//$membershipRows = "";
            $data = [
				'membershipRows' => $membershipRows,
                /*'lastManagedMember' => $this->getLastManagedMember($app),
                'totalMembers' => $records->getTotalRecords(),
                'members' =>  $records->getRecords(),
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'scheduleUrlPrefix' => $this->getScheduleUrlPrefix($app),
                'loginRangeUrlPrefix' => $this->getLoginRangeUrlPrefix($app),
                'logoutRangeUrlPrefix' => $this->getLogoutRangeUrlPrefix($app)*/
            ];            
            $content = $app['templating']->render('range/config/memberships.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Memberships']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Memberships';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function addEmployee(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
            $records= $range_owner->getMembers([], $pager);
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);			
            $data = [
				'err' => $err,
                /*'lastManagedMember' => $this->getLastManagedMember($app),
                'totalMembers' => $records->getTotalRecords(),
                'members' =>  $records->getRecords(),
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'scheduleUrlPrefix' => $this->getScheduleUrlPrefix($app),
                'loginRangeUrlPrefix' => $this->getLoginRangeUrlPrefix($app),
                'logoutRangeUrlPrefix' => $this->getLogoutRangeUrlPrefix($app)*/
            ];            
            $content = $app['templating']->render('range/config/add_employee.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['link' => '/xapp/config/employees', 'title'=>'Employees'], ['title' => 'Add Employee']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Employees';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function manageEmployee(Application $app, $employee_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM employees WHERE accountnumber='" . $accno . "' AND id='" . $employee_id . "'";
			$assoc_array = DbHelper::dbFetchRowAssoc($link, $msql);
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);
            $data = [
				'employee_id' => $employee_id,
				'fname' => $assoc_array['fname'],
				'lname' => $assoc_array['lname'],
				'initials' => $assoc_array['initials'],
				'err' => $err,
                /*'lastManagedMember' => $this->getLastManagedMember($app),
                'totalMembers' => $records->getTotalRecords(),
                'members' =>  $records->getRecords(),
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'scheduleUrlPrefix' => $this->getScheduleUrlPrefix($app),
                'loginRangeUrlPrefix' => $this->getLoginRangeUrlPrefix($app),
                'logoutRangeUrlPrefix' => $this->getLogoutRangeUrlPrefix($app)*/
            ];            
            $content = $app['templating']->render('range/config/manage_employee.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['link' => '/xapp/config/employees', 'title'=>'Employees'], ['title' => 'Manage Employee']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Employees';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function addMembership(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
            $records= $range_owner->getMembers([], $pager);
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);
            $data = [
				'err' => $err,
                /*'lastManagedMember' => $this->getLastManagedMember($app),
                'totalMembers' => $records->getTotalRecords(),
                'members' =>  $records->getRecords(),
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'scheduleUrlPrefix' => $this->getScheduleUrlPrefix($app),
                'loginRangeUrlPrefix' => $this->getLoginRangeUrlPrefix($app),
                'logoutRangeUrlPrefix' => $this->getLogoutRangeUrlPrefix($app)*/
            ];            
            $content = $app['templating']->render('range/config/add_membership.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['link' => '/xapp/config/memberships', 'title'=>'Memberships'], ['title' => 'Add Membership']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Memberships';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function manageMembership(Application $app, $membership_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "' AND membership_type_id='" . $membership_id . "'";
			$assoc_array = DbHelper::dbFetchRowAssoc($link, $msql);
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);
            $data = [
				'err' => $err,
				'membership_id' => $membership_id,
				'mname' => $assoc_array['membership_name'],
				'mprice' => $assoc_array['membership_price'],
				'mlength' => $assoc_array['membership_length'],
				'mpayment' => $assoc_array['payment_required'],
            ];            
            $content = $app['templating']->render('range/config/manage_membership.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['link' => '/xapp/config/memberships', 'title'=>'Memberships'], ['title' => 'Manage Membership']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Memberships';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function insertEmployee(Application $app, $fname, $lname, $initials, $employee_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			if ($employee_id == 0) $msql = "INSERT INTO employees (acctnumberhashed, accountnumber, fname, lname, initials) VALUES ('" . $hash . "', '" . $accno . "', '" . $fname . "', '" . $lname . "', '" . $initials . "')";
			else $msql = "UPDATE employees SET fname='" . $fname . "', lname='" . $lname . "', initials='" . $initials . "' WHERE id=" . $employee_id;
			$link->query($msql);
        }
		public function insertMembership(Application $app, $name, $price, $length, $payment_required, $membership_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			if ($membership_id == 0) $msql = "INSERT INTO editable_memberships (acctnumberhashed, accountnumber, membership_name, membership_price, membership_length, payment_required) VALUES ('" . $hash . "', '" . $accno . "', '" . $name . "', '" . $price . "', '" . $length . "', '" . $payment_required . "')";
			else $msql = "UPDATE editable_memberships SET membership_name='" . $name . "', membership_price='" . $price . "', membership_length='" . $length . "', payment_required='" . $payment_required . "' WHERE membership_type_id=" . $membership_id;
			$link->query($msql);
        }
		public function deleteEmployee(Application $app, $employee_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "DELETE FROM employees WHERE id=" . $employee_id;
			$link->query($msql);
			return '<script>window.location = "https://rangecontroller.com/xapp/config/employees"</script>';
        }
		public function deleteMembership(Application $app, $membership_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "DELETE FROM editable_memberships WHERE membership_type_id=" . $membership_id;
			$link->query($msql);
			return '<script>window.location = "https://rangecontroller.com/xapp/config/memberships"</script>';
        }
        public function listMembers(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
            $records= $range_owner->getMembers([], $pager);
			$membership_type= $range_owner->membership_type(); 
            $data = [
                'lastManagedMember' => $this->getLastManagedMember($app),
				'getfilterTypes' => $this->getfilterTypes($app),
                'totalMembers' => $records->getTotalRecords(),
                'members' =>  $records->getRecords(),
				'membership_type' => $membership_type,
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'scheduleUrlPrefix' => $this->getScheduleUrlPrefix($app),
                'loginRangeUrlPrefix' => $this->getLoginRangeUrlPrefix($app),
                'logoutRangeUrlPrefix' => $this->getLogoutRangeUrlPrefix($app)
            ];            
            $content = $app['templating']->render('range/customer/list.php', $data)
                    ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Member List';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function getfilterTypes(Application $app){
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT filtertypes FROM admin_login WHERE accountnumber='" . $accno . "'";
			$fetch_filtertypes = DbHelper::dbFetchRowAssoc($link, $sql);
			return $fetch_filtertypes;
        }
		public function getIndividualShooterReport (Application $app, $from, $to, $name, $shooter_id) {
		   $from = str_replace("%2F", "/", $from);
		   $to = str_replace("%2F", "/", $to);
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT *, STR_TO_DATE( date_stamp,  '%m/%d/%Y' ) AS parsedDate FROM range_time_tracker WHERE membership_number='" . $shooter_id . "' AND accountnumber='" . $accno . "' AND end_time <> 0 AND end_time <> 1";
			if ($name != 'unfiltered') $sql = $sql . " AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			$sql = $sql . " ORDER BY STR_TO_DATE(start_time, '%H:%i:%s')";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			//mem no, name, start time, end time, duration
			$shooter_rows = "";
			$count = 0;
			foreach ($assoc_array as $shooter_log) {
				$count++;
				$tname = $shooter_log['first_name'] . " " . $shooter_log['last_name'];
				$start_time = date("g:i a", strtotime($shooter_log['start_time']));//date("g:i a", strtotime("13:30:30"))
				$end_time = date("g:i a", strtotime($shooter_log['end_time']));
				//now get duration - php version 5.4.36
				//$time1 = new DateTime(date("H:i:s", $shooter_log['start_time']));
				//$time2 = new DateTime(date("H:i:s", $shooter_log['end_time']));
				//$interval = $time1->diff($time2);
				$st_t = explode(":", $shooter_log['start_time']);
				$en_t = explode(":", $shooter_log['end_time']);
				$hrs = (int) $en_t[0] - (int) $st_t[0];
				$mins = (int) $en_t[1] - (int) $st_t[1];
				if ($mins < 0){
					$mins += 60;
					$hrs--;
					}
				$duration = $hrs . ":" . sprintf("%02s", $mins) . "";//$interval->format('%s second(s)');
				$mem_no = $shooter_log['membership_number'];
				//now get membership type
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $shooter_id . "'";
				$this_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$mem_type = $this_row['membership_type'];
				$shooter_rows = $shooter_rows . '<tr>
                  <td>' . $count . '</td>
                  <td nowrap="0">' . $mem_type . '</td>
                  <td nowrap="0">' . $mem_no . '</td>
                  <td nowrap="0" style="width:100px;">' . $tname . '</td>
                  <td nowrap="0">' . $start_time . '</td>
                  <td nowrap="0">' . $end_time . '</td>
                  <td nowrap="0">' . $duration . '</td>
                  <td nowrap="0">' . $shooter_log['date_stamp'] . '</td>
                  <td nowrap="0" style="max-width:30px;">' . $shooter_log['employee_name'] . '</td>
                </tr>';
			}
				if ($name == 'unfiltered') $name = "UNFILTERED";
            $data = [
				'shooter_rows' => $shooter_rows,
				'shooter_id' => $shooter_id,
				'report_type' => $name,
				'start_date' => $from,
				'end_date' => $to,
            ];            
            $content = $app['templating']->render('range/reports/shooter_report.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Shooter Report';
			$breadcrumbs[2]['link'] = '/xapp/reports/shooter/0/0/UNFILTERED';
			$breadcrumbs[3]['title'] = $tname;;
            $pageTitle = 'Range Controller - Reports - Shooter Reports';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		public function getShooterReport (Application $app, $from, $to, $name) 
		{		
		   $from = str_replace("%2F", "/", $from);
		   $to = str_replace("%2F", "/", $to);
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT *, STR_TO_DATE( date_stamp,  '%m/%d/%Y' ) AS parsedDate FROM range_time_tracker WHERE accountnumber='" . $accno . "' AND end_time <> 0 AND end_time <> 1";
			if ($name != 'unfiltered') $sql = $sql . " AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			if ($name == 'UNFILTERED') $sql = $sql . " AND end_time='123' "; //get empty set
			$sql = $sql . " ORDER BY STR_TO_DATE(start_time, '%H:%i:%s')";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			//mem no, name, start time, end time, duration
			$shooter_rows = "";
			$count = 0;
			foreach ($assoc_array as $shooter_log) {
				$count++;
				$tname = $shooter_log['first_name'] . " " . $shooter_log['last_name'];
				$start_time = date("g:i a", strtotime($shooter_log['start_time']));//date("g:i a", strtotime("13:30:30"))
				$end_time = date("g:i a", strtotime($shooter_log['end_time']));
				//now get duration - php version 5.4.36
				//$time1 = new DateTime(date("H:i:s", $shooter_log['start_time']));
				//$time2 = new DateTime(date("H:i:s", $shooter_log['end_time']));
				//$interval = $time1->diff($time2);
				$st_t = explode(":", $shooter_log['start_time']);
				$en_t = explode(":", $shooter_log['end_time']);
				$hrs = (int) $en_t[0] - (int) $st_t[0];
				$mins = (int) $en_t[1] - (int) $st_t[1];
				if ($mins < 0){
					$mins += 60;
					$hrs--;
					}
				if ($hrs < 0) $hrs * -1;
				$duration = $hrs . ":" . sprintf("%02s", $mins) . "";//$interval->format('%s second(s)');
				$mem_no = $shooter_log['membership_number'];
				$lane_number = $shooter_log['lane_number'];
				//now get membership type
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $mem_no . "'";
				$this_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$mem_type = $this_row['membership_type'];
				/*$shooter_rows = $shooter_rows . '<tr>
                  <td>' . $count . '</td>
				  <td>' . $lane_number . '</td>
                  <td nowrap="0">' . $mem_type . '</td>
                  <td nowrap="0" style="width:30px;"><a href="/xapp/customers/' . $mem_no . '">' . $mem_no . '</a></td>
                  <td nowrap="0" style="width:150px;">' . $tname . '</td>
                  <td nowrap="0" style="width:80px;"><a href="/xapp/reports/shooter/0/0/unfiltered/' . $mem_no . '">' . $start_time . '</a></td>
                  <td nowrap="0" style="width:80px;"><a href="/xapp/reports/shooter/0/0/unfiltered/' . $mem_no . '">' . $end_time . '</a></td>
                  <td nowrap="0">' . $duration . '</td>
                  <td nowrap="0">' . $shooter_log['date_stamp'] . '</td>
                  <td nowrap="0" style="max-width:30px;">' . $shooter_log['employee_name'] . '</td>
                </tr>';*/
				$shooter_rows = $shooter_rows . '<tr>
				  <td>' . $lane_number . '</td>
                  <td nowrap="0">' . $mem_type . '</td>
                  <td nowrap="0" style="width:30px;"><a href="/xapp/customers/' . $mem_no . '">' . $mem_no . '</a></td>
                  <td nowrap="0" style="width:150px;">' . $tname . '</td>
                  <td nowrap="0" style="width:80px;"><a href="/xapp/reports/shooter/0/0/unfiltered/' . $mem_no . '">' . $start_time . '</a></td>
                  <td nowrap="0" style="width:80px;"><a href="/xapp/reports/shooter/0/0/unfiltered/' . $mem_no . '">' . $end_time . '</a></td>
                  <td nowrap="0">' . $duration . '</td>
                  <td nowrap="0">' . $shooter_log['date_stamp'] . '</td>
                  <td nowrap="0" style="max-width:30px;">' . $shooter_log['employee_name'] . '</td>
                </tr>';
			}
                $isempty = (empty($shooter_rows)) ? true : false;  
				if ($name == 'unfiltered') $name = "UNFILTERED";
				if ($name == 'UNFILTERED') { $shooter_rows = '<tr><td colspan="9" style="text-align:center;">Please select some filters to see your results here!</td></tr>'; $isempty=true;}
            $data = [
				'shooter_rows' => $shooter_rows,
				'shooter_id' => $shooter_id,
				'report_type' => $name,
				'start_date' => $from,
				'end_date' => $to,
				'isempty'=>$isempty
            ];            
            $content = $app['templating']->render('range/reports/shooter_report.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Shooter Report';
            $pageTitle = 'Range Controller - Reports - Shooter Reports';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		public function getSalesReport(Application $app, $from, $to, $name){
            //$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            //$pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
           // $records= $range_owner->getMembers([], $pager);
		   $from = str_replace("%2F", "/", $from);
		   $to = str_replace("%2F", "/", $to);
		   $payment_types_acc = array();
		   $payment_types_acc['Cash'] = 0;
		   $payment_types_acc['Visa'] = 0;
		   $payment_types_acc['Mastercard'] = 0;
		   $payment_types_acc['Check'] = 0;
		   $payment_types_acc['Discover'] = 0;
		   $payment_types_acc['Debit'] = 0;
		   $payment_types_acc['Paypal'] = 0;
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			if ($name != "unfiltered") $rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "'" . " AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )" . " ORDER BY transaction_number";
			else $rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' ORDER BY transaction_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
			$pos_transaction_rows = "";
			$pos_grand_total = 0;
			//get number of times id exists in table - if the id row counter is 0, assign the number to it
			//else if the id row counter is not zero, decrement by one
			//if number of times = row counter -> set rowspan to number of times on the two relevant columns
			//else if not equal and number of times > 1 -> omit relevant columns
			//else display columns as normal
			$lastTransactionNo = 0;
			$thisTransactionNo = 0;
			foreach ($assoc_array as $transaction) {
			$thisTransactionNo = $transaction['transaction_number'];
			$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $thisTransactionNo . " AND accountnumber='" . $accno . "'";
			$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
			$rowspan = $resultsArr['mycount'];
			if ($thisTransactionNo != $lastTransactionNo) $payment_types_acc[$transaction['payment1_method']] += $transaction['total']; 
			if ($thisTransactionNo != $lastTransactionNo) $pos_grand_total = $pos_grand_total + ((double) $transaction['total']);
			    $pos_transaction_rows = $pos_transaction_rows . '<tr class="pos_class">';
                  if ($thisTransactionNo != $lastTransactionNo) $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">' . $transaction['transaction_number'] . '</td>';
                  $pos_transaction_rows = $pos_transaction_rows . '<td>' . $transaction['quantity'] . '</td>
                  <td>' . $transaction['description'] . '</td>
                  <td>' . $transaction['item_number'] . '</td>
                  <td>$' . $transaction['price'] . '</td>';
                  if ($thisTransactionNo != $lastTransactionNo) $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">$' . $transaction['total'] . '</td>';
				  if ($thisTransactionNo != $lastTransactionNo) {
				  $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">';
				  if ((float) $transaction['discount'] <= 0) $pos_transaction_rows = $pos_transaction_rows . '<span style="color: red; font-weight: bold;">X</span>';
				  else $pos_transaction_rows = $pos_transaction_rows . '<span style="color: green; font-weight: bold;">&#10003;</span>';
				  //<span style="color: green; font-weight: bold;">&#10003;</span> or <span style="color: red; font-weight: bold;">X</span>
				  $pos_transaction_rows = $pos_transaction_rows . '</td>';
				  }
                $pos_transaction_rows = $pos_transaction_rows . '</tr>';
				$lastTransactionNo = $thisTransactionNo;
			}
			/*<tr>
                  <td>1617</td>
                  <td>2</td>
                  <td nowrap="">FEDERAL 22 LR 40 GR SOLID AUTOMATCH</td>
                  <td>I-42895</td>
                  <td>$5.99</td>
                  <td>$11.83</td>
                  <td><span style="color: green; font-weight: bold;">&#10003;</span> or <span style="color: red; font-weight: bold;">X</span></td>
                </tr>*/
			$mem_types_query = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $mem_types_query);
			$membership_types = array();
			$membership_types_sub = array();
			$membership_type_names = array('', '', '', '', '', '', '', '', '', '');
			$arr_index = 0;
			foreach ($assoc_array as $mem_type) {
				$keyname = $mem_type['membership_name'];
				$membership_type_names[$arr_index] = $keyname;
				$membership_types[$keyname] = 0;//will hold number of memberships sold  for this type		
				$membership_types_sub[$keyname] = 0;
				$arr_index++;
			}
			if ($name != "unfiltered") $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'" . " AND STR_TO_DATE( date,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( date,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			else $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
			foreach ($assoc_array as $billing) {
				if ($billing['payment_for'] != "Class" && $billing['payment_for'] != "Visit") $this_mem = $billing['membership_type'];
				else if ($billing['payment_for'] == "Class") $this_mem = 'Student';
				else if ($billing['payment_for'] == "Visit") $this_mem = 'Visitor';
				$payment_types_acc[$billing['payment_method']] += $billing['payment_amount'];
				foreach ($membership_type_names as $memtype) {
					if ($this_mem == $memtype) {
						$membership_types[$memtype]++;//add to number of this type sold
						$membership_types_sub[$memtype] = $membership_types_sub[$memtype] + $billing['payment_amount'];
					}
				}
			}
			$memberships_transaction_rows = "";
			$memberships_grand_total = 0;
			$memberships_total_count;
			foreach ($membership_type_names as $memtype) {
				$memberships_grand_total = $memberships_grand_total + $membership_types_sub[$memtype];
				$memberships_total_count = $memberships_total_count + $membership_types[$memtype];
				$memberships_transaction_rows = $memberships_transaction_rows . '<tr class="memberships_class">
                  <td>' . $memtype . '</td>
                  <td>' . $membership_types[$memtype] . '</td>
                  <td>$' . number_format($membership_types_sub[$memtype], 2) . '</td>
                </tr>';
			}
			/*
                <tr>
                  <td>Platinum</td>
                  <td>8</td>
                  <td>$2,218.75</td>
                </tr>
			*/
            $data = [
                'pos_transaction_rows' => $pos_transaction_rows,
				'memberships_transaction_rows' => $memberships_transaction_rows,
				'pos_grand_total' => number_format($pos_grand_total, 2),
				'memberships_grand_total' => number_format($memberships_grand_total, 2),
				'memberships_total_count' => $memberships_total_count,
				'cash' => $payment_types_acc['Cash'],
				'check' => $payment_types_acc['Check'],
				'debit' => $payment_types_acc['Debit'],
				'mastercard' => $payment_types_acc['Mastercard'],
				'visa' => $payment_types_acc['Visa'],
				'discover' => $payment_types_acc['Discover'],
				'paypal' => $payment_types_acc['Paypal'],
				'drawer_end' => number_format(($pos_grand_total + $memberships_grand_total), 2),
				'report_type' => $name,
				'start_date' => $from,
				'end_date' => $to,
            ];            			
            $content = $app['templating']->render('range/reports/reports.php', $data)
                    ;
            //$breadcrumbs = $this->getBreadcrumbs($app);
            //$breadcrumbs[1]['link'] = '';
			$breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => '/xapp/reports', 'title' => 'Reports'], ['title' => 'Sales Report']);
            //$breadcrumbs[1]['link'] = '/xapp/configurations.php';
            $pageTitle = 'Range Controller - Reports - Sales Report';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function memberBenefitsConfig(Application $app, $memtypeid){
			if (!isset($memtypeid)) $memtypeid = 0;
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$query_sql = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "' AND acctnumberhashed='" . $hash . "'";
			//return $insert_sql;
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			if ($memtypeid == 0) $membershipOptions = "<option selected value=\"0\">Select a membership type : </option>";
			else $membershipOptions = "<option value=\"0\">Select a membership type : </option>";
			foreach ($assoc_array as $optionsArr) {
				if ($optionsArr['membership_type_id'] == $memtypeid) {
				$membershipOptions = $membershipOptions . "<option selected value='" . $optionsArr['membership_type_id'] . "'>" . $optionsArr['membership_name'] . "</option>";
				$memTypeName = $optionsArr['membership_name'];
				}
				else $membershipOptions = $membershipOptions . "<option value='" . $optionsArr['membership_type_id'] . "'>" . $optionsArr['membership_name'] . "</option>";
			}
			//
			//
			$qsql = "SELECT * FROM benefits WHERE membership_type_id=" . $memtypeid;
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $qsql);
			$benefitsFields = "";
			$count = 0;
			foreach ($assoc_array as $fieldsArr) {
			$count++;//user for css id and for form submit
				$benefitsFields = $benefitsFields . '<tr style="display:none;" id="fieldno-'. $count . '">
                <td><input type="text" id="quantity-' . $count . '" style="width:50px;" value="' . $fieldsArr['quantity'] . '"></td>
                  <td><select id="timeframe-' . $count . '"> <option value=""> </option>';
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Expiration") ? '<option selected value="Expiration">Expiration</option>' : '<option value="Expiration">Expiration</option>');
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Yearly") ? '<option selected value="Yearly">Yearly</option>' : '<option value="Yearly">Yearly</option>');
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Monthly") ? '<option selected value="Monthly">Monthly</option>' : '<option value="Monthly">Monthly</option>');
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Weekly") ? '<option selected value="Weekly">Weekly</option>' : '<option value="Weekly">Weekly</option>');
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Daily") ? '<option selected value="Daily">Daily</option>' : '<option value="Daily">Daily</option>');
                    $benefitsFields = $benefitsFields . '</select></td>
                  <td><input id="benefit-name-' . $count . '" type="text" style="width:450px;" value="' . $fieldsArr['name'] .  '"></td>
				  <input type="hidden" id="benefit-id-' . $count . '" name="benefit_id" value="' . $fieldsArr['id'] . '"/>
                </tr>';
			}
			while ($count < 9) {
				$count++;
				$benefitsFields = $benefitsFields . '<tr style="display:none;" id="fieldno-' . $count . '">
                <td><input type="text" id="quantity-' . $count . '" style="width:50px;" value="' . '' . '"></td>
                  <td><select id="timeframe-' . $count . '">
					  <option selected value=""> </option>
                      <option value="Expiration">Expiration</option>
                      <option value="Yearly">Yearly</option>
                      <option value="Monthly">Monthly</option>
                      <option value="Weekly">Weekly</option>
                      <option value="Daily">Daily</option>
                    </select></td>
                  <td><input id="benefit-name-' . $count . '" type="text" style="width:450px;" value="' . '' .  '"></td>
				  <input type="hidden" id="benefit-id-' . $count . '" name="benefit_id" value="' . '0' . '"/>
                </tr>';
			}
			//
			//
			if ($memtypeid == 0) $benefitsFields = "<tr><td colspan=3>Please select a membership type to create benefits here!</td></tr>";
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields in each row you use are completed!"]);
            $data = [
                'membershipOptions' => $membershipOptions,
				'benefitsFields' => $benefitsFields,
				'memtypename' => $memTypeName,
				'memtypeid' => $memtypeid,
				'err' => $err,
            ];            
            $content = $app['templating']->render('range/config/benefits_config.php', $data);                   ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Member Benefits']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Member Benefits Configuration';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		function getGraphReport (Application $app) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$type = $app['request']->get('type', '');
			$append = "";
			if ($from != "" && $to != "") {
				$append = " AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' )";
				$type = "CUSTOM";
			}
			$decrement = 0;
			if ($type != "" && $type != "CUSTOM") {
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
				$append = " AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY )";
			}
			else if ($type != "CUSTOM") $type = "UNFILTERED";
			$visitorTimes = array('8AM'=>0,'9AM'=>0,'10AM'=>0,'11AM'=>0,'12PM'=>0,'1PM'=>0,'2PM'=>0,'3PM'=>0,'4PM'=>0,'5PM'=>0,'6PM'=>0,'7PM'=>0);
			$studentTimes = array('8AM'=>0,'9AM'=>0,'10AM'=>0,'11AM'=>0,'12PM'=>0,'1PM'=>0,'2PM'=>0,'3PM'=>0,'4PM'=>0,'5PM'=>0,'6PM'=>0,'7PM'=>0);
			$memberTimes = array('8AM'=>0,'9AM'=>0,'10AM'=>0,'11AM'=>0,'12PM'=>0,'1PM'=>0,'2PM'=>0,'3PM'=>0,'4PM'=>0,'5PM'=>0,'6PM'=>0,'7PM'=>0);
			$visitorMonths = array('January'=>0,'February'=>0,'March'=>0,'April'=>0,'May'=>0,'June'=>0,'July'=>0,'August'=>0,'September'=>0,'October'=>0,'November'=>0,'December'=>0);
			$studentMonths = array('January'=>0,'February'=>0,'March'=>0,'April'=>0,'May'=>0,'June'=>0,'July'=>0,'August'=>0,'September'=>0,'October'=>0,'November'=>0,'December'=>0);
			$memberMonths = array('January'=>0,'February'=>0,'March'=>0,'April'=>0,'May'=>0,'June'=>0,'July'=>0,'August'=>0,'September'=>0,'October'=>0,'November'=>0,'December'=>0);
			$visitorYears = array('y2013'=>0,'y2014'=>0,'y2015'=>0);
			$studentYears = array('y2013'=>0,'y2014'=>0,'y2015'=>0);
			$memberYears = array('y2013'=>0,'y2014'=>0,'y2015'=>0);
			//1 - sunday, 2 - monday
			$visitorWeekdays = array('Monday'=>0,'Tuesday'=>0,'Wednesday'=>0,'Thursday'=>0,'Friday'=>0,'Saturday'=>0);
			$studentWeekdays = array('Monday'=>0,'Tuesday'=>0,'Wednesday'=>0,'Thursday'=>0,'Friday'=>0,'Saturday'=>0);
			$memberWeekdays = array('Monday'=>0,'Tuesday'=>0,'Wednesday'=>0,'Thursday'=>0,'Friday'=>0,'Saturday'=>0);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "SELECT range_time_tracker.accountnumber, range_time_tracker.membership_number, memberships.membership_type, range_time_tracker.start_time, range_time_tracker.date_stamp, DAYOFWEEK(STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' )) AS weekday FROM range_time_tracker INNER JOIN memberships ON range_time_tracker.membership_number = memberships.membership_number WHERE range_time_tracker.accountnumber='" . $accno . "'" . $append;
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			foreach ($assoc_array as $row) {
				//if ($from == '') $from = $row['date_from'];
				//if ($to == '') $to = $row['date_to'];
				$index = "12PM";
				$monthIndex = "January";
				$yearIndex = "y2013";
				$timestamp = $row['start_time'];
				$tArr = explode(":", $timestamp);
				$datestamp = $row['date_stamp'];
				$mArr = explode("/", $datestamp);
				$weekday = (int) $row['weekday'];
				switch ($weekday) {
					case 2 : $weekdayIndex = "Monday";
					break;
					case 3 : $weekdayIndex = "Tuesday";
					break;
					case 4 : $weekdayIndex = "Wednesday";
					break;
					case 5 : $weekdayIndex = "Thursday";
					break;
					case 6 : $weekdayIndex = "Friday";
					break;
					case 7 : $weekdayIndex = "Saturday";
					break;
				}
				switch ($tArr[0]) {
					case 8 : $index = "8AM";
					break;
					case 9 : $index = "9AM";
					break;
					case 10 : $index = "10AM";
					break;
					case 11 : $index = "11AM";
					break;
					case 12 : $index = "12PM";
					break;
					case 13 : $index = "1PM";
					break;
					case 14 : $index = "2PM";
					break;
					case 15 : $index = "3PM";
					break;
					case 16 : $index = "4PM";
					break;
					case 17 : $index = "5PM";
					break;
					case 18 : $index = "6PM";
					break;
					case 19 : $index = "7PM";
					break;
				}
				switch ($mArr[0]) {
					case 1 : $monthIndex = "January";
					break;
					case 2 : $monthIndex = "February";
					break;
					case 3 : $monthIndex = "March";
					break;
					case 4 : $monthIndex = "April";
					break;
					case 5 : $monthIndex = "May";
					break;
					case 6 : $monthIndex = "June";
					break;
					case 7 : $monthIndex = "July";
					break;
					case 8 : $monthIndex = "August";
					break;
					case 9 : $monthIndex = "September";
					break;
					case 10 : $monthIndex = "October";
					break;
					case 11 : $monthIndex = "November";
					break;
					case 12 : $monthIndex = "December";
					break;
				}
				switch ($mArr[2]) {
					case 13 : $yearIndex = "y2013";
					break;
					case 14 : $yearIndex = "y2014";
					break;
					case 15 : $yearIndex = "y2015";
					break;
				}
				if ($row['membership_type'] == "Student") {
					$studentTimes[$index]++;
					$studentMonths[$monthIndex]++;
					$studentYears[$yearIndex]++;
					$studentWeekdays[$weekdayIndex]++;
				}
				else if ($row['membership_type'] == "Visitor") {
					$visitorTimes[$index]++;
					$visitorMonths[$monthIndex]++;
					$visitorYears[$yearIndex]++;
					$visitorWeekdays[$weekdayIndex]++;
				}
				else {
					$memberTimes[$index]++;
					$memberMonths[$monthIndex]++;
					$memberYears[$yearIndex]++;
					$memberWeekdays[$weekdayIndex]++;
				}
			}
			//data: [65, 59, 80, 10, 54, 32, 9, 13, 43]
			$studentData = 'data: [';
			foreach ($studentTimes as $key=>$value) {
				if ($key != "7PM") $studentData = $studentData . $value . ",";
				else $studentData = $studentData . $value . "]";
			}
			$visitorData = 'data: [';
			foreach ($visitorTimes as $key=>$value) {
				if ($key != "7PM") $visitorData = $visitorData . $value . ",";
				else $visitorData = $visitorData . $value . "]";
			}
			$memberData = 'data: [';
			foreach ($memberTimes as $key=>$value) {
				if ($key != "7PM") $memberData = $memberData . $value . ",";
				else $memberData = $memberData . $value . "]";
			}
			$studentMData = 'data: [';
			foreach ($studentMonths as $key=>$value) {
				if ($key != "December") $studentMData = $studentMData . $value . ",";
				else $studentMData = $studentMData . $value . "]";
			}
			$visitorMData = 'data: [';
			foreach ($visitorMonths as $key=>$value) {
				if ($key != "December") $visitorMData = $visitorMData . $value . ",";
				else $visitorMData = $visitorMData . $value . "]";
			}
			$memberMData = 'data: [';
			foreach ($memberMonths as $key=>$value) {
				if ($key != "December") $memberMData = $memberMData . $value . ",";
				else $memberMData = $memberMData . $value . "]";
			}
			$studentYData = 'data: [';
			foreach ($studentYears as $key=>$value) {
				if ($key != "y2015") $studentYData = $studentYData . $value . ",";
				else $studentYData = $studentYData . $value . "]";
			}
			$visitorYData = 'data: [';
			foreach ($visitorYears as $key=>$value) {
				if ($key != "y2015") $visitorYData = $visitorYData . $value . ",";
				else $visitorYData = $visitorYData . $value . "]";
			}
			$memberYData = 'data: [';
			foreach ($memberYears as $key=>$value) {
				if ($key != "y2015") $memberYData = $memberYData . $value . ",";
				else $memberYData = $memberYData . $value . "]";
			}
			$studentWData = 'data: [';
			foreach ($studentWeekdays as $key=>$value) {
				if ($key != "Saturday") $studentWData = $studentWData . $value . ",";
				else $studentWData = $studentWData . $value . "]";
			}
			$visitorWData = 'data: [';
			foreach ($visitorWeekdays as $key=>$value) {
				if ($key != "Saturday") $visitorWData = $visitorWData . $value . ",";
				else $visitorWData = $visitorWData . $value . "]";
			}
			$memberWData = 'data: [';
			foreach ($memberWeekdays as $key=>$value) {
				if ($key != "Saturday") $memberWData = $memberWData . $value . ",";
				else $memberWData = $memberWData . $value . "]";
			}
			$to = " to <br>" . $to;
			if ($type == "UNFILTERED") $from = $to = "";
            $data = [
                'studentData' => $studentData,
                'visitorData' => $visitorData,
                'memberData' => $memberData,
                'studentMonthData' => $studentMData,
                'visitorMonthData' => $visitorMData,
                'memberMonthData' => $memberMData,
                'studentYearData' => $studentYData,
                'visitorYearData' => $visitorYData,
                'memberYearData' => $memberYData,
                'studentWeekdayData' => $studentWData,
                'visitorWeekdayData' => $visitorWData,
                'memberWeekdayData' => $memberWData,
                'type' => $type,
                'from' => $from,
                'to' => $to,
            ];
            $content = $app['templating']->render('range/reports/range_usage_graph.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Range Usage Report (Graph)';
            $pageTitle = 'Range Controller - Reports - Range Usage Graph';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getPosHistoryReport (Application $app, $membershipNumber) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$type = $app['request']->get('type', '');
			$append = " AND pos_new_sale.membership_number='" . $membershipNumber . "'";
			if ($from != "" && $to != "") {
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' ) AND pos_new_sale.membership_number='" . $membershipNumber . "'";
				$type = "CUSTOM";
			}
			$decrement = 0;
			if ($type != "" && $type != "CUSTOM") {
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY ) AND pos_new_sale.membership_number='" . $membershipNumber . "'";
			}
			else if ($type != "CUSTOM") $type = "UNFILTERED";
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "SELECT pos_new_sale.accountnumber, pos_new_sale.transaction_number, pos_new_sale.membership_number, pos_new_sale.date_stamp, pos_new_sale.total, customers.first_name, customers.last_name, memberships.membership_type FROM pos_new_sale INNER JOIN customers on pos_new_sale.membership_number=customers.membership_number INNER JOIN memberships ON customers.membership_number = memberships.membership_number WHERE pos_new_sale.accountnumber='" . $accno . "' " . $append . " GROUP BY pos_new_sale.transaction_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = "";
			$total = 0.00;
			$memberName = "";
			$membershipType = "";
			foreach ($assoc_array as $row) {
				$rows = $rows . '<tr class="text-center"">
					<td>' . $row['date_stamp'] . '</td>
					<td style="text-align: left;">' . $row['total'] . '</td>
					<td>' . $row['transaction_number'] . '</td>
					<td><a href="/xapp/reports/pointOfSale/details/' . $row['transaction_number'] . '" >Details</a></td>
				</tr>';
				$total = $total + (double) $row['total'];
				$memberName = $row['first_name'] . " " . $row['last_name'];
				$membershipType = $row['membership_type'];
			}
			$to = " to <br>" . $to;
			if ($type == "UNFILTERED") $from = $to = "";
            $data = [
                'memberName' => $memberName,
                'membershipNumber' => $membershipNumber,
                'membershipType' => $membershipType,
                'total' => $total,
                'type' => $type,
                'from' => $from,
                'to' => $to,
                'rows' => $rows,
            ];
            $content = $app['templating']->render('range/reports/pos_sales_history.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/reports/pos/sales?default=DAILY';
			$breadcrumbs[3]['title'] = $memberName;
            $pageTitle = 'Range Controller - Reports - Point Of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getPosSaleDetails (Application $app, $transactionNo) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $transactionNo;
			$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
			$rowspan = $resultsArr['mycount'];
			$sql = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' AND transaction_number='" . $transactionNo . "'";
			$resultsArr = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = "";
			$counter = 0;
			$membershipNumber = "";
			foreach ($resultsArr as $transaction) {
				$membershipNumber = $transaction['membership_number'];
				$transactionNumber = $transaction['transaction_number'];
				$counter++;
				$rows = $rows . '<tr class="text-center">
					<td>' . $transaction['quantity'] . '</td>
					<td>' . $transaction['description'] . '</td>
					<td>' . $transaction['item_number'] . '</td>
					<td>$' . $transaction['price'] . '</td>';
					if ($counter == 1) $rows = $rows . '<td rowspan="' . $rowspan . '">$' . $transaction['total'] . '</td>';
					$rows = $rows . '<td>';
					if ($transaction['price'] <= $transaction['total']) $rows = $rows . '<span style="color: red; font-weight: bold;">X</span>';
					else $rows = $rows . '<span style="color: green; font-weight: bold;">✓</span></td>';
				$rows = $rows . '</tr>';
			}
			$sql = "SELECT first_name, last_name FROM customers WHERE membership_number='" . $membershipNumber . "'";
			$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
			$memberName = $resultsArr['first_name'] . " " . $resultsArr['last_name'];
            $data = [
                'rows' => $rows,
                'transactionNumber' => $transactionNumber,
                'memberName' => $memberName,
                'membershipNumber' => $membershipNumber,
            ];
            $content = $app['templating']->render('range/reports/pos_sale_details.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/reports/pos/sales?default=DAILY';
			$breadcrumbs[3]['title'] = 'Sale # ' . $transactionNumber . '';
            $pageTitle = 'Range Controller - Reports - Point Of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getPos (Application $app) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$default = $app['request']->get('default', '');
			$type = $app['request']->get('type', $default);
			$append = "";
			if ($from != "" && $to != "") {
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' )";
				$type = "CUSTOM";
			}
			$decrement = 0;
			if ($type != "" && $type != "CUSTOM") {
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY )";
			}
			else if ($type != "CUSTOM") $type = "UNFILTERED";
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "SELECT pos_new_sale.accountnumber, pos_new_sale.transaction_number, pos_new_sale.membership_number, pos_new_sale.date_stamp, pos_new_sale.total, customers.first_name, customers.last_name, memberships.membership_type FROM pos_new_sale INNER JOIN customers on pos_new_sale.membership_number=customers.membership_number INNER JOIN memberships ON customers.membership_number = memberships.membership_number WHERE pos_new_sale.accountnumber='" . $accno . "' " . $append . " GROUP BY pos_new_sale.transaction_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = "";
			$total = 0.00;
			foreach ($assoc_array as $row) {
				$rows = $rows . '<tr class="text-center"">
					<td>' . $row['date_stamp'] . '</td>
					<td><a href="/xapp/customers/' . $row['membership_number'] . '">' . $row['first_name'] . " " . $row['last_name'] . '</a></td>
					<td>' . $row['membership_type'] . '</td>
					<td style="text-align: left;">$' . $row['total'] . '</td>
					<td>' . $row['transaction_number'] . '</td>
					<td><a href="/xapp/reports/pointOfSale/details/' . $row['transaction_number'] . '">Details</a></td>
					<td><a href="/xapp/reports/pointOfSale/history/' . $row['membership_number'] . '">Sales History</a></td>
				</tr>';
				$total = $total + (double) $row['total'];
			}
			$to = " to <br>" . $to;
			if ($type == "UNFILTERED") $from = $to = "";
            $data = [
                'rows' => $rows,
                'type' => $type,
                'from' => $from,
                'to' => $to,
                'total' => $total,
            ];
            $content = $app['templating']->render('range/reports/point.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '';
            $pageTitle = 'Range Controller - Reports - Point Of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		
		function getReorders (Application $app) 
		{
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT * from pos_new_inventory WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
            $data = [
                'assoc_array' => $assoc_array,
            ];
            $content = $app['templating']->render('range/reports/reorder.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/reports/pos';
			$breadcrumbs[3]['title'] = 'Inventory Reorders';
            $pageTitle = 'Range Controller - Reports - Point Of Sale - Inventory Reorders';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getPosReport (Application $app) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$default = $app['request']->get('default', '');
			$type = $app['request']->get('type', $default);
			$append = "";
			if ($from != "" && $to != "") {
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' )";
				$type = "CUSTOM";
			}
			$decrement = 0;
			if ($type != "" && $type != "CUSTOM") {
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY )";
			}
			else if ($type != "CUSTOM") $type = "UNFILTERED";
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "SELECT pos_new_sale.accountnumber, pos_new_sale.transaction_number, pos_new_sale.membership_number, pos_new_sale.date_stamp, pos_new_sale.total, customers.first_name, customers.last_name, memberships.membership_type FROM pos_new_sale INNER JOIN customers on pos_new_sale.membership_number=customers.membership_number INNER JOIN memberships ON customers.membership_number = memberships.membership_number WHERE pos_new_sale.accountnumber='" . $accno . "' " . $append . " GROUP BY pos_new_sale.transaction_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = "";
			$total = 0.00;
			foreach ($assoc_array as $row) {
				$rows = $rows . '<tr class="text-center"">
					<td>' . $row['date_stamp'] . '</td>
					<td><a href="/xapp/customers/' . $row['membership_number'] . '">' . $row['first_name'] . " " . $row['last_name'] . '</a></td>
					<td>' . $row['membership_type'] . '</td>
					<td style="text-align: left;">$' . $row['total'] . '</td>
					<td>' . $row['transaction_number'] . '</td>
					<td><a href="/xapp/reports/pointOfSale/details/' . $row['transaction_number'] . '">Details</a></td>
					<td><a href="/xapp/reports/pointOfSale/history/' . $row['membership_number'] . '">Sales History</a></td>
				</tr>';
				$total = $total + (double) $row['total'];
			}
			$to = " to <br>" . $to;
			if ($type == "UNFILTERED") $from = $to = "";
            $data = [
                'rows' => $rows,
                'type' => $type,
                'from' => $from,
                'to' => $to,
                'total' => $total,
            ];
            $content = $app['templating']->render('range/reports/pos.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/reports/pos';
			$breadcrumbs[3]['title'] = 'Sales';
            $pageTitle = 'Range Controller - Reports - Point Of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function sendEmail(Application $app){//$email
			//return "NOTHING?";
			$email = $app['request']->get('email', '');
			$name = $app['request']->get('name', '');
			$comment = $app['request']->get('comment', '');
			$the_browser = $app['request']->get('browser', '');
			$the_version = $app['request']->get('version', '');
			$the_platform = $app['request']->get('platform', '');
			$the_useragent = $app['request']->get('useragent', '');
			//$b = new Browser();
			$subject = $_SESSION['company_name']." - TECH SUPPORT";
			//return "NOTHING?" . $_SESSION['company_name'];
			$message = 
			'
			<table>
			<tr><td>Name of Sender</td><td>'.$name.'</td></tr>
			<tr><td>Customer Number</td><td>'.$_SESSION['account_number'].'</td></tr>
			<tr><td>Company Name</td><td>'.$_SESSION['company_name'].'</td></tr>
			<tr><td>Customer Email</td><td>'.$email.'</td></tr>
			<tr><td>Timestamp</td><td>'.date("D M j Y g:i:s").'</td></tr>
			</table>
			<br />
			We have received your request and can respond within 3-5 business days with the solution.
			Keep this email for your records.
			<br /><br />
			We hope to resolve the issue asap, Thank You.
			<br /><br />
			<b>SYSTEM SPECS DETECTED:</b><br />
			<table>
			<tr><td width="20%">Browser &amp; Version</td><td>'.$the_browser.' '.$the_version.'</td></tr>
			<tr><td>Platform</td><td>'.$the_platform.'</td></tr>
			<tr><td>Other Browsers</td><td>'.$the_useragent.'</td></tr>
			</table>
			<br /><br />
			<b>START ISSUE:</b><br />
			'.$comment.'
			<br /><b>END ISSUE:</b>
			<br /><br />
			';
			$email_from = "noreply@davespawnshop.com";
			// create email headers
			$headers = 'From: '.$email_from."\r\n".
			'Reply-To: '.$email_from."\r\n" .
			'Content-Type: text/html; charset=ISO-8859-1\r\n';
			mail($email, $subject, $message, $headers);//TO MEMBER FOR HIS RECORDS
			//mail("swat@davespawnshop.com", $subject, $message, $headers);//TO TECH SUPPORT
			mail("stratmansblues@gmail.com", $subject, $message, $headers);//TO TECH SUPPORT
			mail("nick22891@yahoo.com", $subject, $message, $headers);//TO TECH SUPPORT
			//mail(PRIMARY_EMAIL_FROM, $subject, $message, $headers);//TO TERRY
			return "DONE";
		}//END FUNCTION
                
		function techSupport (Application $app) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);            
            $data = [
            ];
            $content = $app['templating']->render('range/customer/techsupport.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
			$breadcrumbs[1]['title'] = 'Tech Support';
			//$breadcrumbs[2]['title'] = 'Range Usage Report (Graph)';
            $pageTitle = 'Range Controller - Tech Support';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
        
        
        
		function sendRenewalMail(Application $app, $mem_no) {
  	 	 //print 'send mail called'; 
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
   		    $range_owner = range_owner::initWithAccount($link, $hash, $accno);
    		$range_member = range_member::initForMember(
                    $link, $hash, $accno, $mem_no);
    		//print_r($range_owner);
    		//print_r($range_member); 
    		if (!$range_owner || !$range_member) {
     		   return "FAILED HERE";
    		}
    		$data = array_merge(
    	        server_email_template_types::getTemplateVarValuesForSite(), server_email_template_types::getTemplateVarValuesForRangeOwner($range_owner), server_email_template_types::getTemplateVarValuesForMember($range_member)
    		);
            $data['shooter_email'] = $range_member->getEmail();
    		//print_r($data);
    		$tplTypeId = server_email_template_types::MEMBERSHIP_RENEWAL_ID;
    		$set = new server_email_templates($link);
    		$res = $set->sendTemplateMailForAccount(
            $hash, $accno, $tplTypeId, $data);
    		//print 'send result' . $res;
    		$range_member->updateOrInsertMembershipRenewalDate(new \DateTime); 
    		return $res;
		}
		function membershipRenewal (Application $app, $month, $year) {
			$month = $app['request']->get('month2', $month);
			$year = $app['request']->get('year2', $year);
			switch ($month) {
				case 'January' :
					$maxday = 31;
					$month_no = 1;
					break;
				case 'February' :
					$maxday = 28;
					$month_no = 2;
					break;
				case 'March' :
					$maxday = 31;
					$month_no = 3;
					break;
				case 'April' :
					$maxday = 30;
					$month_no = 4;
					break;
				case 'May' :
					$maxday = 31;
					$month_no = 5;
					break;
				case 'June' :
					$maxday = 30;
					$month_no = 6;
					break;
				case 'July' :
					$maxday = 31;
					$month_no = 7;
					break;
				case 'August' :
					$maxday = 31;
					$month_no = 8;
					break;
				case 'September' :
					$maxday = 30;
					$month_no = 9;
					break;
				case 'October' :
					$maxday = 31;
					$month_no = 10;
					break;
				case 'November' :
					$maxday = 30;
					$month_no = 11;
					break;
				case 'December' :
					$maxday = 31;
					$month_no = 12;
					break;
			}
			$from = $month_no . "/1/" . $year;
			$to = $month_no . "/" . $maxday . "/" . $year;
			$type = $app['request']->get('type', 'none');
			//$from = $app['request']->get('from', 'none');
			//$to = $app['request']->get('to', 'none');
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT customers.accountnumber, customers.first_name, customers.last_name, customers.membership_number, customers.home_phone, memberships.membership_expires FROM customers INNER JOIN memberships ON customers.membership_number = memberships.membership_number WHERE customers.accountnumber='" . $accno . "' ORDER BY STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) ASC";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = '';
			$counter = 0;
			$append = '';
			if ($type != 'none') $append = $append . " AND membership_type='" . $type . "'";
			if ($from != 'none' && $to != 'none') $append = $append . " AND STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' )" . " AND STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			foreach ($assoc_array as $row) {
				$name = $row['first_name'] . ' ' . $row['last_name'];
				$mem_no = $row['membership_number'];
				$phone_no = $row['home_phone'];
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'" . $append;
				$member_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$reg_date = $member_row['membership_date'];
				$exp_date = $member_row['membership_expires'];
				$mem_type = $member_row['membership_type'];
				$myArr = explode("/", $exp_date);
				$sortable_exp = $myArr[2] . $myArr[0] . $myArr[1];
				$myArr = explode("/", $reg_date);
				$sortable_reg = $myArr[2] . $myArr[0] . $myArr[1];
				$sql = "SELECT COUNT(*) AS mycount FROM `range_time_tracker` WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'";
				$result = DbHelper::dbFetchRowAssoc($link, $sql);
				$visits = (int) $result['mycount'];
					if (!empty($member_row)) {
						$counter++;
						/*$rows = $rows . '<tr>
                		<td>' . $counter . '</td>
                		<td nowrap="">' . $mem_type . '</td>
                		<td nowrap=""><a href="/xapp/customers/' . $mem_no . '">' . $name . '</a></td>
                		<td nowrap="">' . $phone_no . '</td>
                		<td>' . $mem_no . '</td>
                		<td><span style="color: #fbb91f;">' . $visits . '</span></td>
                		<td sorttable_customkey="' . $sortable_reg . '">' . $reg_date . '</td>
                		<td sorttable_customkey="' . $sortable_exp . '">' . $exp_date . '</td>
                		</tr>';*/
                		$sql = "SELECT * FROM membership_renewal WHERE accountnumber='" . $accno . "' AND membership_number='". $mem_no . "'";// AND expiration_date='" . $exp_date . "'";
                		$result2 = DbHelper::dbFetchRowAssoc($link, $sql);
                		$send_resend = 'Send Email';
                		$sent_date = '';
                		if (!empty($result2)) {
                		$send_resend = "Resend Email";
                		$sent_date = $result2['date_sent'];
                		}
                		$rows = $rows . '<tr>
                  <td>' . $counter . '</td>
                  <td>' . $mem_type . '</td>
                  <td nowrap=""><a href="/xapp/customers/' . $mem_no . '">' . $name . '</a></td>
                  <td>' . $mem_no . '</td>
                  <td><span style="color: #fbb91f;">' . $visits . '</span></td>
                  <td sorttable_customkey="' . $sortable_exp . '">' . $exp_date . '</td>
                  <td><a href="#none" onclick="sendRenewalMail(' . $mem_no . ", '" . $name . "')" . '">' . $send_resend . '</td>
                  <td>' . $sent_date . '</td>
                </tr>';
                	}
			}
            $data = [
                'rows' => $rows,
                'month' => $month,
                'year' => $year,
            ];
            $content = $app['templating']->render('range/reports/membership_renewal.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Membership Renewal';
            $pageTitle = 'Range Controller - Reports - Membership Renewal';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function membershipRenewalPost (Application $app) {
		$month = $app['request']->get('month', 'none');
		$year = $app['request']->get('year', 'none');
			switch ($month) {
				case 'January' :
					$maxday = 31;
					$month_no = 1;
					break;
				case 'February' :
					$maxday = 28;
					$month_no = 2;
					break;
				case 'March' :
					$maxday = 31;
					$month_no = 3;
					break;
				case 'April' :
					$maxday = 30;
					$month_no = 4;
					break;
				case 'May' :
					$maxday = 31;
					$month_no = 5;
					break;
				case 'June' :
					$maxday = 30;
					$month_no = 6;
					break;
				case 'July' :
					$maxday = 31;
					$month_no = 7;
					break;
				case 'August' :
					$maxday = 31;
					$month_no = 8;
					break;
				case 'September' :
					$maxday = 30;
					$month_no = 9;
					break;
				case 'October' :
					$maxday = 31;
					$month_no = 10;
					break;
				case 'November' :
					$maxday = 30;
					$month_no = 11;
					break;
				case 'December' :
					$maxday = 31;
					$month_no = 12;
					break;
			}
			$from = $month_no . "/1/" . $year;
			$to = $month_no . "/" . $maxday . "/" . $year;
			$type = $app['request']->get('type', 'none');
			//$from = $app['request']->get('from', 'none');
			//$to = $app['request']->get('to', 'none');
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT * FROM customers WHERE accountnumber='" . $accno . "' ORDER BY membership_number ASC";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = '';
			$counter = 0;
			$append = '';
			if ($type != 'none') $append = $append . " AND membership_type='" . $type . "'";
			if ($from != 'none' && $to != 'none') $append = $append . " AND STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' )" . " AND STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			foreach ($assoc_array as $row) {
				$name = $row['first_name'] . ' ' . $row['last_name'];
				$mem_no = $row['membership_number'];
				$phone_no = $row['home_phone'];
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'" . $append;
				$member_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$reg_date = $member_row['membership_date'];
				$exp_date = $member_row['membership_expires'];
				$mem_type = $member_row['membership_type'];
				$myArr = explode("/", $exp_date);
				$sortable_exp = $myArr[2] . $myArr[0] . $myArr[1];
				$myArr = explode("/", $reg_date);
				$sortable_reg = $myArr[2] . $myArr[0] . $myArr[1];
				$sql = "SELECT COUNT(*) AS mycount FROM `range_time_tracker` WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'";
				$result = DbHelper::dbFetchRowAssoc($link, $sql);
				$visits = (int) $result['mycount'];
					if (!empty($member_row)) {
						$counter++;
						/*$rows = $rows . '<tr>
                		<td>' . $counter . '</td>
                		<td nowrap="">' . $mem_type . '</td>
                		<td nowrap=""><a href="/xapp/customers/' . $mem_no . '">' . $name . '</a></td>
                		<td nowrap="">' . $phone_no . '</td>
                		<td>' . $mem_no . '</td>
                		<td><span style="color: #fbb91f;">' . $visits . '</span></td>
                		<td sorttable_customkey="' . $sortable_reg . '">' . $reg_date . '</td>
                		<td sorttable_customkey="' . $sortable_exp . '">' . $exp_date . '</td>
                		</tr>';*/
                		$sql = "SELECT * FROM membership_renewal WHERE accountnumber='" . $accno . "' AND membership_number='". $mem_no . "'";// AND expiration_date='" . $exp_date . "'";
                		$result2 = DbHelper::dbFetchRowAssoc($link, $sql);
                		$send_resend = 'Send Email';
                		$sent_date = '';
                		if (!empty($result2)) {
                		$send_resend = "Resend Email";
                		$sent_date = $result2['date_sent'];
                		}
                		$rows = $rows . '<tr>
                  <td>' . $counter . '</td>
                  <td>' . $mem_type . '</td>
                  <td nowrap=""><a href="/xapp/customers/' . $mem_no . '">' . $name . '</a></td>
                  <td>' . $mem_no . '</td>
                  <td><span style="color: #fbb91f;">' . $visits . '</span></td>
                  <td sorttable_customkey="' . $sortable_exp . '">' . $exp_date . '</td>
                  <td><a href="#none" onclick="sendRenewalMail(' . $mem_no . ", '" . $name . "')" . '">' . $send_resend . '</td>
                  <td>' . $sent_date . '</td>
                </tr>';
                	}
			}
            $data = [
                'rows' => $rows,
                'month' => $month,
                'year' => $year,
            ];
            $content = $app['templating']->render('range/reports/membership_renewal.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Membership Renewal';
            $pageTitle = 'Range Controller - Reports - Membership Renewal';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getCompleteMembershipsReport (Application $app) {
			$type = $app['request']->get('type', 'none');
			$from = $app['request']->get('from', 'none');
			$to = $app['request']->get('to', 'none');
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$types = '';
			//grab the list of types for the drop down
			$sql2 = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
			$mem_types_array = DbHelper::dbFetchAllAssoc($link, $sql2);
			foreach ($mem_types_array as $this_type) {
				$types = $types . '<option value="' . $this_type['membership_name'] . '">' . $this_type['membership_name'] . '</option>';
			}
			$start_time = microtime(TRUE);
			/*$sql = "SELECT * FROM customers WHERE accountnumber='" . $accno . "' ORDER BY membership_number ASC";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = '';
			$counter = 0;
			$append = '';
			if ($type != 'none') $append = $append . " AND membership_type='" . $type . "'";
			if ($from != 'none' && $to != 'none') $append = $append . " AND STR_TO_DATE( membership_date,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' )" . " AND STR_TO_DATE( membership_date,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
                        //construct new account number loop
                        $mxnum = count($assoc_array);
                        foreach ($assoc_array as $row) {
                            $mem_no = $row['membership_number'];
                            $mxnum--;
                            $mem_where .= "'". $row['membership_number'] . "'";
                            if($mxnum-- != 0)
                                $mem_where .=  ", ";
                        }
                        //echo $mem_where;
			foreach ($assoc_array as $row) {
				$name = $row['first_name'] . ' ' . $row['last_name'];
				$mem_no = $row['membership_number'];
				$phone_no = $row['home_phone'];
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'" . $append;
				$member_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$reg_date = $member_row['membership_date'];
				$exp_date = $member_row['membership_expires'];
				$mem_type = $member_row['membership_type'];
				$myArr = explode("/", $exp_date);
				$sortable_exp = $myArr[2] . $myArr[0] . $myArr[1];
				$myArr = explode("/", $reg_date);
				$sortable_reg = $myArr[2] . $myArr[0] . $myArr[1];
				$sql = "SELECT COUNT(*) AS mycount FROM `range_time_tracker` WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'";
				$result = DbHelper::dbFetchRowAssoc($link, $sql);
				$visits = (int) $result['mycount'];
					if (!empty($member_row)) {
						$counter++;
						$rows = $rows . '<tr>
                		<td>' . $counter . '</td>
                		<td nowrap="">' . $mem_type . '</td>
                		<td nowrap=""><a href="/xapp/customers/' . $mem_no . '">' . $name . '</a></td>
                		<td nowrap="">' . $phone_no . '</td>
                		<td>' . $mem_no . '</td>
                		<td><span style="color: #fbb91f;">' . $visits . '</span></td>
                		<td sorttable_customkey="' . $sortable_reg . '">' . $reg_date . '</td>
                		<td sorttable_customkey="' . $sortable_exp . '">' . $exp_date . '</td>
                		</tr>';
                	}
			}*/
			//keyur 21-03-2015
			//$sql = "SELECT COUNT(*) AS mycount FROM `range_time_tracker` WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'";
			//$start_time = microtime(TRUE);
			//$sql="SELECT memberships.membership_type,concat(customers.first_name,customers.last_name) as cname,(select count(*) from range_time_tracker where range_time_tracker.accountnumber=$accno and range_time_tracker.membership_number=memberships.membership_number) as visits, customers.home_phone,memberships.membership_number,memberships.membership_date,memberships.membership_expires from memberships,customers where memberships.membership_number=customers.membership_number and customers.accountnumber=memberships.accountnumber and customers.accountnumber=$accno and memberships.membership_number <> '' ";
			//JOIN memberships ON customers.membership_number = memberships.membership_number
			$sql="SELECT * from customers JOIN memberships ON customers.membership_number = memberships.membership_number WHERE customers.acctnumberhashed = '" . $hash . "' AND customers.accountnumber = '" . $accno . "' ";
			if ($type != 'none') $sql = $sql . " AND memberships.membership_type='" . $type . "' ";
			if ($from != 'none' && $to != 'none') $sql = $sql . " AND STR_TO_DATE( memberships.membership_date,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' )" . " AND STR_TO_DATE( memberships.membership_date,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			$sql.=" GROUP BY customers.membership_number ASC";
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			$counter=1;
			$rows="";
			/*foreach($results as $result)
			{
				$rows = $rows . '<tr>
                		<td>' . $counter . '</td>
                		<td nowrap="">' . $result['membership_type'] . '</td>
                		<td nowrap=""><a href="/xapp/customers/' . $result['membership_number'] . '">' . $result['first_name'] .' '.$result['last_name'].'</a></td>
                		<td nowrap="">' . $result['home_phone']. '</td>
                		<td>' . $result['membership_number'] . '</td>
                		<td><span style="color: #fbb91f;">' . $this->numOfVisits($app,$result['membership_number']) . '</span></td>
                		<td sorttable_customkey="' . $result['membership_date'] . '">' . $result['membership_date'] . '</td>
                		<td sorttable_customkey="' . $result['membership_expires'] . '">' . $result['membership_expires'] . '</td>
                		</tr>';
				$counter++;
			}*/
			$end_time = microtime(TRUE);
			//echo ($end_time - $start_time);
            $data = [
                'rows'=>$rows,
                'types'=>$types,
            ];
            $content = $app['templating']->render('range/reports/complete_member_list.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Complete Member List';
            $pageTitle = 'Range Controller - Reports - Complete Member List';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function numOfVisits(Application $app,$membership_number) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$visits_sql = "";
			$visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $membership_number . "' AND acctnumberhashed = '" . $hash . "' AND accountnumber = '" .$accno . "' ";
			if ($result = $link->query($visits_sql)) {
				$count_visits = 0;
				while ($row = $result->fetch_assoc()) {
					$count_visits++;
				}//END WHILE LOOP  
			}//END IF
			return $count_visits;
		}
		function getMembershipsReport (Application $app) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT *, STR_TO_DATE(  membership_expires,  '%m/%d/%Y' ) AS parsedDate FROM memberships WHERE accountnumber='" . $accno . "' AND STR_TO_DATE(  membership_expires,  '%m/%d/%Y' ) > NOW()";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$eachMembershipTypeCounter = array();
			$eachMembershipTypeAccumulator = array();
			foreach ($assoc_array as $mem) {
				$mem_no = $mem['membership_number'];
				$mem_type = $mem['membership_type'];
				if (!isset($eachMembershipTypeCounter[$mem_type])) {
					$eachMembershipTypeCounter[$mem_type] = 0;
					$eachMembershipTypeAccumulator[$mem_type] = 0.00;
				}
				$eachMembershipTypeCounter[$mem_type]++;
				$sql = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "' AND membership_number='" . $mem_no . "' and payment_amount!=0.00 ORDER BY STR_TO_DATE( date,  '%m/%d/%Y' ) DESC LIMIT 1";
				$billing_array = DbHelper::dbFetchAllAssoc($link, $sql);
				foreach ($billing_array as $item) {
					//if (!isset($eachMembersshipTypeAccumulator[$mem_type])) $eachMembershipTypeAccumulator[$mem_type] = 0;
					$eachMembershipTypeAccumulator[$mem_type] = $eachMembershipTypeAccumulator[$mem_type] + $item['payment_amount'];
				}
			}
			$rows = "";
			$total = 0;
			$qty = 0;
			foreach ($eachMembershipTypeCounter as $key=>$value) {
				if ($key != '' && $key != 'Student' && $key != 'Visitor' && $key != 'Law Enforcement' && $key != 'Employee') {
					$rows = $rows . '<tr>
                	<td>' . $key . '</td>
                	<td>' . $value . '</td>
               		<td>$' . $eachMembershipTypeAccumulator[$key] . '</td>
              		</tr>';
              		$qty = $qty + $value;
              		$total = $total + $eachMembershipTypeAccumulator[$key];
              	}
			}
            $data = [
                'rows'=>$rows,
                'qty' => $qty,
                'total' => $total,
            ];
            $content = $app['templating']->render('range/reports/memberships.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Current Memberships';
            $pageTitle = 'Range Controller - Reports - Current Memberships';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function addOrUpdateBenefit (Application $app, $membership_type_id, $quantity, $duration, $benefit_text, $benefit_id) {
			//return "SAASFA";
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			//if benefit id is set (or not 0) use update
			//if not, use insert
			if ((int) $benefit_id != 0) $qsql = "UPDATE benefits SET name='" . $benefit_text . "', duration='" . $duration . "', quantity=" . $quantity . " WHERE id='" . $benefit_id . "'";
			else $qsql = "INSERT INTO benefits (membership_type_id, name, duration, quantity, account_number, acct_number_hashed) VALUES (" . $membership_type_id . ", '" . $benefit_text . "', '" . $duration . "', " . $quantity . ", '" . $accno . "', '" . $hash . "')";
			$link->query($qsql);
		}
        function sendMemberSignUpMail($membershipNo, range_owner $range_owner, \mysqli $link){
            $accHashKey = $range_owner->getAccountHashKey(); //SessionModel::getAccHashKey($app);
            $accNo = $range_owner->getAccountNumber(); //SessionModel::getAccNo($app);
            $range_member = range_member::initForMember(
                    $link, $accHashKey, $accNo, $membershipNo);
            //print_r($range_owner);
            //print_r($range_member); 
            if (!$range_owner || !$range_member) {
                return false;
            }
            $data = array_merge(
                server_email_template_types::getTemplateVarValuesForSite(),
                server_email_template_types::getTemplateVarValuesForRangeOwner($range_owner),
                server_email_template_types::getTemplateVarValuesForMember($range_member)
            );
            $data['shooter_email'] = $range_member->getEmail();
            //print_r($data);
            $tplTypeId = server_email_template_types::MEMBERSHIP_SIGN_UP_ID;
            $set = new server_email_templates($link);
            $res =  $set->sendTemplateMailForAccount(
                    $accHashKey, $accNo, $tplTypeId, $data);      
            //print 'send result' . $res;
            return $res;
        }
        private function getPostForFields($fields, Request $req){
            $results = [];
            foreach($fields as $f) {
                $results[$f] = trim($req->get($f, ''));
            }
            return $results;
        }
        private function getAddMemberFormError($data, Application $app){
            $required = ['firstName', 'lastName', 
                'street', 'city', 'state', 'zipcode',
                'phone1', 'email', 'membershipType'];
            foreach($required as $f) {
                if (empty($data[$f])) {
                    return 'Please fill out all of the required fields marked with *';
                }
            }
            $valida =$app['validator'];
            $emailErrors = $valida->validateValue($data['email'], new Assert\Email());
            if (count($emailErrors)) {
                return 'Email address is invalid';
            }            
            if (!Services\StringHelper::isPhoneValid($data['phone1'])) {
                return ' Phone 1# is an invalid number, please re-enter in '
                . '###-###-#### format';
            }
            if (!empty($data['phone2']) 
                    && !Services\StringHelper::isPhoneValid($data['phone2'])) {
                return ' Phone 2# is an invalid number, please re-enter in '
                . '###-###-#### format';
            }
            if (!empty($data['emergencyPhone']) && 
                    !Services\StringHelper::isPhoneValid($data['emergencyPhone'])) {
                return ' Emergency phone is an invalid number, please re-enter in '
                . '###-###-#### format';
            }
            $regDate = $data['memRegistrationDate'];
            $expDate = $data['memExpirationDate'];
            if (!empty($regDate) && !DateTimeHelper::isDateFormattedAsMdy($regDate)) {
                return 'Invalid membership registration date';
            }
            if (!empty($expDate) && !DateTimeHelper::isDateFormattedAsMdy($expDate)) {
                return 'Invalid membership expiration date';
            }
            if (!empty($regDate) && !empty($expDate)) {
                $reg = DateTimeHelper::mdyToDateTime($data['memRegistrationDate']);
                $exp = DateTimeHelper::mdyToDateTime($data['memExpirationDate']);
                $today = new \DateTime(date('Y-m-d'));
                if ($exp < $reg) {
                    return 'Membership expiration date cannot be less than '
                    . 'registration date';
                }
                if ($exp < $today) {
                    return 'Membership expiration date cannot be less than '
                    . 'today\'s date';
                }
            }
            return false; 
        }
        public function addMember(Application $app) {
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$email_template_sql = "select notify_shooter from server_email_templates where acctnumberhashed = '".$link->real_escape_string($hash) . "' and accountnumber = '".$link->real_escape_string($accno)."' and se_templatetype_fk = 1 ";
			$server_email_check = DbHelper::dbFetchRowAssoc($link, $email_template_sql);

            $contactFields = ['firstName', 'middleName', 'lastName', 
                'street', 'city', 'state', 'zipcode',
                'ssn', 'phone1', 'phone2', 'email', 'email_status'];
            $contactData = $this->getPostForFields($contactFields, $app['request']); 
            $contactData['stateList'] = Services\CommonData::getStates();
            $emergencyFields = ['emergencyName', 'emergencyPhone'];
            $emergencyData = $this->getPostForFields($emergencyFields, $app['request']);
            $membershipFields = ['membershipType', 'memRegistrationDate', 
                'memExpirationDate', 'employee_id'];
            $membershipData = $this->getPostForFields(
                    $membershipFields, $app['request']);
            $membershipData['addMemberTypeUrl'] = $app['get_range_url'](
                        'common/config.php#createnow');
            $connhandle = Services\DbHelper::getWrappedHandle($app['dbs']['main']);
            $memtypes = Models\MembershipType::getMembershipTypesForAccount(
                    $connhandle , $app['range_owner_id']);
            $membershipData['membershipTypes'] = $memtypes;
            $errorMessage = '';
			$pageTokenIdKey = 'addMemberPageToken';
			$emp_id = $membershipData['employee_id'];
            if ($app['request']->get('cmd', '') === 'add') {
                if ($app['session']->get($pageTokenIdKey) !== 
                    $app['request']->get($pageTokenIdKey)) {
                    return $app->redirect($app['get_range_url']('customers/add?rand='.strtotime('now')));
                }
                $data = array_merge($contactData, $emergencyData, $membershipData);
				$data['emp_id'] = $emp_id;
                $errorMessage = $this->getAddMemberFormError($data, $app);
                if (!$errorMessage) {
                    $uniqudata = [
                        'first_name' => $contactData['firstName'],
                        'last_name' => $contactData['lastName'],
                        'middle_name' => $contactData['middleName'],
                        'home_phone' => $contactData['phone1'],
                        'email' => $contactData['email']
                    ];
                    $exists = false;
                    try {
                        $exists = range_member::doesMemberExist($uniqudata, 
                                $app['range_owner_id'],$app['dbs']['main']);
                    } catch (Exception $ex) {
                        $errorMessage = 'Error occured';
                    }
                    if ($exists) {
                        $ufields = array_values(
                                range_member::memberUniqueFields());
                        $errorMessage = "A member with same " . 
                                implode(', ', $ufields) . 
                                " already exists for this account. "
                                . " If this is really a different person, "
                                . "try to put/change middle name to "
                                . "make it unqiue";
                    }
                }
				
                if (!$errorMessage ) {
                    $memNo = range_member::createNewMember($data, 
                            $app['dbs']['main'] , $app['range_owner']);
                    if ($memNo){
                        $app['session']->set($pageTokenIdKey, '');
						if($data['email_status'] == "subscribe" && $server_email_check['notify_shooter'] == 1)
						{
							$this->sendMemberSignUpMail($memNo, $app['range_owner'], Services\DbHelper::getWrappedHandle($app['dbs']['main']));
						}
                        return $app->redirect($app['get_range_url']('customers/'. $memNo));
                    }
                    $errorMessage ='Error occured while saving the member info. '
                                . 'Please try again ' . $memNo;
                }
            } else {
                $app['session']->set($pageTokenIdKey, uniqid("addMember", true));
                $membershipData['memRegistrationDate'] = date('n/j/Y');
            }
            $data = [
                'errorMessage' => $errorMessage,
                'addMemberPostUrl' => $app['get_range_url']('customers/1234'),
                'membershipTypeData' =>  $membershipData,
                'contactData' => $contactData,
                'emergencyData' => $emergencyData,
                'tokenValue' => $app['session']->get($pageTokenIdKey),
                'tokenName' => $pageTokenIdKey
            ];
            $content = $app['templating']->render('range/customer/add.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$title = 'Add New Member';
            $breadcrumbs[] = ['title' => 'Add New Member ' ];
			$pageTitle = 'Range Controller - Members - ' . $title;
            return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
        private function getFieldValues(range_member $range_member) {
            return [
                'firstName' => $range_member->getFirstName(),
                'middleName' => $range_member->getMiddleName(),
                'lastName' => $range_member->getLastName(),
                'street' => $range_member->getAddress()->street,
                'city' => $range_member->getAddress()->city,
                'state' => $range_member->getAddress()->state,
                'zipcode' => $range_member->getAddress()->zipcode,
                'ssn' => $range_member->getSsn(),
                'phone1' => $range_member->getPhone1(),
                'phone2'  => $range_member->getPhone2(),
                'email' => $range_member->getEmail(),
				'email_status' => $range_member->getEmailStatus(),
                'emergencyName' => $range_member->getEmergencyContactName(),
                'emergencyPhone' => $range_member->getEmergencyContactPhone(),
                'membershipType' => $range_member->getMembershipType(),
                'memRegistrationDate' => $range_member->getMembershipDate(),
                'memExpirationDate' => $range_member->getMembershipExpirationDate(),
                'genders' => CommonData::getGenders(),
                'races' => CommonData::getRaces(),
                'stateList' => CommonData::getStates(),
                'hairColors' => CommonData::getHairColors(),
                'eyeColors' => CommonData::getEyeColors(),
                'paymentTypes' => CommonData::getPaymentTypes(),
                'yesNo' => CommonData::getYesNo()                
            ];
        }
        public function setLastManagedMember(Application $app, $memNo){
            $app['session']->set('lastManagedMemberNo', $memNo);
        }
        public function getLastManagedMember(Application $app){
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $roid = $app['range_owner_id'];
            $memberNo = $app['session']->get('lastManagedMemberNo');
            if (!$memberNo) {
                return null;
            }
            $rm = range_member::initForMember($link, 
                    $roid->getAccountHashKey(), $roid->getAccountNo(),
                    $memberNo);
            return $rm;
        }
        public function getScheduleUrlPrefix(Application $app){
            return $app['get_range_url'](
                    'scheduler/point.php?todays_date=1&fromcp=t');
        }
        public function getLogoutRangeUrlPrefix(Application $app){
            return $app['get_range_url'](
                    'login_select_lane.php?s=1&eteled=logout_member&mn=');
        }
        public function getLoginRangeUrlPrefix(Application $app){
            return $app['get_range_url']( 'login_select_lane.php?mn=');
        }
        public function blockUnblock(Application $app) {           
            $isBlock = $app['request']->request->get('isBlock',0);
            $memberNo = (int)$app['request']->request->get('membershipNo');
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $roid =  $app['range_owner_id'];
            $range_member = range_member::initForMember($link, 
                    $roid->getAccountHashKey(), $roid->getAccountNo(), 
                    $memberNo);
            if (!$range_member) {
                return $app->json(['error' => 'Error occured '], 400);
            }
            if ($isBlock) {
                $r = $range_member->blockMember($app['dbs']['rcadmin']);
            } else {
                $r = $range_member->unBlockMember($app['dbs']['rcadmin']);
            }
            if ($r) {
                return $app->json(['status' => 1, 
                    'payload' => $isBlock ? 'blocked' : 'unblocked'] );
            }
            return $app->json(['error' => 'Eror occured'], 400);
        }
        public function editMember(Application $app, $membershipNo) {
            $this->setLastManagedMember($app, $membershipNo);
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $roid =  $app['range_owner_id'];
            $range_member = range_member::initForMember($link, 
                    $roid->getAccountHashKey(), $roid->getAccountNo(), $membershipNo);
            $content = '';
            if (!$range_member) {
                $content = $app['templating']->render('range/error-message.php', 
                        ['message' => "Member with id $membershipNo could not be found"]);
            } else {
                $memtypes = Models\MembershipType::getMembershipTypesForAccount(
                    $link , $app['range_owner_id']);
                $vals = $this->getFieldValues($range_member);
                $vals['membershipTypes'] = $memtypes;
                $billingList = $app['range_owner']->getBillingInfo($range_member->getMembershipNumber());
                $vals['billingList'] = empty($billingList) ? [] : $billingList;
                $vals['schedulerUrlPrefix'] = $this->getScheduleUrlPrefix($app);
                $vals['loginRangeUrlPrefix'] = $this->getLoginRangeUrlPrefix($app);
                $vals['logoutRangeUrlPrefix'] = 
                        $this->getLogoutRangeUrlPrefix($app);
                $vals['isMemberBlocked'] = $range_member->isBlocked(
                        $app['dbs']['rcadmin']);
                $vals['addMemberTypeUrl'] = $app['get_range_url'](
                        'common/config.php#createnow');
				//get employee that added the member
				$sqll = "SELECT * FROM `customers` WHERE `membership_number`='" . $membershipNo . "' AND `accountnumber`='" . $roid->getAccountNo() . "'";
				$results = DbHelper::dbFetchRowAssoc($link, $sqll);
				$eid = $results['emp_id'];
				$sql2 = "SELECT * FROM employees WHERE id=" . $eid;
				$results2 = DbHelper::dbFetchRowAssoc($link, $sql2);
				$emp_name = $results2['fname'] . " " . $results2['lname'];
				//variables for benefits section
				$listOfBenefits = "";
				$rid = $app['range_owner_id'];
				$acno = $rid->getAccountNo();	
				$hash = $rid->getAccountHashKey();
				$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				$sql1 = "SELECT * FROM memberships WHERE membership_number='" . $membershipNo . "'";
				$mem_array = DbHelper::dbFetchRowAssoc($link, $sql1);
				$membershiptypename = $mem_array['membership_type'];
				//get member type id to start fetching benefits
				$sql2 = "SELECT * FROM editable_memberships WHERE membership_name='" . $membershiptypename . "' AND accountnumber='" . $acno . "'";
				$mem_id_array = DbHelper::dbFetchRowAssoc($link, $sql2);
				$membertypeid = $mem_id_array['membership_type_id'];
				//get the benefits for this membership type next
				$sql3 = "SELECT * FROM benefits WHERE membership_type_id='" . $membertypeid . "'";
				$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql3);
				$listOfBenefits = "";
				$row_counter = 0;
				$overview_benefits = "";
				foreach ($assoc_array as $benefitArr) {
					$row_counter++;
					$redeemableamount = $amountused = $max = 0;
					$benefit_id = $benefitArr['id'];
					$benefit_name = $benefitArr['name'];
					$max = $benefitArr['quantity'];
					$sql4 = "SELECT * FROM benefits_used WHERE customer_id='" . $membershipNo . "' AND benefit_id=" . $benefit_id . "";
					$assoc_array2 = DbHelper::dbFetchAllAssoc($link, $sql4);
					//now count the usages of this benefit for this user
					foreach ($assoc_array2 as $benefit_used) {
						$amountused += (int) $benefit_used['amount_used'];
					}
					$redeemableamount = $max - $amountused;
					$overview_benefits = $overview_benefits . '<tr><td><span style="color: red;">' . $amountused . ' of ' . $max . '</span></td><td>' . $benefit_name . '</td></tr>';
					//finish counting this user's usages of this benefit
					$listOfBenefits = $listOfBenefits . '
						<tr>
						<td class="text-center" style="width:50px;"><input id="benefit-id-' . $row_counter . '" type="hidden" value="' . $benefit_id . '"/>
						<select id="amount-' . $row_counter . '" style="width:40px;"><option value="0" selected></option>';
							for ($i = 1;$i <= $redeemableamount;$i++) {
								$listOfBenefits = $listOfBenefits . '<option value="' . $i . '">' . $i . '</option>';
							}						
					$listOfBenefits = $listOfBenefits . '</select>
						</td>
						<td nowrap=""><span style="color: red;">' . $amountused . ' of ' . $max . '</span></td>
						<td nowrap="">' . $benefit_name . '</td>
						</tr>
					';
				}	
				//and now - get the used benefits
				$sql4 = "SELECT * FROM benefits_used WHERE customer_id='" . $membershipNo . "'";
				$assoc_array2 = DbHelper::dbFetchAllAssoc($link, $sql4);
				$listOfUsedBenefits = "";
				foreach ($assoc_array2 as $benefit_used) {
					$ben_id = $benefit_used['benefit_id'];
					$used_ben_id = $benefit_used['id'];
					$employee_id = $benefit_used['employee_id'];
					//get employee initials
					$newsql = "SELECT * FROM employees WHERE id=" . $employee_id;
					$emp_name_array = DbHelper::dbFetchRowAssoc($link, $newsql);
					$employee_name = $emp_name_array['initials'];
					//
					$getnamesql = "SELECT * FROM benefits WHERE id=" . $ben_id . "";
					$get_name_array = DbHelper::dbFetchRowAssoc($link, $getnamesql);
					$dateArr = explode("-", $benefit_used['date']);
					$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
					$listOfUsedBenefits = $listOfUsedBenefits . '<tr>
								<td style="width:60px;">' . $parsedDate . '</td>
								<td><span style="color: red;">' . $benefit_used['amount_used'] . '</span></td>
								<td>' . $get_name_array['name'] . '</td>
								<td>' . $employee_name . '</td>
								<td style="width:60px;"><span id="deletebtn-' . $ben_id . '"><a onclick="return myConfirm(\'Are you sure you want to unredeem this benefit?\', ' . $used_ben_id . ')" href="/xapp/benefits/unredeem/' . $membershipNo . '/' . $used_ben_id . '">Delete</a></span></td>
							</tr>
							<tr>
								<td colspan="5" style="font-size: 14px;">' . $benefit_used['comments'] . '</td>
							</tr>';
				} 
				/*<!--<tr>
								<td>12/4/2014</td>
								<td><span style="color: red;">2</span></td>
								<td>Guest passes per year (Same lane as Member)</td>
								<td><a href="#">Delete</a></td>
							</tr>
							<tr>
								<td colspan="4" style="font-size: 14px;">Notes Here......</td>
							</tr>-->*/
							$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please select an employee!"]);	
				$benVariables = [
					'membershiptypename' => $membershiptypename,
					'listOfBenefits' => $listOfBenefits,
					'listOfUsedBenefits' => $listOfUsedBenefits,
					'customer_id' => $membershipNo,
					'overview_benefits' => $overview_benefits,
					'employee_name' => $employee_name,
					'errmsg' => $err,
				];
				//end of variables for benefits section
                $data = [                    
                    'memberAddUrl' => $app['get_range_url']('customers/add'),
                    'member' => $range_member,
                    'values' =>  $vals + ['emp_name' => $emp_name],//magic
                    'overview' => $app['templating']->render(
                            'range/customer/overview.php', 
                            $vals+['member'=>$range_member, 'emp_name' => $emp_name]),
					'benVariables' => $benVariables,
					'link' => $link,
					'emp_name' => $emp_name,
					'mem_no' => $membershipNo,
                ];
                $content = $app['templating']->render('range/customer/edit.php', $data);
            }
            $breadcrumbs = $this->getBreadcrumbs($app);
            $title = 'Manage Member: ' .  $range_member->getFullName();
            $breadcrumbs[] = ['title' => $title];
            $pageTitle = 'Range Controller - Members - ' . $title;
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		function redeemBenefit (Application $app, $customer_id, $benefit_id, $amount, $comments, $employee_id) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			//if benefit id is set (or not 0) use update
			//if not, use insert
			$qsql = "INSERT INTO benefits_used (employee_id, benefit_id, customer_id, comments, amount_used, date) VALUES (" . $employee_id . ", " . $benefit_id . ", '" . $customer_id . "', '" . $comments . "', " . $amount . ", " . "CURDATE()" . ")";
			$link->query($qsql);
			return $qsql;
		}
		function unredeemBenefit (Application $app, $customer_id, $benefit_id) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qsql = "DELETE FROM benefits_used WHERE customer_id='" . $customer_id . "' AND id=" . $benefit_id;
			$link->query($qsql);		
		}
        public function getOverview(Application $app) {
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $roid =  $app['range_owner_id'];
            $range_member = range_member::initForMember($link, 
                    $roid->getAccountHashKey(), $roid->getAccountNo(),
                    $app['request']->get('membershipNo',0));
            $vals = $this->getFieldValues($range_member);
            return $app['templating']->render(
                            'range/customer/overview.php', 
                            $vals+['member'=>$range_member]);
        }
        public function updateMemberJson(Application $app){   
            $conn = $app['dbs']['main'];
            $roid =  $app['range_owner_id'];
            $range_member = range_member::initForMember(DbHelper::getWrappedHandle($conn), 
                    $roid->getAccountHashKey(), $roid->getAccountNo(), 
                    $app['request']->get('membershipNo',0));
            if (empty($range_member)) {
                return $app->json(['error'=>'Invalid member'], 400);
            }
            $post = $app['request']->request->all();
			
            $cmd = $app['request']->get('cmd', '');
			$emp_id = $app['request']->get('emp_id', '');
			$payment_for = $app['request']->get('payment_for', '');
            $payload = null;
            if ($cmd == 'updateContact') {
                $fieldMap = $this->getFieldMapsForTable('customers');
                $vals = $this->getDataThatIsSet($fieldMap, $post);
                $range_member->updateCustomers($vals, $conn);
                $fieldMapEContact = $this->getFieldMapsForTable('emergency_contact');
                $vals2 = $this->getDataThatIsSet($fieldMapEContact, $post);
                $range_member->updateOrInsertCommon('emergency_contact', $vals2, $conn);
            } else if ($cmd == 'updateMembership') {
                $fieldMap = $this->getFieldMapsForTable('memberships');
                $vals = $this->getDataThatIsSet($fieldMap, $post);
                $range_member->updateOrInsertCommon('memberships', $vals, $conn);
            } else if ($cmd == 'updateDriversLicense') {                
                $vals = $this->getDlFromPost($app['request']);
                $range_member->updateOrInsertCommon('driver_license_info', $vals, $conn);
            } else if ($cmd == 'updateComments') {
                $vals = ['license_comments' => 
                    $app['request']->request->get('commentsText', '')];
                $range_member->updateOrInsertCommon('driver_license_info', $vals, $conn);
               // echo $range_member;
            } else if ($cmd == 'addBilling') {
                $fieldMap = $this->getFieldMapsForTable('billing_info');
                $vals = $this->getDataThatIsSet($fieldMap, $post);
                $vals['membership_type'] = $range_member->getMembershipType();
                $vals['source'] = 'range member account';
                //$vals['id'] = strtotime('now');
				$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				$qry = "SELECT * FROM employees WHERE id=" . $emp_id;
				$results = DbHelper::dbFetchRowAssoc($link, $qry);
				//$emp_name = $results['fname'] . " " . $results['lname'];
				$emp_name = $results['initials']; 
				$vals['employee_id'] = $emp_id;
				$vals['employee_name'] = $emp_name;
				$vals['payment_for'] = $payment_for;
				//$emp_id
                //placeholder
                $range_member->addBilling( $vals, $conn);
                $bl =  $app['range_owner']->getBillingInfo(
                        $range_member->getMembershipNumber());
                $payload = empty($bl) ? null : $bl[0];
            } else if ($cmd == 'deleteBilling') {
                $id =  $app['request']->get('id', '');
                $res = $range_member->deleteBilling($id, $conn);
                $bl = $app['range_owner']->getBillingInfo(
                        $range_member->getMembershipNumber());
                $payload = empty($bl) ? [] : $bl; 
            } else if ($cmd == 'updateBilling') {                
                $res = $range_member->updateBilling($post, $post['id'], $conn);
                $bl = $app['range_owner']->getBillingInfo(
                        $range_member->getMembershipNumber());
                $payload = empty($bl) ? [] : $bl; 
            } else if ($cmd == 'updateAdditionalInfo') {
                $fieldMap = $this->getFieldMapsForTable('certs_and_safety');
                $vals = $this->getDataThatIsSet($fieldMap, $post);
                $range_member->updateOrInsertCommon('certs_and_safety', $vals, $conn);
            } else if ($cmd == 'getOverview') {
                return $this->getOverview($app);
            } else {
                return $app->json(['message' => 'Invalid command'],400 );
            }
            return $app->json(['status'=>1, 'payload' => $payload]);
        }
        private function getDataThatIsSet($fieldMap, $data){
            $vals =[];
            foreach($fieldMap as $dbcol => $dataKey) {
                if (isset($data[$dataKey])) {
                    $vals[$dbcol] = $data[$dataKey];
                }
            }
            return $vals;
        }
        private function getDlFromPost(Request $req){
            $dl = new Models\DriversLicense();
            $dl->state = $req->request->get('dlState', '');
            $dl->number = $req->request->get('dlNumber', '');
            $dl->expirationDate = $req->request->get('dlExpiration', '');
            $dl->heightFeet = $req->request->get('dlHeightFeet', '');
            $dl->heightInches = $req->request->get('dlHeightInches', '');
            $dl->weight = $req->request->get('dlWeight', '');
            $dl->hairColor = $req->request->get('dlHair', '');
            $dl->eyeColor = $req->request->get('dlEyes', '');
            $dl->race = $req->request->get('dlRace', '');
            $dl->gender = $req->request->get('dlGender', '');
            $dl->birthDate = $req->request->get('dlBirthDate', '');
            $dl->birthCity  = $req->request->get('dlBirthCity', '');
            $dl->birthState  = $req->request->get('dlBirthState', '');
            //$dl->comments = $req->request->get('comments', '');
            $dl->comments = $req->request->get('commentsText', '');
            return $dl->getTableRowArray();
        }
        private function getFieldMapsForTable($table) {
            $maps = [
                'customers' => [
                    'first_name' => 'firstName',
                    'middle_name' => 'middleName',
                    'last_name' => 'lastName',
                    'street_address' => 'street',
                    'city' => 'city',
                    'state' => 'state',
                    'zipcode' => 'zipcode',
                    'ssn' => 'ssn',
                    'home_phone' => 'phone1',
                    'cell_phone' => 'phone2',
                    'email' => 'email',
					'email_status' => 'email_status',
                ],
                'memberships' => [
                    'membership_type' => 'membershipType',
                    'membership_expires' => 'memExpirationDate',
                    'membership_date' => 'memRegistrationDate'
                ],
                'emergency_contact' => [
                    'contact_name' => 'emergencyName',
                    'contact_phone' => 'emergencyPhone'
                ],
                'billing_info' => [
                    'date' => 'billingDate',
                    'payment_method' => 'billingPaymentType', 
                    'payment_amount' => 'billingAmount',
                    'payment_for' => 'billingPaymentFor'
                ],
                'certs_and_safety' => [
                    'certs_video_complete' => 'safetyVideoCompleted',
                    'certs_safety_form_complete' => 'safetyRangeFormCompleted',
                    'certs_certificates_comments' => 'certsComments'
                ]
            ];
            return isset($maps[$table]) ? $maps[$table] : null;
        }
        private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $data['breadcrumbs']]);
            $vars = [
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
        }
        private function getBreadcrumbs(Application $app){
            return array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Members']
            );
        }
		private function getBreadcrumbs_config(Application $app){
            return array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations']
            );
        }
        public function getMembersJson(Application $app){
            $filters = [];
            $req = $app['request'];
            $filterType = $req->get('filterType', '');
            if ($filterType) {
                $filters[$filterType] = $req->get('filterValue', '');
            }
            $currentPage = $req->get('currentPage', 1);
            $pager = new Services\Pager($currentPage, $this->pageSize);
            $pagedResults=  $app['range_owner']->getMembers($filters, $pager);
            return $app->json($pagedResults->toArray());
        }
		
        public function uploadImage(Application $app, $memberNo){
            $req = $app['request'];
            /* @var range_owner_id */
            $range_owner_id = $app['range_owner_id'];
            $file = $req->files->get('file'); 
            if (empty($file)) {
                return $app->json(['error'=> 'No file is sent']);
            }   
            $path = $app['range_member_image_dir'] . '/';            
            //$filename = $files['FileUpload']->getClientOriginalName();
            $mime = $file->getMimeType();
            $okMimes = $this->acceptedImageMimeTypes();
            if (!in_array($mime, array_keys($okMimes))) {
                return $app->json(['error' => 'Invalid file format; please use jpg/jpeg, gif, png files']);
            }
            if (empty($memberNo)) {
                return $app->json(['error' => 'Member no is missing']);
            }
//            $finder = new Finder();
//            $files = $finder->files()->name($memberNo . '.*')->in($path);
//            //$f = new File;
//            foreach( $files as $f) {
//                unlink($path . $f->getFilename());
//            }
            $this->deleteMemberImage($memberNo, $path);
            $filename = $memberNo . '.' . $okMimes[$mime];
            $file->move($path, $filename);
            $url = str_replace($app['config']['base_path'], '', $path . $filename);
            //$files['FileUpload']->move($path,$filename);
            return $app->json(['url' => $url]);      
        }
        private  function deleteMemberImage($memberNo, $path) {
            $finder = new Finder();
            $files = $finder->files()->name($memberNo . '.*')->in($path)->followLinks();
            //$f = new File;
            foreach( $files as $f) {
                @unlink($path . $f->getFilename());
            }
        }
        public function downloadImage(Application $app, $memberNo) {
            $imgpath = $app['range_member_image_path']($memberNo);
            if (!empty($imgpath) && file_exists($imgpath)) {
                return $app->sendFile($imgpath)
                        ->setContentDisposition(
                                ResponseHeaderBag::DISPOSITION_ATTACHMENT, 
                                basename($imgpath));
            }
            return null;
        }
        public function saveWebcamImage(Application $app, $memberNo){
            $dir = $app['range_member_image_dir']; 
            $fname = $dir ."/" . (int)$memberNo . '.png';
            $this->deleteMemberImage($memberNo, $dir . '/');
            if ($_POST['type'] == "pixel") {
                    // input is in format 1,2,3...|1,2,3...|...
                    $im = imagecreatetruecolor(320, 240);
                    foreach (explode("|", $_POST['image']) as $y => $csv) {
                            foreach (explode(";", $csv) as $x => $color) {
                                    imagesetpixel($im, $x, $y, $color+0);
                            }
                    }
                    $r = imagepng($im, $fname );
            } else { //type=data
                    // input is in format: data:image/png;base64,...
                    $im = imagecreatefrompng($_POST['image']);
                    $r = imagejpeg($im, $fname);
            }
            $url = str_replace($app['config']['base_path'], '', $fname);
            if ($r) {
                return $app->json(['url' => $url]);
            } else {
                return $app->json(['error' => 'Error occured while saving', 'file'=>$fname] );
            }
        }
        private function acceptedImageMimeTypes(){
            return [
                'image/jpeg' => 'jpg', 
                'image/jpeg'=> 'jpeg',
                'image/png' => 'png',
                'image/gif' => 'gif'
                ];
        }
		public function printIdCard(Application $app, $membershipNo){
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$get_id_card_creator = range_member::get_id_card_creator($app, $hash, $accno);
			$get_customer = range_member::get_customer($app, $hash, $accno, $membershipNo);
			$get_emergency_contact = range_member::get_emergency_contact($app, $hash, $accno, $membershipNo);
			$get_driver_license_info = range_member::get_driver_license_info($app, $hash, $accno, $membershipNo);
			$get_memberships = range_member::get_memberships($app, $hash, $accno, $membershipNo);
			$get_certs_and_safety = range_member::get_certs_and_safety($app, $hash, $accno, $membershipNo);
			$get_controller_config = range_member::get_controller_config($app, $hash, $accno, $membershipNo);
			$get_member_login = range_member::get_member_login($app, $hash, $accno, $membershipNo);
			$range_member = range_member::initForMember($link, $hash, $accno, $membershipNo);
			$data = array(
				'get_id_card_creator' =>  $get_id_card_creator,
				'get_customer' => $get_customer,
				'get_emergency_contact' => $get_emergency_contact,
				'get_driver_license_info' => $get_driver_license_info,
				'get_memberships' => $get_memberships,
				'get_certs_and_safety' => $get_certs_and_safety,
				'get_controller_config' => $get_controller_config,
				'get_member_login' => $get_member_login,
				'member' => $range_member,
				'rangeLogoUrl' => $app['range_logo_url']
            );
			return $app['templating']->render('range/customer/printidcard.php', $data);
        }
		public function getEmployeeReport(Application $app, $from, $to, $name, $empid)
		{
            //$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            //$pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
           // $records= $range_owner->getMembers([], $pager);
		   $from = str_replace("%2F", "/", $from);
		   $to = str_replace("%2F", "/", $to);
		   $payment_types_acc = array();
		   $payment_types_acc['Cash'] = 0;
		   $payment_types_acc['Visa'] = 0;
		   $payment_types_acc['Mastercard'] = 0;
		   $payment_types_acc['Check'] = 0;
		   $payment_types_acc['Discover'] = 0;
		   $payment_types_acc['Debit'] = 0;
		   $payment_types_acc['Paypal'] = 0;
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			if ($name != "unfiltered")  
			{
				if($empid != 0 )
				{
					 $rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "'" . " AND employee_id='" . $empid . "' AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )" . " ORDER BY transaction_number";
				}
				else
				{
					 $rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "'" . " AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )" . " ORDER BY transaction_number";		
				}
			}
			else 
			{
				$rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' ORDER BY transaction_number";
			}
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
			$pos_transaction_rows = "";
			$pos_grand_total = 0;
			//get number of times id exists in table - if the id row counter is 0, assign the number to it
			//else if the id row counter is not zero, decrement by one
			//if number of times = row counter -> set rowspan to number of times on the two relevant columns
			//else if not equal and number of times > 1 -> omit relevant columns
			//else display columns as normal
			$lastTransactionNo = 0;
			$thisTransactionNo = 0;
			foreach ($assoc_array as $transaction) {
			$thisTransactionNo = $transaction['transaction_number'];
			if($empid != 0)
			{
				$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $thisTransactionNo . " AND accountnumber='" . $accno . "' AND employee_id='" . $empid . "'";
			}
			else
			{
				$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $thisTransactionNo . " AND accountnumber='" . $accno . "'";	
			}
			$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
			$rowspan = $resultsArr['mycount'];
			if ($thisTransactionNo != $lastTransactionNo) $payment_types_acc[$transaction['payment1_method']] += $transaction['total']; 
			if ($thisTransactionNo != $lastTransactionNo) $pos_grand_total = $pos_grand_total + ((double) $transaction['total']);
			    $pos_transaction_rows = $pos_transaction_rows . '<tr class="pos_class">';
                  if ($thisTransactionNo != $lastTransactionNo) $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">' . $transaction['transaction_number'] . '</td>';
                  $pos_transaction_rows = $pos_transaction_rows . '<td>' . $transaction['quantity'] . '</td>
                  <td>' . $transaction['description'] . '</td>
                  <td>' . $transaction['item_number'] . '</td>
                  <td>$' . $transaction['price'] . '</td>';
                  if ($thisTransactionNo != $lastTransactionNo) $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">$' . $transaction['total'] . '</td>';
				  if ($thisTransactionNo != $lastTransactionNo) {
				  $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">';
				  if ((float) $transaction['discount'] <= 0) $pos_transaction_rows = $pos_transaction_rows . '<span style="color: red; font-weight: bold;">X</span>';
				  else $pos_transaction_rows = $pos_transaction_rows . '<span style="color: green; font-weight: bold;">&#10003;</span>';
				  //<span style="color: green; font-weight: bold;">&#10003;</span> or <span style="color: red; font-weight: bold;">X</span>
				  $pos_transaction_rows = $pos_transaction_rows . '</td>';
				  }
                $pos_transaction_rows = $pos_transaction_rows . '</tr>';
				$lastTransactionNo = $thisTransactionNo;
			}
			/*<tr>
                  <td>1617</td>
                  <td>2</td>
                  <td nowrap="">FEDERAL 22 LR 40 GR SOLID AUTOMATCH</td>
                  <td>I-42895</td>
                  <td>$5.99</td>
                  <td>$11.83</td>
                  <td><span style="color: green; font-weight: bold;">&#10003;</span> or <span style="color: red; font-weight: bold;">X</span></td>
                </tr>*/
			$mem_types_query = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $mem_types_query);
			$membership_types = array();
			$membership_types_sub = array();
			$membership_type_names = array('', '', '', '', '', '', '', '', '', '');
			$arr_index = 0;
			foreach ($assoc_array as $mem_type) {
				$keyname = $mem_type['membership_name'];
				$membership_type_names[$arr_index] = $keyname;
				$membership_types[$keyname] = 0;//will hold number of memberships sold  for this type		
				$membership_types_sub[$keyname] = 0;
				$arr_index++;
			}
			if ($name != "unfiltered") 
			{
				if($empid != 0)
				{
					 $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'" . " AND employee_id='" . $empid . "' AND STR_TO_DATE( date,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( date,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
				}
				else
				{
					 $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'" . " AND STR_TO_DATE( date,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( date,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";		
				}
			}
			else
			{
				 $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'";
			}
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
			foreach ($assoc_array as $billing) {
				if ($billing['payment_for'] != "Class" && $billing['payment_for'] != "Visit") $this_mem = $billing['membership_type'];
				else if ($billing['payment_for'] == "Class") $this_mem = 'Student';
				else if ($billing['payment_for'] == "Visit") $this_mem = 'Visitor';
				$payment_types_acc[$billing['payment_method']] += $billing['payment_amount'];
				foreach ($membership_type_names as $memtype) {
					if ($this_mem == $memtype) {
						$membership_types[$memtype]++;//add to number of this type sold
						$membership_types_sub[$memtype] = $membership_types_sub[$memtype] + $billing['payment_amount'];
					}
				}
			}
			$memberships_transaction_rows = "";
			$memberships_grand_total = 0;
			$memberships_total_count;
			foreach ($membership_type_names as $memtype) {
				$memberships_grand_total = $memberships_grand_total + $membership_types_sub[$memtype];
				$memberships_total_count = $memberships_total_count + $membership_types[$memtype];
				$memberships_transaction_rows = $memberships_transaction_rows . '<tr class="memberships_class">
                  <td>' . $memtype . '</td>
                  <td>' . $membership_types[$memtype] . '</td>
                  <td>$' . number_format($membership_types_sub[$memtype], 2) . '</td>
                </tr>';
			}
			/*
                <tr>
                  <td>Platinum</td>
                  <td>8</td>
                  <td>$2,218.75</td>
                </tr>
			*/
			$employees = "SELECT * FROM employees WHERE accountnumber = '".$_SESSION['account_number']."' ORDER BY fname";
			$getemplists = DbHelper::dbFetchAllAssoc($link, $employees);
			$employees_name = "SELECT `fname`, `lname` FROM employees WHERE id = '".$empid."'";
			$getemp_name = DbHelper::dbFetchAllAssoc($link, $employees_name);
            $data = [
                'pos_transaction_rows' => $pos_transaction_rows,
				'memberships_transaction_rows' => $memberships_transaction_rows,
				'pos_grand_total' => number_format($pos_grand_total, 2),
				'memberships_grand_total' => number_format($memberships_grand_total, 2),
				'memberships_total_count' => $memberships_total_count,
				'cash' => $payment_types_acc['Cash'],
				'check' => $payment_types_acc['Check'],
				'debit' => $payment_types_acc['Debit'],
				'mastercard' => $payment_types_acc['Mastercard'],
				'visa' => $payment_types_acc['Visa'],
				'discover' => $payment_types_acc['Discover'],
				'paypal' => $payment_types_acc['Paypal'],
				'drawer_end' => number_format(($pos_grand_total + $memberships_grand_total), 2),
				'report_type' => $name,
				'start_date' => $from,
				'end_date' => $to,
				'getemplists'=>$getemplists,
				'emp_name'=> $getemp_name,
            ];            			
            $content = $app['templating']->render('range/reports/employee_reports.php', $data)
                    ;
            //$breadcrumbs = $this->getBreadcrumbs($app);
            //$breadcrumbs[1]['link'] = '';
			$breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => '/xapp/reports', 'title' => 'Reports'], ['title' => 'Employee Report']);
            //$breadcrumbs[1]['link'] = '/xapp/configurations.php';
            $pageTitle = 'Range Controller - Reports - Employee Report';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		function getGraphLaneReport (Application $app) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$type = $app['request']->get('type', '');
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		    if($from != "" && $to != "")
			{
				$sql = "select lane_number,count(lane_number) as total from range_time_tracker where accountnumber=$accno AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' ) group by lane_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$type = "CUSTOM";
			}
			$decrement = 0;
			if($type != '' && $type != "CUSTOM")
			{
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "select lane_number,count(lane_number) as total from range_time_tracker where accountnumber=$accno  AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY ) group by lane_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);	
			}
			else if ($type != "CUSTOM")
			{
				$type = "UNFILTERED";
			$sql = "select lane_number,count(lane_number) as total from range_time_tracker where accountnumber=$accno group by lane_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			}
			$sql_lanes_no = "select number_of_lanes from controller_config where accountnumber=$accno";
			$total_lanes_no = DbHelper::dbFetchRowAssoc($link, $sql_lanes_no);
			$laneNo = $total_lanes_no['number_of_lanes'];
			$result = array();
			for($i=1;$i<=$laneNo;$i++)
			{
				$result[$i]=0;
			}
			$lan = array();
			foreach($result as $key=>$value){
				$lan[$key]=$key;
			}
			$lane_number = 'labels: ['.implode(',',$lan).']';
			foreach($assoc_array as $key=>$value)
			{	
				$result[$value['lane_number']]=$value['total'];
			}
			$lane_number_data = 'data: ['.implode(',',$result).']';
			$to = " to <br>" . $to;
		if ($type == "UNFILTERED") $from = $to = "";
            $data = [
				'lane_number' => $lane_number,
                'lane_number_data' => $lane_number_data,
                'type' => $type,
                'from' => $from,
                'to' => $to,
            ];
            $content = $app['templating']->render('range/reports/range_usage_graph_lane.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Range Lane Usage Report (Graph)';
            $pageTitle = 'Range Controller - Reports - Range Lane Usage Graph'; 
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		public function filterTypes(Application $app){
	
			$filterType = $_POST['filterType']; 

			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$qsql = "UPDATE admin_login SET filtertypes='" . $filterType . "' WHERE accountnumber='" . $accno . "'";
			
			$link->query($qsql);
			return $app->json();
        }
	
    }*/
	
}