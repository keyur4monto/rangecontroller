<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    use Rc\Services\ArrayHelper;
	use Rc\Services\MailHelper;
    ///include('//includes/browser.php');
    
    class ServerEmailConfigController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			if ($app['request']->get('restore_templates_to_defaults', '') != '') {
				  $link = DbHelper::getWrappedHandle($app['dbs']['main']);
				  $sql_clean = " delete from server_email_templates where acctnumberhashed = '".$link->real_escape_string($hash) . "' and accountnumber = '".$link->real_escape_string($accno)."'";
      
        $sql_restore = " insert into server_email_templates "
            . "(acctnumberhashed, accountnumber, se_templatetype_fk, "
            . " template_subject, template_body, notify_admin, notify_shooter) "
            . " select '" . $link->real_escape_string($hash) . "', "
            . " '" . $link->real_escape_string($accno) . "', "
            . " se_templatetype_fk, "
            . " template_subject, template_body, notify_admin, notify_shooter "
            . " from server_email_templates "
            . "where acctnumberhashed='default' ";
				$res1 = $link->query($sql_clean);
				$res2 = $link->query($sql_restore);
			 }
			 
			$content = $app['templating']->render('range/serveremailconfig/list.php');
            $breadcrumbs = $this->getBreadcrumbs($app);
            //$breadcrumbs[1]['link'] = '';
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'configurations';
			$breadcrumbs[1]['title'] = 'Module Configurations';
			$breadcrumbs[2]['title'] = 'Server Email Settings';
            $pageTitle = 'Range Controller - Module Configurations - Server Email Settings';
			 
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function setTemplate(Application $app){
				 if ($app['request']->get('cmd', '') === 'updateTemplate') {
					 $link = DbHelper::getWrappedHandle($app['dbs']['main']);
					$tplSubject=$_POST['template_subject'];
					$tplBody=$_POST['template_body'];
					$id=$_POST['id'];
					  $sql = " update server_email_templates ". " set template_subject = '" . $link->real_escape_string($tplSubject) . "' ". ", template_body = '" . $link->real_escape_string($tplBody) . "' ". " where id = " . (int)($id);
					  $link->query($sql);
					return 'success';
				 }
				 else if($app['request']->get('cmd', '') === 'updateNotifyMulti'){
						$link = DbHelper::getWrappedHandle($app['dbs']['main']);
						 $recs = $_POST['data'];
						foreach($recs as $rec) {
							$notifyWhat = $rec['notifyWhat'];
							if (in_array($notifyWhat, array('notify_admin', 'notify_shooter'))) {
								//$result = $setpl->updateTemplateNotif($rec['id'], $notifyWhat, $rec['val']);
									$notifyCols = array('notify_admin', 'notify_shooter');
									if (!in_array($notifyWhat, $notifyCols)) {
										throw new \Exception('$notifColName must be one of ' . implode(',', $notifyCols) );
									}
									$sql = " update server_email_templates set `$notifyWhat` = " .($rec['val'] ? 1 : 0) . " where id = " . (int)($rec['id']);
									$link->query($sql);
							}        
						}
					return 'success';	
				 }
				 return 'failed';
		}
		public function sendTestEmail(Application $app){
			if ($app['request']->get('cmd', '') === 'TestEmail') {
					$link = DbHelper::getWrappedHandle($app['dbs']['main']);
					$roid = $app['range_owner_id'];
					$ro = $app['range_owner'];
					$accno = $roid->getAccountNo();	
					$hash = $roid->getAccountHashKey();
					
					$ro->initWithAccount($link,$hash,$accno);
					
					$tplTypeId =  $_POST['templateTypeId'];
					$email =  $_POST['email']; 
					$subject = $_POST['subject'];
					$body = $_POST['body'];
					$data_pre = $this->getVariablesForType($tplTypeId);
					
					$data = array();
						foreach($data_pre as $k=> $v) {
							$data[$k] = $v['example'];
						}
						$tpl_raw = array(
							'template_subject' => $subject,
							'template_body' => $body
						);
						 //$comp_data = $this->getTemplateVarValuesForRangeOwner();
						 $comp_data = array();
						$comp_data['[company-name]'] = $ro->getCompanyName();
						$comp_data['[company-phone]'] = $ro->getPhoneNumber();						
						$comp_data['[range-number]'] = $ro->getRangeNumber();
						$comp_data['[company-address]'] = implode('<br />', $ro->getCompanyAddress());
						$comp_data['[company-email]'] = $ro->getEmail();
						$data = array_merge($data, $comp_data);
						
						 $data['shooter_email'] = $email;
						if (in_array($tplTypeId , array(1,2,3) )) {
							//$this->sendTest($hash, $accno, $tplTypeId, $data, $tpl_raw);
							$tpl_filled = $this->getFilledTemplateForAccount($tpl_raw, $tplTypeId, $data);
								$to = $data['shooter_email'];
								// subject
								$subjects = $tpl_filled['template_subject'];
								// message
								$message = $tpl_filled['template_body'];
								// To send HTML mail, the Content-type header must be set
								$headers  = 'MIME-Version: 1.0' . "\r\n";
								$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
								$headers .= 'From: Range Controller<info@rangecontroller.com>' . "\r\n";
								mail($to, $subjects, $message, $headers);
							$result = 1;
						} else {
							$response['status'] = 'error';
							$response['message'] = 'Invalid mail type';
							print json_encode($response);
							exit();
						}
					return 'success';
				 }
				 return 'failed';
		}
		private function getFilledTemplateForAccount($tpl, $templateTypeId, $data)
			{
				$reqs = array('template_subject', 'template_body');
				if (!ArrayHelper::arrayContainsAllTheseKeys($tpl, $reqs)) {
					throw new Exception('$tplData argument must have values for keys: ' . 
							implode(',', $reqs) . '');
				}
				
				$subject = $tpl['template_subject'];
				$body = $tpl['template_body'];
				
				foreach($data as $placeholder => $value) {
					$placeholder = str_replace('[', '\[', $placeholder);
					$placeholder = str_replace(']', '\]', $placeholder);
					$placeholder = str_replace('-', '\-', $placeholder);
					$subject = preg_replace("/". $placeholder . '/im', $value, $subject);
					$body = preg_replace("/". $placeholder . '/im', $value, $body);
				}
			
				$pre = $this->getPrependTemplate($templateTypeId);
				$post = $this->getAppendTemplate($templateTypeId);
				return array(
					'template_subject' => $subject,
					'template_body' => $pre . 
						'<div style="width:685px;">' . $body . '</div>'.$post
				);
			}   
			public function getPrependTemplate($templateTypeId){
				$str = '';
				$dir = dirname(__FILE__) . '/mail-template-parts';
				if ($templateTypeId == 3) {
					$str = file_get_contents($dir . '/mem_renewal_pre.html');
				} else if ($templateTypeId == 2) {
					$str = file_get_contents($dir . '/scheduler_pre.html');
				}
				return $str;
			}
			private function getAppendTemplate($templateTypeId) {
				return ''; // stuff below appends logo and it is supposed to be
				 // customized per range owner.. skipping for now
				$str = '';
				$dir = dirname(__FILE__) . '/mail-template-parts';
				if ($templateTypeId == 3) {
					$str = file_get_contents($dir . '/mem_renewal_post.html');
				} else if ($templateTypeId == 2) {
					$str = file_get_contents($dir . '/scheduler_post.html');
				}
				return $str;
			}
		 private function getVariablesForType($typeId){
            $vars = $this->getTemplateVariables();
            $pick = null;
            if ($typeId == 3) {
                $pick = array(
                    '[company-name]', 
                    '[membership-expiration-date]', 
                    '[shooter-login-link]',
                    '[member-first-name]'
                );            
            } else if ($typeId == 2) {
                $pick = array(
                    '[company-name]', 
                    '[company-phone]', 
                    '[shooter-login-link]', 
                    //'[scheduled-time-slot]', 
                    '[scheduled-date]',
                    //'[scheduled-lane]',
                    '[scheduled-lanes-times]',
                    '[member-first-name]',
                    '[member-last-name]',
                    '[member-full-name]');
            } else if ($typeId == 1) {
                $pick = array(
                    '[member-full-name]',
                    '[member-first-name]',
                    '[member-last-name]',
                    '[range-number]',
                    '[membership-number]',
                    '[temporary-password]',
                    '[member-email]',
                    '[membership-type]',
                    '[company-name]',
                    '[company-phone]', 
                    '[company-email]',
                    '[shooter-login-link]',
                );
            }
            $result = array();
            if (!empty($pick)) {
                sort($pick);
                foreach($pick as $i => $v) {
                    $result[$v] = $vars[$v];
                }
            }
			
            return $result;
        }
		private function getTemplateVariables(){
            $vars = array(
            '[company-name]' => array(
                'desc' => 'Displays the name of your company', 
                'example' => 'Test Range Co.'),
            '[company-phone]' => array(
                'desc' => 'Displays the phone number of your company', 
                'example' => '(123)-456-7890'),
            '[shooter-login-link]' => array(
                'desc' => 'Displays the shooter login link', 
                'example' => 'http://www.rangecontroller.com'),
            '[range-number]' => array(
                'desc' => 'Displays your company\'s range number', 
                'example' => '10000'),
            '[company-address]' => array(
                'desc' => 'Displays the address of your company', 
                'example' => '101 Main Street <br />Nashville, TN 12345'),
            '[company-email]' => array(
                'desc' => 'Displays your company\'s email address', 
                'example' => 'hello@testRangeCo.com'),
            '[scheduled-lanes-times]' => array(
                'desc' => 'Displays the schedules lanes and times for each lane',
                'example' => 'Lane 1: 7:30, 8:00, 9:00<br />Lane 2: 8:30, 9:00'
                ),
            '[scheduled-date]' => array(
                'desc' => 'Displays the date of the scheduled shooting', 
                'example' => 'Tuesday Nov. 4, 2014'),
            '[membership-expiration-date]' => array(
                'desc' => 'Displays the membership expiration date of '
            . 'the receiver of this email', 
                'example' => '8/5/2014'),
            '[membership-type]' => array(
                'desc' => 'Displays the type of membership such as Bronze, Silver, etc', 
                'example' => 'Bronze'),
            '[member-first-name]' => array(
                'desc' => 'Displays the FIRST name of the receiver of this email such as John', 
                'example' => 'John'),
            '[member-last-name]' => array(
                'desc' => 'Displays the LAST name of the receiver of this email such as Doe', 
                'example' => 'Doe'),
            '[member-full-name]' => array(
                'desc' => 'Displays the full name of the receiver of this email such as John Doe', 
                'example' => 'John Doe'),        
            '[membership-number]' => array(
                'desc' => 'Displays the membership number of the receiver of this email', 
                'example' => '123'),
            '[temporary-password]' => array(
                'desc' => 'Displays the temporary password of the receiver of this email', 
                'example' => 'agWetgy'),
            '[member-email]' => array(
                'desc' => 'Displays the email address of the receiver of this email', 
                'example' => 'john@doe.com')

            );
            ksort($vars);
            return $vars;
        }
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'),
                array('link' => $app['get_range_url']('serverEmailConfig'), 'title'=>' Module Configurations'),
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            
            $breadcrumbsContent = $app['templating']->render(
                    'range/serveremailconfig/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            );
            return $app['templating']->render('range/layout-range.php', $vars);
        }    
    }
}
