<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class NewsletterController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
            return $this->listMembers($app);
        }
				
		public function listMembers(Application $app){
            
            
			$newsletter_subcriber = $app['newsletter_subcriber']; // \Rc\Models\NewsletterSubcriber::initWithAccount($link, $accHashKey, $accNo);//
            
            $pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
            //filter
				$emp=$app['request']->get('filter_employee');
				$emp=(!empty($emp)) ? $emp : false;
				$dt=$app['request']->get('filter_date');
				$dt=(!empty($dt)) ? $dt : false;
				$fm=$app['request']->get('filter_month');
				$fm=(!empty($fm)) ? $fm : false;
				$fy=$app['request']->get('filter_year');
				$fy=(!empty($ft)) ? $ft : false;
			//
            $records= $newsletter_subcriber->getNewsletters(array('employee_id'=>$emp,'timestamp'=>$dt,'month'=>$fm,'year'=>$fy), $pager);
            $getemplists = $newsletter_subcriber->getemplist();			
			$data = array(
				'allnewsletter' =>  $records,                 
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'getemplists'=>$getemplists,
				'emp'=>$emp,
				'dt'=>$dt,
				'fm'=>$fm,
				'fy'=>$fy,
            );            
            
            $content = $app['templating']->render('range/newsletter/list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Newsletter List';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
        
        public function getfilterDataJson(Application $app){        	
			
			//filter
				$emp=$app['request']->get('filter_employee');
				$emp=(!empty($emp)) ? $emp : false;
				$dt=$app['request']->get('filter_date');				
				$ndt=(!empty($dt)) ? date( 'Y-m-d', strtotime($dt)) : false;
				$fm=$app['request']->get('filter_month');
				$fm=(!empty($fm)) ? $fm : false;
				$fy=$app['request']->get('filter_year');
				$fy=(!empty($fy)) ? $fy : false;
				$data =	array(
						'employee_id'=>$emp,
						'timestamp'=>$ndt,
						'month'=>$fm,
						'year'=>$fy
					);				
			//			
			$records =  $app['newsletter_subcriber']->getNewslettersjson($data);			
        }
        				        
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('dashboard'), 'title'=>'Home'),
                array('link' => $app['get_range_url']('newsletters'), 'title'=>'Newsletters')
            );
        }
		
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            
            $breadcrumbsContent = $app['templating']->render(
                    'range/newsletter/breadcrumbs.php',
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            );
            return $app['templating']->render('range/layout-range.php', $vars);
        } 
		
		public function addNewsletter(Application $app) {
            $contactFields = array('firstName', 'middleName', 'lastName', 
                'street', 'city', 'state', 'zipcode',
                'ssn', 'phone1', 'phone2', 'email');
            //$contactData = $this->getPostForFields($contactFields, $app['request']); 
            $contactData['stateList'] = Services\CommonData::getStates();
                       
            $emergencyFields = array('emergencyName', 'emergencyPhone');
            //$emergencyData = $this->getPostForFields($emergencyFields, $app['request']);
            
            $membershipFields = array('membershipType', 'memRegistrationDate', 'memExpirationDate', 'employee_id');
            //$membershipData = $this->getPostForFields(
                    //$membershipFields, $app['request']);
            $membershipData['addMemberTypeUrl'] = $app['get_range_url']('common/config.php#createnow');
            $connhandle = Services\DbHelper::getWrappedHandle($app['dbs']['main']);
            $memtypes = Models\MembershipType::getMembershipTypesForAccount(
                    $connhandle , $app['range_owner_id']);
            $membershipData['membershipTypes'] = $memtypes;
            
            $errorMessage = '';
            
			$pageTokenIdKey = 'addMemberPageToken';
            
			//$emp_id = $membershipData['employee_id'];
			
            if ($app['request']->get('cmd', '') === 'add') {
                /*if ($app['session']->get($pageTokenIdKey) !== $app['request']->get($pageTokenIdKey)) {
                    return $app->redirect($app['get_range_url']('newsletter/addnew?rand='.strtotime('now')));
                }*/
                //$data = array_merge($contactData, $emergencyData, $membershipData);
				
				//$data['emp_id'] = $emp_id;
				
                //$errorMessage = $this->getAddMemberFormError($data, $app);
				$empname=$app['request']->get('employeename', '');
				$sub=$app['request']->get('subject', '');
				$msg=$app['request']->get('message_body', '');
				if(empty($empname)  || empty($sub) || empty($msg))
					$errorMessage="Please fill out all of the required fields marked with *";
                if (!$errorMessage) {				
						$link = DbHelper::getWrappedHandle($app['dbs']['main']);
						$newsletter_subcriber = $app['newsletter_subcriber'];
						$member_type = '';
						$everyone    = '';
						$memberkey   = '';
						$memberval   = '';
						$everyonekey = '';
						$everyoneval = '';
						$employee_id  = $_POST['employeename'];
						$member_type  = $_POST['member_type'];
						$everyone     = $_POST['everyone'];
						$timestamp    = date("y-m-d h:i:s");
						$receivers    = json_encode($_POST['receivers']);
						$subject      =  $_POST['subject'];
						$message_body =  $_POST['message_body'];
						
						$roid = $app['range_owner_id'];
						$accno = $roid->getAccountNo();	
						$hash = $roid->getAccountHashKey();
						
						if($member_type != ''){				
							$memberkey = 'membership_type,';
							$memberval = ", '$member_type'";				
						}
						if($everyone != ''){				
							$everyonekey = ', everyone';
							$everyoneval = ", '$everyone'";				
						}
						$sql_ins = "INSERT INTO new_newsletter(emp_id, timestamp, $memberkey subject, email, content $everyonekey , acctnumberhashed, accountnumber) VALUES ('$employee_id', '$timestamp' $memberval , '".$link->real_escape_string($subject)."', '$receivers', '".$link->real_escape_string($message_body)."' $everyoneval , '$hash', '$accno' )";						
					
						$link->query($sql_ins);
						
						$em = json_decode($receivers);				
						$newarray = implode(", ", $em);			
						
						//$qur = "SELECT  email FROM customers WHERE accountnumber='".$accno."' and membership_number IN ($newarray)";
						$emails=array();
						if($receivers!='null' && !empty($receivers))
						{
							$qur = "SELECT  email FROM customers WHERE accountnumber='".$accno."' and membership_number IN ($newarray)";						
							$emaillist = $link->query($qur);
							while($row=$emaillist->fetch_assoc()){					
								if(!empty($row['email']) || $row['email']!='n/a')
									$emails[]=$row['email'];
							}
						}
						
						if(!empty($member_type)){			
							$qur = "SELECT email FROM customers,memberships WHERE customers.accountnumber='".$accno."' and memberships.membership_number=customers.membership_number and memberships.accountnumber=customers.accountnumber and customers.accountnumber=$accno and memberships.membership_type='$member_type'";
							$emaillist = $link->query($qur);
							while($row=$emaillist->fetch_assoc()){					
								if(!in_array($row['email'],$emails))
								{
									if(!empty($row['email']) || $row['email']!='n/a')
										$emails[]=$row['email'];
								}
							}
						}
						if(!empty($everyone)){
							$qur = "SELECT email FROM customers,memberships WHERE customers.accountnumber='".$accno."' and memberships.membership_number=customers.membership_number and memberships.accountnumber=customers.accountnumber and customers.accountnumber=$accno";
							$emaillist = $link->query($qur);
							while($row=$emaillist->fetch_assoc()){					
								if(!in_array($row['email'],$emails))
								{
									if(!empty($row['email']) || $row['email']!='n/a')
										$emails[]=$row['email'];
								}
							}
						}
						//$emails=array('Stratmansblues@gmail.com','keyur@yopmail.com');		//uncomment to make live
						//$getemails = implode(",", $emails);
					// multiple recipients
					foreach($emails as $to){
						//$to=$getemails;
						// subject
						$subjects = $subject;			
						// message
						$message = "
						<html>
						<head>
						  <title>$subject</title>
						</head>
						<body>
						  <p>$message_body</p>			  
						</body>
						</html>
						";			
						// To send HTML mail, the Content-type header must be set
						$headers  = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						
						$headers .= 'From: RangeController <noreply@rangecontroller.com>' . "\r\n";			
						// Mail it
						mail($to, $subjects, $message, $headers);
					}
						return $app->redirect($app['get_range_url']('newsletters'));
                }
               
            } else {
                $app['session']->set($pageTokenIdKey, uniqid("addMember", true));
                $membershipData['memRegistrationDate'] = date('n/j/Y');
            }
            
            $newsletter_subcriber = $app['newsletter_subcriber']; // \Rc\Models\NewsletterSubcriber::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            $mrecords = $newsletter_subcriber->getmemberslist();
            $getemplists = $newsletter_subcriber->getemplist();
            
			$data = array(
                'errorMessage' => $errorMessage,
                'addMemberPostUrl' => $app['get_range_url']('customers/1234'),
                'membershipTypeData' =>  $membershipData,
                'getemplists' => $getemplists,
                'contactData' => $contactData,
                'mrecordstype' => $mrecords,
                'tokenValue' => $app['session']->get($pageTokenIdKey),
                'tokenName' => $pageTokenIdKey
            );
            $content = $app['templating']->render('range/newsletter/addnew.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[] = array('title' => 'Create' );
			$pageTitle = 'Range Controller - Newsletter - Create ';
            return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }  
		
		public function getGroupMembersJson(Application $app){
			$pos = array();
			$pos = $_POST;
			$pagedResults =  $app['newsletter_subcriber']->getgrouomemberslist($pos);
			//return $app->json($pagedResults->toArray());
        }
		
		
		public function editNewsletter(Application $app, $newsletterNo){
			//$gdata = array();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$membershiptype = "SELECT * FROM new_newsletter WHERE newsletter_id = $newsletterNo";
			$memtype_list = $link->query($membershiptype);
			//return DbHelper::dbFetchRowAssoc($this->link, $sql);
			$newsletter_subcriber = $app['newsletter_subcriber']; // \Rc\Models\NewsletterSubcriber::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            //$gdata = $memtype_list->fetch_assoc();
			$mrecords = $newsletter_subcriber->getmemberslist();
            $maillist = $newsletter_subcriber->getemaillist($newsletterNo);
			$getemplists = $newsletter_subcriber->getemplist();
			
			 if ($app['request']->get('cmd', '') === 'edit') {
				 $link = DbHelper::getWrappedHandle($app['dbs']['main']);
					$member_type = '';
					$member_type = '';
					$everyone    = '';
					$memberkeyval   = '';			
					$everyonekeyval = '';
								
					$employee_id   = $_POST['employeename'];
					$member_type   = $_POST['member_type'];
					$newsletter_id = $_POST['newsletter_id'];
					$everyone      = $_POST['everyone'];						
					$timestamp     = date("y-m-d h:i:s");
					$receivers     = json_encode($_POST['receivers']);
					$subject       =  $_POST['subject'];
					$message_body  =  $_POST['message_body'];
					$roid = $app['range_owner_id'];
					$accno = $roid->getAccountNo();	
					$hash = $roid->getAccountHashKey();
					
					if($member_type != ''){				
						$memberkeyval = ", membership_type  = '$member_type'";								
					}
					if($everyone != ''){				
						$everyonekeyval = ", everyone = '$everyone'";				
					}
								
					$sql_ins = "UPDATE new_newsletter SET emp_id = $employee_id, timestamp = '$timestamp' $memberkeyval, subject = '".$link->real_escape_string($subject)."', email = '$receivers', content = '".$link->real_escape_string($message_body)."' $everyonekeyval WHERE newsletter_id = $newsletter_id";  			
					$link->query($sql_ins);
				
					$em = json_decode($receivers);				
					$newarray = implode(", ", $em);		

					$emails=array();
					if($receivers!='null' && !empty($receivers))
					{
						$qur = "SELECT  email FROM customers WHERE accountnumber='".$accno."' and membership_number IN ($newarray)";						
						$emaillist = $link->query($qur);
						while($row=$emaillist->fetch_assoc()){					
							if(!empty($row['email']) || $row['email']!='n/a')
								$emails[]=$row['email'];
						}
					}
						if(!empty($member_type)){			
							$qur = "SELECT email FROM customers,memberships WHERE customers.accountnumber='".$accno."' and memberships.membership_number=customers.membership_number and memberships.accountnumber=customers.accountnumber and customers.accountnumber=$accno and memberships.membership_type='$member_type'";
							$emaillist = $link->query($qur);
							while($row=$emaillist->fetch_assoc()){					
								if(!in_array($row['email'],$emails))
								{
									if(!empty($row['email']) || $row['email']!='n/a')
										$emails[]=$row['email'];
								}
							}
						}
						if(!empty($everyone)){
								$qur = "SELECT email FROM customers,memberships WHERE customers.accountnumber='".$accno."' and memberships.membership_number=customers.membership_number and memberships.accountnumber=customers.accountnumber and customers.accountnumber=$accno";
							$emaillist = $link->query($qur);
							while($row=$emaillist->fetch_assoc()){					
								if(!in_array($row['email'],$emails))
								{
									if(!empty($row['email']) || $row['email']!='n/a')
										$emails[]=$row['email'];
								}
							}
						}
						//$emails=array('Stratmansblues@gmail.com','keyur.edit@yopmail.com');		//uncomment to make live 
						
						$getemails = implode(",", $emails);
					// multiple recipients
						foreach($emails as $to){
							//$to=$getemails;
							// subject
							$subjects = $subject;			
							// message
							$message = "
							<html>
							<head>
							  <title>$subject</title>
							</head>
							<body>
							  <p>$message_body</p>			  
							</body>
							</html>
							";			
							// To send HTML mail, the Content-type header must be set
							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
							
							$headers .= 'From: RangeController <noreply@rangecontroller.com>' . "\r\n";			
							// Mail it
							if(!mail($to, $subjects, $message, $headers)){
								//echo "<br/>Error on sending : $to";
							}else{
								//echo "<br/>Sent To : $to";
							}
							
							
					}
					//done with email 
					return $app->redirect($app['get_range_url']('newsletters'));
			 }
			
			$data = array(                    
                'memberAddUrl' => $app['get_range_url']('newsletters/addnew'),
                'member' => $memtype_list,
                'getemplists' => $getemplists,                
                'mrecordstype' => $mrecords,
                'allmaillist' => $maillist                
            );
			
            $content = $app['templating']->render('range/newsletter/edit.php', $data);
			
			
			$breadcrumbs = $this->getBreadcrumbs($app);
            $title = 'Resend';
            $breadcrumbs[] = array('title' => $title);
            $pageTitle = 'Range Controller - Newsletter - ' . $title;
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));			
				
		}
		
		public function editNewNewsletter(Application $app){			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$member_type = '';
			$member_type = '';
			$everyone    = '';
			$memberkeyval   = '';			
			$everyonekeyval = '';
						
			$employee_id   = $_POST['employeename'];
			$member_type   = $_POST['member_type'];
			$newsletter_id = $_POST['newsletter_id'];
			$everyone      = $_POST['everyone'];						
       		$timestamp     = date("y-m-d h:i:s");
			$receivers     = json_encode($_POST['receivers']);
			$subject       = $_POST['subject'];
			$message_body  = $_POST['message_body'];
									
			if($member_type != ''){				
				$memberkeyval = ", membership_type  = '$member_type'";								
			}
			if($everyone != ''){				
				$everyonekeyval = ", everyone = '$everyone'";				
			}
						
			$sql_ins = "UPDATE new_newsletter SET emp_id = $employee_id, timestamp = '$timestamp' $memberkeyval, subject = '$subject', email = '$receivers', content = '$message_body' $everyonekeyval WHERE newsletter_id = $newsletter_id";  			
   			$link->query($sql_ins);
			   
			$em = json_decode($receivers);				
			$newarray = implode(", ", $em);			
			$qur = "SELECT distinct(CONCAT_WS(first_name,middle_name,last_name)) as membername, membership_number, email FROM customers WHERE membership_number IN ($newarray)";
			$emaillist = $link->query($qur);
			$i=0;
			while($row=$emaillist->fetch_assoc()){					
					$result[$i]['email']=$row['email'];
					$i++;
			}
			$emails = array();			
			foreach($result as $nam){
				$emails[] = $nam['email'];	
			}
			$getemails = implode(";", $emails);
			// multiple recipients
			$to  = 'tushar4monto@gmail.com' . ', '; // note the comma
			$to .= 'keyur@yopmail.com';			
			// subject
			$subjects = 'Test mail For Range Controller';			
			// message
			$message = "
			<html>
			<head>
			  <title>Test mail For Range Controller</title>
			</head>
			<body>
			  <p>$message_body</p>			  
			</body>
			</html>
			";			
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			
			$headers .= 'From: Tushr <Tushar4monto@gmail.com>' . "\r\n";			
			// Mail it
			//mail($to, $subjects, $message, $headers);
						   
   			return $app->redirect($app['get_range_url']('newsletters'));				
		}
		
		public function deleteNewsletter (Application $app, $newsletterNo){			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql_ins = "DELETE FROM `new_newsletter` WHERE newsletter_id = $newsletterNo";			  			
   			$link->query($sql_ins);
   			return $app->redirect($app['get_range_url']('newsletters'));				
		}     
        
    }
}
