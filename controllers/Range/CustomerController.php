<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    //include('//includes/browser.php');
    class CustomerController {
        protected $layoutPath;
        protected $pageSize;
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        public function index(Application $app){
     
            return $this->listMembers($app);
        }		
		
		public function updateImage(Application $app, $ext){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qry = "UPDATE controller_config SET company_logo='logo." . $ext . "' WHERE accountnumber='" . $accno . "'";
			$link->query($qry);
			}
		public function getEmployeeList (Application $app) {
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qry = "SELECT * FROM employees WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $qry);
			$selects = '<select onChange="addEmployee(this.value)" name="employee_id" id="employee_id" style="float:right;"><option value="">Choose an employee : </option>';
			foreach ($assoc_array as $employee_record) {
				$selects = $selects . '<option value="' . $employee_record['id'] . '">' . $employee_record['fname'] . ' ' . $employee_record['lname'] . '</option>';
			}
			$selects = $selects . '<option value="-1">Or Add An Employee...</option></select><span style="float:right;" class="requiredf">* </span>';
			return $selects;
		}
		public function setEmployeeId (Application $app, $section, $emp_id) {
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qry = "SELECT * FROM employees WHERE id=" . $emp_id;
			$results = DbHelper::dbFetchRowAssoc($link, $qry);
			//$emp_name = $results['fname'] . " " . $results['lname'];
			$emp_name = $results['initials']; 
			if ($section == 'billing') {
				$sqll = "SELECT * FROM billing_info ORDER BY id DESC LIMIT 1";
				$results = DbHelper::dbFetchRowAssoc($link, $sqll);
				$highest_id = $results['id'];
				$qry = "UPDATE billing_info SET employee_id=" . $emp_id . ", employee_name='" . $emp_name . "' WHERE id = " . $highest_id;
				$link->query($qry);
			}
		}
		
        
		public function listEmployees(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM employees WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);
			$count = 0;
			foreach ($assoc_array as $employeeRow) {
				$count++;
				$employeeRows = $employeeRows . '<tr><td>' . $count . '</td>
									<td>' . $employeeRow['fname'] . ' ' . $employeeRow['lname'] . '</td>
									<td>' . $employeeRow['initials'] . '</td>
									<td><a href="/xapp/config/employees/manage/' . $employeeRow['id'] . '">Manage</a></td>
									<td><a href="/xapp/config/employees/delete/' . $employeeRow['id'] . '" onclick="return myConfirm(\'Are you sure you want to delete this employee?\', ' . $employeeRow['id'] . ')">Delete</a></td>
								</tr>';	
			}
            $data = [
				'employeeRows' => $employeeRows,
            ];            
            $content = $app['templating']->render('range/config/employees.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Employees']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Employees';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
        
		
		public function resendSignup(Application $app){
			$membership_id=$app['request']->get('member_id','');
			if(!empty($membership_id)){
				$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
				$roid = $app['range_owner_id'];
				$accno = $roid->getAccountNo();	
				$hash = $roid->getAccountHashKey();
				//$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				
				$this->sendMemberSignUpMail($membership_id, $app['range_owner'], Services\DbHelper::getWrappedHandle($app['dbs']['main']));
			}
			die;
			/*$msql = "DELETE FROM editable_memberships WHERE membership_type_id=" . $membership_id;
			$link->query($msql);
			return '<script>window.location = "https://rangecontroller.com/xapp/config/memberships"</script>';*/
        }
        public function listMembers(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
            $records= $range_owner->getMembers([], $pager);
			$membership_type= $range_owner->membership_type(); 
            $data = [
                'lastManagedMember' => $this->getLastManagedMember($app),
				'getfilterTypes' => $this->getfilterTypes($app),
                'totalMembers' => $records->getTotalRecords(),
                'members' =>  $records->getRecords(),
				'membership_type' => $membership_type,
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'scheduleUrlPrefix' => $this->getScheduleUrlPrefix($app),
                'loginRangeUrlPrefix' => $this->getLoginRangeUrlPrefix($app),
                'logoutRangeUrlPrefix' => $this->getLogoutRangeUrlPrefix($app)
            ];            
            $content = $app['templating']->render('range/customer/list.php', $data)
                    ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Member List';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function getfilterTypes(Application $app){
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT filtertypes FROM admin_login WHERE accountnumber='" . $accno . "'";
			$fetch_filtertypes = DbHelper::dbFetchRowAssoc($link, $sql);
			return $fetch_filtertypes;
        }
		
		function sendEmail(Application $app){//$email
			//return "NOTHING?";
			$email = $app['request']->get('email', '');
			$name = $app['request']->get('name', '');
			$comment = $app['request']->get('comment', '');
			$the_browser = $app['request']->get('browser', '');
			$the_version = $app['request']->get('version', '');
			$the_platform = $app['request']->get('platform', '');
			$the_useragent = $app['request']->get('useragent', '');
			//$b = new Browser();
			$subject = $_SESSION['company_name']." - TECH SUPPORT";
			//return "NOTHING?" . $_SESSION['company_name'];
			$message = 
			'
			<table>
			<tr><td>Name of Sender</td><td>'.$name.'</td></tr>
			<tr><td>Customer Number</td><td>'.$_SESSION['account_number'].'</td></tr>
			<tr><td>Company Name</td><td>'.$_SESSION['company_name'].'</td></tr>
			<tr><td>Customer Email</td><td>'.$email.'</td></tr>
			<tr><td>Timestamp</td><td>'.date("D M j Y g:i:s").'</td></tr>
			</table>
			<br />
			We have received your request and can respond within 3-5 business days with the solution.
			Keep this email for your records.
			<br /><br />
			We hope to resolve the issue asap, Thank You.
			<br /><br />
			<b>SYSTEM SPECS DETECTED:</b><br />
			<table>
			<tr><td width="20%">Browser &amp; Version</td><td>'.$the_browser.' '.$the_version.'</td></tr>
			<tr><td>Platform</td><td>'.$the_platform.'</td></tr>
			<tr><td>Other Browsers</td><td>'.$the_useragent.'</td></tr>
			</table>
			<br /><br />
			<b>START ISSUE:</b><br />
			'.$comment.'
			<br /><b>END ISSUE:</b>
			<br /><br />
			';
			$email_from = "noreply@davespawnshop.com";
			// create email headers
			$headers = 'From: '.$email_from."\r\n".
			'Reply-To: '.$email_from."\r\n" .
			'Content-Type: text/html; charset=ISO-8859-1\r\n';
			mail($email, $subject, $message, $headers);//TO MEMBER FOR HIS RECORDS
			//mail("swat@davespawnshop.com", $subject, $message, $headers);//TO TECH SUPPORT
			mail("stratmansblues@gmail.com", $subject, $message, $headers);//TO TECH SUPPORT
			mail("nick22891@yahoo.com", $subject, $message, $headers);//TO TECH SUPPORT
			//mail(PRIMARY_EMAIL_FROM, $subject, $message, $headers);//TO TERRY
			return "DONE";
		}//END FUNCTION
                
		function techSupport (Application $app) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);            
            $data = [
            ];
            $content = $app['templating']->render('range/customer/techsupport.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
			$breadcrumbs[1]['title'] = 'Tech Support';
			//$breadcrumbs[2]['title'] = 'Range Usage Report (Graph)';
            $pageTitle = 'Range Controller - Tech Support';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
        
        
        
		function sendRenewalMail(Application $app, $mem_no) {
  	 	 //print 'send mail called'; 
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
   		    $range_owner = range_owner::initWithAccount($link, $hash, $accno);
    		$range_member = range_member::initForMember(
                    $link, $hash, $accno, $mem_no);
    		//print_r($range_owner);
    		//print_r($range_member); 
    		if (!$range_owner || !$range_member) {
     		   return "FAILED HERE";
    		}
    		$data = array_merge(
    	        server_email_template_types::getTemplateVarValuesForSite(), server_email_template_types::getTemplateVarValuesForRangeOwner($range_owner), server_email_template_types::getTemplateVarValuesForMember($range_member)
    		);
            $data['shooter_email'] = $range_member->getEmail();
    		//print_r($data);
    		$tplTypeId = server_email_template_types::MEMBERSHIP_RENEWAL_ID;
    		$set = new server_email_templates($link);
    		$res = $set->sendTemplateMailForAccount(
            $hash, $accno, $tplTypeId, $data);
    		//print 'send result' . $res;
    		$range_member->updateOrInsertMembershipRenewalDate(new \DateTime); 
    		return $res;
		}
		
		function membershipRenewalPost (Application $app) {
		$month = $app['request']->get('month', 'none');
		$year = $app['request']->get('year', 'none');
			switch ($month) {
				case 'January' :
					$maxday = 31;
					$month_no = 1;
					break;
				case 'February' :
					$maxday = 28;
					$month_no = 2;
					break;
				case 'March' :
					$maxday = 31;
					$month_no = 3;
					break;
				case 'April' :
					$maxday = 30;
					$month_no = 4;
					break;
				case 'May' :
					$maxday = 31;
					$month_no = 5;
					break;
				case 'June' :
					$maxday = 30;
					$month_no = 6;
					break;
				case 'July' :
					$maxday = 31;
					$month_no = 7;
					break;
				case 'August' :
					$maxday = 31;
					$month_no = 8;
					break;
				case 'September' :
					$maxday = 30;
					$month_no = 9;
					break;
				case 'October' :
					$maxday = 31;
					$month_no = 10;
					break;
				case 'November' :
					$maxday = 30;
					$month_no = 11;
					break;
				case 'December' :
					$maxday = 31;
					$month_no = 12;
					break;
			}
			$from = $month_no . "/1/" . $year;
			$to = $month_no . "/" . $maxday . "/" . $year;
			$type = $app['request']->get('type', 'none');
			//$from = $app['request']->get('from', 'none');
			//$to = $app['request']->get('to', 'none');
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT * FROM customers WHERE accountnumber='" . $accno . "' ORDER BY membership_number ASC";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = '';
			$counter = 0;
			$append = '';
			if ($type != 'none') $append = $append . " AND membership_type='" . $type . "'";
			if ($from != 'none' && $to != 'none') $append = $append . " AND STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' )" . " AND STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			foreach ($assoc_array as $row) {
				$name = $row['first_name'] . ' ' . $row['last_name'];
				$mem_no = $row['membership_number'];
				$visits = $row['visitcount'];
				$phone_no = $row['home_phone'];
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'" . $append;
				$member_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$reg_date = $member_row['membership_date'];
				$exp_date = $member_row['membership_expires'];
				$mem_type = $member_row['membership_type'];
				$myArr = explode("/", $exp_date);
				$sortable_exp = $myArr[2] . $myArr[0] . $myArr[1];
				$myArr = explode("/", $reg_date);
				$sortable_reg = $myArr[2] . $myArr[0] . $myArr[1];
				/*$sql = "SELECT COUNT(*) AS mycount FROM `range_time_tracker` WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'";
				$result = DbHelper::dbFetchRowAssoc($link, $sql);
				$visits = (int) $result['mycount'];*/
					if (!empty($member_row)) {
						$counter++;
                		$sql = "SELECT * FROM membership_renewal WHERE accountnumber='" . $accno . "' AND membership_number='". $mem_no . "'";// AND expiration_date='" . $exp_date . "'";
                		$result2 = DbHelper::dbFetchRowAssoc($link, $sql);
                		$send_resend = 'Send Email';
                		$sent_date = '';
                		if (!empty($result2)) {
                		$send_resend = "Resend Email";
                		$sent_date = $result2['date_sent'];
                		}
                		$rows = $rows . '<tr>
                  <td>' . $counter . '</td>
                  <td>' . $mem_type . '</td>
                  <td nowrap=""><a href="/xapp/customers/' . $mem_no . '">' . $name . '</a></td>
                  <td>' . $mem_no . '</td>
                  <td><span style="color: #fbb91f;">' . $visits . '</span></td>
                  <td sorttable_customkey="' . $sortable_exp . '">' . $exp_date . '</td>
                  <td><a href="#none" onclick="sendRenewalMail(' . $mem_no . ", '" . $name . "')" . '">' . $send_resend . '</td>
                  <td>' . $sent_date . '</td>
                </tr>';
                	}
			}
            $data = [
                'rows' => $rows,
                'month' => $month,
                'year' => $year,
            ];
            $content = $app['templating']->render('range/reports/membership_renewal.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Membership Renewal';
            $pageTitle = 'Range Controller - Reports - Membership Renewal';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		
		function numOfVisits(Application $app,$membership_number) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$visits_sql = "";
			$visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $membership_number . "' AND acctnumberhashed = '" . $hash . "' AND accountnumber = '" .$accno . "' ";
			if ($result = $link->query($visits_sql)) {
				$count_visits = 0;
				while ($row = $result->fetch_assoc()) {
					$count_visits++;
				}//END WHILE LOOP  
			}//END IF
			return $count_visits;
		}
		
        function sendMemberSignUpMail($membershipNo, range_owner $range_owner, \mysqli $link){
            $accHashKey = $range_owner->getAccountHashKey(); //SessionModel::getAccHashKey($app);
            $accNo = $range_owner->getAccountNumber(); //SessionModel::getAccNo($app);
            $range_member = range_member::initForMember(
                    $link, $accHashKey, $accNo, $membershipNo);
            //print_r($range_owner);
            //print_r($range_member); 
            if (!$range_owner || !$range_member) {
                return false;
            }
            $data = array_merge(
                server_email_template_types::getTemplateVarValuesForSite(),
                server_email_template_types::getTemplateVarValuesForRangeOwner($range_owner),
                server_email_template_types::getTemplateVarValuesForMember($range_member)
            );
            $data['shooter_email'] = $range_member->getEmail();
            //print_r($data);
            $tplTypeId = server_email_template_types::MEMBERSHIP_SIGN_UP_ID;
            $set = new server_email_templates($link);
            $res =  $set->sendTemplateMailForAccount(
                    $accHashKey, $accNo, $tplTypeId, $data);      
            //print 'send result' . $res;
            return $res;
        }
        private function getPostForFields($fields, Request $req){
            $results = [];
            foreach($fields as $f) {
                $results[$f] = trim($req->get($f, ''));
            }
            return $results;
        }
        private function getAddMemberFormError($data, Application $app){
            $required = ['firstName', 'lastName', 
                'street', 'city', 'state', 'zipcode',
                'phone1', 'email', 'membershipType'];
            foreach($required as $f) {
                if (empty($data[$f])) {
                    return 'Please fill out all of the required fields marked with *';
                }
            }
            $valida =$app['validator'];
            $emailErrors = $valida->validateValue($data['email'], new Assert\Email());
            if (count($emailErrors)) {
                return 'Email address is invalid';
            }            
            if (!Services\StringHelper::isPhoneValid($data['phone1'])) {
                return ' Phone 1# is an invalid number, please re-enter in '
                . '###-###-#### format';
            }
            if (!empty($data['phone2']) 
                    && !Services\StringHelper::isPhoneValid($data['phone2'])) {
                return ' Phone 2# is an invalid number, please re-enter in '
                . '###-###-#### format';
            }
            if (!empty($data['emergencyPhone']) && 
                    !Services\StringHelper::isPhoneValid($data['emergencyPhone'])) {
                return ' Emergency phone is an invalid number, please re-enter in '
                . '###-###-#### format';
            }
            $regDate = $data['memRegistrationDate'];
            $expDate = $data['memExpirationDate'];
            if (!empty($regDate) && !DateTimeHelper::isDateFormattedAsMdy($regDate)) {
                return 'Invalid membership registration date';
            }
            if (!empty($expDate) && !DateTimeHelper::isDateFormattedAsMdy($expDate)) {
                return 'Invalid membership expiration date';
            }
            if (!empty($regDate) && !empty($expDate)) {
                $reg = DateTimeHelper::mdyToDateTime($data['memRegistrationDate']);
                $exp = DateTimeHelper::mdyToDateTime($data['memExpirationDate']);
                $today = new \DateTime(date('Y-m-d'));
                if ($exp < $reg) {
                    return 'Membership expiration date cannot be less than '
                    . 'registration date';
                }
                if ($exp < $today) {
                    return 'Membership expiration date cannot be less than '
                    . 'today\'s date';
                }
            }
            return false; 
        }
        public function addMember(Application $app) {
        	error_reporting(E_ALL);
          ini_set('display_errors', 1);
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$email_template_sql = "select notify_shooter from server_email_templates where acctnumberhashed = '".$link->real_escape_string($hash) . "' and accountnumber = '".$link->real_escape_string($accno)."' and se_templatetype_fk = 1 ";
			$server_email_check = DbHelper::dbFetchRowAssoc($link, $email_template_sql);

            $contactFields = ['firstName', 'middleName', 'lastName', 
                'street', 'city', 'state', 'zipcode',
                'ssn', 'phone1', 'phone2', 'email', 'email_status'];
            $contactData = $this->getPostForFields($contactFields, $app['request']); 
            $contactData['stateList'] = Services\CommonData::getStates();
            $emergencyFields = ['emergencyName', 'emergencyPhone'];
            $emergencyData = $this->getPostForFields($emergencyFields, $app['request']);
            $membershipFields = ['membershipType', 'memRegistrationDate', 
                'memExpirationDate', 'employee_id'];
            $membershipData = $this->getPostForFields(
                    $membershipFields, $app['request']);
            $membershipData['addMemberTypeUrl'] = $app['get_range_url'](
                        'common/config.php#createnow');
            $connhandle = Services\DbHelper::getWrappedHandle($app['dbs']['main']);
            $memtypes = Models\MembershipType::getMembershipTypesForAccount(
                    $connhandle , $app['range_owner_id']);
            $membershipData['membershipTypes'] = $memtypes;
            $errorMessage = '';
			$pageTokenIdKey = 'addMemberPageToken';
			$emp_id = $membershipData['employee_id'];
            if ($app['request']->get('cmd', '') === 'add') {
                if ($app['session']->get($pageTokenIdKey) !== 
                    $app['request']->get($pageTokenIdKey)) {
                    return $app->redirect($app['get_range_url']('customers/add?rand='.strtotime('now')));
                }
                $data = array_merge($contactData, $emergencyData, $membershipData);
				$data['emp_id'] = $emp_id;
                $errorMessage = $this->getAddMemberFormError($data, $app);
                if (!$errorMessage) {
                    $uniqudata = [
                        'first_name' => $contactData['firstName'],
                        'last_name' => $contactData['lastName'],
                        'middle_name' => $contactData['middleName'],
                        'home_phone' => $contactData['phone1'],
                        'email' => $contactData['email']
                    ];
                    $exists = false;
                    try {
                        $exists = range_member::doesMemberExist($uniqudata, 
                                $app['range_owner_id'],$app['dbs']['main']);
                    } catch (Exception $ex) {
                        $errorMessage = 'Error occured';
                    }
                    if ($exists) {
                        $ufields = array_values(
                                range_member::memberUniqueFields());
                        $errorMessage = "A member with same " . 
                                implode(', ', $ufields) . 
                                " already exists for this account. "
                                . " If this is really a different person, "
                                . "try to put/change middle name to "
                                . "make it unqiue";
                    }
                }
				
                if (!$errorMessage ) {
                    $memNo = range_member::createNewMember($data, 
                            $app['dbs']['main'] , $app['range_owner']);
                    if ($memNo){
                        $app['session']->set($pageTokenIdKey, '');
						if($data['email_status'] == "subscribe" && $server_email_check['notify_shooter'] == 1)
						{
							$this->sendMemberSignUpMail($memNo, $app['range_owner'], Services\DbHelper::getWrappedHandle($app['dbs']['main']));
						}
                        return $app->redirect($app['get_range_url']('customers/'. $memNo));
                    }
                    $errorMessage ='Error occured while saving the member info. '
                                . 'Please try again ' . $memNo;
                }
            } else {
                $app['session']->set($pageTokenIdKey, uniqid("addMember", true));
                $membershipData['memRegistrationDate'] = date('n/j/Y');
            }
            $data = [
                'errorMessage' => $errorMessage,
                'addMemberPostUrl' => $app['get_range_url']('customers/1234'),
                'membershipTypeData' =>  $membershipData,
                'contactData' => $contactData,
                'emergencyData' => $emergencyData,
                'tokenValue' => $app['session']->get($pageTokenIdKey),
                'tokenName' => $pageTokenIdKey
            ];
            $content = $app['templating']->render('range/customer/add.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$title = 'Add New Member';
            $breadcrumbs[] = ['title' => 'Add New Member ' ];
			$pageTitle = 'Range Controller - Members - ' . $title;
            return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }
        private function getFieldValues(range_member $range_member) {
            return [
                'firstName' => $range_member->getFirstName(),
                'middleName' => $range_member->getMiddleName(),
                'lastName' => $range_member->getLastName(),
                'street' => $range_member->getAddress()->street,
                'city' => $range_member->getAddress()->city,
                'state' => $range_member->getAddress()->state,
                'zipcode' => $range_member->getAddress()->zipcode,
                'ssn' => $range_member->getSsn(),
                'phone1' => $range_member->getPhone1(),
                'phone2'  => $range_member->getPhone2(),
                'email' => $range_member->getEmail(),
				'email_status' => $range_member->getEmailStatus(),
                'emergencyName' => $range_member->getEmergencyContactName(),
                'emergencyPhone' => $range_member->getEmergencyContactPhone(),
                'membershipType' => $range_member->getMembershipType(),
                'memRegistrationDate' => $range_member->getMembershipDate(),
                'memExpirationDate' => $range_member->getMembershipExpirationDate(),
                'genders' => CommonData::getGenders(),
                'races' => CommonData::getRaces(),
                'stateList' => CommonData::getStates(),
                'hairColors' => CommonData::getHairColors(),
                'eyeColors' => CommonData::getEyeColors(),
                'paymentTypes' => CommonData::getPaymentTypes(),
                'yesNo' => CommonData::getYesNo()                
            ];
        }
        public function setLastManagedMember(Application $app, $memNo){
            $app['session']->set('lastManagedMemberNo', $memNo);
        }
        public function getLastManagedMember(Application $app){
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $roid = $app['range_owner_id'];
            $memberNo = $app['session']->get('lastManagedMemberNo');
            if (!$memberNo) {
                return null;
            }
            $rm = range_member::initForMember($link, 
                    $roid->getAccountHashKey(), $roid->getAccountNo(),
                    $memberNo);
            return $rm;
        }
        public function getScheduleUrlPrefix(Application $app){
            return $app['get_range_url'](
                    'scheduler/point.php?todays_date=1&fromcp=t');
        }
        public function getLogoutRangeUrlPrefix(Application $app){
            return $app['get_range_url'](
                    'login_select_lane.php?s=1&eteled=logout_member&mn=');
        }
        public function getLoginRangeUrlPrefix(Application $app){
            return $app['get_range_url']( 'login_select_lane.php?mn=');
        }
        public function blockUnblock(Application $app) {           
            $isBlock = $app['request']->request->get('isBlock',0);
            $memberNo = (int)$app['request']->request->get('membershipNo');
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $roid =  $app['range_owner_id'];
            $range_member = range_member::initForMember($link, 
                    $roid->getAccountHashKey(), $roid->getAccountNo(), 
                    $memberNo);
            if (!$range_member) {
                return $app->json(['error' => 'Error occured '], 400);
            }
            if ($isBlock) {
                $r = $range_member->blockMember($app['dbs']['rcadmin']);
            } else {
                $r = $range_member->unBlockMember($app['dbs']['rcadmin']);
            }
            if ($r) {
                return $app->json(['status' => 1, 
                    'payload' => $isBlock ? 'blocked' : 'unblocked'] );
            }
            return $app->json(['error' => 'Eror occured'], 400);
        }
        public function editMember(Application $app, $membershipNo) {
            $this->setLastManagedMember($app, $membershipNo);
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $roid =  $app['range_owner_id'];
            $range_member = range_member::initForMember($link, 
                    $roid->getAccountHashKey(), $roid->getAccountNo(), $membershipNo);
            $content = '';
            if (!$range_member) {
                $content = $app['templating']->render('range/error-message.php', 
                        ['message' => "Member with id $membershipNo could not be found"]);
            } else {
                $memtypes = Models\MembershipType::getMembershipTypesForAccount(
                    $link , $app['range_owner_id']);
                $vals = $this->getFieldValues($range_member);
                $vals['membershipTypes'] = $memtypes;
                $billingList = $app['range_owner']->getBillingInfo($range_member->getMembershipNumber());
                $vals['billingList'] = empty($billingList) ? [] : $billingList;
                $vals['schedulerUrlPrefix'] = $this->getScheduleUrlPrefix($app);
                $vals['loginRangeUrlPrefix'] = $this->getLoginRangeUrlPrefix($app);
                $vals['logoutRangeUrlPrefix'] = 
                        $this->getLogoutRangeUrlPrefix($app);
                $vals['isMemberBlocked'] = $range_member->isBlocked(
                        $app['dbs']['rcadmin']);
                
                $vals['addMemberTypeUrl'] = $app['get_range_url'](
                        'common/config.php#createnow');
				//get employee that added the member
				$sqll = "SELECT * FROM `customers` WHERE `membership_number`='" . $membershipNo . "' AND `accountnumber`='" . $roid->getAccountNo() . "'";
				$results = DbHelper::dbFetchRowAssoc($link, $sqll);
				$eid = $results['emp_id'];
				$sql2 = "SELECT * FROM employees WHERE id=" . $eid;
				$results2 = DbHelper::dbFetchRowAssoc($link, $sql2);
				$emp_name = $results2['fname'] . " " . $results2['lname'];
				//variables for benefits section
				$listOfBenefits = "";
				$rid = $app['range_owner_id'];
				$acno = $rid->getAccountNo();	
				$hash = $rid->getAccountHashKey();
				$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				$sql1 = "SELECT * FROM memberships WHERE membership_number='" . $membershipNo . "'";
				$mem_array = DbHelper::dbFetchRowAssoc($link, $sql1);
				$membershiptypename = $mem_array['membership_type'];
				//get member type id to start fetching benefits
				$sql2 = "SELECT * FROM editable_memberships WHERE membership_name='" . $membershiptypename . "' AND accountnumber='" . $acno . "'";
				$mem_id_array = DbHelper::dbFetchRowAssoc($link, $sql2);
				$membertypeid = $mem_id_array['membership_type_id'];
				//get the benefits for this membership type next
				$sql3 = "SELECT * FROM benefits WHERE membership_type_id='" . $membertypeid . "'";
				$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql3);
				
                $visits_sql = "";
                $visits_sql = "SELECT * FROM block_unblock WHERE membership_number='" . $membershipNo . "' AND acctnumberhashed = '" . $hash . "' AND accountnumber = '" .$acno . "' ";
                $get_assoc_array = DbHelper::dbFetchAllAssoc($link, $visits_sql);
                
                $vals['block_stat_date'] = $get_assoc_array[0]['start_block'];
                
                
                $listOfBenefits = "";
				$row_counter = 0;
				$overview_benefits = "";
				$exp=$range_member->getMembershipExpirationDate();
				$exparray=explode('/',$exparry);
				
				$exp=$exparray[2].'-'.$exparray[1].'-'.$exparray[0];
					
					
					//get current dates
					$cday=date('d');
					$cmonth=date('m');
					$cyear=date('Y');
					//test 
					/*$cday=8;
					$cmonth=2;
					$cyear=2016;*/
					//generate membership expiry dates
					$yearlyexp=$cyear.'-01-01';
					$monthlyexp=$cyear.'-'.$cmonth.'-01';
					$dailyexp=date('Y-m-d');
					$weeklyexp=date("Y-m-d", strtotime('monday this week'));
					//test
					/*$dailyexp='2016-02-08';
					$weeklyexp='2016-02-11';*/
					 $sqlused = "SELECT * FROM benefits_used WHERE customer_id=$membershipNo and (";
				foreach ($assoc_array as $benefitArr) {
					if($benefitArr['duration']=='Monthly')
						$expiry=$monthlyexp;
					else if($benefitArr['duration']=='Yearly')
						$expiry=$yearlyexp;
					else if($benefitArr['duration']=='Daily')
						$expiry=$dailyexp;
					else if($benefitArr['duration']=='Weekly')
						$expiry=$weeklyexp;
					else
						$expiry=$exp;
					
					$row_counter++;
					$redeemableamount = $amountused = $max = 0;
					$benefit_id = $benefitArr['id'];
					$benefit_name = $benefitArr['name'];
					$max = $benefitArr['quantity'];
					
					//get all types of benefits here 
					//if($acno==10026)
						$sql4 = "SELECT sum(amount_used) as used FROM benefits_used WHERE customer_id='" . $membershipNo . "' AND benefit_id=" . $benefit_id . " AND date >= '$expiry'";
					   //if($benefit_name == 'daily benifit'){echo $sql4;}
                   /* else
						$sql4 = "SELECT count(*) as used FROM benefits_used WHERE customer_id='" . $membershipNo . "' AND benefit_id=" . $benefit_id;*/
					
					$assoc_array2 = DbHelper::dbFetchAllAssoc($link, $sql4);
					//now count the usages of this benefit for this user
					$amountused=$assoc_array2[0]['used'];
					if(empty($amountused))
						$amountused=0;
					/*foreach ($assoc_array2 as $benefit_used) {
						//21-12-2015
						//check the membership duration
						$amountused += (int) $benefit_used['amount_used'];
					}*/
					$redeemableamount = $max - $amountused;
					$overview_benefits = $overview_benefits . '<tr><td><span style="color: red;">' . $amountused . ' of ' . $max . '</span></td><td>' . $benefit_name . '</td></tr>';
					//finish counting this user's usages of this benefit
					$listOfBenefits = $listOfBenefits . '
						<tr>
						<td class="text-center" style="width:50px;">
                        <input id="benefit-id-' . $row_counter . '" type="hidden" value="' . $benefit_id . '"/>
						<select id="amount-' . $row_counter . '" style="width:40px;"><option value="0" selected></option>';
							for ($i = 1;$i <= $redeemableamount;$i++) {
								$listOfBenefits = $listOfBenefits . '<option value="' . $i . '">' . $i . '</option>';
							}
                            						
					$listOfBenefits = $listOfBenefits . '</select>
						</td>
						<td nowrap=""><span style="color: red;">' . $amountused . ' of ' . $max . '</span></td>
						<td nowrap="">' . $benefit_name . '</td>
						</tr>
					';
					//here generate the benefits used list
					$sqlused.="( benefit_id=" . $benefit_id . " AND date >= '$expiry' ) OR";
					//
				}	
				$sqlused=substr($sqlused, 0, -2);
				$sqlused.=")";
				$sqlused.="ORDER BY date DESC";
				//echo "<br/>".$sqlused;
				//and now - get the used benefits
				/*$sql_new_1 = "SELECT benefits_used.*,employees.*,employees.id as emp_id, benefits.*, benefits.id as ben_id 
                FROM benefits_used 
                INNER JOIN employees ON employees.id = benefits_used.employee_id 
                INNER JOIN benefits ON benefits.id = benefits_used.benefit_id 
                WHERE customer_id=$membershipNo ORDER BY date DESC";*/
                
                
                //$sql4 = "SELECT * FROM benefits_used WHERE customer_id=$membershipNo ORDER BY date DESC";
				$sql4=$sqlused;
				$assoc_array2 = DbHelper::dbFetchAllAssoc($link, $sql4);
				$listOfUsedBenefits = "";
				foreach ($assoc_array2 as $benefit_used) {
					
					$ben_id = $benefit_used['benefit_id'];
					$used_ben_id = $benefit_used['id'];
					$employee_id = $benefit_used['employee_id'];
					//get employee initials
					$newsql = "SELECT * FROM employees WHERE id=" . $employee_id;
					$emp_name_array = DbHelper::dbFetchRowAssoc($link, $newsql);
					$employee_name = $emp_name_array['initials'];
					//
					$getnamesql = "SELECT * FROM benefits WHERE id=" . $ben_id . "";
					$get_name_array = DbHelper::dbFetchRowAssoc($link, $getnamesql);
					$dateArr = explode("-", $benefit_used['date']);
					$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
					$listOfUsedBenefits = $listOfUsedBenefits . '<tr>
								<td style="width:60px;">' . $parsedDate . '</td>
								<td><span style="color: red;">' . $benefit_used['amount_used'] . '</span></td>
								<td>' . $get_name_array['name'] . '</td>
								<td>' . $employee_name . '</td>
								<td style="width:60px;"><span id="deletebtn-' . $ben_id . '"><a onclick="return myConfirm(\'Are you sure you want to unredeem this benefit?\', ' . $used_ben_id . ')" href="/xapp/benefits/unredeem/' . $membershipNo . '/' . $used_ben_id . '">Delete</a></span></td>
							</tr>
							<tr>
								<td colspan="5" style="font-size: 14px;">' . $benefit_used['comments'] . '</td>
							</tr>';
				} 
							$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please select an employee!"]);	
				$benVariables = [
					'membershiptypename' => $membershiptypename,
					'listOfBenefits' => $listOfBenefits,
					'listOfUsedBenefits' => $listOfUsedBenefits,
					'customer_id' => $membershipNo,
					'overview_benefits' => $overview_benefits,
					'employee_name' => $employee_name,
					'errmsg' => $err,
				];
				$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
				$roid = $app['range_owner_id'];
				$accno = $roid->getAccountNo();	
				$hash = $roid->getAccountHashKey();
				$sql5 = "SELECT * FROM payment_types WHERE accountnumber='$accno'";
				$payment_types = DbHelper::dbFetchAllAssoc($link, $sql5);
				$sql6 = "SELECT * FROM payment_for WHERE accountnumber='$accno'";
				$payment_for = DbHelper::dbFetchAllAssoc($link, $sql6);
				/// 3-12-2015 get the values of lasts visits
				$lastvisits=array();
				
				$sql8 = "SELECT count(*) as totalvisits FROM range_time_tracker WHERE accountnumber='" . $accno . "' AND membership_number='".$membershipNo."' AND end_time <> 0 AND end_time <> 1";
				$visitcount=DbHelper::dbFetchAllAssoc($link, $sql8);
				$totalvisitcounter=$visitcount[0]['totalvisits'];
				$totalsection=($totalvisitcounter/3);
				
				$sql7 = "SELECT *, STR_TO_DATE( date_stamp,  '%m/%d/%Y' ) AS parsedDate FROM range_time_tracker WHERE accountnumber='" . $accno . "' AND membership_number='".$membershipNo."' AND end_time <> 0 AND end_time <> 1 order by parsedDate desc limit 0,3";
				$lastvisits=DbHelper::dbFetchAllAssoc($link, $sql7);
				/*echo '<pre>';
				print_r($lastvisits);
				echo '</pre>';
				die;*/
				// 
				//end of variables for benefits section
                $data = [                    
                    'memberAddUrl' => $app['get_range_url']('customers/add'),
                    'member' => $range_member,
                    'values' =>  $vals + ['emp_name' => $emp_name],//magic
                    'overview' => $app['templating']->render(
                            'range/customer/overview.php', 
                            $vals+['member'=>$range_member, 'emp_name' => $emp_name]),
					'benVariables' => $benVariables,
					'link' => $link,
					'emp_name' => $emp_name,
					'mem_no' => $membershipNo,
					'payment_types' => $payment_types,
					'payment_for' => $payment_for,
					'lastvisits'=>$lastvisits,
					'totalvisitcounter'=>$totalvisitcounter,
					'totalsection'=>$totalsection
                ];
                $content = $app['templating']->render('range/customer/edit.php', $data);
            }
            $breadcrumbs = $this->getBreadcrumbs($app);
            $title = 'Manage Member: ' .  $range_member->getFullName();
            $breadcrumbs[] = ['title' => $title];
            $pageTitle = 'Range Controller - Members - ' . $title;
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function visit(Application $app,$membershipno,$pageno)
		{
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$start=($pageno)*3;
			$sql = "SELECT *, STR_TO_DATE( date_stamp,  '%m/%d/%Y' ) AS parsedDate FROM range_time_tracker WHERE accountnumber='" . $accno . "' AND membership_number='".$membershipno."' AND end_time <> 0 AND end_time <> 1 order by parsedDate desc limit $start,3";
			$lastvisits=DbHelper::dbFetchAllAssoc($link, $sql);
			$result=array('success'=>true,'data'=>array());
			if(empty($lastvisits)) { $result['success']=false; } 
			$i=0;
			foreach($lastvisits as $list){
				$vdate=$list['date_stamp'];
				$start_time=date("g:i a", strtotime($list['start_time']));
				$end_time=date("g:i a", strtotime($list['end_time']));
				$st_t = explode(":", $list['start_time']);
				$en_t = explode(":", $list['end_time']);
				$hrs = (int) $en_t[0] - (int) $st_t[0];
				$mins = (int) $en_t[1] - (int) $st_t[1];
				if ($mins < 0){
					$mins += 60;
					$hrs--;
				}
				if ($hrs < 0) $hrs * -1;
				$duration = $hrs . ":" . sprintf("%02s", $mins);
				
				$result['data'][$i]['date_stamp']=$list['id'];
				$result['data'][$i]['date_stamp']=$vdate;
				$result['data'][$i]['start_time']=$start_time;
				$result['data'][$i]['end_time']=$end_time;
				$result['data'][$i]['duration']=$duration;
				
				$i++;
			}
			header('Content-type: application/json');
			echo json_encode($result);
			die;
		}
		public function visitdelete(Application $app,$membershipno,$recno){
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "delete range_time_tracker WHERE accountnumber='" . $accno . "' AND membership_number='".$membershipno."' AND id=".$recno;
			
			$link->query($sql);
			
			$sql = "update `customers` set `visitcount`=`visitcount`-1 WHERE `accountnumber`='" . $accno . "' AND `membership_number`='".$membershipno."'";
			
			$link->query($sql);
			
			$result=array('success'=>true);
			
			header('Content-type: application/json');
			echo json_encode($result);
			die;
		}
		function redeemBenefit (Application $app, $customer_id, $benefit_id, $amount, $comments, $employee_id) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			//if benefit id is set (or not 0) use update
			//if not, use insert
			$qsql = "INSERT INTO benefits_used (employee_id, benefit_id, customer_id, comments, amount_used, date) VALUES (" . $employee_id . ", " . $benefit_id . ", '" . $customer_id . "', '" . $comments . "', " . $amount . ", " . "CURDATE()" . ")";
			$link->query($qsql);
			return $qsql;
		}
		function unredeemBenefit (Application $app, $customer_id, $benefit_id) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qsql = "DELETE FROM benefits_used WHERE customer_id='" . $customer_id . "' AND id=" . $benefit_id;
			$link->query($qsql);		
		}
        public function getOverview(Application $app) {
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $roid =  $app['range_owner_id'];
            $range_member = range_member::initForMember($link, 
                    $roid->getAccountHashKey(), $roid->getAccountNo(),
                    $app['request']->get('membershipNo',0));
            $vals = $this->getFieldValues($range_member);
            return $app['templating']->render(
                            'range/customer/overview.php', 
                            $vals+['member'=>$range_member]);
        }
        public function updateMemberJson(Application $app){   
            $conn = $app['dbs']['main'];
            $roid =  $app['range_owner_id'];
            $range_member = range_member::initForMember(DbHelper::getWrappedHandle($conn), 
                    $roid->getAccountHashKey(), $roid->getAccountNo(), 
                    $app['request']->get('membershipNo',0));
            if (empty($range_member)) {
                return $app->json(['error'=>'Invalid member'], 400);
            }
            $post = $app['request']->request->all();
			
            $cmd = $app['request']->get('cmd', '');
			$emp_id = $app['request']->get('emp_id', '');
			$payment_for = $app['request']->get('payment_for', '');
            $payload = null;
            if ($cmd == 'updateContact') {
                $fieldMap = $this->getFieldMapsForTable('customers');
                $vals = $this->getDataThatIsSet($fieldMap, $post);
                $range_member->updateCustomers($vals, $conn);
                $fieldMapEContact = $this->getFieldMapsForTable('emergency_contact');
                $vals2 = $this->getDataThatIsSet($fieldMapEContact, $post);
                $range_member->updateOrInsertCommon('emergency_contact', $vals2, $conn);
            } else if ($cmd == 'updateMembership') {
                $fieldMap = $this->getFieldMapsForTable('memberships');
                $vals = $this->getDataThatIsSet($fieldMap, $post);
                $range_member->updateOrInsertCommon('memberships', $vals, $conn);
            } else if ($cmd == 'updateDriversLicense') {                
                $vals = $this->getDlFromPost($app['request']);
                $range_member->updateOrInsertCommon('driver_license_info', $vals, $conn);
            } else if ($cmd == 'updateComments') {
                $vals = ['license_comments' => 
                    $app['request']->request->get('commentsText', '')];
                $range_member->updateOrInsertCommon('driver_license_info', $vals, $conn);
               // echo $range_member;
            } else if ($cmd == 'addBilling') {
                $fieldMap = $this->getFieldMapsForTable('billing_info');
                $vals = $this->getDataThatIsSet($fieldMap, $post);
                $vals['membership_type'] = $range_member->getMembershipType();
                $vals['source'] = 'range member account';
                //$vals['id'] = strtotime('now');
				$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				$qry = "SELECT * FROM employees WHERE id=" . $emp_id;
				$results = DbHelper::dbFetchRowAssoc($link, $qry);
				//$emp_name = $results['fname'] . " " . $results['lname'];
				$emp_name = $results['initials']; 
				$vals['employee_id'] = $emp_id;
				$vals['employee_name'] = $emp_name;
				$vals['payment_for'] = $payment_for;
				//$emp_id
                //placeholder
                $range_member->addBilling( $vals, $conn);
                $bl =  $app['range_owner']->getBillingInfo(
                        $range_member->getMembershipNumber());
                $payload = empty($bl) ? null : $bl[0];
            } else if ($cmd == 'deleteBilling') {
                $id =  $app['request']->get('id', '');
                $res = $range_member->deleteBilling($id, $conn);
                $bl = $app['range_owner']->getBillingInfo(
                        $range_member->getMembershipNumber());
                $payload = empty($bl) ? [] : $bl; 
            } else if ($cmd == 'updateBilling') {                
                $res = $range_member->updateBilling($post, $post['id'], $conn);
                $bl = $app['range_owner']->getBillingInfo(
                        $range_member->getMembershipNumber());
                $payload = empty($bl) ? [] : $bl; 
            } else if ($cmd == 'updateAdditionalInfo') {
                $fieldMap = $this->getFieldMapsForTable('certs_and_safety');
                $vals = $this->getDataThatIsSet($fieldMap, $post);
                $range_member->updateOrInsertCommon('certs_and_safety', $vals, $conn);
            } else if ($cmd == 'getOverview') {
                return $this->getOverview($app);
            } else {
                return $app->json(['message' => 'Invalid command'],400 );
            }
            return $app->json(['status'=>1, 'payload' => $payload]);
        }
        private function getDataThatIsSet($fieldMap, $data){
            $vals =[];
            foreach($fieldMap as $dbcol => $dataKey) {
                if (isset($data[$dataKey])) {
                    $vals[$dbcol] = $data[$dataKey];
                }
            }
            return $vals;
        }
        private function getDlFromPost(Request $req){
            $dl = new Models\DriversLicense();
            $dl->state = $req->request->get('dlState', '');
            $dl->number = $req->request->get('dlNumber', '');
            $dl->expirationDate = $req->request->get('dlExpiration', '');
            $dl->heightFeet = $req->request->get('dlHeightFeet', '');
            $dl->heightInches = $req->request->get('dlHeightInches', '');
            $dl->weight = $req->request->get('dlWeight', '');
            $dl->hairColor = $req->request->get('dlHair', '');
            $dl->eyeColor = $req->request->get('dlEyes', '');
            $dl->race = $req->request->get('dlRace', '');
            $dl->gender = $req->request->get('dlGender', '');
            $dl->birthDate = $req->request->get('dlBirthDate', '');
            $dl->birthCity  = $req->request->get('dlBirthCity', '');
            $dl->birthState  = $req->request->get('dlBirthState', '');
            //$dl->comments = $req->request->get('comments', '');
            $dl->comments = $req->request->get('commentsText', '');
            return $dl->getTableRowArray();
        }
        private function getFieldMapsForTable($table) {
            $maps = [
                'customers' => [
                    'first_name' => 'firstName',
                    'middle_name' => 'middleName',
                    'last_name' => 'lastName',
                    'street_address' => 'street',
                    'city' => 'city',
                    'state' => 'state',
                    'zipcode' => 'zipcode',
                    'ssn' => 'ssn',
                    'home_phone' => 'phone1',
                    'cell_phone' => 'phone2',
                    'email' => 'email',
					'email_status' => 'email_status',
                ],
                'memberships' => [
                    'membership_type' => 'membershipType',
                    'membership_expires' => 'memExpirationDate',
                    'membership_date' => 'memRegistrationDate'
                ],
                'emergency_contact' => [
                    'contact_name' => 'emergencyName',
                    'contact_phone' => 'emergencyPhone'
                ],
                'billing_info' => [
                    'date' => 'billingDate',
                    'payment_method' => 'billingPaymentType', 
                    'payment_amount' => 'billingAmount',
                    'payment_for' => 'billingPaymentFor'
                ],
                'certs_and_safety' => [
                    'certs_video_complete' => 'safetyVideoCompleted',
                    'certs_safety_form_complete' => 'safetyRangeFormCompleted',
                    'certs_certificates_comments' => 'certsComments'
                ]
            ];
            return isset($maps[$table]) ? $maps[$table] : null;
        }
        private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $data['breadcrumbs']]);
            $vars = [
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
        }
        private function getBreadcrumbs(Application $app){
            return array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Members']
            );
        }
		private function getBreadcrumbs_config(Application $app){
            return array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations']
            );
        }
        public function getMembersJson(Application $app){
            $filters = [];
            $req = $app['request'];
            $filterType = $req->get('filterType', '');
            if ($filterType) {
                $filters[$filterType] = $req->get('filterValue', '');
            }
            $currentPage = $req->get('currentPage', 1);
            $pager = new Services\Pager($currentPage, $this->pageSize);
            $pagedResults=  $app['range_owner']->getMembers($filters, $pager);
            return $app->json($pagedResults->toArray());
        }
		
        public function uploadImage(Application $app, $memberNo){
            $req = $app['request'];
            /* @var range_owner_id */
            $range_owner_id = $app['range_owner_id'];
            $file = $req->files->get('file'); 
            if (empty($file)) {
                return $app->json(['error'=> 'No file is sent']);
            }   
            $path = $app['range_member_image_dir'] . '/';            
            //$filename = $files['FileUpload']->getClientOriginalName();
            $mime = $file->getMimeType();
            $okMimes = $this->acceptedImageMimeTypes();
            if (!in_array($mime, array_keys($okMimes))) {
                return $app->json(['error' => 'Invalid file format; please use jpg/jpeg, gif, png files']);
            }
            if (empty($memberNo)) {
                return $app->json(['error' => 'Member no is missing']);
            }
//            $finder = new Finder();
//            $files = $finder->files()->name($memberNo . '.*')->in($path);
//            //$f = new File;
//            foreach( $files as $f) {
//                unlink($path . $f->getFilename());
//            }
            $this->deleteMemberImage($memberNo, $path);
            $filename = $memberNo . '.' . $okMimes[$mime];
            $file->move($path, $filename);
            $url = str_replace($app['config']['base_path'], '', $path . $filename);
            //$files['FileUpload']->move($path,$filename);
            return $app->json(['url' => $url]);      
        }
        private  function deleteMemberImage($memberNo, $path) {
            $finder = new Finder();
            $files = $finder->files()->name($memberNo . '.*')->in($path)->followLinks();
            //$f = new File;
            foreach( $files as $f) {
                @unlink($path . $f->getFilename());
            }
        }
        public function downloadImage(Application $app, $memberNo) {
            $imgpath = $app['range_member_image_path']($memberNo);
            if (!empty($imgpath) && file_exists($imgpath)) {
                return $app->sendFile($imgpath)
                        ->setContentDisposition(
                                ResponseHeaderBag::DISPOSITION_ATTACHMENT, 
                                basename($imgpath));
            }
            return null;
        }
        public function saveWebcamImage(Application $app, $memberNo){
            $dir = $app['range_member_image_dir']; 
            $fname = $dir ."/" . (int)$memberNo . '.png';
            $this->deleteMemberImage($memberNo, $dir . '/');
            if ($_POST['type'] == "pixel") {
                    // input is in format 1,2,3...|1,2,3...|...
                    $im = imagecreatetruecolor(320, 240);
                    foreach (explode("|", $_POST['image']) as $y => $csv) {
                            foreach (explode(";", $csv) as $x => $color) {
                                    imagesetpixel($im, $x, $y, $color+0);
                            }
                    }
                    $r = imagepng($im, $fname );
            } else { //type=data
                    // input is in format: data:image/png;base64,...
                    $im = imagecreatefrompng($_POST['image']);
                    $r = imagejpeg($im, $fname);
            }
            $url = str_replace($app['config']['base_path'], '', $fname);
            if ($r) {
                return $app->json(['url' => $url]);
            } else {
                return $app->json(['error' => 'Error occured while saving', 'file'=>$fname] );
            }
        }
        private function acceptedImageMimeTypes(){
            return [
                'image/jpeg' => 'jpg', 
                'image/jpeg'=> 'jpeg',
                'image/png' => 'png',
                'image/gif' => 'gif'
                ];
        }
		public function deletemember(Application $app, $membershipNo){
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$sql="update customers set is_delete=0 where acctnumberhashed='".$hash."' and accountnumber='".$accno."' and membership_number='".$membershipNo."'";
			$link->query($sql);
			die;
        }
		public function printIdCard(Application $app, $membershipNo){
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$get_id_card_creator = range_member::get_id_card_creator($app, $hash, $accno);
			$get_customer = range_member::get_customer($app, $hash, $accno, $membershipNo);
			$get_emergency_contact = range_member::get_emergency_contact($app, $hash, $accno, $membershipNo);
			$get_driver_license_info = range_member::get_driver_license_info($app, $hash, $accno, $membershipNo);
			$get_memberships = range_member::get_memberships($app, $hash, $accno, $membershipNo);
			$get_certs_and_safety = range_member::get_certs_and_safety($app, $hash, $accno, $membershipNo);
			$get_controller_config = range_member::get_controller_config($app, $hash, $accno, $membershipNo);
			$get_member_login = range_member::get_member_login($app, $hash, $accno, $membershipNo);
			$range_member = range_member::initForMember($link, $hash, $accno, $membershipNo);
			$data = array(
				'get_id_card_creator' =>  $get_id_card_creator,
				'get_customer' => $get_customer,
				'get_emergency_contact' => $get_emergency_contact,
				'get_driver_license_info' => $get_driver_license_info,
				'get_memberships' => $get_memberships,
				'get_certs_and_safety' => $get_certs_and_safety,
				'get_controller_config' => $get_controller_config,
				'get_member_login' => $get_member_login,
				'member' => $range_member,
				'rangeLogoUrl' => $app['range_logo_url']
            );
			return $app['templating']->render('range/customer/printidcard.php', $data);
        }
		
		public function filterTypes(Application $app){
	
			$filterType = $_POST['filterType']; 

			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$qsql = "UPDATE admin_login SET filtertypes='" . $filterType . "' WHERE accountnumber='" . $accno . "'";
			
			$link->query($qsql);
			return $app->json();
        }
	
    }
	
}