<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class ClassController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
            //$pf = $app['range_url_prefix'];
            //return $app['request']->get('cmd', 'aa') .print_r($pf,1) ;
            
            return $this->listClasses($app);
        }
        
        
        
        public function listClasses(Application $app){
		
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			//get upcoming classes
			
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND start_date >= CAST(NOW() AS DATE) ORDER BY start_date ASC";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$classRows = "";
			
			foreach ($assoc_array as $classArr) {
			
				$dateArr = explode("-", $classArr['start_date']);
				
				$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
				$classRows = $classRows . '<tr>
          <td nowrap="">' . $parsedDate . '</td>
          <td nowrap="">' . $classArr['title'] . '</td>
          <td>' . $classArr['instructor_name'] . '</td>
          <td class="text-center"><a href="classes/manage/'. $classArr['id'] . '">Manage</a></td>
          <td class="text-center"><a href="classes/registrationList/' . $classArr['id'] . '">Registration List</a></td>
          <td class="text-center"><a href="classes/delete/' . $classArr['id'] . '" onclick="return myConfirm(\'Are you sure you want to delete ' . $classArr['title'] . '?\', ' . $classArr['id'] . ')">Delete</a></td>
        </tr>';
			
			}
			//get past classes
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND start_date < CAST(NOW() AS DATE) ORDER BY start_date DESC";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$classRows2 = "";
			
			foreach ($assoc_array as $classArr) {
			
				$dateArr = explode("-", $classArr['start_date']);
				
				$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
				$classRows2 = $classRows2 . '<tr>
          <td nowrap="">' . $parsedDate . '</td>
          <td nowrap="">' . $classArr['title'] . '</td>
          <td>' . $classArr['instructor_name'] . '</td>
          <td class="text-center"><a href="classes/manage/'. $classArr['id'] . '">Manage</a></td>
          <td class="text-center"><a href="classes/registrationList/' . $classArr['id'] . '">Registration List</a></td>
          <td class="text-center"><a href="classes/delete/' . $classArr['id'] . '" onclick="return myConfirm(\'Are you sure you want to delete ' . $classArr['title'] . '?\', ' . $classArr['id'] . ')">Delete</a></td>
        </tr>';
			
			}
			
			//
			
			$new_sql = "SELECT * FROM instructors WHERE accountnumber='" . $accno . "'";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $new_sql);
			
			$instructorOptions = "";
			
			foreach ($assoc_array as $instructor) {
			
				$instructorOptions = $instructorOptions . "<option value='" . $instructor['instructor_number'] . "'>" . $instructor['first_name'] . " " . $instructor['last_name'] . "</option>";
			
			}
			
					//
			
			$vars = [
				'upcomingclasses' => $classRows,
				'pastclasses' => $classRows2,
				'instructorOptions' => $instructorOptions,
			];
            
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[1]['link'] = '';
					
			$content = $app['templating']->render('range/class/class_list.php', $vars);
			
			$pageTitle = 'Range Controller - Class List';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
			
        }
		
		public function listClassesByInstructor(Application $app, $instructor_id){
		
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			//get upcoming classes
			
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND start_date >= CAST(NOW() AS DATE) ORDER BY start_date ASC";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$classRows = "";
			
			foreach ($assoc_array as $classArr) {
			
				$dateArr = explode("-", $classArr['start_date']);
				
				$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
				if ($classArr['instructor_number'] == $instructor_id) {
				
					$classRows = $classRows . '<tr>
					<td nowrap="">' . $parsedDate . '</td>
					<td nowrap="">' . $classArr['title'] . '</td>
					<td>' . $classArr['instructor_name'] . '</td>
					<td class="text-center"><a href="../../classes/manage/'. $classArr['id'] . '">Manage</a></td>
					<td class="text-center"><a href="../../classes/registrationList/' . $classArr['id'] . '">Registration List</a></td>
					<td class="text-center"><a href="../../classes/delete/' . $classArr['id'] . '" onclick="return confirm(\'Are you sure you want to delete ' . $classArr['title'] . '?\')">Delete</a></td>
					</tr>';
					
					}
			
			}
			//get past classes
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND start_date < CAST(NOW() AS DATE) ORDER BY start_date DESC";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$classRows2 = "";
			
			foreach ($assoc_array as $classArr) {
			
				$dateArr = explode("-", $classArr['start_date']);
				
				$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
				if ($classArr['instructor_number'] == $instructor_id) {
				
					$classRows2 = $classRows2 . '<tr>
					<td nowrap="">' . $parsedDate . '</td>
					<td nowrap="">' . $classArr['title'] . '</td>
					<td>' . $classArr['instructor_name'] . '</td>
					<td class="text-center"><a href="../../classes/manage/'. $classArr['id'] . '">Manage</a></td>
					<td class="text-center"><a href="../../classes/registrationList/' . $classArr['id'] . '">Registration List</a></td>
					<td class="text-center"><a href="../../classes/delete/' . $classArr['id'] . '" onclick="return confirm(\'Are you sure you want to delete ' . $classArr['title'] . '?\')">Delete</a></td>
					</tr>';
					
					}
			
			}
			
			//
			
			$new_sql = "SELECT * FROM instructors WHERE accountnumber='" . $accno . "'";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $new_sql);
			
			$instructorOptions = "";
			
			foreach ($assoc_array as $instructor) {
			
				$instructorOptions = $instructorOptions . "<option value='" . $instructor['instructor_number'] . "'>" . $instructor['first_name'] . " " . $instructor['last_name'] . "</option>";
			
			}
			
					//
			
			$vars = [
				'upcomingclasses' => $classRows,
				'pastclasses' => $classRows2,
				'instructorOptions' => $instructorOptions,
			];
            
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[1]['link'] = '';
					
			$content = $app['templating']->render('range/class/class_list.php', $vars);
			
			$pageTitle = 'Range Controller - Class List';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
			
        }
		
		
		
		
		
		public function listClassesByDate(Application $app, $month, $year){
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			//get upcoming classes
			
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND start_date >= CAST(NOW() AS DATE) ORDER BY start_date ASC";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$classRows = "";
			
			foreach ($assoc_array as $classArr) {
			
				$dateArr = explode("-", $classArr['start_date']);
				
				$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
				if ($dateArr[1] == $month && $dateArr[0] == $year) {
				
					$classRows = $classRows . '<tr>
					<td nowrap="">' . $parsedDate . '</td>
					<td nowrap="">' . $classArr['title'] . '</td>
					<td>' . $classArr['instructor_name'] . '</td>
					<td class="text-center"><a href="../../../classes/manage/'. $classArr['id'] . '">Manage</a></td>
					<td class="text-center"><a href="../../../classes/registrationList/' . $classArr['id'] . '">Registration List</a></td>
					<td class="text-center"><a href="../../../classes/delete/' . $classArr['id'] . '" onclick="return confirm(\'Are you sure you want to delete ' . $classArr['title'] . '?\')">Delete</a></td>
					</tr>';
					
					}
			
			}
			//get past classes
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND start_date < CAST(NOW() AS DATE) ORDER BY start_date DESC";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$classRows2 = "";
			
			foreach ($assoc_array as $classArr) {
			
				$dateArr = explode("-", $classArr['start_date']);
				
				$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
				if ($dateArr[1] == $month && $dateArr[0] == $year) {
				
					$classRows2 = $classRows2 . '<tr>
					<td nowrap="">' . $parsedDate . '</td>
					<td nowrap="">' . $classArr['title'] . '</td>
					<td>' . $classArr['instructor_name'] . '</td>
					<td class="text-center"><a href="../../../classes/manage/'. $classArr['id'] . '">Manage</a></td>
					<td class="text-center"><a href="../../../classes/registrationList/' . $classArr['id'] . '">Registration List</a></td>
					<td class="text-center"><a href="../../../classes/delete/' . $classArr['id'] . '" onclick="return confirm(\'Are you sure you want to delete ' . $classArr['title'] . '?\')">Delete</a></td>
					</tr>';
					
					}
			
			}
			
			//
			
			$new_sql = "SELECT * FROM instructors WHERE accountnumber='" . $accno . "'";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $new_sql);
			
			$instructorOptions = "";
			
			foreach ($assoc_array as $instructor) {
			
				$instructorOptions = $instructorOptions . "<option value='" . $instructor['instructor_number'] . "'>" . $instructor['first_name'] . " " . $instructor['last_name'] . "</option>";
			
			}
			
					//
			
			$vars = [
				'upcomingclasses' => $classRows,
				'pastclasses' => $classRows2,
				'instructorOptions' => $instructorOptions,
			];
            
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[1]['link'] = '';
					
			$content = $app['templating']->render('range/class/class_list.php', $vars);
			
			$pageTitle = 'Range Controller - Class List';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
			
        }
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		
		if ($classArr['instructor_id'] == $instructor_id) {
				
					$classRows = $classRows . '<tr>
					<td nowrap="">' . $parsedDate . '</td>
					<td nowrap="">' . $classArr['title'] . '</td>
					<td>' . $classArr['instructor_name'] . '</td>
					<td class="text-center"><a href="classes/manage/'. $classArr['id'] . '">Manage</a></td>
					<td class="text-center"><a href="classes/registrationList/' . $classArr['id'] . '">Registration List</a></td>
					<td class="text-center"><a href="classes/delete/' . $classArr['id'] . '" onclick="return confirm(\'Are you sure you want to delete ' . $classArr['title'] . '?\')">Delete</a></td>
					</tr>';
					
					}
					
		*/
		
		private function wrapInLayout(Application $app, $data ){
                        
			$breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $data['breadcrumbs']]);
						
			$ro = $app['range_owner'];
						
            $vars = [
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
					'companyName' => $ro->getCompanyName(),
                'content' => $breadcrumbsContent . $data['content'],
				'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
        }
        
        private function getBreadcrumbs(Application $app){
            return array(
                ['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('classes'), 'title'=>'Classes']
            );
        }
		 
		function getInstructors (Application $app) {
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Instructors ' ];
			
			$tablerows = "";
			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$query_sql = "SELECT * FROM instructors WHERE accountnumber='" . $accno . "'";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			foreach ($assoc_array as $instructor) {

			$tablerows = $tablerows . '<tr>
      <td>' . $instructor['first_name'] . " " . $instructor['last_name'] . '</td>
      <td>' . $instructor['email_address'] . '</td>
      <td class="text-center"><a href="instructors/manage/' . $instructor['instructor_number'] . '">Manage</a></td>
	  <td class="text-center"><a href="instructors/delete/' . $instructor['instructor_number'] . '" onclick="return myConfirm(\'Are you sure you want to delete ' . $instructor['first_name'] . " " . $instructor['last_name'] . '?\', ' . $instructor['instructor_number'] . ')">Delete</a></td>
    </tr>';

			}
	
			$vars = [
                'instructors' => $tablerows,
            ];
					//echo $app['templating']->render('range/error-message.php', "Please fill out all of the required fields");
			$content = $app['templating']->render('range/class/instructors.php', $vars);
			
			$pageTitle = 'Range Controller - Class List - Instructors';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}
		
		function addClass (Application $app) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Add New Class ' ];
			
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$query_sql = "SELECT * FROM instructors WHERE accountnumber='" . $accno . "'";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$instructorOptions = "";
			
			foreach ($assoc_array as $instructor) {
			
				$instructorOptions = $instructorOptions . "<option value='" . $instructor['instructor_number'] . "'>" . $instructor['first_name'] . " " . $instructor['last_name'] . "</option>";
			
			}
			
			$vars = [
			'instructorOptions' => $instructorOptions,
			'errmsg' => $err,
			];
					
			$content = $app['templating']->render('range/class/add_class.php', $vars);
			
			$pageTitle = 'Range Controller - Classes - Add Class';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}
		
		function manageClass (Application $app, $class_id) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Manage Class ' ];
			
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$query_sql = "SELECT * FROM instructors WHERE accountnumber='" . $accno . "'";
    
			//return $insert_sql;
	
			$assoc_array2 = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$query_sql = "SELECT * FROM classes WHERE id='" . $class_id . "'";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchRowAssoc($link, $query_sql);
			
			$instructorOptions = "";
			
			foreach ($assoc_array2 as $instructor) {
			
				$selected = ($instructor['instructor_number'] == $assoc_array['instructor_number']) ? " selected='selected' " : "";
			
				$instructorOptions = $instructorOptions . "<option " . $selected . "value='" . $instructor['instructor_number'] . "'>" . $instructor['first_name'] . " " . $instructor['last_name'] . "</option>";
			
			}
			
			$dateArr = explode("-", $assoc_array['start_date']);
				
			$startD = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
			$dateArr = explode("-", $assoc_array['end_date']);
				
			$endD = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
			
			$vars = [
			'instructorOptions' => $instructorOptions,
			'errmsg' => $err,
			'classtitle' => $assoc_array['title'],
			'nos' => $assoc_array['numofstudents'],
			'cost' => $assoc_array['cost'],
			'startd' => $startD,
			'endd' => $endD,
			'startt' => $assoc_array['start_time'],
			'endt' => $assoc_array['end_time'],
			'description' => $assoc_array['description'],
			'class_id' => $assoc_array['id'],
			];
					
			$content = $app['templating']->render('range/class/manage_class.php', $vars);
			
			$pageTitle = 'Range Controller - Classes - Manage Class';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}
		
		function manageStudent (Application $app, $class_id, $student_id) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
		
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sqlq = "SELECT customers.first_name, customers.last_name, customers.email, customers.home_phone, customers.street_address, customers.city, customers.state, customers.zipcode, student_data.amount_paid, student_data.drivers_license, student_data.emergency_no FROM customers INNER JOIN student_data ON customers.membership_number=student_data.membership_number WHERE customers.membership_number='" . $student_id . "'" ;

			$temp = DbHelper::dbFetchRowAssoc($link, $sqlq);
			
			$fname = $temp['first_name'];
			
			$lname = $temp['last_name'];
			
			$email = $temp['email'];
			
			$address = $temp['street_address'];
			
			$city = $temp['city'];
			
			$state = $temp['state'];
			
			$zipcode = $temp['zipcode'];
			
			$phone = $temp['home_phone'];
			
			$emergency_phone = $temp['emergency_no'];
			
			$amount_paid = $temp['amount_paid'];
			
			$license = $temp['drivers_license'];
			
			$vars = [
			'fname' => $fname,
			'lname' => $lname,
			'email' => $email,
			'address' => $address,
			'city' => $city,
			'state' => $state,
			'zipcode' => $zipcode,
			'phone' => $phone,
			'emergency_phone' => $emergency_phone,
			'amount_paid' => $amount_paid,
			'license' => $license,
			'class_id' => $class_id,
			'student_id' => $student_id,
			'errmsg' => $err,
			
			];
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Manage Student ' ];
					
			$content = $app['templating']->render('range/class/manage_student.php', $vars);
			
			$pageTitle = 'Range Controller - Classes - Manage Student';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}
		
		function manageInstructor (Application $app, $instructor_no) {
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Manage Instructor ' ];
			
			
			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$query_sql = "SELECT * FROM instructors WHERE instructor_number='" . $instructor_no . "' AND accountnumber='" . $accno . "'";
    
			//return $insert_sql;
	
			$data = DbHelper::dbFetchRowAssoc($link, $query_sql);
			
			//foreach ($assoc_array as $instructor) {

			/*$tablerows = $tablerows . '<tr>
      <td>' . $instructor['first_name'] . " " . $instructor['last_name'] . '</td>
      <td>' . $instructor['email_address'] . '</td>
      <td class="text-center"><a href="instructors/manage">Manage</a></td>
      <td class="text-center"><a href="">Delete</a></td>
    </tr>'; */

			//}
			
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
	
			$params = [
                'first_name' => $data['first_name'],
				'last_name' => $data['last_name'],
				'email_address' => $data['email_address'],
				'instructor_number' => $data['instructor_number'],
				'class_full_address' => $data['class_full_address'],
				'range_full_address' => $data['range_full_address'],
				'school_name' => $data['school_name'],
				'school_number' => $data['school_number'],
				'errmsg' => $err,
            ];
            
					
			$content = $app['templating']->render('range/class/manage_instructor.php', $params);
			
			$pageTitle = 'Range Controller - Classes - Manage Instructor';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}
		
		function addStudent (Application $app, $class_id) {
		
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Add New Student ' ];
			
			$vars = [
			
			'errmsg'=> $err,
			
			'class_id' => $class_id,
			
			];
					
			$content = $app['templating']->render('range/class/add_student.php', $vars);
			
			$pageTitle = 'Range Controller - Class List - Add New Student';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}
		
		function rescheduleStudent (Application $app, $class_id, $student_id) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND start_date >= CAST(NOW() AS DATE)";
		
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			
			$classRows = "";
			
			foreach ($assoc_array as $classArr) {
			
				$cnt_sql = "SELECT COUNT(*) AS counted FROM registrations WHERE class_id='" . $classArr['id'] . "'";
			
				$cnter = DbHelper::dbFetchRowAssoc($link, $cnt_sql);
			
				$tkenseats = $cnter['counted'];
			
				$seats = ((int) $classArr['numofstudents']) - ((int)$tkenseats);
			
				$dateArr = explode("-", $classArr['start_date']);
				
				$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
			
				$classRows = $classRows . '<tr>
				<td><input type="radio" name="class_select" value="' . $classArr['id'] . '"></td>
				<td>' . $parsedDate . '</td>
				<td nowrap="">' . $classArr['title'] . '</td>
				<td>' . $classArr['instructor_name'] . '</td>
				<td>' . $seats . ' of ' . $classArr['numofstudents'] . ' seats available</td>
				</tr>';
			
			}
		
			//$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
		
			/*
			
			
	
			*/
			
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND id='" . $class_id . "'";
			
			$this_class = DbHelper::dbFetchRowAssoc($link, $query_sql);
			
			$query_sql = "SELECT * FROM customers WHERE accountnumber='" . $accno . "' AND membership_number='" . $student_id . "'";
			
			$this_student = DbHelper::dbFetchRowAssoc($link, $query_sql);
			
			$count_sql = "SELECT COUNT(*) AS counted FROM registrations WHERE class_id='" . $class_id . "'";
			
			$counter = DbHelper::dbFetchRowAssoc($link, $count_sql);
			
			$takenseats = $counter['counted'];
			
			$availableseats = ((int) $this_class['numofstudents']) - ((int)$takenseats);
			
			$vars = [
			
				'upcomingClasses' => $classRows,
				'student_name' => $this_student['first_name'] . ' ' . $this_student['last_name'],
				'student_id' => $this_student['membership_number'],
				'classdate' => $parsedDate,
				'seatstaken' => $availableseats,
				'totalseats' => $this_class['numofstudents'],
				'classtitle' => $this_class['title'],
				'classtime' =>  $this_class['start_time'],
				'class_id' => $this_class['id'],
			
			];
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Reschedule Student ' ];
					
			$content = $app['templating']->render('range/class/reschedule_student.php', $vars);
			
			$pageTitle = 'Range Controller - Class List - Reschedule Student';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}
		
		function rescheduleStudentNewClass (Application $app, $student_id, $old_class_id, $new_class_id) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
		
			//$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
		
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sqlq = "DELETE FROM registrations WHERE student_id='" . $student_id . "' AND class_id='" . $old_class_id . "'";
			
			$link->query($sqlq);
			
			$sqlq = "INSERT INTO registrations (student_id, class_id) VALUES ('" . $student_id . "', '" . $new_class_id . "')";
		
			$link->query($sqlq);
		
		}
		
		function registrationList (Application $app, $class_id) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
		
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
		
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$query_sql = "SELECT * FROM classes WHERE accountnumber='" . $accno . "' AND id='" . $class_id . "'";
    
			//return $insert_sql;
	
			$assoc_array = DbHelper::dbFetchRowAssoc($link, $query_sql);
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Registration List ' ];
			
			$dateArr = explode("-", $assoc_array['start_date']);
				
			$parsedDate = $dateArr[1] . "/" .  $dateArr[2] . "/" . $dateArr[0];
				
			$query_sql = "SELECT * FROM registrations WHERE class_id='" . $class_id . "'";	
			
			$all_registrations = DbHelper::dbFetchAllAssoc($link, $query_sql);
				
			$reg_list = "";
				
			foreach ($all_registrations as $reg) {

				$sqlq = "SELECT customers.membership_number, customers.first_name, customers.last_name, customers.email, customers.home_phone, student_data.amount_paid, student_data.drivers_license FROM customers INNER JOIN student_data ON customers.membership_number=student_data.membership_number WHERE customers.membership_number='" . $reg['student_id'] . "'" ;

				$temp = DbHelper::dbFetchRowAssoc($link, $sqlq);
				
				$reg_list = $reg_list . '<tr>
			<td nowrap="">' . $temp['first_name'] . ' ' . $temp['last_name'] . '</td>
			<td style="font-size:14px;width:80px;">' . strtolower($temp['email']) . '</td>
			<td nowrap="">' . $temp['home_phone'] . '</td>
			<td class="text-center">' . $temp['amount_paid'] . '</td>
			<td class="text-center">' . $temp['drivers_license'] . '</td>
			<td class="text-center"><a href="../' . $class_id . '/manageStudent/' . $reg['student_id'] . '">Manage</a></td>
			<td class="text-center"><a href="../' . $class_id . '/rescheduleStudent/' . $temp['membership_number'] . '">Reschedule</a></td>
			<td class="text-center"><a href="#" onclick="myConfirm(\'Are you sure you want to delete this student?\', \'' . $reg['student_id'] . '\')">Delete</a></td>
			</tr>';

			}
				
			/* <tr>
			<td nowrap="">Terry Hassler</td>
			<td nowrap="" style="font-size:14px;">terry@davespawnshop.com </td>
			<td nowrap="">123-123-1234 </td>
			<td class="text-center">60</td>
			<td class="text-center">12345678</td>
			<td class="text-center"><a href="manageStudent">Manage</a></td>
			<td class="text-center"><a href="rescheduleStudent">Reschedule</a></td>
			<td class="text-center"><a href="">Delete</a></td>
			</tr>

			*/
			
				$sqql = "SELECT * FROM classes WHERE id='" . $class_id . "'";
				
				$tmp = DbHelper::dbFetchRowAssoc($link, $sqql);
				
				$cnt_sql = "SELECT COUNT(*) AS counted FROM registrations WHERE class_id='" . $class_id . "'";
			
				$cnter = DbHelper::dbFetchRowAssoc($link, $cnt_sql);
			
				$tkenseats = $cnter['counted'];
			
				$seats = ((int) $tmp['numofstudents']) - ((int)$tkenseats);
			
			$vars = [
				'class_no' => $class_id,
				//'errmsg' => $err,
				'class_title' => $assoc_array['title'],
				'start_time' => $assoc_array['start_time'],
				'parsed_date' => $parsedDate,
				'reg_list' => $reg_list,
				'available' => $seats,
				'totalseats' => $tmp['numofstudents'],
			];
			
			$content = $app['templating']->render('range/class/registration_list.php', $vars);
			
			$pageTitle = 'Range Controller - Class List - Registration List';
			
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}
		
		function addInstructor (Application $app) {
		
			$breadcrumbs = $this->getBreadcrumbs($app);
			
            $breadcrumbs[] = ['title' => 'Add Instructor ' ];
			
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Please fill out all the required fields * "]);
			
			$vars = [
                'errmsg' => $err,
            ];
					
			$content = $app['templating']->render('range/class/add_instructor.php', $vars);
			
			$pageTitle = 'Range Controller - Class List - Add Instructor';
					
			return $this->wrapInLayout($app, compact('content', 'pageTitle', 'breadcrumbs'));
		
		}

		
		
		function insertInstructor (Application $app, $instructor_no, $fname, $lname, $email, $class_add, $range_add, $school_name, $school_no) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$insert_sql = "INSERT INTO instructors (acctnumberhashed, accountnumber, instructor_number, first_name, last_name, email_address, class_full_address, range_full_address, school_name, school_number) VALUES ('" .  $hash . "', '" . $accno . "', '" . $instructor_no . "', '" . $fname . "', '" . $lname . "', '" . $email . "', '" . $class_add . "', '" . $range_add . "', '" . $school_name . "', '" . $school_no . "')";
    
			//return $insert_sql;
	
			$link->query($insert_sql);
			
		}
	
		function updateInstructor (Application $app, $old_instructor_no, $new_instructor_no, $fname, $lname, $email, $class_add, $range_add, $school_name, $school_no) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$delete_sql = "DELETE FROM instructors WHERE instructor_number='" . $old_instructor_no . "' AND acctnumberhashed='" . $hash . "'";
			
			$link->query($delete_sql);
			
			$insert_sql = "INSERT INTO instructors (acctnumberhashed, accountnumber, instructor_number, first_name, last_name, email_address, class_full_address, range_full_address, school_name, school_number) VALUES ('" .  $hash . "', '" . $accno . "', '" . $new_instructor_no . "', '" . $fname . "', '" . $lname . "', '" . $email . "', '" . $class_add . "', '" . $range_add . "', '" . $school_name . "', '" . $school_no . "')";
    
			//return $insert_sql;
	
			$link->query($insert_sql);
			
		}
		
		function deleteInstructor (Application $app, $instructor_no) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$delete_sql = "DELETE FROM instructors WHERE instructor_number='" . $instructor_no . "' AND acctnumberhashed='" . $hash . "'";
			
			$link->query($delete_sql);
			
			return '<script>window.location = "/xapp/classes/instructors";</script>';
			
		}
		
		function deleteClass (Application $app, $class_id) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$delete_sql = "DELETE FROM classes WHERE id='" . $class_id . "' AND acctnumberhashed='" . $hash . "'";
			
			$link->query($delete_sql);
			
			return '<script>window.location = "/xapp/classes";</script>';
			
		}
		
		function insertClass (Application $app) {
		
			$title = $app['request']->get('title', '');
		
			$nos = $app['request']->get('nos', '');
			
			$cost = $app['request']->get('cost', '');
			
			$startd = $app['request']->get('startd', '');
			
			$endd = $app['request']->get('endd', '');
			
			$startt = $app['request']->get('startt', '');
			
			$endt = $app['request']->get('endt', '');
			
			$instructor_id = $app['request']->get('instructor_id', '');
			
			$description = $app['request']->get('description', '');
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				
			$select_sql = "SELECT * FROM instructors WHERE instructor_number='" . $instructor_id . "' AND accountnumber='" . $accno . "'";
			
			$ins_array = DbHelper::dbFetchRowAssoc($link, $select_sql);
			
			$instructor_name = $ins_array['first_name'] . " " . $ins_array['last_name'];
				
			$startd = str_replace("%2F", "/", $startd);
			
			$endd = str_replace("%2F", "/", $endd);
			
			$start_date_array = explode("/", $startd);
			
			$end_date_array = explode("/", $endd);
			
			$sql_start_date = $start_date_array[2] . "-" . $start_date_array[0] . "-" . $start_date_array[1];
			
			$sql_end_date = $end_date_array[2] . "-" . $end_date_array[0] . "-" . $end_date_array[1];
			
			$description = str_replace("%20", " ", $description);
			
			$description = str_replace("%0A", "\n", $description);
			
			$insert_sql = "INSERT INTO classes (acctnumberhashed, accountnumber, title, numofstudents, cost, start_date, end_date, start_time, end_time, instructor_number, instructor_name, description) VALUES ('" .  $hash . "', '" . $accno . "', '" . $title . "', '" . $nos . "', '" . $cost . "', '" . $sql_start_date . "', '" . $sql_end_date . "', '" . $startt . "', '" . $endt . "', '" . $instructor_id . "', '" . $instructor_name . "', '" . $description . "')";
    
			//return $insert_sql;
	
			$link->query($insert_sql);
			
		}
		
		function updateClass (Application $app, $class_id) {
			
			$title = $app['request']->get('title', '');
		
			$nos = $app['request']->get('nos', '');
			
			$cost = $app['request']->get('cost', '');
			
			$startd = $app['request']->get('startd', '');
			
			$endd = $app['request']->get('endd', '');
			
			$startt = $app['request']->get('startt', '');
			
			$endt = $app['request']->get('endt', '');
			
			$instructor_id = $app['request']->get('instructor_id', '');
			
			$description = $app['request']->get('description', '');
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				
			$select_sql = "SELECT * FROM instructors WHERE instructor_number='" . $instructor_id . "' AND accountnumber='" . $accno . "'";
			
			$ins_array = DbHelper::dbFetchRowAssoc($link, $select_sql);
			
			$instructor_name = $ins_array['first_name'] . " " . $ins_array['last_name'];
				
			$startd = str_replace("%2F", "/", $startd);
			
			$endd = str_replace("%2F", "/", $endd);
			
			$start_date_array = explode("/", $startd);
			
			$end_date_array = explode("/", $endd);
			
			$sql_start_date = $start_date_array[2] . "-" . $start_date_array[0] . "-" . $start_date_array[1];
			
			$sql_end_date = $end_date_array[2] . "-" . $end_date_array[0] . "-" . $end_date_array[1];
			
			$description = str_replace("%20", " ", $description);
			
			$description = str_replace("%0A", "\n", $description);
			
			$delete_sql = "DELETE FROM classes WHERE id='" . $class_id . "'";
			
			$link->query($delete_sql);
			
			$insert_sql = "INSERT INTO classes (id, acctnumberhashed, accountnumber, title, numofstudents, cost, start_date, end_date, start_time, end_time, instructor_number, instructor_name, description) VALUES ('" . $class_id . "', '" . $hash . "', '" . $accno . "', '" . $title . "', '" . $nos . "', '" . $cost . "', '" . $sql_start_date . "', '" . $sql_end_date . "', '" . $startt . "', '" . $endt . "', '" . $instructor_id . "', '" . $instructor_name . "', '" . $description . "')";
    
			//return $insert_sql;
	
			$link->query($insert_sql);
			
		}
		
		function insertStudent (Application $app, $class_id) {
		
			$fname = $app['request']->get('fname', '');
		
			$lname = $app['request']->get('lname', '');
			
			$email = $app['request']->get('email', '');
			
			$address = $app['request']->get('address', '');
			
			$city = $app['request']->get('city', '');
			
			$state = $app['request']->get('state', '');
			
			$zipcode = $app['request']->get('zipcode', '');
			
			$phone = $app['request']->get('phone', '');
			
			$emergency = $app['request']->get('emergency', '');
			
			$amountpaid = $app['request']->get('amountpaid', '');
			
			$license = $app['request']->get('license', '');
		
			$employee_id = $app['request']->get('employee_id', '');
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			/*	
			$select_sql = "SELECT * FROM customers WHERE acctnumberhashed='" . $hash . "' ORDER BY membership_number DESC";
			
			$ins_array = DbHelper::dbFetchRowAssoc($link, $select_sql);
			
			$memberMax = (int) $ins_array['membership_number'];
			
			$newmemid = ++$memberMax;
			*/
			/* 
			
			$fields = ['firstName', 'lastName', 'middleName', 
                'street', 'city', 'state', 'zipcode', 
                'ssn', 'phone1', 'phone2', 'email',
                'emergencyName', 'emergencyPhone', 
                'membershipType', 'memRegistrationDate', 
                'memExpirationDate'];
				
				*/
				
			$regDate = date('m/d/Y');
				
			$args = [
			
			'firstName' => $fname,
			'lastName' => $lname,
			'middleName' => '',
			'street' => $address,
			'city' => $city,
			'state' => $state,
			'zipcode' => $zipcode,
			'ssn' => '',
			'phone1' => $phone,
			'email' => $email,
			'membershipType' => 'Student',
			'emergencyName' => '',
			'emergencyPhone' => '',
			'memRegistrationDate' => $regDate,
			'memExpirationDate' => '',
			'phone2' => '',
			'emp_id' => $employee_id,
			
			];
			
			$newmemid = range_member::createNewMember($args, 
                            $app['dbs']['main'] , $app['range_owner']);
			
			//$insert_sql = "INSERT INTO customers (acctnumberhashed, accountnumber, membership_number, first_name, last_name, email, street_address, city, state, zipcode, home_phone) VALUES ('" . $hash . "', '" . $accno . "', '" . $newmemid . "', '" . $fname . "', '" . $lname . "', '" . $email . "', '" . $address . "', '" . $city . "', '" . $state . "', '" . $zipcode . "', '" . $phone . "')";
			
			//$link->query($insert_sql);
			
			$insert_sql = "INSERT INTO student_data (acctnumberhashed, membership_number, emergency_no, amount_paid, drivers_license) VALUES ('" . $hash . "', '" . $newmemid . "', '" . $emergency . "', '" . $amountpaid . "', '" . $license . "')";  
		
			$link->query($insert_sql);
			
			$insert_sql = "INSERT INTO registrations (class_id, student_id) VALUES ('" . $class_id . "', '" . $newmemid . "')";
			
			$link->query($insert_sql);
			
			$insert_sql = "INSERT INTO memberships (accountnumber, acctnumberhashed, membership_number, membership_type, membership_date) VALUES ('" . $accno . "', '" . $hash . "', '" . $newmemid . "', 'Student')";
			
			$link->query($insert_sql);
			
			$insert_sql = "INSERT INTO driver_license_info (accountnumber, acctnumberhashed, membership_number, license_number) VALUES ('" . $accno . "', '" . $hash . "', '" . $newmemid . "', '" . $license . "')";
			
			$link->query($insert_sql);
		
		}
		
		function updateStudent (Application $app, $class_id, $student_id) {
		
			$fname = $app['request']->get('fname', '');
		
			$lname = $app['request']->get('lname', '');
			
			$email = $app['request']->get('email', '');
			
			$address = $app['request']->get('address', '');
			
			$city = $app['request']->get('city', '');
			
			$state = $app['request']->get('state', '');
			
			$zipcode = $app['request']->get('zipcode', '');
			
			$phone = $app['request']->get('phone', '');
			
			$emergency_phone = $app['request']->get('emergency', '');
			
			$amount_paid = $app['request']->get('amountpaid', '');
			
			$license = $app['request']->get('license', '');
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$update_sql = "UPDATE customers INNER JOIN student_data ON customers.membership_number=student_data.membership_number SET customers.first_name='" . $fname . "', customers.last_name='" . $lname . "', customers.email='" . $email . "', customers.street_address = '" . $address . "', customers.city = '" . $city . "', customers.state = '" . $state . "', customers.zipcode='" . $zipcode . "', customers.home_phone='" . $phone . "', student_data.emergency_no='" . $emergency_phone . "', student_data.amount_paid='" . $amount_paid . "', student_data.drivers_license='" . $license . "' WHERE customers.membership_number='" . $student_id . "'";
		
			$link->query($update_sql);
		
			//$insert_sql = "INSERT INTO customers (acctnumberhashed, accountnumber, membership_number, first_name, last_name, email, street_address, city, state, zipcode, home_phone) VALUES ('" . $hash . "', '" . $accno . "', '" . $newmemid . "', '" . $fname . "', '" . $lname . "', '" . $email . "', '" . $address . "', '" . $city . "', '" . $state . "', '" . $zipcode . "', '" . $phone . "')";
			
			//$link->query($insert_sql);
		
		}
		
		function addCustomerToClass (Application $app, $class_id, $customer_id) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$sqlq = "INSERT INTO registrations (class_id, student_id) VALUES('" . $class_id . "', '" . $customer_id . "')";
			
			$link->query($sqlq);
			
			$sqlq = "INSERT INTO student_data (acctnumberhashed, membership_number) VALUES('" . $hash . "', '" . $customer_id . "')";
			
			$link->query($sqlq);
		
		}
		
		function removeCustomerFromClass (Application $app, $class_id, $customer_id) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$sqlq = "DELETE FROM registrations WHERE class_id='" . $class_id . "' AND student_id='" . $customer_id . "'";
			
			$link->query($sqlq);
		
		}
		
		function checkForDuplicateMember (Application $app, $first_name, $last_name, $address) {
		
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		
			$sqlq = "SELECT * FROM customers WHERE acctnumberhashed='" . $hash . "' AND first_name = '" . $first_name . "' AND last_name='" . $last_name . "' AND street_address='" . $address . "'";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sqlq);
			
			$namematchcount = 0;
			
			foreach ($assoc_array as $temp) {
			
				$namematchcount++;
			
			}
			/*
			$sqlq = "SELECT * FROM customers WHERE acctnumberhashed='" . $hash . "' AND street_address = '" . $address ."'";
			
			$assoc_array2 = DbHelper::dbFetchAllAssoc($link, $sqlq);
			
			$addressmatchcount = 0;
			
			foreach ($assoc_array2 as $temp2) {
			
				$addressmatchcount++;
			
			}
			
			*/
			
			if ($namematchcount > 0) return "name and address";
			
			else return "NONE";
			
			//$sqlq = "INSERT INTO student_data (acctnumberhashed, membership_number) VALUES('" . $hash . "', '" . $customer_id . "')";
			
			//$link->query($sqlq);
		
		}
			
	}
	
}
