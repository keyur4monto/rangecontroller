<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    //include('//includes/browser.php');
    
    class SpecialeventController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
            //$pf = $app['range_url_prefix'];
            //return $app['request']->get('cmd', 'aa') .print_r($pf,1) ;            
            return $this->listMembers($app);
        }
				
		public function listMembers(Application $app){
            $roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();

			$special_event = $app['special_event']; // \Rc\Models\NewsletterSubcriber::initWithAccount($link, $accHashKey, $accNo);//
           
            $pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
            $records= $special_event->getSpecialevent(array(), $pager);
			//filter
				$emp=$app['request']->get('filter_employee');
				$emp=(!empty($emp)) ? $emp : false;
				$dt=$app['request']->get('filter_date');
				$dt=(!empty($dt)) ? $dt : false;
				$fm=$app['request']->get('filter_month');
				$fm=(!empty($fm)) ? $fm : false;
				$fy=$app['request']->get('filter_year');
				$fy=(!empty($ft)) ? $ft : false;
			//
            $records= $special_event->getSpecialevent(array('employee_id'=>$emp,'timestamp'=>$dt,'month'=>$fm,'year'=>$fy), $pager);
            $getemplists = $special_event->getemplist();	
			
			$data = array(
				'allnewsletter' =>  $records,                 
                'addMemberUrl' => $app['get_range_url']('customers/add'),
                'pageSize' => $this->pageSize,
                'getemplists'=>$getemplists,
				'emp'=>$emp,
				'dt'=>$dt,
				'fm'=>$fm,
				'fy'=>$fy,
            );            
            
            $content = $app['templating']->render('range/specialevent/list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Special Events';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
        
        public function getfilterDataJson(Application $app){        	
			//filter
				$emp=$app['request']->get('filter_employee');
				$emp=(!empty($emp)) ? $emp : false;
				$dt=$app['request']->get('filter_date');				
				$ndt=(!empty($dt)) ? $dt : false;
				$fm=$app['request']->get('filter_month');
				$fm=(!empty($fm)) ? $fm : false;
				$fy=$app['request']->get('filter_year');
				$fy=(!empty($fy)) ? $fy : false;
				$data =	array(
						'employee_id'=>$emp,
						'timestamp'=>$ndt,
						'month'=>$fm,
						'year'=>$fy
					);				
			//
			$records =  $app['special_event']->getSpecialEventjson($data);			
        }
        
		private function getBreadcrumbs(Application $app){
            return array(
                array('link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'),
                array('link' => $app['get_range_url']('specialEvents'), 'title'=>'Special Events')
            );
        }
		
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            
            $breadcrumbsContent = $app['templating']->render(
                    'range/newsletter/breadcrumbs.php', 
                    array('breadcrumbs' => $data['breadcrumbs']));
            
            
            $vars = array(
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            );
            return $app['templating']->render('range/layout-range.php', $vars);
        } 
		
		public function addSpecialevent(Application $app) {
         //   $contactFields = array('firstName', 'middleName', 'lastName', 
               // 'street', 'city', 'state', 'zipcode',
              //  'ssn', 'phone1', 'phone2', 'email');
            //$contactData = $this->getPostForFields($contactFields, $app['request']); 
         //   $contactData['stateList'] = Services\CommonData::getStates();
                       
         //   $emergencyFields = array('emergencyName', 'emergencyPhone');
            //$emergencyData = $this->getPostForFields($emergencyFields, $app['request']);
            
          //  $membershipFields = array('membershipType', 'memRegistrationDate', 
               // 'memExpirationDate', 'employee_id');
            //$membershipData = $this->getPostForFields(
                    //$membershipFields, $app['request']);
       //     $membershipData['addMemberTypeUrl'] = $app['get_range_url'](
                  //      'common/config.php#createnow');
       //     $connhandle = Services\DbHelper::getWrappedHandle($app['dbs']['main']);
        //    $memtypes = Models\MembershipType::getMembershipTypesForAccount(
            //        $connhandle , $app['range_owner_id']);
       //     $membershipData['membershipTypes'] = $memtypes;
            
          $errorMessage = '';
            
		//	$pageTokenIdKey = 'addMemberPageToken';
            
			//$emp_id = $membershipData['employee_id'];
			
            if ($app['request']->get('cmd', '') === 'add') {
               	
				$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				$member_type = '';
				$everyone    = '';
				$memberkey   = '';
				$memberval   = '';
				$everyonekey = '';
				$everyoneval = '';
				
				
				$employee_id  = $_POST['employeename'];
				$date  = $_POST['date'];
				$start_time     = $_POST['start_time'];			
				$end_time    = $_POST['end_time'];
				$event_name      = $_POST['event_name'];
				$message_body = $_POST['message_body'];			
				
				$roid = $app['range_owner_id'];
				$accno = $roid->getAccountNo();	
				$hash = $roid->getAccountHashKey();
				if(empty($employee_id)  || empty($date) || empty($start_time) || empty($end_time) || empty($event_name))
					$errorMessage="Please fill out all of the required fields marked with *";
                if (!$errorMessage) {
					
					$sql_ins = "INSERT INTO special_events(acctnumberhashed, accountnumber, emp_id, date, event_starttime, event_endtime, event_name, event_description) VALUES ('$hash', '$accno', '$employee_id', '$date', '$start_time', '$end_time', '$event_name', '$message_body')";			
					//$sql_ins = "INSERT INTO new_newsletter(emp_id,timestamp, membership_type, subject, email, content, everyone) VALUES ('".$employee_id."','".$timestamp."','".$member_type."','".$subject."','".$receivers."','".$message_body."','".$everyone.")";
					$link->query($sql_ins);
					return $app->redirect($app['get_range_url']('specialEvents'));
				}
            }
            
            $newsletter_subcriber = $app['special_event']; // \Rc\Models\NewsletterSubcriber::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            $mrecords = $newsletter_subcriber->getmemberslist();
            $getemplists = $newsletter_subcriber->getemplist();
            
			$data = array(
                'errorMessage' => $errorMessage,
                'addMemberPostUrl' => $app['get_range_url']('customers/1234'),
                //'membershipTypeData' =>  $membershipData,
                'getemplists' => $getemplists,
                //'contactData' => $contactData,
                'mrecordstype' => $mrecords,
               // 'tokenValue' => $app['session']->get($pageTokenIdKey),
                //'tokenName' => $pageTokenIdKey
            );
            $content = $app['templating']->render('range/specialevent/addnew.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[] = array('title' => 'Add' );
			$pageTitle = 'Range Controller - Special Events - Add';
            return $this->wrapInLayout($app, compact('content', 'breadcrumbs', 'pageTitle'));
        }  
		
		public function getGroupMembersJson(Application $app){
			$pos = array();
			$pos = $_POST;
			$pagedResults =  $app['newsletter_subcriber']->getgrouomemberslist($pos);
			//return $app->json($pagedResults->toArray());
        }
		
		public function addNewSpecialevent(Application $app){
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$member_type = '';
			$everyone    = '';
			$memberkey   = '';
			$memberval   = '';
			$everyonekey = '';
			$everyoneval = '';
			
			
			$employee_id  = $_POST['employeename'];
			$date  = $_POST['date'];
			$start_time     = $_POST['start_time'];			
       		$end_time    = $_POST['end_time'];
			$event_name      = $_POST['event_name'];
			$message_body = $_POST['message_body'];			
			
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			if($member_type != ''){				
				$memberkey = 'membership_type,';
				$memberval = ", '$member_type'";				
			}
			if($everyone != ''){				
				$everyonekey = ', everyone';
				$everyoneval = ", '$everyone'";				
			}
			$sql_ins = "INSERT INTO special_events(acctnumberhashed, accountnumber, emp_id, date, event_starttime, event_endtime, event_name, event_description) VALUES ('$hash', '$accno', '$employee_id', '$date', '$start_time', '$end_time', '$event_name', '$message_body')";			
  			//$sql_ins = "INSERT INTO new_newsletter(emp_id,timestamp, membership_type, subject, email, content, everyone) VALUES ('".$employee_id."','".$timestamp."','".$member_type."','".$subject."','".$receivers."','".$message_body."','".$everyone.")";
   			$link->query($sql_ins);
   			return $app->redirect($app['get_range_url']('specialEvents'));				
		}
		
		public function editSpecialevent(Application $app, $specialeventNo){
			//$gdata = array();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$membershiptype = "SELECT * FROM special_events WHERE event_id = $specialeventNo";
			$memtype_list = $link->query($membershiptype);
			//return DbHelper::dbFetchRowAssoc($this->link, $sql);
			$special_event = $app['special_event']; // \Rc\Models\NewsletterSubcriber::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            //$gdata = $memtype_list->fetch_assoc();
			$mrecords = $special_event->getmemberslist();
			$getemplists = $special_event->getemplist();
			 $errorMessage = '';
            if ($app['request']->get('cmd', '') === 'edit') {
				 $link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$member_type = '';
			$everyone    = '';
			$memberkeyval   = '';			
			$everyonekeyval = '';	
			
			$date  = $_POST['date'];
			$start_time     = $_POST['start_time'];			
       		$end_time    = $_POST['end_time'];
			$event_name      = $_POST['event_name'];
			$message_body = $_POST['message_body'];	
			$event_id = $_POST['event_id'];
			
			
				if(empty($date) || empty($start_time) || empty($end_time) || empty($event_name))
					$errorMessage="Please fill out all of the required fields marked with *";
                if (!$errorMessage) {	
			$sql_ins = "UPDATE special_events SET date = '$date' $memberkeyval, event_starttime = '$start_time', event_endtime = '$end_time', event_name = '$event_name', event_description = '$message_body' $everyonekeyval WHERE event_id = $event_id";  			
   			$link->query($sql_ins);		
					//done with email 
					return $app->redirect($app['get_range_url']('specialEvents'));
				}
			 }
			$data = array(    
				'errorMessage' => $errorMessage,                
                'memberAddUrl' => $app['get_range_url']('specialevent/addnew'),
                'member' => $memtype_list,
                'getemplists' => $getemplists,                
                'mrecordstype' => $mrecords,             
            );
			
            $content = $app['templating']->render('range/specialevent/edit.php', $data);
			
			
			$breadcrumbs = $this->getBreadcrumbs($app);
            $title = 'Manage ';
            $breadcrumbs[] = array('title' => $title);
            //$pageTitle = 'Range Controller - ' . $title;
			$pageTitle = 'Range Controller - Special Events - Manage';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));			
				
		}
		
		public function editNewSpecialevent(Application $app){			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$member_type = '';
			$everyone    = '';
			$memberkeyval   = '';			
			$everyonekeyval = '';	
			
			$date  = $_POST['date'];
			$start_time     = $_POST['start_time'];			
       		$end_time    = $_POST['end_time'];
			$event_name      = $_POST['event_name'];
			$message_body = $_POST['message_body'];	
			$event_id = $_POST['event_id'];
			
			if($member_type != ''){				
				$memberkeyval = ", membership_type  = '$member_type'";								
			}
			if($everyone != ''){				
				$everyonekeyval = ", everyone = '$everyone'";				
			}
						
			$sql_ins = "UPDATE special_events SET date = '$date' $memberkeyval, event_starttime = '$start_time', event_endtime = '$end_time', event_name = '$event_name', event_description = '$message_body' $everyonekeyval WHERE event_id = $event_id";  			
   			$link->query($sql_ins);			   
   			return $app->redirect($app['get_range_url']('specialevent'));				
		}
		public function deleteSpecialevent (Application $app, $specialeventNo){			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']); 
			$sql_ins = "DELETE FROM `special_events` WHERE event_id = $specialeventNo";			  			
   			$link->query($sql_ins);
   			return $app->redirect($app['get_range_url']('specialEvents'));				
		}
		       
    }
}
