<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class SchedulerController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->layoutShooterPath = 'layout_shooter.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
            //$pf = $app['range_url_prefix'];
            //return $app['request']->get('cmd', 'aa') .print_r($pf,1) ;
            
            return $this->indexPage($app);
        }
		
		private function getBreadcrumbs(Application $app){
            return array(
                ['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('').'maintenance-tasks.php', 'title'=>'Maintenance Tasks'],
            );
        }
		
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $member_name = "";
            
            if (isset($_SESSION['member_number'])) {
            
            	$roid = $app['range_owner_id'];
			
				$accno = $roid->getAccountNo();	

				$hash = $roid->getAccountHashKey();
			
				$link = DbHelper::getWrappedHandle($app['dbs']['main']);
            
            	$sql = "SELECT * FROM customers WHERE accountnumber='" . $accno . "' AND membership_number='" . $_SESSION['member_number'] . "'";
            
            	$cust = DbHelper::dbFetchRowAssoc($link, $sql);
            	
            	$member_name = $cust['first_name'] . " " . $cust['last_name'];
            	
            }
            
            $breadcrumbsContent = $app['templating']->render('range/customer/breadcrumbs.php', ['breadcrumbs' => $data['breadcrumbs']]);

            $vars = [
                'pageTitle' => isset($data['pageTitle']) ? $data['pageTitle'] : 'Range Controller',
                'memberName' => $member_name,
				'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
        }
        private function wrapInLayoutShooter(Application $app, $data ){
            $ro = $app['shooter_owner'];
            //$ro = new Models\RangeOwner;
            $member_name = "";
            
            if (isset($_SESSION['member_number'])) {
            
            	$roid = $app['shooter_owner_id'];
			
				$accno = $roid->getAccountNo();	

				$hash = $roid->getAccountHashKey();
			
				$link = DbHelper::getWrappedHandle($app['dbs']['main']);
            
            	$sql = "SELECT * FROM customers WHERE accountnumber='" . $accno . "' AND membership_number='" . $_SESSION['member_number'] . "'";
            
            	$cust = DbHelper::dbFetchRowAssoc($link, $sql);
            	
            	$member_name = $cust['first_name'] . " " . $cust['last_name'];
            	
            }
            
            $breadcrumbsContent = $app['templating']->render('range/customer/breadcrumbs.php', ['breadcrumbs' => $data['breadcrumbs']]);

            $vars = [
                'pageTitle' => isset($data['pageTitle']) ? $data['pageTitle'] : 'Range Controller',
                'memberName' => $member_name,
				'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
				'rangeLogoUrl' => $app['shooter_logo_url']
            ];
            return $app['templating']->render('shooter/layout-shooter2.php', $vars);
        }
        function schedulerMain (Application $app) {
        
        	$sdate = $app['request']->get('scheduled', '' . date("m/d/Y") . '');
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			//$date = date("m/d/Y");//get from post!
			
			$sql = "SELECT * FROM controller_config WHERE accountnumber='" . $accno . "'";
			
			$config = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$numOfLanes = $config['number_of_lanes'];
			
			$sql = "SELECT * FROM new_scheduler 
			INNER JOIN customers ON new_scheduler.membership_number = customers.membership_number 
			INNER JOIN memberships ON customers.membership_number = memberships.membership_number 
			WHERE new_scheduler.accountnumber='" . $accno . "' AND customers.accountnumber='" . $accno . "' 
			AND STR_TO_DATE(  new_scheduler.date_stamp,  '%m/%d/%Y' ) = STR_TO_DATE(  '" . $sdate . "',  '%m/%d/%Y' )
			ORDER BY start_time
			"; 
			// GROUP BY new_scheduler.membership_number";
			
			//print "\n" . '<!-- mytesting  ' . "\n";
			//print $sql;
			//print "\n" . '-->';
			//return $sql;

			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$timeslotArray = array();//each td goes here
			
            $schedulerRows =  "";
            
            $arr = $this->getHoursForToday($app, $sdate);
            
            $openClose = false;
            
            $alternator = false;
            
            foreach ($arr as $time) {
            
            	$openClose = true;
            
            	$timeslotArray = array("<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>", "<td class='nicktd'></td>");
            	
            	$counter = 0;
            	
            	//return "HERE";

            	$membership_numbers = array();
            	
            	foreach ($assoc_array as $booking) {

            	  //
            	  // Update begin
            	  //
            	  $membership_number_key = $booking['membership_number'] . ' :: ' . $booking['start_time'];

            	  //
            	  // Not more than just one record per each "membership_number :: start_time" combination.
            	  //
                if ( array_key_exists($membership_number_key, $membership_numbers)) {
                
                  continue;
                }
            	
            		if ($this->doesTimeFit($app, $booking['start_time'], $booking['end_time'], $time)) {

            		  $membership_numbers[$membership_number_key] = 1;
            		
            			$timeslotArray[$counter] = '';
            		
            			//for ($i = 0;$i< $booking['number_of_lanes'];$i++) {
            			
            				$timeslotArray[$counter] = '<td class="nicktd"><a title="Name : ' . $booking['first_name'] . ' ' . $booking['last_name'] . '

Membership Number : ' . $booking['membership_number'] . '

Membership Type : ' . $booking['membership_type'] . '

Start Time : ' . $booking['start_time'] . '

End Time : ' . $booking['end_time'] . '" href="rcScheduler/manage/' . $booking['id'] . '">' . $booking['first_name'] . ' ' . $booking['last_name'] . '</a></td>';
            			
            			//}
            			
            			$counter++;//change colspan to 2 and up it by 2 instead (maybe)
            			
            			if ((int)$booking['number_of_lanes'] == 2) {
            			
            			$timeslotArray[$counter] = '<td class="nicktd"><a href="rcScheduler/manage/' . $booking['id'] . '">' . $booking['first_name'] . ' ' . $booking['last_name'] . '</a></td>';
            			
            			$counter++;
            			
            			}
            		
            		}
            	
            	}
            	
            	if ($alternator) $color_hex = "303030";
            	
            	else $color_hex = "000000";
            	
            	$alternator = !$alternator;
            	
            	$alternator2 = false;
            
            	$schedulerRows = $schedulerRows . '<tr style="background-color:#' . $color_hex . ';">
                <td class="timeCol nicktd" rowspan="' . (ceil(($numOfLanes / 5))) . '" style="width: 10%;background-color: #' . $color_hex . ';">' . $time . '</td>';
            
            	for ($j = 0;$j < $numOfLanes;$j++) {
            	
            		if ($j > 0 && ($j % 10) == 5)  $schedulerRows = $schedulerRows . '</tr><tr style="background-color: #' . $color_hex . ';">';
            		
            		else if ($j > 0 && ($j % 10) == 0) $schedulerRows = $schedulerRows . '</tr><tr style="background-color: #' . $color_hex . ';">';
            		
                  	$schedulerRows = $schedulerRows . $timeslotArray[$j];
                
                }
                
                if (($j % 5) != 0) $schedulerRows = $schedulerRows . '<td style="background-color: #' . $color_hex . ';" colspan="' . (5 - ($j % 5)) . '"></td>';
                
                $schedulerRows = $schedulerRows . '</tr>';
            
            }
        	
        	if (!$openClose) $schedulerRows = '<tr><td style="width:110%;color:red;" colspan =""><b>WE ARE CLOSED TODAY</b></td></tr>';
        	
            $data = [
            	'schedulerRows' => $schedulerRows,
            	'sdate' => $sdate,
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/scheduler/main.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
			$breadcrumbs[1]['title'] = 'Scheduler';
			//$breadcrumbs[2]['title'] = 'Login Select Lane';
            $pageTitle = 'Range Controller - Scheduler';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}


		function schedulerMainShooter (Application $app) {
       
        	//$range_member = $app['range_member']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
            if (!isset($_SESSION['member_number'])) return "<script>window.location='https://www.rangecontroller.com'</script>";
                        			
			//$roid = $app['range_owner_id'];
			
			//$accno = $roid->getAccountNo();	

			//$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$date = date("m/d/Y");//get from post!
			
			$sql = "SELECT * FROM new_scheduler WHERE accountnumber='" . $_SESSION['an'] . "' AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . date("m/d/Y") . "',  '%m/%d/%Y' ) AND membership_number='" . $_SESSION['member_number'] . "'";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$upcomingRows = '';
			
			foreach ($assoc_array as $row) {
			
				$upcomingRows = $upcomingRows . '<tr>
                  <td>' . $row['date_stamp'] . '</td>
                  <td>' . $row['start_time'] . '</td>
                  <td>' . $row['end_time'] . '</td>
                  <td>' . $row['number_of_lanes'] . '</td>
                  <td><a href="/xapp/members/rcScheduler/manage/' . $row['id'] . '">Manage</a></td>
                  <td><a href="#nothing" onclick="deleteThis(' . $row['id'] . ')">Delete</a></td>
                </tr>';
			
			}
			
			if ($upcomingRows == '') $upcomingRows = '<tr>
                  <td colspan="6"><span style="color: red;">No records found</span></td>
                </tr>';
                
            
            
            
            $sql = "SELECT * FROM new_scheduler WHERE accountnumber='" . $_SESSION['an'] . "' AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) < STR_TO_DATE(  '" . date("m/d/Y") . "',  '%m/%d/%Y' ) AND membership_number='" . $_SESSION['member_number'] . "'";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$previousRows = '';
			
			foreach ($assoc_array as $row) {
			
				$previousRows = $previousRows . '<tr>
                  <td>' . $row['date_stamp'] . '</td>
                  <td>' . $row['start_time'] . '</td>
                  <td>' . $row['end_time'] . '</td>
                  <td>' . $row['number_of_lanes'] . '</td>
                  <td><a href="/xapp/members/rcScheduler/manage/' . $row['id'] . '">Manage</a></td>
                  <td><a href="#nothing" onclick="deleteThis(' . $row['id'] . ')">Delete</a></td>
                </tr>';
			
			}
			
			if ($previousRows == '') $previousRows = '<tr>
                  <td colspan="6"><span style="color: red;">No records found</span></td>
                </tr>';
			
			/*<tr>
                  <td>12/21/2014</td>
                  <td>9:00 AM</td>
                  <td>10:00 AM</td>
                  <td>1</td>
                  <td><a href="edit_scheduler.html">Manage</a></td>
                  <td><a href="#">Delete</a></td>
                </tr>*/
        	
            $data = [
            	'upcomingRows' => $upcomingRows,
            	'previousRows' => $previousRows,
            ];
            
            $content = $app['templating']->render('shooter/scheduler/main.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[0]['link'] = '/xapp/members/dashboard';
            $breadcrumbs[1]['link'] = '';
			$breadcrumbs[1]['title'] = 'Scheduler';
			//$breadcrumbs[2]['title'] = 'Login Select Lane';
            $pageTitle = 'Range Controller - Scheduler';
            return $this->wrapInLayoutShooter($app,compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}

	
		
		function SchedulerAdd (Application $app) {
		
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$hrs = $this->getHoursForToday($app);
        	
            $data = [
            	'hrs' => $hrs,
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/scheduler/add.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = $app['get_range_url']('').'rcScheduler';
			$breadcrumbs[1]['title'] = 'Scheduler';
			$breadcrumbs[2]['title'] = 'Add';
            $pageTitle = 'Range Controller - Scheduler - Add';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function SchedulerAddShooter (Application $app) {
			if (!isset($_SESSION['member_number'])) return "<script>window.location='https://www.rangecontroller.com'</script>";
        	$range_owner = $app['shooter_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['shooter_owner_id'];
			
			$accno = $roid->getAccountNo();	
	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			
			$hrs = $this->getHoursForTodayShooter($app);
			
			$sql = "SELECT * FROM customers WHERE accountnumber='" . $accno . "' AND membership_number='" . $_SESSION['member_number'] . "'";

            $cust = DbHelper::dbFetchRowAssoc($link, $sql);
            	
            $member_name = $cust['first_name'] . " " . $cust['last_name'];
        	
            $data = [
            	'hrs' => $hrs,
            	'member_name' => $member_name,
            	'member_number' => $_SESSION['member_number'],
            ];
            
            $content = $app['templating']->render('shooter/scheduler/add.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[0]['link'] = '/xapp/members/dashboard';
            $breadcrumbs[1]['link'] = '/xapp/members/rcScheduler';
			$breadcrumbs[1]['title'] = 'Scheduler';
			$breadcrumbs[2]['title'] = 'Add';
            $pageTitle = 'Range Controller - Scheduler - Add';
            return $this->wrapInLayoutShooter($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function SchedulerManage (Application $app, $booking_number) {
        
        	$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM new_scheduler INNER JOIN customers ON new_scheduler.membership_number=customers.membership_number and new_scheduler.accountnumber=customers.accountnumber INNER JOIN employees ON employees.id=new_scheduler.employee_id WHERE new_scheduler.id='" . $booking_number . "'";
        	
        	$arr = DbHelper::dbFetchRowAssoc($link, $sql);
        	
        	$hrs = $this->getHoursForToday($app, $arr['date_stamp']);
        	
            $data = [
            
            	'shooter_name' => $arr['first_name'] . ' ' . $arr['last_name'],
            	'date' => $arr['date_stamp'],
            	'time_from' => $arr['start_time'],
            	'time_to' => $arr['end_time'],
            	'timestamp' => $arr['timestamp'],
            	'added_by' => $arr['fname'] . ' ' . $arr['lname'],
            	'hrs' => $hrs,
            	'booking_number' => $booking_number,
            	
			//'temp' => $range_owner,
			
            ];
            
            $content = $app['templating']->render('range/scheduler/manage.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = $app['get_range_url']('').'rcScheduler';
			$breadcrumbs[1]['title'] = 'Scheduler';
			$breadcrumbs[2]['title'] = 'Manage';
            $pageTitle = 'Range Controller - Scheduler - Manage';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		function SchedulerManageShooter (Application $app, $booking_number) {
        
        	$range_owner = $app['shooter_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['shooter_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM new_scheduler INNER JOIN customers ON new_scheduler.membership_number=customers.membership_number INNER JOIN employees ON employees.id=new_scheduler.employee_id WHERE new_scheduler.id='" . $booking_number . "'";
        	
        	$arr = DbHelper::dbFetchRowAssoc($link, $sql);
        	
        	$hrs = $this->getHoursForTodayShooter($app);
        	
            $data = [
            
            	'shooter_name' => $arr['first_name'] . ' ' . $arr['last_name'],
            	'date' => $arr['date_stamp'],
            	'time_from' => $arr['start_time'],
            	'time_to' => $arr['end_time'],
            	'timestamp' => $arr['timestamp'],
            	'added_by' => $arr['fname'] . ' ' . $arr['lname'],
            	'hrs' => $hrs,
            	'booking_number' => $booking_number,
            	
			//'temp' => $range_owner,
			
            ];
            
            $content = $app['templating']->render('shooter/scheduler/manage.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[0]['link'] = '/xapp/members/dashboard';
            $breadcrumbs[1]['link'] = '/xapp/members/rcScheduler';
			$breadcrumbs[1]['title'] = 'Scheduler';
			$breadcrumbs[2]['title'] = 'Manage';
            $pageTitle = 'Range Controller - Scheduler - Manage';
            return $this->wrapInLayoutShooter($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		
		function SchedulerPost (Application $app) {
        
        	$employee_id = $app['request']->get('employee_id', '');
        	
        	$number_of_lanes = $app['request']->get('number_of_lanes', '1');
        	
        	$date_stamp = $app['request']->get('scheduleDate', '');//validate date time here
        	
        	$start_time = $app['request']->get('startTime', '');
        	
        	$end_time = $app['request']->get('endTime', '');
        	
        	$shooter = $app['request']->get('shooter', '');
        	
        	$arr = explode(" ", $shooter);
        	
        	$membership_number = $arr[0];
        
        	$range_owner = $app['shooter_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['shooter_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "INSERT INTO new_scheduler (acctnumberhashed, accountnumber, date_stamp, start_time, end_time, number_of_lanes, membership_number, employee_id, timestamp) VALUES ('" . $hash . "', '" . $accno . "', '" . $date_stamp . "', '" . $start_time . "', '" . $end_time . "', '" . $number_of_lanes . "', '" . $membership_number . "', '" . $employee_id . "', '" . date("m/d/Y \a\\t h:i:s A") . "')";
			
			$link->query($sql);
        	
            return "DONE!";
		
		}
		
		function SchedulerPostUpdate (Application $app) {
        	
        	$date_stamp = $app['request']->get('scheduleDate', '');
        	
        	$start_time = $app['request']->get('startTime', '');
        	
        	$end_time = $app['request']->get('endTime', '');
        	
        	$booking_number = $app['request']->get('booking_number', '');
        	
        	$arr = explode(" ", $shooter);
        	
        	$membership_number = $arr[0];
        
        	$range_owner = $app['shooter_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['shooter_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "UPDATE new_scheduler SET start_time='" . $start_time ."', end_time='" . $end_time . "', date_stamp='" . $date_stamp . "' WHERE id='" . $booking_number . "'";
						
			$link->query($sql);
        	
            return "DONE";
		
		}
		
		function schedulerDelete (Application $app, $booking_number) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "DELETE FROM new_scheduler WHERE id='" . $booking_number . "'";
		
			$link->query($sql);
			
			return "<script>window.location='/xapp/rcScheduler'</script>";
		
		}
		
		function schedulerDeleteShooter (Application $app, $booking_number) {
		
			$range_owner = $app['shooter_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['shooter_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "DELETE FROM new_scheduler WHERE id='" . $booking_number . "'";
		
			$link->query($sql);
			
			return "<script>window.location='/xapp/members/rcScheduler'</script>";
		
		}
		
		function getTimeArray (Application $app, $from, $to, $inclusive) {//get all times between two provided (inclusive) - expects :00, :30 

			$switch = false; //detects when to start pushing the times into the array to be returned
			
			$returnArray = array();
		
			//$tracker = $from;
		
			$times = array("5:00 AM","5:30 AM","6:00 AM","6:30 AM","7:00 AM","7:30 AM","8:00 AM","8:30 AM","9:00 AM","9:30 AM","10:00 AM","10:30 AM","11:00 AM","11:30 AM","12:00 PM","12:30 PM","1:00 PM","1:30 PM","2:00 PM","2:30 PM","3:00 PM","3:30 PM","4:00 PM","4:30 PM","5:00 PM","5:30 PM","6:00 PM","6:30 PM","7:00 PM");
			//$times = array("5:00am","5:30am","6:00am","6:30am","7:00am","7:30am","8:00am","8:30am","9:00am","9:30am","10:00am","10:30am","11:00am","11:30am","12:00pm","12:30pm","1:00pm","1:30pm","2:00pm","2:30pm","3:00pm","3:30pm","4:00pm","4:30pm","5:00pm","5:30pm","6:00pm","6:30pm","7:00pm");
			//array length 27
			
			foreach ($times as $time) {
			
				if ($from == $time) $switch = true;
				
				if ($to == $time && !$inclusive) $switch = false;
				
				if ($switch) array_push($returnArray, $time);
				
				if ($to == $time) $switch = false;
			
			}
			
			return $returnArray;
		
		}
		
		function getHoursForToday (Application $app, $today) {
			
			//$date_stamp = $app['request']->get('today', '');
		
			$day = date("l", strtotime($today));
		
			$day = strtolower($day);
			
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			$arr = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$from = $arr[$day . "_from"];
			
			$to = $arr[$day . "_to"];
			
			$from=str_replace(" ","",$from);
			$to=str_replace(" ","",$to);
			
			$from = str_replace("a", "A", $from);
			$to = str_replace("p", "P", $to);
			
			$from = str_replace("m", "M", $from);
			$to = str_replace("m", "M", $to);
			
			$from = str_replace("A", " A", $from);
			$to = str_replace("P", " P", $to);
			//return $from . " " . $to;
			
			$returnArray = $this->getTimeArray($app, $from, $to, false);
			
			//return $from . " " . $to;
			
			return $returnArray;
		
		}
		function getHoursForTodayShooter(Application $app, $today) {
			
			//$date_stamp = $app['request']->get('today', '');
		
			$day = date("l", strtotime($today));
		
			$day = strtolower($day);
			
			$range_owner = $app['shooter_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['shooter_owner_id'];
			
			$accno = $roid->getAccountNo();	

			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			$arr = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$from = $arr[$day . "_from"];
			
			$to = $arr[$day . "_to"];
			
			$from=str_replace(" ","",$from);
			$to=str_replace(" ","",$to);
			
			$from = str_replace("a", "A", $from);
			$to = str_replace("p", "P", $to);
			
			$from = str_replace("m", "M", $from);
			$to = str_replace("m", "M", $to);
			
			$from = str_replace("A", " A", $from);
			$to = str_replace("P", " P", $to);
			//return $from . " " . $to;
			
			$returnArray = $this->getTimeArray($app, $from, $to, false);
			
			//return $from . " " . $to;
			
			return $returnArray;
		}
		function doesTimeFit (Application $app, $from, $to, $time) {
			
			$fit = false;
		
			$myArr = $this->getTimeArray($app, $from, $to, false);
		
			foreach ($myArr as $t) {
			
				if ($time == $t) $fit = true;
			
			}
			
			return $fit;
		
		}
        

	}
	
}
	