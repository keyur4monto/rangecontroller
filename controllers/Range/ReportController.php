<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    //include('//includes/browser.php');
    class ReportController {
        protected $layoutPath;
        protected $pageSize;
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
		public function landing(Application $app){
			$content = $app['templating']->render('range/reports/list.php');
            //$breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs=array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Reports']
            );
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Reports';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		public function getShooterReport (Application $app, $from, $to, $name) 
		{		
		   $from = str_replace("%2F", "/", $from);
		   $to = str_replace("%2F", "/", $to);
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT *, STR_TO_DATE( date_stamp,  '%m/%d/%Y' ) AS parsedDate FROM range_time_tracker WHERE accountnumber='" . $accno . "' AND end_time <> 0 AND end_time <> 1";
			if ($name != 'unfiltered') $sql = $sql . " AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			if ($name == 'UNFILTERED') $sql = $sql . " AND end_time='123' "; //get empty set
			$sql = $sql . " ORDER BY STR_TO_DATE(start_time, '%H:%i:%s')";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			//mem no, name, start time, end time, duration
			$shooter_rows = "";
			$count = 0;
			foreach ($assoc_array as $shooter_log) {
				
				$count++;
				$tname = $shooter_log['first_name'] . " " . $shooter_log['last_name'];
				$start_time = date("g:i a", strtotime($shooter_log['start_time']));
				$end_time = date("g:i a", strtotime($shooter_log['end_time']));
				$st_t = explode(":", $shooter_log['start_time']);
				$en_t = explode(":", $shooter_log['end_time']);
				$hrs = (int) $en_t[0] - (int) $st_t[0];
				$mins = (int) $en_t[1] - (int) $st_t[1];
				if ($mins < 0){
					$mins += 60;
					$hrs--;
					}
				if ($hrs < 0) $hrs * -1;
				$duration = $hrs . ":" . sprintf("%02s", $mins) . "";//$interval->format('%s second(s)');
				$mem_no = $shooter_log['membership_number'];
				$lane_number = $shooter_log['lane_number'];
				//now get membership type
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $mem_no . "'";
				$this_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$mem_type = $this_row['membership_type'];
				$shooter_rows = $shooter_rows . '<tr>
				  <td>' . $lane_number . '</td>
                  <td nowrap="0">' . $mem_type . '</td>
                  <td nowrap="0" style="width:30px;">' . $mem_no . '</td>
                  <td nowrap="0" style="width:150px;"><a href="/xapp/customers/' . $mem_no . '">' . $tname . '</a></td>
                  <td nowrap="0" style="width:80px;"><a href="/xapp/reports/shooter/0/0/unfiltered/' . $mem_no . '">' . $start_time . '</a></td>
                  <td nowrap="0" style="width:80px;"><a href="/xapp/reports/shooter/0/0/unfiltered/' . $mem_no . '">' . $end_time . '</a></td>
                  <td nowrap="0">' . $duration . '</td>
                  <td nowrap="0">' . $shooter_log['date_stamp'] . '</td>
                  <td nowrap="0" style="max-width:30px;">' . $shooter_log['employee_name'] . '</td>
                </tr>';
			}
             $isempty = (empty($shooter_rows)) ? true : false;  
				if ($name == 'unfiltered') $name = "UNFILTERED";
				if ($name == 'UNFILTERED') { $shooter_rows = '<tr><td colspan="9" style="text-align:center;">Please select some filters to see your results here!</td></tr>'; $isempty=true;}
            $data = [
				'shooter_rows' => $shooter_rows,
				'shooter_id' => $shooter_id,
				'report_type' => $name,
				'start_date' => $from,
				'end_date' => $to,
				'isempty'=>$isempty
            ];            
            $content = $app['templating']->render('range/reports/shooter_report.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Shooter Report';
            $pageTitle = 'Range Controller - Reports - Shooter Reports';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		public function getIndividualShooterReport (Application $app, $from, $to, $name, $shooter_id) {
		   $from = str_replace("%2F", "/", $from);
		   $to = str_replace("%2F", "/", $to);
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT *, STR_TO_DATE( date_stamp,  '%m/%d/%Y' ) AS parsedDate FROM range_time_tracker WHERE membership_number='" . $shooter_id . "' AND accountnumber='" . $accno . "' AND end_time <> 0 AND end_time <> 1";
			if ($name != 'unfiltered') $sql = $sql . " AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			$sql = $sql . " ORDER BY STR_TO_DATE(start_time, '%H:%i:%s')";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			//mem no, name, start time, end time, duration
			$shooter_rows = "";
			$count = 0;
			foreach ($assoc_array as $shooter_log) {
				$count++;
				$tname = $shooter_log['first_name'] . " " . $shooter_log['last_name'];
				$start_time = date("g:i a", strtotime($shooter_log['start_time']));
				$end_time = date("g:i a", strtotime($shooter_log['end_time']));
				$st_t = explode(":", $shooter_log['start_time']);
				$en_t = explode(":", $shooter_log['end_time']);
				$hrs = (int) $en_t[0] - (int) $st_t[0];
				$mins = (int) $en_t[1] - (int) $st_t[1];
				if ($mins < 0){
					$mins += 60;
					$hrs--;
					}
				$duration = $hrs . ":" . sprintf("%02s", $mins) . "";//$interval->format('%s second(s)');
				$mem_no = $shooter_log['membership_number'];
				//now get membership type
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $shooter_id . "'";
				$this_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$mem_type = $this_row['membership_type'];
				$shooter_rows = $shooter_rows . '<tr>
                  <td>' . $count . '</td>
                  <td nowrap="0">' . $mem_type . '</td>
                  <td nowrap="0">' . $mem_no . '</td>
                  <td nowrap="0" style="width:100px;">' . $tname . '</td>
                  <td nowrap="0">' . $start_time . '</td>
                  <td nowrap="0">' . $end_time . '</td>
                  <td nowrap="0">' . $duration . '</td>
                  <td nowrap="0">' . $shooter_log['date_stamp'] . '</td>
                  <td nowrap="0" style="max-width:30px;">' . $shooter_log['employee_name'] . '</td>
                </tr>';
			}
				if ($name == 'unfiltered') $name = "UNFILTERED";
            $data = [
				'shooter_rows' => $shooter_rows,
				'shooter_id' => $shooter_id,
				'report_type' => $name,
				'start_date' => $from,
				'end_date' => $to,
            ];            
            $content = $app['templating']->render('range/reports/shooter_report.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Shooter Report';
			$breadcrumbs[2]['link'] = '/xapp/reports/shooter/0/0/UNFILTERED';
			$breadcrumbs[3]['title'] = $tname;;
            $pageTitle = 'Range Controller - Reports - Shooter Reports';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		public function memberAddressList (Application $app) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			/*$sql="SELECT memberships.membership_type,memberships.membership_number,concat(customers.first_name,customers.middle_name,customers.last_name) as cname,customers.street_address,customers.city,customers.state,customers.zipcode,(select count(*) from range_time_tracker where range_time_tracker.accountnumber=$accno and range_time_tracker.membership_number=memberships.membership_number) as visits, memberships.membership_date from memberships,customers where memberships.membership_number=customers.membership_number and customers.accountnumber=memberships.accountnumber and customers.accountnumber=$accno and customers.is_delete=1 and memberships.membership_number <> '' ";
			$results = DbHelper::dbFetchAllAssoc($link, $sql);
			$output = "";
			foreach($results as $individual)
			{
				$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['cname'] ."</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
				$output = $output . $individual['visits'] . "</td><td>" . $individual['membership_date'] . "</td><td><input name='membership_number' class='membership_number' type='checkbox' value='".$individual['membership_number']."' /></td></tr>";
			}*/
			$vars = [
                'rows' => '',
            ];
			$content = $app['templating']->render('range/reports/member_address.php', $vars);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Member Address List';
            $pageTitle = 'Range Controller - Member Address List';
			$ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $breadcrumbs]);
            $vars = [
                'pageTitle' => isset($pageTitle) 
                    ? $pageTitle : 'Range Controller',
                'content' => $breadcrumbsContent . $content,
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		public function memberAddressPrintLabel (Application $app, $id) 
		{
			$id_array = explode(",",$id);
			foreach($id_array as $id)
			{
				$roid = $app['range_owner_id'];
				$accno = $roid->getAccountNo();
				$link = DbHelper::getWrappedHandle($app['dbs']['main']);
				$membership_number = $id;
				$sql="SELECT * from customers where accountnumber=$accno and membership_number=$membership_number";
				$results = DbHelper::dbFetchAllAssoc($link, $sql);
				
				$first_name = $results[0]['first_name'];
				
				$last_name = $results[0]['last_name'];
			
				$street_address = $results[0]['street_address'];
				
				$city = $results[0]['city'];
				if($city)
				{
					$comma = ',';
				}
				$state = $results[0]['state'];
				
				$zipcode = $results[0]['zipcode']; 
				if(!empty($results))
				{
				echo '
				<html>
				<head>
				<style>
				@media print {  
					@page {
						width:3.2in;
						height:1.4in;
						margin: 0;
						/*border:1px solid black;*/
					}}
				body {
				margin: 0;
				}
				div {
					padding: 0px;
					margin: 0px;
					font-family: Times New Roman;
				}
				.post-area {
					padding:5px 10px;
				}
				.post-area p {
					margin:0;
					font-size:18pt;
					font-weight:bold;
					line-height:1;
				}
				</style>
				</head>
				<body>
				<div class="post-content" style="width:307.2px; /*height:86.4px*/ height: 116.4px; /*border:1px solid #000;*/">
					<div class="post-area">
						<p>'.$first_name.' '.$last_name.'</p>
						<p>'.$street_address.'</p> 
						<p>'.$city.''.$comma.' '.$state.' '.$zipcode.'</p>
					</div>
				</div>
				</body>
				</html>
				';
				}
			}
			die();
		}
		function getGraphReport (Application $app) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$type = $app['request']->get('type', '');
			$append = "";
			if ($from != "" && $to != "") {
				$append = " AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' )";
				$type = "CUSTOM";
			}
			$decrement = 0;
			if ($type != "" && $type != "CUSTOM") {
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
				$append = " AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY )";
			}
			else if ($type != "CUSTOM") $type = "UNFILTERED";
			$visitorTimes = array('8AM'=>0,'9AM'=>0,'10AM'=>0,'11AM'=>0,'12PM'=>0,'1PM'=>0,'2PM'=>0,'3PM'=>0,'4PM'=>0,'5PM'=>0,'6PM'=>0,'7PM'=>0);
			$studentTimes = array('8AM'=>0,'9AM'=>0,'10AM'=>0,'11AM'=>0,'12PM'=>0,'1PM'=>0,'2PM'=>0,'3PM'=>0,'4PM'=>0,'5PM'=>0,'6PM'=>0,'7PM'=>0);
			$memberTimes = array('8AM'=>0,'9AM'=>0,'10AM'=>0,'11AM'=>0,'12PM'=>0,'1PM'=>0,'2PM'=>0,'3PM'=>0,'4PM'=>0,'5PM'=>0,'6PM'=>0,'7PM'=>0);
			$visitorMonths = array('January'=>0,'February'=>0,'March'=>0,'April'=>0,'May'=>0,'June'=>0,'July'=>0,'August'=>0,'September'=>0,'October'=>0,'November'=>0,'December'=>0);
			$studentMonths = array('January'=>0,'February'=>0,'March'=>0,'April'=>0,'May'=>0,'June'=>0,'July'=>0,'August'=>0,'September'=>0,'October'=>0,'November'=>0,'December'=>0);
			$memberMonths = array('January'=>0,'February'=>0,'March'=>0,'April'=>0,'May'=>0,'June'=>0,'July'=>0,'August'=>0,'September'=>0,'October'=>0,'November'=>0,'December'=>0);
			$visitorYears = array('y2013'=>0,'y2014'=>0,'y2015'=>0);
			$studentYears = array('y2013'=>0,'y2014'=>0,'y2015'=>0);
			$memberYears = array('y2013'=>0,'y2014'=>0,'y2015'=>0);
			//1 - sunday, 2 - monday
			$visitorWeekdays = array('Monday'=>0,'Tuesday'=>0,'Wednesday'=>0,'Thursday'=>0,'Friday'=>0,'Saturday'=>0);
			$studentWeekdays = array('Monday'=>0,'Tuesday'=>0,'Wednesday'=>0,'Thursday'=>0,'Friday'=>0,'Saturday'=>0);
			$memberWeekdays = array('Monday'=>0,'Tuesday'=>0,'Wednesday'=>0,'Thursday'=>0,'Friday'=>0,'Saturday'=>0);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "SELECT range_time_tracker.accountnumber, range_time_tracker.membership_number, memberships.membership_type, range_time_tracker.start_time, range_time_tracker.date_stamp, DAYOFWEEK(STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' )) AS weekday FROM range_time_tracker INNER JOIN memberships ON range_time_tracker.membership_number = memberships.membership_number WHERE range_time_tracker.accountnumber='" . $accno . "'" . $append;
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			foreach ($assoc_array as $row) {
				//if ($from == '') $from = $row['date_from'];
				//if ($to == '') $to = $row['date_to'];
				$index = "12PM";
				$monthIndex = "January";
				$yearIndex = "y2013";
				$timestamp = $row['start_time'];
				$tArr = explode(":", $timestamp);
				$datestamp = $row['date_stamp'];
				$mArr = explode("/", $datestamp);
				$weekday = (int) $row['weekday'];
				switch ($weekday) {
					case 2 : $weekdayIndex = "Monday";
					break;
					case 3 : $weekdayIndex = "Tuesday";
					break;
					case 4 : $weekdayIndex = "Wednesday";
					break;
					case 5 : $weekdayIndex = "Thursday";
					break;
					case 6 : $weekdayIndex = "Friday";
					break;
					case 7 : $weekdayIndex = "Saturday";
					break;
				}
				switch ($tArr[0]) {
					case 8 : $index = "8AM";
					break;
					case 9 : $index = "9AM";
					break;
					case 10 : $index = "10AM";
					break;
					case 11 : $index = "11AM";
					break;
					case 12 : $index = "12PM";
					break;
					case 13 : $index = "1PM";
					break;
					case 14 : $index = "2PM";
					break;
					case 15 : $index = "3PM";
					break;
					case 16 : $index = "4PM";
					break;
					case 17 : $index = "5PM";
					break;
					case 18 : $index = "6PM";
					break;
					case 19 : $index = "7PM";
					break;
				}
				switch ($mArr[0]) {
					case 1 : $monthIndex = "January";
					break;
					case 2 : $monthIndex = "February";
					break;
					case 3 : $monthIndex = "March";
					break;
					case 4 : $monthIndex = "April";
					break;
					case 5 : $monthIndex = "May";
					break;
					case 6 : $monthIndex = "June";
					break;
					case 7 : $monthIndex = "July";
					break;
					case 8 : $monthIndex = "August";
					break;
					case 9 : $monthIndex = "September";
					break;
					case 10 : $monthIndex = "October";
					break;
					case 11 : $monthIndex = "November";
					break;
					case 12 : $monthIndex = "December";
					break;
				}
				switch ($mArr[2]) {
					case 13 : $yearIndex = "y2013";
					break;
					case 14 : $yearIndex = "y2014";
					break;
					case 15 : $yearIndex = "y2015";
					break;
				}
				if ($row['membership_type'] == "Student") {
					$studentTimes[$index]++;
					$studentMonths[$monthIndex]++;
					$studentYears[$yearIndex]++;
					$studentWeekdays[$weekdayIndex]++;
				}
				else if ($row['membership_type'] == "Visitor") {
					$visitorTimes[$index]++;
					$visitorMonths[$monthIndex]++;
					$visitorYears[$yearIndex]++;
					$visitorWeekdays[$weekdayIndex]++;
				}
				else {
					$memberTimes[$index]++;
					$memberMonths[$monthIndex]++;
					$memberYears[$yearIndex]++;
					$memberWeekdays[$weekdayIndex]++;
				}
			}
			//data: [65, 59, 80, 10, 54, 32, 9, 13, 43]
			$studentData = 'data: [';
			foreach ($studentTimes as $key=>$value) {
				if ($key != "7PM") $studentData = $studentData . $value . ",";
				else $studentData = $studentData . $value . "]";
			}
			$visitorData = 'data: [';
			foreach ($visitorTimes as $key=>$value) {
				if ($key != "7PM") $visitorData = $visitorData . $value . ",";
				else $visitorData = $visitorData . $value . "]";
			}
			$memberData = 'data: [';
			foreach ($memberTimes as $key=>$value) {
				if ($key != "7PM") $memberData = $memberData . $value . ",";
				else $memberData = $memberData . $value . "]";
			}
			$studentMData = 'data: [';
			foreach ($studentMonths as $key=>$value) {
				if ($key != "December") $studentMData = $studentMData . $value . ",";
				else $studentMData = $studentMData . $value . "]";
			}
			$visitorMData = 'data: [';
			foreach ($visitorMonths as $key=>$value) {
				if ($key != "December") $visitorMData = $visitorMData . $value . ",";
				else $visitorMData = $visitorMData . $value . "]";
			}
			$memberMData = 'data: [';
			foreach ($memberMonths as $key=>$value) {
				if ($key != "December") $memberMData = $memberMData . $value . ",";
				else $memberMData = $memberMData . $value . "]";
			}
			$studentYData = 'data: [';
			foreach ($studentYears as $key=>$value) {
				if ($key != "y2015") $studentYData = $studentYData . $value . ",";
				else $studentYData = $studentYData . $value . "]";
			}
			$visitorYData = 'data: [';
			foreach ($visitorYears as $key=>$value) {
				if ($key != "y2015") $visitorYData = $visitorYData . $value . ",";
				else $visitorYData = $visitorYData . $value . "]";
			}
			$memberYData = 'data: [';
			foreach ($memberYears as $key=>$value) {
				if ($key != "y2015") $memberYData = $memberYData . $value . ",";
				else $memberYData = $memberYData . $value . "]";
			}
			$studentWData = 'data: [';
			foreach ($studentWeekdays as $key=>$value) {
				if ($key != "Saturday") $studentWData = $studentWData . $value . ",";
				else $studentWData = $studentWData . $value . "]";
			}
			$visitorWData = 'data: [';
			foreach ($visitorWeekdays as $key=>$value) {
				if ($key != "Saturday") $visitorWData = $visitorWData . $value . ",";
				else $visitorWData = $visitorWData . $value . "]";
			}
			$memberWData = 'data: [';
			foreach ($memberWeekdays as $key=>$value) {
				if ($key != "Saturday") $memberWData = $memberWData . $value . ",";
				else $memberWData = $memberWData . $value . "]";
			}
			$to = " to <br>" . $to;
			if ($type == "UNFILTERED") $from = $to = "";
            $data = [
                'studentData' => $studentData,
                'visitorData' => $visitorData,
                'memberData' => $memberData,
                'studentMonthData' => $studentMData,
                'visitorMonthData' => $visitorMData,
                'memberMonthData' => $memberMData,
                'studentYearData' => $studentYData,
                'visitorYearData' => $visitorYData,
                'memberYearData' => $memberYData,
                'studentWeekdayData' => $studentWData,
                'visitorWeekdayData' => $visitorWData,
                'memberWeekdayData' => $memberWData,
                'type' => $type,
                'from' => $from,
                'to' => $to,
            ];
            $content = $app['templating']->render('range/reports/range_usage_graph.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Range Usage Report (Graph)';
            $pageTitle = 'Range Controller - Reports - Range Usage Graph';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getGraphLaneReport (Application $app) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$type = $app['request']->get('type', '');
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
		    if($from != "" && $to != "")
			{
				$sql = "select lane_number,count(lane_number) as total from range_time_tracker where accountnumber=$accno AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' ) group by lane_number";
    			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
    			$type = "CUSTOM";
			    
               $sql_new_for_hour_count = "
                SELECT lane_number,
                sum(abs(TIME_TO_SEC(end_time)-TIME_TO_SEC(start_time))) AS active_hour
                FROM range_time_tracker
                WHERE accountnumber=$accno 
                AND end_time !='' 
                AND STR_TO_DATE(date_stamp,'%m/%d/%y') >= '" . date('Y-m-d',strtotime($from)) . "' 
                AND STR_TO_DATE(date_stamp,'%m/%d/%y') <= '" . date('Y-m-d',strtotime($to)) . "' 
                group by lane_number
                "; 
                $assoc_array_hour_count = DbHelper::dbFetchAllAssoc($link, $sql_new_for_hour_count);
                /*echo '<pre>';
                print_r($assoc_array_hour_count);
                echo '</pre>';*/
            }
			$decrement = 0;
			if($type != '' && $type != "CUSTOM")
			{
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
    			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
    			$row = DbHelper::dbFetchRowAssoc($link, $sql);
    			if ($from == '') $from = $row['date_from'];
    			if ($to == '') $to = $row['date_to'];
    			$sql = "select lane_number,count(lane_number) as total from range_time_tracker where accountnumber=$accno  AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY ) group by lane_number";
    			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);

				$sql_new_for_hour_count = "
                SELECT lane_number,
                sum(abs(TIME_TO_SEC(end_time)-TIME_TO_SEC(start_time))) AS active_hour
                FROM range_time_tracker
                WHERE accountnumber=$accno 
                AND end_time !='' 
               AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( range_time_tracker.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY )  
                group by lane_number
                "; 
                $assoc_array_hour_count = DbHelper::dbFetchAllAssoc($link, $sql_new_for_hour_count);
			}
			else if ($type != "CUSTOM")
			{
				$type = "UNFILTERED";
    			$sql = "select lane_number,count(lane_number) as total from range_time_tracker where accountnumber=$accno group by lane_number";
    			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
                
                $sql_new_for_hour_count = "
                select lane_number,
                    sum(abs(TIME_TO_SEC(end_time)-TIME_TO_SEC(start_time))) as active_hour 
                    from range_time_tracker
                    where accountnumber=$accno
                    and end_time !='' 
                    and lane_number !=''
                    group by lane_number
                "; 
                $assoc_array_hour_count = DbHelper::dbFetchAllAssoc($link, $sql_new_for_hour_count);
                
			}
           
			$sql_lanes_no = "select number_of_lanes from controller_config where accountnumber=$accno";
			$total_lanes_no = DbHelper::dbFetchRowAssoc($link, $sql_lanes_no);
			$laneNo = $total_lanes_no['number_of_lanes'];
			$result = array();
			for($i=1;$i<=$laneNo;$i++)
			{
				$result[$i]=0;
			}
			$lan = array();
			foreach($result as $key=>$value){
				$lan[$key]=$key;
			}
			$lane_number = 'labels: ['.implode(',',$lan).']';
			foreach($assoc_array as $key=>$value)
			{	
				$result[$value['lane_number']]=$value['total'];
			}
			$lane_number_data = 'data: ['.implode(',',$result).']';
			$to = " to <br>" . $to;
		    $i=1;$assoc_num='';//$k=0;
            $len_limit = sizeof($lan);
            foreach($assoc_array_hour_count as $key=>$val){
                $assoc_array_lane[]=$val['lane_number'];
            }            
            for($i=1;$i<=$len_limit;$i++) {
                if(in_array($i,$assoc_array_lane)){
                    $assoc_array_hour_count[$i]['lane_number'];
                    foreach($assoc_array_hour_count as $key=>$val){
                        if($val['lane_number']==$i){
                            $lan_active_hour=number_format(($val['active_hour']/(60*60)),2,'.','');
                            break;
                        }
                    }                    
                    //tmp[$i]=$lan_active_hour;
                    /*list($hour, $minute) = explode(':', $tmp[$i]);
                    $hour = (int)$hour;
                    $at_hr =$hour.'.'.$minute;
                    $hr[$i] = $at_hr;*/
                    $hr[$i] = $lan_active_hour;              
                 }else{
                        $at_hr ='0.00';
                        $hour = (int)$at_hr;
                        $hr[$i] = $at_hr; 
                        $tmp[$i]=0;                                
                 }                 
            }           
            $lane_number_of_hour = 'data: ['.implode(',',$hr).']';    		
        if ($type == "UNFILTERED") $from = $to = "";
            $data = [
				'lane_number' => $lane_number,
                'lane_number_data' => $lane_number_data,
                'lane_number_of_hour_used' => $lane_number_of_hour,
                'type' => $type,
                'from' => $from,
                'to' => $to,
            ];
          
            $content = $app['templating']->render('range/reports/range_usage_graph_lane.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Range Lane Usage Report (Graph)';
            $pageTitle = 'Range Controller - Reports - Range Lane Usage Graph'; 
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getMembershipsReport (Application $app) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT *, STR_TO_DATE(  membership_expires,  '%m/%d/%Y' ) AS parsedDate FROM memberships WHERE accountnumber='" . $accno . "' AND STR_TO_DATE(  membership_expires,  '%m/%d/%Y' ) > NOW()";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$eachMembershipTypeCounter = array();
			$eachMembershipTypeAccumulator = array();
			foreach ($assoc_array as $mem) {
				$mem_no = $mem['membership_number'];
				$mem_type = $mem['membership_type'];
				if (!isset($eachMembershipTypeCounter[$mem_type])) {
					$eachMembershipTypeCounter[$mem_type] = 0;
					$eachMembershipTypeAccumulator[$mem_type] = 0.00;
				}
				$eachMembershipTypeCounter[$mem_type]++;
				$sql = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "' AND membership_number='" . $mem_no . "' and payment_amount!=0.00 ORDER BY STR_TO_DATE( date,  '%m/%d/%Y' ) DESC LIMIT 1";
				$billing_array = DbHelper::dbFetchAllAssoc($link, $sql);
				foreach ($billing_array as $item) {
					//if (!isset($eachMembersshipTypeAccumulator[$mem_type])) $eachMembershipTypeAccumulator[$mem_type] = 0;
					$eachMembershipTypeAccumulator[$mem_type] = $eachMembershipTypeAccumulator[$mem_type] + $item['payment_amount'];
				}
			}
			$rows = "";
			$total = 0;
			$qty = 0;
			foreach ($eachMembershipTypeCounter as $key=>$value) {
				if ($key != '' && $key != 'Student' && $key != 'Visitor' && $key != 'Law Enforcement' && $key != 'Employee') {
					$rows = $rows . '<tr>
                	<td>' . $key . '</td>
                	<td>' . $value . '</td>
               		<td>$' . $eachMembershipTypeAccumulator[$key] . '</td>
              		</tr>';
              		$qty = $qty + $value;
              		$total = $total + $eachMembershipTypeAccumulator[$key];
              	}
			}
            $data = [
                'rows'=>$rows,
                'qty' => $qty,
                'total' => $total,
            ];
            $content = $app['templating']->render('range/reports/memberships.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Current Memberships';
            $pageTitle = 'Range Controller - Reports - Current Memberships';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getCompleteMembershipsReport (Application $app) {
			$type = $app['request']->get('type', 'none');
			$from = $app['request']->get('from', 'none');
			$to = $app['request']->get('to', 'none');
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$types = '';
			//grab the list of types for the drop down
			$sql2 = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
			$mem_types_array = DbHelper::dbFetchAllAssoc($link, $sql2);
			foreach ($mem_types_array as $this_type) {
				$types = $types . '<option value="' . $this_type['membership_name'] . '">' . $this_type['membership_name'] . '</option>';
			}
			$start_time = microtime(TRUE);
			$counter=1;
			$rows="";
			$end_time = microtime(TRUE);
			//echo ($end_time - $start_time);
            $data = [
                'rows'=>$rows,
                'types'=>$types,
            ];
            $content = $app['templating']->render('range/reports/complete_member_list.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Complete Member List';
            $pageTitle = 'Range Controller - Reports - Complete Member List';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getPos (Application $app) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$default = $app['request']->get('default', '');
			$type = $app['request']->get('type', $default);
			$append = "";
			if ($from != "" && $to != "") {
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' )";
				$type = "CUSTOM";
			}
			$decrement = 0;
			if ($type != "" && $type != "CUSTOM") {
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY )";
			}
			else if ($type != "CUSTOM") $type = "UNFILTERED";
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "SELECT pos_new_sale.accountnumber, pos_new_sale.transaction_number, pos_new_sale.membership_number, pos_new_sale.date_stamp, pos_new_sale.total, customers.first_name, customers.last_name, memberships.membership_type FROM pos_new_sale INNER JOIN customers on pos_new_sale.membership_number=customers.membership_number INNER JOIN memberships ON customers.membership_number = memberships.membership_number WHERE pos_new_sale.accountnumber='" . $accno . "' " . $append . " GROUP BY pos_new_sale.transaction_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = "";
			$total = 0.00;
			foreach ($assoc_array as $row) {
				$rows = $rows . '<tr class="text-center"">
					<td>' . $row['date_stamp'] . '</td>
					<td><a href="/xapp/customers/' . $row['membership_number'] . '">' . $row['first_name'] . " " . $row['last_name'] . '</a></td>
					<td>' . $row['membership_type'] . '</td>
					<td style="text-align: left;">$' . $row['total'] . '</td>
					<td>' . $row['transaction_number'] . '</td>
					<td><a href="/xapp/reports/pointOfSale/details/' . $row['transaction_number'] . '">Details</a></td>
					<td><a href="/xapp/reports/pointOfSale/history/' . $row['membership_number'] . '">Sales History</a></td>
				</tr>';
				$total = $total + (double) $row['total'];
			}
			$to = " to <br>" . $to;
			if ($type == "UNFILTERED") $from = $to = "";
            $data = [
                'rows' => $rows,
                'type' => $type,
                'from' => $from,
                'to' => $to,
                'total' => $total,
            ];
            $content = $app['templating']->render('range/reports/point.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '';
            $pageTitle = 'Range Controller - Reports - Point Of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getPosReport (Application $app) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$default = $app['request']->get('default', '');
			$type = $app['request']->get('type', $default);
			$append = "";
			if ($from != "" && $to != "") {
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' )";
				$type = "CUSTOM";
			}
			$decrement = 0;
			if ($type != "" && $type != "CUSTOM") {
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY )";
			}
			else if ($type != "CUSTOM") $type = "UNFILTERED";
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "SELECT pos_new_sale.accountnumber, pos_new_sale.transaction_number, pos_new_sale.membership_number, pos_new_sale.date_stamp, pos_new_sale.total, customers.first_name, customers.last_name, memberships.membership_type FROM pos_new_sale INNER JOIN customers on pos_new_sale.membership_number=customers.membership_number INNER JOIN memberships ON customers.membership_number = memberships.membership_number WHERE pos_new_sale.accountnumber='" . $accno . "' " . $append . " GROUP BY pos_new_sale.transaction_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = "";
			$total = 0.00;
			foreach ($assoc_array as $row) {
				$rows = $rows . '<tr class="text-center"">
					<td>' . $row['date_stamp'] . '</td>
					<td><a href="/xapp/customers/' . $row['membership_number'] . '">' . $row['first_name'] . " " . $row['last_name'] . '</a></td>
					<td>' . $row['membership_type'] . '</td>
					<td style="text-align: left;">$' . $row['total'] . '</td>
					<td>' . $row['transaction_number'] . '</td>
					<td><a href="/xapp/reports/pointOfSale/details/' . $row['transaction_number'] . '">Details</a></td>
					<td><a href="/xapp/reports/pointOfSale/history/' . $row['membership_number'] . '">Sales History</a></td>
				</tr>';
				$total = $total + (double) $row['total'];
			}
			$to = " to <br>" . $to;
			if ($type == "UNFILTERED") $from = $to = "";
            $data = [
                'rows' => $rows,
                'type' => $type,
                'from' => $from,
                'to' => $to,
                'total' => $total,
            ];
            $content = $app['templating']->render('range/reports/pos.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/reports/pos';
			$breadcrumbs[3]['title'] = 'Sales';
            $pageTitle = 'Range Controller - Reports - Point Of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getReorders (Application $app) 
		{
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT * from pos_new_inventory WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
            $data = [
                'assoc_array' => $assoc_array,
            ];
            $content = $app['templating']->render('range/reports/reorder.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/reports/pos';
			$breadcrumbs[3]['title'] = 'Inventory Reorders';
            $pageTitle = 'Range Controller - Reports - Point Of Sale - Inventory Reorders';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getPosHistoryReport (Application $app, $membershipNumber) {
			$from = $app['request']->get('from', '');
			$to = $app['request']->get('to', '');
			$type = $app['request']->get('type', '');
			$append = " AND pos_new_sale.membership_number='" . $membershipNumber . "'";
			if ($from != "" && $to != "") {
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE( '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE( '" . $to . "',  '%m/%d/%Y' ) AND pos_new_sale.membership_number='" . $membershipNumber . "'";
				$type = "CUSTOM";
			}
			$decrement = 0;
			if ($type != "" && $type != "CUSTOM") {
				if ($type == "DAILY") $decrement = 0;
				if ($type == "WEEKLY") $decrement = -7;
				if ($type == "MONTHLY") $decrement = -30;
				if ($type == "YEARLY") $decrement = -365;
				$append = " AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) >= DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) AND STR_TO_DATE( pos_new_sale.date_stamp,  '%m/%d/%Y' ) <= DATE_ADD( NOW( ) , INTERVAL 0 DAY ) AND pos_new_sale.membership_number='" . $membershipNumber . "'";
			}
			else if ($type != "CUSTOM") $type = "UNFILTERED";
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT DATE_FORMAT(DATE_ADD( NOW() , INTERVAL " . $decrement . " DAY ) , '%m-%d-%Y') AS date_from, DATE_FORMAT(NOW(),'%m-%d-%Y') AS date_to";
			$row = DbHelper::dbFetchRowAssoc($link, $sql);
			if ($from == '') $from = $row['date_from'];
			if ($to == '') $to = $row['date_to'];
			$sql = "SELECT pos_new_sale.accountnumber, pos_new_sale.transaction_number, pos_new_sale.membership_number, pos_new_sale.date_stamp, pos_new_sale.total, customers.first_name, customers.last_name, memberships.membership_type FROM pos_new_sale INNER JOIN customers on pos_new_sale.membership_number=customers.membership_number INNER JOIN memberships ON customers.membership_number = memberships.membership_number WHERE pos_new_sale.accountnumber='" . $accno . "' " . $append . " GROUP BY pos_new_sale.transaction_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = "";
			$total = 0.00;
			$memberName = "";
			$membershipType = "";
			foreach ($assoc_array as $row) {
				$rows = $rows . '<tr class="text-center"">
					<td>' . $row['date_stamp'] . '</td>
					<td style="text-align: left;">' . $row['total'] . '</td>
					<td>' . $row['transaction_number'] . '</td>
					<td><a href="/xapp/reports/pointOfSale/details/' . $row['transaction_number'] . '" >Details</a></td>
				</tr>';
				$total = $total + (double) $row['total'];
				$memberName = $row['first_name'] . " " . $row['last_name'];
				$membershipType = $row['membership_type'];
			}
			$to = " to <br>" . $to;
			if ($type == "UNFILTERED") $from = $to = "";
            $data = [
                'memberName' => $memberName,
                'membershipNumber' => $membershipNumber,
                'membershipType' => $membershipType,
                'total' => $total,
                'type' => $type,
                'from' => $from,
                'to' => $to,
                'rows' => $rows,
            ];
            $content = $app['templating']->render('range/reports/pos_sales_history.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/reports/pos/sales?default=DAILY';
			$breadcrumbs[3]['title'] = $memberName;
            $pageTitle = 'Range Controller - Reports - Point Of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function getPosSaleDetails (Application $app, $transactionNo) {
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $transactionNo;
			$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
			$rowspan = $resultsArr['mycount'];
			$sql = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' AND transaction_number='" . $transactionNo . "'";
			$resultsArr = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = "";
			$counter = 0;
			$membershipNumber = "";
			foreach ($resultsArr as $transaction) {
				$membershipNumber = $transaction['membership_number'];
				$transactionNumber = $transaction['transaction_number'];
				$counter++;
				$rows = $rows . '<tr class="text-center">
					<td>' . $transaction['quantity'] . '</td>
					<td>' . $transaction['description'] . '</td>
					<td>' . $transaction['item_number'] . '</td>
					<td>$' . $transaction['price'] . '</td>';
					if ($counter == 1) $rows = $rows . '<td rowspan="' . $rowspan . '">$' . $transaction['total'] . '</td>';
					$rows = $rows . '<td>';
					if ($transaction['price'] <= $transaction['total']) $rows = $rows . '<span style="color: red; font-weight: bold;">X</span>';
					else $rows = $rows . '<span style="color: green; font-weight: bold;">✓</span></td>';
				$rows = $rows . '</tr>';
			}
			$sql = "SELECT first_name, last_name FROM customers WHERE membership_number='" . $membershipNumber . "'";
			$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
			$memberName = $resultsArr['first_name'] . " " . $resultsArr['last_name'];
            $data = [
                'rows' => $rows,
                'transactionNumber' => $transactionNumber,
                'memberName' => $memberName,
                'membershipNumber' => $membershipNumber,
            ];
            $content = $app['templating']->render('range/reports/pos_sale_details.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Point Of Sale';
			$breadcrumbs[2]['link'] = '/xapp/reports/pos/sales?default=DAILY';
			$breadcrumbs[3]['title'] = 'Sale # ' . $transactionNumber . '';
            $pageTitle = 'Range Controller - Reports - Point Of Sale';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		function membershipRenewal (Application $app, $month, $year) {
			$month = $app['request']->get('month2', $month);
			$year = $app['request']->get('year2', $year);
			switch ($month) {
				case 'January' :
					$maxday = 31;
					$month_no = 1;
					break;
				case 'February' :
					$maxday = 28;
					$month_no = 2;
					break;
				case 'March' :
					$maxday = 31;
					$month_no = 3;
					break;
				case 'April' :
					$maxday = 30;
					$month_no = 4;
					break;
				case 'May' :
					$maxday = 31;
					$month_no = 5;
					break;
				case 'June' :
					$maxday = 30;
					$month_no = 6;
					break;
				case 'July' :
					$maxday = 31;
					$month_no = 7;
					break;
				case 'August' :
					$maxday = 31;
					$month_no = 8;
					break;
				case 'September' :
					$maxday = 30;
					$month_no = 9;
					break;
				case 'October' :
					$maxday = 31;
					$month_no = 10;
					break;
				case 'November' :
					$maxday = 30;
					$month_no = 11;
					break;
				case 'December' :
					$maxday = 31;
					$month_no = 12;
					break;
			}
			$from = $month_no . "/1/" . $year;
			$to = $month_no . "/" . $maxday . "/" . $year;
			$type = $app['request']->get('type', 'none');
			//$from = $app['request']->get('from', 'none');
			//$to = $app['request']->get('to', 'none');
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$sql = "SELECT customers.accountnumber, customers.first_name, customers.last_name, customers.membership_number, customers.home_phone, memberships.membership_expires,customers.visitcount FROM customers INNER JOIN memberships ON customers.membership_number = memberships.membership_number WHERE customers.accountnumber='" . $accno . "' and customers.is_delete=1 ORDER BY STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) ASC";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$rows = '';
			$counter = 0;
			$append = '';
			if ($type != 'none') $append = $append . " AND membership_type='" . $type . "'";
			if ($from != 'none' && $to != 'none') $append = $append . " AND STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' )" . " AND STR_TO_DATE( membership_expires,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			foreach ($assoc_array as $row) {
				$name = $row['first_name'] . ' ' . $row['last_name'];
				$mem_no = $row['membership_number'];
				$phone_no = $row['home_phone'];
				$sql = "SELECT * FROM memberships WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'" . $append;
				$member_row = DbHelper::dbFetchRowAssoc($link, $sql);
				$reg_date = $member_row['membership_date'];
				$exp_date = $member_row['membership_expires'];
				$mem_type = $member_row['membership_type'];
				$myArr = explode("/", $exp_date);
				$sortable_exp = $myArr[2] . $myArr[0] . $myArr[1];
				$myArr = explode("/", $reg_date);
				$sortable_reg = $myArr[2] . $myArr[0] . $myArr[1];
				/*$sql = "SELECT COUNT(*) AS mycount FROM `range_time_tracker` WHERE membership_number='" . $mem_no . "' AND accountnumber='" . $accno . "'";
				$result = DbHelper::dbFetchRowAssoc($link, $sql);
				$visits = (int) $result['mycount'];*/
				$visits=$row['visitcount'];
					if (!empty($member_row)) {
						$counter++;
                		$sql = "SELECT * FROM membership_renewal WHERE accountnumber='" . $accno . "' AND membership_number='". $mem_no . "'";// AND expiration_date='" . $exp_date . "'";
                		$result2 = DbHelper::dbFetchRowAssoc($link, $sql);
                		$send_resend = 'Send Email';
                		$sent_date = '';
                		if (!empty($result2)) {
                		$send_resend = "Resend Email";
                		$sent_date = $result2['date_sent'];
                		}
                		$rows = $rows . '<tr>
                  <td>' . $counter . '</td>
                  <td>' . $mem_type . '</td>
                  <td nowrap=""><a href="/xapp/customers/' . $mem_no . '">' . $name . '</a></td>
                  <td>' . $mem_no . '</td>
                  <td><span style="color: #fbb91f;">' . $visits . '</span></td>
                  <td sorttable_customkey="' . $sortable_exp . '">' . $exp_date . '</td>
                  <td><a href="#none" onclick="sendRenewalMail(' . $mem_no . ", '" . $name . "')" . '">' . $send_resend . '</td>
                  <td>' . $sent_date . '</td>
                </tr>';
                	}
			}
            $data = [
                'rows' => $rows,
                'month' => $month,
                'year' => $year,
            ];
            $content = $app['templating']->render('range/reports/membership_renewal.php', $data);                   ;
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '/xapp/reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Membership Renewal';
            $pageTitle = 'Range Controller - Reports - Membership Renewal';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		public function memberAddressListByType (Application $app, $type) {
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
			$query_condition = "";
			if($type == "all" )
			{
			$query_condition = " WHERE memberships.accountnumber='" . $accno . "'";
			}
			else
			{
			$query_condition = " WHERE memberships.accountnumber='" . $accno . "' AND memberships.membership_type = '" . $type . "'";
			}
			$allMembers = range_member::getAllMembersByType($app, $query_condition);
			$output = "";
			foreach ($allMembers as $individual) {
			$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['first_name'] . " " . $individual['middle_name'] . " " . $individual['last_name'] . "</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
			$count_visits=$individual['visitcount'];
//			function numOfVisits($membership_number, $link) {
				/*$visits_sql = "";
				$visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $individual['membership_number'] . "'";
				if ($result = $link->query($visits_sql)) {
					$count_visits = 0;
					while ($row = $result->fetch_assoc()) {
						$count_visits++;
					}//END WHILE LOOP  
				}//END IF
    //return $count_visits;*/
	$output = $output . $count_visits . "</td><td>" . $individual['membership_date'] . "</td><td><input name='membership_number' class='membership_number' type='checkbox' value='".$individual['membership_number']."' /></td></tr>";
			}
			$output = $output . "";
			$vars = [
                'output' => $output,
				'type' => $type,
            ];
			$content = $app['templating']->render('range/reports/member_address.php', $vars);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Member Address List';
            $pageTitle = 'Range Controller - Reports';
			$ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $breadcrumbs]);
            $vars = [
                'pageTitle' => isset($pageTitle) 
                    ? $pageTitle : 'Range Controller',
                'content' => $breadcrumbsContent . $content,
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		public function memberAddressListByTypeShortNo (Application $app, $type, $shortno) {
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
			$query_condition = "";
			if($type == "all" )
			{
			$query_condition = " WHERE memberships.accountnumber='" . $accno . "'";
			}
			else
			{
			$query_condition = " WHERE memberships.accountnumber='" . $accno . "' AND memberships.membership_type = '" . $type . "'";
			}
			$allMembers = range_member::getAllMembersByShortNo($app, $query_condition, $shortno);
			$output = "";
			foreach ($allMembers as $individual) {
			$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['first_name'] . " " . $individual['middle_name'] . " " . $individual['last_name'] . "</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
			$count_visits=$individual['visitcount'];
//			function numOfVisits($membership_number, $link) {
				/*$visits_sql = "";
				$visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $individual['membership_number'] . "'";
				if ($result = $link->query($visits_sql)) {
					$count_visits = 0;
					while ($row = $result->fetch_assoc()) {
						$count_visits++;
					}//END WHILE LOOP  
				}//END IF
    //return $count_visits;*/
	$output = $output . $count_visits . "</td><td>" . $individual['membership_date'] . "</td><td><input name='membership_number' class='membership_number' type='checkbox' value='".$individual['membership_number']."' /></td></tr>";
			}
			$output = $output . "";
			$vars = [
                'output' => $output,
				'type' => $type,
				'shortno' => $shortno
            ];
			//return $app['templating']->render('range/reports/member_address.php', $vars);
			//return $output;
			$content = $app['templating']->render('range/reports/member_address.php', $vars);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Member Address List';
            $pageTitle = 'Range Controller - Reports';
			$ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $breadcrumbs]);
            $vars = [
                'pageTitle' => isset($pageTitle) 
                    ? $pageTitle : 'Range Controller',
                'content' => $breadcrumbsContent . $content,
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		public function memberAddressListByDate (Application $app, $month, $year) {
			$roid = $app['range_owner_id'];
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
			$query_condition = " WHERE memberships.accountnumber='" . $accno . "'";
			$allMembers = range_member::getAllMembersByType($app, $query_condition);
			$output = "";
			foreach ($allMembers as $individual) {
			$dateArr = explode("/",$individual['membership_date']);
			if ((int) $month == (int) $dateArr[0] && (int) $year == (int) $dateArr[2]) {
				$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['first_name'] . " " . $individual['middle_name'] . " " . $individual['last_name'] . "</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
				$count_visits=$individual['visitcount'];
//			function numOfVisits($membership_number, $link) {
    /*$visits_sql = "";
    $visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $individual['membership_number'] . "'";
    if ($result = $link->query($visits_sql)) {
        $count_visits = 0;
        while ($row = $result->fetch_assoc()) {
            $count_visits++;
        }//END WHILE LOOP  
    }//END IF
    //return $count_visits;*/
	$output = $output . $count_visits . "</td><td>" . $individual['membership_date'] . "</td><td><input name='membership_number' class='membership_number' type='checkbox' value='".$individual['membership_number']."' /></td></tr>";
			}			
			//$output = $output . " ASDD ";
			} 
			$output = $output . "";
			$vars = [
                'output' => $output,
				'month' => $month,
				'year' => $year,
            ];
			//return $app['templating']->render('range/reports/member_address.php', $vars);
			//return $output;
			$content = $app['templating']->render('range/reports/member_address.php', $vars);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Member Address List';
            $pageTitle = 'Range Controller - Reports';
			$ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $breadcrumbs]);
            $vars = [
                'pageTitle' => isset($pageTitle) 
                    ? $pageTitle : 'Range Controller',
                'content' => $breadcrumbsContent . $content,
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		public function memberAddressListByDateShortNo (Application $app, $month, $year, $shortno) {
			$roid = $app['range_owner_id'];
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();
			$query_condition = " WHERE memberships.accountnumber='" . $accno . "'";
			$allMembers = range_member::getAllMembersByTypeShortNo($app, $query_condition, $shortno);
			$output = "";
			
			foreach ($allMembers as $individual) {
			$dateArr = explode("/",$individual['membership_date']);
			if ((int) $month == (int) $dateArr[0] && (int) $year == (int) $dateArr[2]) {
				$output = $output . "<tr><td>" . $individual['membership_type'] . "</td><td>" . $individual['membership_number'] . "</td><td>" . $individual['first_name'] . " " . $individual['middle_name'] . " " . $individual['last_name'] . "</td><td>" . $individual['street_address'] . "</td><td>" . $individual['city'] . "</td><td>" . $individual['state'] . "</td><td>" . $individual['zipcode'] . "</td><td>";
				$count_visits=$individual['visitcount'];
//			function numOfVisits($membership_number, $link) {
    /*$visits_sql = "";
    $visits_sql = "SELECT membership_number FROM range_time_tracker WHERE membership_number='" . $individual['membership_number'] . "'";
    if ($result = $link->query($visits_sql)) {
        $count_visits = 0;
        while ($row = $result->fetch_assoc()) {
            $count_visits++;
        }//END WHILE LOOP  
    }//END IF
    //return $count_visits;*/
	$output = $output . $count_visits . "</td><td>" . $individual['membership_date'] . "</td><td><input name='membership_number' class='membership_number' type='checkbox' value='".$individual['membership_number']."' /></td></tr>";
			}			
			//$output = $output . " ASDD ";
			} 
			$output = $output . "";
			$vars = [
                'output' => $output,
				'month' => $month,
				'year' => $year,
				'shortno' => $shortno,
            ];
			//return $app['templating']->render('range/reports/member_address.php', $vars);
			//return $output;
			$content = $app['templating']->render('range/reports/member_address.php', $vars);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[1]['link'] = $app['get_range_url']('').'reports';
			$breadcrumbs[1]['title'] = 'Reports';
			$breadcrumbs[2]['title'] = 'Member Address List';
            $pageTitle = 'Range Controller - Reports';
			$ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $breadcrumbs]);
            $vars = [
                'pageTitle' => isset($pageTitle) 
                    ? $pageTitle : 'Range Controller',
                'content' => $breadcrumbsContent . $content,
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url'],
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
		}
		public function getSalesReport(Application $app, $from, $to, $name){
		   $from = str_replace("%2F", "/", $from);
		   $to = str_replace("%2F", "/", $to);
		   $payment_types_acc = array();
		   $payment_types_acc['Cash'] = 0;
		   $payment_types_acc['Visa'] = 0;
		   $payment_types_acc['Mastercard'] = 0;
		   $payment_types_acc['Check'] = 0;
		   $payment_types_acc['Discover'] = 0;
		   $payment_types_acc['Debit'] = 0;
		   $payment_types_acc['Paypal'] = 0;
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			if ($name != "unfiltered") $rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "'" . " AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )" . " ORDER BY transaction_number";
			else $rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' ORDER BY transaction_number";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
			$pos_transaction_rows = "";
			$pos_grand_total = 0;
			
			$lastTransactionNo = 0;
			$thisTransactionNo = 0;
			foreach ($assoc_array as $transaction) {
			$thisTransactionNo = $transaction['transaction_number'];
			$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $thisTransactionNo . " AND accountnumber='" . $accno . "'";
			$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
			$rowspan = $resultsArr['mycount'];
			if ($thisTransactionNo != $lastTransactionNo) $payment_types_acc[$transaction['payment1_method']] += $transaction['total']; 
			if ($thisTransactionNo != $lastTransactionNo) $pos_grand_total = $pos_grand_total + ((double) $transaction['total']);
			    $pos_transaction_rows = $pos_transaction_rows . '<tr class="pos_class">';
                  if ($thisTransactionNo != $lastTransactionNo) $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">' . $transaction['transaction_number'] . '</td>';
                  $pos_transaction_rows = $pos_transaction_rows . '<td>' . $transaction['quantity'] . '</td>
                  <td>' . $transaction['description'] . '</td>
                  <td>' . $transaction['item_number'] . '</td>
                  <td>$' . $transaction['price'] . '</td>';
                  if ($thisTransactionNo != $lastTransactionNo) $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">$' . $transaction['total'] . '</td>';
				  if ($thisTransactionNo != $lastTransactionNo) {
				  $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">';
				  if ((float) $transaction['discount'] <= 0) $pos_transaction_rows = $pos_transaction_rows . '<span style="color: red; font-weight: bold;">X</span>';
				  else $pos_transaction_rows = $pos_transaction_rows . '<span style="color: green; font-weight: bold;">&#10003;</span>';
				  //<span style="color: green; font-weight: bold;">&#10003;</span> or <span style="color: red; font-weight: bold;">X</span>
				  $pos_transaction_rows = $pos_transaction_rows . '</td>';
				  }
                $pos_transaction_rows = $pos_transaction_rows . '</tr>';
				$lastTransactionNo = $thisTransactionNo;
			}
			
			$mem_types_query = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $mem_types_query);
			$membership_types = array();
			$membership_types_sub = array();
			$membership_type_names = array('', '', '', '', '', '', '', '', '', '');
			$arr_index = 0;
			foreach ($assoc_array as $mem_type) {
				$keyname = $mem_type['membership_name'];
				$membership_type_names[$arr_index] = $keyname;
				$membership_types[$keyname] = 0;//will hold number of memberships sold  for this type		
				$membership_types_sub[$keyname] = 0;
				$arr_index++;
			}
			if ($name != "unfiltered") $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'" . " AND STR_TO_DATE( date,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( date,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
			else $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
			foreach ($assoc_array as $billing) {
				if ($billing['payment_for'] != "Class" && $billing['payment_for'] != "Visit") $this_mem = $billing['membership_type'];
				else if ($billing['payment_for'] == "Class") $this_mem = 'Student';
				else if ($billing['payment_for'] == "Visit") $this_mem = 'Visitor';
				$payment_types_acc[$billing['payment_method']] += $billing['payment_amount'];
				foreach ($membership_type_names as $memtype) {
					if ($this_mem == $memtype) {
						$membership_types[$memtype]++;//add to number of this type sold
						$membership_types_sub[$memtype] = $membership_types_sub[$memtype] + $billing['payment_amount'];
					}
				}
			}
			$memberships_transaction_rows = "";
			$memberships_grand_total = 0;
			$memberships_total_count;
			foreach ($membership_type_names as $memtype) {
				$memberships_grand_total = $memberships_grand_total + $membership_types_sub[$memtype];
				$memberships_total_count = $memberships_total_count + $membership_types[$memtype];
				$memberships_transaction_rows = $memberships_transaction_rows . '<tr class="memberships_class">
                  <td>' . $memtype . '</td>
                  <td>' . $membership_types[$memtype] . '</td>
                  <td>$' . number_format($membership_types_sub[$memtype], 2) . '</td>
                </tr>';
			}
			
            $data = [
                'pos_transaction_rows' => $pos_transaction_rows,
				'memberships_transaction_rows' => $memberships_transaction_rows,
				'pos_grand_total' => number_format($pos_grand_total, 2),
				'memberships_grand_total' => number_format($memberships_grand_total, 2),
				'memberships_total_count' => $memberships_total_count,
				'cash' => $payment_types_acc['Cash'],
				'check' => $payment_types_acc['Check'],
				'debit' => $payment_types_acc['Debit'],
				'mastercard' => $payment_types_acc['Mastercard'],
				'visa' => $payment_types_acc['Visa'],
				'discover' => $payment_types_acc['Discover'],
				'paypal' => $payment_types_acc['Paypal'],
				'drawer_end' => number_format(($pos_grand_total + $memberships_grand_total), 2),
				'report_type' => $name,
				'start_date' => $from,
				'end_date' => $to,
            ];            			
            $content = $app['templating']->render('range/reports/reports.php', $data)
                    ;
            //$breadcrumbs = $this->getBreadcrumbs($app);
            //$breadcrumbs[1]['link'] = '';
			$breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => '/xapp/reports', 'title' => 'Reports'], ['title' => 'Sales Report']);
            //$breadcrumbs[1]['link'] = '/xapp/configurations.php';
            $pageTitle = 'Range Controller - Reports - Sales Report';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function getEmployeeReport(Application $app, $from, $to, $name, $empid)
		{
		   $from = str_replace("%2F", "/", $from);
		   $to = str_replace("%2F", "/", $to);
		   $payment_types_acc = array();
		   $payment_types_acc['Cash'] = 0;
		   $payment_types_acc['Visa'] = 0;
		   $payment_types_acc['Mastercard'] = 0;
		   $payment_types_acc['Check'] = 0;
		   $payment_types_acc['Discover'] = 0;
		   $payment_types_acc['Debit'] = 0;
		   $payment_types_acc['Paypal'] = 0;
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			if ($name != "unfiltered")  
			{
				if($empid != 0 )
				{
					 $rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "'" . " AND employee_id='" . $empid . "' AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )" . " ORDER BY transaction_number";
				}
				else
				{
					 $rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "'" . " AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE(  date_stamp,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )" . " ORDER BY transaction_number";		
				}
			}
			else 
			{
				$rep_query = "SELECT * FROM pos_new_sale WHERE accountnumber='" . $accno . "' ORDER BY transaction_number";
			}
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
			$pos_transaction_rows = "";
			$pos_grand_total = 0;
		
			$lastTransactionNo = 0;
			$thisTransactionNo = 0;
			foreach ($assoc_array as $transaction) {
			$thisTransactionNo = $transaction['transaction_number'];
			if($empid != 0)
			{
				$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $thisTransactionNo . " AND accountnumber='" . $accno . "' AND employee_id='" . $empid . "'";
			}
			else
			{
				$sql = "SELECT COUNT(*) as mycount FROM pos_new_sale WHERE transaction_number=" . $thisTransactionNo . " AND accountnumber='" . $accno . "'";	
			}
			$resultsArr = DbHelper::dbFetchRowAssoc($link, $sql);
			$rowspan = $resultsArr['mycount'];
			if ($thisTransactionNo != $lastTransactionNo) $payment_types_acc[$transaction['payment1_method']] += $transaction['total']; 
			if ($thisTransactionNo != $lastTransactionNo) $pos_grand_total = $pos_grand_total + ((double) $transaction['total']);
			    $pos_transaction_rows = $pos_transaction_rows . '<tr class="pos_class">';
                  if ($thisTransactionNo != $lastTransactionNo) $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">' . $transaction['transaction_number'] . '</td>';
                  $pos_transaction_rows = $pos_transaction_rows . '<td>' . $transaction['quantity'] . '</td>
                  <td>' . $transaction['description'] . '</td>
                  <td>' . $transaction['item_number'] . '</td>
                  <td>$' . $transaction['price'] . '</td>';
                  if ($thisTransactionNo != $lastTransactionNo) $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">$' . $transaction['total'] . '</td>';
				  if ($thisTransactionNo != $lastTransactionNo) {
				  $pos_transaction_rows = $pos_transaction_rows . '<td rowspan="' . $rowspan . '">';
				  if ((float) $transaction['discount'] <= 0) $pos_transaction_rows = $pos_transaction_rows . '<span style="color: red; font-weight: bold;">X</span>';
				  else $pos_transaction_rows = $pos_transaction_rows . '<span style="color: green; font-weight: bold;">&#10003;</span>';
				  //<span style="color: green; font-weight: bold;">&#10003;</span> or <span style="color: red; font-weight: bold;">X</span>
				  $pos_transaction_rows = $pos_transaction_rows . '</td>';
				  }
                $pos_transaction_rows = $pos_transaction_rows . '</tr>';
				$lastTransactionNo = $thisTransactionNo;
			}
			
			$mem_types_query = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $mem_types_query);
			$membership_types = array();
			$membership_types_sub = array();
			$membership_type_names = array('', '', '', '', '', '', '', '', '', '');
			$arr_index = 0;
			foreach ($assoc_array as $mem_type) {
				$keyname = $mem_type['membership_name'];
				$membership_type_names[$arr_index] = $keyname;
				$membership_types[$keyname] = 0;//will hold number of memberships sold  for this type		
				$membership_types_sub[$keyname] = 0;
				$arr_index++;
			}
			if ($name != "unfiltered") 
			{
				if($empid != 0)
				{
					 $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'" . " AND employee_id='" . $empid . "' AND STR_TO_DATE( date,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( date,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";
				}
				else
				{
					 $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'" . " AND STR_TO_DATE( date,  '%m/%d/%Y' ) >= STR_TO_DATE(  '" . $from . "',  '%m/%d/%Y' ) AND STR_TO_DATE( date,  '%m/%d/%Y' ) <= STR_TO_DATE(  '" . $to . "',  '%m/%d/%Y' )";		
				}
			}
			else
			{
				 $rep_query = "SELECT * FROM billing_info WHERE accountnumber='" . $accno . "'";
			}
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $rep_query);
			foreach ($assoc_array as $billing) {
				if ($billing['payment_for'] != "Class" && $billing['payment_for'] != "Visit") $this_mem = $billing['membership_type'];
				else if ($billing['payment_for'] == "Class") $this_mem = 'Student';
				else if ($billing['payment_for'] == "Visit") $this_mem = 'Visitor';
				$payment_types_acc[$billing['payment_method']] += $billing['payment_amount'];
				foreach ($membership_type_names as $memtype) {
					if ($this_mem == $memtype) {
						$membership_types[$memtype]++;//add to number of this type sold
						$membership_types_sub[$memtype] = $membership_types_sub[$memtype] + $billing['payment_amount'];
					}
				}
			}
			$memberships_transaction_rows = "";
			$memberships_grand_total = 0;
			$memberships_total_count;
			foreach ($membership_type_names as $memtype) {
				$memberships_grand_total = $memberships_grand_total + $membership_types_sub[$memtype];
				$memberships_total_count = $memberships_total_count + $membership_types[$memtype];
				$memberships_transaction_rows = $memberships_transaction_rows . '<tr class="memberships_class">
                  <td>' . $memtype . '</td>
                  <td>' . $membership_types[$memtype] . '</td>
                  <td>$' . number_format($membership_types_sub[$memtype], 2) . '</td>
                </tr>';
			}
			
			$employees = "SELECT * FROM employees WHERE accountnumber = '".$_SESSION['account_number']."' ORDER BY fname";
			$getemplists = DbHelper::dbFetchAllAssoc($link, $employees);
			$employees_name = "SELECT `fname`, `lname` FROM employees WHERE id = '".$empid."'";
			$getemp_name = DbHelper::dbFetchAllAssoc($link, $employees_name);
            $data = [
                'pos_transaction_rows' => $pos_transaction_rows,
				'memberships_transaction_rows' => $memberships_transaction_rows,
				'pos_grand_total' => number_format($pos_grand_total, 2),
				'memberships_grand_total' => number_format($memberships_grand_total, 2),
				'memberships_total_count' => $memberships_total_count,
				'cash' => $payment_types_acc['Cash'],
				'check' => $payment_types_acc['Check'],
				'debit' => $payment_types_acc['Debit'],
				'mastercard' => $payment_types_acc['Mastercard'],
				'visa' => $payment_types_acc['Visa'],
				'discover' => $payment_types_acc['Discover'],
				'paypal' => $payment_types_acc['Paypal'],
				'drawer_end' => number_format(($pos_grand_total + $memberships_grand_total), 2),
				'report_type' => $name,
				'start_date' => $from,
				'end_date' => $to,
				'getemplists'=>$getemplists,
				'emp_name'=> $getemp_name,
            ];            			
            $content = $app['templating']->render('range/reports/employee_reports.php', $data)
                    ;
            //$breadcrumbs = $this->getBreadcrumbs($app);
            //$breadcrumbs[1]['link'] = '';
			$breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => '/xapp/reports', 'title' => 'Reports'], ['title' => 'Employee Report']);
            //$breadcrumbs[1]['link'] = '/xapp/configurations.php';
            $pageTitle = 'Range Controller - Reports - Employee Report';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		
		 private function getBreadcrumbs(Application $app){
            return array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Members']
            );
        }
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $data['breadcrumbs']]);
            $vars = [
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
        }
	}
}