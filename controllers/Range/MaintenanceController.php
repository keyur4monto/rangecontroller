<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    
    class MaintenanceController {
        
        protected $layoutPath;
        protected $pageSize;
        
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
        
        public function index(Application $app){
            //$pf = $app['range_url_prefix'];
            //return $app['request']->get('cmd', 'aa') .print_r($pf,1) ;
            
            return $this->indexPage($app);
        }
		public function landing(Application $app){
			$content = $app['templating']->render('range/maintenance/list.php');
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Maintenance Tasks';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
		private function getBreadcrumbs(Application $app){
            return array(
                ['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('').'maintenance', 'title'=>'Maintenance Tasks'],
            );
        }
		
		private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $data['breadcrumbs']]);
            
            
            $vars = [
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
        }
		
		public function rangeCleaning (Application $app) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM cleaning_job_duties WHERE accountnumber='" . $accno . "' ORDER BY id ASC";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$jobsContent = "";
			
			$jobOptions = "";
			
			foreach ($assoc_array as $job) {
			
				$jobOptions = $jobOptions . '<option value=' . $job['id'] . '>' . $job['job_title'] . '</option>';
			
				$jobsContent = $jobsContent . '<table>
                <tr>
                  <th colspan="2" style="background-color: #808080;">' . $job['job_title'] . ' (<a href="cleaning/manageJob/' . $job['id'] . '">Edit</a>)</th>
                </tr>
                <tr>
                  <td><ul>';
                      if ($job['duty1'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty1'] . '</li>';
                      if ($job['duty2'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty2'] . '</li>';
                      if ($job['duty3'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty3'] . '</li>';
					  if ($job['duty4'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty4'] . '</li>';
					  if ($job['duty5'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty5'] . '</li>';
                  $jobsContent = $jobsContent .  '</ul></td>
                  <td class="text-center" style="width:15%;"><input type="button" value="Completed By" onclick="window.location=\'/xapp/maintenance/cleaning/completeJob/' . $job['id'] . '\'"></td>
                </tr>
              </table>';
			  
			  //now get the sign offs
			  
			  $sql = "SELECT * FROM cleaning_task_signoff WHERE accountnumber='" . $accno . "' AND task='" . $job['job_title'] . "' ORDER BY id DESC";
			  
			  $assoc_array2 = DbHelper::dbFetchAllAssoc($link, $sql);
			  
			  $jobsContent = $jobsContent . '<table>';
			  
			  foreach ($assoc_array2 as $signoff) {
			  
					$jobsContent = $jobsContent . '<tr id="signoff-' . $signoff['id'] . '">
                  <td style="width: 25%;">' . $signoff['timestamp'] . '</td>
                  <td style="width: 8%;">' . $signoff['signoff'] . '</td>
                  <td style="width: 58%;">' . $signoff['comment'] . '</td>
                  <td style="width: 9%;"><a href="#asd" onclick="deleteById(' . $signoff['id'] . ')">Delete</td>
                </tr>';
			  
			  }
			
			  $jobsContent = $jobsContent . '</table>';
			
			}
			
			/* 
			
			<table>
                <tr>
                  <th colspan="2" style="background-color: #808080;">Lobby Office Trash (<a href="cleaning/manageJob">Edit</a>)</th>
                </tr>
                <tr>
                  <td><ul>
                      <li>Clean trash off floors, tables, and chairs</li>
                      <li>Empty garbage cans and take out to trash pickup</li>
                      <li>Take trash to recycling center each friday</li>
                    </ul></td>
                  <td class="text-center"><input type="button" value="Completed By" onclick="window.location='cleaning/completeJob'"></td>
                </tr>
              </table>
              <table>
                <tr>
                  <td style="width: 25%;">Thu Dec 11 8:56:17 2014</td>
                  <td style="width: 8%;">DR</td>
                  <td style="width: 58%;">Comment...</td>
                  <td style="width: 9%;"><a href="" onclick="alert('Delete')">Delete</td>
                </tr>
              </table>
			  
			  */
			
            $data = [
				'jobsContent' => $jobsContent,
				'jobOptions' => $jobOptions,
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/maintenance/cleaning_list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[2]['title'] = 'Cleaning List';
            $pageTitle = 'Range Controller - Maintenance - Cleaning List';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		
		
		}
		
		public function completeJob (Application $app, $id) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM cleaning_job_duties WHERE id=" . $id;
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$job_title = $results['job_title'];
            			
            $data = [
			//'temp' => $range_owner,
				'job_title' => $job_title,
            ];
            
            $content = $app['templating']->render('range/maintenance/complete_job.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[2]['link'] = $app['get_range_url']('').'maintenance/cleaning';
			$breadcrumbs[2]['title'] = 'Cleaning List';
			$breadcrumbs[3]['title'] = 'Complete Job';
            $pageTitle = 'Range Controller - Maintenance - Cleaning List - Complete Job';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		
		
		}
		
		public function addJob (Application $app) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
            $data = [
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/maintenance/add_job.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[2]['link'] = $app['get_range_url']('').'maintenance/cleaning';
			$breadcrumbs[2]['title'] = 'Cleaning List';
			$breadcrumbs[3]['title'] = 'Add Job';
            $pageTitle = 'Range Controller - Maintenance - Cleaning List - Add Job';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		
		
		}
		
		public function postJob(Application $app){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$job_title = $app['request']->get('job_title', '');
			
			$duty1 = $app['request']->get('duty1', '');
			
			$duty2 = $app['request']->get('duty2', '');
			
			$duty3 = $app['request']->get('duty3', '');
			
			$duty4 = $app['request']->get('duty4', '');
			
			$duty5 = $app['request']->get('duty5', '');
			
			$sql = "INSERT INTO cleaning_job_duties (acctnumberhashed, accountnumber, job_title, duty1, duty2, duty3, duty4, duty5) VALUES (\"" . $hash . "\", \"" . $accno . "\", \"" . $job_title . "\", \"" . $duty1 . "\", \"" . $duty2 . "\", \"" . $duty3 . "\", \"" . $duty4 . "\", \"" . $duty5 . "\")";
			
			$link->query($sql);
			
			return $sql;
			
        }
		
		public function deleteCompletedJob(Application $app){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$id = $app['request']->get('id', '');
						
			$sql = "DELETE FROM cleaning_task_signoff WHERE id=" . $id;
			
			$link->query($sql);
			
			return $sql;
			
        }
		
		public function postManagedJob(Application $app){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$job_title = $app['request']->get('job_title', '');
			
			$duty1 = $app['request']->get('duty1', '');
			
			$duty2 = $app['request']->get('duty2', '');
			
			$duty3 = $app['request']->get('duty3', '');
			
			$duty4 = $app['request']->get('duty4', '');
			
			$duty5 = $app['request']->get('duty5', '');
			
			$id = $app['request']->get('id', '');
			
			$sql = "UPDATE cleaning_job_duties SET job_title=\"" . $job_title . "\", duty1=\"" . $duty1 . "\", duty2=\"" . $duty2 . "\", duty3 = \"" . $duty3 . "\", duty4=\"" . $duty4 . "\", duty5 = \"" . $duty5 . "\" WHERE id=" . $id;
			
			$link->query($sql);
			
			return $sql;
			
        }
		
		public function postCompletedJob(Application $app){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$job_title = $app['request']->get('job_title', '');
			
			$comments = $app['request']->get('comments', '');
			
			$timestamp = $app['request']->get('timestamp', '');
			
			$signoff = $app['request']->get('signoff', '');
			
			$month = $app['request']->get('month', '');
			
			$year = $app['request']->get('year', '');
			
			$sql = "SELECT * FROM employees WHERE id=" . $signoff;
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$initials = $results['initials'];
			
			$sql = "INSERT INTO cleaning_task_signoff (acctnumberhashed, accountnumber, task, month, year, signoff, comment, timestamp) VALUES ('" . $hash . "', '" . $accno . "', '" . $job_title . "', '" . $month . "', '" . $year . "', '" . $initials . "', '" . $comments . "', '" . $timestamp . "')";
			
			$link->query($sql);
			
			return $sql;
			
        }
		
		public function manageJob (Application $app, $id) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM cleaning_job_duties WHERE accountnumber='" . $accno . "' AND id=" . $id;
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
            
            $data = [
			//'temp' => $range_owner,
				'job_title' => $results['job_title'],
				'duty1' => $results['duty1'],
				'duty2' => $results['duty2'],
				'duty3' => $results['duty3'],
				'duty4' => $results['duty4'],
				'duty5' => $results['duty5'],
				'id' => $id,
            ];
            
            $content = $app['templating']->render('range/maintenance/manage_job.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[2]['link'] = $app['get_range_url']('').'maintenance/cleaning';
			$breadcrumbs[2]['title'] = 'Cleaning List';
			$breadcrumbs[3]['title'] = 'Manage Job';
            $pageTitle = 'Range Controller - Maintenance - Cleaning List - Manage Job';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		
		
		}
		
		public function laneMaintenance(Application $app, $toggle){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$buttonColors = array();
			
			//first, get number of lanes
			
			$sql = "SELECT num_of_lanes FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$num_of_lanes = $results['num_of_lanes'];
			
			//next check each lane for being open or closed - 1 is closed - 0 is open
			
			for ($k = 1;$k<=$num_of_lanes;$k++) {
			
				$sql = "SELECT COUNT(*) as closed FROM `range_lanes` WHERE accountnumber='" . $accno . "' AND lane_number='" . $k . "' AND membership_number='CLOSE'";
			
				$results = DbHelper::dbFetchRowAssoc($link, $sql);
				
				$closed = $results['closed'];
				
				if ($closed == 1) $buttonColors[$k-1] = 'red';
				
				else $buttonColors[$k-1] = 'green';
			
			}
			
			//make any necessary changes - need to check here if lane is already closed in which case delete the record
			
			if ($buttonColors[$toggle-1] == 'green') {
			
				$sql = "INSERT INTO range_lanes (acctnumberhashed, accountnumber, membership_number, first_name, last_name, lane_number) VALUES ('" . $hash . "', '" . $accno . "', '" . "CLOSE" . "', '" . "CLOSE" . "', '" . "CLOSE" . "', '" . $toggle . "')";
			
				$buttonColors[$toggle-1] = 'red';
			
			}
			
			else if ($buttonColors[$toggle-1] == 'red')  { 
			
				$sql = "DELETE FROM range_lanes WHERE accountnumber='" . $accno . "' AND membership_number='CLOSE' AND lane_number='" . $toggle . "'";
			
				$buttonColors[$toggle-1] = 'green';
			
			}
			
			if ($toggle != "home" && $toggle > 0 && $toggle <= $num_of_lanes) $link->query($sql);
			
			//update color for relevant lane
			
			$num_of_rows = ((int)($num_of_lanes / 5));//an array will be indexed by num of rows to track which are open or closed
			
			if (($num_of_lanes % 5) != 0) $num_of_rows++;
			
			$buttonRows = "";
			
			$bigCounter = 0;
			
			for ($i = 1; $i <= $num_of_rows; $i++){
			
				$buttonRows = $buttonRows . '<tr>
                <td>';
			
				for ($j = 1; $j <= 5; $j++) {
				
					$bigCounter++;
				
					if ($bigCounter <= $num_of_lanes) { 
					
						$mylink = "window.location='/xapp/maintenance/lane/" . $bigCounter . "'";
				
						$buttonRows = $buttonRows . '&nbsp;
						<input type="button" onclick="' . $mylink . '" value="Lane ' . $bigCounter . '" style="height: 35px; width: 80px; text-transform: Uppercase; font-weight: bold; background-color: ' . $buttonColors[$bigCounter - 1] . ';">';
					
					}
				
				}
			
				$buttonRows = $buttonRows . '</tr>';
			
			}
			
			$sql = "SELECT *, STR_TO_DATE(  bug_date,  '%m/%d/%Y' ) AS parsedDate FROM bug_report WHERE accountnumber='" . $accno . "' ORDER BY parsedDate DESC, id DESC";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$bugReports = "";
			
			foreach ($assoc_array as $bug_report) {
			
				$bugReports = $bugReports . '<tr id="report-' . $bug_report['id'] . '">
                <td nowrap="" style="width: 10%;">' . $bug_report['bug_date'] . '</td>
                <td nowrap="">' . $bug_report['employee_name'] . '</td>
                <td>' . $bug_report['bug_report'] . '</td>
                <td nowrap="" style="width: 10%;"><a href="#asd" onclick="myConfirm(' . $bug_report['id'] . ')">Delete</a></td>
              </tr>';			
			
			}
			
			/*
			
			 <tr>
                <td nowrap="" style="width: 10%;">12/20/2014</td>
                <td nowrap="">Employee</td>
                <td>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</td>
                <td nowrap="" style="width: 10%;"><a href="#">Delete</a></td>
              </tr>
			  
			 */
			
            $data = [
				'buttonRows' => $buttonRows,
				'bugReports' => $bugReports,
            ];
            
            $content = $app['templating']->render('range/maintenance/range_maintenance.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[2]['link'] = '';
			$breadcrumbs[2]['title'] = 'Lane Maintenance';
            $pageTitle = 'Range Controller - Maintenance Tasks - Lane Maintenance';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		
		public function addNote(Application $app){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
            $data = [
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/maintenance/add_note.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[2]['link'] = $app['get_range_url']('').'maintenance/lane/home';
			$breadcrumbs[2]['title'] = 'Lane Maintenance';
			$breadcrumbs[3]['link'] = '';
			$breadcrumbs[3]['title'] = 'Add Note';
            $pageTitle = 'Range Controller - Maintenance - Add Note';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		
		public function postNote(Application $app){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$bug_content = $app['request']->get('bug_content', '');
			
			$employee_id = $app['request']->get('employee_id', '');
			
			$bug_date = $app['request']->get('bug_date', '');
			
			//$mystring = $bug_content . $employee_id . $bug_date;
			
			//return $mystring;
			
			$qry = "SELECT * FROM employees WHERE id=" . $employee_id;
			
			$results = DbHelper::dbFetchRowAssoc($link, $qry);
			
			//$emp_name = $results['fname'] . " " . $results['lname'];
			
			$employee_name = $results['fname'] . " " . $results['lname']; 
			
			$sql = "INSERT INTO bug_report (acctnumberhashed, accountnumber, bug_date, bug_report, employee_name) VALUES ('" . $hash . "', '" . $accno . "', '" . $bug_date . "', '" . $bug_content . "', '" . $employee_name . "')";
			
			$link->query($sql);
			
			return $sql;
			
        }
				
		public function deleteNote(Application $app){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$id = $app['request']->get('id', '');
			
			$sql = "DELETE FROM bug_report WHERE accountnumber='" . $accno . "' AND id=" . $id;
			
			$link->query($sql);
			
		}
		
		
		public function rangeCleaningByJob (Application $app, $id) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM cleaning_job_duties WHERE accountnumber='" . $accno . "' AND id=" . $id;
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$jobsContent = "";
			
			$jobOptions = "";
			
			foreach ($assoc_array as $job) {
			
				$jobOptions = $jobOptions . '<option value=' . $job['id'] . '>' . $job['job_title'] . '</option>';
			
				$jobsContent = $jobsContent . '<table>
                <tr>
                  <th colspan="2" style="background-color: #808080;">' . $job['job_title'] . ' (<a href="cleaning/manageJob/' . $job['id'] . '">Edit</a>)</th>
                </tr>
                <tr>
                  <td><ul>';
                      if ($job['duty1'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty1'] . '</li>';
                      if ($job['duty2'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty2'] . '</li>';
                      if ($job['duty3'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty3'] . '</li>';
					  if ($job['duty4'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty4'] . '</li>';
					  if ($job['duty5'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty5'] . '</li>';
                  $jobsContent = $jobsContent .  '</ul></td>
                  <td class="text-center"  style="width:15%";><input type="button" value="Completed By" onclick="window.location=\'/xapp/maintenance/cleaning/completeJob/' . $job['id'] . '\'"></td>
                </tr>
              </table>';
			  
			  //now get the sign offs
			  
			  $sql = "SELECT * FROM cleaning_task_signoff WHERE accountnumber='" . $accno . "' AND task='" . $job['job_title'] . "' ORDER BY id DESC";
			  
			  $assoc_array2 = DbHelper::dbFetchAllAssoc($link, $sql);
			  
			  $jobsContent = $jobsContent . '<table>';
			  
			  foreach ($assoc_array2 as $signoff) {
			  
					$jobsContent = $jobsContent . '<tr id="signoff-' . $signoff['id'] . '">
                  <td style="width: 25%;">' . $signoff['timestamp'] . '</td>
                  <td style="width: 8%;">' . $signoff['signoff'] . '</td>
                  <td style="width: 58%;">' . $signoff['comment'] . '</td>
                  <td style="width: 9%;"><a href="#asd" onclick="deleteById(' . $signoff['id'] . ')">Delete</td>
                </tr>';
			  
			  }
			
			  $jobsContent = $jobsContent . '</table>';
			
			}
			
			/* 
			
			<table>
                <tr>
                  <th colspan="2" style="background-color: #808080;">Lobby Office Trash (<a href="cleaning/manageJob">Edit</a>)</th>
                </tr>
                <tr>
                  <td><ul>
                      <li>Clean trash off floors, tables, and chairs</li>
                      <li>Empty garbage cans and take out to trash pickup</li>
                      <li>Take trash to recycling center each friday</li>
                    </ul></td>
                  <td class="text-center"><input type="button" value="Completed By" onclick="window.location='cleaning/completeJob'"></td>
                </tr>
              </table>
              <table>
                <tr>
                  <td style="width: 25%;">Thu Dec 11 8:56:17 2014</td>
                  <td style="width: 8%;">DR</td>
                  <td style="width: 58%;">Comment...</td>
                  <td style="width: 9%;"><a href="" onclick="alert('Delete')">Delete</td>
                </tr>
              </table>
			  
			  */
			
            $data = [
				'jobsContent' => $jobsContent,
				'jobOptions' => $jobOptions,
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/maintenance/cleaning_list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[2]['title'] = 'Cleaning List';
            $pageTitle = 'Range Controller - Maintenance - Cleaning List';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		
		
		}
		
		
		
		public function rangeCleaningByDate (Application $app, $month, $year) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM cleaning_job_duties WHERE accountnumber='" . $accno . "' ORDER BY id ASC";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			
			$jobsContent = "";
			
			$jobOptions = "";
			
			foreach ($assoc_array as $job) {
			
				$jobOptions = $jobOptions . '<option value=' . $job['id'] . '>' . $job['job_title'] . '</option>';
			
				$jobsContent = $jobsContent . '<table>
                <tr>
                  <th colspan="2" style="background-color: #808080;">' . $job['job_title'] . ' (<a href="cleaning/manageJob/' . $job['id'] . '">Edit</a>)</th>
                </tr>
                <tr>
                  <td><ul>';
                      if ($job['duty1'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty1'] . '</li>';
                      if ($job['duty2'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty2'] . '</li>';
                      if ($job['duty3'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty3'] . '</li>';
					  if ($job['duty4'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty4'] . '</li>';
					  if ($job['duty5'] != '') $jobsContent = $jobsContent . '<li>' . $job['duty5'] . '</li>';
                  $jobsContent = $jobsContent .  '</ul></td>
                  <td class="text-center hideFromPrint" style="width:15%"><input type="button" value="Completed By" onclick="window.location=\'/xapp/maintenance/cleaning/completeJob/' . $job['id'] . '\'"></td>
                </tr>
              </table>';
			  
			  //now get the sign offs
			  
			  $sql = "SELECT * FROM cleaning_task_signoff WHERE accountnumber='" . $accno . "' AND task='" . $job['job_title'] . "' AND month='" . $month . "' AND year='" . $year . "' ORDER BY id DESC";
			  
			  $assoc_array2 = DbHelper::dbFetchAllAssoc($link, $sql);
			  
			  $jobsContent = $jobsContent . '<table>';
			  
			  foreach ($assoc_array2 as $signoff) {
			  
					$jobsContent = $jobsContent . '<tr id="signoff-' . $signoff['id'] . '">
                  <td style="width: 25%;">' . $signoff['timestamp'] . '</td>
                  <td style="width: 8%;">' . $signoff['signoff'] . '</td>
                  <td style="width: 58%;">' . $signoff['comment'] . '</td>
                  <td style="width: 9%;"><a href="#asd" onclick="deleteById(' . $signoff['id'] . ')">Delete</td>
                </tr>';
			  
			  }
			
			  $jobsContent = $jobsContent . '</table>';
			
			}
			
			/* 
			
			<table>
                <tr>
                  <th colspan="2" style="background-color: #808080;">Lobby Office Trash (<a href="cleaning/manageJob">Edit</a>)</th>
                </tr>
                <tr>
                  <td><ul>
                      <li>Clean trash off floors, tables, and chairs</li>
                      <li>Empty garbage cans and take out to trash pickup</li>
                      <li>Take trash to recycling center each friday</li>
                    </ul></td>
                  <td class="text-center"><input type="button" value="Completed By" onclick="window.location='cleaning/completeJob'"></td>
                </tr>
              </table>
              <table>
                <tr>
                  <td style="width: 25%;">Thu Dec 11 8:56:17 2014</td>
                  <td style="width: 8%;">DR</td>
                  <td style="width: 58%;">Comment...</td>
                  <td style="width: 9%;"><a href="" onclick="alert('Delete')">Delete</td>
                </tr>
              </table>
			  
			  */
			
            $data = [
				'jobsContent' => $jobsContent,
				'jobOptions' => $jobOptions,
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/maintenance/cleaning_list.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
			$breadcrumbs[2]['title'] = 'Cleaning List - ' . $month . ' ' . $year;
            $pageTitle = 'Range Controller - Maintenance - Cleaning List';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		
		
		}
		
		public function currentRangeUsage (Application $app) {
			//echo $range_owner = $app['range_owner'];
			$roid = $app['range_owner_id'];
			
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
						
			$sql = "SELECT num_of_lanes FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$num_of_lanes = $results['num_of_lanes'];
			
			$buttonColors = array();
			
			$closedLanesRows = "";
		
			//next check each lane for being open or closed - 1 is closed - 0 is open
			
			for ($k = 1;$k<=$num_of_lanes;$k++) {
			
				$sql = "SELECT COUNT(*) as closed FROM `range_lanes` WHERE accountnumber='" . $accno . "' AND lane_number='" . $k . "' AND membership_number='CLOSE'";
			
				$results = DbHelper::dbFetchRowAssoc($link, $sql);
				$closed = $results['closed'];
				
				if ($closed == 1) { 
				
					$buttonColors[$k-1] = 'red';
				
					$closedLanesRows = $closedLanesRows . '<tr>
              <th colspan="7" style="background-color: red;">LANE ' . $k . '------IS CLOSED FOR MAINTENANCE</th></tr>';
				
				}
				
				else $buttonColors[$k-1] = 'green';
			
			}
			$sql = "SELECT * FROM range_lanes WHERE accountnumber='" . $accno . "' AND membership_number!='" . "CLOSE" . "' ORDER BY start_time ASC";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$usercount = 0;
			
			$rangeUserRows = "";
			
			foreach ($assoc_array as $range_user) {
			$member = range_member::initForMember($link, $roid->getAccountHashKey(), $roid->getAccountNo(), $range_user['membership_number']);
			$member_img = $app['range_member_image_url']($member->getMembershipNumber()) . '?rand='. strtotime('now');
			
				$rangeUserRows = $rangeUserRows . '
              <tr>';
                //$rangeUserRows = $rangeUserRows . '<td>' . $range_user['employee'] . '</td>';
                $rangeUserRows = $rangeUserRows . '<td>' . $range_user['employee'] . '</td>';
                $rangeUserRows = $rangeUserRows . '<td>' . $range_user['membership_number'] . '</td>';
                 $rangeUserRows = $rangeUserRows . '<td><a class="thumbnail" href="/xapp/customers/' . $range_user['membership_number'] . '">' . $range_user['first_name'] . ' ' . $range_user['last_name'] . '<span class="arrow_box"><img src="'.$member_img.'" style="width:auto; height: 250px; max-width:250px;" /><br /></span></a></td>
                <td>' . $range_user['lane_number'] . '</td>
                <td class="startTimes">' . $range_user['start_time'] . '</td>';
                //<td style="width: 20%;"><a href="add_member.html">Add Member to Lane</a></td>
                $rangeUserRows = $rangeUserRows . '<td><a href="#" onclick="logmeout('.$range_user['membership_number'].')">Log Out</a></td>
              </tr>';
			  
			  $usercount++;
			
			}
			if ($usercount == 0) $rangeUserRows = '<tr>
                <td colspan="7"> <span style="color: red;">There are no members currently on the range.</span> </td>
              </tr>';
			
            $data = [
				'rangeUserRows' => $rangeUserRows,
				'closedLanesRows' => $closedLanesRows,
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/laneusage/current_range_usage.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            //$breadcrumbs[2]['link'] = $app['get_range_url']('').'maintenance/cleaning';
			$breadcrumbs[1]['title'] = 'Current Range Usage';
			$breadcrumbs[1]['link'] = '';
			//$breadcrumbs[3]['title'] = 'Add Job';
            $pageTitle = 'Range Controller - Current Range Usage';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		
		
		}
		
		public function rangeUserLogin (Application $app) {
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            
            $data = [
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/laneusage/add_member.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[2]['link'] = $app['get_range_url']('').'currentRangeUsage';
			$breadcrumbs[2]['title'] = 'Current Range Usage';
			$breadcrumbs[3]['title'] = 'Add';
            $pageTitle = 'Range Controller - Current Range Usage - Add';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		
		
		}
		
		public function loginSelectLane (Application $app, $member_id) { //to be used to get first and last name as well
		
			$range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM customers WHERE accountnumber='" . $accno . "' AND membership_number='" . $member_id . "'";
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$first_name = $results['first_name'];
			
			$last_name = $results['last_name'];
			
			$buttonColors = array();
			
			//first, get number of lanes
			
			$sql = "SELECT num_of_lanes FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$num_of_lanes = $results['num_of_lanes'];
			
			//next check each lane for being open or closed - 1 is closed - 0 is open
			
			for ($k = 1;$k<=$num_of_lanes;$k++) {
			
				$sql = "SELECT COUNT(*) as closed FROM `range_lanes` WHERE accountnumber='" . $accno . "' AND lane_number='" . $k . "'";
			
				$results = DbHelper::dbFetchRowAssoc($link, $sql);
				
				$closed = $results['closed'];
				
				if ($closed == 6) $buttonColors[$k-1] = 'red';
				
				else if ($closed == 5) $buttonColors[$k-1] = 'yellow';
				
			  else if ($closed == 4) $buttonColors[$k-1] = 'yellow';
				
				else if ($closed == 3) $buttonColors[$k-1] = 'yellow';
				
				else if ($closed == 2) $buttonColors[$k-1] = 'yellow';
				
				else if ($closed == 1) $buttonColors[$k-1] = 'yellow';
				
				else $buttonColors[$k-1] = 'green';
			
			}
			
			
			
			$num_of_rows = ((int)($num_of_lanes / 5));//an array will be indexed by num of rows to track which are open or closed
			
			if (($num_of_lanes % 5) != 0) $num_of_rows++;
			
			$buttonRows = "";
			
			//
			
			
			
			
			$sql = "SELECT num_of_lanes FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$num_of_lanes = $results['num_of_lanes'];
			
			//$buttonColors = array();
			
			//next check each lane for being open or closed - 1 is closed - 0 is open
			
			$closedLanesRows = "";
			
			for ($k = 1;$k<=$num_of_lanes;$k++) {
			
				$sql = "SELECT COUNT(*) as closed FROM `range_lanes` WHERE accountnumber='" . $accno . "' AND lane_number='" . $k . "' AND membership_number='CLOSE'";
			
				$results = DbHelper::dbFetchRowAssoc($link, $sql);
				
				$closed = $results['closed'];
				
				//if ($closed == 2) $buttonColors[$k-1] = 'red';
				
				if ($closed == 1) $closedLanesRows = $closedLanesRows . '<tr>
              <th colspan="5" style="background-color: red;">LANE ' . $k . '------IS CLOSED FOR MAINTENANCE</th></tr>';
				
				if ($closed == 1) $buttonColors[$k-1] = 'red';
				
				//else $buttonColors[$k-1] = 'green';
			
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			//
			
			$bigCounter = 0;
			
			for ($i = 1; $i <= $num_of_rows; $i++){
			
				$buttonRows = $buttonRows . '<tr>
                <td>';
			
				for ($j = 1; $j <= 5; $j++) {
				
					$bigCounter++;
				
					if ($bigCounter <= $num_of_lanes) { 
					
						if ($buttonColors[$bigCounter - 1] != 'red') $mylink = "loginLane(" . $bigCounter . ")";
						
						else $mylink = "";
				
						$buttonRows = $buttonRows . '&nbsp;
						<input type="button" onclick="' . $mylink . '" value="Lane ' . $bigCounter . '" style="height: 35px; width: 80px; text-transform: Uppercase; font-weight: bold; background-color: ' . $buttonColors[$bigCounter - 1] . ';" class="lane_cl">';
					
					}
				
				}
			
				$buttonRows = $buttonRows . '</tr>';
			
			}
			
			////////////////////////////
			
			$sql = "SELECT num_of_lanes FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$num_of_lanes = $results['num_of_lanes'];
			
			$buttonColors = array();
			
			//next check each lane for being open or closed - 1 is closed - 0 is open
			
			for ($k = 1;$k<=$num_of_lanes;$k++) {
			
				$sql = "SELECT COUNT(*) as closed FROM `range_lanes` WHERE accountnumber='" . $accno . "' AND lane_number='" . $k . "' AND membership_number='CLOSE'";
			
				$results = DbHelper::dbFetchRowAssoc($link, $sql);
				
				$closed = $results['closed'];
				
				//if ($closed == 2) $buttonColors[$k-1] = 'red';
				
				if ($closed == 1) $buttonColors[$k-1] = 'red';
				
				else $buttonColors[$k-1] = 'green';
			
			}
			
			$sql = "SELECT * FROM range_lanes WHERE accountnumber='" . $accno . "' AND membership_number!='" . "CLOSE" . "' ORDER BY lane_number";
			
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $sql);
			$usercount = 0;
			
			$rangeUserRows = "";
			
			foreach ($assoc_array as $range_user) {
			
				$rangeUserRows = $rangeUserRows . '
              <tr>';
                //$rangeUserRows = $rangeUserRows . '<td>' . $range_user['employee'] . '</td>';
                $rangeUserRows = $rangeUserRows . '<td>' . $range_user['membership_number'] . '</td>';
                 $rangeUserRows = $rangeUserRows . '<td>' . $range_user['first_name'] . ' ' . $range_user['last_name'] . '</td>
                <td>' . $range_user['lane_number'] . '</td>
                <td>' . $range_user['start_time'] . '</td>';
                //<td style="width: 20%;"><a href="add_member.html">Add Member to Lane</a></td>
                $rangeUserRows = $rangeUserRows . '<td><a href="#" onclick="logmeout('.$range_user['membership_number'].')">Log Out</a></td>
              </tr>';
			  
			  $usercount++;
			
			}
			
			if ($usercount == 0) $rangeUserRows = '<tr>
                <td colspan="7"> <span style="color: red;">There are no members currently on the range.</span> </td>
              </tr>';
			
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please select an employee!"]);
			
			
            $data = [
				'buttonRows' => $buttonRows,
				'rangeUserRows' => $rangeUserRows,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'member_no' => $member_id,
				'closedLanesRows' => $closedLanesRows,
				'err' => $err,
			//'temp' => $range_owner,
            ];
            
            $content = $app['templating']->render('range/laneusage/select_lane.php', $data);
            $breadcrumbs = $this->getBreadcrumbs($app);
            $breadcrumbs[1]['link'] = $app['get_range_url']('').'customers';
			$breadcrumbs[1]['title'] = 'Members';
			$breadcrumbs[2]['title'] = 'Login Select Lane';
            $pageTitle = 'Range Controller - Members - Login Select Lane';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
					
		
		}
		
		public function logThemIn (Application $app) {
		
			$first_name = $app['request']->get('first_name', '');
		
			$last_name = $app['request']->get('last_name', '');
		
			$start_time = $app['request']->get('start_time', '');
			
			$lane_no = $app['request']->get('lane_no', '');
			
			$member_no = $app['request']->get('member_no', '');
			
			$employee_id = $app['request']->get('employee_id', '');
			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			
			$sql = "SELECT * FROM employees WHERE id=" . $employee_id;
			
			$results = DbHelper::dbFetchRowAssoc($link, $sql);
			
			$employee_name = $results['initials'];
			
			$sql = "INSERT INTO range_lanes (acctnumberhashed, accountnumber, membership_number, first_name, last_name, lane_number, start_time, employee) VALUES ('" . $hash . "', '" . $accno . "', '" . $member_no . "', '" . $first_name . "', '" . $last_name . "', '" . $lane_no . "', '" . $start_time . "', '" . $employee_name . "')";
			
			$link->query($sql);
			
			$sql = "INSERT INTO range_time_tracker (acctnumberhashed, accountnumber, membership_number, first_name, last_name, lane_number, start_time, end_time, date_stamp, employee_name) VALUES ('" . $hash . "', '" . $accno . "', '" . $member_no . "', '" . $first_name . "', '" . $last_name . "', '" . $lane_no . "', '" . $start_time . "', '0','" . date('n/j/y') . "', '" . $employee_name . "')";
			
			$link->query($sql);
			
			$sql = "Update customers set is_login=1 where accountnumber=$accno and membership_number=$member_no";
			$link->query($sql);
			
			
			return $sql;
			
		
		}
		
		public function logThemOut (Application $app, $member_no) {
		/*echo "<script>
		var date = new Date();
		var hours = date.getHours();
		var minutes = '0' + date.getMinutes();
		var seconds = '0' + date.getSeconds();
		var formattedTime = hours + ':' + minutes.substr(minutes.length-2) + ':' + seconds.substr(seconds.length-2);
		document.cookie =  'end_time =' + formattedTime ;
		</script>";*/
		
			//$_COOKIE['end_time'];
			
			$roid = $app['range_owner_id'];
			
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);		
			
			$sql = "DELETE FROM range_lanes WHERE membership_number='" . $member_no . "'";
			$link->query($sql);
			
			$sql = "UPDATE range_time_tracker SET end_time = '" . $_COOKIE['end_time'] . "' WHERE end_time = '0' AND accountnumber='" . $accno . "' AND membership_number='" . $member_no . "'";
			
			$link->query($sql);
			
			$sql = "Update customers set `is_login`=0,`visitcount`=`visitcount`+1 where accountnumber=$accno and membership_number=$member_no";
			$link->query($sql);
			
			
			setcookie ("end_time", "", time() - 3600);
			return '<script>window.location = "/xapp/currentRangeUsage";</script>';
			
		}
		
	}
	
}