<?php
namespace Rc\Controllers\Range {
    use Silex\Application;
    use Rc\Services;
    use Rc\Models;
    use Rc\Services\DateTimeHelper;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Validator\Constraints as Assert;
    use Rc\Models\range_owner;
    use Rc\Models\range_member;
    //use Rc\Models\browser;
    use Rc\Models\range_owner_id;
    use Rc\Models\server_email_template_types;
    use Rc\Models\server_email_templates;
    use Rc\Services\StringHelper;
    use Rc\Services\DbHelper;
    use Rc\Services\CommonData;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Symfony\Component\Finder\Finder;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    //include('//includes/browser.php');
    class ConfigController {
        protected $layoutPath;
        protected $pageSize;
        public function __construct() {
            $this->layoutPath = 'layout-range.php';
            $this->pageSize = 12;
        }
	 public function landing_configurations(Application $app){
			
			$content = $app['templating']->render('range/configurations/list.php');
            $breadcrumbs = $this->getBreadcrumbs_config($app);
            $breadcrumbs[1]['link'] = '';
            $pageTitle = 'Range Controller - Module Configurations';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
		}
	public function memberBenefitsConfig(Application $app, $memtypeid){
			if (!isset($memtypeid)) $memtypeid = 0;
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$query_sql = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "' AND acctnumberhashed='" . $hash . "'";
			//return $insert_sql;
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $query_sql);
			if ($memtypeid == 0) $membershipOptions = "<option selected value=\"0\">Select a membership type : </option>";
			else $membershipOptions = "<option value=\"0\">Select a membership type : </option>";
			foreach ($assoc_array as $optionsArr) {
				if ($optionsArr['membership_type_id'] == $memtypeid) {
				$membershipOptions = $membershipOptions . "<option selected value='" . $optionsArr['membership_type_id'] . "'>" . $optionsArr['membership_name'] . "</option>";
				$memTypeName = $optionsArr['membership_name'];
				}
				else $membershipOptions = $membershipOptions . "<option value='" . $optionsArr['membership_type_id'] . "'>" . $optionsArr['membership_name'] . "</option>";
			}
			//
			//
			$qsql = "SELECT * FROM benefits WHERE membership_type_id=" . $memtypeid;
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $qsql);
			$benefitsFields = "";
			$count = 0;
			foreach ($assoc_array as $fieldsArr) {
			$count++;//user for css id and for form submit
				$benefitsFields = $benefitsFields . '<tr style="display:none;" id="fieldno-'. $count . '">
                <td><input type="text" id="quantity-' . $count . '" style="width:50px;" value="' . $fieldsArr['quantity'] . '"></td>
                  <td><select id="timeframe-' . $count . '"> <option value=""> </option>';
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Expiration") ? '<option selected value="Expiration">Expiration</option>' : '<option value="Expiration">Expiration</option>');
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Yearly") ? '<option selected value="Yearly">Yearly</option>' : '<option value="Yearly">Yearly</option>');
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Monthly") ? '<option selected value="Monthly">Monthly</option>' : '<option value="Monthly">Monthly</option>');
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Weekly") ? '<option selected value="Weekly">Weekly</option>' : '<option value="Weekly">Weekly</option>');
                      $benefitsFields = $benefitsFields . (($fieldsArr['duration'] == "Daily") ? '<option selected value="Daily">Daily</option>' : '<option value="Daily">Daily</option>');
                    $benefitsFields = $benefitsFields . '</select></td>
                  <td><input id="benefit-name-' . $count . '" type="text" style="width:450px;" value="' . $fieldsArr['name'] .  '"></td>
				  <input type="hidden" id="benefit-id-' . $count . '" name="benefit_id" value="' . $fieldsArr['id'] . '"/>
                </tr>';
			}
			while ($count < 9) {
				$count++;
				$benefitsFields = $benefitsFields . '<tr style="display:none;" id="fieldno-' . $count . '">
                <td><input type="text" id="quantity-' . $count . '" style="width:50px;" value="' . '' . '"></td>
                  <td><select id="timeframe-' . $count . '">
					  <option selected value=""> </option>
                      <option value="Expiration">Expiration</option>
                      <option value="Yearly">Yearly</option>
                      <option value="Monthly">Monthly</option>
                      <option value="Weekly">Weekly</option>
                      <option value="Daily">Daily</option>
                    </select></td>
                  <td><input id="benefit-name-' . $count . '" type="text" style="width:450px;" value="' . '' .  '"></td>
				  <input type="hidden" id="benefit-id-' . $count . '" name="benefit_id" value="' . '0' . '"/>
                </tr>';
			}
			//
			//
			if ($memtypeid == 0) $benefitsFields = "<tr><td colspan=3>Please select a membership type to create benefits here!</td></tr>";
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields in each row you use are completed!"]);
            $data = [
                'membershipOptions' => $membershipOptions,
				'benefitsFields' => $benefitsFields,
				'memtypename' => $memTypeName,
				'memtypeid' => $memtypeid,
				'err' => $err,
            ];            
            $content = $app['templating']->render('range/config/benefits_config.php', $data);                   ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Member Benefits']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Member Benefits Configuration';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		function addOrUpdateBenefit (Application $app, $membership_type_id, $quantity, $duration, $benefit_text, $benefit_id) {
			//return "SAASFA";
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			//if benefit id is set (or not 0) use update
			//if not, use insert
			if ((int) $benefit_id != 0) $qsql = "UPDATE benefits SET name='" . $benefit_text . "', duration='" . $duration . "', quantity=" . $quantity . " WHERE id='" . $benefit_id . "'";
			else $qsql = "INSERT INTO benefits (membership_type_id, name, duration, quantity, account_number, acct_number_hashed) VALUES (" . $membership_type_id . ", '" . $benefit_text . "', '" . $duration . "', " . $quantity . ", '" . $accno . "', '" . $hash . "')";
			$link->query($qsql);
		}
		public function listEmployees(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM employees WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);
			$count = 0;
			foreach ($assoc_array as $employeeRow) {
				$count++;
				$employeeRows = $employeeRows . '<tr><td>' . $count . '</td>
									<td>' . $employeeRow['fname'] . ' ' . $employeeRow['lname'] . '</td>
									<td>' . $employeeRow['initials'] . '</td>
									<td><a href="/xapp/config/employees/manage/' . $employeeRow['id'] . '">Manage</a></td>
									<td><a href="/xapp/config/employees/delete/' . $employeeRow['id'] . '" onclick="return myConfirm(\'Are you sure you want to delete this employee?\', ' . $employeeRow['id'] . ')">Delete</a></td>
								</tr>';	
			}
            $data = [
				'employeeRows' => $employeeRows,
            ];            
            $content = $app['templating']->render('range/config/employees.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Employees']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Employees';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		 public function listBillingCobfig(Application $app){
            $content = $app['templating']->render('range/billing/list.php');
            $breadcrumbs = $this->getBreadcrumbs_config($app);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $breadcrumbs[2]['link'] = '';
            $breadcrumbs[2]['title'] = 'Billing';            
            $pageTitle = 'Range Controller - Configuration - Billing';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));    
        }
		public function billingPaymentType(Application $app){
            $range_owner = $app['range_owner'];
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM payment_types WHERE accountnumber='$accno' and acctnumberhashed='$hash'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);			
            $data = [
				'payment_types' => $assoc_array,
            ];
    
            $content = $app['templating']->render('range/billing/paymentType.php',$data);
            $breadcrumbs = $this->getBreadcrumbs_config($app);            
            
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $breadcrumbs[2]['link'] = '/xapp/config/billing';
            $breadcrumbs[2]['title'] = 'Billing';
            $breadcrumbs[3]['link'] = '';
            $breadcrumbs[3]['title'] = 'Payment Type'; 
            $pageTitle = 'Range Controller - Configuration - Billing - Payment For';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));        
        }
		public function add_editBillingPaymentType(Application $app, $type_id=""){            
            $range_owner = $app['range_owner'];
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
            $b_title = 'Add';
            if ($app['request']->get('cmd', '') === 'add') {
                $contactFields = ['payment_type'];
                $contactData = $this->getPostForFields($contactFields, $app['request']);    
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $msql = "INSERT INTO payment_types (type_title,accountnumber,acctnumberhashed) VALUES ('" . $contactData['payment_type'] . "','" . $accno . "','" . $hash . "')";			
			    $link->query($msql);                
                return $app->redirect($app['get_range_url']('config/billing/paymentType'));
            }
            
            if ($app['request']->get('cmd', '') === 'edit') {                
                $contactFields2 = ['payment_type','type_id'];
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $contactData = $this->getPostForFields($contactFields2, $app['request']);                
                $msql = "UPDATE payment_types SET type_title='" . $contactData['payment_type'] . "' WHERE type_id='" . $contactData['type_id'] . "' and accountnumber='" . $accno . "' and acctnumberhashed='". $hash ."'";			
			    $link->query($msql);                
                
                return $app->redirect($app['get_range_url']('config/billing/paymentType'));
            }            
            $type_title = '';
            if($type_id != ''){
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $msql = "SELECT * FROM payment_types WHERE type_id=$type_id";
                $assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);
                
                $type_title = $assoc_array[0]['type_title'];
                $b_title = 'Manage';                
            }           
            
            $err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please enter payment type!"]);			
            $data = [
				'err' => $err,
                'type_id' => $type_id,
                'type_title' => $type_title               
            ];
             
            $content = $app['templating']->render('range/billing/paymentType_add_edit.php', $data);
            $breadcrumbs = $this->getBreadcrumbs_config($app);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $breadcrumbs[2]['link'] = '/xapp/config/billing';
            $breadcrumbs[2]['title'] = 'Billing';
            $breadcrumbs[3]['link'] = '/xapp/config/billing/paymentType';
            $breadcrumbs[3]['title'] = 'Payment Type';
            $breadcrumbs[4]['link'] = '';            
            $breadcrumbs[4]['title'] = $b_title;
            $pageTitle = 'Range Controller - Configuration - Billing - Payment Type - '.$b_title;
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));         
        }
		public function checkBillingPaymentType(Application $app){
            $db_type_title = '';
            $contactFields2 = ['id','title'];
            $type_id = '';
            $type_title = '';
            $contactData = $this->getPostForFields($contactFields2, $app['request']);                
            /*echo '<pre>';
            print_r($contactData);
            echo '</pre>';
            die;*/
            $type_id = $contactData['id'];
            $type_title = $contactData['title'];
            if($type_id != ''){
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $msql = "SELECT * FROM payment_types WHERE type_id NOT IN ($type_id) AND type_title = '$type_title'";
                $assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);                
                $db_type_title = $assoc_array[0]['type_title'];        
                if($db_type_title != ''){
                    echo 'true';
                } else {
                    echo 'false';
                }        
            } else {                
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $msql = "SELECT * FROM payment_types WHERE type_title = '$type_title'";
                $assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);                
                $db_type_title = $assoc_array[0]['type_title'];
                if($db_type_title != ''){
                    echo 'true';
                } else {
                    echo 'false';
                }     
            }
            die;        
        }
		public function deleteBillingPaymentType(Application $app ,$type_id){
			$range_owner = $app['range_owner'];
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $msql = "DELETE FROM payment_types WHERE type_id = '$type_id' and accountnumber='$accno' and acctnumberhashed='$hash'";            
            $link->query($msql);
            return $app->redirect($app['get_range_url']('config/billing/paymentType'));
        }
		public function billingPaymentFor(Application $app){     
			$range_owner = $app['range_owner'];
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$msql = "SELECT * FROM payment_types WHERE accountnumber='$accno' and acctnumberhashed='$hash'";
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM payment_for WHERE accountnumber='$accno' and acctnumberhashed='$hash'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);			
            $data = [
				'payment_for' => $assoc_array,
            ];
            
            $content = $app['templating']->render('range/billing/paymentFor.php',$data);
            $breadcrumbs = $this->getBreadcrumbs_config($app);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $breadcrumbs[2]['link'] = '/xapp/config/billing';
            $breadcrumbs[2]['title'] = 'Billing';
            $breadcrumbs[3]['link'] = '';
            $breadcrumbs[3]['title'] = 'Payment For'; 
            $pageTitle = 'Range Controller - Configuration - Billing - Payment For';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));        
        }
        
        public function add_editBillingPaymentFor(Application $app, $for_id=""){            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
            $b_title = 'Add';
            if ($app['request']->get('cmd', '') === 'add') {
                $contactFields = ['payment_for'];
                $contactData = $this->getPostForFields($contactFields, $app['request']);    
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $msql = "INSERT INTO payment_for (for_title,accountnumber,acctnumberhashed) VALUES ('" . $contactData['payment_for'] . "','" . $accno . "','" . $hash . "')";		
			    $link->query($msql);                
                return $app->redirect($app['get_range_url']('config/billing/paymentFor'));
            }
            
            if ($app['request']->get('cmd', '') === 'edit') {                
                $contactFields2 = ['payment_for','for_id'];
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $contactData = $this->getPostForFields($contactFields2, $app['request']);                
                $msql = "UPDATE payment_for SET for_title='" . $contactData['payment_for'] . "' WHERE for_id='" .   $contactData['for_id'] . "' and accountnumber='" . $accno . "' and acctnumberhashed='". $hash ."'";
			    $link->query($msql);                
                return $app->redirect($app['get_range_url']('config/billing/paymentFor'));
            }            
            $for_title = '';
            if($for_id != ''){
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $msql = "SELECT * FROM payment_for WHERE for_id=$for_id";
                $assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);
                
                $for_title = $assoc_array[0]['for_title'];
                $b_title = 'Manage';                
            }           
            
            $err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please enter payment for!"]);			
            $data = [
				'err' => $err,
                'for_id' => $for_id,
                'for_title' => $for_title               
            ];
             
            $content = $app['templating']->render('range/billing/paymentFor_add_edit.php', $data);
            $breadcrumbs = $this->getBreadcrumbs_config($app);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $breadcrumbs[2]['link'] = '/xapp/config/billing';
            $breadcrumbs[2]['title'] = 'Billing';
            $breadcrumbs[3]['link'] = '/xapp/config/billing/paymentFor';
            $breadcrumbs[3]['title'] = 'Payment For';
            $breadcrumbs[4]['link'] = '';            
            $breadcrumbs[4]['title'] = $b_title;
            $pageTitle = 'Range Controller - Configuration - Billing - Payment For - '.$b_title;
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));         
        }
        
        public function checkBillingPaymentFor(Application $app){
            $db_for_title = '';
            $contactFields2 = ['id','title'];
            $for_id = '';
            $for_title = '';
            $contactData = $this->getPostForFields($contactFields2, $app['request']);                
            /*echo '<pre>';
            print_r($contactData);
            echo '</pre>';
            die;*/
            $for_id = $contactData['id'];
            $for_title = $contactData['title'];
            if($for_id != ''){
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $msql = "SELECT * FROM payment_for WHERE for_id NOT IN ($for_id) AND type_title = '$for_title'";
                $assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);                
                $db_for_title = $assoc_array[0]['for_title'];        
                if($db_for_title != ''){
                    echo 'true';
                } else {
                    echo 'false';
                }        
            } else {                
                $link = DbHelper::getWrappedHandle($app['dbs']['main']);
                $msql = "SELECT * FROM payment_for WHERE for_title = '$for_title'";
                $assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);                
                $db_for_title = $assoc_array[0]['for_title'];
                if($db_for_title != ''){
                    echo 'true';
                } else {
                    echo 'false';
                }     
            }
            die;        
        }
        
        public function deleteBillingPaymentFor(Application $app ,$for_id){
            
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
            $link = DbHelper::getWrappedHandle($app['dbs']['main']);
            $msql = "DELETE FROM payment_for WHERE for_id = '$for_id' and accountnumber='$accno' and acctnumberhashed='$hash'";            
            $link->query($msql);
            $scc = 'Record deleted successfully.';
            return $app->redirect($app['get_range_url']('config/billing/paymentFor'));
        }
		public function listMemberships(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$membershipRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "'";
			$assoc_array = DbHelper::dbFetchAllAssoc($link, $msql);
			
			foreach ($assoc_array as $membership_row) {
				$membershipRows = $membershipRows . ' <tr>
                  <td>' . $membership_row['membership_name'] . '</td>
                  <td>$' . $membership_row['membership_price'] . '</td>
                  <td>' . $membership_row['membership_length'] . '</td>
                  <td>' . $membership_row['payment_required'] . '</td>
                  <td><a href="/xapp/config/memberships/manage/' . $membership_row['membership_type_id'] . '">Manage</a></td>
				  <td><a href="/xapp/config/memberships/delete/' . $membership_row['membership_type_id'] . '" onclick="return myConfirm(\'Are you sure you want to delete this membership?\', ' . $membership_row['membership_type_id'] . ')">Delete</a></td>
                </tr>';		
			}
			$count = 0;
			//$membershipRows = "";
            $data = [
				'membershipRows' => $membershipRows,
              
            ];            
            $content = $app['templating']->render('range/config/memberships.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Memberships']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Memberships';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function addEmployee(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
            $records= $range_owner->getMembers([], $pager);
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);			
            $data = [
				'err' => $err,
                
            ];            
            $content = $app['templating']->render('range/config/add_employee.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['link' => '/xapp/config/employees', 'title'=>'Employees'], ['title' => 'Add Employee']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Employees';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function manageEmployee(Application $app, $employee_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM employees WHERE accountnumber='" . $accno . "' AND id='" . $employee_id . "'";
			$assoc_array = DbHelper::dbFetchRowAssoc($link, $msql);
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);
            $data = [
				'employee_id' => $employee_id,
				'fname' => $assoc_array['fname'],
				'lname' => $assoc_array['lname'],
				'initials' => $assoc_array['initials'],
				'err' => $err,
                
            ];            
            $content = $app['templating']->render('range/config/manage_employee.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['link' => '/xapp/config/employees', 'title'=>'Employees'], ['title' => 'Manage Employee']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Employees';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function addMembership(Application $app){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $pager = new \Rc\Services\Pager();
            /**
             * @var \Rc\Services\PagedResults
             */
            $records= $range_owner->getMembers([], $pager);
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);
            $data = [
				'err' => $err,
              
            ];            
            $content = $app['templating']->render('range/config/add_membership.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['link' => '/xapp/config/memberships', 'title'=>'Memberships'], ['title' => 'Add Membership']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Memberships';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function manageMembership(Application $app, $membership_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
            $roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM editable_memberships WHERE accountnumber='" . $accno . "' AND membership_type_id='" . $membership_id . "'";
			$assoc_array = DbHelper::dbFetchRowAssoc($link, $msql);
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);
            $data = [
				'err' => $err,
				'membership_id' => $membership_id,
				'mname' => $assoc_array['membership_name'],
				'mprice' => $assoc_array['membership_price'],
				'mlength' => $assoc_array['membership_length'],
				'mpayment' => $assoc_array['payment_required'],
            ];            
            $content = $app['templating']->render('range/config/manage_membership.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['link' => '/xapp/config/memberships', 'title'=>'Memberships'], ['title' => 'Manage Membership']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Memberships';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function insertEmployee(Application $app, $fname, $lname, $initials, $employee_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			if ($employee_id == 0) $msql = "INSERT INTO employees (acctnumberhashed, accountnumber, fname, lname, initials) VALUES ('" . $hash . "', '" . $accno . "', '" . $fname . "', '" . $lname . "', '" . $initials . "')";
			else $msql = "UPDATE employees SET fname='" . $fname . "', lname='" . $lname . "', initials='" . $initials . "' WHERE id=" . $employee_id;
			$link->query($msql);
			die;
        }
		public function insertMembership(Application $app, $name, $price, $length, $payment_required, $membership_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			if ($membership_id == 0) $msql = "INSERT INTO editable_memberships (acctnumberhashed, accountnumber, membership_name, membership_price, membership_length, payment_required) VALUES ('" . $hash . "', '" . $accno . "', '" . $name . "', '" . $price . "', '" . $length . "', '" . $payment_required . "')";
			else $msql = "UPDATE editable_memberships SET membership_name='" . $name . "', membership_price='" . $price . "', membership_length='" . $length . "', payment_required='" . $payment_required . "' WHERE membership_type_id=" . $membership_id;
			$link->query($msql);
        }
		public function deleteEmployee(Application $app, $employee_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "DELETE FROM employees WHERE id=" . $employee_id;
			$link->query($msql);
			return '<script>window.location = "https://rangecontroller.com/xapp/config/employees"</script>';
        }
		public function deleteMembership(Application $app, $membership_id){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "DELETE FROM editable_memberships WHERE membership_type_id=" . $membership_id;
			$link->query($msql);
			return '<script>window.location = "https://rangecontroller.com/xapp/config/memberships"</script>';
        }
		public function configRC(Application $app){
            $range_owner = $app['range_owner'];  // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "SELECT * FROM controller_config WHERE accountnumber='" . $accno . "'";
			$configs = DbHelper::dbFetchRowAssoc($link, $msql);
			$nsql = "SELECT * FROM hours_of_operation WHERE accountnumber='" . $accno . "'";
			$hoo = DbHelper::dbFetchRowAssoc($link, $nsql);
			$count = 0;
			$err = $app['templating']->render('range/error-message.php', ['message'=>"Validation Error : Please double-check that all fields are completed!"]);	
            $data = [
				//'employeeRows' => $employeeRows,
				'errmsg' => $err,
				'email' => $configs['registered_email'],
				'companyname' => $configs['company_name'],
				'company_address' => $configs['company_address'],
				'company_city' => $configs['company_city'],
				'company_state' => $configs['company_state'],
				'company_zipcode' => $configs['company_zipcode'],
				'company_phone' => $configs['company_phone'],
				'accountno' => $accno,
				'password' => $configs['password'],
				'companylogo' => $configs['company_logo'],
				'taxrate' => $configs['tax_rate'],
				'numberoflanes' => $configs['number_of_lanes'],
				'hasmemberships' => $configs['has_memberships'],
				'startcount' => $configs['member_number_start_count'],
				'time_zone' => $configs['time_zone'],
				'sunday_from' => $hoo['sunday_from'],
				'sunday_to' => $hoo['sunday_to'],
				'monday_from' => $hoo['monday_from'],
				'monday_to' => $hoo['monday_to'],
				'tuesday_from' => $hoo['tuesday_from'],
				'tuesday_to' => $hoo['tuesday_to'],
				'wednesday_from' => $hoo['wednesday_from'],
				'wednesday_to' => $hoo['wednesday_to'],
				'thursday_from' => $hoo['thursday_from'],
				'thursday_to' => $hoo['thursday_to'],
				'friday_from' => $hoo['friday_from'],
				'friday_to' => $hoo['friday_to'],
				'saturday_from' => $hoo['saturday_from'],
				'saturday_to' => $hoo['saturday_to'],
            ];            
            $content = $app['templating']->render('range/config/configuration.php', $data)
                    ;
            $breadcrumbs = array(['link' => $app['get_range_url']('').'dashboard', 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations'], ['title'=>'Range Controller Settings']);
            $breadcrumbs[1]['link'] = '/xapp/configurations';
            $pageTitle = 'Range Controller - Configuration - Range';
            return $this->wrapInLayout($app, 
                    compact('content', 'breadcrumbs', 'pageTitle'));
        }
		public function updateRC(Application $app, $email, $password, $companyname, $company_address, $company_city, $company_state, $company_zipcode, $company_phone, $taxrate, $numberoflanes, $sunday_from, $sunday_to, $monday_from, $monday_to, $tuesday_from, $tuesday_to, $wednesday_from, $wednesday_to, $thursday_from, $thursday_to, $friday_from, $friday_to, $saturday_from, $saturday_to, $time_zone){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$msql = "UPDATE controller_config SET registered_email='" . $email . "', company_name='" . $companyname .  "', company_address='".$company_address."', company_city='".$company_city."', company_state= '".$company_state."', company_zipcode= '".$company_zipcode."', company_phone= '".$company_phone."', password='" . $password . "',  tax_rate='" . $taxrate . "', number_of_lanes='" . $numberoflanes . "', time_zone='" . $time_zone . "' WHERE accountnumber='" . $accno . "'";
			$link->query($msql);
			$nsql = "UPDATE hours_of_operation SET sunday_from='" . $sunday_from . "', sunday_to='" . $sunday_to . "', monday_from='" . $monday_from . "', monday_to='" . $monday_to . "', tuesday_from='" . $tuesday_from . "', tuesday_to='" . $tuesday_to . "', wednesday_from='" . $wednesday_from . "', wednesday_to='" . $wednesday_to . "', thursday_from='" . $thursday_from . "', thursday_to='" . $thursday_to ."', friday_from='" . $friday_from . "', friday_to='" . $friday_to . "', saturday_from='" . $saturday_from . "', saturday_to='" . $saturday_to . "', num_of_lanes='" . $numberoflanes . "' WHERE accountnumber='" . $accno . "'";
			$link->query($nsql);
			$pwsql = "UPDATE admin_login SET  current_pw='" . $password . "' WHERE accountnumber='" . $accno . "'";
			$link->query($pwsql);
			$count = 0;
			return $msql . " " . $nsql;
        }
		public function updateImage(Application $app, $ext){
            $range_owner = $app['range_owner']; // \Rc\Models\RangeOwner::initWithAccount($link, $accHashKey, $accNo);//
			$roid = $app['range_owner_id'];
			$accno = $roid->getAccountNo();	
			$hash = $roid->getAccountHashKey();
			//$employeeRows = "";
			$link = DbHelper::getWrappedHandle($app['dbs']['main']);
			$qry = "UPDATE controller_config SET company_logo='logo." . $ext . "' WHERE accountnumber='" . $accno . "'";
			$link->query($qry);
			}
	private function wrapInLayout(Application $app, $data ){
            $ro = $app['range_owner'];
            //$ro = new Models\RangeOwner;
            $breadcrumbsContent = $app['templating']->render(
                    'range/customer/breadcrumbs.php', 
                    ['breadcrumbs' => $data['breadcrumbs']]);
            $vars = [
                'pageTitle' => isset($data['pageTitle']) 
                    ? $data['pageTitle'] : 'Range Controller',
                'content' => $breadcrumbsContent . $data['content'],
                'companyName' => $ro->getCompanyName(),
                'rangeLogoUrl' => $app['range_logo_url']
            ];
            return $app['templating']->render('range/layout-range.php', $vars);
        }
        private function getBreadcrumbs(Application $app){
            return array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Members']
            );
        }
		private function getBreadcrumbs_config(Application $app){
            return array(
                ['link' => $app['get_range_url']('dashboard'), 'title'=>'Home'],
                ['link' => $app['get_range_url']('customers'), 'title'=>'Module Configurations']
            );
        }
	}
}