<?php
include 'includes/session.php';
?>

  <!DOCTYPE HTML>

  <html>

  <head>
    <title>Range Controller - Privacy Policy</title>
    <?php
include 'includes/headerTags.php';
?>
  </head>

  <body>
    <?php
include 'includes/header.php';
?>
      <?php
include 'includes/navBar.php';
?>
        <div id="privacy">
          <h1>Privacy Policy</h1>
          <p>Your satisfaction and sense of privacy is a priority for us, so we take a number of steps to ensure your safety and to give you a pleasurable shopping experience with us. RangeController.com will offer a service for sale on this website, other
            third party sites as well as on the forum/chat/communications sections of the sites RangeController.com and swatclasses.com. Any and all information collected on these sites are your property and will not be used or sold except by RangeController.com
            for special offers or other information purposes. We do not sell, lend or loan client information out to a third party for any reason, we guarantee it! </p>
          <h3>Credit Card Numbers </h3>
          <p>Your credit card transaction is conducted through a secure server. Your credit card number is stored only long enough for a charge to be made to it. Once the charge is completed, the number is removed from our server to guarantee your safety
            and privacy. Other forms of acceptable payment would be filtered through a Paypal shopping cart (the nations leading online payment gateway) or our own Eprocessing Network gateway. RangeController.com and swatclasses.com may complete recurring
            billing statements depending on your subscription level. This is a preference the end user selects. RangeController.com, Dave&#39;s Pawn Shop&#39;s Rockinguns.com (shown on your credit card statement as the biller) and swatclasses.com comply
            with all local and federal PCI compliance regulations concerning your personal credit card information and we are audited each year to stay abreast of these regulations. All data is encrypted and never stored. </p>
          <h3>E-mail Addresses </h3>
          <p>We understand the annoyance of receiving junk e-mail. So, we want you to know that your e-mail address will never be given out or sold to any outside party, even ones that you might be interested in. This ensures that when you give your e-mail
            address to us for ordering or account purposes, you will not receive any other e-mail resulting from that action. You will not even receive emails from RangeController.com, Dave&#39;s Pawn Shop or swatclasses.com unless you want to and request
            this action in advance. </p>
          <p>Our regular e-mails are limited to order confirmations, customer questions, and possibly an optional newsletter. If you have an account with us, you may choose to not receive our newsletter if we offer one. </p>
          <h3>Personal Information </h3>
          <p>We never give out your e-mail address, as described above, and the same applies for your personal information. Any names or addresses provided will only be used by you the customer, and by us, davespawnshop.com dba RangeController.com for ordering
            or shipping purposes only.</p>
        </div>
        <?php
include 'includes/content.php';
?>
          <?php
include 'includes/footer.php';
?>
  </body>

  </html>