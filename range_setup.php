<?php	                                       			
session_start();
require('common/db.php');

if(isset($_SESSION['hn'])){
$sql = "";
$sql = "SELECT * FROM controller_config WHERE acctnumberhashed = '".$_SESSION['hn']."' AND accountnumber = '".$_SESSION['an']."' "; 
if ($result = $link->query($sql)) { 
while ($row = $result->fetch_assoc()) { 
$company_name = $row['company_name']; 
}//END WHILE LOOP 
}//END IF
}//END IF SESSION

?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="carlos r garcia" />
    
	<title>Progress - Range Controller Setup</title>
<style>
.hide { position:absolute; visibility:hidden; }
.show { position:absolute; visibility:visible; }

#dialog p{
color: white;
}

#dialog label{
color: white;
}

.bg {/*for background image*/
	width: 100%;
	height: 100%;
	position: fixed;
	top: 0;
	left: 0;
	z-index: -5000;
}
body {
	background:url(../images/background.jpg) repeat; /*background-color:#000;*/
	font:13px/20px Arial, Helvetica, sans-serif;
	color:#78706a;
	position:relative;/*min-width:960px;*/
}

.box {
    width:300px;
    height:300px;    
    position:fixed;
    margin-left:-150px; /* half of width */
    margin-top:-150px;  /* half of height */
    top:50%;
    left:50%;
}

.payment_button{
width: 60px;
height: 25px;
cursor: pointer;
/*round corners*/
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.payment_button:hover{
background-color: green;
color: white;
}
</style>

<script lang="JavaScript">

var duration=25;// Specify duration of progress bar in seconds
var _progressWidth = 100;	// Display width of progress bar.
var _progressBar = "||||||||||||||||||||"

var _progressEnd = 5;
var _progressAt = 0;


// Create and display the progress dialog.
// end: The number of steps to completion
function ProgressCreate(end) {
// Initialize state variables
_progressEnd = end;
_progressAt = 0;

// Move layer to center of window to show
if (document.all) {	// Internet Explorer
progress.className = 'show';
progress.style.left = (document.body.clientWidth/2) - (progress.offsetWidth/2);
progress.style.top = document.body.scrollTop+(document.body.clientHeight/2) - (progress.offsetHeight/2);
} else if (document.layers) {	// Netscape
document.progress.visibility = true;
document.progress.left = (window.innerWidth/2) - 100+"px";
document.progress.top = pageYOffset+(window.innerHeight/2) - 40+"px";
} else if (document.getElementById) {	// Netscape 6+
document.getElementById("progress").className = 'show';
document.getElementById("progress").style.left = (window.innerWidth/2)- 100+"px";
document.getElementById("progress").style.top = pageYOffset+(window.innerHeight/2) - 40+"px";
}

ProgressUpdate();	// Initialize bar
}

// Hide the progress layer
function ProgressDestroy() {
// Move off screen to hide
if (document.all) {	// Internet Explorer
progress.className = 'hide';
} else if (document.layers) {	// Netscape
document.progress.visibility = false;
} else if (document.getElementById) {	// Netscape 6+
document.getElementById("progress").className = 'hide';
}
}

// Increment the progress dialog one step
function ProgressStepIt() {
_progressAt++;
if(_progressAt > _progressEnd) _progressAt = _progressAt % _progressEnd;
ProgressUpdate();
}

// Update the progress dialog with the current state
function ProgressUpdate() {
var n = (_progressWidth / _progressEnd) * _progressAt;
if (document.all) {	// Internet Explorer
var bar = dialog.bar;
} else if (document.layers) {	// Netscape
var bar = document.layers["progress"].document.forms["dialog"].bar;
n = n * 0.55;	// characters are larger
} else if (document.getElementById){
var bar=document.getElementById("bar")
}
var temp = _progressBar.substring(0, n);
bar.value += temp;
//document.getElementById("lblProgress").innerHTML = "";
}

// Demonstrate a use of the progress dialog.
function Demo() {
ProgressCreate(10);
window.setTimeout("Click()", 100);
}

function Click() {
if(_progressAt >= _progressEnd) {
//ProgressDestroy();
return;
}
ProgressStepIt();
window.setTimeout("Click()", (duration-1)*1000/10);
}

function CallJS(jsStr) { //v2.0
return eval(jsStr)
}//end function

var count=30;
function startTimer(){var counter=setInterval(timer, 500);}//end function
function timer(){
count = count - 1;
if (count <= -1){clearInterval(counter);return;}//end if
document.getElementById("timer").innerHTML = count;

var db = document.getElementById("lblCreatingDatabase");
var insert = document.getElementById("lblInsertValues");
var folders = document.getElementById("lblFoldersAndFiles");
var email = document.getElementById("lblSendEmail");
var finalize = document.getElementById("lblFinalizing");
var done = document.getElementById("lblComplete");
//&#x2717  x
//&#x2713; checkmark

//var lbl = document.getElementById("lblProgress");
if(count==29){db.innerHTML = "<font color='red'>Stand by...</font>&nbsp;Creating Range Controller Database";}
if(count==25){insert.innerHTML = "<font color='red'>Stand by...</font>&nbsp;Inserting Database Entries";db.innerHTML = "<font color='yellow'>&#x2713;</font>&nbsp;Creating Range Controller Database";}
if(count==19){folders.innerHTML = "<font color='red'>Stand by...</font>&nbsp;Creating Folder and Loading Files";insert.innerHTML = "<font color='yellow'>&#x2713;</font>&nbsp;Inserting Database Entries";}
if(count==15){email.innerHTML = "<font color='red'>Stand by...</font>&nbsp;Sending Email Confirmation";folders.innerHTML = "<font color='yellow'>&#x2713;</font>&nbsp;Creating Folder and Loading Files";}
if(count==10){finalize.innerHTML = "<font color='red'>Stand by...</font>&nbsp;Finalizing Setup...";email.innerHTML = "<font color='yellow'>&#x2713;</font>&nbsp;Sending Email Confirmation";}
if(count==5) {done.innerHTML = "Setup Complete, Press OK to view your settings.<div align='right'><input type='button' value='OK' onclick='login();' class='payment_button'>&emsp;</div>";finalize.innerHTML = "<font color='yellow'>&#x2713;</font>&nbsp;Finalizing Setup";}

}//end function
</script>

<script lang="JavaScript">

// Create layer for progress dialog
document.write("<span id=\"progress\" class=\"hide\">");
document.write("<form name=dialog id=dialog>");
document.write("<table class='box' border=2  bgcolor=\"#666666\">");
document.write("<tr><td align=\"left\">");
document.write("<font color='white'><?=$company_name?> Range Setup Progress (approx. 1 min.)</font><br />");
document.write("<input type=text style='color: red; font-weight: bolder;' readonly='readonly' name=\"bar\" id=\"bar\" size=\"" + _progressWidth/2 + "\"");
if(document.all||document.getElementById) 	// Microsoft, NS6
document.write(" bar.style=\"color:red;\">");
else	// Netscape
document.write(">");
document.write("</td></tr>");

document.write("<tr><td>");
document.write("<p>Please stand by as your settings are being applied...</p>");
document.write("&emsp;&emsp;<label id='lblCreatingDatabase'></label>");
document.write("<br />&emsp;&emsp;<label id='lblInsertValues'></label>");
document.write("<br />&emsp;&emsp;<label id='lblFoldersAndFiles'></label>");
document.write("<br />&emsp;&emsp;<label id='lblSendEmail'></label>");
document.write("<br />&emsp;&emsp;<label id='lblFinalizing'></label>");
document.write("<br /><br />&emsp;&emsp;<label id='lblComplete'></label>");
document.write("</td></tr>");

document.write("</table>");
document.write("</form>");
document.write("</span>");

//ProgressDestroy();	// Hides
function login(){
//window.location = fn+"/login.php";
window.location = "setup_values.php";
//window.location = "setup_values.php";
}//end function
</script>
</head>

<body onload="CallJS('Demo()');startTimer();">
<div align="center"><img src="images/background.jpg" class="bg" /></div>

<span id="timer" style="visibility: hidden;"></span>

<span id="progress" class="hide"></span>

</body>
</html>