<?php
include 'includes/session.php';
?>

<!DOCTYPE HTML>

<html>
<head>
<title>Range Controller - Blog</title>
<?php
include 'includes/headerTags.php';
?>
</head>

<body>
<?php
include 'includes/header.php';
?>
<?php
include 'includes/navBar.php';
?>
<div id="blog">
  <?php
include("common/dba.php");
$sql_blog = "";
$sql_blog = "SELECT * FROM blog ORDER BY blog_id DESC"; 
if ($result = $link2->query($sql_blog)) { 
$i=0;
while ($row = $result->fetch_assoc()) { 
$i++;
print "<a name=\"section$i\"></a><p><h2>".$row['title']."</h2><h4>".$row['timestamp']."</h4>";
print "".$row['content']."<hr style='border: 0;height: 1px;background: #333;background-image: linear-gradient(to right, #ccc, #333, #ccc);'>";
}//END WHILE LOOP 
}//END IF
?>
  </div>
<?php
include 'includes/content.php';
?>
<?php
include 'includes/footer.php';
?>
</body>
</html>