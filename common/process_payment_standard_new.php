<script>
function captcha_redirect() 
{ 
	window.location="https://www.rangecontroller.com/payment_form_new.php"; 
}
</script>
<?php

require_once('recaptchalib.php');

// Get a key from https://www.google.com/recaptcha/admin/create
$publickey = "6LeVIwUTAAAAAB_9O19TaFpXp2sGzB7qa8OCW9dq";
$privatekey = "6LeVIwUTAAAAAMdTUzq0epQoigqynFyh73ne9LVc";

# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
$error = null;


# was there a reCAPTCHA response?

        $resp = recaptcha_check_answer ($privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
$che = $resp->is_valid;
        if ($resp->is_valid) {
                //echo "You got it!";
        } else {
                # set the error code so that we can display it
                //echo $error = $resp->error;
				header('Location: https://www.rangecontroller.com/payment_form_new.php');
	}
?>
<?php
session_start();
require('db.php');
require('dba.php');
require('constants.php');
// this is set in payment_form_trial so we know which customer is paying for trial 
$user_account_number = isset($_POST['h_accountnumber']) ? intval(trim($_POST['h_accountnumber'])) : null;


//THESE TWO VARIABLES ARE USED FOR THE TRIAL VERSION
$customer_number = $_POST['h_customer_number'];
$customer_range = $_POST['h_customer_range'];

//CREDIT CARD INFO
$first_name = $_POST['txtFirstName'];
$last_name = $_POST['txtLastName'];
$street_address = $_POST['txtStreetAddress'];
$city = $_POST['txtCity'];
$state = $_POST['h_state'];
$zipcode = $_POST['txtZipcode'];
$card_number = $_POST['txtCardNumber'];
$month_expired = $_POST['h_MonthExpired'];
$year_expired = $_POST['h_YearExpired'];
$cvv2 = $_POST['txtCVV2'];
$payment_sum = $_POST['h_payment_sum'];
$rk = $_POST['RestrictKey'];
$recur_amount_override = $_POST['RecurAmountOverride'];

//these posted values will be passed to eProcessingNetwork 
//to validate and approve card or decline it    
$post_string = "";
$post_string .= "ePNAccount=" . $_POST['ePNAccount'];
$post_string .= "&Inv=report";
$post_string .= "&RestrictKey=" . urlencode($rk);
$post_string .= "&TranType=Sale";
$post_string .= "&CardNo=" . $card_number;
$post_string .= "&CVV2Type=1";
$post_string .= "&CVV2=" . urlencode($cvv2);
$post_string .= "&ExpMonth=" . $month_expired;
$post_string .= "&ExpYear=" . $year_expired;
$post_string .= "&Total=" . $payment_sum;
$post_string .= "&Company=ePN";
$post_string .= "&FirstName=" . urlencode($first_name);
$post_string .= "&LastName=" . urlencode($last_name);
$post_string .= "&Address=" . urlencode($street_address);
$post_string .= "&City=" . urlencode($city);
$post_string .= "&State=" . urlencode($state);
$post_string .= "&Zip=" . urlencode($zipcode);
$post_string .= "&RecurMethodID=0";
$post_string .= "&Identifier=RangeControllerSoftware";
//$post_string .= "&RCRRecurAmount=0.05";
$post_string .= "&RCRRecurAmount=49";
$post_string .= "&RCRRecurs=0";
$post_string .= "&RCRPeriod=Monthly";
$post_string .= "&RCRChargeWhen=OnDayOfCycle";
$Rdate = date('m/d/Y', strtotime('+1 month'));
$post_string .= "&RCRStartOnDay=" . $Rdate;


echo $finurl = "https://www.eprocessingnetwork.com/cgi-bin/tdbe/transact.pl?".$post_string;

$response = file_get_contents($finurl);

if ($response{13} == 'Y') {
    $_SESSION['signup_paid_for'] = true;
    function generatePassword($length = 100) {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . '01234567890123456789%$@';
        $str = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++)
            $str .= $chars[rand(0, $max)];
        return $str;
    }
    // update the trial_paid to yes if this is a trial
    if ($user_account_number) {
        $upd_trial_paid = " update trial_version set trial_paid = 'Yes' "
                . "where accountnumber = '$user_account_number' ";
        $link->query($upd_trial_paid);
        $upd_status = " update controller_config set account_status = 'Active' "
                . "where accountnumber = '$user_account_number'";
        $link->query($upd_status);
    }
   
    
//END function generatePassword{   

    $random_str = generatePassword(); //CALL FUNCTION
    $acctnumberhashed = hash('sha1', $random_str);
    //sleep(1);
    $unique_account_number_sql = "SELECT account_number FROM control_account_number ORDER BY account_number DESC";
    if ($result = $link->query($unique_account_number_sql)) {
        $row = $result->fetch_assoc();
        $account_number = ($row['account_number'] + 1); //UNIQUE ACCOUNT BY ADDING ONE(1)   
    }//END IF
//sleep(1);
    $ins_new_account_number_sql = "";
    $ins_new_account_number_sql = "UPDATE control_account_number SET account_number = '$account_number' "; //account_number = (account_number+1) (this also will work)
    $link->query($ins_new_account_number_sql);
//sleep(1);
    $ins_new_signup_sql = "";
    $ins_new_signup_sql = "INSERT INTO signupcc VALUES('$acctnumberhashed','$account_number','$first_name','$last_name','$street_address','$city','$state','$zipcode','$payment_sum','" . date("D M j g:i:s Y") . "')";
    $link->query($ins_new_signup_sql);
	
//sleep(1);

    /*$upd_company_payments = "INSERT INTO `company_detail_payments` (`transid`, `customer_number`, `first_name`, `last_name`, `address`, `city`, `state`, `zip`, `email`, `payment_date`, `payment_amount`, `rep_commision`, `rep_paid_up_to`, `payment_number`, `payment_total`) VALUES ('Initial Signup','$account_number','$first_name','$last_name','$street_address','$city','$state','$zipcode',NULL,'" . date("D M j g:i:s Y") . "','$payment_sum',NULL,NULL,NULL,NULL)";
	
    $link->query($upd_company_payments);*/
	
	$dbs = new PDO('mysql:host=10.209.137.50;dbname=Y3nF7j6uK6eNbUOrmMtvkcs1hWnC9BgO49', 'swat', 'Thoinae9Yutai');
	
	$sth = $dbs->prepare("INSERT INTO `company_detail_payments` (`transid`, `customer_number`, `first_name`, `last_name`, `address`, `city`, `state`, `zip`, `email`, `payment_date`, `payment_amount`, `rep_commision`, `rep_paid_up_to`, `payment_number`, `payment_total`) VALUES ('Initial Signup',:account_number,:first_name,:last_name,:street_address,:city,:state,:zipcode,NULL,'" . date("D M j g:i:s Y") . "',:payment_sum,NULL,NULL,NULL,NULL)");

	$sth->bindParam(':account_number', $account_number, PDO::PARAM_INT);
	$sth->bindParam(':first_name', $first_name, PDO::PARAM_STR);
	$sth->bindParam(':last_name', $last_name, PDO::PARAM_STR);
	$sth->bindParam(':street_address', $street_address, PDO::PARAM_STR);
	$sth->bindParam(':city', $city, PDO::PARAM_STR);
	$sth->bindParam(':state', $state, PDO::PARAM_STR);
	$sth->bindParam(':zipcode', $zipcode, PDO::PARAM_INT);
	$sth->bindParam(':payment_sum', $payment_sum, PDO::PARAM_INT);
	$sth->execute();

    $link->close();

//DO NOT COLLECT CC INFO FOR STANDARD VERSION
//$ins_ccinfo_sql = "";
//$ins_ccinfo_sql = "INSERT INTO ccinfo VALUES('$acctnumberhashed','$account_number','$card_number','$month_expired','$year_expired','$cvv2','$payment_sum')";
//$link->query($ins_ccinfo_sql);
//START TO CREATE UPLOAD FOLDERS
    $uploaddir1 = "/var/www/vhosts/rangecontroller.com/xapp/img/$account_number";
    mkdir($uploaddir1);
    sleep(1);
    $uploaddir2 = "/var/www/vhosts/rangecontroller.com/xapp/img/$account_number/webcam";
    mkdir($uploaddir2);
//END TO CREATE UPLOAD FOLDERS

	setcookie("payment_form", "payment_form", time() + 900, '/');
	
    header("Location: https://www.rangecontroller.com/approved.php?u=neiTaTkB2GdBOB2VsidLpkMv7ceK9QSnu16LLhNDnrE2t7NcfqoLQ06OTbZTrs7cj3nub1yFj37S0KuW1ZhIPeWj5w3me0EDusyL");
    $_SESSION['account_number_for_logo'] = $account_number;
    
} else {
//echo "Your card has been declined...<a href='javascript:history.back()'>BACK</a><br \>";
    echo "Your card has been declined...<a href='https://www.rangecontroller.com/payment_form.php'>BACK</a><br \>";
}//-->END if ($response{13} == 'Y') 
?>

<body>
    <table style="height: 500px;">
        <tr><td><!--this table only supplies the height of the page in case of an error--></td></tr>
    </table>
</body>