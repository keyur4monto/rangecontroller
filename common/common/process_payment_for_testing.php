<?php
session_start();
require('db.php');

//THESE TWO VARIABLES ARE USED FOR THE TRIAL VERSION
$customer_number = $_POST['h_customer_number'];
$customer_range = $_POST['h_customer_range'];

//CREDIT CARD INFO
$first_name = $_POST['txtFirstName'];
$last_name  = $_POST['txtLastName'];
$street_address = $_POST['txtStreetAddress'];
$city    = $_POST['txtCity'];
$state   = $_POST['h_state'];
$zipcode = $_POST['txtZipcode'];
$card_number   = $_POST['txtCardNumber'];
$month_expired = $_POST['h_MonthExpired'];
$year_expired  = $_POST['h_YearExpired'];
$cvv2 = $_POST['txtCVV2'];

$payment_sum = $_POST['h_payment_sum'];
$rk = $_POST['RestrictKey'];
$recur_amount_override = $_POST['RecurAmountOverride'];

//these posted values will be passed to eProcessingNetwork 
//to validate and approve card or decline it    
$post_string = "";    
$post_string .= "ePNAccount=" .$_POST['ePNAccount'];
$post_string .= "&CardNo="    .$card_number;
$post_string .= "&ExpMonth="  .$month_expired;
$post_string .= "&ExpYear="   .$year_expired;

if(date("n/j/Y")==date("n/1/Y")){
//$post_string .= "&Total=".$payment_sum;
$post_string .= "&Total=0.01";//HARD CODED AMOUNT FOR TESTING
}else{
$post_string .= "&Total=0.02";//HARD CODED AMOUNT FOR TESTING
//$post_string .= "&Total=".$payment_sum;
$post_string .= "&RecurMethodID=1";//SET AT ePN RECUR BILLING SECTION
$post_string .= "&RecurAmountOverride=0.03";//HARD CODED AMOUNT FOR TESTING
//$post_string .= "&RecurAmountOverride=" .$recur_amount_override;
}//END IF

$post_string .= "&FirstName=" .$first_name;
$post_string .= "&LastName="  .$last_name;
$post_string .= "&Address="   .$street_address;
$post_string .= "&City="      .$city;
$post_string .= "&State="     .$state;
$post_string .= "&Zip="       .$zipcode;
$post_string .= "&CVV2Type=1";//1 means the cvv2 number will be sent, 0 means it can be blank 
$post_string .= "&CVV2="      .$cvv2;
$post_string .= "&RestrictKey=".$rk;
//RECURRING BILLING FOR RANGE CONTROLLER SOFTWARE
//$post_string .= "&RecurMethodID=1";//SET AT ePN RECUR BILLING SECTION
$post_string .= "&Identifier=RangeControllerSoftware";
//MAY NEED CODE TO SHOW OVERRIDE ONLY IF NOT FIRST OF MONTH..............
//$post_string .= "&RecurAmountOverride=" .$recur_amount_override;
//$post_string .= "&RecurAmountOverride=0.02";//HARD CODED AMOUNT FOR TESTING
               
//--> initiate cURL w/ protocol & URL of remote host (ePN)
$ch=curl_init("https://www.eProcessingNetwork.Com/cgi-bin/tdbe/transact.pl");
// normal POST request
curl_setopt($ch,CURLOPT_POST,1);
curl_setopt($ch,CURLOPT_POSTFIELDS, $post_string );
// set response to return as variable
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
// trap response into $response var
$response=curl_exec($ch);
// close cURL transfer
curl_close($ch); 
//-->cURL has initiated and is now closed

//$response{13} is the character from the response string from ePN for a CC approved
//<html><body>"YAUTH/TKT 019829","Address and Zip Code Do Not Match","CVV2 Not a Match"</body></html>
//ePN documentation says use the first char with substr(), That does not work. The letter 'Y'
//is the 13th char within the string, We just focus on getting the 'Y', all else is declined
//ePN can send the response back without HTML then the letter 'Y' will be elswhere in the string

//echo $response."<br /><br />";//FOR TESTING
//$response{13}='Y';//FOR TESTING

if ($response{13} == 'Y'){  

function generatePassword($length = 100){
$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.'01234567890123456789%$@';
$str = '';
$max = strlen($chars) - 1;
for ($i=0; $i < $length; $i++)
$str .= $chars[rand(0, $max)];
return $str;
}//END function generatePassword{   

$random_str = generatePassword(); //CALL FUNCTION
$acctnumberhashed= hash('sha1',$random_str);
//sleep(1);
$unique_account_number_sql = "SELECT account_number FROM control_account_number ORDER BY account_number DESC"; 
if ($result = $link->query($unique_account_number_sql)) {  
$row = $result->fetch_assoc();     
$account_number = ($row['account_number']+1);//UNIQUE ACCOUNT BY ADDING ONE(1)   
}//END IF
//sleep(1);
$ins_new_account_number_sql = "";
$ins_new_account_number_sql = "UPDATE control_account_number SET account_number = '$account_number' ";//account_number = (account_number+1) (this also will work)
$link->query($ins_new_account_number_sql);
//sleep(1);
$ins_new_signup_sql = "";
$ins_new_signup_sql = "INSERT INTO signupcc VALUES('$acctnumberhashed','$account_number','$first_name','$last_name','$street_address','$city','$state','$zipcode','$payment_sum','".date("D M j g:i:s Y")."')";
$link->query($ins_new_signup_sql);
//sleep(1);
$ins_ccinfo_sql = "";
$ins_ccinfo_sql = "INSERT INTO ccinfo VALUES('$acctnumberhashed','$account_number','$card_number','$month_expired','$year_expired','$cvv2','$payment_sum')";
$link->query($ins_ccinfo_sql);

//START TO CREATE UPLOAD FOLDERS
$uploaddir1 = "/var/www/vhosts/rangecontroller.com/xapp/img/$account_number"; 
mkdir($uploaddir1);
sleep(1);
$uploaddir2 = "/var/www/vhosts/rangecontroller.com/xapp/img/$account_number/webcam"; 
mkdir($uploaddir2);
//END TO CREATE UPLOAD FOLDERS

//sleep(1);
//if($link->query($ins_cc_sql)){echo "success";}

//$upd_trail_paid = "";
//$upd_trail_paid = "UPDATE trial_version SET trial_paid='Yes' ";
//$upd_trail_paid .= "WHERE customer_number= $customer_number";
//$link->query($upd_trail_paid); 

//echo "<br /><br />Your card has been approved...<a href='https://rangecontroller.com/sign-up.php'>Create your Range Controller Software</a><br \>";
//$_SESSION['processpayment'] = "activepaymentprocessed";
header("Location: https://www.rangecontroller.com/approved.php?u=neiTaTkB2GdBOB2VsidLpkMv7ceK9QSnu16LLhNDnrE2t7NcfqoLQ06OTbZTrs7cj3nub1yFj37S0KuW1ZhIPeWj5w3me0EDusyL");

$_SESSION['account_number_for_logo'] = $account_number;
//FOR TRIAL VERSION TESTING COMMENT THE LINE ABOVE, DO NOT NAVIGATE TO APPROVED.PHP,THEN UN-COMMENT THE TWO LINES OF CODE BELOW
//YOU WANT TO NAVIGATE TO CUSTOMERS RANGE
//$path = $customer_range."/range-login.php?l=t&tvpif=069fd3a44db682e9a4ea4bf495c0ffbee58c8431279ae453e0194f58b4eee71220bfbf1eb7ef556ab6445900a3b5bc28e726cbcfd59c5924b1d28fe8";
//header("Location: https://www.rangecontroller.com/".$path);

}else{                    
//echo "Your card has been declined...<a href='javascript:history.back()'>BACK</a><br \>";
echo "Your card has been declined...<a href='https://www.rangecontroller.com/payment_form.php'>BACK</a><br \>";
 
}//-->END if ($response{13} == 'Y') 
?>

<body>
<table style="height: 500px;">
<tr><td><!--this table only supplies the height of the page in case of an error--></td></tr>
</table>
</body>