<?php
/**
 * As the app transitions from no structure to mvc-like structure that uses silex, 
 * all routes that are not a directory, not a file will be routed here 
 * 
 */
require_once __DIR__ .'/bootstrap.php';

// Routes paths to controller actions 
$devAndLiveRoutes = [
    '/customers' => 'Rc\\Controllers\\Range\\CustomerController::index',
	 '/customers/resendSignup' => 'Rc\\Controllers\\Range\\CustomerController::resendSignup',
    '/customers/uploadImage/{memberNo}' => 'Rc\\Controllers\\Range\\CustomerController::uploadImage',
    '/customers/blockUnblock' => 'Rc\\Controllers\\Range\\CustomerController::blockUnblock',
    '/customers/downloadImage/{memberNo}' => 'Rc\\Controllers\\Range\\CustomerController::downloadImage',
    '/customers/saveWebcamImage/{memberNo}' => 'Rc\\Controllers\\Range\\CustomerController::saveWebcamImage',
    '/customers/add' => 'Rc\\Controllers\\Range\\CustomerController::addMember',
    '/customers/updateMemberJson' => 'Rc\\Controllers\\Range\\CustomerController::updateMemberJson',
    '/customers/getMembersJson' => 'Rc\\Controllers\\Range\\CustomerController::getMembersJson',
	'/customers/filterTypes' => 'Rc\\Controllers\\Range\\CustomerController::filterTypes',
    '/customers/{membershipNo}' => 'Rc\\Controllers\\Range\\CustomerController::editMember',   
	'/customers/printIdCard/{membershipNo}' => 'Rc\\Controllers\\Range\\CustomerController::printIdCard',
    '/customers/deletemember/{membershipNo}' => 'Rc\\Controllers\\Range\\CustomerController::deletemember',
	
	'/newsletters' => 'Rc\\Controllers\\Range\\NewsletterController::index',
    '/newsletters/addNew' => 'Rc\\Controllers\\Range\\NewsletterController::addNewsletter',
    '/newsletters/getGroupMembersJson' => 'Rc\\Controllers\\Range\\NewsletterController::getGroupMembersJson',
    '/newsletters/addnewnewsletter' => 'Rc\\Controllers\\Range\\NewsletterController::addNewNewsletter',    
	'/newsletters/edit/{newsletterNo}' => 'Rc\\Controllers\\Range\\NewsletterController::editNewsletter',
	'/newsletters/editNew' => 'Rc\\Controllers\\Range\\NewsletterController::editNewNewsletter',
	'/newsletters/editNews' => 'Rc\\Controllers\\Range\\NewsletterController::editNewNewsletter',
	'/newsletters/delete/{newsletterNo}' => 'Rc\\Controllers\\Range\\NewsletterController::deleteNewsletter',
	'/newsletters/getfilters' => 'Rc\\Controllers\\Range\\NewsletterController::getfilterDataJson',
	
	'/specialEvents' => 'Rc\\Controllers\\Range\\SpecialeventController::index',
    '/specialEvents/addNew' => 'Rc\\Controllers\\Range\\SpecialeventController::addSpecialevent',
    '/specialEvents/getGroupMembersJson' => 'Rc\\Controllers\\Range\\SpecialeventController::getGroupMembersJson',
    '/specialEvents/addnewspecialevent' => 'Rc\\Controllers\\Range\\SpecialeventController::addNewSpecialevent',
    '/specialEvents/edit/{specialeventNo}' => 'Rc\\Controllers\\Range\\SpecialeventController::editSpecialevent',
	'/specialEvents/editevent' => 'Rc\\Controllers\\Range\\SpecialeventController::editNewSpecialevent',
	'/specialEvents/delete/{specialeventNo}' => 'Rc\\Controllers\\Range\\SpecialeventController::deleteSpecialevent',
	'/specialEvents/getfilters' => 'Rc\\Controllers\\Range\\SpecialeventController::getfilterDataJson',
	
	'/classes' => 'Rc\\Controllers\\Range\\ClassController::index',
	'/classes/byDate/{month}/{year}' => 'Rc\\Controllers\\Range\\ClassController::listClassesByDate',
	'/classes/byInstructors/{instructor_id}' => 'Rc\\Controllers\\Range\\ClassController::listClassesByInstructor',
	'/classes/instructors' => 'Rc\\Controllers\\Range\\ClassController::getInstructors',
	'/classes/add' => 'Rc\\Controllers\\Range\\ClassController::addClass',
	'/classes/instructors/add' => 'Rc\\Controllers\\Range\\ClassController::addInstructor',
	'/classes/instructors/manage/{instructor_no}' => 'Rc\\Controllers\\Range\\ClassController::manageInstructor',
	'/classes/manage/{class_id}' => 'Rc\\Controllers\\Range\\ClassController::manageClass',
	'/classes/registrationList/{class_id}' => 'Rc\\Controllers\\Range\\ClassController::registrationList',
	'/classes/manage' => 'Rc\\Controllers\\Range\\ClassController::manageClass',
	'/classes/{class_id}/manageStudent/{student_id}' => 'Rc\\Controllers\\Range\\ClassController::manageStudent',	
	'/classes/{class_id}/updateStudent/{student_id}' => 'Rc\\Controllers\\Range\\ClassController::updateStudent',
    '/classes/{class_id}/rescheduleStudent/{student_id}' => 'Rc\\Controllers\\Range\\ClassController::rescheduleStudent',
	'/classes/{class_id}/addStudent' => 'Rc\\Controllers\\Range\\ClassController::addStudent',
	'/classes/{class_id}/insertStudent' => 'Rc\\Controllers\\Range\\ClassController::insertStudent',
	'/visit/{membershipno}/{pageno}' => 'Rc\\Controllers\\Range\\CustomerController::visit',
	'/visitdelete/{membershipno}/{recno}' => 'Rc\\Controllers\\Range\\CustomerController::visitdelete',
	
	'/reports' => 'Rc\\Controllers\\Range\\ReportController::landing',
	'/reports/shooter/{from}/{to}/{name}' => 'Rc\\Controllers\\Range\\ReportController::getShooterReport',
	'/reports/shooter/{from}/{to}/{name}/' => 'Rc\\Controllers\\Range\\ReportController::getShooterReport',
	
	'/reports/shooter/{from}/{to}/{name}/{shooter_id}' => 'Rc\\Controllers\\Range\\ReportController::getIndividualShooterReport',
    '/reports/memberaddress' => 'Rc\\Controllers\\Range\\ReportController::memberAddressList',
	'/reports/memberAddressPrintLabel/{id}' => 'Rc\\Controllers\\Range\\ReportController::memberAddressPrintLabel',
	'/reports/rangeUsageGraph' => 'Rc\\Controllers\\Range\\ReportController::getGraphReport',
	'/reports/rangeUsageGraphLane' => 'Rc\\Controllers\\Range\\ReportController::getGraphLaneReport',
	'/reports/membershipsReport' => 'Rc\\Controllers\\Range\\ReportController::getMembershipsReport',
	'/reports/completeMembershipsReport' => 'Rc\\Controllers\\Range\\ReportController::getCompleteMembershipsReport',
	'/reports/pos' => 'Rc\\Controllers\\Range\\ReportController::getPos',
	'/reports/pos/sales/{tp}' => 'Rc\\Controllers\\Range\\ReportController::getPosReport',
	'/reports/pos/sales' => 'Rc\\Controllers\\Range\\ReportController::getPosReport',
	'/reports/pos/reOrders' => 'Rc\\Controllers\\Range\\ReportController::getReorders',
	'/reports/pointOfSale/history/{membershipNumber}' => 'Rc\\Controllers\\Range\\ReportController::getPosHistoryReport',
	'/reports/pointOfSale/details/{transactionNo}' => 'Rc\\Controllers\\Range\\ReportController::getPosSaleDetails',
	'/reports/membershipRenewal/{month}/{year}' => 'Rc\\Controllers\\Range\\ReportController::membershipRenewal',
	'/reports/memberaddress/type/{type}' => 'Rc\\Controllers\\Range\\ReportController::memberAddressListByType',
	'/reports/memberaddress/type/{type}/shortno/{shortno}' => 'Rc\\Controllers\\Range\\ReportController::memberAddressListByTypeShortNo',
	'/reports/memberaddress/month/{month}/year/{year}' => 'Rc\\Controllers\\Range\\ReportController::memberAddressListByDate',
	'/reports/memberaddress/month/{month}/year/{year}/shortno/{shortno}' => 'Rc\\Controllers\\Range\\ReportController::memberAddressListByDateShortNo',
	'/reports/sales/{from}/{to}/{name}' => 'Rc\\Controllers\\Range\\ReportController::getSalesReport',
	'/reports/employee/{from}/{to}/{name}/{empid}' => 'Rc\\Controllers\\Range\\ReportController::getEmployeeReport',
	'/classes/instructors/add/{instructor_no}/{fname}/{lname}/{email}/{class_add}/{range_add}/{school_name}/{school_no}' => 'Rc\\Controllers\\Range\\ClassController::insertInstructor',
	'/classes/instructors/update/{old_instructor_no}/{new_instructor_no}/{fname}/{lname}/{email}/{class_add}/{range_add}/{school_name}/{school_no}' => 'Rc\\Controllers\\Range\\ClassController::updateInstructor',
	'/classes/instructors/delete/{instructor_no}' => 'Rc\\Controllers\\Range\\ClassController::deleteInstructor',
	'/classes/insert' => 'Rc\\Controllers\\Range\\ClassController::insertClass',
	'/classes/update/{class_id}' => 'Rc\\Controllers\\Range\\ClassController::updateClass',
	'/classes/delete/{class_id}' => 'Rc\\Controllers\\Range\\ClassController::deleteClass',
	'/classes/{old_class_id}/rescheduleStudent/{student_id}/{new_class_id}' => 'Rc\\Controllers\\Range\\ClassController::rescheduleStudentNewClass',
	'/classes/{class_id}/deleteStudent/{customer_id}' => 'Rc\\Controllers\\Range\\ClassController::removeCustomerFromClass',
	'/classes/{class_id}/addCustomer/{customer_id}' => 'Rc\\Controllers\\Range\\ClassController::addCustomerToClass',
	'/checkForDuplicateMember/{first_name}/{last_name}/{address}' => 'Rc\\Controllers\\Range\\ClassController::checkForDuplicateMember',
	
	'/config/benefits/{memtypeid}' => 'Rc\\Controllers\\Range\\ConfigController::memberBenefitsConfig', 
	'/config/benefits/edit/{membership_type_id}/{quantity}/{duration}/{benefit_text}/{benefit_id}' => 'Rc\\Controllers\\Range\\ConfigController::addOrUpdateBenefit',
	'/benefits/redeem/{customer_id}/{benefit_id}/{amount}/{comments}/{employee_id}' => 'Rc\\Controllers\\Range\\CustomerController::redeemBenefit',
	'/benefits/unredeem/{customer_id}/{benefit_id}' => 'Rc\\Controllers\\Range\\CustomerController::unredeemBenefit',
	//'/billingcobfig' => 'Rc\\Controllers\\Range\\BillingController::landing_billingcobfig',
    
    '/configurations' => 'Rc\\Controllers\\Range\\ConfigController::landing_configurations',
	'/config/employees' => 'Rc\\Controllers\\Range\\ConfigController::listEmployees',
    '/config/billing' => 'Rc\\Controllers\\Range\\ConfigController::listBillingCobfig',
    '/config/billing/paymentType' => 'Rc\\Controllers\\Range\\ConfigController::billingPaymentType',
    '/config/billing/paymentType/add' => 'Rc\\Controllers\\Range\\ConfigController::add_editBillingPaymentType',
    '/config/billing/paymentType/manage/{type_id}' => 'Rc\\Controllers\\Range\\ConfigController::add_editBillingPaymentType',
    '/config/billing/paymentType/check' => 
	'Rc\\Controllers\\Range\\ConfigController::checkBillingPaymentType',
    '/config/billing/paymentType/delete/{type_id}' => 'Rc\\Controllers\\Range\\ConfigController::deleteBillingPaymentType',
    '/config/billing/paymentFor' => 'Rc\\Controllers\\Range\\ConfigController::billingPaymentFor',
	'/config/billing/paymentFor/add' => 'Rc\\Controllers\\Range\\ConfigController::add_editBillingPaymentFor',
    '/config/billing/paymentFor/manage/{for_id}' => 'Rc\\Controllers\\Range\\ConfigController::add_editBillingPaymentFor',
    '/config/billing/paymentFor/check' => 'Rc\\Controllers\\Range\\ConfigController::checkBillingPaymentFor',
    '/config/billing/paymentFor/delete/{for_id}' => 'Rc\\Controllers\\Range\\ConfigController::deleteBillingPaymentFor',
    
    '/config/memberships' => 'Rc\\Controllers\\Range\\ConfigController::listMemberships', 
	'/config/employees/add' => 'Rc\\Controllers\\Range\\ConfigController::addEmployee',
	'/config/employees/manage/{employee_id}' => 'Rc\\Controllers\\Range\\ConfigController::manageEmployee',
	'/config/employees/delete/{employee_id}' => 'Rc\\Controllers\\Range\\ConfigController::deleteEmployee',
	'/config/memberships/add' => 'Rc\\Controllers\\Range\\ConfigController::addMembership',
	'/config/memberships/manage/{membership_id}' => 'Rc\\Controllers\\Range\\ConfigController::manageMembership',
	'/config/memberships/delete/{membership_id}' => 'Rc\\Controllers\\Range\\ConfigController::deleteMembership',
	'/config/employees/insert/{fname}/{lname}/{initials}/{employee_id}' => 'Rc\\Controllers\\Range\\ConfigController::insertEmployee',
	'/config/memberships/insert/{name}/{price}/{length}/{payment_required}/{membership_id}' => 'Rc\\Controllers\\Range\\ConfigController::insertMembership',
	'/configRC' => 'Rc\\Controllers\\Range\\ConfigController::configRC',	'/configRC/update/{email}/{password}/{companyname}/{company_address}/{company_city}/{company_state}/{company_zipcode}/{company_phone}/{taxrate}/{numberoflanes}/{sunday_from}/{sunday_to}/{monday_from}/{monday_to}/{tuesday_from}/{tuesday_to}/{wednesday_from}/{wednesday_to}/{thursday_from}/{thursday_to}/{friday_from}/{friday_to}/{saturday_from}/{saturday_to}/{time_zone}' => 'Rc\\Controllers\\Range\\ConfigController::updateRC',
	'/config/updateImage/{ext}' => 'Rc\\Controllers\\Range\\ConfigController::updateImage',
	'/config/idCard' => 'Rc\\Controllers\\Range\\IdCardController::index',
	'/config/ajaxSetPreferences' => 'Rc\\Controllers\\Range\\IdCardController::ajaxSetPreferences',
	'/config/idCardLayout' => 'Rc\\Controllers\\Range\\IdCardController::idCardLayout',
	'/getEmployeeList' => 'Rc\\Controllers\\Range\\CustomerController::getEmployeeList',
	'/setEmployeeId/{section}/{emp_id}' => 'Rc\\Controllers\\Range\\CustomerController::setEmployeeId',
	'/maintenance/lane/{toggle}' => 'Rc\\Controllers\\Range\\MaintenanceController::laneMaintenance',
	'/maintenance' => 'Rc\\Controllers\\Range\\MaintenanceController::landing',
	'/maintenance/lane/add/note' => 'Rc\\Controllers\\Range\\MaintenanceController::addNote',
	'/maintenance/lane/add/note/post' => 'Rc\\Controllers\\Range\\MaintenanceController::postNote',
	'/maintenance/lane/add/note/delete' => 'Rc\\Controllers\\Range\\MaintenanceController::deleteNote',
	'/maintenance/cleaning' => 'Rc\\Controllers\\Range\\MaintenanceController::rangeCleaning',
	'/maintenance/cleaning/filterBy/job/{id}' => 'Rc\\Controllers\\Range\\MaintenanceController::rangeCleaningByJob',
	'/maintenance/cleaning/filterBy/date/{month}/{year}' => 'Rc\\Controllers\\Range\\MaintenanceController::rangeCleaningByDate',
	'/maintenance/cleaning/completeJob/{id}' => 'Rc\\Controllers\\Range\\MaintenanceController::completeJob',
	'/maintenance/cleaning/addJob' => 'Rc\\Controllers\\Range\\MaintenanceController::addJob',
	'/maintenance/cleaning/manageJob/{id}' => 'Rc\\Controllers\\Range\\MaintenanceController::manageJob',
	'/maintenance/cleaning/addJob/post' => 'Rc\\Controllers\\Range\\MaintenanceController::postJob',
	'/maintenance/cleaning/manage/job/post' => 'Rc\\Controllers\\Range\\MaintenanceController::postManagedJob',
	'/maintenance/cleaning/complete/job/post' => 'Rc\\Controllers\\Range\\MaintenanceController::postCompletedJob',
	'/maintenance/cleaning/complete/job/delete' => 'Rc\\Controllers\\Range\\MaintenanceController::deleteCompletedJob',
	'/currentRangeUsage' => 'Rc\\Controllers\\Range\\MaintenanceController::currentRangeUsage',
	'/maintenance/currentRangeUsage/add' => 'Rc\\Controllers\\Range\\MaintenanceController::rangeUserLogin',
	'/customers/login/SelectLane/{member_id}' => 'Rc\\Controllers\\Range\\MaintenanceController::loginSelectLane',
	'/customers/lane/logThemIn' => 'Rc\\Controllers\\Range\\MaintenanceController::logThemIn',
	'/customers/lane/logThemOut/{member_no}' => 'Rc\\Controllers\\Range\\MaintenanceController::logThemOut',
	'/sendRenewalMail/{mem_no}' => 'Rc\\Controllers\\Range\\CustomerController::sendRenewalMail',
	'/techSupport' => 'Rc\\Controllers\\Range\\CustomerController::techSupport',
	'/sendEmail' => 'Rc\\Controllers\\Range\\CustomerController::sendEmail',
	'/rcScheduler' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerMain',
	'/rcScheduler/add' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerAdd',
	'/rcScheduler/add/post' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerPost',
	'/rcScheduler/add/postUpdate' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerPostUpdate',
	'/rcScheduler/manage/{booking_number}' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerManage',
	'/rcScheduler/delete/{booking_number}' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerDelete',
	'/members/rcScheduler' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerMainShooter',
	'/members/rcScheduler/delete/{booking_number}' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerDeleteShooter',
	'/members/rcScheduler/add' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerAddShooter',
	'/members/rcScheduler/manage/{booking_number}' => 'Rc\\Controllers\\Range\\SchedulerController::schedulerManageShooter',
	'/pointOfSale/{customer_id}' => 'Rc\\Controllers\\Range\\PosController::posMain',
	'/ajax/getMembersOptions/{search}' => 'Rc\\Controllers\\Range\\AjaxController::getMembersRewrite',
	'/ajax/getPosItems/{search}' => 'Rc\\Controllers\\Range\\AjaxController::getPosItems',
	'/ajax/getCustomerForPos/{customer_id}' => 'Rc\\Controllers\\Range\\AjaxController::getCustomerForPos',
	'/ajax/getCustomersForPos/{search}' => 'Rc\\Controllers\\Range\\AjaxController::getCustomersForPos',
	'/pointOfSalePayment/{transaction_id}' => 'Rc\\Controllers\\Range\\PosController::posPayment',
	'/pointOfSalePost' => 'Rc\\Controllers\\Range\\PosController::posPost',
	'/postPayment' => 'Rc\\Controllers\\Range\\PosController::postPayment',
	'/getTransactionId' => 'Rc\\Controllers\\Range\\PosController::getTransactionId',
	'/pointOfSaleReceipt/{transaction_id}' => 'Rc\\Controllers\\Range\\PosController::posReceipt',
	'/pointOfSaleReceipt/Print/{transaction_id}' => 'Rc\\Controllers\\Range\\PosController::posReceiptPrint',
	'/inventory' => 'Rc\\Controllers\\Range\\PosController::inventoryMain',
	'/inventory/bulk' => 'Rc\\Controllers\\Range\\PosController::inventoryBulkMain',
	'/inventory/bulkpost' => 'Rc\\Controllers\\Range\\PosController::inventoryBulkPost',
	'/inventory/add' => 'Rc\\Controllers\\Range\\PosController::inventoryAdd',
	'/inventory/manage/{inventory_id}' => 'Rc\\Controllers\\Range\\PosController::inventoryManage',
	'/inventory/add/post' => 'Rc\\Controllers\\Range\\PosController::inventoryAddPost',
	'/inventory/manageID/post' => 'Rc\\Controllers\\Range\\PosController::inventoryManagePost',
	'/inventory/delete/{inventory_id}' => 'Rc\\Controllers\\Range\\PosController::inventoryDelete',
	'/inventory/deletes/{inventory_id}' => 'Rc\\Controllers\\Range\\PosController::inventoryDeletes',
	'/inventory/posConfig' => 'Rc\\Controllers\\Range\\PosController::posConfig',
	'/inventory/manageSale' => 'Rc\\Controllers\\Range\\PosController::posManageSale',
	'/inventory/manageSale/delete/{sale_id}' => 'Rc\\Controllers\\Range\\PosController::saleDelete',
	'/inventory/addInventoryType' => 'Rc\\Controllers\\Range\\PosController::addInventoryType',
	'/inventory/deleteInventoryType' => 'Rc\\Controllers\\Range\\PosController::deleteInventoryType',
	'/inventory/updateRegisterBase' => 'Rc\\Controllers\\Range\\PosController::updateRegisterBase',
	'/inventory/printbarImage/{id}' => 'Rc\\Controllers\\Range\\PosController::printbarImage',
	'/inventory/printbarMain/{id}/{type}/{brand}/{model}/{description}/{serial_number}/{color}/{resale}/{barrels_length}/{caliber_gauge}' => 'Rc\\Controllers\\Range\\PosController::printbarMain',
	'/dashboard' => 'Rc\\Controllers\\Range\\DashboardController::index',
	'/serverEmailConfig' => 'Rc\\Controllers\\Range\\ServerEmailConfigController::index',
	'/serverEmailConfig/update' => 'Rc\\Controllers\\Range\\ServerEmailConfigController::setTemplate',
	'/serverEmailConfig/TestEmail' => 'Rc\\Controllers\\Range\\ServerEmailConfigController::sendTestEmail',
	
	'/members/dashboard' => 'Rc\\Controllers\\Shooter\\MembersController::index',
	'/members/billing' => 'Rc\\Controllers\\Shooter\\BillingController::index',
	'/members/changePassword' => 'Rc\\Controllers\\Shooter\\ChangePasswordController::index',
	'/members/memberRangeUsage' => 'Rc\\Controllers\\Shooter\\MemberRangeUsageController::index',
	'/members/contactInfo' => 'Rc\\Controllers\\Shooter\\ContactInfoController::index',
	'/members/editContactInfo' => 'Rc\\Controllers\\Shooter\\EditContactInfoController::index',
	'/members/upcomingEvents' => 'Rc\\Controllers\\Shooter\\UpcomingEventController::index',
	'/members/eventDetails/{eventNo}' => 'Rc\\Controllers\\Shooter\\EventDetailsController::index',
	'/members/upcomingClasses' => 'Rc\\Controllers\\Shooter\\UpcomingClassesController::index',
	'/members/classDetails/{id}' => 'Rc\\Controllers\\Shooter\\ClassDetailsController::index',
	'/members/logout' => 'Rc\\Controllers\\Shooter\\LogoutController::index',
	
	'/admin/login' => 'Rc\\Controllers\\Admin\\AdminLoginController::index',
	'/admin/loginCheck' => 'Rc\\Controllers\\Admin\\AdminLoginController::loginCheck',
	'/admin/dashboard' => 'Rc\\Controllers\\Admin\\DashboardController::index',
	'/admin/account' => 'Rc\\Controllers\\Admin\\AccountController::index',
	'/admin/account/manage/{accountnumber}' => 'Rc\\Controllers\\Admin\\AccountController::manageAccount',
	'/admin/account/delete/{accountnumber}' => 'Rc\\Controllers\\Admin\\AccountController::deleteAccount',
	'/admin/account/suspend/{accountnumber}' => 'Rc\\Controllers\\Admin\\AccountController::suspendAccount',
	'/admin/account/active/{accountnumber}' => 'Rc\\Controllers\\Admin\\AccountController::activeAccount',
	'/admin/account/trial/{accountnumber}' => 'Rc\\Controllers\\Admin\\AccountController::trialAccount',
	'/admin/account/getfilters' => 'Rc\\Controllers\\Admin\\AccountController::getfilterDataJson',
	'/admin/blog' => 'Rc\\Controllers\\Admin\\BlogController::index',
	'/admin/blog/add' => 'Rc\\Controllers\\Admin\\BlogController::addBlog',
	'/admin/blog/manage/{blog_id}' => 'Rc\\Controllers\\Admin\\BlogController::manageBlog',
	'/admin/blog/delete/{blog_id}' => 'Rc\\Controllers\\Admin\\BlogController::deleteBlog',
	'/admin/announcementBoard' => 'Rc\\Controllers\\Admin\\announcementBoardController::index',
	'/admin/announcementBoard/add' => 'Rc\\Controllers\\Admin\\announcementBoardController::addAnnouncementBoard',
	'/admin/announcementBoard/manage/{announcementboard_id}' => 'Rc\\Controllers\\Admin\\announcementBoardController::manageAnnouncementBoard',
	'/admin/announcementBoard/delete/{announcementboard_id}' => 'Rc\\Controllers\\Admin\\announcementBoardController::deleteAnnouncementBoard',
	'/admin/newsletters' => 'Rc\\Controllers\\Admin\\newslettersController::index',
	'/admin/newsletters/add' => 'Rc\\Controllers\\Admin\\newslettersController::addNewsletters',
	'/admin/newsletters/manage/{newsletter_id}' => 'Rc\\Controllers\\Admin\\newslettersController::manageNewsletters',
	'/admin/newsletters/delete/{newsletter_id}' => 'Rc\\Controllers\\Admin\\newslettersController::deleteNewsletter',
	'/admin/newsletters/getfilters' => 'Rc\\Controllers\\Admin\\newslettersController::getfilterDataJson',
	'/admin/newsletters/getGroupMembersJson' => 'Rc\\Controllers\\Admin\\newslettersController::getGroupMembersJson',
	
	'/help' => 'Rc\\Controllers\\Range\\HelpController::index',
];

foreach ($devAndLiveRoutes as $key => $value) {
    $app->match('/dev1' . $key, $value);
    $app->match('/xapp' . $key, $value);
}
 

// If a route is not found, shows that url is not found. 
$app->get('{url}', function($url) use ($app){
    return $app['config']['site_url'] . '/' . $url . ' is not found';
})->assert('url', '.+');

$app->run();

