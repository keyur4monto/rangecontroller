<?php	                                       			
session_start();
//require('common_constants.php');
require('common/db.php');

$sel_controller_settings = "";
$sel_controller_settings = "SELECT * ";
$sel_controller_settings .= "FROM controller_config ";
$sel_controller_settings .= "JOIN hours_of_operation ON controller_config.accountnumber = hours_of_operation.accountnumber ";
$sel_controller_settings .= "WHERE controller_config.acctnumberhashed = '".$_SESSION['hn']."' AND controller_config.accountnumber = '".$_SESSION['an']."' ";
//$sel_controller_settings .= "WHERE controller_config.acctnumberhashed = '3b05f1885273ef42da964b4d1b35d4c4f741e9b8' AND controller_config.accountnumber = '10000' ";

if ($result = $link->query($sel_controller_settings)) {     
while ($row = $result->fetch_assoc()) { 
$account_number = $row['accountnumber']; 
$registered_email = $row['registered_email']; 
$username = $row['username']; 
$password = $row['password']; 
//$url_folder_name = $row['url_folder_name']; 
$company_name = $row['company_name']; 
$company_logo = $row['company_logo']; 
$tax_rate = $row['tax_rate']; 
$number_of_lanes = $row['number_of_lanes']; 
//$hours_of_operation = $row['hours_of_operation']; 
$has_memberships = $row['has_memberships']; 
$member_number_start_count = $row['member_number_start_count']; 
//$has_benefits_section = $row['has_benefits_section']; 
$timestamp = $row['timestamp']; 
$sun_from = $row['sunday_from'];
$sun_to = $row['sunday_to'];
$mon_from = $row['monday_from'];
$mon_to = $row['monday_to'];
$tue_from = $row['tuesday_from'];
$tue_to = $row['tuesday_to'];
$wed_from = $row['wednesday_from'];
$wed_to = $row['wednesday_to'];
$thur_from = $row['thursday_from'];
$thur_to = $row['thursday_to'];
$fri_from = $row['friday_from'];
$fri_to = $row['friday_to'];
$sat_from = $row['saturday_from'];
$sat_to = $row['saturday_to'];
}//END WHILE LOOP   
$link->close();
}//END IF
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Range Controller Setup Values</title>
<meta charset="utf-8"/>
<meta name="author" content="carlos garcia"/>
<link rel="stylesheet" href="css/style.css"/>
<link rel="shortcut icon" href="/favicon.ico" />

<style type="text/css">
a:visited {
	color: #3a3630;
}
a:hover {
	color: #F90;
}
a:active {
	color: #3A3630;
}
a:link {
	color: #3a3630;
}
</style>

<!--[if lt IE 7]>
<div class='aligncenter'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg"border="0"></a></div>  
<![endif]-->
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css"> 
<![endif]-->

<script>
//empty...
</script>
<style>
iframe {display:none;}
</style>

</head>

<body>
<?php	                                       			
//include 'header.php';
echo '  

<img src="images/background.jpg" class="bg" />
<div id="blanket" style="display:none;"></div>
<div class="main">
';
?>



<div id="payment-form-slider" >

<header>
    <div class="wrapper">
      <h1 class="logo"><a href="point.php">
      <img src="images/logo.png" width="400" height="70"/></a></h1>
    </div>
  </header>
  
 <div class="content" style="margin-top: -40px;">
  
<table width="100%" border="0">
<tr>
<td class="table_approved" style="margin-bottom: 100px;">
<p style="text-align: center; font-size: larger; font-weight: bold;"><u>RANGE CONTROLLER SETTINGS AND LOGIN INFORMATION</u>
<br />
</p>

<p>ACCOUNT NUMBER:&emsp;&emsp;&emsp;&emsp;<?=$account_number?></p>
<p>REGISTERED EMAIL:&emsp;&emsp;&emsp;&emsp;<?=$registered_email?></p>
<p>ADMIN LOGIN PASSWORD:&emsp;<?=$password?></p>

<!--<p>LOGIN URL:&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://rangecontroller.com/beta/range-login.php</p>-->

<p>COMPANY NAME:&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<?=$company_name?></p>
<p>COMPANY LOGO:&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<?=$company_logo?></p>
<p>TAX RATE SET:&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<?=$tax_rate?></p>
<p>NUMBER OF LANES:&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$number_of_lanes?></p>
<p>CUSTOMERS/MEMBERS<br />START COUNT:&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?=$member_number_start_count?></p>
<p style="text-align: left;">
Take note of your account number and password and use these to log into your Range Controller via admin panel. 
We also sent an email to you with these credentials. 
</p>
<p>Thank you for choosing The Worlds Best Software For Running Your Shooting RangeController.com, Press Login to begin.</p>
</td>
</tr>
</table>

</div>
</div>

<?php	                                       			
//include 'footer.php';
?>

</body>
</html>