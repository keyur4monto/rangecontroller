<?php                           			
session_start();
/*
if($_GET['u'] != "neiTaTkB2GdBOB2VsidLpkMv7ceK9QSnu16LLhNDnrE2t7NcfqoLQ06OTbZTrs7cj3nub1yFj37S0KuW1ZhIPeWj5w3me0EDusyL"){
//session_destroy();
echo 'Direct access not permitted';    
$str_path = "Location: https://www.rangecontroller.com";
header($str_path);
return;
}

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {//secure
//empty
}else{//not secure,send em back to https 
//session_destroy();
$str_path = "Location: https://www.rangecontroller.com";
header($str_path);
return;
}//END IF
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Range Controller - Payment Received</title>
<?php 
include('includes/meta.php');
?>
<link rel="stylesheet" href="css/style.css"/>
<link rel="shortcut icon" href="/favicon.ico" />
<style type="text/css">
#payment-form-slider {
	background-color: #1E1E1E;
	height: 1275px;
	width: 950px;   
    margin:0 auto;   
}

.table_approved {
background-color:#666;
border:none;
width: 700px;
font-weight:bold;
font-size:16px;
vertical-align:middle;
padding: 25px;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.content table{
margin:0 auto;
}

.payment_button {
width: 125px;
height: 25px;
cursor: pointer;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.payment_button:hover{
background-color: green;
color: white;
}

#header_signup{
display: none;
}

#headercol2 {
	display: none;
}
</style>

<!--[if lt IE 7]>
<div class='aligncenter'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg"border="0"></a></div>  
<![endif]-->
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css"> 
<![endif]-->

<script>
//empty...
</script>
</head>

<body>
<img src="images/background.jpg" class="bg" />
<div id="blanket" style="display:none;"></div>

<div id="payment-form-slider">

<?php
include 'includes/header.php';
?>
  
<div class="content" >
<table width="700" >
<tr>
<td class="table_approved">
<p><h3>Thank you, your payment has been received!</h3></p>
<p>Now you are ready to setup your range controller company configuration.</p>
<p>You will be prompted to tell Range Controller about your company. This information will be used to populate your custom Range Controller Software. After this process is complete, you are ready to start using Range Controller.</p>
<p>The company build process will take around 10 minutes or so to complete. To ensure a full and functional setup, please keep these things in mind:</p>
<ul>
  <li>A pen and paper is handy for notes if you wish.</li>
  <li>Have your company logo ready to upload at setup time. The file extentions available&nbsp;are jpg, png, jpeg or gif.</li>
  <li>You will need the sales tax rate for your company.</li>
  <li>Be ready for the number of lanes your range has and their times from open to close</li>
  <li>Will you be selling memberships? i.e. Platinum, Shooter, Classic, Visitor etc.</li>
  <li>Starting number for your client entry. i.e. 10001 or any number you choose.</li>
  <li>It's advised to close all other applications and make sure they stay idle during the company setup.</li>
  <li>Wait for all uploads or downloads to complete before pressing OK.</li>
  <li>Make sure there won't be any interruption with the Internet connection.</li>
  <li>Make sure your printer is on to print your settings once setup is complete.</li>
  <li>Stay clear of the keyboard and mouse during setup.</li>
  <li>Please read the following pages carefully to ensure a successful setup.</li>
  <li>Press OK when you ready</li>
</ul>
<p>Setting up your company with Range Controller is simple! If you have any questions, please refer to the help section or send us a email to tech support at the bottom right of any page. Once your information is input, you are ready to start using your personal, secure database right away! You will receive an email with the specific info you input along with your password to use to access your company. </p>
<p>Any modules or updates we perform will be FREE to use and your data is safe. Now, you can stay on top of your business once and for all. Run your range like a pro, don't let your range run you! Thank you for using Range Controller as your range software solution!</p>
<p>The gang at RangeController.com<br>
  swat@davespawnshop.com<br>
  "The world's foremost software for running and growing your shooting range!"</p>
<p>Thank you very much!</p>

<p style="text-align: right;">
<input type="button" class="payment_button" onclick="window.location = 'https://www.rangecontroller.com/sign-up.php?u=5ysEcmxoS5rEfLK3dYQaALyDDMk6UJOQ87UajHF1Dx6j8Gdcft2f5qj92J5NTKdRHysr6xj9lgjkmCCIv4nrlMqeWmrpwL749pm5';" value="OK" />
&emsp;</p></td>
</tr>
</table>
</div>
</div>

<?php	                                       			
include('includes/footer.php');
?>
</body>
</html>