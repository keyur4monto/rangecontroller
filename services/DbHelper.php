<?php
namespace Rc\Services {
    
    class DbHelper  {

        function __construct() {
            
        }
        
        public static function dbFetchAllAssoc(\mysqli $link, $sql){
            $data = array();
            if ($res = $link->query($sql)){
                while ($row = $res->fetch_assoc()) {            
                    $data[] = $row;
                }
                $res->free();
            }
            return $data;
        }

        public static function dbFetchRowAssoc(\mysqli $link, $sql){
            $data = self::dbFetchAllAssoc($link, $sql);
            if ($data && count($data) >0 ) {
                return $data[0];
            }
            return $data;
        }

        public static function dbEscapeStr(\mysqli $link, $str) {
            return $link->real_escape_string($str);
        }
        
        /**
         * 
         * @param \Doctrine\DBAL\Connection $conn
         * @return mixed returns wrapped resource handle
         */
        public static function getWrappedHandle(\Doctrine\DBAL\Connection $conn){
            return $conn->getWrappedConnection()->getWrappedResourceHandle();
        }

    }
}
