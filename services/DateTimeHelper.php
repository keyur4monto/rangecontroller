<?php

namespace Rc\Services {
    class DateTimeHelper {

        function __construct() {
            
        }

        /**
        * Returns date formatted like 2014-05-23 
        * @param DateTime $dt
        * @return type
        */
        public static function dateToMysqlDateStr(\DateTime $dt){    
            return $dt->format('Y-m-d');
        }

       /**
        * Returns date formatted like '2014-05-23 04:12:21'   
        * @param DateTime $dt
        * @return string
        */
        public static function datetimeToMysqlDatetimeStr(\DateTime $dt){
            return $dt->format('Y-m-d H:i:s');    
        }
        
        public static function isDateFormattedAsMdy($dateStr) {
            return preg_match('/\d{1,2}\/\d{1,2}\/\d{4}/', $dateStr);
        }
        
        public static function mdyToDateTime($mdyStr, $sep = '/'){
            $parts = explode($sep, $mdyStr);
            if (count($parts) !== 3) {
                return null;
            }
            return new \DateTime($parts[2] . '-' . $parts[0] . '-' .$parts[1]);
        }
    }
}