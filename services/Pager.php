<?php
namespace Rc\Services {
    class Pager {
        protected $pageSize ;
        protected $currentPage;
        protected $offset; 
        
        public function __construct($currentPage = 1, $pageSize = 10) {
            $this->currentPage = (int) $currentPage ?: 1;
            $this->pageSize = (int)$pageSize ?: 10;
            $this->offset = ($this->currentPage -1) * $this->pageSize;
        }
        
        public function getPageSize(){ return $this->pageSize; }
        public function getCurrentPage(){ return $this->currentPage; }
        public function getOffset(){ return $this->offset; }
        public function toArray(){
            return [
                'pageSize' => $this->pageSize,
                'currentPage' => $this->currentPage,
                'offset' => $this->offset
            ];
        }
    }
}
