<?php

namespace Rc\Services {
    
    class StringHelper {

        function __construct() {
            
        }
        public static function formatPhonePart($phone){
            $phone = preg_replace('/[^0-9]/m', '', $phone);
            $result = '(';
            $result .=  substr($phone, 0,3);
            if (strlen($phone) > 3) {
                $phone = substr($phone, 3);
                $result .= ')-' . substr($phone, 0, 3);
                if (strlen($phone) > 3) {
                    $result .= '-' . substr($phone, 3, 4);
                }
            }
            return $result;
        }
        
        public static function formatPhone($phone){
            $phone_ = preg_replace('/[^0-9]/m', '', $phone);
            $len = strlen($phone_);
            if ($len == 11  && substr($phone_, 0,1) == 1) {
                $phone_ = substr($phone_, 1);
                $len = 10;
            }
            if ($len == 10) {
                return '('.substr($phone_,0,3) . ')-' . substr($phone_, 3, 3) 
                        . '-' . substr($phone_, 6, 4); 
            }
            return $phone;
        }
        
        public static function generateRandomString($length = 8){
            $chars =  'ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz'.'23456789#$';
            $str = '';
            $max = strlen($chars) - 1;
            for ($i=0; $i < $length; $i++) {
                $str .= $chars[rand(0, $max)];
            }
            return $str;
        }
        
        public static function isPhoneValid($phone){            
            $phone_ = preg_replace('/[^0-9]/m', '', $phone);
            return preg_match('/^[1]{0,1}[0-9]{10}$/', $phone_);
        }
        
        
        public static function parseHeightToFeetAndInches($height){
            $height = trim(strtolower($height));
            
            preg_match('/^([3-9]{1})[^0-9\.]+(([0-9]{0,2}\.?)?[0-9]{0,2})/', $height, $matches);
            if (empty($matches)) {
                return null;
            }
            $res = [
                'feet' => (int)$matches[1],
                'inches' => round((float)$matches[2],1)
            ];
            return $res;
        }

    }
}