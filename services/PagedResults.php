<?php

namespace Rc\Services  {
    
    class PagedResults {

        protected $records;
        protected $totalRecords;
        protected $pager;
        function __construct(array $records,  $totalRecords, \Rc\Services\Pager $pager) {
            $this->records = empty($records) ? [] : (is_array($records) ? $records : [$records]);
            $this->totalRecords = $totalRecords;
            $this->pager = $pager;
        }
        
        /**
         * 
         * @return int
         */
        public function getRecords(){
            return $this->records;
        }
        
        /**
         * 
         * @return array
         */
        public function getTotalRecords(){
            return $this->totalRecords;
        }
        
        /**
         * 
         * @return \Rc\Services\Pager
         */
        public function getPager(){
            return $this->pager;
        }
        
        public function toArray(){
            return [
                'records' => $this->records,
                'totalRecords' => $this->totalRecords,
                'pager' => $this->pager->toArray()
            ];
        }

    }
}