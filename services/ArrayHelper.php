<?php

namespace Rc\Services {
    
    class ArrayHelper {

        function __construct() {
            
        }
        
        public static function getIfExists($arr, $key, $default = null){
            return isset($arr[$key]) ? $arr[$key] : $default;
        }
        
        /**
         * 
         * @deprecated use getIfExists
         * @param type $arr
         * @param type $key
         * @param type $default
         * @return mixed
         */
        public static function getValForKeyWithDefault($arr, $key, $default = null){
            return self::getIfExists($arr, $key, $default);
        }
        
        public static function arrayGroupBy($arr, $groupByColumnName){
            $distinct_vals = array();
            foreach($arr as $k => $row){
                $v = trim($row[$groupByColumnName]) . '';
                if (!array_key_exists($v, $distinct_vals)) {
                    $distinct_vals[$v] = [];
                }
                $distinct_vals[$v][] = $row;        
            }
            return $distinct_vals;
        }

        public static function arrayIndexBy($arr, $indexByColumnName){
            $distinct_vals = array();
            foreach($arr as $k => $row){
                $v = trim($row[$indexByColumnName]) . '';        
                $distinct_vals[$v] = $row;        
            }
            return $distinct_vals;
        }

        /**
         * 
         * @param array $arr array of key value arrays
         * @param string $columnName
         * @return array
         */
        public static function arrayColumn($arr, $columnName){
            $res = array();
            foreach($arr as $ind => $v){
                if (isset($v[$columnName])) {
                    $res[] = $v[$columnName];
                }
            }
            return array_unique($res);
        }

        /**
         * sums the values of given column: if $isFloat false, then it is assumed int
         * @param type $arr
         * @param type $columnName
         * @param bool $isFloat
         * @return int|float
         */
        public static function arraySumColumn($arr, $columnName, $isFloat){
            $total = 0;
            foreach($arr as $k => $d){
                $num = trim($d[$columnName]);
                $total += $isFloat ? (float)$num : (int)$num; 
            }
            return $total;
        }

        /**
         * 
         * @param array $arr associated array
         * @param array $keysArr numeric indexed array
         * @return boolean
         */
        public static function arrayContainsAllTheseKeys($arr, $keysArr){
            foreach ($keysArr as $ind => $v){
                if (!array_key_exists($v, $arr)) {
                    return false;
                }
            }
            return true;
        }
        
        /**
         * 
         * @param type $arr
         * @param type $keys
         * @param type $emptyIs
         * @return boolean
         */
        public static function hasEmptyValuesForKeys($arr, $keys, $emptyIs = ['']) {
            foreach ($keys as $key) {
                if (!isset($arr[$key])) {
                    return true;
                }
                if (in_array($arr[$key], $emptyIs)) {
                    return true;
                }
            }
            return false;
        }

    }
}