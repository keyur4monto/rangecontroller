<?php
namespace Rc\Services {
    
    class NSession {
        public function __construct() {
            ;
        }
        
        public function set($k, $v){
            if (!session_id()) {
                session_start();
            }
            $_SESSION[$k] = $v;            
        }
        
        public function get($k, $default = null){
            return isset($_SESSION[$k]) ? $_SESSION[$k] : $default;
        }
            
    }
}