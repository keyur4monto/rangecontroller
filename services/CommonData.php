<?php

namespace Rc\Services {
    

    class CommonData {

        function __construct() {

        }
        
        public static function getPaymentTypes(){
            $arr = ['Cash', 'Check', 'Debit', 'Discover', 'Mastercard', 'Visa'];
            sort($arr);
            return array_combine($arr, $arr);
        }
        
        public static function getHairColors(){
            $arr = ['Black', 'Brown', 'Blonde', 'Dark Blonde', 'Salt & Pepper', 
                'Red', 'Gray', 'Dark Brown'];
            sort($arr);
            return array_combine($arr, $arr);
        }
        
        public static function getEyeColors(){
            $arr = ['Black', 'Brown', 'Blue', 'Gray', 'Green', 
                'Hazel'];
            sort($arr);
            return array_combine($arr, $arr);
        }
        
        public static function getRaces(){
            $arr = ['White', 'Asian', 'African American', 'Native American', 
                'Hawaiian', 'Other'];
            sort($arr);
            return array_combine($arr, $arr);
        }
        
        public static function getGenders(){
            return [
                'Male' => 'Male',
                'Female' => 'Female'
            ];
        }
        
        public static function getYesNo(){
            return ['Yes' => 'Yes', 'No' => 'No'];
        }
        
        public static function getStates(){
            static $states = null;
            if ($states){
                return $states;
            }
            $states = ['AL'=>'Alabama',
                'AK'=>'Alaska',
                'AZ'=>'Arizona',
                'AR'=>'Arkansas',
                'CA'=>'California',
                'CO'=>'Colorado',
                'CT'=>'Connecticut',
                'DE'=>'Delaware',
                'DC'=>'District of Columbia',
                'FL'=>'Florida',
                'GA'=>'Georgia',
                'HI'=>'Hawaii',
                'ID'=>'Idaho',
                'IL'=>'Illinois',
                'IN'=>'Indiana',
                'IA'=>'Iowa',
                'KS'=>'Kansas',
                'KY'=>'Kentucky',
                'LA'=>'Louisiana',
                'ME'=>'Maine',
                'MD'=>'Maryland',
                'MA'=>'Massachusetts',
                'MI'=>'Michigan',
                'MN'=>'Minnesota',
                'MS'=>'Mississippi',
                'MO'=>'Missouri',
                'MT'=>'Montana',
                'NE'=>'Nebraska',
                'NV'=>'Nevada',
                'NH'=>'New Hampshire',
                'NJ'=>'New Jersey',
                'NM'=>'New Mexico',
                'NY'=>'New York',
                'NC'=>'North Carolina',
                'ND'=>'North Dakota',
                'OH'=>'Ohio',
                'OK'=>'Oklahoma',
                'OR'=>'Oregon',
                'PA'=>'Pennsylvania',
                'RI'=>'Rhode Island',
                'SC'=>'South Carolina',
                'SD'=>'South Dakota',
                'TN'=>'Tennessee',
                'TX'=>'Texas',
                'UT'=>'Utah',
                'VT'=>'Vermont',
                'VA'=>'Virginia',
                'WA'=>'Washington',
                'WV'=>'West Virginia',
                'WI'=>'Wisconsin',
                'WY'=>'Wyoming'
            ];
            return $states;
        }
        
    }
    
    
}
