<?php

namespace Rc\Services {
    
    class MailHelper {

        function __construct() {
            
        }
        
        /**
        * Sends email with fromName RangeController.Com as default
        * @param type $from
        * @param type $to
        * @param type $subject
        * @param type $body
        * @param type $isHtml
        * @param type $fromName
        * @return type
        */
       public static function sendEmailCommon(
               $from, $to, $subject, $body, $isHtml, $fromName=null){ 
           // incoming port 993

           require_once 'phpmailer/PHPMailerAutoload.php';
           $mail = new \PHPMailer();

           $noreply = 'noreply@rangecontroller.com';
           $mail->isSMTP();                                      // Set mailer to use SMTP
           $mail->Host = 'secure.emailsrvr.com';  // Specify main and backup SMTP servers
           $mail->SMTPAuth = true;                               // Enable SMTP authentication
           $mail->Username = $noreply;                 // SMTP username
           $mail->Password = 'dawman02';                           // SMTP password
           $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
           $mail->Port = 465; //587;                                    // TCP port to connect to

           $mail->From =  $noreply;
           $mail->FromName = empty($fromName) ? 'RangeController.Com' : $fromName;

           //$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
           $mail->addAddress($to);               // Name is optional
           //$mail->addReplyTo('info@example.com', 'Information');
           $mail->AddReplyTo($from);
           //$mail->addCC('cc@example.com');
           //$mail->addBCC('bcc@example.com');

           //$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
           //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
           //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
           $mail->isHTML($isHtml);                                  // Set email format to HTML

           $mail->Subject = $subject;
           $mail->Body    = $body;
           //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

           /**
           if(!$mail->send()) {
               echo 'Message could not be sent.';
               echo 'Mailer Error: ' . $mail->ErrorInfo;
           } else {
               echo 'Message has been sent';
           } */
           $r =  $mail->send();
           if (!$r) {
               //print $mail->ErrorInfo;
           } 

           return $r;
       }

    }
}