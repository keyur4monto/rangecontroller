<?php
session_start();
require('common/db.php');

$sql = "";
$sql = "SELECT company_name FROM controller_config WHERE accountnumber = '".$_GET['cn']."' "; 
if ($result = $link->query($sql)) { 
while ($row = $result->fetch_assoc()) { 
$company_name = $row['company_name']; 
}//END WHILE LOOP 
}//END IF
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Range Controller - Sign Up Form</title>
<?php
include 'includesOLD/meta.php';
?>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<style>
#welcome_table3 {
	display: none;
}
#headercol2 {
	display: none;
}
.table1 {
	color:white;
}
.table2 {
	color:white;
}
.table3 {
	color:white;
}
#content_table2 {
    background-color: #1e1e1e;
    border-spacing: 0;
    color: #ccc;
    font-size: 16px;
    line-height: 20px;
    margin: 0 auto;
    width: 950px;
}
</style>

<script>
function submitControllerSettings(){

var first_name    = document.getElementById("txtFirstName").value;
var last_name     = document.getElementById("txtLastName").value;
var street_address= document.getElementById("txtStreetAddress").value;
var city          = document.getElementById("txtCity").value;
var state         = document.getElementById("h_state").value;
var zipcode       = document.getElementById("txtZipcode").value;
var card_number   = document.getElementById("txtCardNumber").value;
var month_expired = document.getElementById("h_MonthExpired").value;
var year_expired  = document.getElementById("h_YearExpired").value;
var three_digit_code = document.getElementById("txtCVV2").value;
var captcha          = document.getElementById("recaptcha_response_field").value;

//create message if something is missing   
var msg = "";
if(first_name == ""){    msg += "Required First Name\n";}
if(last_name == ""){     msg += "Required Last Name\n";}
if(street_address == ""){msg += "Required Street Address\n";}
if(city == ""){          msg += "Required City\n";}
if(state == ""){         msg += "Required State\n";}
if(zipcode == ""){       msg += "Required Zipcode\n";}
if(card_number == ""){   msg += "Required Card Number\n";}
if(month_expired == ""){ msg += "Required Month Expired\n";}
if(year_expired == ""){  msg += "Required Year Expired\n";}
if(three_digit_code == ""){msg += "Required 3 Digit Security Code\n";}
if(captcha == ""){       msg += "Required Captcha\n";}

if(msg != ""){alert(msg);return;}

document.getElementById("frmPaymentForm").submit();
}//end function

function checkCaptcha(){
var cap = document.getElementById("cap").value;
var cap_value = document.getElementById("hMathValue").value;
if(parseInt(cap)!=parseInt(cap_value)){return false;}else{return true;}
}//end function

function selStateWithKey(state){
document.getElementById("h_state").value = state;
//alert("set with key: "+document.getElementById("h_state").value);
}//end function

function setTheState(state){
document.getElementById("h_state").value = state;
//alert("set with mouse: "+document.getElementById("h_state").value);
}//end function

function setMonthExpired(month){
document.getElementById("h_MonthExpired").value = month;
}//end function

function setYearExpired(year){
document.getElementById("h_YearExpired").value = year;
}//end function

document.oncontextmenu=RightMouseDown;
function RightMouseDown() { return false; }

history.pushState({ page: 1 }, "title 1", "#nbb");

window.onhashchange = function (event) {

window.location.hash = "nbb";
};

//window.onbeforeunload = function(){ return 'Please make sure all data is correct and then press submit';}

</script>
</head>
<body>
<div align="center"><img src="images/background.jpg" class="bg" alt=""/></div>
<?php
include 'includesOLD/header.php';
?>
<?php
if(!isset($_COOKIE['payment_form'])) 
{
?>
<div class="credit_debit">
<table id="content_table" border="0">
    <tr>
  
    <td id="contentcol">
  
  <form  id="frmPaymentForm" name="frmPaymentForm" action="common/process_payment_standard_new.php" method="POST">
    <!--common/process_payment_standard.php-->
    <input type="hidden" id="RecurMethodID" name="RecurMethodID" value="1"/>
    <!--<table id="company_setup_table" width="700">
        <tr>
          <td width="100%" class="table1" style="color: white;">Company set-up takes about 10 minutes!</td>
        </tr>
      </table>
      <table id="company_setup_table_desc" width="700">
        <tr>
          <td class="table3" > Enter your billing information to get your subscription started.  Once your
            account has been approved, you will be routed to the company information
            page.  Be sure and have your company info and logo on hand.  Your new
            RangeController software will be customized and ready to use in as little as
            10 minutes. </td>
        </tr>
      </table>
      <br />-->
    <table id="billing_info_table" width="700">
      <tr>
        <td width="100%" class="table1" style="color: white;">Enter Billing Information&emsp;(as it appears on your credit card statement)</td>
      </tr>
    </table>
    <table id="billing_info_table_desc" width="700">
      <tr>
        <td class="table2" >First Name:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="text" id="txtFirstName" name="txtFirstName" style="width: 200px" autocomplete="off" /></td>
      </tr>
    </table>
    <table id="billing_info_table_desc" width="700">
      <tr>
        <td class="table2">Last Name:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="text" id="txtLastName" name="txtLastName" value="" style="width: 200px" autocomplete="off"/></td>
      </tr>
    </table>
    <table id="billing_info_table_desc" width="700">
      <tr>
        <td class="table2">Street Address:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="text" id="txtStreetAddress" name="txtStreetAddress" value="" style="width: 200px" autocomplete="off"/></td>
      </tr>
    </table>
    <table id="billing_info_table_desc" width="700">
      <tr>
        <td class="table2">City:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="text" id="txtCity" name="txtCity" value="" style="width: 200px" autocomplete="off"/></td>
      </tr>
    </table>
    <table id="billing_info_table_desc" width="700">
      <tr>
        <td class="table2">State:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;
          <input type="hidden" id="h_state" name="h_state"/>
          <select name="selState" id="selState" size="1" onchange="setTheState(this.value);" onblur="selStateWithKey(this.value);">
            <option></option>
            <option value="AK">AK</option>
            <option value="AL">AL</option>
            <option value="AR">AR</option>
            <option value="AZ">AZ</option>
            <option value="CA">CA</option>
            <option value="CO">CO</option>
            <option value="CT">CT</option>
            <option value="DC">DC</option>
            <option value="DE">DE</option>
            <option value="FL">FL</option>
            <option value="GA">GA</option>
            <option value="HI">HI</option>
            <option value="IA">IA</option>
            <option value="ID">ID</option>
            <option value="IL">IL</option>
            <option value="IN">IN</option>
            <option value="KS">KS</option>
            <option value="KY">KY</option>
            <option value="LA">LA</option>
            <option value="MA">MA</option>
            <option value="MD">MD</option>
            <option value="ME">ME</option>
            <option value="MI">MI</option>
            <option value="MN">MN</option>
            <option value="MO">MO</option>
            <option value="MS">MS</option>
            <option value="MT">MT</option>
            <option value="NC">NC</option>
            <option value="ND">ND</option>
            <option value="NE">NE</option>
            <option value="NH">NH</option>
            <option value="NJ">NJ</option>
            <option value="NM">NM</option>
            <option value="NV">NV</option>
            <option value="NY">NY</option>
            <option value="OH">OH</option>
            <option value="OK">OK</option>
            <option value="OR">OR</option>
            <option value="PA">PA</option>
            <option value="RI">RI</option>
            <option value="SC">SC</option>
            <option value="SD">SD</option>
            <option value="TN">TN</option>
            <option value="TX">TX</option>
            <option value="UT">UT</option>
            <option value="VA">VA</option>
            <option value="VT">VT</option>
            <option value="WA">WA</option>
            <option value="WI">WI</option>
            <option value="WV">WV</option>
            <option value="WY">WY</option>
          </select></td>
      </tr>
    </table>
    <table id="billing_info_table_desc" width="700">
      <tr>
        <td class="table2">Zipcode:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="text" id="txtZipcode" name="txtZipcode" value="" style="width: 200px" autocomplete="off"/></td>
      </tr>
    </table>
    <br />
    <!--start credit card information-->
    <table id="billing_info_table_desc" width="700">
      <tr>
        <td class="table3" > All credit card information is encrypted and complies with state and
          federal laws for compliance.  Once your information is input, press
          the &rdquo;Submit&rdquo; button at the bottom of the page only once. </td>
      </tr>
    </table>
    <br />
    <table id="credit_card_table" width="700">
      <tr>
        <td width="700" class="table1">Enter Credit Card Information&emsp;<img src="images/visa_mc_dis.png"  style="height: 20px;"/></td>
      </tr>
    </table>
    <table id="credit_card_table_desc" width="700">
      <tr>
        <td class="table2">Card Number:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="text" id="txtCardNumber" name="txtCardNumber" /></td>
      </tr>
    </table>
    <table id="credit_card_table_desc" width="700">
      <tr>
        <td class="table2">Month Expired:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="hidden" id="h_MonthExpired" name="h_MonthExpired" />
          <select id="selMonthExpired" name="selMonthExpired" onchange="setMonthExpired(this.value);">
            <option></option>
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
          </select></td>
      </tr>
    </table>
    <table id="credit_card_table_desc" width="700">
      <tr>
        <td class="table2">Year Expired:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="hidden" id="h_YearExpired" name="h_YearExpired" />
          <select id="selYearExpired" name="selYearExpired" onchange="setYearExpired(this.value);" style="width: 85px;">
            <option></option>
            <option value="2013">2013</option>
            <option value="2014">2014</option>
            <option value="2015">2015</option>
            <option value="2016">2016</option>
            <option value="2016">2017</option>
            <option value="2018">2018</option>
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
            <option value="2022">2022</option>
            <option value="2023">2023</option>
            <option value="2024">2024</option>
            <option value="2025">2025</option>
            <option value="2026">2026</option>
            <option value="2027">2027</option>
          </select></td>
      </tr>
    </table>
    <table id="credit_card_table_desc" width="700">
      <tr>
        <td class="table2">3 Digit Code:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <input type="text" id="txtCVV2" name="txtCVV2" style="width: 85px;"/>
          &nbsp;( on back of card )</td>
      </tr>
    </table>
    <table id="credit_card_table_desc" width="700" border="0">
      <tr>
        <td class="table2">Payment Sum:&nbsp;&nbsp;</td>
        <td class="table3">
		<input type="hidden" id="h_payment_sum" name="h_payment_sum" value="339.00" />
          &nbsp;&nbsp;<br>
          <label id="lblPaymentSum" style="color: red;">$339.00</label>
          <br>
          <span style="font-size: 12px; font-style: italic;">($49 monthly charge + $290 yearly maintenance fee)</span>
          <?php     
/*          
<label id="lblProratedSum" style="color: green;">        			
//CALCULATE PRORATED AMOUNT, eProcessingNetwork RECUR BILLING IS SET TO MONTHLY
$monthly_charge = 49.95;
$days_in_month = date('t');//DAYS IN MONTH
$value1 = $monthly_charge / $days_in_month;
$value2 = $value1 * date('j');//DAY OF MONTH
$pro_rated_total = $monthly_charge - $value2;
//if(date('j')!=1){
//echo "<input type='hidden' id='RecurAmountOverride' name='RecurAmountOverride' value='".money_format('%i', $pro_rated_total)."' />";
//echo money_format('$%i', $pro_rated_total)."&nbsp;(pro rated amount as of today)";
//}//END IF
if(date('j') != 1){
echo "<input type='hidden' id='RecurAmountOverride' name='RecurAmountOverride' value='".money_format('%i', $pro_rated_total)."' />";
if(date('j')!=date('t')){
echo money_format('$%i', $pro_rated_total)."&nbsp;(pro rated amount as of today)";
}else{echo "----------------";}
}//END IF
</label>
*/
?>
          &nbsp; </td>
      </tr>
    </table>
    <table id="credit_card_table_desc" width="700">
      <!--<tr>
          <td class="table2">Captcha:&nbsp;&nbsp;</td>
          <td class="table3">&nbsp;&nbsp;
<?php	                                       			 
$switch_value = rand(1,5);
switch($switch_value){            
case 1: echo '&nbsp;5 + 5 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="10" />';
break;
case 2: echo '&nbsp;10 + 5 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="15" />';
break;
case 3: echo '&nbsp;1 + 1 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="2" />';
break;
case 4: echo '&nbsp;2 + 1 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="3" />';
break;
case 5: echo '&nbsp;3 + 3 = <input autocomplete="off" type="text" name="cap" id="cap" maxlength="3" style="width:40px;height:20px;text-align: center;" /><input id="hMathValue" name="hMathValue" type="hidden" value="6" />';           
break;
}//-->end switch
      ?>
        </td>
      
        </tr>
      -->
      <tr>
        <td class="table2">Captcha:&nbsp;&nbsp;</td>
        <td class="table3">&nbsp;&nbsp;
          <?php

require_once('recaptchalib.php');

// Get a key from https://www.google.com/recaptcha/admin/create
$publickey = "6LeVIwUTAAAAAB_9O19TaFpXp2sGzB7qa8OCW9dq";
$privatekey = "6LeVIwUTAAAAAMdTUzq0epQoigqynFyh73ne9LVc";

# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
$error = null;


# was there a reCAPTCHA response?

        $resp = recaptcha_check_answer ($privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
$che = $resp->is_valid;
        if ($resp->is_valid) {
                echo "You got it!";
        } else {
                # set the error code so that we can display it
                $error = $resp->error;
        
}
echo recaptcha_get_html($publickey,'',$error);
?></td>
      </tr>
    </table>
    <!--end credit card information-->
    <table id="submit_order_table" width="700">
      <tr>
        <td width="700" class="table1">Submit Order Form</td>
      </tr>
    </table>
    <table id="submit_order_table_desc" width="700" >
      <tr>
        <input type="hidden" id="h_submit" name="h_submit" />
        <!--<td class="table9"><input type="submit" id="btnSubmit" name="btnSubmit" onclick="submitControllerSettings();" value="Submit" class="payment_button" /></td>-->
        <td class="table9"><input type="button" id="btnSubmit" name="btnSubmit" class="payment_button" value="Submit" onclick="submitControllerSettings();" /></td>
      </tr>
    </table>
      </div>
    
      

    <input type="hidden" name="ePNAccount" value="1011371" />
    <!-- # for testing 080880                        original # 1011371-->
    <input type="hidden" name="ID" />
    <!-- RE:CVV2Type value for required cvv code is 1 sent to ePN -->
    <input type="hidden" name="CVV2Type" value="1" />
    <input type="hidden" name="RestrictKey" value="O8y9gcnP4Lh9Vgy"/>
    <!--# for testing yFqqXJh9Pqnugfr                   original# O8y9gcnP4Lh9Vgy-->
    <input type="hidden" name="ReturnApprovedURL" value="" />
    <input type="hidden" name="ReturnDeclinedURL" value="" />
    <input type="hidden" name="BackgroundColor" />
    <input type="hidden" name="TextColor" />
    <!--END RIGHT PANE-->
  </form>
	 </td>
    </td>
	</tr>
</table>
</div>
<?php
}
else
{
	echo "Access Denied!";
}
include 'includesOLD/footer.php';
?>
</div>
</body>
</html>
<style>
.recaptchatable * {
	border: 0 none;
	bottom: auto;
	color: red;
	font-family: helvetica, sans-serif;
	font-size: 8pt;
	left: auto;
	margin: 0;
	padding: 0;
	position: static;
	right: auto;
	top: auto;
}
</style>
