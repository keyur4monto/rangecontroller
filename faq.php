<?php
include 'includes/session.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Range Controller - Frequently Asked Questions</title><?php
            include 'includes/headerTags.php';
            ?>
</head>
<body>
    <?php
            include 'includes/header.php';
            ?><?php
            include 'includes/navBar.php';
            ?>
    <div id="faq">
        <h1>FAQ</h1>
        <p><strong>What is Range Controller?</strong><br>
        <span class="smallText">Range Controller is a web based application
        that keeps track of your customers shooting and spending
        habits.</span></p>
        <p><strong>How much does Range Controller cost?</strong><br>
        <span class="smallText">Range Controller has a yearly maintenance fee
        of $499, but currently on sale for $290, and a monthly fee of
        $49.</span></p>
        <p>More info coming soon!</p>
    </div><?php
            include 'includes/content.php';
            ?><?php
            include 'includes/footer.php';
            ?>
</body>
</html>