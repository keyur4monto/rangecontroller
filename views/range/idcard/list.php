<?php
/*echo '<pre>';
print_r($getidcarddetail->fetch_assoc());
echo '</pre>';*/
while ($row1 = $getidcarddetail->fetch_assoc()) { 
    $gender = $row1['gender'];
    $client_name = $row1['client_name'];
    $client_picture = $row1['client_picture'];
    $client_address = $row1['client_address'];
    $client_dob = $row1['client_dob'];
    $client_certs = $row1['client_certs'];
    $client_emergency_contact = $row1['client_emergency_contact'];
    $client_membership_type = $row1['client_membership_type'];
    $client_membership_number = $row1['membership_number'];
    $client_expiration_date = $row1['expiration_date'];
    
    $barcode = $row1['barcode'];
    $company_name = $row1['company_name'];
    $company_logo = $row1['company_logo'];
    $use_logo_as_background = $row1['use_logo_for_background'];
    $company_address = $row1['company_address'];
    $company_phone_number = $row1['company_phone_number'];
    $use_company_slogan = $row1['use_company_slogan'];
    $company_slogan = $row1['company_slogan'];
    $membership_border_banner = $row1['membership_border_banner'];
    $use_special_instructions = $row1['use_special_instructions'];
    $special_instructions = $row1['special_instructions'];
    $shooter_portal_text = $row1['shooter_portal_text'];
    $shooter_login_and_password = $row1['shooter_login_and_password'];
    $use_background_image = $row1['use_background_image'];
}
?>

<link rel="stylesheet" type="text/css" href="../../tools/protoplasm_full.css"/>
<script lang="javascript" src="../../tools/protoplasm.js"></script>
<script lang="javascript">
Protoplasm.use('upload').transform('input.upload', {
'inline': true
});
</script>
<script>
window.onload = function() {
 check();
};
function check()
{
		if(document.getElementById("chkClientName").checked==true)
		{
		document.getElementById("client_name").style.display = 'block';
		}
		if(document.getElementById("chkClientPicture").checked==true)
		{
		document.getElementById("client_picture").style.display = 'block';
		}
		if(document.getElementById("chkClientAddress").checked==true)
		{
		document.getElementById("client_address").style.display = 'block';
		document.getElementById("client_address1").style.display = 'block';
		}
		if(document.getElementById("chkClientDOB").checked==true)
		{
		document.getElementById("client_dob").style.display = 'block';
		}
		if(document.getElementById("chkClientCerts").checked==true)
		{
		document.getElementById("client_certification").style.display = 'block';
		}
		if(document.getElementById("chkClientEmergencyContact").checked==true)
		{
		document.getElementById("emergency_contact").style.display = 'block';
		}
		if(document.getElementById("chkClientMembershipType").checked==true)
		{
		  document.getElementById("membership_type").style.display = 'block';
		}
        if(document.getElementById("chkClientExpirationDate").checked==true)
		{
		  document.getElementById("expiration_date").style.display = 'block';
		}
		if(document.getElementById("chkMembershipBorderBanner").checked==true)
		{
		document.getElementById("header_r_btm").classList.remove("back-none");
		document.getElementById("client_picture").classList.remove("border-none");
		}
		if(document.getElementById("chkClientMembershipNumber").checked==true)
		{
		document.getElementById("membership_number").style.display = 'block';
		}
		if(document.getElementById("chkBarcode").checked==true)
		{
		document.getElementById("barcode").style.display = 'block';
		}
		if(document.getElementById("chkCompanyName").checked==true)
		{
		document.getElementById("company_name").style.display = 'block';
		}
		if(document.getElementById("chkCompanyLogo").checked==true)
		{
		document.getElementById("company_logo").style.display = 'block';
		}
		if(document.getElementById("chkCompanyAddress").checked==true)
		{
		document.getElementById("company_address").style.display = 'block';
		document.getElementById("company_address1").style.display = 'block';
		}
		if(document.getElementById("chkCompanyPhoneNumber").checked==true)
		{
		document.getElementById("company_phone_number").style.display = 'block';
		}
		if(document.getElementById("chkUseCompanySlogan").checked==true)
		{
		document.getElementById("company_slogan").style.display = 'block';
		}
		if(document.getElementById("chkUseSpecialInstructions").checked==true)
		{
		document.getElementById("special_instructions").style.display = 'block';
		}
		if(document.getElementById("chkShooterLoginAndPassword").checked==true)
		{
		document.getElementById("login_pass").style.display = 'block';
		}
		if(document.getElementById("chkShooterPortalText").checked==true)
		{
		document.getElementById("rangecontroller_shooter_portal").style.display = 'block';
		}
		if(document.getElementById("chkClientName").checked==false)
		{
		document.getElementById("client_name").style.display = 'none';
		}
		if(document.getElementById("chkClientPicture").checked==false)
		{
			document.getElementById("client_picture").style.display = 'none';
		}
		if(document.getElementById("chkClientAddress").checked==false)
		{
		document.getElementById("client_address").style.display = 'none';
		document.getElementById("client_address1").style.display = 'none';
		}
		if(document.getElementById("chkClientDOB").checked==false)
		{
		document.getElementById("client_dob").style.display = 'none';
		}
		if(document.getElementById("chkClientCerts").checked==false)
		{
		document.getElementById("client_certification").style.display = 'none';
		}
		if(document.getElementById("chkClientEmergencyContact").checked==false)
		{
		document.getElementById("emergency_contact").style.display = 'none';
		}
		if(document.getElementById("chkClientMembershipType").checked==false)
		{
			document.getElementById("membership_type").style.display = 'none';
		}
        if(document.getElementById("chkClientExpirationDate").checked==false)
		{
		  document.getElementById("expiration_date").style.display = 'none';
		}
        
		if(document.getElementById("chkMembershipBorderBanner").checked==false)
		{
			document.getElementById("header_r_btm").classList.add("back-none");
			document.getElementById("client_picture").classList.add("border-none");
		}
		if(document.getElementById("chkClientMembershipNumber").checked==false)
		{
		document.getElementById("membership_number").style.display = 'none';
		}
		if(document.getElementById("chkBarcode").checked==false)
		{
		document.getElementById("barcode").style.display = 'none';
		}
		if(document.getElementById("chkCompanyName").checked==false)
		{
		document.getElementById("company_name").style.display = 'none';
		}
		if(document.getElementById("chkCompanyLogo").checked==false)
		{
		document.getElementById("company_logo").style.display = 'none';
		}
		if(document.getElementById("chkCompanyAddress").checked==false)
		{
		document.getElementById("company_address").style.display = 'none';
		document.getElementById("company_address1").style.display = 'none';
		}
		if(document.getElementById("chkCompanyPhoneNumber").checked==false)
		{
		document.getElementById("company_phone_number").style.display = 'none';
		}
		if(document.getElementById("chkUseCompanySlogan").checked==false)
		{
		document.getElementById("company_slogan").style.display = 'none';
		}
		if(document.getElementById("chkUseSpecialInstructions").checked==false)
		{
		document.getElementById("special_instructions").style.display = 'none';
		}
		if(document.getElementById("chkShooterLoginAndPassword").checked==false)
		{
		document.getElementById("login_pass").style.display = 'none';
		}
		if(document.getElementById("chkShooterPortalText").checked==false)
		{
			document.getElementById("rangecontroller_shooter_portal").style.display = 'none';
		}
}
// <![CDATA[  
function overlaySearch(){
el1 = document.getElementById("overlay");
el1.style.visibility = (el1.style.visibility == "visible") ? "hidden" : "visible";
el2 = document.getElementById("div_search");
el2.style.visibility = (el2.style.visibility == "visible") ? "hidden" : "visible";
if(el1.style.visibility=="hidden"||el2.style.visibility=="hidden"){}//location.reload();
}//end overlaySearch

function overlayPreview(){
var g = document.getElementById("h_gender").value;
document.getElementById("embedObject").src = "sam?g="+g;
//document.getElementById("embedObject").data = "id-card-layout.php?g="+g;

el1 = document.getElementById("overlay");
el1.style.visibility = (el1.style.visibility == "visible") ? "hidden" : "visible";
el2 = document.getElementById("div_preview");
el2.style.visibility = (el2.style.visibility == "visible") ? "hidden" : "visible";
el3 = document.getElementById("embedObject");
el3.style.display = (el3.style.display == "none") ? "none" : "block";

el4 = document.getElementById("lblLoadingPreview");
window.setTimeout(function(){
el4.style.visibility = "hidden";
},2005);
if(el4.style.visibility=="hidden"){location.reload();}//location.reload();
}//end overlayPreview

//START AJAX TO FILL SELECT WITH LIVE SEARCH
function populateSelect(ctrl_id,str){
if (str==""){document.getElementById("divSearchResults").innerHTML="";
return;
}
if (window.XMLHttpRequest){
// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else{
// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
document.getElementById("divSearchResults").innerHTML = xmlhttp.responseText;
}
}//end onreadystatechange
xmlhttp.open("GET","ajax-search-cust.php?ctrl_id="+ctrl_id+"&q="+str,true);
xmlhttp.send();
}//END AJAX TO FILL SELECT WITH LIVE SEARCH

function setCheckedPreferences(id,field_name)
{
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
    else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			if(xmlhttp.responseText!="success")
			{
				alert("Error: setting "+field_name+" value. Please contact tech support.");
			}
			else
			{
				autoSave(id);
			}
		}//end if
	}//end onreadystatechange
	var chk = document.getElementById(id).checked;
	if(chk==true)
	{
		value="Yes";
		if(document.getElementById("chkClientName").checked==true)
		{
		document.getElementById("client_name").style.display = 'block';
		}
		if(document.getElementById("chkClientPicture").checked==true)
		{
		document.getElementById("client_picture").style.display = 'block';
		}
		if(document.getElementById("chkClientAddress").checked==true)
		{
		document.getElementById("client_address").style.display = 'block';
		document.getElementById("client_address1").style.display = 'block';
		}
		if(document.getElementById("chkClientDOB").checked==true)
		{
		document.getElementById("client_dob").style.display = 'block';
		}
		if(document.getElementById("chkClientCerts").checked==true)
		{
		document.getElementById("client_certification").style.display = 'block';
		}
		if(document.getElementById("chkClientEmergencyContact").checked==true)
		{
		document.getElementById("emergency_contact").style.display = 'block';
		}
		if(document.getElementById("chkClientMembershipType").checked==true)
		{
			document.getElementById("membership_type").style.display = 'block';
		}
        if(document.getElementById("chkClientExpirationDate").checked==true)
		{
		  document.getElementById("expiration_date").style.display = 'block';
		}
        
		if(document.getElementById("chkMembershipBorderBanner").checked==true)
		{
			document.getElementById("header_r_btm").classList.remove("back-none");
			document.getElementById("client_picture").classList.remove("border-none");
		}
		if(document.getElementById("chkClientMembershipNumber").checked==true)
		{
		document.getElementById("membership_number").style.display = 'block';
		}
		if(document.getElementById("chkBarcode").checked==true)
		{
		document.getElementById("barcode").style.display = 'block';
		}
		if(document.getElementById("chkCompanyName").checked==true)
		{
		document.getElementById("company_name").style.display = 'block';
		}
		if(document.getElementById("chkCompanyLogo").checked==true)
		{
		document.getElementById("company_logo").style.display = 'block';
		}
		if(document.getElementById("chkCompanyAddress").checked==true)
		{
		document.getElementById("company_address").style.display = 'block';
		document.getElementById("company_address1").style.display = 'block';
		}
		if(document.getElementById("chkCompanyPhoneNumber").checked==true)
		{
		document.getElementById("company_phone_number").style.display = 'block';
		}
		if(document.getElementById("chkUseCompanySlogan").checked==true)
		{
		document.getElementById("company_slogan").style.display = 'block';
		}
		if(document.getElementById("chkUseSpecialInstructions").checked==true)
		{
		document.getElementById("special_instructions").style.display = 'block';
		}
		if(document.getElementById("chkShooterLoginAndPassword").checked==true)
		{
		document.getElementById("login_pass").style.display = 'block';
		}
		if(document.getElementById("chkShooterPortalText").checked==true)
		{
		document.getElementById("rangecontroller_shooter_portal").style.display = 'block';
		}
	}
	else
	{
		value="No";
		if(document.getElementById("chkClientName").checked==false)
		{
		document.getElementById("client_name").style.display = 'none';
		}
		if(document.getElementById("chkClientPicture").checked==false)
		{
		document.getElementById("client_picture").style.display = 'none';
		}
		if(document.getElementById("chkClientAddress").checked==false)
		{
		document.getElementById("client_address").style.display = 'none';
		document.getElementById("client_address1").style.display = 'none';
		}
		if(document.getElementById("chkClientDOB").checked==false)
		{
		document.getElementById("client_dob").style.display = 'none';
		}
		if(document.getElementById("chkClientCerts").checked==false)
		{
		document.getElementById("client_certification").style.display = 'none';
		}
		if(document.getElementById("chkClientEmergencyContact").checked==false)
		{
		document.getElementById("emergency_contact").style.display = 'none';
		}
		if(document.getElementById("chkClientMembershipType").checked==false)
		{
			document.getElementById("membership_type").style.display = 'none';
		}
        if(document.getElementById("chkClientExpirationDate").checked==false)
		{
		  document.getElementById("expiration_date").style.display = 'none';
		}
        
        
		if(document.getElementById("chkMembershipBorderBanner").checked==false)
		{
			document.getElementById("header_r_btm").classList.add("back-none");
			document.getElementById("client_picture").classList.add("border-none");
		}
		if(document.getElementById("chkClientMembershipNumber").checked==false)
		{
		document.getElementById("membership_number").style.display = 'none';
		}
		if(document.getElementById("chkBarcode").checked==false)
		{
		document.getElementById("barcode").style.display = 'none';
		}
		if(document.getElementById("chkCompanyName").checked==false)
		{
		document.getElementById("company_name").style.display = 'none';
		}
		if(document.getElementById("chkCompanyLogo").checked==false)
		{
		document.getElementById("company_logo").style.display = 'none';
		}
		if(document.getElementById("chkCompanyAddress").checked==false)
		{
		document.getElementById("company_address").style.display = 'none';
		document.getElementById("company_address1").style.display = 'none';
		}
		if(document.getElementById("chkCompanyPhoneNumber").checked==false)
		{
		document.getElementById("company_phone_number").style.display = 'none';
		}
		if(document.getElementById("chkUseCompanySlogan").checked==false)
		{
		document.getElementById("company_slogan").style.display = 'none';
		}
		if(document.getElementById("chkUseSpecialInstructions").checked==false)
		{
		document.getElementById("special_instructions").style.display = 'none';
		}
		if(document.getElementById("chkShooterLoginAndPassword").checked==false)
		{
		document.getElementById("login_pass").style.display = 'none';
		}
		if(document.getElementById("chkShooterPortalText").checked==false)
		{
		document.getElementById("rangecontroller_shooter_portal").style.display = 'none';
		}
	}
	xmlhttp.open("POST","ajaxSetPreferences",false);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("field_name="+field_name+"&value="+value);

	autoRefreshPreview();
}//END AJAX setCheckedPreferences

function setText(id,field_name){
var ctrl = document.getElementById(id).value;
if(ctrl==""){alert("Required Input.");return;}
if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}
else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
if(xmlhttp.responseText!="success"){alert("Error: setting "+field_name+" value. Please contact tech support.");}
}//end if
}//end onreadystatechange
var value = document.getElementById(id).value;

switch(id){
case "rdMaleGender":{value = "male";}break;
case "rdFemaleGender":{value = "female";}break;
}//end switch

xmlhttp.open("POST","ajaxSetPreferences",false);
xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xmlhttp.send("field_name="+field_name+"&value="+value);

autoRefreshPreview();
}//end AJAX setSlogan

function autoSave(id){

document.getElementById("lblAutoSave").innerHTML = "Saving...";

window.setTimeout(function(){
document.getElementById("lblAutoSave").innerHTML = "All data is now auto saved...";
},2000);

window.setTimeout(function(){
switch(id){
case "txtCompanySlogan":{setText('txtCompanySlogan','company_slogan');}break;
case "txtSpecialInstructions":{setText('txtSpecialInstructions','special_instructions');}break;
}//end switch
},2000);

}//end auto save

function setGender(id,field_name){
//document.getElementById("h_gender").value = gender;
setText(id,field_name);
autoSave(id);
}//end set gender

function autoRefreshPreview(){
//document.getElementById('ifrmPreview').contentWindow.location.reload(true);//for an iframe  
document.getElementById("ifrmPreview").innerHTML = document.getElementById("ifrmPreview").innerHTML;  
}
// ]]>
</script>
<style>
#overlay {
	visibility: hidden;
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background-color: rgba(0, 0, 0, 0.5);
	z-index: 10;
}
#div_search {
	visibility: hidden;
	width: 500px;
	height: 425px;
	position: fixed;
	top: 15%;
	left: 20%;
	background-color: #1e1e1e;
	border-radius: 5px;
	text-align: center;
	z-index: 11; /* 1px higher than the overlay layer */
	border-style: solid;
	border-color: #808080;
}
#div_preview {
	visibility: hidden;
	width: 400px;
	height: 365px;
	position: fixed;
	top: 20%;
	left: 35%;
	margin-top:50px;
	background-color: #1e1e1e;
	border-radius: 5px;
	text-align: center;
	z-index: 11; /* 1px higher than the overlay layer */
	border-style: solid;
	border-color: #808080;
}
#div_preview table tr td label {
	position: relative;
	top:90px;
}
#embedObject {
	display: none;
}
#aClose {
	position: relative;
	top:-10px;
}
</style>
<?php
include('includes/browser.php');
$b = new Browser;

switch($b->getBrowser()){
case "Safari":{
print '
<style>
#div_preview{
visibility: hidden;
width: 400px;
height: 360px;
position: fixed;
top: 20%; 
left: 40%;
margin-top:50px;
background-color: #1e1e1e;
border-radius: 5px;
text-align: center;
z-index: 11; /* 1px higher than the overlay layer */
border-style: solid;
border-color: #808080;
}

#div_preview table tr td label{
position: relative; top:100px;
}

#aClose{
position: relative; top:-20px;
}
</style>
';
}break;

case "Chrome":{

}break;

case "Internet Explorer":{
//print '<link rel="stylesheet" type="text/css" href="../../css/internet_explorer.css"/>';
}break;

case "Firefox":{
//print '<link rel="stylesheet" type="text/css" href="../../css/fire_fox.css"/>';
}break;

}//END SWITCH
?>
<div class="grid_3" id="breadcrumbsRight"> </div>
<div class="clear"></div>
<div class="grid_12">
<div class="text-right"> </div>
</div>
<div class="margt"></div>
<div class="grid_12">
      <div class="margb text-center" style="color: red;"><label id="lblAutoSave" style="color: red;">** All data is autosaved **</label></div>
      <table>
        <tr>
          <th colspan="3" style="background-color: #666;">Step 1: Choose a layout</th>
        </tr>
        <tr class="text-center">
          <td style="width: 30%;"></td>
          <td style="width: 30%;">
          <img src="/xapp/tools/idcreator/cardexample1.jpg">
          </br>
          <input type="radio" id="id_card_layput" name="id_card_layput" checked="checked" />
          </td>
          <td style="width: 30%;"></td>
        </tr>
      </table>
      <table>
        <tr>
           <th colspan="3" style="background-color: #666;">Step 2: Insert data from client file</th>
        </tr>
        <tr>
          <td width="30%"><?php
                if($client_name=="Yes"){
                print '<input type="checkbox" id="chkClientName" onclick="setCheckedPreferences(this.id,\'client_name\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientName" onclick="setCheckedPreferences(this.id,\'client_name\');"/>';
                }
                ?>
            &nbsp;Client Name (First, Mi, Last) </td>
          <td width="30%"><?php
                if($client_picture=="Yes"){
                print '<input type="checkbox" id="chkClientPicture" onclick="setCheckedPreferences(this.id,\'client_picture\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientPicture" onclick="setCheckedPreferences(this.id,\'client_picture\');"/>';
                }
                ?>
            &nbsp;Client Picture </td>
          <td width="30%"><?php
                if($client_address=="Yes"){
                print '<input type="checkbox" id="chkClientAddress" onclick="setCheckedPreferences(this.id,\'client_address\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientAddress" onclick="setCheckedPreferences(this.id,\'client_address\');"/>';
                }
                ?>
            &nbsp;Client Address</td>
        </tr>
        <tr>
          <td width="30%"><?php
                if($client_dob=="Yes"){
                print '<input type="checkbox" id="chkClientDOB" onclick="setCheckedPreferences(this.id,\'client_dob\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientDOB" onclick="setCheckedPreferences(this.id,\'client_dob\');"/>';
                }
                ?>
            &nbsp;Client Date of Birth </td>
          <td width="30%"><?php
                if($client_certs=="Yes"){
                print '<input type="checkbox" id="chkClientCerts" onclick="setCheckedPreferences(this.id,\'client_certs\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientCerts" onclick="setCheckedPreferences(this.id,\'client_certs\');"/>';
                }
                ?>
            &nbsp;Client Certifications</td>
          <td width="30%"><?php
                if($client_emergency_contact=="Yes"){
                print '<input type="checkbox" id="chkClientEmergencyContact" onclick="setCheckedPreferences(this.id,\'client_emergency_contact\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientEmergencyContact" onclick="setCheckedPreferences(this.id,\'client_emergency_contact\');"/>';
                }
                ?>
            &nbsp;Client Emergency Contact</td>
        </tr>
        <tr>
          <td width="30%"><?php
                if($client_membership_type=="Yes"){
                print '<input type="checkbox" id="chkClientMembershipType" onclick="setCheckedPreferences(this.id,\'client_membership_type\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientMembershipType" onclick="setCheckedPreferences(this.id,\'client_membership_type\');"/>';
                }
                ?>
            &nbsp;Membership Type </td>
          <td width="30%">
                <?php
                if($client_expiration_date=="Yes"){
                    print '<input type="checkbox" id="chkClientExpirationDate" onclick="setCheckedPreferences(this.id,\'expiration_date\');" checked/>';
                }else{
                    print '<input type="checkbox" id="chkClientExpirationDate" onclick="setCheckedPreferences(this.id,\'expiration_date\');"/>';
                }
                ?>
            &nbsp;Expiration Date
          </td>
          <td width="30%">&nbsp;</td>
        </tr>
      </table>
      <table>
        <tr>
          <th colspan="3" style="background-color: #666;">Step 3: Insert data from company file</th>
        </tr>
        <tr>
          <td width="30%"><?php
                if($client_membership_number=="Yes"){
                print '<input type="checkbox" id="chkClientMembershipNumber" onclick="setCheckedPreferences(this.id,\'membership_number\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkClientMembershipNumber" onclick="setCheckedPreferences(this.id,\'membership_number\');"/>';
                }
                ?>
            &nbsp;Show Membership Number</td>
          <td width="30%"><?php
                if($barcode=="Yes"){
                print '<input type="checkbox" id="chkBarcode" onclick="setCheckedPreferences(this.id,\'barcode\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkBarcode" onclick="setCheckedPreferences(this.id,\'barcode\');"/>';
                }
                ?>
            &nbsp;Add Generated Barcode</td>
          <td width="30%"><?php
                if($company_name=="Yes"){
                print '<input type="checkbox" id="chkCompanyName" onclick="setCheckedPreferences(this.id,\'company_name\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkCompanyName" onclick="setCheckedPreferences(this.id,\'company_name\');"/>';
                }
                ?>
            &nbsp;Company Name</td>
        </tr>
        <tr>
          <td width="30%"><?php
                if($company_logo=="Yes"){
                print '<input type="checkbox" id="chkCompanyLogo" onclick="setCheckedPreferences(this.id,\'company_logo\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkCompanyLogo" onclick="setCheckedPreferences(this.id,\'company_logo\');"/>';
                }
                ?>
            &nbsp;Company Logo
            <!-- 
                  <ul>                    
                <?php
                
                if($use_logo_as_background=="Yes"){
                print '<input type="checkbox" id="chkBlnUseCompanyLogo" onclick="setCheckedPreferences(this.id,\'use_logo_for_background\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkBlnUseCompanyLogo" onclick="setCheckedPreferences(this.id,\'use_logo_for_background\');"/>';
                }
                
                ?>
                  &nbsp;Use logo as background
                  </ul>
               --></td>
          <td width="30%"><?php
                if($company_address=="Yes"){
                print '<input type="checkbox" id="chkCompanyAddress" onclick="setCheckedPreferences(this.id,\'company_address\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkCompanyAddress" onclick="setCheckedPreferences(this.id,\'company_address\');"/>';
                }
                ?>
            &nbsp;Company Address</td>
          <td width="30%"><?php
                if($company_phone_number=="Yes"){
                print '<input type="checkbox" id="chkCompanyPhoneNumber" onclick="setCheckedPreferences(this.id,\'company_phone_number\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkCompanyPhoneNumber" onclick="setCheckedPreferences(this.id,\'company_phone_number\');"/>';
                }
                ?>
            &nbsp;Company Phone Number</td>
        </tr>
        <tr>
          <td width="30%"><?php
                if($use_company_slogan=="Yes"){
                print '<input type="checkbox" id="chkUseCompanySlogan" onclick="setCheckedPreferences(this.id,\'use_company_slogan\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkUseCompanySlogan" onclick="setCheckedPreferences(this.id,\'use_company_slogan\');"/>';
                }
                ?>
            &nbsp;Use Company Slogan<br />
            <textarea id="txtCompanySlogan" cols="30" onKeyUp="autoSave('txtCompanySlogan');"><?=$company_slogan?>
</textarea></td>
          <td width="30%"><?php
                if($use_special_instructions=="Yes"){
                print '<input type="checkbox" id="chkUseSpecialInstructions" onclick="setCheckedPreferences(this.id,\'use_special_instructions\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkUseSpecialInstructions" onclick="setCheckedPreferences(this.id,\'use_special_instructions\');"/>';
                }
                ?>
            &nbsp;Use Special Instructions<br />
            <textarea id="txtSpecialInstructions" cols="30" onKeyUp="autoSave('txtSpecialInstructions');"><?=$special_instructions?>
</textarea></td>
          <td width="30%"><?php
                if($membership_border_banner=="Yes"){
                print '<input type="checkbox" id="chkMembershipBorderBanner" onclick="setCheckedPreferences(this.id,\'membership_border_banner\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkMembershipBorderBanner" onclick="setCheckedPreferences(this.id,\'membership_border_banner\');"/>';
                }
                ?>
            &nbsp;Add Border to Membership rank and client image</td>
        </tr>
        <tr>
          <td width="30%"><?php
                if($shooter_portal_text=="Yes"){
                print '<input type="checkbox" id="chkShooterPortalText" onclick="setCheckedPreferences(this.id,\'shooter_portal_text\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkShooterPortalText" onclick="setCheckedPreferences(this.id,\'shooter_portal_text\');"/>';
                }
                ?>
            &nbsp;Use Rangecontroller Shooter Portal</td>
          <td width="30%"><?php
                if($shooter_login_and_password=="Yes"){
                print '<input type="checkbox" id="chkShooterLoginAndPassword" onclick="setCheckedPreferences(this.id,\'shooter_login_and_password\');" checked/>';
                }else{
                print '<input type="checkbox" id="chkShooterLoginAndPassword" onclick="setCheckedPreferences(this.id,\'shooter_login_and_password\');"/>';
                }
                ?>
            &nbsp;Print Shooter Portal login and password at base of the card</td>
          <td width="30%">&nbsp;</td>
        </tr>
      </table>
      <table>
      <tr>
        <th style="background-color: #666;">Step 4: ID Card Preview</th>
      </tr>
      <tr>
        <td>
<style>
.idcard {margin:0; padding:0; /*background:#d1d1d1;*/ font-family:Arial, Helvetica, sans-serif; }
.clr{clear:both; font-size:0; line-height:0; height:0px;}
.idcard{ color:#000;}
.wapper{width:323.527559px;margin:0 auto; padding:3px 0px 3px 3px; background:#FFF; -webkit-box-shadow: 0px 5px 10px 0px rgba(50, 50, 50, 0.38);
-moz-box-shadow:    0px 5px 10px 0px rgba(50, 50, 50, 0.38);
box-shadow:         0px 5px 10px 0px rgba(50, 50, 50, 0.38); height:204.094488px;}
.header{}
.header_l{float:left; display:inline-block; width:99px;}
.header_img{border: 5px solid #000; height:80px; width:89px;}
.left_id {display:block; width:100%; text-align:center; font-size:14px; font-weight:bold; color:#000; margin-top:8px; }
.header_r{float:left; display:inline-block; width:218px;}

.header_r_img {  display: inline-block; margin:6px 14px; float:left;}

.header_r_text {  float:left; display:block;}
.header_r_text p{margin-top:0px; margin-bottom:3px;}
.header_r_titel{font-size:12px; font-weight:bold; margin-left:5px; margin-bottom:0px; margin-top:0px;}
.header_r_text span{font-size:9.33333px; font-weight:bold;}
strong{font-size:8px; font-weight:bold;}
.header_r_btm{display:block; background:#000; font-size:13.3333px; font-weight:bold; color:#FFF; width:225px; text-align:center;}
.header_r_btm.back-none {
  background: #ffffff;
  color: #000000;
}
.header_r_btm2{width:225px; padding-left:22px;}
.btm2_l{float:left; display:inline-block;}
.btm2_l strong{font-size:8px; font-weight:bold;}
.btm2_l span{font-size:10.6667px; font-weight:bold;}
.btm2_r{ display: block;    float: right;    font-size: 10px;    font-weight: bold;    text-align: right; margin-top:0px;  margin-right: 32px;}
.btm2_l > p {  margin:0 0 3px;}
.conten{text-align:center; display:inline-block; width:100%;}
.conten p{margin:0}
.conten strong{font-size:8px; font-weight:bold;}
.conten span{font-size:9.33333px; font-weight:bold;}
.conten .range{font-size:18px; font-weight:bold; margin-top:25px;}
.barcode > img {  height: 15px;  width: auto;}
.barcode{text-align:center; margin-top:3px;}

span.certifi{ font-size:10.6667px;}
.full{ display:block;}
.header_img.border-none {
  border: 5px solid #ffffff;
}
</style>

<div class="idcard">
	<div class="wapper">
		<div class="header">
            <div class="header_l">
                <span style="display: inline-block; height: 85px; min-height: 85px;">
                <div id="client_picture" class="header_img" style="height:80px;"> 
                	<img src="https://www.rangecontroller.com/images/client_logo.jpg" style="width:auto; height: 80px;max-height:80px; max-width:100%;" />
                </div>
                </span>
                <div id="membership_number" class="left_id">
                	12565            
                </div>
            </div>
        <div class="header_r">
        	<div class="header_r_top">
            	<div class="header_r_titel" style="height:15px;">
					<span id="company_name">Your Company Name Here</span>
                </div>
                <span style="float: left; display: inline-block; height: 52px; width: 65px;">
                <div id="company_logo" class="header_r_img">
               		<img src="https://www.rangecontroller.com/images/company_logo.png" style="width:auto; height: 35px;" />
                </div>
                </span>
                <div class="header_r_text">
                    <p>
                    <strong><span class="full" style="height:12px;"><span id="company_slogan">Company Slogan</span></span>
					<span>
                    <span class="full" style="height:12px;"><span id="company_address">Street Address</span></span>
                    <span class="full" style="height:12px;"><span id="company_address1">City, State Zipcode</span></span>
                    <span class="full" style="height:13px;"><span id="company_phone_number">Phone Number</span></span>
                    </span>
                    </strong></p>
                </div>
            </div>
            <div class="clr"></div>
            <span id="membership_type" style="display:inline-block; height:17px;">
            <div id="header_r_btm" class="header_r_btm">
             	Membership Type Here            
            </div>
            </span>
            <div class="header_r_btm2">
            	<div class="btm2_l">
                	<span>
                    	<span class="full" style="height:14px;"><span id="client_name">John Doe</span></span>
                    	<span class="full" style="height:14px;"><span id="client_address">4321 Cypress Grove</span></span>
                    	<span class="full" style="height:14px;"><span id="client_address1">Chattanooga, TN 37123</span></span>
                        </span>
                     	<p class="full" style="height:14px; line-height:8px; margin:0;">
                        	<strong id="emergency_contact">Emergency: Name & Phone Number</strong>
                        </p>
                </div>
                <div id="client_dob" class="btm2_r">
				12/12/1966<span id="expiration_date"><br />Expiration<br />12/12/1966</span>
                </div>                
           </div>
        </div>
    </div>
    <div class="conten">
    	<span class="full" style="height:9px;"><strong id="special_instructions">Special Instructions: Customer parking in back of bldg.</strong></span>         
        <span class="full" style="height:12px; display:inline-block;"><span id="client_certification" class="certifi">Other Certifications: TN Handgun Permit, NRA IH,NRA OH</span></span>        
        <span class="full" style="height:12px;"><span id="rangecontroller_shooter_portal">Shooter portal online! www.rangecontroller.com</span></span>        
        <span class="full" style="height:13px;"><span id="login_pass">RANGE#: 10000 SHOOTER#: 4409 PASSWORD: FDS44D5#</span></span>    </div>
        <span style="height:15px;"><div id="barcode" class="barcode" style="margin-top:-1px;">
            <img src="/xapp/inventory/printbarImage/10007" />
        </div>
        </span>
	</div>
</div>

        </td>
      </tr>
    </table>
</div>



