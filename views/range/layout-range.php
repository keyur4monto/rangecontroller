<!DOCTYPE html>
<html>
        <head>
        <meta http-equiv="content-type" content="text/html" />
        <title><?php echo $pageTitle; ?></title>
        <link rel="stylesheet" type="text/css" media="all"
                href="<?php echo $view['assets']->getUrl('css/site_layout.css', 
                        'rangepath');?>"/>
        <link rel="stylesheet" type="text/css" 
              href="<?php echo $view['assets']->getUrl('css/grid-fluid.css', 
                      'rangepath');?>" />
        <link rel="stylesheet" type="text/css" 
              href="<?php echo $view['assets']->getUrl('css/silex.css', 
                      'rangepath');?>" />
        <link rel="stylesheet" type='text/css' 
              href='<?php echo $view['assets']->getUrl(
                      'tools/jquery-ui-1.11.2.custom/jquery-ui.min.css', 'rangepath');?>' />
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" 
                      rel="stylesheet">
			  <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
				<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <script src="<?php 
            echo $view['assets']->getUrl('tools/jquery-1.11.0.min.js', 'rangepath')?>">
        </script>
        <script src='<?php echo $view['assets']->getUrl('js/vendor/ractive.min.js');?>'></script>
        <script  src='<?php echo $view['assets']->getUrl('js/rclib.js');?>' ></script>
        <script  src='<?php echo $view['assets']->getUrl('js/vendor/jquery.mask.js');?>' ></script>
        <script src="<?php 
            echo $view['assets']->getUrl('tools/jquery.simplePagination.js', 'rangepath')?>">
        </script>
        <script src="<?php echo $view['assets']->getUrl('js/vendor/ejs/ejs_production.js')?>"></script>
        <script src="<?php echo $view['assets']->getUrl('js/vendor/ejs/view.js')?>"></script>
        <script src="<?php echo $view['assets']->getUrl('js/vendor/validator.min.js')?>"></script>
        <script src="<?php echo $view['assets']->getUrl('js/vendor/numeral.min.js')?>"
        ></script>
        <script src="<?php 
            echo $view['assets']->getUrl('tools/jquery-ui-1.11.2.custom/jquery-ui.min.js', 
                            'rangepath')?>"></script>
        <script src="<?php echo $view['assets']->getUrl('js/vendor/jquery.fileupload.js')?>"></script>
        <script src="/xapp/js/angular.js"></script>
        <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
        <script>
            // global namespace for ranger functions 
            var ranger = ranger || {};
            // global namespace for ranger members/customers functions
            ranger.members = ranger.members || {};
            rc.disableNavigateBackOnBackspace();
        </script>
        <script src="/js/Chart.min.js"></script>
					<script>
								$(function() {
									$(document).tooltip();
								});
							</script>
        </head>
        <body>
<div class="hideFromPrint marginCenter"><img class="bg"
                src="<?php echo $view['assets']->getUrl('img/background.jpg', 
                    'rangepath'); ?>" 
                     /> </div>
<table id="headerTable" class="hideFromPrint marginCenter">
          <tr>
    <td class="blackHeaderCell">
			<div class="floatLeft">
				
				<img src="<?php echo $view['assets']->getUrl('img/logoNew.png', 'rangepath') ?>" id="logo" style="width: 60px; height: 60px;">&nbsp;&nbsp;</div>
			<div class="floatLeft padTop"><span class="rcLogoText"><span style="color: #fbb91f;">Range</span> <span style="color: white;">Controller</span></span></div>
						</td>
  </tr>
        </table>
<table id="companyTable" class="hideFromPrint marginCenter">
          <tr>
   
    <td id="companyTableCell" class="textCenter borderTop"><span class="companyName"> <?php echo $companyName ?: 'No company name'; ?> </span></td>
    <td id="companyLogoTableCell" class="textCenter borderTop">
              
              <?php if($rangeLogoUrl) : ?>
              <img class="logoimg" src="<?php echo $rangeLogoUrl?>"/>
              <?php endif; ?></td>
  </tr>
          <?php if (isset($_SESSION['member_number'])) echo '<tr>
      <td colspan="3" class="textCenter"><span class="memberWelcome">Welcome <span class="gold">' . $memberName . '</span></span></td>
    </tr>'; ?>
        </table>
<table id="contentTable">
          <tbody>
    <tr>
              <td id="contentTableCell"><div class="container_12 customer" > <?php echo $content ?: 'No content'; ?>
                  <div class="clear">&nbsp;</div>
                </div></td>
            </tr>
  </tbody>
        </table>
<table id="footer1Table" class="hideFromPrint textCenter">
         
          <tbody>
    <tr>
              <td style="border-top: 0px;"><div class="floatLeft padLeft"> <a href="<?php echo $app['get_range_url'](''); ?>common/logout.php" class="text-nodeco">LOGOUT</a> </div>
        <div class="floatRight padRight"> <a href="/xapp/techSupport" class="text-nodeco">Tech Support</a> </div></td>
            </tr>
  </tbody>
        </table>
<table id="footerTable" class="hideFromPrint textCenter">
          <tr>
    <td id="footerCell" class="textColor noBorder textCenter"> Range Controller v2.3 by Rockin' Guns<br />
              &copy; 
              <script>document.write(new Date().getFullYear());</script></td>
  </tr>
        </table>
</body>
</html>
