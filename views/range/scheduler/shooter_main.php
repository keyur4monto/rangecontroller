
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right">
              <input type="button" value="Add" onclick="window.location='add_scheduler.html'">
            </div>
            <div class="margt"> </div>
            <div class="grid_12 text-center">
              <table style="width: 80%;">
                <tr>
                  <th colspan="6" style="background-color: #666;">Upcoming Scheduled Range Time</th>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>Time from</td>
                  <td>Time to</td>
                  <td>Lane Amount</td>
                  <td colspan="2">Action</td>
                </tr>
                <tr>
                  <td>12/21/2014</td>
                  <td>9:00 AM</td>
                  <td>10:00 AM</td>
                  <td>1</td>
                  <td><a href="edit_scheduler.html">Manage</a></td>
                  <td><a href="#">Delete</a></td>
                </tr>
                <tr>
                  <td>12/19/2014</td>
                  <td>9:00 AM</td>
                  <td>10:00 AM</td>
                  <td>1</td>
                  <td><a href="edit_scheduler.html">Manage</a></td>
                  <td><a href="#">Delete</a></td>
                </tr>
                <tr>
                  <td>12/17/2014</td>
                  <td>9:00 AM</td>
                  <td>10:00 AM</td>
                  <td>1</td>
                  <td><a href="edit_scheduler.html">Manage</a></td>
                  <td><a href="#">Delete</a></td>
                </tr>
              </table>
              <table style="width: 80%;">
                <tr>
                  <th colspan="6" style="background-color: #666;">Previous Scheduled Range Time</th>
                </tr>
                <tr>
                  <td colspan="6"><span style="color: red;">No records found</span></td>
                </tr>
              </table>
            </div>
          </div>
        