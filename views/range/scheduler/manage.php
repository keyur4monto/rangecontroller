<script>

var single = true;

$(function(){
        $('#scheduleDate').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     
		
    });
    
    
    
function submitForm () {

			var xmlHttp = null;
			
			var myurlstring = "/xapp/rcScheduler/add/postUpdate";

			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "POST", myurlstring, true );
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			
			var numOfLanes = "";
			
			if (!single) numOfLanes = "number_of_lanes=" + document.getElementById("numOfLanes").value;
			
			xmlHttp.onload = function (e) {
			
				//alert(xmlHttp.responseText);
				$( "#dialog" ).dialog( "open" );
				
				$("#shadow").show();
			
			}
			
			xmlHttp.send("booking_number=" + "<?php echo($booking_number); ?>" + "&scheduleDate=" + document.getElementById("scheduleDate").value + "&startTime=" + document.getElementById("startTime").value + "&endTime=" + document.getElementById("endTime").value);
			
			//else document.getElementById("popupContent").innerHTML = "Please select an employee to continue!";
			
			//document.getElementById("popupContent").innerHTML = "Your note has been saved!";
			
			//if (document.getElementById("employee_id").value != '') xmlHttp.send("bug_content=" + document.getElementById("bug_content").value + "&employee_id=" + document.getElementById("employee_id").value + "&bug_date=" + tday);
			
			//else document.getElementById("popupContent").innerHTML = "Please select an employee to continue!";
			
			
			//$( "#dialog" ).dialog( "open" );
			
			//$("#shadow").show();
				

}

function deleteBooking () {

	$( "#dialog2" ).dialog( "open" );

}
    

</script>


<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Your changes have been saved!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

<!-- ui-dialog -->
<div id="dialog2" style="z-index: 101;" title="Delete Confirmation">
	<p id="popupContent">Are you sure you want to delete this booking?</p>
</div>

          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right">
              <input type="button" value="Delete" onClick="deleteBooking()">
            </div>
            <div class="grid_12 margt">
              <table style="width: 40%;">
                <tr>
                  <td class="text-right">Shooter 1:</td>
                  <td><input readonly type="text" value="<?php echo($shooter_name); ?>"></td>
                </tr>
                <tr>
                  <td class="text-right">Date:</td>
                  <td><input id="scheduleDate" type="text" value="<?php echo($date); ?>" style="width: 75px;"></td>
                </tr>
                <tr>
                  <td class="text-right">Time from:</td>
                  <td><select id="startTime" style="width: 90px;border-radius:5px;">
                <?php 
                
                	foreach ($hrs as $timeslot) { 
                	
                		if ((string)$timeslot == (string)$time_from) $append = "selected";
                		
                		else $append = "";
                	
                		echo '<option ' . $append . ' value="' . $timeslot . '">' . $timeslot . '</option>';
                		
                	}
                	
                ?>
                </select>
                </td>
                </tr>
                <tr>
                  <td class="text-right">Time to:</td>
                  <td><select id="endTime" style="width: 90px;border-radius:5px;">
                <?php 
                
                	foreach ($hrs as $timeslot) { 
                	
                		if ((string)$timeslot == (string)$time_to) $append = "selected";
                		
                		else $append = "";
                	
                		echo '<option ' . $append . ' value="' . $timeslot . '">' . $timeslot . '</option>';
                		
                	}
                	
                ?>
                </select></td>
                </tr>
              </table>
            </div>
            <div class="grid_12">
            * all fields required
              <div class="text-center">
                <input type="button" value="Save" onClick="submitForm()">
              </div><br><br>
              <div><span style="color: #fbb91f;">Added by <?php echo($added_by); ?> on <?php echo($timestamp); ?></span></div>
            </div>
          </div>
          
          
          
		<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				window.location = "/xapp/rcScheduler";
				
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

$( "#dialog2" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				window.location="/xapp/rcScheduler/delete/<?php echo($booking_number); ?>";
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link2" ).click(function( event ) {
	$( "#dialog2" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

</script>


          
          
          