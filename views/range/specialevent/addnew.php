<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">-->
<script>
// global namespace for ranger functions 
var ranger = ranger || {};
// global namespace for ranger members/customers functions
ranger.members = ranger.members || {};
rc.disableNavigateBackOnBackspace();
tinymce.init({
    selector: "textarea",
	theme: "modern",
                plugins: [
                    "advlist autolink lists link charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons paste textcolor colorpicker textpattern"
                ],
                height: 250,
                menubar:false,
                toolbar1: "undo redo | styleselect fontsizeselect | link print preview | fullscreen ",
                toolbar2: "bold italic underline | forecolor backcolor emoticons | bullist numlist | alignleft aligncenter alignright alignjustify | outdent indent",
});
var urlPrefix = '<?php echo $app['get_range_url']('specialEvents/')?>';
</script>

<style>
#ui-id-2{
	width: 150px !important;
    list-style: none !important;
    padding-left: 0 !important;
}
#ui-id-2.ui-widget-content {
    background: none repeat scroll 0 0 white !important;
    border: 1px solid #666 !important;
    color: black !important;
}
#ui-id-2 .ui-state-focus{
	color:red !important;
	border:none !important;
	background:none !important;
	font-weight:normal !important;
}
</style>

<form name="specialevent" action="" method="post" id="specialevent" onsubmit="return myValidation()">
<div class="grid_3 pull-right" id="breadcrumbsRight">
	<select name="employeename" id="employeename" style="float: right;">
	 <option value="">Choose an Employee:</option>
	  <?php while($row = $getemplists->fetch_assoc()) { ?>
			<option value="<?php echo $row['id']; ?>"><?php echo $row['fname'].' '.$row['lname']; ?></option>
	  <?php } ?>
	</select>
    <span class="requiredf" style="float:right;">* </span>
</div>
<div class="clear"></div>
<div class="grid_12">
	
		<div class="text-right"> </div>
		<div class="grid_12 margt"><br>	
         <?php if ($errorMessage): ?>
				<div class="grid_12 margb">
					<?php //echo $view->render('range/error-message.php', ['message'=>$errorMessage]);?>
                    <div class="error padlr padtb">
						<?php echo $errorMessage; ?>
                	</div>
				</div>
                <div class="clear">&nbsp;</div>
			<?php endif; ?>		
			<table style="width: 80%;">
				<tr>
					<td class="text-right"><span class="requiredf">*</span>Date</td>
					<td><input type="text" placeholder="datepicker" style="width: 75px;" name="date" id="date"></td>
				</tr>
				 <tr>
                  <td class="text-right"><span class="requiredf">*</span>Start Time:</td>
                  <td><input type="text" placeholder="timepicker" style="width: 75px;" name="start_time" id="start_time"></td>
                </tr>
                <tr>
                  <td class="text-right"><span class="requiredf">*</span>End Time:</td>
                  <td><input type="text" placeholder="timepicker" style="width: 75px;" name="end_time" id="end_time"></td>
                </tr>
                <tr>
                  <td class="text-right"><span class="requiredf">*</span>Event Name:</td>
                  <td><input type="text" style="width: 300px;" name="event_name"></td>
                </tr>
                <tr>
                  <td colspan="2" class="text-center"><textarea style="width: 550px; height: 275px;" name="message_body"></textarea></td>
                </tr>
			</table>
		</div>
		<div class="grid_12"><span class="requiredf">*</span> all fields required</div>
		
		<div class="text-center">
            <input type="hidden" name="cmd" value="add" />
			<input type="submit" value="Save" name="submit" onclick="checkForValidations()">
		</div>
</div>
</form>
<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('xapp/css/jquery.timepicker.css');?>">
<!--<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>-->
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery-ui-timepicker-addon.js');?>"></script>
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery.timepicker.js');?>"></script>
<script>
$(document).ready(function(){
	var urlPrefix = '<?php echo $app['get_range_url']('specialEvents/')?>';	
	$('#date').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
    });
	$('#start_time, #end_time').timepicker();
});
</script>
<style>

.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0;display:none; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }

</style>

<!-- ui-dialog -->
<div id="dialog" title="Edit Confirmation">
	<p id="dialtext">Your changes have been saved!</p>
</div>
<script>
$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				if (document.getElementById("employeename").value != "") valid = true;
				var extraElement = document.getElementById("employeename");
				//var eCopy = extraElement.cloneNode(true);
				//extraElement.style.display = "none";
				if (document.getElementById("employeename").value != "") document.forms["specialevent"].appendChild(extraElement);
				if (document.getElementById("employeename").value != "") document.forms["specialevent"].submit();
				$( this ).dialog( "close" );
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});

function dialogBox () {
	$( "#dialog" ).dialog( "open" );
	}
// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);
</script>
<script>
function checkForValidations () {
			valid = false;
			if (document.getElementById("employeename").value != "") {
				valid = true;
				var val = document.getElementById("employeename").value;
				var extraElement = document.getElementById("employeename");
				var eCopy = extraElement.cloneNode(true);
				eCopy.style.display = "none";
				eCopy.value = val;
				document.forms["specialevent"].appendChild(eCopy);
				document.forms["specialevent"].submit();
				
				}
				
			else {
			
				valid = false;
	
				var errmsg = "Please select an employee to complete this registration!";
		
				document.getElementById("dialtext").innerHTML = errmsg;
		
				dialogBox();
				}
				
}
var valid = true;
function myValidation () {

	return valid;

}
</script>