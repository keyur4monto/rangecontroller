<style>
.customer .padtb {
    padding-bottom: 4em;
    padding-top: 1em;
}
</style>
<div class="grid_12">
    <div class="text-right">
      <input type="button" value="Add" onClick="window.location='specialEvents/addNew'">
    </div>
    <div class="text-center padtb padlr margt lightblackbg">
		<form name="filter_newsletter" action="" method="get">
		  <div class="grid_3">Filter by Date<br>
			<input type="text" name="filter_date" id="filter_date" value="<?php echo $dt; ?>" placeholder="datepicker" style="width: 75px;">
		  </div>
		  <div class="grid_3">Filter by Month & Year<br>
			<select name="filter_month" id="filter_month">
			  <option value="">Month</option>
			  <?php for($i=1;$i<=12;$i++){ ?>
				<option value="<?php echo $i; ?>" <?php echo ($fm==$i) ? "selected=''" : ''; ?>><?php echo $i; ?></option>
			  <?php } ?>
			</select>
			<select name="filter_year" id="filter_year">
			  <option value="">Year</option>
			  <?php for($i=2010;$i<=2020;$i++){ ?>
				<option value="<?php echo $i; ?>" <?php echo ($fy==$i) ? "selected=''" : ''; ?>><?php echo $i; ?></option>
			  <?php } ?>
			</select>
		  </div>
		  <div class="grid_3">Filter by Employee<br>
		   <select name="filter_employee" id="filter_employee">
				  <option value="">Choose an Employee:</option>
				  <?php while($row = $getemplists->fetch_assoc()) { ?>
						<option value="<?php echo $row['id']; ?>" <?php echo ($emp==$row['id']) ? "selected=''" : ''; ?>><?php echo $row['fname'].' '.$row['lname']; ?></option>
				  <?php } ?>
			</select>
		  </div>
		  <div class="grid_3">
			<!--input type="submit" value="Filter"-->
			<input type="button" style="padding: 15px;" value="Show All" onClick="javascript:window.location.href='/xapp/specialEvents';">
		  </div>
	  </form>
    </div>
	<div class="grid_12 margt">
		<table>
			<thead>
			    <tr>
			      <th style="width: 15%;">Employee</th>
			      <th>Date</th>
                  <th>Time</th>
			      <th>Event</th>
			      <th colspan="2" style="width: 15%;">Actions</th>
			    </tr>
			</thead>
			<tbody class="inner">
            	<?php
				if($allnewsletter->num_rows == 0)
				{
				?>
                <tr>
                <td colspan="5" style="color:#F00"><center>No Records Found</center></td>
                </tr>
                <?php
				}
				else
				{
				?>
			    <?php 
				while($row=$allnewsletter->fetch_assoc()){?> 
					<tr>
				      <td><?php echo $row['emp_name'];?></td>
				      <td><?php echo $row['date'];?></td>
                      <td>(<?php echo $row['event_starttime'];?> <?php echo $row['event_endtime'];?>)</td>
				      <td><?php echo $row['event_name']?></td>
				      <td>
				      	<a href="/xapp/specialEvents/edit/<?php echo $row['event_id']?>">Manage</a></td>
				      <td>
				      	<a href="/xapp/specialEvents/delete/<?php echo $row['event_id'];?>" onclick="return myConfirm('Are you sure want to delete this special event?','<?php echo $row['event_id']; ?>');">Delete</a> 
					  </td>          
				    </tr>
				<?php }
				}
				?>
			</tbody>
		</table>
		<div class="text-center" id="pagerBottom"></div>
	</div>
</div> 
<!-- ui-dialog -->
<div id="dialog" title="Delete Confirmation">
	<p id="warningtext" >Your changes have been saved!</p>
</div>
<script>
var classid = -1;
$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Yes",
			click: function() {
				$( this ).dialog( "close" );
				//window.setTimeout(function () {
					window.location = "/xapp/specialEvents/delete/" + classid;		
					//},500);
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});



// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
	);

function myConfirm (str, class_id) {

	classid = class_id;

	var temp = $( "#dialog" ).dialog( "open" );
	
	document.getElementById("warningtext").innerHTML = str;
	
	//alert(temp);
	
	return false;
	
}
</script>

<script>
$(document).ready(function(){
	
	
	var urlPrefix = '<?php echo $app['get_range_url']('specialEvents/')?>';	
	$( "#filter_date" ).datepicker();
	$("#filter_month,#filter_year,#filter_employee,#filter_date").change(function() {
	    //get the selected value
	    var filter_date = $('#filter_date').val();
		var filter_month = $('#filter_month').val();
	    var filter_year = $('#filter_year').val();
	    var filter_employee = $('#filter_employee').val();		
	    //make the ajax call
	    $.ajax({
		      type: "Post",
		      url: urlPrefix + 'getfilters',
		      dataType: "json",
			  data: {filter_month:filter_month, filter_year:filter_year, filter_employee:filter_employee,filter_date:filter_date},
		      success: function(result){	          
				 option = '';
				 if(result == '')
				 {
					option += "<tr>";
				    option += "  <td colspan='5' style='color:#F00'><center>No Records Found</center></td>";  
				    option += "</tr>";
				 }
				 else
				 {
				 	$.each(result, function(i, v) {
			        option += "<tr>";
				    option += "  <td>"+v.emp_name+"</td>";
				    option += '  <td style="width: 25%;">'+v.date+'</td>';
				    option += "  <td>("+v.event_starttime+"&nbsp;&nbsp"+v.event_endtime+")</td>";
					option += "  <td>"+v.event_name+"</td>";
				    option += "  <td>";
				    option += "  	<a href='/xapp/specialEvents/edit/"+v.event_id+"'>Manage</a></td>";
					option += "  <td>";
				    option += "  	<a href=\"/xapp/specialEvents/delete/"+v.event_id+"\" onclick=\"return myConfirm('Are you sure want to delete this special event?','"+v.event_id+"')\">Delete</a>";
					option += "  </td>";     
				    option += "</tr>";					
					//alert(v.emp_id);
					// For each record in the returned array
			        //alert(v.emp_name);					 
			        });
				 }
			    $( ".inner" ).html(option);
		      }
		});
	});

});

</script>