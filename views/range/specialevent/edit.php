<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">-->
<script>
// global namespace for ranger functions 
var ranger = ranger || {};
// global namespace for ranger members/customers functions
ranger.members = ranger.members || {};
rc.disableNavigateBackOnBackspace();
tinymce.init({
    selector: "textarea",
	 theme: "modern",
                plugins: [
                    "advlist autolink lists link charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons paste textcolor colorpicker textpattern"
                ],
                height: 250,
                menubar:false,
                toolbar1: "undo redo | styleselect fontsizeselect | link print preview | fullscreen ",
                toolbar2: "bold italic underline | forecolor backcolor emoticons | bullist numlist | alignleft aligncenter alignright alignjustify | outdent indent",
});
var urlPrefix = '<?php echo $app['get_range_url']('specialevent/')?>';
</script>
<?php $data = $member->fetch_assoc();
/*echo '<pre>';
print_r($data);
echo '</pre>';*/
?>
<div class="grid_12 pull-right" id="breadcrumbsRight"> 
<div class="text-right">
	<form name="specialeventdel" action="" method="post" id="specialeventdel" onsubmit="return myConfirm('Are you sure want to delete this special events?','<?php echo $data['event_id']; ?>');">
        <input type="submit" value="Delete" name="submit" >
    </form>
</div>
</div>
<form name="specialevent" action="" method="post" id="specialevent">
<div class="clear"></div>
<div class="grid_12">
		
		<div class="text-right"> </div>
		<div class="grid_12 margt"><br>
						 <?php if ($errorMessage): ?>
				<div class="grid_12 margb">
					<?php //echo $view->render('range/error-message.php', ['message'=>$errorMessage]);?>
                    <div class="error padlr padtb">
						<?php echo $errorMessage; ?>
                	</div>
				</div>
                <div class="clear">&nbsp;</div>
			<?php endif; ?>	
			<table style="width: 80%;">
                <tr>
                  <td class="text-right"><span class="requiredf">*</span> Date:</td>
                  <td><input type="text" name="date" value="<?php echo $data['date'];?>" style="width: 75px;" id="date"></td>
                </tr>
                <tr>
                  <td class="text-right"><span class="requiredf">*</span> Start Time:</td>
                  <td><input type="text" name="start_time" value="<?php echo $data['event_starttime'];?>" style="width: 75px;" id="start_time"></td>
                </tr>
                <tr>
                  <td class="text-right"><span class="requiredf">*</span> End Time:</td>
                  <td><input type="text" name="end_time" value="<?php echo $data['event_endtime'];?>" style="width: 75px;" id="end_time"></td>
                </tr>
                <tr>
                  <td class="text-right"><span class="requiredf">*</span> Event Name:</td>
                  <td><input type="text" name="event_name" value="<?php echo $data['event_name'];?>" style="width: 300px;"></td>
                </tr>
                <tr>
                  <td colspan="2" class="text-center">
                    <textarea style="width: 550px; height: 275px;" name="message_body"><?php echo $data['event_description'];?></textarea></td>
                </tr>
			</table>
		</div>
		<div class="grid_12"><span class="requiredf">*</span> all fields required</div>
		
		<div class="text-center">
			<input type="hidden" name="cmd" value="edit" />
			<input type="submit" value="Save" name="submit">
			<input type="hidden" value="<?php echo $data['event_id'];?>" name="event_id">
		</div>
</div>
</form>
<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('xapp/css/jquery.timepicker.css');?>">
<!--<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>-->
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery-ui-timepicker-addon.js');?>"></script>
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery.timepicker.js');?>"></script>
<script>
$(document).ready(function(){
	var urlPrefix = '<?php echo $app['get_range_url']('specialEvents/')?>';	
	$('#date').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
    });
	
	$('#start_time, #end_time').timepicker();
});
</script>
<style>

.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0;display:none; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }

</style>
<!-- ui-dialog -->
<div id="dialog" title="Delete Confirmation">
	<p id="warningtext" >Your changes have been saved!</p>
</div>
<script>
var classid = -1;
$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Yes",
			click: function() {
				$( this ).dialog( "close" );
				//window.setTimeout(function () {
					window.location = "/xapp/specialEvents/delete/" + classid;		
					//},500);
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});



// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
	);

function myConfirm (str, class_id) {

	classid = class_id;

	var temp = $( "#dialog" ).dialog( "open" );
	
	document.getElementById("warningtext").innerHTML = str;
	
	//alert(temp);
	
	return false;
	
}

</script>