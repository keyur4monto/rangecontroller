<script src="../js/ranger.customer.js"></script>
<style>
	</style>
<script>
$(function() {
    $("#pagerBottom").pagination({
        items: 10,
        itemsOnPage: 10,
        cssStyle: 'light-theme'
    });
});
</script>
<script src="/xapp/js/multifilter.min.js"></script>
<script>
$(document).ready(function() {
$('.myfilter').multifilter({
    'target' : $('#main-table')
  })
//alert("Test!");
});
function filterInstructors() {
window.location = "/xapp/classes/byInstructors/" + document.getElementById("instructor_select").value;
}
function filterByDate() {
if (document.getElementById("month").value != 0 && document.getElementById("year").value != 0) window.location = "/xapp/classes/byDate/" + document.getElementById("month").value + "/" + document.getElementById("year").value;
}
</script>
<div class="grid_12">
  <div class="text-right">
    <input type="button" value="Instructors" onClick="window.location= '/xapp/classes/instructors'">
    <input type="button" value="Add Class" onClick="window.location='/xapp/classes/add'">
  </div>
  <div class="text-center padtb padlr margt lightblackbg" style="height: 3em;">
    <div class="grid_3">Filter by Month and Year<br>
      <select id="month" onchange="filterByDate()">
        <option value="0"></option>
		<option value="1">January</option>
		<option value="2">February</option>
		<option value="3">March</option>
		<option value="4">April</option>
		<option value="5">May</option>
		<option value="6">June</option>
		<option value="7">July</option>
		<option value="8">August</option>
		<option value="9">September</option>
		<option value="10">October</option>
		<option value="11">November</option>
		<option value="12">December</option>
      </select>
      <select id="year" onchange="filterByDate()">
        <option value="0"></option>
		<option value="2013">2013</option>
		<option value="2014">2014</option>
		<option value="2015">2015</option>
      </select>
      <br>
    </div>
    <div class="grid_3">Filter by Class Name<br>
      <input type="text" class="myfilter" data-col="title"/>
    </div>
    <div class="grid_3">Filter by Instructor<br>
      <select id="instructor_select" onchange="filterInstructors()">
        <option value=""></option>
		<?php echo ($instructorOptions) ; ?>
      </select>
    </div>
    <div class="grid_3">
    <input type="button" value="SHOW ALL" style="padding: 15px;" onClick="window.location = '/xapp/classes'">
    </div>
  </div>
  <div class="grid_12 margt">
  <br>
      <table class="margt" style="margin:0;">
        <tr>
          <th colspan="6" class="text-center lightblackbg">Upcoming Classes (Ascending Order)</th>
        </tr>
		</table>
    <table id="main-table" class="margt" style="margin-top:0;">
        <tr>
		<thead>
          <th nowrap="">Date</th>
          <th>Title</th>
          <th>Instructor</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php echo $upcomingclasses; ?>
            <tr id="norecords" <?php echo (empty($upcomingclasses)) ? "" : "style='display:none'"; ?>>
            	<td colspan="8" style="color:#F00"><center>No Records Found</center></td>
            </tr>
        <tr>
          <th colspan="6" class="text-center lightblackbg">Past Classes (Descending Order)</th>
        </tr>
        <tr>
          <th nowrap="">Date</th>
          <th>Class Title</th>
          <th>Instructor</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
        <?php echo $pastclasses; ?>
            <tr id="norecords" <?php echo (empty($pastclasses)) ? "" : "style='display:none'"; ?>>
            	<td colspan="8" style="color:#F00"><center>No Records Found</center></td>
            </tr>
      </tbody>
    </table>
  </div>
  <div class="text-center light-theme simple-pagination" id="pagerBottom">
    <ul>
      <li class="active"><span class="current prev">Prev</span></li>
      <li class="active"><span class="current">1</span></li>
      <li><a href="" class="page-link">2</a></li>
      <li><a href="" class="page-link">3</a></li>
      <li><a href="" class="page-link">4</a></li>
      <li><a href="" class="page-link">5</a></li>
      <li class="disabled"><span class="ellipse">…</span></li>
      <li><a href="">140</a></li>
      <li><a href="">141</a></li>
      <li><a href="" class="page-link next">Next</a></li>
    </ul>
  </div>
</div>
<!-- ui-dialog -->
<div id="dialog" title="Delete Confirmation">
	<p id="warningtext" >Your changes have been saved!</p>
</div>
<script src="../js/jquery.js"></script>
<script src="../js/jquery-ui.js"></script>
<script>
var classid = -1;
$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Yes",
			click: function() {
				$( this ).dialog( "close" );
				//window.setTimeout(function () {
					window.location = "/xapp/classes/delete/" + classid;		
					//},500);
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});
// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});
// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
	);
function myConfirm (str, class_id) {
	classid = class_id;
	var temp = $( "#dialog" ).dialog( "open" );
	document.getElementById("warningtext").innerHTML = str;
	//alert(temp);
	return false;
}
</script>