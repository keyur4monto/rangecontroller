<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('xapp/css/jquery.timepicker.css');?>">
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery-ui-timepicker-addon.js');?>"></script>
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery.timepicker.js');?>"></script>
<script src="https://www.rangecontroller.com/js/ranger.customer.js"></script>

<!--<script src="/xapp/js/jquery-ui-timepicker-addon.js"></script>
-->
<style>

.demoHeaders {
		margin-top: 2em;
	}
	#dialog-link {
		padding: .4em 1em .4em 20px;
		text-decoration: none;
		position: relative;
	}
	#dialog-link span.ui-icon {
		margin: 0 5px 0 0;
		position: absolute;
		left: .2em;
		top: 50%;
		margin-top: -8px;
	}
	#icons {
		margin: 0;
		padding: 0;
	}
	#icons li {
		margin: 2px;
		position: relative;
		padding: 4px 0;
		cursor: pointer;
		float: left;
		list-style: none;
	}
	#icons span.ui-icon {
		float: left;
		margin: 0 4px;
	}
	.fakewindowcontain .ui-widget-overlay {
		position: absolute;
	}
	select {
		width: 200px;
	}
	
	
	</style>
	
	

<script>

$(function(){
        $('#startd, #endd').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     

		
	
$('#startt, #endt').timepicker();
		
    });
function submitForm () {

if (document.getElementById("instructor_id").value == "" || document.getElementById("classtitle").value == "" || document.getElementById("nos").value == "" || document.getElementById("cost").value == "" || document.getElementById("startd").value == "" || document.getElementById("endd").value == "" || document.getElementById("startt").value == "" || document.getElementById("endt").value == "") document.getElementById("errid").style.display = "block";

else {

var myurlstring = "/xapp/classes/update/<?echo($class_id);?>"

var xmlHttp = null;

	xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "POST", myurlstring, true );
    
	xmlHttp.onload = function (e) {
	
		//alert(xmlHttp.responseText);
	
		window.location = "/xapp/classes";
		
	}

	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp.send("title=" + document.getElementById("classtitle").value + "&nos=" + document.getElementById("nos").value + "&cost=" + document.getElementById("cost").value + "&startt=" + document.getElementById("startt").value + "&endt=" +  document.getElementById("endt").value + "&startd=" + document.getElementById("startd").value + "&endd=" + document.getElementById("endd").value + "&description=" + document.getElementById("description").value + "&instructor_id=" +  document.getElementById("instructor_id").value);
    

}


}

function hideDivandReset() {
	
$("#popupdiv").hide();
	


}

function hideDiv () {

$("#popupdiv").hide();

}

function deleteClass () {

if (confirm("Are you sure you want to delete '<?php echo ($classtitle) ; ?>'?")) window.location= '/xapp/classes/delete/<?php echo ($class_id) ; ?>'


}

</script>

	 <br><div class="grid_12 margb" style="display:none;" id="errid">
        <?php echo($errmsg);?>
        </div><div class="clear">&nbsp;</div>


<div class="grid_12 ">
  <div class="text-right">
    <button onClick="deleteClass()">Delete </button>
  </div>
  <div class="grid_2">&nbsp;</div>
  <div class="grid_8">
    <div class="margt"> </div>
    <table width="70%" align="center">
      <tr>
      <td align="right">Class Title:&nbsp;</td>
      <td><input id="classtitle" type="text" style="width:300px;" value="<?php echo ($classtitle) ?>"></td>
    </tr>
    <tr>
      <td align="right">Number of Students:&nbsp; </td>
      <td><input id="nos" type="text" style="width:50px;" value="<?php echo ($nos) ?>"></td>
    </tr>
    <tr>
      <td align="right">Cost:&nbsp; </td>
      <td><input id="cost" type="text" style="width:50px;" value="<?php echo ($cost) ?>"></td>
    </tr>
    <tr>
      <td align="right">Start Date:&nbsp;</td>
      <td><input id="startd" type="text" style="width:75px;" value="<?php echo ($startd) ?>"></td>
    </tr>
    <tr>
      <td align="right">End Date:&nbsp; </td>
      <td><input id="endd" type="text" style="width:75px;" value="<?php echo ($endd) ?>"></td>
    </tr>
    <tr>
      <td align="right">Start Time:&nbsp;</td>
      <td><input id="startt" type="text" style="width:75px;" value="<?php echo ($startt) ?>"></td>
    </tr>
    <tr>
      <td align="right">End Time:&nbsp;</td>
      <td><input id="endt" type="text" style="width:75px;" value="<?php echo ($endt) ?>"></td>
    </tr>
    <tr>
      <td align="right">Instructor:&nbsp; 
      <td><select id="instructor_id"><option value="">Please select : </option>
          <?php echo($instructorOptions); ?>
        </select></td>
    </tr>
    <tr>
      <td align="right">Class Description:&nbsp;</td>
      <td><textarea id="description" cols="40" rows="6"><?php echo ($description) ?></textarea></td>
    </tr>
    </table>
  </div>
  <div class="grid_2">&nbsp;</div>
  <div class="clear">&nbsp;</div>
  <div class="text-center">
    <input type="submit"  value="Save" id="dialog-link1" />
  </div>
</div>

<div id="popupdiv" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable" tabindex="-1" role="dialog" aria-describedby="elm09454444972798228" aria-labelledby="ui-id-1" style="position: absolute; height: auto; width: 300px; top: 232.5px; left: 528.5px; display: block; z-index: 101;"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle"><span id="ui-id-1" class="ui-dialog-title">&nbsp;</span><button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" title="Close"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">Close</span></button></div><div id="elm09454444972798228" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 1px; max-height: none; height: auto;"><span style="color:#0f0"><i class="fa fa-check fa-3x"></i></span> &nbsp;Successfully updated</div><div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><div class="ui-dialog-buttonset"><button type="button" onclick="hideDivandReset()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">Ok</span></button> <button type="button" onclick="hideDivandReset()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">Cancel</span></button></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-sw" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-ne" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-nw" style="z-index: 90;"></div></div>

<script>$("#popupdiv").hide();</script>


<style>

.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0;display:none; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }

</style>

<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p>Your changes have been saved!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

<script src="external/jquery/jquery.js"></script>
<script src="jquery-ui.js"></script>
<script>






$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				submitForm();
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});



function myConfirm (str) {

	var temp = $( "#dialog" ).dialog( "open" );
	
	alert(temp);
	
	return false;
	
}


// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>

