


<style>

@media print {
.hideFromPrint, .no-print, .no-print * {
	display: none !important;
}
}

.customer_results {

	width: 200px;
	max-height: 300px;
	position: absolute;
	top:55px;
	left:213px;
	background-color: white;
	color: black;
	text-align: left;
	overflow: auto; 
	vertical-align: top;
	border-color: black;
	border-width: thick;
}

</style>

<script>

$("#namesearch").click(function(){
  x=$("#namesearch").offset();
  $("#div_customer_list").offset(x);
  //alert("Top: " + x.top + " Left: " + x.left);
});

function setStudent (customer_id) {

	customer_id = customer_id.split(" ")[0];
	
	//alert(customer_id);

	var myurlstring = "/xapp/classes/<?php echo($class_no); ?>/addCustomer/" + customer_id;

	var xmlHttp = null;

    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", myurlstring, false );
    xmlHttp.send( null );
    //alert(xmlHttp.responseText);
	
	window.location = "/xapp/classes/registrationList/<?php echo($class_no); ?>";
	
}

function deleteStudent (customer_id) {

	//if (myConfirm("Are you sure you want to delete this student?")) {

	customer_id = customer_id.split(" ")[0];
	
	//alert(customer_id);

	var myurlstring = "/xapp/classes/<?php echo($class_no); ?>/deleteStudent/" + customer_id;

	var xmlHttp = null;

    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", myurlstring, false );
    xmlHttp.send( null );
    //alert(xmlHttp.responseText);
	
	window.location = "/xapp/classes/registrationList/<?php echo($class_no); ?>";
	
	//}
	

}

function populateCustomerList(str){/*
if(document.getElementById("lblSelectedMemberHeader").innerHTML == "Select a member to begin..."){
document.getElementById("txtFirearm").value="";
alert("You must select a member to begin");
document.getElementById("txtMember").focus();
return;}*/
//if (str==""){document.getElementById("div_firearm_list").innerHTML="";return;}

//alert("Runs!");

if (window.XMLHttpRequest){
// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else{
// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
document.getElementById("div_customer_list").innerHTML = xmlhttp.responseText;

if (str == "") $("#div_customer_list").hide(); 

else $("#div_customer_list").show();
//alert("Test!" + xmlhttp.responseText + str);
}
}//end onreadystatechange
xmlhttp.open("GET","../../ajax/getMembersOptions/"+str,true);
xmlhttp.send();
}//END AJAX TO FILL POS ITEMS LIST

</script>

<div class="grid_12 ">
  <div class="text-right">
    <!-- Print Friendly -->
    <button onClick="window.print()">Print </button>
  </div>
</div>
<div class="grid_12 text-center margt margb"><span style="font-size: 24px;"><strong><?php echo($parsed_date) ?></strong> - <span style="text-decoration: underline;"><?php echo($class_title) ?></span> - <span style="font-style: italic;"></span> (<?php echo($start_time) ?>)</span><br>
  <br>
  <span style="color: red;"><?php echo($available); ?> of <?php echo ($totalseats); ?> seats of available</span> </div>
<table class="margt">
  <thead>
    <tr>
      <th nowrap="">Name</th>
      <th style="width:80px;" nowrap="">Email</th>
      <th>Phone</th>
      <th>Paid</th>
      <th>DL #</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php echo ($reg_list) ; ?> 
  </tbody>
</table>
</div>
<div class="clear">&nbsp;</div>
<div class="clear">&nbsp;</div>
<div class="container_12 classes">
  <div class="grid_12 lightblackbg" style="padding: 15px;position:relative;left:-15px;">
    <div class="grid_2">&nbsp;</div>
    <div class="grid_4 text-center">Add Existing Member or Student<br>
      <input type="text" autocomplete="off" id="namesearch" onKeyUp="populateCustomerList(this.value)" placeholder="Enter First or Last Name here.." style="width: 200px;">
    </div>
	 <div id="div_customer_list" class="customer_results"></div>
    <div class="grid_4 text-center">
      <input type="button" value="Add New Student" style="padding: 20px;" onClick="window.location = '../<?php echo($class_no); ?>/addStudent'">
    </div>
    <div class="grid_2">&nbsp;</div>
  </div>
</div>
</div>





<!-- ui-dialog -->
<div id="dialog" title="Delete Confirmation">
	<p id="warningtext" >Your changes have been saved!</p>
</div>



<script src="external/jquery/jquery.js"></script>
<script src="jquery-ui.js"></script>
<script>


var classid = -1;



$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Yes",
			click: function() {
				$( this ).dialog( "close" );
				//window.setTimeout(function () {
					deleteStudent(classid);		
					//},500);
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});



// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
	);

function myConfirm (str, class_id) {

	classid = class_id;

	var temp = $( "#dialog" ).dialog( "open" );
	
	document.getElementById("warningtext").innerHTML = str;
	
	//alert(temp);
	
	return false;
	
}

</script>



