<div class="grid_9">
    <div class="breadcrumbs">
    <?php
    $lastind = count($breadcrumbs)-1;
    foreach($breadcrumbs as $ind => $bc) {
        if (empty($bc['link'])) {
            echo "<span>" . $bc['title'] . " </span>";
        } else {
            echo "<a href='" . $bc['link'] . "'>" . $bc['title'] . " </a>";
        }
        if ($lastind > $ind) {
            echo " / ";
        }
    }
    ?>
    </div>
</div>
<div class="grid_3" id='breadcrumbsRight'></div><div class="clear">&nbsp;</div>