
<script>

		  $(document).ready(function () {
		  
			getEmployees();
		  
			$("#breadcrumbsRigh").show();
			
		});
		
</script>

<script>
    $(function(){
        $('#phone, #emergency').mask('(000)-000-0000');       
        $('#zipcode').mask('00000-0000');    
        $('#ssn').mask('000-00-0000');
		$('#amountpaid').mask('$00000');
    });
</script>

<script>

function submitForm () {

if (document.getElementById("fname").value == "" || document.getElementById("lname").value == "" || document.getElementById("email").value == "" || document.getElementById("address").value == "" || document.getElementById("city").value == "" || document.getElementById("zipcode").value == "" || document.getElementById("phone").value == "" || document.getElementById("emergency").value == "" || document.getElementById("employee_id").value == "") document.getElementById("errid").style.display = "block";

else {

var myurlstring = "/xapp/classes/<?php echo($class_id) ?>/insertStudent"

//alert("Instructor added!");

//alert(myurlstring);


var xmlHttp = null;

	xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "POST", myurlstring, true );
    
	xmlHttp.onload = function (e) {
	
		//alert(xmlHttp.responseText);
	
		window.location = "/xapp/classes/registrationList/<?php echo ($class_id) ?>";
		
	}
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp.send( "fname=" + document.getElementById("fname").value + "&lname=" + document.getElementById("lname").value + "&email=" + document.getElementById("email").value + "&address=" + document.getElementById("address").value + "&city=" + document.getElementById("city").value + "&state=" + document.getElementById("state").value +  "&zipcode=" + document.getElementById("zipcode").value +  "&phone=" + document.getElementById("phone").value +  "&emergency=" + document.getElementById("emergency").value + "&amountpaid=" + document.getElementById("amountpaid").value + "&license=" + document.getElementById("license").value + "&employee_id=" + document.getElementById("employee_id").value);
    
    //alert(xmlHttp.responseText);
	
//window.location = "/xapp/classes/registrationList/<?php echo ($class_id) ?>";

}


}

</script>

	 <br><div class="grid_12 margb" style="display:none;" id="errid">
        <?php echo($errmsg);?>
        </div><div class="clear">&nbsp;</div>


<div class="grid_2">&nbsp;</div>
<div class="grid_8">
  <div class="margt"> </div>
  <table width="70%" align="center">
    <tr>
      <td align="right">First Name:&nbsp;</td>
      <td><input type="text" id="fname" style="width:100px;"></td>
    </tr>
    <tr>
      <td align="right">Last Name:&nbsp; </td>
      <td><input type="text" id="lname" style="width:100px;"></td>
    </tr>
    <tr>
      <td align="right">Email Address:&nbsp; </td>
      <td><input type="text" id="email" style="width:200px;"></td>
    </tr>
    <tr>
      <td align="right">Street Address:&nbsp;</td>
      <td><input type="text" id="address" style="width:200px;"></td>
    </tr>
    <tr>
      <td align="right">City:&nbsp; </td>
      <td><input type="text" id="city" style="width:150px;"></td>
    </tr>
    <tr>
      <td align="right">State:&nbsp;</td>
      <td><select id="state">
          <option value="AL">AL</option>
    <option value="AK">AK</option>
    <option value="AZ">AZ</option>
    <option value="AR">AR</option>    
    <option value="CA">CA</option>    
    <option value="CO">CO</option>    
    <option value="CT">CT</option>    
    <option value="DE">DE</option>    
    <option value="DC">DC</option>    
    <option value="FL">FL</option>    
    <option value="GA">GE</option>    
    <option value="HI">HI</option>    
    <option value="ID">ID</option>    
    <option value="IL">IL</option>    
    <option value="IN">IN</option>    
    <option value="IA">IA</option>    
    <option value="KS">KS</option>    
    <option value="KY">KY</option>    
    <option value="LA">LA</option>    
    <option value="ME">ME</option>
    <option value="MD">MD</option>
    <option value="MA">MA</option>
    <option value="MI">MI</option>
    <option value="MN">MN</option>
    <option value="MS">MS</option>
    <option value="MO">MO</option>
    <option value="MT">MT</option>
    <option value="NE">NE</option>    
    <option value="NV">NV</option>    
    <option value="NH">NH</option>    
    <option value="NJ">NJ</option>    
    <option value="NM">NM</option>
    <option value="NY">NY</option>    
    <option value="NC">NC</option>
    <option value="ND">ND</option>
    <option value="OH">OH</option>    
    <option value="OK">OK</option>
    <option value="OR">OR</option>
    <option value="PA">PA</option>
    <option value="RI">RI</option>
    <option value="SC">SC</option>
    <option value="SD">SD</option>
    <option value="TN" selected="selected">TN</option>
    <option value="TX">TX</option>
    <option value="UT">UT</option>
    <option value="VT">VT</option>    
    <option value="VA">VA</option>
    <option value="WA">WA</option>    
    <option value="WV">WV</option>    
    <option value="WI">WI</option>
    <option value="WY">WY</option>
        </select></td>
    </tr>
    <tr>
      <td align="right">Zip Code:&nbsp;</td>
      <td><input type="text" id="zipcode" style="width:50px;"></td>
    </tr>
    <tr>
      <td align="right">Phone Number:&nbsp;</td>
      <td><input type="text" id="phone" style="width:90px;"></td>
    </tr>
    <tr>
      <td align="right">Emergency Phone Number:&nbsp;</td>
      <td><input type="text" id="emergency" style="width:90px;"></td>
    </tr>
    <tr>
      <td align="right">Amount Paid:&nbsp;</td>
      <td><input type="text" id="amountpaid" style="width:50px;"></td>
    </tr>
    <tr>
      <td align="right">Driver License:&nbsp;</td>
      <td><input type="text" id="license" style="width:90px;"></td>
    </tr>
  </table>
</div>
<div class="grid_2">&nbsp;</div>
<div class="clear">&nbsp;</div>
<div class="grid_12">* all fields required</div>
<div class="text-center">
  <input type="submit"  value="Add Student" onClick="submitForm()" />
</div>
</div>
<div class="clear">&nbsp;</div>
</div>
