<div class="grid_12">
  <div class="text-right">
    <input type="button" value="Add Instructor" onClick="window.location= 'instructors/add'">
  </div>
</div>
<div class="margt"></div>
<div class="grid_2">&nbsp;</div>
<div class="grid_8"><br>
  <table width="100%">
    <tr>
      <th>Name</th>
      <th>Email Address</th>
      <th></th>
      <th></th>
    </tr>
    <?php echo $instructors; ?>
            <tr id="norecords" <?php echo (empty($instructors)) ? "" : "style='display:none'"; ?>>
            	<td colspan="8" style="color:#F00"><center>No Records Found</center></td>
            </tr>
  </table>
</div>
<div class="grid_2">&nbsp;</div>
</div>
<div class="clear">&nbsp;</div>



<!-- ui-dialog -->
<div id="dialog" title="Delete Confirmation">
	<p id="warningtext" >Your changes have been saved!</p>
</div>



<script src="external/jquery/jquery.js"></script>
<script src="jquery-ui.js"></script>
<script>


var classid = -1;



$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Yes",
			click: function() {
				$( this ).dialog( "close" );
				//window.setTimeout(function () {
					window.location = "/xapp/classes/instructors/delete/" + classid;		
					//},500);
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});



// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
	);

function myConfirm (str, class_id) {

	classid = class_id;

	var temp = $( "#dialog" ).dialog( "open" );
	
	document.getElementById("warningtext").innerHTML = str;
	
	//alert(temp);
	
	return false;
	
}

</script>


