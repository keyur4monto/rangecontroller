

<script>
    $(function(){
        $('#phone, #emergency_phone').mask('(000)-000-0000');       
        $('#zipcode').mask('00000-0000');    
        $('#ssn').mask('000-00-0000');
		$('#amount_paid').mask('$00000');
    });
</script>


<script>

function submitForm () {

if (document.getElementById("fname").value == "" || document.getElementById("lname").value == "" || document.getElementById("email").value == "" || document.getElementById("address").value == "" || document.getElementById("city").value == "" || document.getElementById("state").value == "" || document.getElementById("zipcode").value == "" || document.getElementById("phone").value == "") document.getElementById("errid").style.display = "block";

else {

var myurlstring = "/xapp/classes/<?echo($class_id);?>/updateStudent/<?php echo($student_id) ?>";

	var xmlHttp = null;

	xmlHttp = new XMLHttpRequest();
	
    xmlHttp.open( "POST", myurlstring, true );
    
	xmlHttp.onload = function (e) {
	
		//alert(xmlHttp.responseText);
	
		window.location = "/xapp/classes/registrationList/<?php echo ($class_id) ?>";
		
	}
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp.send( "fname=" + document.getElementById("fname").value + "&lname=" + document.getElementById("lname").value + "&email=" + document.getElementById("email").value + "&address=" + document.getElementById("address").value + "&city=" + document.getElementById("city").value + "&state=" + document.getElementById("state").value +  "&zipcode=" + document.getElementById("zipcode").value +  "&phone=" + document.getElementById("phone").value +  "&emergency=" + document.getElementById("emergency_phone").value + "&amountpaid=" + document.getElementById("amount_paid").value + "&license=" + document.getElementById("license").value);
    
    //alert(xmlHttp.responseText);
	
	//$("#popupdiv").show();
	
//window.location = "/xapp/classes/registrationList/<?php echo($class_id) ?>";

}


}


function hideDivandReset() {
	
$("#popupdiv").hide();
	
window.location = "/xapp/classes/registrationList/<?php echo($class_id) ?>";

}

function hideDiv () {

$("#popupdiv").hide();

}

</script>

	 <br><div class="grid_12 margb" style="display:none;" id="errid">
        <?php echo($errmsg);?>
        </div><div class="clear">&nbsp;</div>




<div class="grid_2">&nbsp;</div>
<div class="grid_8">
  <div class="margt"> </div>
  <table width="70%" align="center">
    <tr>
      <td align="right">First Name:&nbsp;</td>
      <td><input type="text" id="fname" style="width:100px;" value="<?php echo ($fname) ?>"></td>
    </tr>
    <tr>
      <td align="right">Last Name:&nbsp; </td>
      <td><input type="text" id="lname" style="width:100px;" value="<?php echo ($lname) ?>"></td>
    </tr>
    <tr>
      <td align="right">Email Address:&nbsp; </td>
      <td><input type="text" id="email" style="width:200px;" value="<?php echo ($email) ?>"></td>
    </tr>
    <tr>
      <td align="right">Street Address:&nbsp;</td>
      <td><input type="text" id="address" style="width:200px;" value="<?php echo ($address) ?>"></td>
    </tr>
    <tr>
      <td align="right">City:&nbsp; </td>
      <td><input type="text" id="city" style="width:150px;" value="<?php echo ($city) ?>"></td>
    </tr>
    <tr>
      <td align="right">State:&nbsp;</td>
      <td><select id="state">
          <option selected>TN</option>
        </select></td>
    </tr>
    <tr>
      <td align="right">Zip Code:&nbsp;</td>
      <td><input type="text" id="zipcode" style="width:50px;" value="<?php echo ($zipcode) ?>"></td>
    </tr>
    <tr>
      <td align="right">Phone Number:&nbsp;</td>
      <td><input type="text" id="phone" style="width:90px;" value="<?php echo ($phone) ?>"></td>
    </tr>
    <tr>
      <td align="right">Emergency Phone Number:&nbsp;</td>
      <td><input type="text" id="emergency_phone" style="width:90px;" value="<?php echo ($emergency_phone) ?>"></td>
    </tr>
    <tr>
      <td align="right">Amount Paid:&nbsp;</td>
      <td><input type="text" id="amount_paid" style="width:50px;" value="<?php echo ($amount_paid) ?>"></td>
    </tr>
    <tr>
      <td align="right">Driver License:&nbsp;</td>
      <td><input type="text" id="license" style="width:90px;" value="<?php echo ($license) ?>"></td>
    </tr>
  </table>
</div>
<div class="grid_2">&nbsp;</div>
<div class="clear">&nbsp;</div>
<div class="grid_12">* all fields required</div>
<div class="text-center">
  <input type="submit"  value="Update Student" onClick="dialogBox()" />
</div>
</div>

<div id="popupdiv" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable" tabindex="-1" role="dialog" aria-describedby="elm09454444972798228" aria-labelledby="ui-id-1" style="position: absolute; height: auto; width: 300px; top: 232.5px; left: 528.5px; display: block; z-index: 101;"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle"><span id="ui-id-1" class="ui-dialog-title">&nbsp;</span><button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" title="Close"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">Close</span></button></div><div id="elm09454444972798228" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 1px; max-height: none; height: auto;"><span style="color:#0f0"><i class="fa fa-check fa-3x"></i></span> &nbsp;Successfully updated</div><div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><div class="ui-dialog-buttonset"><button type="button" onclick="hideDivandReset()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">Ok</span></button></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-sw" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-ne" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-nw" style="z-index: 90;"></div></div>

<script>$("#popupdiv").hide();</script>


<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p>Your changes have been saved!</p>
</div>



<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>
		


<script src="external/jquery/jquery.js"></script>
<script src="jquery-ui.js"></script>
<script>






$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				submitForm();
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

function dialogBox () {

	$( "#dialog" ).dialog( "open" );
	
	$("#shadow").show();
	
	}

// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>




