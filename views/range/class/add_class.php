<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('xapp/css/jquery.timepicker.css');?>">
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery-ui-timepicker-addon.js');?>"></script>
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery.timepicker.js');?>"></script>
<script>
    $(function(){
		$('#cost').mask('$00000');
    });
</script>

<!--<script src="/xapp/js/jquery-ui-timepicker-addon.js"></script>-->

<script>

$(document).ready(function(){
        $('#startd, #endd').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     

		
	
$('#startt, #endt').timepicker();
		
    });

function submitForm () {

if (document.getElementById("instructor_id").value == "" || document.getElementById("classtitle").value == "" || document.getElementById("nos").value == "" || document.getElementById("cost").value == "" || document.getElementById("startd").value == "" || document.getElementById("endd").value == "" || document.getElementById("startt").value == "" || document.getElementById("endt").value == "") document.getElementById("errid").style.display = "block";

else {

var myurlstring = "/xapp/classes/insert";

var xmlHttp = null;

	xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "POST", myurlstring, true );
    
	xmlHttp.onload = function (e) {
	
		//alert(xmlHttp.responseText);
	
		window.location = "/xapp/classes";
		
	}

	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp.send("title=" + document.getElementById("classtitle").value + "&nos=" + document.getElementById("nos").value + "&cost=" + document.getElementById("cost").value + "&startt=" + document.getElementById("startt").value + "&endt=" +  document.getElementById("endt").value + "&startd=" + document.getElementById("startd").value + "&endd=" + document.getElementById("endd").value + "&description=" + document.getElementById("description").value + "&instructor_id=" +  document.getElementById("instructor_id").value);
    
}


}

</script>

	 <br><div class="grid_12 margb" style="display:none;" id="errid">
        <?php echo($errmsg);?>
        </div><div class="clear">&nbsp;</div>

<div class="grid_2">&nbsp;</div>
<div class="grid_8">
  <div class="margt"> </div>
  <table width="70%" align="center">
    <tr>
      <td align="right">Class Title:&nbsp;</td>
      <td><input id="classtitle" type="text" style="width:300px;"></td>
    </tr>
    <tr>
      <td align="right">Number of Students:&nbsp; </td>
      <td><input id="nos" type="text" style="width:50px;"></td>
    </tr>
    <tr>
      <td align="right">Cost:&nbsp; </td>
      <td><input id="cost" type="text" style="width:50px;"></td>
    </tr>
    <tr>
      <td align="right">Start Date:&nbsp;</td>
      <td><input name="startd" id="startd" type="text" style="width:75px;"></td>
    </tr>
    <tr>
      <td align="right">End Date:&nbsp; </td>
      <td><input id="endd" type="text" style="width:75px;"></td>
    </tr>
    <tr>
      <td align="right">Start Time:&nbsp;</td>
      <td><input id="startt" type="text" style="width:75px;"></td>
    </tr>
    <tr>
      <td align="right">End Time:&nbsp;</td>
      <td><input id="endt" type="text" style="width:75px;"></td>
    </tr>
    <tr>
      <td align="right">Instructor:&nbsp; 
      <td><select id="instructor_id"><option value="">Please select : </option>
          <?php echo($instructorOptions); ?>
        </select></td>
    </tr>
    <tr>
      <td align="right">Class Description:&nbsp;</td>
      <td><textarea id="description" cols="40" rows="6"></textarea></td>
    </tr>
  </table>
</div>
<div class="grid_2">&nbsp;</div>
<div class="clear">&nbsp;</div>
<div class="grid_12">* all fields required</div>
<div class="text-center">
  <input type="submit"  value="Add Class" onClick="submitForm()" />
</div>
</div>



<style>

.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0;display:none; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }

</style>
