<?php
use Rc\Services\DbHelper;

//session_start(); 
//require('common_security.php');
//require('common/db.php');
$link = DbHelper::getWrappedHandle($app['dbs']['main']);
include('/xapp/includes/subjects.php');

$show_hide_sql = "";
$show_hide_sql = "SELECT point_of_sale, firearm_rentals  "
        . "FROM show_hide "
        . "WHERE acctnumberhashed = '" . $_SESSION['account_number_hashed']
        . "' AND accountnumber = '" . $_SESSION['account_number'] . "' ";
if ($result = $link->query($show_hide_sql)) {
    while ($row = $result->fetch_assoc()) {
        $point_of_sale = $row['point_of_sale'];
        $firearm_rentals = $row['firearm_rentals'];
    }
}
// cookie is set in ajax/show_hide.php 

//$hide_browser_compat = $_COOKIE['hide_browser_compat'];
//print $hide_browser_compat . '--';
?><!--<script type="text/javascript" src="js/blanket_popup.js"></script>
        <script type="text/javascript">
            window.history.forward(1);
            function noBack() {
                window.history.forward();
            }
            document.onkeypress = stopRKey;
            function stopRKey(evt) {
                var evt = (evt) ? evt : ((event) ? event : null);
                var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                if ((evt.keyCode == 13) && (node.type == "text")) {
                    return false;
                }
            }//end function

            function showHide(field, show_or_hide) {
                var element_value = show_or_hide;
                var msg = "";
                switch (field) {
                    case "point_of_sale":
                        {
                            msg = element_value + " POS module.";
                        }
                        break;
                    case "firearm_rentals":
                        {
                            msg = element_value + " Firearm Rentals module.";
                        }
                        break;
                }//end switch
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "success") {
                            alert(msg);
                            location.reload();
                        }
                        //else{alert("show message...");}
                    }//end if
                }//end onreadystatechange
                xmlhttp.open("POST", "../ajax/show_hide.php", false);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send("field=" + field + "&show_or_hide=" + show_or_hide);
            }//end showHide

            function dontShowAgain(field, show_or_hide) {//for browser compatibility popup
                var chk = document.getElementById("chkBrowserCompat").checked;

                var option = "";
                if (chk == true) {
                    option = "Hide";
                } else {
                    option = "Show";
                }
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "success") {
                            //alert(msg); 
                            location.reload();
                        }
                        //else{alert("show message...");}
                    }//end if
                }//end onreadystatechange
                xmlhttp.open("POST", "ajax/show_hide.php", false);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send("field=" + field + "&show_or_hide=" + option);
            }//end dontShowAgain
        </script>-->
<!DOCTYPE html>
<html>
<head>

    <style>
                        .divLeft {
                            padding-right: 20px;
                            padding-bottom: 10px;
                            float: left;
                        }
                        
                        .divRight {
                            padding-left: 20px;
                            padding-bottom: 10px;
                            float: left;
                        }
                        
                        .paddingTop {
                            padding-top: 20px;
                        }
                        
                        .paddingLeft {
                            padding-left: 20px;
                        }
    </style>
    <title></title>
</head>
<body>
    <div class="grid_12">
        <br>
        <table align="center" bgcolor="#1E1E1E" border="0" width="950">
            <tr>
                <td align="center" class="noBorder" style=
                "border-right: 1px solid #333333; border-bottom: 1px solid #333333;"
                valign="top" width="50%">
                    <div class="paddingTop paddingLeft">
                        <div class="divLeft">
                            <input class="navButton" onclick=
                            "window.location = 'customers';" type="button"
                            value="Search Members">
                        </div>
                        <div class="divRight">
                            <input class="navButton" onclick=
                            "window.location = 'currentRangeUsage';" type=
                            "button" value="Current Range Usage">
                        </div>
                        <div class="divLeft">
                            <input class="navButton" onclick=
                            "window.location = 'customers/add';" type="button"
                            value="Add New Member">
                        </div>
                        <div class="divRight">
                            <input class="navButton" onclick=
                            "window.location = 'rcScheduler';" type="button"
                            value="Scheduler">
                        </div>
                        <div class="divLeft">
                            <input class="navButton" onclick=
                            "window.location = 'maintenance';" type="button"
                            value="Range Maintenance">
                        </div>
                        <div class="divRight">
                            <input class="navButton" onclick=
                            "window.location = 'reports';" type="button" value=
                            "Reports">
                        </div>
                        <div class="divLeft">
                            <input class="navButton" onclick=
                            "window.location = 'specialEvents';" type="button"
                            value="Special Events">
                        </div>
                        <div class="divRight">
                            <input class="navButton" onclick=
                            "window.location = 'newsletters';" type="button"
                            value="Newsletter">
                        </div>
                        <div class="divLeft">
                            <input class="navButton" onclick=
                            "window.location = 'pointOfSale/0';" type="button"
                            value="Point of Sale">
                        </div>
                        <div class="divRight">
                            <input class="navButton" onclick=
                            "window.location = '../xapp/classes';" type=
                            "button" value="Classes">
                        </div>
                        <div class="divLeft">
                            <input class="navButton" onclick=
                            "window.location = 'configurations';" type="button"
                            value="Configuration">
                        </div>
                        <div class="divRight">
                            <input class="navButton" onclick=
                            "window.location='help';" type="button" value=
                            "Help">
                        </div>
                    </div>
                </td>
                <td class="noBorder" style=
                "border-bottom: 1px solid #333333; padding-left: 15px;" valign=
                "top" width="40%">
                    <br>
                    <table style=
                    "width: 100%; margin-top: 5px; margin-bottom: 0px;">
                        <tbody>
                            <tr>
                                <th style=
                                "font-size: 18px; background-color: #666;">
                                Announcement Board</th>
                            </tr>
                        </tbody>
                    </table>
                    <div style=
                    "height: 300px; overflow : auto; font-size: 14px;">
                        <table style="width: 100%;">
                            <?php foreach($announcement_board as $row)
                                    {
                                    ?>
                            <tr>
                                <td><span style=
                                "color: #fbb91f; font-weight: bold;"><?php echo $row['timestamp']; ?></span>
                                -
                                <?php echo htmlspecialchars_decode($row['description']); ?></td>
                            </tr><?php
                                    }
                                    ?>
                        </table>
                    </div><br>
                    <br>
                    <div class="text-center">
                        <?php if($days_remaining > 0){ ?><label class="version"
                        id="lblVersion">TRIAL VERSION<br>
                        There are <?php echo $days_remaining; ?> days left<br>
                        Or <a href=
                        "../payment_form_trial.php?cn=%3C?php%20echo%20$accno;%20?%3E">
                        click here</a> to pay now.</label>
                    </div>
                </td><?php } ?>
            </tr>
        </table>
    </div><?php
    //require('xapp/version.php');
    ?>
</body>
</html>