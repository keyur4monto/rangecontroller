<script>
function submitForm () {    
    if (document.getElementById("payment_type").value == "") 
	{
		document.getElementById("popupContent").innerHTML = "Please fill out all the required fields!";
			
		$( "#dialog" ).dialog( "open" );
			
		$("#shadow").show();
		//document.getElementById("errid").style.display = "block";
	}
    else {
        var id = $("#type_id").val();
        var title = $("#payment_type").val();        
        
        $.ajax({  
            type: "POST",  
            url: "/xapp/config/billing/paymentType/check",  
            data:{"id": id, "title":title},
            success: function(datas){                
                if(datas == 'true'){
                    document.getElementById("errid").style.display = "block";
                    $('.error').html('Payment type already exist!');
                    return false;        
                }else{
				/*document.getElementById("popupContent").innerHTML = "Your changes have been saved!";
			
		$( "#dialog1" ).dialog( "open" );
			
		$("#shadow").show();*/
        $('#payment_type_frm').submit();                    
                }                
            }
        });
        
        /*var myurlstring = encodeURIComponent(document.getElementById("payment_type").value);        
        myurlstring = "/xapp/config/billing/paymentType/add/" + myurlstring;
        //alert(myurlstring);
        return false
        var xmlHttp = null;
        xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", myurlstring, false );
        xmlHttp.send( null );
        //alert(xmlHttp.responseText);
        //ranger.members.edit.showSuccess("Successfully updated!");
        //$("#popupdiv").show();*/
        //window.location = "/xapp/config/billing/paymentType/add";        
    }
}
</script>
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Your changes have been saved!</p>
</div>
<div id="dialog1" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Your changes have been saved!</p>
</div>
<div class="grid_12">
	<div class="text-right">
</div>
</div>
<div class="grid_3">&nbsp;</div>
<div id="errid" style="display:none;">
    <?php echo $err; ?>
</div>
<div class="clear"></div>
<div class="grid_12">
    <div class="text-right"></div>
    <form action="" method="POST" id="payment_type_frm">
    <div class="grid_12 margt">
    	<table style="width: 50%;">
    		<tr>
    			<td class="text-right">Type</td>
    			<td><input id="payment_type" name="payment_type" type="text" value="<?php echo ($type_title != '')?$type_title:'';?>"/></td>
                <input type="hidden" name="cmd" value="<?php echo ($type_id != '')?'edit':'add';?>" />                
                <input id="type_id" type="hidden" name="type_id" value="<?php echo ($type_id != '')?$type_id:'';?>" />
    		</tr>
    	</table>
    </div>
    <div class="grid_12 text-center">
    	<input type="button" value="Save" onclick="submitForm()">
    </div>
    </form>
</div>
<div class="clear">&nbsp;</div>
<script>
		
	$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
			}
		},
	]
	});
	$( "#dialog1" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
				$('#payment_type_frm').submit();
			}
		},
	]
	});
	  
	// Link to open the dialog
	$( "#dialog-link1" ).click(function( event ) {
		$( "#dialog" ).dialog( "open" );
		$( "#dialog1" ).dialog( "open" );
		$("#shadow").show();
		event.preventDefault();
	});
</script>