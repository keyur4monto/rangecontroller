<script src="/xapp/js/multifilter.min.js"></script>
<script>


var type_id = 0;

function deleteItem(item_id) {
type_id = item_id;
document.getElementById("popupContent").innerHTML = "Really delete this Payment Type ?";

$( "#dialog" ).dialog( "open" );

$("#shadow").show();

}

</script>
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
  <p id="popupContent" style="">Really delete this item from the inventory?</p>
</div>
<div class="grid_12">
	<div class="text-right">
    <input type="button" onclick="window.location='paymentType/add'" value="Add">
</div>
</div>
<div class="grid_3">&nbsp;</div>
<div class="grid_6 margt">
<div class="success"><?php
if(isset($scc) && !empty($scc)){
    echo $scc;
}
?></div>
<table>
	<tr>
        <th style="width: 70%;">For</th><th colspan="2">Action</th>
    </tr>
    <?php
    if(!empty($payment_types)){
        foreach($payment_types as $payment_type){
            ?>
            <tr>
            	<td><?php echo $payment_type['type_title']?></td>
            	<td><a href="paymentType/manage/<?php echo $payment_type['type_id']?>">Manage</a></td>
            	<td><a href="#none" onclick="deleteItem(<?php echo $payment_type['type_id']?>)">Delete</a></td>
            </tr>        
            <?php    
        }            
    }else{
        ?>
        <tr>
        	<td colspan="3">No record found.</td>        	
        </tr>
        <?php
    }
    ?>
</table>
<div class="grid_3">&nbsp;</div>
<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				window.location="/xapp/config/billing/paymentType/delete/" + type_id;
				
				
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

</script> 
