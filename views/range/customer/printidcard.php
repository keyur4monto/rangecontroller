<?php
foreach($get_id_card_creator as $row1)
{
$gender = $row1['gender'];
$client_name = $row1['client_name'];
$client_picture = $row1['client_picture'];
$client_address = $row1['client_address'];
$client_dob = $row1['client_dob'];
$client_certs = $row1['client_certs'];
$client_emergency_contact = $row1['client_emergency_contact'];
$client_membership_type = $row1['client_membership_type'];
$client_membership_number = $row1['membership_number'];
$client_expiration_date = $row1['expiration_date'];

$barcode = $row1['barcode'];
$company_name = $row1['company_name'];
$company_logo = $row1['company_logo'];
$use_logo_for_background = $row1['use_logo_for_background'];
$company_add = $row1['company_address'];
$company_phone_number = $row1['company_phone_number'];
$use_company_slogan = $row1['use_company_slogan'];
$company_slogan = $row1['company_slogan'];
$membership_border_banner = $row1['membership_border_banner'];
$use_special_instructions = $row1['use_special_instructions'];
$special_instructions = $row1['special_instructions'];
$shooter_portal_text = $row1['shooter_portal_text'];
$shooter_login_and_password = $row1['shooter_login_and_password'];
$use_background_image = $row1['use_background_image'];
}
foreach($get_customer as $get_customers)
{
$first_name = $get_customers['first_name'];
$last_name = $get_customers['last_name'];
$street_address = $get_customers['street_address'];
$city = $get_customers['city'];
$state = $get_customers['state'];
$zipcode = $get_customers['zipcode'];
}
foreach($get_emergency_contact as $get_emergency_contacts)
{
$emergency_contact_phone = $get_emergency_contacts['contact_phone'];
}
foreach($get_driver_license_info as $get_driver_license_infos)
{
$birth_date = $get_driver_license_infos['license_birthdate'];
}
foreach($get_memberships as $get_membership)
{
    $membership_type = $get_membership['membership_type'];
    $membership_expires = $get_membership['membership_expires'];
    
}
foreach($get_certs_and_safety as $get_certs_and_safeties)
{
$certs_certificates_comments = $get_certs_and_safeties['certs_certificates_comments'];
}
foreach($get_controller_config as $get_controller_configs)
{
$company_names = $get_controller_configs['company_name'];
$company_address = $get_controller_configs['company_address'];
$company_city = $get_controller_configs['company_city'];
$company_state = $get_controller_configs['company_state'];
$company_zipcode = $get_controller_configs['company_zipcode'];
$company_phone = $get_controller_configs['company_phone'];
}
foreach($get_member_login as $get_member_logins)
{
$current_pw = $get_member_logins['current_pw'];
$accno = $get_member_logins['accountnumber'];
$membershipNo = $get_member_logins['membership_number'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ID Card</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<style>

body {margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:14px;}
.clr{clear:both; font-size:0; line-height:0; height:0px;}
.idcard{}
.wapper{width:323.527559px;margin:0 auto; padding:3px 0px 3px 3px; background:#FFF; height:195.094px;}
.header{}
.header_l{float:left; display:inline-block; width:106px;}
.header_img{border: 5px solid #000;}
.left_id {display:block; width:100%; text-align:center; font-size:14px; font-weight:bold; color:#000; margin-top:8px; }
.header_r{float:left; display:inline-block; width:217px;}
.header_r_img {  display: inline-block; margin:6px 10px 6px 6px; float:left;}
.header_r_text {  float:left; display:block; width:141px;}
.header_r_text p{margin-top:0px; margin-bottom:3px;}
.header_r_titel{font-size:12px; font-weight:bold; margin-left:5px; margin-bottom:0px; margin-top:0px;}
.header_r_text span{font-size:9.33333px; font-weight:bold;}
strong{font-size:8px; font-weight:bold;}
.header_r_btm{display:block; background:#000; background-color:#000; font-size:13.3333px; font-weight:bold; color:#FFF; width:217px; text-align:center; height: 20px;line-height: 20px;}
.header_r_btm2{width:225px; padding-left:22px;}
.btm2_l{float:left; display:inline-block; margin-left:-10px;}
.btm2_l strong{font-size:8px; font-weight:bold;}
.btm2_l span{font-size:10.6667px; font-weight:bold;}
.btm2_r{ display: block;    float: left;    font-size: 10px;    font-weight: bold;    text-align: right; margin-top:0px;  margin-left:26px;}
.btm2_l > p {  margin:0;}
.conten{text-align:center; display:inline-block; width:100%;}
.conten p{margin:0; line-height:10px;}
.conten strong{font-size:8px; font-weight:bold;}
.conten span{font-size:9.33333px; font-weight:bold;}
.conten .range{font-size:18px; font-weight:bold; margin-top:25px;}
.barcode > img {  height: 15px;  margin-left:8px; width: 178px;}
.barcode{text-align:center; margin-top:2px;}
span.certifi{ font-size:10.6667px;}
.header_img.border-none { border: 5px solid #ffffff;}
.header_r_btm.back-none { background: #ffffff; color: #000000;}

@media screen and (-webkit-min-device-pixel-ratio:0) {
.header_r_text span{ line-height:11px;} 

}
</style>
</head>
<body>
<div class="idcard">
<div class="wapper">
	<div class="header">
    	<div class="header_l">
			<?php if($client_picture=="Yes"){ ?>
            <?php if($membership_border_banner=="Yes"){ ?>
        	<div class="header_img"> 
            <img src="<?php echo $app['range_member_image_url']($member->getMembershipNumber());?>" style="width:auto; height: 102px;max-height:102px; max-width:100%;""  />
            </div>
            <?php } ?>
             <?php if($membership_border_banner=="No"){ ?>
            <div class="header_img border-none"> 
            <img src="<?php echo $app['range_member_image_url']($member->getMembershipNumber());?>" style="width:auto; height: 102px;max-height:102px; max-width:100%;""  />
            </div>
            <?php } ?>
            <?php } ?>
            <?php if($client_membership_number=="Yes"){ ?>
            <div class="left_id">
			<?php echo $membershipNo; ?>
            </div>
            <?php } ?>
        </div>
        <div class="header_r">
        	<div class="header_r_top">
            <?php if($company_name=="Yes"){ ?>
            <div class="header_r_titel">
			<?php echo $company_names; ?>
            </div>
            <?php } ?>
            <?php if($company_logo=="Yes"){ ?>
                <div class="header_r_img">
                <img src="<?php echo $rangeLogoUrl; ?>" style="width:40px;; height:40px;"/>
                 </div>
                 <?php } ?>
                <div class="header_r_text">
                    <p><strong>
					<?php if($use_company_slogan=="Yes"){ ?>
					<?php echo $company_slogan; 
					echo "</br>"; ?>
                    <?php } ?>
<span>
<?php if($company_add=="Yes"){ ?>
<?php echo $company_address; ?> <br />
<?php echo $company_city; ?>, <?php echo $company_state; ?> <?php echo $company_zipcode; ?>
<?php } ?><br />
<?php if($company_phone_number=="Yes"){ ?>
<?php echo $company_phone; ?>
<?php } ?>
</span></strong></p>
                </div>
            </div>
            <div class="clr"></div>
            <?php if($client_membership_type=="Yes"){ ?>
            <?php if($membership_border_banner=="Yes"){ ?>
            <div class="header_r_btm">
             	<?php echo $membership_type; ?>
            </div>
            <?php } ?>
            <?php if($membership_border_banner=="No"){ ?>
            <div class="header_r_btm back-none">
             	<?php echo $membership_type; ?>
            </div>
            <?php } ?>
            <?php } ?>
            <div class="header_r_btm2">
            	<div class="btm2_l">
                	<p><span>
                    <?php if($client_name=="Yes"){ ?>
					<?php echo $first_name; ?> <?php echo $last_name; ?>
                    <?php } ?>
                    <br />
                    <?php if($client_address=="Yes"){ ?>
<?php echo $street_address; ?><br />
<?php echo $city; ?>, <?php echo $state; ?> <?php echo $zipcode; ?>
 <?php } ?>
 </span><br />
 <?php if($client_emergency_contact=="Yes"){ ?>
<strong>Emergency: <?php echo $emergency_contact_phone; ?></strong>
 <?php } ?>
</p>
                </div>
                <?php if($client_dob=="Yes"){ ?>
                <div class="btm2_r">
				<?php echo $birth_date; ?>
                 <?php }
                 if($client_expiration_date=="Yes"){
                    ?>
                    <span id="expiration_date" style="color: red;"><br />Expiration<br /><?php echo $membership_expires;?></span>
                    <?php
                 }?>
                </div>                 
            </div>
        </div>
    </div>
    <div class="conten">
    	<?php if($use_special_instructions=="Yes"){ ?><p><strong>Special Instructions: <?php echo $special_instructions; ?></strong></p> <?php } ?>
        <?php if($client_certs=="Yes"){ ?><p><span class="certifi">Other Certifications: <?php echo $certs_certificates_comments; ?></span></p><?php } ?>
        <?php if($shooter_portal_text=="Yes"){ ?><p><span>Shooter portal online! www.rangecontroller.com</span></p><?php } ?>
        <?php if($shooter_login_and_password=="Yes"){ ?><p><span>RANGE#: <?php echo $accno; ?> SHOOTER#: <?php echo $membershipNo; ?> PASSWORD: <?php echo $current_pw; ?></span></p><?php } ?>
    </div>
    <div class="barcode">
    	<img src="/xapp/inventory/printbarImage/<?php echo $membershipNo; ?>" />
    </div>
</div>
</div>
</body>
</html>