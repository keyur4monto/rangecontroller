<script>
    $(function(){
        $('#memExpirationDate, #memRegistrationDate').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });        
    });
    function memTypeChanged_(elm){
        var val = $(elm).val();
        if (val === 'ADD NEW TYPE') {
            //window.location = $(elm).attr('data-addtype-url');
			window.location = '/xapp/config/memberships';
        }
    }
</script>
<table width="100%" align="center" cellpadding="5"cellspacing="0" border="0">
    <tbody>
        <tr>
            <td align="right" style="width:170px"><span class="requiredf">*</span>Membership Type:</td>
            <td><select name='membershipType' id="membershipType"
                        onchange="memTypeChanged_(this)"
                        data-addtype-url='<?php 
                        echo $addMemberTypeUrl; 
                        ?>'>                    
                <option value=""></option>
                <?php foreach($membershipTypes as $mt) : ?>
                <option value="<?php echo $mt->getKey(); ?>"
                        <?php echo $mt->getKey() == $membershipType ? 'selected' : ''; ?>>
                    <?php echo $mt->getName();?>
                </option>
                
                <?php endforeach; ?>
                <option value=""></option>
                <option value="ADD NEW TYPE">ADD NEW TYPE</option>
                </select>
            </td>
        </tr>
        
        <tr>
            <td align="right">Registration Date:</td>
            <td><input style="position:relative;" type="text" 
                       value="<?php echo $view->escape($memRegistrationDate)?>"                    
                       name="memRegistrationDate"
                       id="memRegistrationDate"
                       style="width:90px"></td>
        </tr>
        <tr>
            <td align="right">Expiration Date:</td>
            <td><input style="position:relative;" type="text"
                       value="<?php echo $view->escape($memExpirationDate)?>"                    
                       name="memExpirationDate"
                       id="memExpirationDate"
                       style="width:90px"></td>
        </tr>
        <tr> </tr>
    </tbody>
</table>

