<?php 
//$member = new \Rc\Models\range_member();
$dl = $member->getDriversLicense(true);

?>
<script>
    $(function(){
        $('#dlExpiration').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true,
            yearRange: '-0:+20'
        });        
        $('#dlBirthDate').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true,
            yearRange: '-99:-3',
            defaultDate: '-30y'
        });        
        $('#dlWeight, #dlHeightFeet').mask('000');
    });
</script>
<table width="100%"  border="1" cellpadding="5" cellspacing="0">
    <tbody>
    <tr >
        
        <td class="labeltd">State:</td>
        <td>
            <select name="dlState" id='dlState'>
                <option value=""></option>
                <?php foreach($stateList as $code => $name) : ?>
                <option value="<?php echo $code; ?>"
                        <?php echo $code == $dl->state ? 'selected' : ''; ?>>
                    <?php echo $name;?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="labeltd">Number:</td>
        <td>
                <input type="text" 
                       value="<?php echo $view->escape($dl->number)?>"                    
                       id="dlNumber"
                       style="width:75px"></td>
    </tr>
    <tr>
        <td class="labeltd">Expiration:</td>
        <td>
            <input type="text" 
                       value="<?php echo $view->escape($dl->expirationDate)?>"
                       id="dlExpiration"
                       style="width:90px"></td>
    </tr>
</tbody></table>

<table width="100%"  border="1" cellpadding="5" cellspacing="0">
    <tbody>
    <tr >
        <td class="labeltd">Height:</td><td>
            <input type="text" 
                   value="<?php echo $view->escape($dl->heightFeet)?>"
                   id="dlHeightFeet"
                   style="width:25px">
            &nbsp;ft&nbsp;
            <input type="text"
                   value="<?php echo $view->escape($dl->heightInches)?>"
                   id="dlHeightInches"
                   style="width:25px">
            &nbsp;in</td>
    </tr>
    <tr>
        <td class="labeltd">Weight:</td><td>
            <input type="text" 
                   value="<?php echo $view->escape($dl->weight)?>"
                   id="dlWeight"
                   style="width:25px">
            &nbsp;lbs</td>
    </tr>
    <tr >
        <td class="labeltd" >Hair:</td>
        <td>
            <select name="dlHair" id='dlHair'>
                <option value=""></option>
                <?php foreach($hairColors as $code => $name) : ?>
                <option value="<?php echo $code; ?>"
                        <?php echo trim(strtolower($code)) == 
                        trim(strtolower($dl->hairColor)) ? 'selected' : ''; ?>>
                    <?php echo $name;?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
</tr><tr>
            <td class="labeltd">Eyes:</td>
            <td>
            <select name="dlEyes" id='dlEyes'>
                <option value=""></option>
                <?php foreach($eyeColors as $code => $name) : ?>
                <option value="<?php echo $code; ?>"
                        <?php echo trim(strtolower($code)) == 
                        trim(strtolower($dl->eyeColor)) ? 'selected' : ''; ?>>
                    <?php echo $name;?>
                </option>
                <?php endforeach; ?>
            </select>
            </td>
    </tr>
    <tr >
        <td  class="labeltd">Race:</td>
        <td>
            <select name="dlRace" id='dlRace'>
                <option value=""></option>
                <?php foreach($races as $code => $name) : ?>
                <option value="<?php echo $code; ?>"
                        <?php echo trim(strtolower($code)) == 
                        trim(strtolower($dl->race)) ? 'selected' : ''; ?>>
                    <?php echo $name;?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="labeltd">Sex:</td>
        <td>
            <select name="dlGender" id='dlGender'>
                <option value=""></option>
                <?php foreach($genders as $code => $name) : ?>
                <option value="<?php echo $code; ?>"
                        <?php echo trim(strtolower($code)) == 
                        trim(strtolower($dl->gender)) ? 'selected' : ''; ?>>
                    <?php echo $name;?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    </tbody>
</table>

<table width="100%"  border="1" cellpadding="5" cellspacing="0">
    <tbody>
    <tr>
        <td class="labeltd">Birth Date:</td>
        <td>
            <input type="text"
                   value="<?php echo $view->escape($dl->birthDate)?>"
                       id="dlBirthDate"
                   style="width:90px"></td>
    </tr>
    <tr>
        <td class="labeltd" >Birth City:</td>
        <td>
            <input type="text" 
                   value="<?php echo $view->escape($dl->birthCity)?>"
                       id="dlBirthCity"
                   style="width:100px"></td>
    </tr>
    <tr>
        <td class="labeltd">Birth State:</td>
        <td>
            <select name="dlBirthState" id='dlBirthState'>
                <option value=""></option>
                <?php foreach($stateList as $code => $name) : ?>
                <option value="<?php echo $code; ?>"
                        <?php echo $code == $dl->birthState ? 'selected' : ''; ?>>
                    <?php echo $name;?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    </tbody>
</table>
 

<?php //print_r($dl); ?>

<!-- input type hidden re lists comments in DL update form -->

<input type="hidden" value="<?php echo $view->escape($dl->comments); ?>" id="commentsText" style="width:90px">