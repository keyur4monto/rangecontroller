<?php 
//$member = new \Rc\Models\range_member();
//echo '==>'.$block_stat_date;
//echo $member->getMembershipVisitcount();
$dl = $member->getDriversLicense(true);
$dlinfo = [
    ['State' => $dl->state ,'Number' => $dl->number, 'Expiration'=> $dl->expirationDate],
    ['Height' => $dl->getHeightText(), 
        'Weight' => $dl->weight. (empty($dl->weight) ? '' : ' lbs')],
    ['Hair' => $dl->hairColor, 'Eyes' => $dl->eyeColor],
    ['Race' => $dl->race, 'Gender' => $dl->gender],
    ['Birth Date' => $dl->birthDate, 'Birth City' => $dl->birthCity, 
        'Birth State' =>$dl->birthState ]
];

$e_contect = ''; $e_name = '';
$e_name = trim($member->getEmergencyContactName());
$e_contect = trim($member->getEmergencyContactPhone());
$error_1='';$error_2='';$error_3='';$error_4='';$error_5='';$error_6='';$error_7='';$error_8='';$error_9='';$error_10='';$error_11='';
$error_12='';$error_13='';$error_14='';
if($e_name == '' || $e_contect == '' || $e_name == null || $e_contect == null){
    $error_1 = 'yes';   
}

$expiration_date = $member->getMembershipExpirationDate();
$e_date = date('Y-m-d',strtotime($expiration_date));
$today = date('Y-m-d');
$date1=date_create($e_date);
$date2=date_create($today);
$diff=date_diff($date1,$date2);
$e_days = $diff->format("%a");
$ago_befor = $diff->format("%R%");
if(isset($expiration_date) && !empty($expiration_date)){
    if($ago_befor == '-' && $e_days <= 60){
        $error_2 = 'yes';    
    }elseif($ago_befor == '+'){
        $error_3 = 'yes';
    }
}    


$driving_number = '';
$driving_number = $dl->number;
$driving_expire = $dl->expirationDate;
$lic = strtotime($driving_expire);
$today = strtotime(date('m/d/Y'));
if($driving_number == ''){
    $error_4 = 'yes';
}elseif($today > $lic){
    $error_5 = 'yes';
}


$b_date = '';
$b_date = $dl->birthDate;        
$e_date = date('Y-m-d',strtotime($b_date));
$today = date('Y-m-d');
$date1=date_create($e_date);
$date2=date_create($today);
$diff_2=date_diff($date1,$date2);      
$customer_yeas = $diff_2->format("%y");
if($customer_yeas < 21){
    $error_6 = 'yes';
}


$b_date = '';
$b_date = $dl->birthDate;
if($b_date == ''){
    $error_7 = 'yes';
}


$visitcount = $member->getVisitCount();        
$membershipType = strtolower($member->getMembershipType());
if($membershipType == 'visitor' || $membershipType == 'student' || $membershipType == 'law enforcement' || $membershipType == 'employee'){
    if(isset($visitcount) && $visitcount > 0){
        $error_8 = 'yes';
    }
}   


$s_video = '';
$s_video = trim($member->getCertsSafetyVideoCompleted());
if( strtolower($s_video) != 'yes'){
    $error_9 = 'yes';
}

$s_form = '';
$s_form = $member->getCertsSafetyRangeFormCompleted();
if(strtolower($s_form) != 'yes'){
    $error_10 = 'yes';
}

if(isset($block_stat_date) && !empty($block_stat_date)){
    $error_11 = 'yes';
}
//echo $emp_name; 
?>
<div class='overview' id="overviewWrap">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>
    <?php /** Notification Table with Error Messages */ 
    //echo $isMemberBlocked;
    if($error_1 == 'yes' || $error_2 == 'yes' || $error_3 == 'yes' || $error_4 == 'yes' || $error_5 == 'yes' || $error_6 == 'yes' || $error_7 == 'yes' || $error_8 == 'yes' || $error_9 == 'yes' || $error_10 == 'yes' || $error_11 == 'yes'){
    ?>
    <table id="alertTable">
        <tbody>
        <?php if($error_1 == 'yes'){ ?>
            <tr id="row_1" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Emergency Contact is missing.</td>
                <td style="width: 10%;"><span class="hide_me_cl" onclick="hide_me('row_1')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>
        <?php if($error_2 == 'yes'){ ?>        
                <tr id="row_2" class="rows">
                    <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Membership will expire in <span style="color: #fbb91f;"><?php echo $e_days?></span> days.</td>
                    <td style="width: 10%;"><span class="hide_me_cl" onclick="hide_me('row_2')" style="cursor: pointer;">Hide</span></td>
                </tr>
        <?php } ?>
        <?php if($error_3 == 'yes'){ ?> 
                <tr id="row_3" class="rows">
                    <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Membership expired <span style="color: #fbb91f;"><?php echo $e_days?></span> days ago.</td>
                    <td style="width: 10%;"><span class="hide_me_cl"  onclick="hide_me('row_3')" style="cursor: pointer;">Hide</span></td>
                </tr>
        <?php } ?>
        <?php if($error_4 == 'yes'){ ?>     
            <tr id="row_4" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Driver License is missing.</td>
                <td style="width: 10%;"><span class="hide_me_cl" onclick="hide_me('row_4')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>
        <?php if($error_5 == 'yes'){ ?>        
            <tr id="row_5" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Driver License is expired.</td>
                <td style="width: 10%;"><span class="hide_me_cl"  onclick="hide_me('row_5')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>
        <?php if($error_6 == 'yes'){ ?>
            <tr id="row_6" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Shooter is under <span style="color: #fbb91f;">21</span> years old. </td>
                <td style="width: 10%;"><span class="hide_me_cl"  onclick="hide_me('row_6')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>    
        <?php if($error_7 == 'yes'){ ?>            
            <tr id="row_7" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Birth Date is missing.</td>
                <td style="width: 10%;"><span class="hide_me_cl"  onclick="hide_me('row_7')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>    
        <?php if($error_8 == 'yes'){ ?>
            <tr id="row_8" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Shooter has visited the range <span style="color: #fbb91f;"><?php echo $visitcount;?></span> times<!-- this year-->, consider a membership.</td>
                <td style="width: 10%;"><span class="hide_me_cl"  onclick="hide_me('row_8')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>
        <?php if($error_9 == 'yes'){ ?>           
            <tr id="row_9" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Safety video has not been completed.</td>
                <td style="width: 10%;"><span class="hide_me_cl"  onclick="hide_me('row_9')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>
        <?php if($error_10 == 'yes'){ ?>
            <tr id="row_10" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;Range safety form has not been completed.</td>
                <td style="width: 10%;"><span class="hide_me_cl"  onclick="hide_me('row_10')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>
        <?php if($error_11 == 'yes'){ ?>
            <tr id="row_11" class="rows">
                <td><i class="icon-warning-sign" style="color: red;"></i>&nbsp;&nbsp;This member is blocked from the range as of <span style="color: #fbb91f;"><?php echo date('m/d/Y',strtotime($block_stat_date))?></span>.</td>
                <td style="width: 10%;"><span class="hide_me_cl"  onclick="hide_me('row_11')" style="cursor: pointer;">Hide</span></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php 
    }
    /** End */ ?>
    <table>
        <tbody>
        <tr>
            <td class='ovlabel '><span class="ovlabeltxt">Visits:</span></td>
            <td><span class="visitCount"> <?php echo $member->getVisitCount(); ?></span></td>
            <td rowspan="7" style="width:45%;vertical-align:middle"
            class='text-center'><img src="<?php 
            echo $app['range_member_image_url']($member->getMembershipNumber()) . '?rand='. strtotime('now');?>"
            style="width:auto; height: 180px;max-height:190px; max-width:280px;"
            /></td>
        </tr>
        <tr>
            <td nowrap class='ovlabel '><span class="ovlabeltxt">Membership Type:</span></td>
            <td nowrap><?php echo $member->getMembershipType(); ?></td>
        </tr>
        <tr>
            <td class='ovlabel '><span class="ovlabeltxt">Member Number:</span></td>
            <td ><?php echo $member->getMembershipNumber();  ?></td>
        </tr>
        <tr>
            <td nowrap class='ovlabel '><span class="ovlabeltxt">Registration Date:</span></td>
            <td ><?php echo $member->getMembershipDate();  ?></td>
        </tr>
        <tr>
            <td class='ovlabel '><span class="ovlabeltxt">Expiration Date:</span></td>
            <td><?php echo $member->getMembershipExpirationDate(); ?></td>
        </tr>
        <tr>
            <td class='ovlabel '><span class="ovlabeltxt">Added By:</span></td>
            <td id="addedBy" class="capitalize"><?php echo ($emp_name); ?></td>
        </tr>
        <tr>
            <td colspan="2" class="text-center" style="font-size: 15px;">
            <!--<a href="<?php echo $schedulerUrlPrefix . '&memberNo='. 
            $member->getMembershipNumber(); ?>">Schedule &raquo;</a>-->
            <a href="../rcScheduler">Schedule »</a> &nbsp;&nbsp;
            <a href="../pointOfSale/<?php echo $member->getMembershipNumber();  ?>">POS »</a> &nbsp;&nbsp;
            <?php if ($member->isLoggedIn()): 
            $logurl = "/xapp/customers/lane/logThemOut/" . 
            $member->getMembershipNumber();
            $logtxt = 'Log Out';
            $onclick= "return ranger.members.edit.logoutClicked(this, '" .
            $member->getMembershipNumber() . ' ' . 
            $view->escape($member->getFullName()) . "') ";
            else :
            $logurl = "/xapp/customers/login/SelectLane/" . 
            $member->getMembershipNumber();
            $onclick= '';
            $logtxt = 'Log In';
            endif; ?>
            <a href="<?php echo $logurl ?>" 
            onclick="<?php echo $onclick; ?>"> <?php echo $logtxt; ?> &raquo;</a> &nbsp;&nbsp; <a href="#" 
            onclick='return ranger.members.edit.blockUnblock(1, this)'
            style='display: <?php 
            echo $isMemberBlocked ? 'none' : ''; ?>'
            id='blockMemberLink'>Block &raquo;</a> <span id='unblockMemberLink' title='Member is blocked'
            style='display: <?php 
            echo $isMemberBlocked ? '' : 'none'; ?>'> <span style='color:red'> <i class='fa fa-warning fa-lg'></i> </span> <a href="#" 
            onclick='return ranger.members.edit.blockUnblock(0, this)'
            > UnBlock</a> </span> &nbsp;&nbsp;
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
            <a id="deletecustomer" href="#" title="Delete">Delete »</a> </td>
        </tr>
        </tbody>
    </table>
    <table width="100%" align="center" class="infodata"
    cellpadding="5" border="1" cellspacing='0'>
        <tbody>
        <tr>
            <td class="capitalize"><div class='grid_4'>
            <h4 >Basic Info</h4>
            <?php echo $member->getFullName(); ?><br />
            <address>
            <?php echo $member->getAddress()->getFullAddress('<br />'); ?>
            </address>
            <label class="ovlabeltxt">SSN#: </label>
            <?php echo $member->getSsn() ; ?> </div>
            <div class='grid_4' style=''>
            <h4>Contact</h4>
            <label class="ovlabeltxt">Phone 1:</label>
            <?php echo $member->getPhone1() ; ?><br />
            <label class="ovlabeltxt">Phone 2:</label>
            <?php echo $member->getPhone2() ; ?><br />
            <label class="ovlabeltxt">Email:</label>
            <?php if ($member->getEmail()) : ?>
            <a href="mailto:<?php echo $member->getEmail() ; ?>"><span class="lowercase"> <?php echo $member->getEmail() ; ?></span></a><br>
            <a href="#"  id="resendsignup" title="Resend Welcome Email"><i class="fa fa-envelope" style="color: white; font-size: 20px;"></i></a>&nbsp;&nbsp;
            <?php endif; ?>
            <!--Toggle these two reading from the DB-->
            <?php $emailstatus = $member->getEmailStatus() ; 
            if($emailstatus == "subscribe")
            {
            ?>
            <label class="ovlabeltxt" style="color: yellow;"><i>Subscribed</i></label>
            <?php
            }
            else
            {
            ?>
            <label class="ovlabeltxt" style="color: red;"><i>Unsubscribed</i></label>
            <?php
            }
            ?>
            <br/>
            <!--<a id="resendsignup" href="#" title="Resend Welcome Email"><i class="fa fa-envelope" style="color: white; font-size: 20px;"></i></a>&nbsp;&nbsp;
            <label class="ovlabeltxt" style="color: yellow;"><i>Resend Sign up email</i></label>-->
            </div>
            <div class='grid_4'>
            <h4>Emergency Contact</h4>
            <?php echo $member->getEmergencyContactName() ; ?><br />
            <?php echo $member->getEmergencyContactPhone() ; ?> </div></td>
        </tr>
        </tbody>
    </table>
    <div class="grid_3">
        <h4>Billing Info</h4>
    </div>
    <div class="grid_9 omega" style="margin-left:2%">
        <div id="overviewBillingTbl" style="float:right;"></div>
    </div>
    <div class="clear">&nbsp;</div>
    <table width="100%"  border="1" cellpadding="5" cellspacing="0">
        <tbody>
        <tr>
            <td style="border-bottom:0px"><h4>Comments</h4></td>
        </tr>
        <tr>
            <td  style="border-top:0px"><?php echo $member->getComments(); ?></td>
        </tr>
        </tbody>
    </table>
    <div class="grid_3">
        <h4>Drivers License<br />
        Information</h4>
    </div>
    <div class="grid_9 omega" style="margin-left:2%">
        <table width="100%"  cellpadding="5" border="1" cellspacing='0'
        class="table-td-centered">
            <tbody>
                <?php 
                foreach ($dlinfo as $row) {
                ?>
                <tr >
                    <?php foreach($row as $k => $v) : ?>
                    <td colspan="<?php echo 6/count($row); ?>"
                    style="awidth: <?php echo round(100.00/count($row),3); ?>%; aborder-bottom:0px;"><?php 
                    echo '<label class="ovlabeltxt">'.$k . '</label>:' . (count($row) <= 2 ? ' ' : '<br />') . $v; 
                    ?></td>
                    <?php                
                    endforeach; ?>
                </tr>
                <?php 
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="clear">&nbsp;</div>
    <h4>Additional Info</h4>
    <table width="100%"  border="1" cellpadding="5" cellspacing="0">
        <tbody>
            <tr>
                <td ><div class="grid_6 alpha text-right">
                <label class="ovlabeltxt">Safety Video Completed: </label>
                </div>
                <div class="grid_6"><?php echo $member->getCertsSafetyVideoCompleted(); ?> </div>
                <div class="clear">&nbsp;</div>
                <div class="grid_6 alpha text-right">
                <label class="ovlabeltxt">Safety Range Form Completed:</label>
                </div>
                <div class="grid_6">
                <?php 
                echo $member->getCertsSafetyRangeFormCompleted(); ?>
                </div>
                <div class="clear">&nbsp;</div>
                <div class="margt-sm">
                <h4 >Certifications</h4>
                <div> <?php echo $member->getCertsComments(); ?> </div>
                </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="clear">&nbsp;</div>
    <div class="grid_3">
        <h4>Recent Visits</h4>
    </div>
    <div class="grid_9 omega">
        <table class="recentvisit" width="100%" cellpadding="5" border="1" cellspacing="0">
            <tr>
                <td>Date</td>
                <td>Time In</td>
                <td>Time Out</td>
                <td>Duration</td>
                <td style="width: 10%;">&nbsp;</td>
            </tr>
            <?php if(!empty($lastvisits)){ ?>
            <?php foreach($lastvisits as $visit) { ?>
            <tr>
                <?php 
                $vdate=$visit['date_stamp'];
                $start_time=date("g:i a", strtotime($visit['start_time']));
                $end_time=date("g:i a", strtotime($visit['end_time']));
                $st_t = explode(":", $visit['start_time']);
                $en_t = explode(":", $visit['end_time']);
                $hrs = (int) $en_t[0] - (int) $st_t[0];
                $mins = (int) $en_t[1] - (int) $st_t[1];
                if ($mins < 0){
                $mins += 60;
                $hrs--;
                }
                if ($hrs < 0) $hrs * -1;
                $duration = $hrs . ":" . sprintf("%02s", $mins);
                ?>
                <td><?php echo $vdate; ?></td>
                <td><?php echo $start_time; ?></td>
                <td><?php echo $end_time; ?></td>
                <td><?php echo $duration;?></td>
                <td style="width: 10%;"><a href="#" class="deletevisit" rel="<?php echo $visit['id']; ?>" >Delete</a></td>
            </tr>
            <?php } ?>
            <?php if($totalvisitcounter > 3) { ?>
            <tr id="visitpagination">
                <td colspan="5" style="text-align: center;"><a href="#" id="showmorevisits" data-page="1"><span>Show more</span></a></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
                <td colspan="5" style="text-align: center;"><span style="color: red;">No Records Found</span></td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="grid_3">
        <h4>Benefits</h4>
    </div>
    <div class="grid_9 omega">
        <table width="100%" cellpadding="5" border="1" cellspacing="0">
            <tbody>
            <!--<tr>
            <td style="width: 15%;">Qty</td><td class="text-center">Benefit</td></tr>-->
            <?php
            /*echo '<pre>';
            print_r($overview_benefits);
            echo '</pre>';*/
            ?>
            <?php //echo($overview_benefits['overview_benefits']); ?>
            <?php echo($overview_benefits); ?>
            </tbody>
        </table>
    </div>
</div>
<div id="deletedialog" title="Delete Confirmation" style="display:none">
    <p id="warningtext2" >Your changes have been saved!</p>
</div>
<script type='text/html-tpl'  id='overviewPrint'>
<div class="text-right hideFromPrint">
    <a onclick="window.print(); return false;" href='#' 
       class='printBtn text-nodeco'>Print</a>
</div>
</script>
<script>
function hide_me(row_id){            
    //alert(row_id);
    $('#'+row_id).fadeOut();    
    setTimeout(function(){
       var rowCount = '';
       rowCount = $('#alertTable >tbody >tr:visible').length;       
       if(rowCount == null || rowCount == 0){
            $('#alertTable').hide();  
       }else{
        $('#alertTable').show();
       } 
    },500);                
}


$(document).ready(function(){
	$("#deletecustomer").click(function(){
		var memberno=<?php echo $member->getMembershipNumber(); ?>;
		//var temp = $( "#dialog" ).dialog( "open" );
		//console.log(temp);
		document.getElementById("warningtext2").innerHTML = "Are you sure you want to delete this member?";
		$( "#deletedialog" ).dialog({
			width: 400,
			buttons: [
				{
					text: "Yes",
					click: function() {
						$( this ).dialog( "close" );
						$.ajax({
							url:'/xapp/customers/deletemember/'+memberno,
							success:function(response){
								document.getElementById("warningtext2").innerHTML = "Customer has been deleted successfully!!";
								$( "#deletedialog" ).dialog( "open" );
								//window.location.href="/xapp/customers";
									window.setTimeout(function () {
										window.location = "/xapp/customers";		
									},500);
								}
						});
						//window.setTimeout(function () {
							/*window.location = "/xapp/classes/delete/" + classid;*/		
							//},500);
						//console.log('ok');
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
		/*if(confirm("Are you sure you want to delete this member?")){
			console.log('ok');
			$.ajax({
				url:'/xapp/customers/deletemember/'+memberno,
				success:function(response){
					alert('Customer has been deleted successfully!!');
					window.location.href="/xapp/customers";
				}
			});
		}*/
		return false;
	});
	$("#resendsignup").click(function(){
		var memberno=<?php echo $member->getMembershipNumber(); ?>;
		$.ajax({
				url:'/xapp/customers/resendSignup',
				data:{member_id:memberno},
				success:function(response){
					alert('Signup Email has been sent!!');
				}
			});
		return false;
	});
	//$("#breadcrumbsRigh").css("position", "absolute");
   //alert("Runs!");
   $("tr[class^='obilrow']:gt(2)").hide();
   $("tr[class^='tbLastRow']:gt(0)").hide();
   var numOfBillingRows = $("tr[class^='obilrow']").length;//number of rows in addition to the 3
   //alert(numOfBillingRows);
   if (numOfBillingRows < 7) $(".tbLastRow").hide();
   //alert(numOfBillingRows);
   var sp = (numOfBillingRows / 2);
   sp = sp - 1;
   $("tr[class^='obilrow']:gt(" + sp + ")").show();
   $("a#showmorevisits").click(function(){
		var nextpage=parseInt($(this).attr('data-page'));
		console.log(nextpage);
		//call 
		$.ajax({
			url:'/xapp/visit/<?php echo $member->getMembershipNumber(); ?>/'+nextpage,
			success:function(response){
				if(response.success){
					nextpage+=1;
					$.each(response.data,function(key,val){
						var trstring='<tr>';
						trstring+='<td>'+val.date_stamp+'</td>';
						trstring+='<td>'+val.start_time+'</td>';
						trstring+='<td>'+val.end_time+'</td>';
						trstring+='<td>'+val.duration+'</td>';
						trstring+='<td style="width: 10%;"><a href="#" class="deletevisit" rel="'+val.id+'" >Delete</a></td>';
						$("#visitpagination").before(trstring);
					});
					$("a#showmorevisits").attr('data-page',nextpage);
				}
				else {
					$("#visitpagination").hide();
				}
			}
		});
		//
		return false;
   });
   $(".recentvisit").on("click","a.deletevisit",function(){
		var visitid=$(this).attr('rel');
		var X=$(this);
		var memberno=<?php echo $member->getMembershipNumber(); ?>;
		if(confirm("Are you sure you want to delete this visit?")){
			$.ajax({
				url:'/xapp/visitdelete/'+memberno+'/'+visitid,
				success:function(response){
					if(response.success){
						X.parent().parent().remove();
						alert('Visit has been deleted');
					}
				}
			});
		}
		return false;
   });
   var rowCount = $('#alertTable >tbody >tr').length;
   if(rowCount == null || rowCount == 0){
        $('#alertTable').hide();  
   }else{
    $('#alertTable').show();
   }    
});
function showAllRows () {
    $("tr[class^='obilrow']:gt(2)").show();
    $(".tbLastRow").hide();
    return false;
}
/*function myConfirm (str, class_id) {
	classid = class_id;
	var temp = $( "#dialog" ).dialog( "open" );
	document.getElementById("warningtext").innerHTML = str;
	//alert(temp);
	return false;
}*/
</script>
<?php //print_R($member); ?>