<?php 
$tabs = [
    ['id' => 'overview', 'title' => 'Overview', 'info' => '', 
        'tpl' => 'overview'],
    ['id' => 'membership', 'title' => 'Edit Membership Status', 'info' => '',
        'tpl' => 'form-membership'],
    ['id' => 'contact', 'title' => 'Edit Contact', 
        'info' => '',
        'tpl' => ['form-contact', 'form-econtact']],
    ['id' => 'dl', 'title' => 'Edit Drivers License', 
        'info' => '',
        'tpl' => 'form-drivers-license'],
    ['id' => 'image', 'title' => 'Edit Image', 'info' => '',
        'tpl' => 'form-image'],
    ['id' => 'billing', 'title' => 'Add Billing', 'info' => '',
        'tpl' => 'form-billing'],
    ['id' => 'additional', 'title' => 'Edit Additional Info', 
        'info' => '',
        'tpl' =>'form-additional-info'],
    ['id' => 'comments', 'title' => 'Edit Comments', 
        'info' => '',
        'tpl' => 'form-comments'],
    ['id' => 'printid', 'title' => 'Print ID Card', 
        'info' => '', 
        'tpl' => 'form-printid'], 
	['id' => 'benefits', 'title' => 'Redeem Benefits', 
        'info' => '', 
        'tpl' => 'form-benefits'],
    
] 
?>
<script>

$(function() {
    $("#membership-link,#contact-link, #dl-link, #image-link, #billing-link, #additional-link, #comments-link, #printid-link, #benefits-link").click(function () {
    
    	$("#prevnext").hide();
    
    });
    
    $("#overview-link").click(function () {
    
    	$("#prevnext").show();
    
    });
	$('#billing-link').click(function (){
		$('#billingPaymentType,#payment_for').val('');
	});
  });

</script>
<?php
$paymentTypesjson=array();
foreach($payment_types as $item){
	$paymentTypesjson[$item['type_title']]=$item['type_title'];
}
?>
<script>
    ranger.members.edit = {};
    ranger.members.edit.bag = {
        isFormUpdated: false,
        urlPrefix: '<?php echo $app['get_range_url']('customers/')?>',
        billingList: <?php echo json_encode($values['billingList']);?>,
        paymentTypes: <?php echo json_encode($paymentTypesjson);?>,
        listRactive: null,
        addBillListRactive: null,
        webcamSwfFile: '<?php echo $view['assets']->getUrl('js/vendor/webcam/jscam.swf');?>',
        webcamSaveUrl: '<?php echo $app['get_range_url']('customers/saveWebcamImage/'.$member->getMembershipNumber()); ?>',
    };
    
    $(function(){
        var rme = ranger.members.edit;
        rc.makeTabs('tabs', rme.tabClicked);
        var ractive = new Ractive({
            el: '#overviewBillingTbl ',
            template: '#billingRow',
            data: {
                list: rme.bag.billingList
            }
        });
        rme.bag.listRactive = ractive;
                       
        var ractive2 = new Ractive({
            el: '#billingListOnAdd ',
            template: '#billingRow',
            data: {
                list:rme.bag.billingList
            }            
        });
        rme.bag.addBillListRactive = ractive2;
        
        
        rme.imageUpload(
            '#customer-image-input',  
            '#customer-image-progress > div', 
            '#customer-image-preview');
        rme.startCam = function(){
            rme.clearCam();
            rme.webcamLoad('#customer-image-webcam .shotarea', rme.bag.webcamSwfFile);
            $('#customer-image-webcam .shotmenu').show();
        };
        rme.clearCam = function(){
            $('#customer-image-webcam .shotarea').html('');
            $('#customer-image-webcam .shotmenu').hide();
        };
        rme.showPrintBtn();
    });
    
    
</script>

<script src="<?php echo $view['assets']->getUrl('js/ranger.customer.js'); ?>"></script>
<script src="<?php echo $view['assets']->getUrl('js/vendor/webcam/jquery.webcam.min.js')?>"></script>
<script src="<?php echo $view['assets']->getUrl('js/pdfjs/build/pdf.js'); ?>"></script>
<script src="<?php echo $view['assets']->getUrl('js/pdfjs/web/compatibility.js'); ?>"></script>
 <style>
     #tabs ul {
         margin:0;padding:0px;
         
     }
     #tabs li {
         list-style: none;
        padding:0.3em 1em;
        background: #666;
        
        border-top: 0.2em solid #666;
         border-bottom:0.2em solid #666;
     }
     #tabs li.activetab {
         background: #1e1e1e;
         border-left:0.5em solid #666;
         border-top:0.2em solid #666;
         border-bottom:0.2em solid #666;
         padding-left:0.5em;
     }
</style>
<div class="clear">&nbsp;</div>
<div id="prevnext" style="position:relative;top:-15px;float:right;padding-right:5px;"><?php if ($mem_no != 500 && $mem_no != 10000) echo ('<a href="/xapp/customers/' . ($mem_no - 1) . '"><-Previous</a>'); ?> &nbsp; &nbsp; <?php if ($mem_no != 559 && $mem_no != 12000) echo ('<a href="/xapp/customers/' . ($mem_no + 1) . '">Next-></a>'); ?></div>
<div class="margt" style="margin-top:2em">
    <div id="tabs">
        <div class="grid_3 hideFromPrint">
            <ul class='tablist' >
                <?php foreach($tabs as $ind => $tab) : ?>
                <li class="<?php echo $ind == 0 ? 'activetab' : ''?>" >
                    <a href="#<?php echo $tab['id']; ?>" id="<?php echo $tab['id'] . "-link"; ?>"><?php echo $tab['title']; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="grid_9 ">
            
            <?php foreach($tabs as $ind=> $tab) : ?>
            <div id="<?php echo $tab['id']; ?>" class='tabitem'
                 style="<?php echo $ind == 0 ? '' : 'display:none;'?>">
                <div class="margl-sm">
                <?php echo $tab['info'] . (empty($tab['info']) ? '' : '<br /><br />'); 
                if (!empty($tab['tpl'])) {
                    $views = is_array($tab['tpl']) ? $tab['tpl'] : array($tab['tpl']);
                    foreach($views as $viewname){
						//echo $viewname."<br/>";
                        echo '<div class="margb">';
                        if ($viewname == 'overview') {
							$visits=array(
								'lastvisits'=>$lastvisits,
								'totalvisitcounter'=>$totalvisitcounter,
								'totalsection'=>$totalsection
								);
                            echo $view->render("range/customer/$viewname.php", compact('member')+$values+$benVariables+$visits);
							//echo $view->render("range/customer/$viewname.php", array('member'=>$member,'lastvisits'=>$lastvisits,'overview_benefits'=>$benVariables));
                        }
						else if($viewname=='form-billing'){
							 echo $view->render("range/customer/$viewname.php",array('payment_types'=>$payment_types,'payment_for'=>$payment_for));
						}
						else {
                         echo $view->render("range/customer/$viewname.php", 
                                compact('member')+$values+$benVariables);
                        }
                        echo '</div>';
                    }
                }
                
                if ((stristr($tab['title'], 'Edit') !== false
                        || stristr($tab['title'], 'Add') !== false) 
                        && stristr($tab['title'], 'Image') === false ) {
                    ?>
                    <div class="text-center">
                        <button onclick="ranger.members.edit.saveData('<?php 
                        echo $tab['id'];?>', this)">Save</button> 
                        
                    </div> 
                    <?php  if (stristr($tab['title'], 'Add Billing') !== false) { ?>
                    <div id="billingListOnAdd" class="margt"></div>
                    <?php } ?>
                    <?php 
                }                
                ?>
                    <input  type="hidden" id="membershipNo" 
                           value="<?php echo $member->getMembershipNumber(); ?>" />
                </div>
            </div>
            <?php endforeach; ?>
            
        </div>
    </div>
    <div class="clear">&nbsp;</div>
</div>

<script type="text/ractive" id="billingRow">
    <table width="100%" align="center" id=""
       cellpadding="5" border="1" cellspacing='0'>
    <thead>
            <tr>
                <td style="width:50px;">Date</td>
                <td style="width:50px;" nowrap="0">Type</td>
                <td style="width:50px;">Amount</td>
				<td style="width:50px;">Emp.</td>
				<td style="width:50px;" nowrap="0">Paid For</td>
                <td style="width:50px;"></td>
                <td style="width:50px;"></td>
            </tr>
        </thead>
<tbody>

    {{#list:i}}
	
    <tr class="obilrow{{id}}" id="{{id}}">
        <td>{{date}}</td>
        <td>{{payment_method}}</td>
        <td>${{payment_amount}}</td>
		<td>{{employee_name}}</td>
		<td>{{payment_for}}</td>
        <td><a href="" onclick="ranger.members.edit.editBilling('{{membership_number}}','{{id}}',this,{{i}}); return false;">Edit</a>
        </td>
        <td><a href="" onclick="ranger.members.edit.deleteBilling('{{membership_number}}','{{id}}',this, {{i}}); return false;">Delete</a>
        </td>
    </tr>      
    {{else}}
        <tr class="norecords"><td colspan='7'><font color="red"> <center> No Records Found  </center> </font></td></tr>
        
    {{/list}}
<tr class="tbLastRow"><td colspan="7" style="text-align:center;"><a href="#" onclick="return showAllRows()">View all transactions</a></td></tr>	
    </tbody></table>
</script>

<script type="text/ractive" id="billingEdit">
    
    <table width="100%"  border="0" cellpadding="5">
        
        <tr>
            <td nowrap class="labeltd text-right">Payment Type:</td>
            <td >
                <select id='billingPaymentType'>                    
                    <option value=""></option>
                    {{#paymentTypes_}}
                    <option value="{{key}}" >
                    {{value}}
                    </option>
                    {{/paymentTypes}}
                    
                    </select>
            </td>
        </tr>
        <tr>
            <td class="labeltd text-right" >Amount:</td>
            <td ><input type="text" value="{{payment_amount}}"
                        id="billingAmount"
                        style="width:100px"></td>
        </tr>
<tr>
            <td  class="labeltd text-right">Date:</td>
            <td  ><input type="text" 
                         value="{{date}}"
                         id="billingDate"
                         style="width:90px"></td>
        </tr>
    </table>
   
</script>

<script>
</script>


