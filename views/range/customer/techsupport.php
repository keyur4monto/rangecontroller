<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
//include('//includes/browser.php');
//include('includes/browser.php');
include('includes/browser.php');
//include('xapp/includes/browser.php');
$b = new Browser();
$the_browser = $b->getBrowser();
$the_version = $b->getVersion();
$the_platform = $b->getPlatform();
$the_useragent = $b->getUserAgent();
?>
<script>
var xmlHttp = null;
function submitEmail () {
    if (document.getElementById("name").value != '' && document.getElementById("email").value != '' && document.getElementById("comment").value != '') {
    	document.getElementById("dialog").innerHTML = "Your message has been sent! Please check the email you provided!";
    } else {
      document.getElementById("dialog").innerHTML = "Please fill out all the fields!";  
    }
    var myurlstring = "/xapp/sendEmail";
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "POST", myurlstring, false );
    xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    $( "#dialog" ).dialog( "open" );
    $("#shadow").show();
}
</script>
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Email Validation">
	<p id="popupContent">Your message has been sent! Please check the email you provided!</p>
</div>
<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>
<div class="clear"></div>
<div class="grid_12">
    <div class="text-right"> </div>
</div>
<div class="margt"></div>
<div class="grid_12"> Send us a synopsis of the issue. We will respond within 24 hours with a solution. An email will be sent to you for your records.
    <table style="width: 50%;" class="margt">
        <tr>
            <td class="text-right">Your Name:</td>
            <td><input id="name" type="text"></td>
        </tr>
        <tr>
            <td class="text-right">Your Email:</td>
            <td><input id="email" type="text"></td>
        </tr>
        <tr>
            <td class="text-right">Issue:</td>
            <td><textarea id="comment" style="width: 300px; height: 150px;"></textarea></td>
        </tr>
    </table>
</div>
<div class="grid_12"> * all fields required
    <div class="text-center">
        <input type="button" onclick="submitEmail()" value="Send">
        <input type="hidden" name="cmd" value="issuemail" />
    </div>
</div>

<script>
$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				if (document.getElementById("name").value != '' && document.getElementById("email").value != '' && document.getElementById("comment").value != '') { 
					window.location = "/xapp/techSupport";
					xmlHttp.send("name=" + document.getElementById("name").value + "&email=" + document.getElementById("email").value + "&comment=" + document.getElementById("comment").value + "<?php echo('&browser=' . $the_browser . '&version=' . $the_version . '&platform=' . $the_platform . '&useragent=' . $the_useragent); ?>");
				}
				else $("#shadow").hide();
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});
// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});
function myConfirm (str) {
	var temp = $( "#dialog" ).dialog( "open" );
	alert(temp);
	return false;
}
// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);
</script>