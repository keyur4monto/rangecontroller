<table width="100%"  border="1" cellpadding="5" cellspacing="0">
    <tbody>
        <tr>
            
            <td class="text-center" style="width:1%;">
                <img id='customer-image-preview' 
                    src="<?php 
                    echo $app['range_member_image_url']($member->getMembershipNumber());?>"
                     style="width:150px; height:auto"
                     />
                <br />
                <a href="<?php echo $app['get_range_url']('customers/downloadImage/'. 
                        $member->getMembershipNumber()); ?>">Download Image</a>
            </td>
            <td class="text-center">
                <div>Upload a photo from computer<br />
                    (jpg, jpeg, gif, and png formats supported)
                    <br />
                    <input type="file" name="file"
                           id='customer-image-input'
                           value="Upload" 
                           data-url="<?php 
                           echo $app['get_range_url']('customers/uploadImage/' . 
                                   $member->getMembershipNumber()); ?>"/>
                    <div style="background:white;width:62%;margin:0.32em auto;display:none;"
                         id="customer-image-progress">
                        <div style="background:#0f0;width:0%;height:1em"></div>
                    </div>
                </div>
                <div class="margt-sm">Take a photo via webcam
                    <br />
                    <button onclick='ranger.members.edit.startCam()'>Take Photo</button>
                    
                    <div id='customer-image-webcam'>
                        <div class="shotarea" style="margin:0.38em auto"></div>
                        <div class="shotmenu" style="display:none;margin-bottom:0.38em">                            
                            <button onclick="ranger.members.edit.webcamCaptureSave()"
                                    class="captureBtn" >Capture</button> 
                            <a onclick='ranger.members.edit.clearCam(); return false;' style='padding-left:1em' href='#'>Clear</a>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
