<script>
    $(function(){
        $('#phone1, #phone2').mask('(000)-000-0000');       
        $('#zipcode').mask('00000-0000');    
        $('#ssn').mask('000-00-0000');
    });
</script>

<table width="100%" align="center" cellpadding="5" cellspacing="0" border="1">
    <tbody>
    <tr>
        <td class="labeltd"><span class="requiredf">*</span>First Name:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($firstName)?>"                    
                   name="firstName"
                   id="firstName"
                   style="width:140px"></td>
    </tr>
    <tr>
        <td class="labeltd">Middle Name:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($middleName)?>"                    
                   name="middleName"
                   id="middleName"
                   style="width:140px"></td>
    </tr>
    <tr>
        <td align="right"><span class="requiredf">*</span>Last Name:</td>
        <td><input type="text"
                   value="<?php echo $view->escape($lastName)?>"                    
                   name="lastName"
                   id="lastName"
                   style="width:140px"></td>
    </tr>
    <tr>
        <td align="right"><span class="requiredf">*</span>Address:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($street)?>"                    
                   name="street"
                   id="street"
                   style="width:200px"></td>
    </tr>
    <tr>
        <td align="right"><span class="requiredf">*</span>City:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($city)?>"                    
                   name="city"
                   id="city"
                   style="width:120px"></td>
    </tr>
    <tr>
        <td align="right"><span class="requiredf">*</span>State:</td>
        <td>
            <select name="state" id="state">
                <option value=""></option>
                <?php foreach($stateList as $code => $name) : ?>
                <option value="<?php echo $code; ?>"
                        <?php echo $code == $state ? 'selected' : ''; ?>>
                    <?php echo $name;?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td align="right"><span class="requiredf">*</span>Zip Code:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($zipcode)?>"                    
                   name="zipcode"
                   id="zipcode"
                   style="width:80px"></td>
    </tr>
    <tr>
        <td align="right">SSN#:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($ssn)?>"                    
                   name="ssn"
                   id="ssn"
                   style="width:120px"></td>
    </tr>
    <tr>
        <td align="right"><span class="requiredf">*</span>Phone 1 #:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($phone1)?>"                    
                   name="phone1"
                   id="phone1"
                   style="width:100px"></td>
    </tr>
    <tr>
        <td align="right">Phone 2 #:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($phone2)?>"                    
                   name="phone2"
                   id="phone2"
                   style="width:100px"></td>
    </tr>
    <tr>
        <td align="right"><span class="requiredf">*</span>Email:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($email)?>"                    
                   name="email"
                   id="email"
                   style="width:200px"></td>
    </tr>
		</tr>
    <tr>
        <td align="right"><span class="requiredf">*</span>Email Status:</td>
        <td><select name="email_status" id="email_status">
			<option value="subscribe" <?php echo ($view->escape($email_status) == "subscribe") ? 'selected=""' : ''; ?>>Subscribe</option>
			<option value="unsubscribe" <?php echo ($view->escape($email_status) == "unsubscribe") ? 'selected=""' : ''; ?>>Unsubscribe</option>
			</select></td>
    </tr>
    </tbody>
</table>

