
<div class="container_12 benefits">
          <div class="grid_9">
            <div class="breadcrumbs"> <a href="#">Home </a> / <a href="#">Module Configurations </a> / <span>Benefits </span> </div>
          </div>
          <div class="grid_3" id="breadcrumbsRight"></div>
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"><span style="color: red;">Select Membership Name:</span>
              <select>
                <option></option>
                <option>Keystone Platinum</option>
                <option>Platinum</option>
                <option>Gold</option>
                <option>Silver</option>
                <option></option>
              </select>
            </div>
          </div>
          <div class="margt"></div>
          <div id="Keystone">
            <div class="grid_9 margt">
              <table>
                <tr>
                  <th>Quantity</th>
                  <th>Duration</th>
                  <th>Keystone Platinum Benefits</th>
                </tr>
                <tr>
                  <td><input type="text" style="width:50px;" value="16"></td>
                  <td><select>
                      <option></option>
                      <option>Expiration</option>
                      <option>Yearly</option>
                      <option>Monthly</option>
                      <option>Weekly</option>
                      <option>Daily</option>
                    </select></td>
                  <td><input type="text" style="width:450px;" value="Guest passes per year (Same lane as Member)"></td>
                </tr>
                <tr>
                  <td><input type="text" style="width:50px;" value="6"></td>
                  <td><select>
                      <option></option>
                      <option>Expiration</option>
                      <option>Yearly</option>
                      <option>Monthly</option>
                      <option>Weekly</option>
                      <option>Daily</option>
                    </select></td>
                  <td><input type="text" style="width:450px;" value="Free Firearm Transfers per year"></td>
                </tr>
                <tr>
                  <td><input type="text" style="width:50px;" value="4"></td>
                  <td><select>
                      <option></option>
                      <option>Expiration</option>
                      <option>Yearly</option>
                      <option>Monthly</option>
                      <option>Weekly</option>
                      <option>Daily</option>
                    </select></td>
                  <td><input type="text" style="width:450px;" value="Free Firearm Rentals per year"></td>
                </tr>
                <tr>
                  <td><input type="text" style="width:50px;" value="2"></td>
                  <td><select>
                      <option></option>
                      <option>Expiration</option>
                      <option>Yearly</option>
                      <option>Monthly</option>
                      <option>Weekly</option>
                      <option>Daily</option>
                    </select></td>
                  <td><input type="text" style="width:450px;" value="Free Machine Gun rentals per year"></td>
                </tr>
                <tr>
                  <td><input type="text" style="width:50px;" value=""></td>
                  <td><select>
                      <option></option>
                      <option>Expiration</option>
                      <option>Yearly</option>
                      <option>Monthly</option>
                      <option>Weekly</option>
                      <option>Daily</option>
                    </select></td>
                  <td><input type="text" style="width:450px;" value=""></td>
                </tr>
                <tr>
                  <td><input type="text" style="width:50px;" value=""></td>
                  <td><select>
                      <option></option>
                      <option>Expiration</option>
                      <option>Yearly</option>
                      <option>Monthly</option>
                      <option>Weekly</option>
                      <option>Daily</option>
                    </select></td>
                  <td><input type="text" style="width:450px;" value=""></td>
                </tr>
                <tr>
                  <td colspan="3" class="text-center"><a href="" onclick="alert('When you click this link, 3 more table rows with empty inputs will appear')">Add 3 More Rows</a></td>
                </tr>
              </table>
            </div>
            <div class="grid_3 margt">
              <table class="benefits_duration">
                <tr>
                  <th colspan="2">Duration Key:</th>
                </tr>
                <tr>
                  <td>Expiration</td>
                  <td>Resets on Membership Expiration</td>
                </tr>
                <tr>
                  <td>Yearly</td>
                  <td>Resets on 1st of Year</td>
                </tr>
                <tr>
                  <td>Monthly</td>
                  <td>Resets on 1st of Month</td>
                </tr>
                <tr>
                  <td>Weekly</td>
                  <td>Resets on Sunday</td>
                </tr>
                <tr>
                  <td>Daily</td>
                  <td>Resets at midnight</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="grid_12 text-center">
            <input type="button" value="Save" onclick="alert('Confirmation Alert of saved changes')">
          </div>
        </div>
        