<script>
    $(function(){
        $('#billingDate').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });  
	
    });
	function billingPaymentType(paymenttype)
	{
		if(paymenttype == "-1")
		{
			window.location.href = "/xapp/config/billing/paymentType/add";
		}
		else
		{
			return false;
		}
	}
	function payment_for(paymentfor)
	{
		if(paymentfor == "-1")
		{
			window.location.href = "/xapp/config/billing/paymentFor/add";
		}
		else
		{
			return false;
		}
	}
</script>
<style>
    /*#billingListOnAdd .norecords {display: none}*/
	 #billingListOnAdd .norecords {}
</style>
<table width="100%"  border="1" cellpadding="5" cellspacing="0">
    <tr>
        <td  class="labeltd">Date:</td>
        <td  ><input type="text" 
                     value="<?php echo date('n/j/Y'); ?>"
                     id="billingDate"
                     style="width:90px"></td>
    </tr>
    <tr>
        <td nowrap class="labeltd">Payment Type:</td>
        <td >
            <select id='billingPaymentType' onchange="billingPaymentType(this.value)">                    
                <option value=""></option>
                <?php foreach($payment_types as $code) : ?>
                <option value="<?php echo $code['type_title']; ?>" >
                   <?php echo $code['type_title']; ?>
                </option>
                <?php endforeach; ?>
				 <option value="-1">Or Add Payment Type...</option>
                </select>
        </td>
        </tr>
        <tr>
        <td nowrap class="labeltd">Payment For:</td>
        <td >
            <select id='payment_for' onchange="payment_for(this.value)">
            	<option value=""></option>                    
               <?php foreach($payment_for as $code) : ?>
                <option value="<?php echo $code['for_title']; ?>" >
                   <?php echo $code['for_title']; ?>
                </option>
                <?php endforeach; ?>
				<option value="-1">Or Add Payment For...</option>
                </select>
        </td>
    </tr>
    <tr>
        <td class="labeltd" >Amount:</td>
        <td ><input type="text" 
                    id="billingAmount"
                    style="width:100px"></td>
    </tr>
</table>