<?php 
/*echo '<pre>';
print_r($loginRangeUrlPrefix);
echo '</pre>';*/

?>
<script>
    ranger.members.bag = {
        totalItems: <?php echo $totalMembers ?: 0; ?>,
        urlPrefix: '<?php echo $app['get_range_url']('customers/')?>',
        members: <?php echo json_encode($members); ?>,
        pageSize: <?php echo $pageSize; ?>,
        scheduleUrlPrefix: '<?php echo $scheduleUrlPrefix?>',
        loginRangeUrlPrefix: '<?php echo $loginRangeUrlPrefix?>',
        logoutRangeUrlPrefix: '<?php echo $logoutRangeUrlPrefix?>',
        
    };
    
    ranger.members.getMemberFilters = function(){
		s=$.trim($('#filterValue').val());
		type=$("#filterType").val();
		if(type=='membershipNumber'){			
			s = s.replace(/^0+/, '');	
			var maxlength=ranger.members.bag.members[0].membership_number.length;
			if(s.length > maxlength)
				s=s.slice(0,maxlength-s.length);
			$('#filterValue').val(s);
		}
        var obj =  {
            filterType: $('#filterType').val(),
            //filterValue: $.trim($('#filterValue').val())
			filterValue: s
        };
        if (obj.filterValue == '') {
            return {};
        }
        return obj;        
    };
    
    ranger.members.loadMembers = function(pageNumber, cb){
        var filters = ranger.members.getMemberFilters();
        filters.currentPage = pageNumber;
        $.ajax({
            url: ranger.members.bag.urlPrefix + 'getMembersJson',
            type: 'POST',
            dataType: 'json',
            data: (filters)
        }).done(function(data){
            ranger.members.bag.members = data.records;
            ranger.members.bag.totalItems = data.totalRecords;
            ranger.members.renderResults();
            cb();
        }).fail(function(){
        });
    };
    ranger.members.membersListPageChanged = function(pageNumber, event){
        ranger.members.loadMembers(pageNumber, function(){
            //$("#pagerBottom").pagination('selectPage', pageNumber);
        });
    };
    
    ranger.members.filterPerformed = function(){
        ranger.members.loadMembers(1, function(){
            $("#pagerBottom").pagination('destroy');
            ranger.members.renderPager(ranger.members.bag.totalItems);
        });        
    };
    
    ranger.members.renderResults = function(){
        
        var data = {
            members: ranger.members.bag.members,
            urlPrefix: ranger.members.bag.urlPrefix,
            scheduleUrlPrefix: ranger.members.bag.scheduleUrlPrefix,
            loginRangeUrlPrefix: ranger.members.bag.loginRangeUrlPrefix,
            logoutRangeUrlPrefix: ranger.members.bag.logoutRangeUrlPrefix
        };
        var html = new EJS({element:'membersTpl'}).render(data);       
        $('#membersTbl tbody').html(html);
    };
    
    ranger.members.renderPager = function(totalItems){
        $('#pagerBottom').pagination({
            items: totalItems,
            itemsOnPage: ranger.members.bag.pageSize,
            displayedPages: 5,
            currentPage: 1, //which page to select after init
            selectOnClick: true, //  dont select page after it is clicked
            onPageClick: ranger.members.membersListPageChanged
        });
    };
    
    ranger.members.logoutClicked = function(elm, ind){
        var mem = ranger.members.bag.members[ind];
        var msg = " Are you sure you want to logout member " + 
                " \n\n"+ mem['membership_number'] + ' ' + 
                mem['first_name'] + ' ' + mem['last_name'] + "\n\n" + 
                " This action is immediate and permanent ";
        if (confirm(msg)) {
            return true;
        }
        return false;
    };
    
    $(function(){        
        ranger.members.renderResults();
        ranger.members.renderPager(ranger.members.bag.totalItems);
        $('#filterValue').keyup(ranger.members.filterPerformed);
    });
</script>
<div class="grid_12 ">
    <div class="text-right">
        <!--<button onclick="window.location='<?php //echo $addMemberUrl ?>';"
                >Add New Member </button>-->
         <input type="button" value="Add New Member" onclick="window.location=' <?php echo $addMemberUrl ?>';">     
                
    </div>
    <div class="text-center padtb padlr margt lightblackbg" >
        Filter members by: <select id="filterType">     	
             <option value="membershipNumber" <?php echo ($getfilterTypes['filtertypes']== "membershipNumber") ? 'selected=""' : ''; ?>	>Member Number</option>
             <option value="lastName" <?php echo ($getfilterTypes['filtertypes']== "lastName") ? 'selected=""' : ''; ?>	>Last Name</option>
            <option value="firstName" <?php echo ($getfilterTypes['filtertypes']== "firstName") ? 'selected=""' : ''; ?>	>First Name</option>
            <option value="phoneNumber" <?php echo ($getfilterTypes['filtertypes']== "phoneNumber") ? 'selected=""' : ''; ?>	>Phone Number</option>
        </select> 
        <input type="text" id="filterValue" value='' />
        <!--button onclick="ranger.members.filterPerformed()" >Go</button-->
        
    </div>
    <?php if ($lastManagedMember) : ?>
    <div class="margt-sm padlr text-right">Recently visited member: 
        <a href="<?php 
            echo $app['get_range_url']('customers/' . 
                    trim($lastManagedMember->getMembershipNumber()));
        ?>" ><?php 
        echo $lastManagedMember->getFullName();
        ?></a></div>
    <?php endif; ?>

    <table class="margt" border="1" cellspacing="0" id='membersTbl'>
        <thead>
            <tr style="font-size: 14px;">
                <th nowrap="">#</th>
				<th nowrap="">Membership</th>
                <th nowrap="">Name</th>
                <th nowrap="">Address</th>
                <th nowrap="">Phone</th>
                <th colspan="3">Actions</th>
                
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <div class="text-center" id="pagerBottom"></div>    
</div>
<div class="clear">&nbsp;</div>

<script type="text/ejs-template"  id="membersTpl">
[% if(members.length == 0)  { %]
	<tr>
	<td colspan="7"> <center><font color="red"> No Records Found </font></center> </td>
	</tr>
[% } else  { %]
   [% for (var i = 0 ; i < members.length; i++)  {  %]
   [% var mem = members[i]; %]
    <tr style="font-size: 14px;">
        <td nowrap>[%=mem['membership_number'] %]</td>
		<td>[%=mem['membership_type'] %]</td>
        <td nowrap="">[%= mem['first_name'] + ' ' + mem['middle_name'] + ' ' +  mem['last_name'] %]</td>
        <td style="font-size: 13px;" nowrap="">[%= mem['street_address'] %]</td>
        <td nowrap=""> [%= mem['home_phone'] %]</td>
        <td nowrap=""><a href="[%= urlPrefix + '' + mem['membership_number'] %]">Manage</a></td>
        <td nowrap=""><!--<a 
            href="[%=scheduleUrlPrefix + '&memberNo='+ mem['membership_number']%]">Schedule</a>-->
            <a href="rcScheduler">Schedule</a></td>
        <!--<td nowrap="">
            [% if (parseInt(mem['_is_logged_in'])) { %]
            <a href="/xapp/customers/lane/logThemOut/[%=mem['membership_number']%]"
                onclick="return ranger.members.logoutClicked(this,[%=i%])" >
                Log Out</a>
            [% } else  { %]
                 <a href="/xapp/customers/login/SelectLane/[%=mem['membership_number']%]"
                 >
                Log In</a>
            [% } %]
        </td>-->
    </tr>
    [% } %]
[% } %]
</script> 

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script>
$(document).ready(function(){
	$('#filterType').change(function() {
		var filterType = $(this).val();
		
		$.ajax({
				url:'customers/filterTypes',
				data:'&filterType=' + filterType,
				dataType: 'json',
				type:'post',  
				success:function(response)
				{
					 //alert();
			    }
			})
	})
	
	/*$("#filterValue").change(function(){
		var s = $(this).val();
		s = s.replace(/^0+/, '');
		$(this).val(s);
		ranger.members.filterPerformed();
	});*/
});
</script>