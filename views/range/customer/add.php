<?php ini_set('display_errors', 1); ?>

<script>

	var YoN = "?";

function checkForDuplicates () {

	var xmlHttp = null;

	var fname = document.getElementById("firstName");
	
	var lname = document.getElementById("lastName");
	
	var address = document.getElementById("street");
	
	var myurlstring = "/xapp/checkForDuplicateMember/" + fname.value + "/" + lname.value + "/" + address.value;

    xmlHttp = new XMLHttpRequest(); 
    xmlHttp.open( "GET", myurlstring, false );
    xmlHttp.send( null );
    //alert(xmlHttp.responseText);

//do ajax query here

//let the php return an object detailing whether the full name or the street address was the problem

	if (xmlHttp.responseText == "name and address") {
	
		valid = false;
	
		var errmsg = "A user with that " + xmlHttp.responseText + " already exists in our database.";
		
		errmsg = errmsg + " Do you want to proceed anyway?";
		
		if (document.getElementById("employee_id").value == "") errmsg = "Please select an employee to continue!";
		
		document.getElementById("dialtext").innerHTML = errmsg;
		
		dialogBox();
		
		//if (confirm(errmsg + "Do you want to proceed anyway?")) return true;
		
		//$("#popupdiv").show();
		
		//while (YoN == '?') {alert("sss")}
		
		//if (YoN == 'Y') return true;
		 
		//else return false;
	
	}
	
	//else return true;
	
	else {
	
			if (document.getElementById("employee_id").value != "") {
			
				valid = true;
				var val = document.getElementById("employee_id").value;
				var extraElement = document.getElementById("employee_id");
				var eCopy = extraElement.cloneNode(true);
				eCopy.style.display = "none";
				eCopy.value = val;
				document.forms["regform"].appendChild(eCopy);
				document.forms["regform"].submit();
				
				}
				
			else {
			
				valid = false;
	
				var errmsg = "Please select an employee to complete this registration!";
		
				document.getElementById("dialtext").innerHTML = errmsg;
		
				dialogBox();
				
				}
				
		}

}


function hideDivandReset() {
	
$("#popupdiv").hide();
	
window.location = "/xapp/classes";

}

function hideDiv () {

$("#popupdiv").hide();

}

var valid = true;

function myValidation () {

	return valid;

}

</script>
<div class="margt">
    <?php if ($errorMessage): ?>
        <div class="grid_12 margb">
			<?php echo $view->render('range/error-message.php', ['message'=>$errorMessage]);?>
        </div><div class="clear">&nbsp;</div>
    <?php endif; ?>
    <form id="regform" method="post" action="" onsubmit="return myValidation()">
        <div class="grid_6 ">
            <?php  echo $view->render('range/customer/form-contact.php', $contactData); ?>

        </div>
        <div class="grid_6">
            <?php  echo $view->render('range/customer/form-econtact.php', $emergencyData); ?>
            <?php  echo $view->render('range/customer/form-membership.php', 
                    $membershipTypeData); ?>

        </div>
        <div class="clear">&nbsp;</div>
        <div class="grid_12"><span class="requiredf">*</span> denotes required fields</div>
        <div class="text-center">
            <input type="hidden" name="cmd" value="add" />
            <input type="hidden" name="<?php echo $tokenName; ?>" 
                   value="<?php echo $tokenValue; ?>" />
            
            <input type="submit" value="Add Member" onclick="checkForDuplicates()" />
        </div>
    </form>
</div>

<script>
 
function Yes () {YoN ='Y';}

</script>

<div id="popupdiv" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable" tabindex="-1" role="dialog" aria-describedby="elm09454444972798228" aria-labelledby="ui-id-1" style="position: absolute; height: auto; width: 300px; top: 232.5px; left: 528.5px; display: block; z-index: 101;"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle"><span id="ui-id-1" class="ui-dialog-title">&nbsp;</span><button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" title="Close"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">Close</span></button></div><div id="elm09454444972798228" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 1px; max-height: none; height: auto;"><span style="color:#0f0"></span><span id="msg">&nbsp;Successfully updated</span></div><div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><div class="ui-dialog-buttonset"><button type="button" onclick="Yes()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text" onclick="Yes()">Ok</span></button> <button type="button" onclick="hideDivandReset()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">Cancel</span></button></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-sw" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-ne" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-nw" style="z-index: 90;"></div></div>

<script>$("#popupdiv").hide();</script>





<!-- ui-dialog -->
<div id="dialog" title="Edit Confirmation">
	<p id="dialtext">Your changes have been saved!</p>
</div>
<script>
$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				if (document.getElementById("employee_id").value != "") valid = true;
				var extraElement = document.getElementById("employee_id");
				//var eCopy = extraElement.cloneNode(true);
				//extraElement.style.display = "none";
				if (document.getElementById("employee_id").value != "") document.forms["regform"].appendChild(extraElement);
				if (document.getElementById("employee_id").value != "") document.forms["regform"].submit();
				$( this ).dialog( "close" );
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});

function dialogBox () {

	$( "#dialog" ).dialog( "open" );
	
	}

// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

$( document ).ready(function() {
	
	getEmployees();

	$("#breadcrumbsRigh").show();

});

</script>







