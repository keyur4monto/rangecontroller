<script>
function submitUpdate () {
	if (document.getElementById("employee_id").value != "") {
	for (var i = 1;i <= 9; i++) {
	if(document.getElementById('amount-' + i) != undefined && document.getElementById("amount-" + i).value != 0) {
		var amount = document.getElementById("amount-" + i).value;
		var benefit_id = document.getElementById("benefit-id-" + i).value;
		var comments = document.getElementById("comments-text").value;
		if (comments == '') comments = 'No comments entered.';
		//alert(comments);
		//if (quantity != '') {
		//	alert(quantity); 
			//alert(timeframe);
			//alert(benefit_name);
			//alert(benefit_id);
			var myurlstring = "/xapp/benefits/redeem/<?php echo ($customer_id); ?>/" + benefit_id + "/" + amount + "/" + encodeURI(comments) + "/" + document.getElementById("employee_id").value;
			//alert(myurlstring);
			//alert(comments);
			var xmlHttp = null;
			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "GET", myurlstring, false );
			xmlHttp.send( null );
			//alert(xmlHttp.responseText);
			//ranger.members.edit.showSuccess("Successfully updated!");
			//$("#popupdiv").show();
		//}
	}	
	}
	}
	else {
		//show error message
	}
}
</script>
<div class="container_12 benefits" style="padding-top:0;margin-top:0;"> 
	 <br><div class="grid_12 margb" style="display:none;" id="errid">
        <?php echo($errmsg);?>
        </div><div class="clear">&nbsp;</div>
					<div class="grid_8 margl" style="position:relative;top:-18px;width:inherit !important;padding-top:0;margin-top:0;">
						<table width="100%">
							<tr>
								<th colspan="3"><?php echo ($membershiptypename); ?> Benefits</th>
							</tr>
							<tr>
								<td>Redeem </td>
								<td>Qty </td>
								<td nowrap="">Benefit </td>
							</tr>
							<?php echo ($listOfBenefits); ?>
							<tr>
								<td colspan="3" style="border-bottom: 0px;">Comments:</td>
							</tr>
							<tr>
								<td colspan="3" style="border-top: 0px;"><textarea id="comments-text" style="width: 99%; height: 40px;" placeholder="Enter notes here...."></textarea></td>
							</tr>
						</table>
					</div>
					<div class="clear"></div>
					<div class="grid_9 text-center" style="width:inherit !important;">
						<input id="dialog-link1" type="button" value="Save" onClick="submitUpdate()">
					</div>
					<div class="clear"></div>
					<div class="grid_8 margl margt" style="width:inherit !important;">
						<table width="100%">
							<tr>
								<td>Date </td>
								<td>Qty </td>
								<td>Benefit </td>
								<td>Emp </td>
								<td></td>
							</tr>
							<!--<tr>
								<td>12/4/2014</td>
								<td><span style="color: red;">2</span></td>
								<td>Guest passes per year (Same lane as Member)</td>
								<td><a href="#">Delete</a></td>
							</tr>
							<tr>
								<td colspan="4" style="font-size: 14px;">Notes Here......</td>
							</tr>-->
                            <?php echo $listOfUsedBenefits; ?>
                            <tr id="norecords" <?php echo (empty($listOfUsedBenefits)) ? "" : "style='display:none'"; ?>>
                            	<td colspan="9" style="color:#F00"><center>No Records Found</center></td>
                            </tr>
						</table>
					</div>
					<div class="clear"></div>
				</div>
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupmsg">Your redemptions have been processed successfully! You will now be taken back to the Customer Overview!</p>
</div>
<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>
<script src="external/jquery/jquery.js"></script>
<script src="jquery-ui.js"></script>
<script>
$( "#dialog" ).dialog({
	autoOpen: false,
	modal:true, 
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				if (document.getElementById("employee_id").value != "") window.location = "/xapp/customers/<?php echo ($customer_id); ?>#benefits";
				$("#shadow").hide();
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	],
	close: function() {
		$("#shadow").hide();
	}
});
// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	if (document.getElementById("employee_id").value != "") document.getElementById("popupmsg").innerHTML = "Your redemptions have been processed successfully! You will now be taken back to the Customer Overview!";
	else document.getElementById("popupmsg").innerHTML = "Please select an employee to process this redemption!";
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});
function myConfirm (str) {
	var temp = $( "#dialog" ).dialog( "open" );
	alert(temp);
	return false;
}
// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);
</script>
<script>
//nick's extra jquery
$( document ).ready(function() {
	getEmployees(); //loads employee dropdown
	/*for (var i = 1;i <= 6; i++) {
	$("#fieldno-" + i).show();
	if (<?php echo ($memtypeid); ?> != 0) $("#addrows").show();
	}*/
	if(window.location.hash) {
		//$("#overview").hide();
		//$("#benefits").show();
		//$('a[href$="#benefits"]').click();
  // Fragment exists
	}
	$('#billing-link').on('click', function() {
      $("#breadcrumbsRigh").show();
	});
	$('#benefits-link').on('click', function() {
      $("#breadcrumbsRigh").show();
	});
	$('#overview-link').on('click', function() {
      $("#breadcrumbsRigh").hide();
	});
	$('#membership-link').on('click', function() {
      $("#breadcrumbsRigh").hide();
	});
	$('#contact-link').on('click', function() {
      $("#breadcrumbsRigh").hide();
	});
	$('#dl-link').on('click', function() {
      $("#breadcrumbsRigh").hide();
	});
	$('#image-link').on('click', function() {
      $("#breadcrumbsRigh").hide();
	});
	$('#additional-link').on('click', function() {
      $("#breadcrumbsRigh").hide();
	});
	$('#comments-link').on('click', function() {
      $("#breadcrumbsRigh").hide();
	});
	$('#printid-link').on('click', function() {
      $("#breadcrumbsRigh").hide();
	});
});
function addRows () {
for (var i = 7;i <= 9; i++) {
	$("#fieldno-" + i).show();
	$("#addrows").hide();
	}
}
</script>
<!--now for the delete button -->
<!-- ui-dialog -->
<div id="dialog2" title="Delete Confirmation">
	<p id="warningtext" >Your changes have been saved!</p>
</div>
<script>
var benid = -1;
$( "#dialog2" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Yes",
			click: function() {
				$( this ).dialog( "close" );
				$("#deletebtn-" + benid).html("Deleted!");
				var xmlHttp = null;
				var myurlstring = "/xapp/benefits/unredeem/<?php echo ($customer_id); ?>/" + benid;
				//alert(myurlstring);
				xmlHttp = new XMLHttpRequest();
				xmlHttp.open( "GET", myurlstring, false );
				xmlHttp.send( null );
				window.location = "/xapp/customers/<?php echo ($customer_id); ?>#benefits";
				//window.setTimeout(function () {
					//window.location = "/xapp/classes/delete/" + classid;		
					//},500);
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});
// Link to open the dialog
$( "#dialog-link2" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});
// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
	);
function myConfirm (str, ben_id) {
	benid = ben_id;
	var temp = $( "#dialog2").dialog( "open" );
	document.getElementById("warningtext").innerHTML = str;
	//alert(temp);
	return false;
}
</script>