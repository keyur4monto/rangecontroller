<?php 
//$member = new \Rc\Models\range_member();
?>
<table width="100%"  border="1" cellpadding="5" cellspacing="0">
    <tbody>
    <tr>
        <td  class="labeltd" >Safety Video Completed:</td>
        <td  >
            <select id='safetyVideoCompleted'>                    
                <option value=""></option>
                <?php foreach($yesNo as $code => $text) : ?>
                <option value="<?php echo $code; ?>" 
                        <?php echo trim(strtolower($code)) == 
                        trim(strtolower($member->getCertsSafetyVideoCompleted())) 
                                ? 'selected' : ''; ?> >
                    <?php echo $text;?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td  class="labeltd" nowrap style="width:auto">Safety Range Form Completed:</td>
        <td  >
            <select id='safetyRangeFormCompleted'>                    
                <option value=""></option>
                <?php foreach($yesNo as $code => $text) : ?>
                <option value="<?php echo $code; ?>" 
                        <?php echo trim(strtolower($code)) == 
                        trim(strtolower($member->getCertsSafetyRangeFormCompleted())) 
                                ? 'selected' : ''; ?> >
                    <?php echo $text;?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
</tbody>
</table>

<!--div class="margb">
    Certifications: 
        
        
</div-->
<table width="100%"  border="1" cellpadding="5" cellspacing="0">
    <tbody>
    <tr>
        <td style="border-bottom:0px">
        Certifications: 
        </td>
    </tr>
    <tr>
        <td  style="border-top:0px">
        <textarea style="width:99%;display: block;" 
                  id="certsComments"
                  placeholder="Enter any certificates here..."
                  rows="10"><?php echo $view->escape($member->getCertsComments());?></textarea>
        </td>
    </tr>
    </tbody>
</table>
