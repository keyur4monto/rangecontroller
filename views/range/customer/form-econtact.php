<script>
    $(function(){
        $('#emergencyPhone').mask('(000)-000-0000');        
    });
</script>
<table width="90%" align="center" cellpadding="5" cellspacing="0" border="1">
    <tbody>
    <tr>
        <td class="labeltd">Emergency Name:</td>
        <td><input type="text"
                   value="<?php echo $view->escape($emergencyName)?>"                    
                   name="emergencyName"
                   id ="emergencyName"
                   style="width:180"
                   ></td>
    </tr>
    <tr>
        <td align="right">Emergency Phone:</td>
        <td><input type="text" 
                   value="<?php echo $view->escape($emergencyPhone)?>"                    
                   name="emergencyPhone"
                   id="emergencyPhone"
                   style="width:100px"></td>
    </tr>
    </tbody>
</table>