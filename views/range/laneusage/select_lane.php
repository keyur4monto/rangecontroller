<!DOCTYPE html>
<html>
<head>

    <script>
    $(document).ready(function() {

        getEmployees();

        $("#breadcrumbsRigh").show();

    });
    </script>
    <script>
    function logmeout(memberno) {
        var date = new Date();
        var hours = date.getHours();
        var minutes = '0' + date.getMinutes();
        var seconds = '0' + date.getSeconds();
        var formattedTime = hours + ':' + minutes.substr(minutes.length - 2) + ':' + seconds.substr(seconds.length - 2);
        document.cookie = 'end_time =' + formattedTime;
        window.location.href = "/xapp/customers/lane/logThemOut/" + memberno;
    }

    function loginLane(lane_no) {

        //memno, first name, last name, start time
        //$('.lane_cl').attr("disabled","disabled");
        if(document.getElementById("employee_id").value != ""){
            $('.lane_cl').attr("disabled","disabled");
        }        
        var date = new Date();
        // hours part from the timestamp
        var hours = date.getHours();
        // minutes part from the timestamp
        var minutes = "0" + date.getMinutes();
        // seconds part from the timestamp
        var seconds = "0" + date.getSeconds();

        // will display time in 10:30:23 format
        var formattedTime = hours + ':' + minutes.substr(minutes.length - 2) + ':' + seconds.substr(seconds.length - 2);

        var xmlHttp = null;

        var myurlstring = "/xapp/customers/lane/logThemIn";

        xmlHttp = new XMLHttpRequest();

        xmlHttp.open("POST", myurlstring, true);

        xmlHttp.onload = function(e) {


            //alert(xmlHttp.responseText);

            window.location = "/xapp/currentRangeUsage";


        }

        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        if (document.getElementById("employee_id").value != "") xmlHttp.send("employee_id=" + document.getElementById("employee_id").value + "&member_no=<?php echo($member_no); ?>&lane_no=" + lane_no + "&start_time=" + formattedTime + "&first_name=<?php echo($first_name); ?>&last_name=<?php echo($last_name); ?>");

        else document.getElementById("errid").style.display = "block";


    }
    </script>
    <title></title>
</head>
<body>
    <br>
    <div class="grid_12 margb" id="errid" style="display:none;">
        <?php echo($err); ?>
    </div>
    <div class="clear">
        &nbsp;
    </div>
    <div class="clear"></div>
    <div class="grid_12">
        <div class="text-center">
            <span style="color: red;">Selected Member:
            <?php echo($first_name); ?>
            <?php echo($last_name); ?>(#<?php echo($member_no); ?>)</span><br>
            <br>
            <!--<div>
                Range 1 | <a href="#">Range 2</a> | <a href="#">Range 3</a> |
                <a href="#" title="Add additional range"><i class=
                "fa fa-plus"></i></a>
            </div>-->
        </div>
    </div>
    <div class="margt"></div>
    <div class="grid_12 margt">
        <table class="text-center" style="width: 60%;">
            <tr>
                <th colspan="2">Choose an available lane</th>
            </tr><?php echo ($buttonRows); ?>
            <tr>
                <th colspan="2" style="padding: 15px;"><span style=
                "background-color: green;">&nbsp;&nbsp;</span> = Open
                &nbsp;&nbsp; <span style=
                "background-color: yellow;">&nbsp;&nbsp;</span> = Open
                <span style="font-size: 12px;">(1-5
                shooters)</span>&nbsp;&nbsp; <span style=
                "background-color: red;">&nbsp;&nbsp;</span> = Closed
                <span style="font-size: 12px;">(6 shooters)</span></th>
            </tr>
        </table>
    </div>
    <div class="grid_12">
        <table style="text-align:center;">
            <tr>
                <!--<th>Employee</th>-->
                <th>Membership #</th>
                <th nowrap>Name</th>
                <th>Lane #</th>
                <th>Start Time</th>
                <th colspan="1">Action</th>
            </tr><?php echo ($rangeUserRows . $closedLanesRows); ?>
        </table>
    </div>
</body>
</html>