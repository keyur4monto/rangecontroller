<script>
function logmeout(memberno){
		var date = new Date();
		var hours = date.getHours();
		var minutes = '0' + date.getMinutes();
		var seconds = '0' + date.getSeconds();
		var formattedTime = hours + ':' + minutes.substr(minutes.length-2) + ':' + seconds.substr(seconds.length-2);
		document.cookie =  'end_time =' + formattedTime ;
		window.location.href="/xapp/customers/lane/logThemOut/"+memberno;
}
</script>
<style type="text/css">
.arrow_box {
    border: 4px solid #f0960e ;
    height: 250px;
    position: relative;
    width: 250px;
}
.arrow_box:after, .arrow_box:before {
 top: 100%;
 left: 50%;
 border: solid transparent;
 content: " ";
 height: 0;
 width: 0;
 position: absolute;
 pointer-events: none;
}

.arrow_box:after {
    border-color: #f0960e  rgba(136, 183, 213, 0) rgba(136, 183, 213, 0);
    border-width: 0;
    margin-left: -30px;
}
.arrow_box:before {
border-color: #f0960e  rgba(194, 225, 245, 0) rgba(194, 225, 245, 0);
    border-width: 16px;
    margin-left: -10px;
}
.gallerycontainer{
position: relative;
/*Add a height attribute and set to largest image's height to prevent overlaying*/
}

.thumbnail img{
border: 1px solid white;
margin: 0 5px 5px 0;
}

.thumbnail:hover{
background-color: transparent;
}

.thumbnail:hover img{
}

.thumbnail span{ /*CSS for enlarged image*/
position: absolute;
left: -1000px;
visibility: hidden;
color: black;
text-decoration: none;
}

.thumbnail span img{ /*CSS for enlarged image*/
border-width: 0;
}

.thumbnail:hover span{ /*CSS for enlarged image*/
visibility: visible;
top: -200px;
left: 290px; /*position where enlarged image should offset horizontally */
z-index: 50;
}

</style>
<div class="clear"></div>
          <div class="grid_12">
            <div class="text-right">
              <input type="button" value="Search" onclick="window.location='/xapp/customers'">
            </div>
          </div>

          <div class="margt"></div>
          <div class="grid_12 margt">
						
						<!--<div class="text-center margb">
                Range 1 | <a href="#">Range 2</a> | <a href="#">Range 3</a> |
                <a href="#" title="Add additional range"><i class=
                "fa fa-plus"></i></a>
            </div>-->
						
            <table class="text-center">
			  <tr>
                <td colspan="7"><span style="color: #fbb91f;" id="nicktext">Current Range Usage as of ( <span id="timeThingy"> </span> )</span></td>
              </tr>
              <tr>
                <th>Employee</th>
                <th>Membership #</th>
                <th>Name</th>
                <th>Lane #</th>
                <th>Start Time</th>
                <th colspan="1">Action</th>
              </tr>
			  <?php echo ($rangeUserRows . $closedLanesRows); ?>
              
            </table>
          </div>		
<script>

var refreshCounter = 0;

function tConvert (time) {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  
  else time [2] = ' AM';
  
  return time.join (''); // return adjusted time or original string
}

function timerTick () {

	refreshCounter++;
	
	if (refreshCounter == 30) {
	
		location.reload();
		
		document.getElementById("nicktext").innerHTML = "Refreshing... Please wait! :)";
		
	}

	var date = new Date();
	// hours part from the timestamp
	var hours = date.getHours();
	// minutes part from the timestamp
	var minutes = "0" + date.getMinutes();
	// seconds part from the timestamp
	var seconds = "0" + date.getSeconds();

	// will display time in 10:30:23 format
	var formattedTime = hours + ':' + minutes.substr(minutes.length-2) + ':' + seconds.substr(seconds.length-2);

	document.getElementById("timeThingy").innerHTML = tConvert(formattedTime);
	
	}
	
	timerTick();

setInterval(function () {timerTick();}, 1000);



$(document).ready(function(){
	$('.startTimes').each(function(i, obj) {
    	this.innerHTML = tConvert(this.innerHTML);
	});
});

</script>
