
		  <script>

			var xmlHttp = new XMLHttpRequest();
			
			var global_id = -1;
					  
			function deleteById (id) {
			
				var myurlstring = "/xapp/maintenance/cleaning/complete/job/delete";
				
				xmlHttp.open( "POST", myurlstring, false );
				
				xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				
				global_id = id;
				
				$( "#dialog" ).dialog( "open" );
				
				$("#shadow").show();
				
				//window.location="/xapp/maintenance/cleaning";
			
			}
			
			
			function dateFilter () {
			
				var y = document.getElementById("year");
				
				var year = y.options[y.selectedIndex].value;  
			
				var m = document.getElementById("month");
				
				var month = m.options[m.selectedIndex].value;  
			
				if (year != '' && month != '') window.location="/xapp/maintenance/cleaning/filterBy/date/" + month + "/" + year;
			
			}
			
			function jobFilter () {
			
				var j = document.getElementById("job");
				
				var job = j.options[j.selectedIndex].value;  
			
				if (job != '') window.location="/xapp/maintenance/cleaning/filterBy/job/" + job;
			
			}
		  
		  </script>

		  
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;display:none;" title="Delete Confirmation">
	<p id="popupContent">Are you sure you want to delete this signoff?</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

		  

          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right">
              <input type="button" class="hideFromPrint" onclick="window.location='/xapp/maintenance/cleaning/addJob';" value="Add Job">
              <input type="button" class="hideFromPrint" onclick="window.print()" value="Print">
            </div>
            <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="padding-bottom:65px;">
              <div class="grid_4">Filter by Month and Year<br>
                <select id="month" onchange="dateFilter()">
                  <option value=""></option>
                  <option value="January">January</option>
				   <option value="February">February</option>
				    <option value="March">March</option>
					 <option value="April">April</option>
					  <option value="May">May</option>
					   <option value="June">June</option>
					    <option value="July">July</option>
						 <option value="August">August</option>
						  <option value="September">September</option>
						   <option value="October">October</option>
						    <option value="November">November</option>
							 <option value="December">December</option>
                </select>
                <select id="year" onchange="dateFilter()">
                  <option value=""></option>
                  <option value="2013">2013</option>
				  <option value="2014">2014</option>
				  <option value="2015">2015</option>
				  <option value="2016">2016</option>
                </select>
                <br>
              </div>
              <div class="grid_4">Filter by Job<br>
                <select id="job" onchange="jobFilter()">
                  <option value=""></option>
                  <?php echo ($jobOptions); ?>
                </select>
              </div>
              <div class="grid_4"> 
                <input type="button" value="Show All" onclick="window.location='/xapp/maintenance/cleaning/filterBy/date/<?=date('F')?>/<?=date('Y')?>';" style="padding: 15px;">
              </div>
            </div>
            <div class="grid_12 margt"><br>
				<?php echo ($jobsContent); ?>
            </div>
            <div class="grid_12 text-center">
              <!--<input type="button" value="Save" onClick="alert('Confirmation Alert of saved changes')">-->
            </div>
		</div>
         
		 <script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				
				xmlHttp.send("id=" + global_id);
				$( this ).dialog( "close" );
				$("#signoff-" + global_id).hide();
				$("#shadow").hide();
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});



function myConfirm (str) {

	var temp = $( "#dialog" ).dialog( "open" );
	
	alert(temp);
	
	return false;
	
}


// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>

		