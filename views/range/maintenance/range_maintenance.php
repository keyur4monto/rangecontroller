		  
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Are you sure you want to delete this note?</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

	
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right">
              <input type="button" value="Add Note" onClick="window.location='add/note'">
            </div>
          </div>
          <div class="grid_12 margt text-center">
						<!--<div class="margb">
                Range 1 | <a href="#">Range 2</a> | <a href="#">Range 3</a> |
                <a href="#" title="Add additional range"><i class=
                "fa fa-plus"></i></a>
            </div>-->
            <table style="width: 60%;">
              <tr>
                <td>Enable/Disable Lanes</td>
              </tr>
              <?php echo ($buttonRows); ?>
            </table>
          </div>
          <div class="grid_12 margt">
            <table>
              <tr>
                <th colspan="4">Notes</th>
              </tr>
              <?php echo $bugReports; ?>
            <tr id="norecords" <?php echo (empty($bugReports)) ? "" : "style='display:none'"; ?>>
            	<td colspan="8" style="color:#F00"><center>No Records Found</center></td>
            </tr>
            </table>
          </div>
        
		<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				var xmlHttp = null;
			
				var myurlstring = "/xapp/maintenance/lane/add/note/delete";

				xmlHttp = new XMLHttpRequest();
				xmlHttp.open( "POST", myurlstring, false );
				xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlHttp.send("id=" + noteToDelete);
				$("#shadow").hide();
				$("#report-" + noteToDelete).hide();
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

var noteToDelete = -1;

function myConfirm (note_id) {

	noteToDelete = note_id;

	$( "#dialog" ).dialog( "open" );
	
	$("#shadow").show();
	
	//alert(temp);
	
	//return false;
	
}


// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>
