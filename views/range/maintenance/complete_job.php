
		<script>
		  
			function submitForm () {
			
				var comments = document.getElementById("comments").value;
				
				var signoff = document.getElementById("employee_id").value;
			
				var xmlHttp = null;
			
				var myurlstring = "/xapp/maintenance/cleaning/complete/job/post";

				xmlHttp = new XMLHttpRequest();
				
				xmlHttp.open( "POST", myurlstring, false );
				
				xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				
				document.getElementById("popupContent").innerHTML = "This job has been marked as complete!";
				
				if (document.getElementById("comments").value != '' && document.getElementById("employee_id").value != '') xmlHttp.send("job_title=<?php echo($job_title); ?>&comments=" + comments + "&timestamp=<?=date("D M j g:i:s Y")?>&signoff=" + signoff + "&month=<?=date('F')?>&year=<?=date('Y')?>");
				
				else document.getElementById("popupContent").innerHTML = "Please fill out all the required fields to continue!";
				
				$( "#dialog" ).dialog( "open" );
				
				$("#shadow").show();
				
				//alert("Submitted!");
				
				//window.location="/xapp/maintenance/cleaning";
			
			}
		  
		  </script>
		  
		  
		  
		  
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">This job has been marked as complete!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"> </div>
          </div> 
          <div class="margt"></div>
          <div class="grid_12 margt">
            <table style="width:50%;">
              <tr>
                <th colspan="2"><span style="color: red;"><?php echo ($job_title); ?></span></th>
              </tr>
              <tr>
                <td class="text-right">Timestamp</td>
                <td><?=date("D M j g:i:s Y")?></td>
              </tr>
              <tr>
                <td class="text-right">Comments</td>
                <td><textarea id="comments" style="width: 300px; height: 100px;"></textarea></td>
              </tr>
            </table>
          </div>
          <div class="grid_12">
            <div>* all fields required</div>
            <div class="text-center">
              <input type="button" value="Save" onClick="submitForm()">
            </div>
          </div>
        
		<script>
		
		$(document).ready(function () {
		  
			getEmployees();
		
			$("#breadcrumbsRigh").show();
			
		});
		
		</script>
		
		
		
		<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				if (document.getElementById("comments").value != '' && document.getElementById("employee_id").value != '') window.location="/xapp/maintenance/cleaning";
				
				else $("#shadow").hide();
				
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});



function myConfirm (str) {

	var temp = $( "#dialog" ).dialog( "open" );
	
	alert(temp);
	
	return false;
	
}


// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>

		