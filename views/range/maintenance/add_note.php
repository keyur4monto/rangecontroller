
          <script>

		  $(document).ready(function () {
		  
			getEmployees();
		  
			$("#breadcrumbsRigh").show();
			
		});
		
		var tday = new Date(); 

//var tday = new Date();
		var dd = tday.getDate();
		var mm = tday.getMonth()+1; //January is 0!
		var yyyy = tday.getFullYear();

		if(dd<10) {
		dd='0'+dd
		} 

		if(mm<10) {
		mm='0'+mm
		} 

		tday = mm+'/'+dd+'/'+yyyy;
		
		//alert(tday);
		
		function submitNote () {
		
			var xmlHttp = null;
			
			var myurlstring = "/xapp/maintenance/lane/add/note/post";

			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "POST", myurlstring, true );
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			
			document.getElementById("popupContent").innerHTML = "Your note has been saved!";
			
			if (document.getElementById("employee_id").value != '') xmlHttp.send("bug_content=" + document.getElementById("bug_content").value + "&employee_id=" + document.getElementById("employee_id").value + "&bug_date=" + tday);
			
			else document.getElementById("popupContent").innerHTML = "Please select an employee to continue!";
			
			//alert(xmlHttp.responseText);
			
			$( "#dialog" ).dialog( "open" );
			
			$("#shadow").show();
				
		}
		  
		  </script>
		  
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Your note has been saved!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

		  
		  <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"> </div>
          </div>
          <div class="grid_12 margt"><br>
            <table style="width: 70%;">
              <tr>
                <th colspan="2">Add a note </th>
              </tr>
              <tr>
              <tr>
                <td class="text-right">Notes:</td>
                <td><textarea id="bug_content" style="width: 500px; height: 60px;"></textarea></td>
              </tr>
            </table>
          </div>
          <div class="grid_12">
            <div>* all fields required</div>
            <div class="text-center">
              <input type="button" value="Save" onClick="submitNote()">
            </div>
          </div>
        </div>
		
		<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				if (document.getElementById("employee_id").value != '') window.location="/xapp/maintenance/lane/home";
				
				else $("#shadow").hide();
				
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});



function myConfirm (str) {

	var temp = $( "#dialog" ).dialog( "open" );
	
	alert(temp);
	
	return false;
	
}


// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>
