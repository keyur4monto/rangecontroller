
		  <script>
		  
			function submitForm () {
			
				var job_title = document.getElementById("job_title").value;
			
				var duty1 = document.getElementById("duty-1").value;
				
				var duty2 = document.getElementById("duty-2").value;
				
				var duty3 = document.getElementById("duty-3").value;
				
				var duty4 = document.getElementById("duty-4").value;
				
				var duty5 = document.getElementById("duty-5").value;
			
				var xmlHttp = null;
			
				var myurlstring = "/xapp/maintenance/cleaning/manage/job/post";

				xmlHttp = new XMLHttpRequest();
				
				xmlHttp.open( "POST", myurlstring, false );
				
				xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				
				document.getElementById("popupContent").innerHTML = "Your job has been updated!";
			
				if (document.getElementById("job_title").value != '' && document.getElementById("duty-1").value != '') xmlHttp.send("id=<?php echo($id); ?>&job_title=" + job_title + "&duty1=" + duty1 + "&duty2=" + duty2 + "&duty3=" + duty3 + "&duty4=" + duty4 + "&duty5=" + duty5);
				
				else document.getElementById("popupContent").innerHTML = "Please fill out all the required fields to continue!";
				
				$( "#dialog" ).dialog( "open" );
				
				$("#shadow").show();
				
				//alert(xmlHttp.responseText);
				
				//window.location="/xapp/maintenance/cleaning";
			
			}
		  
		  </script>
		  
		  
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Your job has been updated!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

		  
		  
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"> </div>
          </div>
          <div class="margt"></div>
          <div class="grid_12 margt">
            <table style="width:50%;">
              <tr>
                <td class="text-right"><span style="color: red;">*</span> Job Title</td>
                <td><input id="job_title" type="text" style="width: 280px;" value="<?php echo($job_title); ?>"></td>
              </tr>
              <tr>
              <td colspan="2" class="text-center"><span style="color: red;">Add a job title and up to 5 duties</span></td></tr>
              <tr>
                <td class="text-right"><span style="color: red;">*</span> Duty 1</td>
                <td><input id="duty-1" type="text" style="width: 280px;" value="<?php echo($duty1); ?>"></td>
              </tr>
              <tr>
               <td class="text-right">Duty 2</td>
                <td><input id="duty-2" type="text" style="width: 280px;" value="<?php echo($duty2); ?>"></td>
              </tr>
              <tr>
                <td class="text-right">Duty 3</td>
                <td><input id="duty-3" type="text" style="width: 280px;" value="<?php echo($duty3); ?>"></td>
              </tr>
              <tr>
               <td class="text-right">Duty 4</td>
                <td><input id="duty-4" type="text" style="width: 280px;" value="<?php echo($duty4); ?>"></td>
              </tr>
              <tr>
                <td class="text-right">Duty 5</td>
                <td><input id="duty-5" type="text" style="width: 280px;" value="<?php echo($duty5); ?>"></td>
              </tr>
            </table>
          </div>
          <div class="grid_12 text-center">
            <input type="button" value="Save" onClick="submitForm()">
          </div>
        
		
		<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				if (document.getElementById("job_title").value != '' && document.getElementById("duty-1").value != '') window.location="/xapp/maintenance/cleaning";
				
				else $("#shadow").hide();
				
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});



function myConfirm (str) {

	var temp = $( "#dialog" ).dialog( "open" );
	
	alert(temp);
	
	return false;
	
}


// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>

		