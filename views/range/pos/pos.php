<script>
var globalTotal = 0.00;

var globalDiscount = 0.00;

var globalCustomer = <?php echo($member_no); ?>;

var transaction_number = 0;

function populateCustomerList(str){

if (window.XMLHttpRequest){
// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else{
// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
document.getElementById("div_customer_list").innerHTML = xmlhttp.responseText;

if (str == "") $("#div_customer_list").hide();

else $("#div_customer_list").show();
//alert("Test!" + xmlhttp.responseText + str);
}
}//end onreadystatechange
xmlhttp.open("GET","../../xapp/ajax/getCustomersForPos/"+str,true);
xmlhttp.send();
}//END AJAX TO FILL POS ITEMS LIST

function closeFrame (del_url) {

	$("#paymentPage").hide();
	
	document.getElementById("paymentPage").src = del_url;
	
	setInterval(function(){ 
	
		document.getElementById("paymentPage").src = "about:blank";
		
	}, 1500);

}

function setCustomer (customer_id) {

$.ajax({url:"/xapp/ajax/getCustomerForPos/" + customer_id,success:function(result){
    $("#currentCustomer").html(result);
  }});
  
  $("#div_customer_list").hide(); 

$("#selectedCustomer").show();

$("#customerSearch").hide(); 

globalCustomer = customer_id;

}

var highestRowIndex = 5;

var numberOfItems = 0;

var numberOfVisibleRows = 5;

function clearCustomer () {

$("#currentCustomer").html("No customer selected");

$("#customerSearch").show(); 

}

function populateItemsList(str){

if (window.XMLHttpRequest){
// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else{
// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
document.getElementById("div_items_list").innerHTML = xmlhttp.responseText;

if (str == "") $("#div_items_list").hide(); 

else $("#div_items_list").show();
//alert("Test!" + xmlhttp.responseText + str);
}
}//end onreadystatechange
xmlhttp.open("GET","../../xapp/ajax/getPosItems/"+str,true);
xmlhttp.send();
}//END AJAX TO FILL POS ITEMS LIST

var item_counter = 0;
	
function isHidden(el) {
    return (el.offsetParent === null)
}

function addItem (item_number, brand, model, description, price, inventory_id) {

numberOfItems++;

if(numberOfItems > numberOfVisibleRows) {

highestRowIndex++;

numberOfVisibleRows++;

$("#row-" + highestRowIndex).show();

}

document.getElementById("namesearch").value = "";

//alert(item_number + brand + model + description + price);

	var newItem = {
	
		item_number: item_number,
		brand: brand,
		model: model,		
		description: description,
		price: price,
		inventory_id: inventory_id
	
	}
	
	//alert(newItem.item_number + newItem.brand + newItem.model + newItem.description + newItem.pricey);
	
	item_counter++;
	
	while (isHidden(document.getElementById("row-" + item_counter))) {
	
		item_counter++;
	
	}
	
	document.getElementById("item-" + item_counter + "-number").innerHTML = newItem.item_number;
	
	document.getElementById("item-" + item_counter + "-brand").innerHTML = newItem.brand;
	
	document.getElementById("item-" + item_counter + "-model").innerHTML = newItem.model;
	
	document.getElementById("item-" + item_counter + "-description").innerHTML = newItem.description;
	
	document.getElementById("item-" + item_counter + "-price").value = "" + newItem.price;
	
	document.getElementById("item-" + item_counter + "-inventory").value = "" + newItem.inventory_id;
	
	$("#div_items_list").hide(); 
	
}

function submitManual () {

	addItem("Manual", document.getElementById("manual-brand").value, document.getElementById("manual-model").value, document.getElementById("manual-description").value, "0.00");

	document.getElementById("manual-brand").value = "";
	
	document.getElementById("manual-model").value = "";
	
	document.getElementById("manual-description").value = "";

}

function updateCalculations () {

	//alert("Change!");
	
	var subtotal1 = 0.00;
	
	var taxExemptSubtotal = 0.00;
	
	var taxableSubtotal = 0.00;

	$('.qty').each(function() {
	
		if (!isHidden(document.getElementById(this.id))) {
	    
	    //alert( this.id );
	    
	    var qty = this.id;
	    
	    var ext = qty.replace("qty", "extended");
	    
	    var price = qty.replace("qty", "price");
	    
	    var tax_exempt = qty.replace("qty", "tax-exempt");
	    
	    //alert(qty);
	    
	    //alert(ext);
	    
	    //alert(price);
	    
	    document.getElementById(ext).innerHTML = "$" + Number(document.getElementById(qty).value * document.getElementById(price).value).toFixed(2);
	    
	    subtotal1 = subtotal1 + document.getElementById(qty).value * document.getElementById(price).value;
	    
	    //alert(tax_exempt);
	    
	    if (document.getElementById(tax_exempt).checked == true) taxExemptSubtotal = taxExemptSubtotal + document.getElementById(qty).value * document.getElementById(price).value;
	    
	    subtotal2 = (subtotal1 - document.getElementById("discount-dollars").value) * ((100 - document.getElementById("discount-percentage").value) / 100);
	    
	    taxableSubtotal = subtotal2 - taxExemptSubtotal;
	    
	    tax = taxableSubtotal * <?php echo($tax_rate); ?>;
	    
	    if (tax < 0) tax = 0;
	    
	    total = subtotal2 + tax;
	    
	    globalTotal = total;
	    
	    globalDiscount = subtotal2 - subtotal1;
	    
	    }
	    
	});
	
	document.getElementById("subtotal1").innerHTML = "$" + Number(subtotal1).toFixed(2);
	
	document.getElementById("subtotal2").innerHTML = "$" + Number(subtotal2).toFixed(2);
	
	document.getElementById("tax").innerHTML = "$" + Number(tax).toFixed(2);
	
	document.getElementById("total").innerHTML = "$" + Number(total).toFixed(2);
	
	document.getElementById("amount-due").innerHTML = "$" + Number(total).toFixed(2);

}


$( document ).ready(function() {

$("#popupContent").show();

$( ":checkbox" ).change(function () {

	updateCalculations();

});

	$('.qty').change(function() { updateCalculations(); });

	$('.price').change(function() { updateCalculations(); });
	
	$('#discount-dollars').change(function() { updateCalculations(); });
	
	$('#discount-percentage').change(function() { updateCalculations(); });
	
	$(".remove").click(function() {
	
		numberOfVisibleRows--;
	
		if (numberOfVisibleRows < 5) {
		
			highestRowIndex++;

			$("#row-" + highestRowIndex).show();
		
		}

  		$(this).closest("tr").hide();
  		
  		updateCalculations();

	});
	
	$(".removeCust").click(function() {
	
		$(this).closest("tr").hide();
	
	});
	
	
	getEmployees();
		  
	$("#breadcrumbsRigh").show();

});




function prepareForPayment () {

	if (document.getElementById("employee_id").value != '' && document.getElementById('selectedCustomer').style.display != "none") {

	var myurlstring2 = "/xapp/getTransactionId"

	var xmlHttp2 = null;

	xmlHttp2 = new XMLHttpRequest();
    xmlHttp2.open( "POST", myurlstring2, true );
    
	xmlHttp2.onload = function (e) {
	
		//alert(xmlHttp.responseText);
		
		transaction_number = xmlHttp2.responseText;
	
		$(".itemclass:visible").each(function () {
	
		//alert(this.id);
		
		var elemID = this.id;
		
		var prefix = elemID.replace("row", "item");
		
		var item_num = $("#" + prefix + "-number").html();
		
		if (item_num != '&nbsp;') {
		
			var item_brand = $("#" + prefix + "-brand").html();
		
			var item_model = $("#" + prefix + "-model").html();
		
			var item_description = $("#" + prefix + "-description").html();
		
			var item_price = $("#" + prefix + "-price").val();
			
			var item_inventory = $("#" + prefix + "-inventory").val();
		
			var item_qty = $("#" + prefix + "-qty").val();
		
			var item_extended = $("#" + prefix + "-extended").html();
		
			item_extended = item_extended.replace("$", "");
		
			var tot = Number(globalTotal).toFixed(2);;
	
			var disc = Number(globalDiscount).toFixed(2);;
		
			var member_no = globalCustomer;
	
			var emp_id = $("#employee_id").val();
			
			var xmlHttp = null;
			
			var myurlstring = "/xapp/pointOfSalePost";

			xmlHttp = new XMLHttpRequest();
			
			xmlHttp.onload = function (e) {
	
				setTimeout(function(){ 
				
				var el = document.getElementById('paymentPage');
				
				el.src = "/xapp/pointOfSalePayment/" + transaction_number;
				
				el.style.display = "block";
				
				  }, 1500);
				
			} 
			
			xmlHttp.open( "POST", myurlstring, true );
			
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			
			xmlHttp.send("inventory_id=" + item_inventory + "&transaction_number=" + transaction_number + "&item_number=" + item_num + "&model=" + item_model + "&brand=" + item_brand + "&description=" + item_description + "&price=" + item_price + "&extended_price=" + item_extended + "&quantity=" + item_qty + "&total=" + tot + "&discount=" + disc + "&membership_number=" + member_no + "&employee_id=" + emp_id);
	
		//alert(item_num + item_brand + item_model + item_description + item_price + item_qty + item_extended + tot + disc + member_no + "AND" + emp_id);
	
			}
	
		});
		
	}

	xmlHttp2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp2.send("dummy=123");
    
    }
    
    else { 
    
    	$( "#dialog" ).dialog( "open" );
    	
    	$("#shadow").show();
    	
    	}
	
}

</script>

<style>

#breadcrumbsRigh {

position:relative;
top:78px;

}

input {



}

</style>


<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent" style="display:none;">Please ensure that you've selected an employee and a customer!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

        <div class="clear"></div>
        <div class="grid_12">
          <div class="text-right">
            <input type="button" value="Manage Inventory" onClick="window.location='/xapp/inventory'">
            <input type="button" value="Manage Sale" onClick="window.location='/xapp/inventory/manageSale'">
            <input type="button" value="Config" onClick="window.location='/xapp/inventory/posConfig'">
          </div>
          <div class="margt">
            <div class="grid_8">Search for item:
              <input type="text" autocomplete="off" id="namesearch" onKeyUp="populateItemsList(this.value)" style="width: 200px;" placeholder="I-#, Brand, Model, or Description">
              <div style="position:absolute;background-color:white;color:black;z-index:100;" id="div_items_list" class="customer_results"></div>
              &nbsp;&nbsp;or&nbsp;
              <input type="button" value="Enter Manual Item" onclick="showManualBox()">
            </div>
            <div class="grid_4 text-right">
              
            </div>
          </div>
          <div class="grid_12 margt">
            <table style="width: 100%;">
              <tr class="text-center" style="font-size: 14px;">
                <th>#</th>
                <th>Brand</th>
                <th>Model</th>
                <th>Item Description</th>
                <th>Price ($)</th>
                <th>Qty</th>
                <th>Ext Price</th>
                <th style="width:20px;font-size:12px;">Tax Exempt</th>
                <th style="width:15px;font-size:12px;">Remove</th>
              </tr>
              <tr class="itemclass" id="row-1">
                <td style="text-align:center;" id="item-1-number" >&nbsp;</td>
                <td style="text-align:center;" id="item-1-brand" >&nbsp;</td>
                <td style="text-align:center;" id="item-1-model" >&nbsp;</td>
                <td style="text-align:center;" id="item-1-description" >&nbsp;</td>
                <td style="text-align:center;width:60px;"><input id="item-1-price" style="width:90%;" type="text" class="price"/></td>
                <td style="text-align:center;width:50px;"><input id="item-1-qty" style="width:90%;" type="text" class="qty"/><input id="item-1-inventory" style="width:90%;" type="hidden"/></td>
                <td style="text-align:center;width:50px;" id="item-1-extended" >$0.00</td>
                <td style="text-align:center;"><input type="checkbox" name="item-1-tax-exempt" id="item-1-tax-exempt"/></td>
                <td style="text-align:center;width:15px;" class="text-center" style="font-size: 14px;"><a class="remove" href="#asd">X</a></td>
              </tr>
              <tr class="itemclass" id="row-2">
                <td style="text-align:center;" id="item-2-number" >&nbsp;</td>
                <td style="text-align:center;" id="item-2-brand" >&nbsp;</td>
                <td style="text-align:center;" id="item-2-model" >&nbsp;</td>
                <td style="text-align:center;" id="item-2-description" >&nbsp;</td>
                <td style="text-align:center;width:60px;"><input id="item-2-price" style="width:90%;" type="text" class="price"/></td>
                <td style="text-align:center;width:50px;"><input id="item-2-qty" style="width:90%;" type="text" class="qty"/><input id="item-2-inventory" style="width:90%;" type="hidden"/></td>
                <td style="text-align:center;width:50px;" id="item-2-extended" >$0.00</td>
                <td style="text-align:center;"><input type="checkbox" name="item-2-tax-exempt" id="item-2-tax-exempt"/></td>
                <td style="text-align:center;width:15px;" class="text-center" style="font-size: 14px;"><a class="remove" href="#asd">X</a></td>
              </tr>
              <tr class="itemclass" id="row-3">
                <td style="text-align:center;" id="item-3-number" >&nbsp;</td>
                <td style="text-align:center;" id="item-3-brand" >&nbsp;</td>
                <td style="text-align:center;" id="item-3-model" >&nbsp;</td>
                <td style="text-align:center;" id="item-3-description" >&nbsp;</td>
                <td style="text-align:center;width:60px;"><input id="item-3-price" style="width:90%;" type="text" class="price"/></td>
                <td style="text-align:center;width:50px;"><input id="item-3-qty" style="width:90%;" type="text" class="qty"/><input id="item-3-inventory" style="width:90%;" type="hidden"/></td>
                <td style="text-align:center;width:50px;" id="item-3-extended" >$0.00</td>
                <td style="text-align:center;"><input type="checkbox" name="item-3-tax-exempt" id="item-3-tax-exempt"/></td>
                <td style="text-align:center;width:15px;" class="text-center" style="font-size: 14px;"><a class="remove" href="#asd">X</a></td>
              </tr>
              <tr class="itemclass" id="row-4">
                <td style="text-align:center;" id="item-4-number" >&nbsp;</td>
                <td style="text-align:center;" id="item-4-brand" >&nbsp;</td>
                <td style="text-align:center;" id="item-4-model" >&nbsp;</td>
                <td style="text-align:center;" id="item-4-description" >&nbsp;</td>
                <td style="text-align:center;width:60px;"><input id="item-4-price" style="width:90%;" type="text" class="price"/></td>
                <td style="text-align:center;width:50px;"><input id="item-4-qty" style="width:90%;" type="text" class="qty"/><input id="item-4-inventory" style="width:90%;" type="hidden"/></td>
                <td style="text-align:center;width:50px;" id="item-4-extended" >$0.00</td>
                <td style="text-align:center;"><input type="checkbox" name="item-4-tax-exempt" id="item-4-tax-exempt"/></td>
                <td style="text-align:center;width:15px;" class="text-center" style="font-size: 14px;"><a class="remove" href="#asd">X</a></td>
              </tr>
              <tr class="itemclass" id="row-5">
                <td style="text-align:center;" id="item-5-number" >&nbsp;</td>
                <td style="text-align:center;" id="item-5-brand" >&nbsp;</td>
                <td style="text-align:center;" id="item-5-model" >&nbsp;</td>
                <td style="text-align:center;" id="item-5-description" >&nbsp;</td>
                <td style="text-align:center;width:60px;"><input id="item-5-price" style="width:90%;" type="text" class="price"/></td>
                <td style="text-align:center;width:50px;"><input id="item-5-qty" style="width:90%;" type="text" class="qty"/><input id="item-5-inventory" style="width:90%;" type="hidden"/></td>
                <td style="text-align:center;width:50px;" id="item-5-extended" >$0.00</td>
                <td style="text-align:center;"><input type="checkbox" name="item-5-tax-exempt" id="item-5-tax-exempt"/></td>
                <td style="text-align:center;width:15px;" class="text-center" style="font-size: 14px;"><a class="remove" href="#asd">X</a></td>
              </tr>
              <?php echo ($extraRows); ?>
            </table>
          </div>
          <div class="grid_7" style="font-size: 14px;">
            <div id="manualBox" style="display:none;">
              <table>
                <tr>
                  <th colspan="2">Enter Manual Item</th>
                  <th><a href="#nothing" onclick="hideManualBox()">Hide</a></th>
                </tr>
                <tr>
                  <td class="text-right">Brand:</td>
                  <td><input id="manual-brand" type="text" style="width: 200px;"></td>
                  <td rowspan="3" class="text-center" style="width: 10%;"><input type="button" onclick="submitManual()" value="Add"></td>
                </tr>
                <tr>
                  <td class="text-right">Model:</td>
                  <td><input id="manual-model" type="text" style="width: 200px;"></td>
                </tr>
                <tr>
                  <td class="text-right">Description:</td>
                  <td><input id="manual-description" type="text" style="width: 200px;"></td>
                </tr>
              </table>
            </div>
            <div id="customerBox">
              <table>
                <tr id="selectedCustomer">
                  <td class="text-right">Selected Customer:</td>
                  <td nowrap="" id="currentCustomer"></td>
                  <td class="text-center" style="font-size: 14px;"><a onclick="clearCustomer()" class="removeCust" href="#asdf">Delete</a></td>
                </tr>
              </table>
            </div>
            <div id="customerSearch">Search for customer:
              <input type="text" autocomplete="off" id="namesearch" onKeyUp="populateCustomerList(this.value)" placeholder="Enter First or Last Name here.." style="width: 200px;"><div style="position:absolute;left:135px;background-color:white;color:black;z-index:100;" id="div_customer_list" class="customer_results"></div>
            </div>
          </div>
          <div class="grid_3" style="float: right;">
            <table>
              <tr>
                <td class="text-right">Subtotal:</td>
                <td id="subtotal1" style="width: 40%;">$</td>
              </tr>
              <tr>
                <td class="text-right" nowrap="">Discount $:</td>
                <td><input id="discount-dollars" style="width:85%;" type="text"/>
                 </td>
              </tr>
              <tr>
                <td class="text-right" nowrap="">Discount %:</td>
                <td><input id="discount-percentage" style="width:85%;" type="text"/>
                  </td>
              </tr>
              <tr>
                <td class="text-right">Subtotal 2:</td>
                <td id="subtotal2"></td>
              </tr>
              <tr>
                <td class="text-right">Tax:</td>
                <td id="tax"></td>
              </tr>
              <tr>
                <td class="text-right">Total:</td>
                <td id="total">$</td>
              </tr>
                </tr>
            </table>
            <div style="float: right; text-align: center;"><span style="font-size: 19px; font-weight: bold;">AMOUNT DUE<br><span id="amount-due" style="color: red; font-size: 26px;">$0.00</span></span></div> </div>
        </div>
        <div class="grid_12 margt">
          <table style="width: 100%;">
            <tr>
              <td style="font-weight: bold;"><div class="grid_6"><span style="color: red; font-size: 22px;">REGISTER: $<?php echo ($register); ?></span></div>
                <div class="grid_6 text-right">
                  <input type="button" onclick="window.location='/xapp/pointOfSale/<?php echo($member_no); ?>'" value="CLEAR">
                  <input type="button" value="PAYMENT" onClick="prepareForPayment()">
                </div></td>
            </tr>
          </table>
        </div>
        
        <iframe id="paymentPage" style="display:none;position:fixed;left:0;top:0;width:100vw;height:100vh;z-index:1000;border-left:0px;border-top:0px;">
        
        
        
        </iframe>
      
<script>

function showManualBox() {
	
	document.getElementById("manualBox").style.display="block";
	
}

function hideManualBox() {
	
	document.getElementById("manualBox").style.display="none";
	
}

setCustomer(<?php echo($member_no); ?>);

//updateCalculations();

if (<?php echo($member_no); ?> == 0) {

	document.getElementById('selectedCustomer').style.display = "none";

	clearCustomer();

}

</script>

<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				
				$( this ).dialog( "close" );
				
				$("#shadow").hide();
				
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>



</body>
</html>