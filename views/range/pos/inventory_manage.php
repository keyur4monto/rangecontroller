
<script>

		function clearFields () {
		
			$.each($('input[type=text]'),function(){
  			$(this).val("");
			});
			
			$.each($('select'),function(){
  			$(this).val("");
			});
		
		}
		
		function updateInventory () {
		
			var xmlHttp = null;
			
			var myurlstring = "/xapp/inventory/manageID/post";

			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "POST", myurlstring, true );
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			
			//document.getElementById("popupContent").innerHTML = "Your item has been added!";
			
			if (document.getElementById("new").checked) new_check = 1;
			
			else new_check = 0;
			
			if (document.getElementById("used").checked) used_check = 1;
			
			else used_check = 0;
			
			if (document.getElementById("consignment").checked) consignment_check = 1;
			
			else consignment_check = 0;
			
			if (document.getElementById("rental").checked) rental_check = 1;
			
			else rental_check = 0;
			
			xmlHttp.onload = function (e) {
			
				document.getElementById("popupContent").innerHTML = "Your changes have been saved!"
			
				$( "#dialog" ).dialog( "open" );
			
				$("#shadow").show();
				
				
			}
			
			if (document.getElementById("item_number").value != '' && document.getElementById("quantity").value != '' && document.getElementById("brand").value != '' && document.getElementById("inventory_type").value != '' && document.getElementById("model").value != '' && document.getElementById("cost").value != '' && document.getElementById("resale").value != '') xmlHttp.send("id=<?php echo($id); ?>&inventory_number=<?php echo($inventory_number); ?>&item_number=" + document.getElementById("item_number").value + "&minquantity=" + document.getElementById("minquantity").value + "&brand=" + document.getElementById("brand").value + "&quantity=" + document.getElementById("quantity").value + "&model=" + document.getElementById("model").value + "&serial=" + document.getElementById("serial").value + "&color=" + document.getElementById("color").value + "&cost=" + document.getElementById("cost").value + "&resale=" + document.getElementById("resale").value + "&profit=" + document.getElementById("profit").value + "&action=" + document.getElementById("action").value + "&finish=" + document.getElementById("finish").value + "&length=" + document.getElementById("length").value + "&calibre=" + document.getElementById("calibre").value + "&upc=" + document.getElementById("upc").value + "&description=" + document.getElementById("description").value + "&inventory_type=" + document.getElementById("inventory_type").value + "&new=" + new_check + "&used=" + used_check + "&consignment=" + consignment_check + "&rental=" + rental_check);
			
			else { 
			
				document.getElementById("popupContent").innerHTML = "Please fill out all the required fields!";
				
				$( "#dialog" ).dialog( "open" );
			
				$("#shadow").show();
				
			}
			
		
		}
		
		$(function() {
			var proftemp1 = parseFloat($("#profit").val()).toFixed(2) + "%";
			$("#profit").val(proftemp1);
		});

</script>


<?$prof1 = number_format(($resale-$cost)*100/$cost,2,'.','');?>

<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Your changes have been saved!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>


        <div class="clear"></div>
        <div class="grid_12">
        <div class="text-right"></div>
        <div class="margt">&nbsp;</div>
        <div class="grid_12">
          <table >
            <tr>
              <td><span class="requiredf">*</span>i-#:
                <input id="item_number" value="<?php echo($item_number); ?>" type="text" style="width: 50px;"></td>
              <td><span class="requiredf">*</span>Qty:
                <input id="quantity" value="<?php echo($quantity); ?>" type="text" style="width: 50px;"></td>
				<!--NEW DR 10/5/2015-->
				<td><span class="requiredf"></span>Min Qty:
                <input id="minquantity" value="<?php echo($minquantity); ?>" type="text" style="width: 50px;"></td>
              <td>New:
                <input id="new" <?php if($new == 'Yes') echo ("checked"); ?> type="checkbox"></td>
              <td>Used:
                <input id="used" <?php if($used == 'Yes') echo ("checked"); ?> type="checkbox"></td>
              <td>Consignment:
                <input id="consignment" <?php if($consignment == 'Yes') echo ("checked"); ?> type="checkbox"></td>
              <td>Rental:
                <input id="rental" <?php if($rental == 'Yes') echo ("checked"); ?> type="checkbox"></td>
            </tr>
          </table>
        </div>
        <div class="grid_4">
          <table>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Type:</td>
              <td><select id="inventory_type" style="width:135px;">
                  <option></option>
                 <?php echo($inventory_types); ?>
                </select></td>
            </tr>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Brand:</td>
              <td><input id="brand" value="<?php echo($brand); ?>" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Model:</td>
              <td><input id="model" value="<?php echo($model); ?>" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right" nowrap="">Serial Number:</td>
              <td><input id="serial" value="<?php echo($serial); ?>" type="text" style="width: 125px;"></td>
            </tr>
          </table>
        </div>
        <div class="grid_4">
          <table>
            <tr>
              <td class="text-right">Color:</td>
              <td><input id="color"  value="<?php echo($color); ?>" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Cost:</td>
              <td><input id="cost"  value="<?php echo($cost); ?>" type="text" style="width: 50px;"></td>
            </tr>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Resale:</td>
              <td><input id="resale"  value="<?php echo($resale); ?>" type="text" style="width: 50px;"></td>
            </tr>
            <tr>
              <td class="text-right">Profit Margin:</td>
              <td><input id="profit"  value="<?php echo ($profit ? $profit : $prof1); ?>" type="text" style="width: 50px;"></td>
            </tr>
          </table>
        </div>
        <div class="grid_4">
          <table>
            <tr>
              <td class="text-right">Action:</td>
              <td><input id="action"  value="<?php echo($action); ?>" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right">Finish:</td>
              <td><input id="finish"  value="<?php echo($finish); ?>" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right">#Barrels/Length:</td>
              <td><input id="length"  value="<?php echo($length); ?>" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right">Caliber/Gauge:</td>
              <td><input id="calibre"  value="<?php echo($calibre); ?>" type="text" style="width: 125px;"></td>
            </tr>
          </table>
        </div>
        <div class="grid_8">
          <table>
			  <tr>
              <td class="text-right">UPC:</td>
              <td><input id="upc" type="text" value="<?php echo($upc); ?>" style="width: 125px;" value=""></td>
            </tr>
            <tr>
              <td class="text-right">Description:</td>
              <td><input id="description"  value="<?php echo($description); ?>" type="text" style="width: 300px;"></td>
            </tr>
          </table>
          
          <span style="color: #fbb91f;">Added by <?php echo($added_by); ?> on <?php echo($timestamp); ?></span>
          
        </div>
        <div class="grid_4 text-right">
          <input type="button" onclick="clearFields()" value="Clear">
          <input type="button" value="Save" onClick="updateInventory()">
          <br>
          <br>
          <span class="requiredf">*</span> denotes required fields </div>
          
          <script>
          
          document.getElementById("inventory_type").value = '<?php echo($type); ?>';
          
          </script>
          
          
		<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
				
				if (document.getElementById("popupContent").innerHTML != "Please fill out all the required fields!") window.location = "/xapp/inventory";
			
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

$( "#profit" ).change(function() {
var restemp = parseFloat($("#cost").val()) + parseFloat($("#cost").val())/100*parseFloat($("#profit").val());
$("#resale").val(restemp.toFixed(2));
var proftemp1 = parseFloat($("#profit").val()).toFixed(2) + "%";
$("#profit").val(proftemp1);
});

$( "#resale" ).change(function() {
var proftemp = (parseFloat($("#resale").val()) - parseFloat($("#cost").val()))*100/parseFloat($("#cost").val());
proftemp = proftemp.toFixed(2) + "%";
$("#profit").val(proftemp);
});

</script>