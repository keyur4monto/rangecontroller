<script src="/xapp/js/multifilter.mod.js"></script>
<script>


$(document).ready(function() {

$("#popupContent").show();

$('.myfilter').multifilter({
    'target' : $('#main-table')
  });


//alert("Test!");

});

function clearFilters() {

	window.location = '/xapp/inventory/bulk';
	
}

var inventory_id = 0;

function SaveBulk(){

document.getElementById("popupContent").innerHTML = "Your changes have been saved!"
			
				$( "#dialog" ).dialog( "open" );
			
				$("#shadow").show();


$("#bulkPost").ajaxForm({url: '/xapp/inventory/bulkpost', type: 'post'})

}



</script>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right">
    <input type="button" value="Save" onclick="SaveBulk();">
   </div>
  <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="height:53px;">
    <div id="typetext" class="grid_3" style="width: 160px;">Filter by type<br>
      <select data-col="type" class="myfilter">
        <option></option>
        <?php echo($inventory_types); ?>
      </select>
    </div>
    <div class="grid_3" style="width: 160px;">Filter by item #<br>
      <input id="itemtext" type="text" data-col="item #" class="myfilter" style="width: 100px;">
    </div>
    <div class="grid_3" style="width: 160px;">Filter by brand :<br>
      <input id="brandtext" type="text"  data-col="brand" class="myfilter" style="width: 100px;padding:0;margin:0;">
    </div>
    <div class="grid_3" style="width: 160px;">Filter by model :<br>
      <input id="modeltext" type="text"  data-col="model" class="myfilter" style="width: 100px;">
    </div>
    <div class="grid_3" style="width: 125px;">
      <input type="button" value="Show All" onclick="clearFilters()" style="padding: 15px;">
    </div>
  </div>
  <form id = "bulkPost">
  <div class="grid_12 margt">
    <table id="main-table" style="width: 100%;">
      <thead>
        <tr>
          <th>Type</th>
                <th>Item #</th>
                <th>Brand</th>
                <th>Model</th>
                <th>Description</th>
				<th>UPC</th>
				<th>Min Qty</th>
                <th>Qty</th>
                <th>Cost</th>
                <th>Price</th>
				<th>Delete</th>
        </tr>
      </thead>
      <tbody>
        <?php echo $inventoryRows; ?>
            <tr id="norecords" <?php echo (empty($inventoryRows)) ? "" : "style='display:none'"; ?>>
            	<td colspan="9" style="color:#F00"><center>No Records Found</center></td>
            </tr>
      </tbody>
    </table>
  </div>
  <div class="grid_12 margt text-center">
            <input type="button" value="Save" onclick="SaveBulk();">
          </div>
		  </form>
</div>
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent" style="display:none;">Your changes have been saved!</p>
</div>
<!-- ui-dialog -->
<div id="dialogdelete" style="z-index: 101;" title="Delete Confirmation">
	<p id="popupContent" style="">Really delete this item from the inventory?</p>
</div>
		 
<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				$.post( "/xapp/inventory/bulkpost", $( "#bulkPost" ).serialize() );
				$("#shadow").hide();
				
				
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
				window.location="/xapp/inventory/bulk";
				
				
				
			}
		}
	],
	close: function() {$("#shadow").hide();}
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

$( ".inventory_delete" ).click(function( event ) {
inventory_id = $( ".inventory_delete" ).val();
$( "#dialogdelete" ).dialog( "open" );
$("#shadow").show();
});
$( "#dialogdelete" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				window.location="/xapp/inventory/deletes/" + inventory_id;
				
				
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
			}
		}
	]
});
</script> 