
<script>

function goBack () {

		var del_url = "/xapp/inventory/manageSale/delete/<?php echo($transaction_number); ?>";

		window.top.closeFrame(del_url);
	
		parent.closeFrame(del_url);
	
}

function submitPayment () {

	var payment1type = document.getElementById("payment1type").value;
	
	var payment2type = document.getElementById("payment2type").value;
	
	var payment1amount = document.getElementById("payment1amount").value;
	
	var payment2amount = document.getElementById("payment2amount").value;
	
	var amountpaid = document.getElementById("amountpaid").value;
	
	var change = document.getElementById("myChange").value;
	
	
	var myurlstring = "/xapp/postPayment"

	var xmlHttp = null;

	xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "POST", myurlstring, true );
    
    xmlHttp.onload = function (e) {

    	//alert("Your payment has been saved! Please standby for your receipt...");
    
    	window.top.location = "/xapp/pointOfSaleReceipt/<?php echo($transaction_number); ?>";
    
	}

	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    
    if (document.getElementById("payment1type").value != '') xmlHttp.send("transaction_number=<?php echo($transaction_number); ?>&payment1type=" + payment1type + "&payment1amount=" + payment1amount + "&payment2type=" + payment2type + "&payment2amount=" + payment2amount + "&amountpaid=" + amountpaid + "&change=" + change);

	else {
	
		$( "#dialog" ).dialog( "open" );
		
		$("#shadow").show();
	
	}

}

</script>

<!--START HIDE LINKS ON POS PAYMENT PAGE-->
<style type="text/css">

.breadcrumbs {
 display: none;
}
.text-nodeco {
display: none;
}

</style>	
<!--END HIDE LINKS ON POS PAYMENT PAGE-->		


<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Please ensure that you've selected at least one payment!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

	<div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"> </div>
            <div class="grid_12 margt" ng-controller="paymentController">
              <table style="width: 40%;">
                <tr>
                  <td class="text-right" style="width: 60%;">Sale Amount</td>
                  <td><input readonly type="text" value="<?php echo ($sale_amount); ?>" style="width: 50px;"></td>
                </tr>
              </table>
              <table style="width: 40%;">
                <tr>
                  <td class="text-right" style="width: 60%;">Payment 1
                    <select id="payment1type">
                      <option value=""></option>
                      <option value="Cash">Cash</option>
                      <option value="Check">Check</option>
                      <option value="Debit">Debit</option>
                      <option value="Discover">Discover</option>
                      <option value="Mastercard">Mastercard</option>
                      <option value="Visa">Visa</option>
                    </select></td>
                  <td><input id="payment1amount" type="text" style="width: 50px;" value="0.00"></td>
                </tr>
                <tr>
                  <td class="text-right">Payment 2
                    <select id="payment2type">
                      <option value=""></option>
                      <option value="Cash">Cash</option>
                      <option value="Check">Check</option>
                      <option value="Debit">Debit</option>
                      <option value="Discover">Discover</option>
                      <option value="Mastercard">Mastercard</option>
                      <option value="Visa">Visa</option>
                    </select></td>
                  <td><input id="payment2amount" type="text" style="width: 50px;" value="0.00">
                </tr>
              </table> 
              <table style="width: 40%;">
                <tr id="amountpaidrow" style="display:none;">
                  <td class="text-right" style="width: 60%;color:green;font-weight:bold;">Amount Paid</td>
                  <td><input type="text" id="amountpaid" value="0.00" style="width: 50px;"></td>
                </tr>
                <tr id="amountneededrow">
                  <td class="text-right" style="width: 60%;color:red;font-weight:bold;">Amount Needed</td>
                  <td><input type="text" id="amountneeded" value="0.00" style="width: 50px;"></td>
                </tr>
              </table>
              <table style="width: 40%;">
                <tr>
                  <td class="text-right" style="width: 60%;">Change</td>
                  <td><input type="text" id="myChange" value="0.00" style="width: 50px;"></td>
                </tr>
              </table>
            </div>
            <div class="grid_12 text-center">
              <input type="button" value="Cancel" onClick="goBack();">
              <input type="button" value="Save" onClick="submitPayment()">
            </div>
          </div>
        
        <script>
        
        setInterval(function(){ 
        
        if (document.getElementById("payment1amount").value == "") document.getElementById("payment1amount").value = 0.00;
        
        if (document.getElementById("payment2amount").value == "") document.getElementById("payment2amount").value = 0.00;
        
        var changeCalc = Number((-1) * Number(<?php echo($sale_amount); ?> - (parseFloat(document.getElementById("payment1amount").value) + parseFloat(document.getElementById("payment2amount").value))).toFixed(2)).toFixed(2); 
        
        document.getElementById("amountpaid").value = Number(parseFloat(document.getElementById("payment1amount").value) + parseFloat(document.getElementById("payment2amount").value)).toFixed(2);
        
        document.getElementById("amountneeded").value = Number((1) * Number(<?php echo($sale_amount); ?> - (parseFloat(document.getElementById("payment1amount").value) + parseFloat(document.getElementById("payment2amount").value))).toFixed(2)).toFixed(2); 
        
        var amountneeded = document.getElementById("amountneeded").value;
        
        if (changeCalc > 0) document.getElementById("myChange").value = changeCalc;
        
        else document.getElementById("myChange").value = 0.00;
        
        if (amountneeded > 0.00) {
        
        	$("#amountneededrow").show();
        	
        	$("#amountpaidrow").hide();
        	
        }
        
        else { 
        
        	$("#amountpaidrow").show();
        	
        	$("#amountneededrow").hide();
        	
        }
        
        }, 200);
        
        </script>
        
        

<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				
				$( this ).dialog( "close" );
				
				$("#shadow").hide();
				
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>


