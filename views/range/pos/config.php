<script>

function saveEverything () {

	$('.checkboxes').each(function() {
	
		if (this.checked) {
		
			var xmlHttp = null;
			
			var myurlstring = "/xapp/inventory/deleteInventoryType";

			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "POST", myurlstring, true );
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlHttp.send("type_id=" + this.value);
		
		}
	
	});
	
	$('input[type=text]').each(function(){
    
    	if (this.value != '' && this.id != 'registerAmount') {
    	
    		var xmlHttp = null;
			
			var myurlstring = "/xapp/inventory/addInventoryType";

			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "POST", myurlstring, true );
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlHttp.send("inventory_type=" + this.value);
    	
    	}
	
	});

	var xmlHttp = null;
			
	var myurlstring = "/xapp/inventory/updateRegisterBase";

	xmlHttp = new XMLHttpRequest();
	xmlHttp.open( "POST", myurlstring, true );
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlHttp.send("amount=" + document.getElementById("registerAmount").value + "&" + "comments=" + encodeURIComponent(document.getElementById("comments").value));
	
	
	$( "#dialog" ).dialog( "open" );
	
	$("#shadow").show();

}

</script>

<!-- ui-dialog -->

<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
  <p id="popupContent">Your changes have been saved!</p>
</div>
<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right"> </div>
  <div class="grid_12 margt">
    <table style="width: 50%;">
      <tr>
        <th colspan="2"><span style="color: red;">Current Amount: $ <?php echo ($registerAmount); ?></span></th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Register Amount:</td>
        <td><input id="registerAmount" type="text" style="width: 50px;" value="<?php echo ($registerAmount); ?>"></td>
      </tr>
    </table>
    <table style="width: 50%;">
      <tr>
        <th colspan="3">Inventory Types:</th>
      </tr>
      <?php echo ($inventoryTypes); ?>
    </table>
    <table style="width: 50%;">
      <tr>
        <th colspan="2">Add Inventory Types</th>
      </tr>
      <tr class="text-center">
        <td><input type="text"></td>
        <td><input type="text"></td>
      </tr>
      <tr class="text-center">
        <td><input type="text"></td>
        <td><input type="text"></td>
      </tr>
      <tr class="text-center">
        <td><input type="text"></td>
        <td><input type="text"></td>
      </tr>
      <tr class="text-center">
        <td><input type="text"></td>
        <td><input type="text"></td>
      </tr>
      </tr>
      
    </table>
    <table style="width: 50%;">
      <tr>
        <th>Receipt Comments</th>
      </tr>
      <tr>
        <td class="text-center"><textarea id="comments" cols="50" rows="5" value = "<?php echo ($comments); ?>" placeholder="Enter comments that display at the bottom of sales receipts"><?php echo ($comments); ?></textarea></td>
      </tr>
    </table>
  </div>
</div>
<div class="grid_12 text-center">
  <input type="button" onclick="saveEverything()" value="Save">
</div>
<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				window.location = "/xapp/inventory/posConfig";
				
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});


</script> 
