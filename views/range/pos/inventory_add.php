
<script>

		function clearFields () {
		
			$.each($('input[type=text]'),function(){
  			$(this).val("");
			});
			
			$.each($('select'),function(){
  			$(this).val("");
			});
		
		}

		$(document).ready(function () {
		  
			getEmployees();
		  
			$("#breadcrumbsRigh").show();
			
		});

		function addInventory () {
		
			var xmlHttp = null;
			
			var myurlstring = "/xapp/inventory/add/post";

			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "POST", myurlstring, true );
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			
			//document.getElementById("popupContent").innerHTML = "Your item has been added!";
			
			if (document.getElementById("new").checked) new_check = 1;
			
			else new_check = 0;
			
			if (document.getElementById("used").checked) used_check = 1;
			
			else used_check = 0;
			
			if (document.getElementById("consignment").checked) consignment_check = 1;
			
			else consignment_check = 0;
			
			if (document.getElementById("rental").checked) rental_check = 1;
			
			else rental_check = 0;
			
			xmlHttp.onload = function (e) {
			
				//alert(xmlHttp.responseText);
				
				window.location = "/xapp/inventory";
			
			}
			
			if (document.getElementById("item_number").value != '' && document.getElementById("quantity").value != '' && document.getElementById("brand").value != '' && document.getElementById("employee_id").value != '' && document.getElementById("inventory_type").value != '' && document.getElementById("model").value != '' && document.getElementById("cost").value != '' && document.getElementById("resale").value != '') xmlHttp.send("employee_id=" + document.getElementById("employee_id").value + "&item_number=" + document.getElementById("item_number").value + "&brand=" + document.getElementById("brand").value + "&model=" + document.getElementById("model").value + "&serial=" + document.getElementById("serial").value + "&color=" + document.getElementById("color").value + "&cost=" + document.getElementById("cost").value + "&resale=" + document.getElementById("resale").value + "&profit=" + document.getElementById("profit").value + "&action=" + document.getElementById("action").value + "&finish=" + document.getElementById("finish").value + "&length=" + document.getElementById("length").value + "&calibre=" + document.getElementById("calibre").value + "&upc=" + document.getElementById("upc").value +"&description=" + document.getElementById("description").value + "&inventory_type=" + document.getElementById("inventory_type").value + "&quantity=" + document.getElementById("quantity").value + "&minquantity=" + document.getElementById("minquantity").value + "&new=" + new_check + "&used=" + used_check + "&consignment=" + consignment_check + "&rental=" + rental_check);
			//add other required fields
			else {
			
				document.getElementById("popupContent").innerHTML = "Please fill out all the required fields!";
			
				$( "#dialog" ).dialog( "open" );
			
				$("#shadow").show();
				
			}
		
		}

</script>




<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Your changes have been saved!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>


        <div class="clear"></div>
        <div class="grid_12">
        <div class="text-right"></div>
        <div class="margt">&nbsp;</div>
        <div class="grid_12">
          <table>
            <tr>
              <td><span class="requiredf">*</span>i-#:
                <input id="item_number" type="text" style="width: 50px;"></td>
              <td><span class="requiredf">*</span>Qty:
                <input id="quantity" type="text" style="width: 50px;"></td>
				<td><span class="requiredf"></span>Min Qty:
                <input id="minquantity" type="text" style="width: 50px;"></td>
              <td>New Item:
                <input id="new" type="checkbox"></td>
              <td>Used Item:
                <input id="used" type="checkbox"></td>
              <td>Consignment:
                <input id="consignment" type="checkbox"></td>
              <td>Rental:
                <input id="rental" type="checkbox"></td>
            </tr>
          </table>
        </div>
        <div class="grid_4">
          <table>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Type:</td>
              <td><select id="inventory_type" style="width:135px;">
                  <option></option>
                 <?php echo($inventory_types); ?>
                </select></td>
            </tr>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Brand:</td>
              <td><input id="brand" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Model:</td>
              <td><input id="model" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right" nowrap="">Serial Number:</td>
              <td><input id="serial" type="text" style="width: 125px;"></td>
            </tr>
          </table>
        </div>
        <div class="grid_4">
          <table>
            <tr>
              <td class="text-right">Color:</td>
              <td><input id="color" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Cost:</td>
              <td><input id="cost" type="text" style="width: 50px;"></td>
            </tr>
            <tr>
              <td class="text-right"><span class="requiredf">*</span>Resale:</td>
              <td><input id="resale" type="text" style="width: 50px;"></td>
            </tr>
            <tr>
              <td class="text-right">Profit Margin:</td>
              <td><input id="profit" type="text" style="width: 50px;"></td>
            </tr>
          </table>
        </div>
        <div class="grid_4">
          <table>
            <tr>
              <td class="text-right">Action:</td>
              <td><input id="action" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right">Finish:</td>
              <td><input id="finish" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right">#Barrels/Length:</td>
              <td><input id="length" type="text" style="width: 125px;"></td>
            </tr>
            <tr>
              <td class="text-right">Caliber/Gauge:</td>
              <td><input id="calibre" type="text" style="width: 125px;"></td>
            </tr>
          </table>
        </div>
        <div class="grid_8">
			<table>
				<tr>
              <td class="text-right">UPC:</td>
              <td><input id="upc" type="text" style="width: 125px;" value=""></td>
            </tr>
            <tr>
              <td class="text-right">Description:</td>
              <td><input id="description" type="text" style="width: 300px;"></td>
            </tr>
          
          </table>
        </div>
        <div class="grid_4 text-right">
          <input type="button" onclick="clearFields()" value="Clear">
          <input type="button" value="Save" onClick="addInventory()">
          <br>
          <br>
          <span class="requiredf">*</span> denotes required fields </div>
          
          
		<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				$("#shadow").hide();
			
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});


</script>
          
          