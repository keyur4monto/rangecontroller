
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right hideFromPrint">
            <input type="button" value="Print Receipt Size" onclick="window.open('Print/<?php echo($transaction_number); ?>', '_blank', '');">
              <input type="button" onclick="window.print()" value="Print Full Page">
            </div>
            <div class="grid_12 margt">
              <table style="width: 100%;">
                <tr>
                <tr>
                  <td colspan="6" style="font-weight: bold;"><div class="grid_6"> Transaction # <?php echo($transaction_number); ?> </div>
                    <div class="grid_6 text-right">Date: <?php echo($date_stamp); ?></div></td>
                </tr>
                <tr class="text-center">
                  <th>Brand</th>
                  <th>Model</th>
                  <th>Item Description</th>
                  <th>Price</th>
                  <th>Qty</th>
                  <th>Ext Price</th>
                </tr>
                <?php echo($receiptRows); ?>
                </table>
            </div>
            <div class="grid_6">
              <table>
                <tr>
                  <td class="text-right">Customer:</td>
                  <td><?php echo($customer_name); ?><br>
                    <?php echo($street_address); ?><br>
                    <?php echo($city); ?>, <?php echo($state); ?> <?php echo($zipcode); ?></td>
                </tr>
                <tr>
                  <td class="text-right">Employee:</td>
                  <td><?php echo($employee_name); ?></td>
                </tr>
              </table>
            </div>
            <div class="grid_4" style="float: right;">
              <table>
                <tr>
                  <td class="text-right">Subtotal:</td>
                  <td>$ <?php echo($subtotal); ?></td>
                </tr>
                <tr>
                  <td class="text-right">Discount:</td>
                  <td>- $
                    <?php echo($discount); ?></td>
                </tr>
                <tr>
                  <td class="text-right">Subtotal 2:</td>
                  <td>$ <?php echo($subtotal2); ?></td>
                </tr>
                <tr>
                  <td class="text-right">Tax:</td>
                  <td>$ <?php echo($tax); ?></td>
                </tr>
                <tr>
                  <td class="text-right">Total:</td>
                  <td>$ <?php echo($total); ?></td>
                </tr>
                <tr>
                  <td class="text-right">Change Due:</td>
                  <td>$ <?php echo($change); ?></td>
                </tr>
                <tr>
                  <td class="text-right" nowrap=""> Payment Type 1:</td>
                  <td><?php echo($payment1_method); ?></td>
                </tr>
                <tr>
                  <td class="text-right"> Payment Type 2:</td>
                  <td><?php echo($payment2_method); ?></td>
                </tr>
              </table>
            </div>
          </div>
          