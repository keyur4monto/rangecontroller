<script src="/xapp/js/multifilter.min.js"></script>
<script>


$(document).ready(function() {

$("#popupContent").show();

$('.myfilter').multifilter({
    'target' : $('#main-table')
  });


//alert("Test!");

});

function clearFilters() {

	window.location = '/xapp/inventory';
	
}

var inventory_id = 0;

function deleteItem (item_id) {

inventory_id = item_id;

$( "#dialog" ).dialog( "open" );

$("#shadow").show();

}



</script>

<!-- ui-dialog -->

<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
  <p id="popupContent" style="display:none;">Are you sure you want to delete this item from inventory?</p>
</div>
<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right">
    <input type="button" value="Bulk Edit" onclick="window.location='/xapp/inventory/bulk'" />
    <input type="button" value="Add" onClick="window.location='inventory/add'">
  </div>
  <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="height:53px;">
    <div id="typetext" class="grid_3" style="width: 160px;">Filter by type<br>
      <select data-col="type" class="myfilter">
        <option></option>
        <?php echo($inventory_types); ?>
      </select>
    </div>
    <div class="grid_3" style="width: 160px;">Filter by item #<br>
      <input id="itemtext" type="text" data-col="item #" class="myfilter" style="width: 100px;">
    </div>
    <div class="grid_3" style="width: 160px;">Filter by brand :<br>
      <input id="brandtext" type="text"  data-col="brand" class="myfilter" style="width: 100px;padding:0;margin:0;">
    </div>
    <div class="grid_3" style="width: 160px;">Filter by model :<br>
      <input id="modeltext" type="text"  data-col="model" class="myfilter" style="width: 100px;">
    </div>
    <div class="grid_3" style="width: 125px;">
      <input type="button" value="Show All" onclick="clearFilters()" style="padding: 15px;">
    </div>
  </div>
  <div class="grid_12 margt">
    <table id="main-table" style="width: 100%;">
      <thead>
        <tr>
          <th>Type</th>
          <th>Item #</th>
          <th>Brand</th>
          <th>Model</th>
          <th>Description</th>
          <th>Qty</th>
          <th>Price</th>
          <th colspan="3">Action</th>
        </tr>
      </thead>
      <tbody>
        	<?php echo $inventoryRows; ?>
            <tr id="norecords" <?php echo (empty($inventoryRows)) ? "" : "style='display:none'"; ?>>
            	<td colspan="9" style="color:#F00"><center>No Records Found</center></td>
            </tr>
      </tbody>
    </table>
  </div>
</div>
<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				window.location="/xapp/inventory/delete/" + inventory_id;
				
				
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

</script> 
