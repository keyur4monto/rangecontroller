
<script>

$(function(){
        $('#startd, #endd').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     
		
    });

	
</script>

<script>

function submitTypeForm () {

	document.getElementById("typeForm").submit();

}


function checkDateForm () {

	if (document.getElementById("startd").value != '' && document.getElementById("endd").value != '') document.getElementById("dateForm").submit();

}

function toggle(num) {

$("#graph" + num).toggle();

}

</script>


          <div class="grid_3 text-right" id="" style="float:right;position:relative;top:-20px;"> <span style="font-size: 22px; font-weight: bold;"><?php echo($type); ?> REPORT</span><br>
            <?php echo($from); ?>&nbsp;
            <?php echo($to); ?></div>
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"> </div>
            <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="height:54px;">
              <div class="grid_4">Report Type:<br>
                
              <form id="typeForm" action="" method="post">
              
              <select name="type" onchange="submitTypeForm()">
                <option value=""></option>
                <option value="DAILY">Daily Report</option>
                <option value="WEEKLY">Weekly Report</option>
                <option value="MONTHLY">Monthly Report</option>
                <option value="YEARLY">Yearly Report</option>
              </select>
              
            </form>
            
              </div>
              <div class="grid_4">Date Range:<br>
                
            <form id="dateForm" action="" method="post">
            
              From:
              <input id="startd" name="from" type="text" value="" oninput="checkDateForm()" onchange="checkDateForm()" style="width: 75px;">
              To:
              <input id="endd" name="to" type="text" value="" oninput="checkDateForm()" onchange="checkDateForm()" style="width: 75px;">
              
              </form>
              
              </div>
              <div class="grid_4">
                <input type="button" onclick="window.location='/xapp/reports/rangeUsageGraph'" value="SHOW ALL" style="padding: 15px;">
              <input type="button" onclick="window.print()" style="padding: 15px;" value="PRINT">
              </div>
            </div>
            <div class="grid_12 margt"><br>
              
			 <center> 
			  
			  <h4 style="text-align:left;">Key : <span style="color:rgba(220,20,20,1)">Members</span>, <span style="color:rgba(255,204,20,1)">Visitors</span>, <span style="color:rgba(255,102,20,1)">Students</span><span style="float:right;">Time of Day : <input type="checkbox" checked="yes" onclick="toggle(1)"/> &nbsp;|&nbsp;Weekday : <input type="checkbox" checked="yes" onclick="toggle(2)" /> &nbsp;|&nbsp;Month-by-Month : <input type="checkbox" checked="yes" onclick="toggle(3)" /> &nbsp;|&nbsp;Year-Over-Year : <input type="checkbox" checked="yes" onclick="toggle(4)" /></span><h4>
			  <br><br><br>
			  
			  <div id="graph1"><h1>Time of Day Graph</h1>
			  
			  <canvas id="myChart" width="800" height="400"></canvas><br></div>
			  
			  <div id="graph2"><h1>Weekday Graph</h1>
			  
			  <canvas id="myChart4" width="800" height="400"></canvas><br></div>
			  
			  <div id="graph3"><h1>Month-by-Month Graph</h1>
			  
			  <canvas id="myChart2" width="800" height="400"></canvas><br></div>
			  
			  <div id="graph4"><h1>Year-Over-Year Graph</h1>
			  
			  <canvas id="myChart3" width="800" height="400"></canvas><br></div>
			  
			  </center>
			  <script>
			  
			  var data = {
    labels: ["8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,20,20,0.5)",
            strokeColor: "rgba(220,20,20,0.8)",
            highlightFill: "rgba(220,20,20,0.75)",
            highlightStroke: "rgba(220,20,20,1)",
            <?php echo ($memberData); ?>
		},
		{
			label: "My Second dataset",
            fillColor: "rgba(255,204,20,0.5)",
            strokeColor: "rgba(255,204,20,0.8)",
            highlightFill: "rgba(255,204,20,0.75)",
            highlightStroke: "rgba(255,204,20,1)",
            <?php echo ($visitorData); ?>
		},
		{	
			label: "My Third dataset",
            fillColor: "rgba(255,102,20,0.5)",
            strokeColor: "rgba(255,102,20,0.8)",
            highlightFill: "rgba(255,102,20,0.75)",
            highlightStroke: "rgba(255,102,20,1)",
            <?php echo ($studentData); ?>
		},			
    ]
};
	
var data2 = {
    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,20,20,0.5)",
            strokeColor: "rgba(220,20,20,0.8)",
            highlightFill: "rgba(220,20,20,0.75)",
            highlightStroke: "rgba(220,20,20,1)",
            <?php echo ($memberMonthData); ?>
		},
		{
			label: "My Second dataset",
            fillColor: "rgba(255,204,20,0.5)",
            strokeColor: "rgba(255,204,20,0.8)",
            highlightFill: "rgba(255,204,20,0.75)",
            highlightStroke: "rgba(255,204,20,1)",
            <?php echo ($visitorMonthData); ?>
		},
		{	
			label: "My Third dataset",
            fillColor: "rgba(255,102,20,0.5)",
            strokeColor: "rgba(255,102,20,0.8)",
            highlightFill: "rgba(255,102,20,0.75)",
            highlightStroke: "rgba(255,102,20,1)",
            <?php echo ($studentMonthData); ?>
		},			
    ]
};
			  
			  
var data3 = {
    labels: ["2013", "2014", "2015"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,20,20,0.5)",
            strokeColor: "rgba(220,20,20,0.8)",
            highlightFill: "rgba(220,20,20,0.75)",
            highlightStroke: "rgba(220,20,20,1)",
            <?php echo ($memberYearData); ?>
		},
		{
			label: "My Second dataset",
            fillColor: "rgba(255,204,20,0.5)",
            strokeColor: "rgba(255,204,20,0.8)",
            highlightFill: "rgba(255,204,20,0.75)",
            highlightStroke: "rgba(255,204,20,1)",
            <?php echo ($visitorYearData); ?>
		},
		{	
			label: "My Third dataset",
            fillColor: "rgba(255,102,20,0.5)",
            strokeColor: "rgba(255,102,20,0.8)",
            highlightFill: "rgba(255,102,20,0.75)",
            highlightStroke: "rgba(255,102,20,1)",
            <?php echo ($studentYearData); ?>
		},			
    ]
};
		
					  
var data4 = {
    labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,20,20,0.5)",
            strokeColor: "rgba(220,20,20,0.8)",
            highlightFill: "rgba(220,20,20,0.75)",
            highlightStroke: "rgba(220,20,20,1)",
            <?php echo ($memberWeekdayData); ?>
		},
		{
			label: "My Second dataset",
            fillColor: "rgba(255,204,20,0.5)",
            strokeColor: "rgba(255,204,20,0.8)",
            highlightFill: "rgba(255,204,20,0.75)",
            highlightStroke: "rgba(255,204,20,1)",
            <?php echo ($visitorWeekdayData); ?>
		},
		{	
			label: "My Third dataset",
            fillColor: "rgba(255,102,20,0.5)",
            strokeColor: "rgba(255,102,20,0.8)",
            highlightFill: "rgba(255,102,20,0.75)",
            highlightStroke: "rgba(255,102,20,1)",
            <?php echo ($studentWeekdayData); ?>
		},			
    ]
};
		  
			  
// Get the context of the canvas element we want to select
				var ctx = document.getElementById("myChart").getContext("2d");
				var myNewChart = new Chart(ctx).Bar(data);
				
				var ctx = document.getElementById("myChart2").getContext("2d");
				var myNewChart2 = new Chart(ctx).Bar(data2);
				
				var ctx = document.getElementById("myChart3").getContext("2d");
				var myNewChart3 = new Chart(ctx).Bar(data3);
				
				var ctx = document.getElementById("myChart4").getContext("2d");
				var myNewChart4 = new Chart(ctx).Bar(data4);
			  
			 </script>
			  
            </div>
          </div>
		  
		  
        