<script>
$(function(){
    $('#startd, #endd').datepicker({
        'showAnim': 'slideDown',
        dateFormat: 'mm/dd/yy',
        changeMonth: true,
        changeYear:true
    });     
});
</script>
<script>
function submitTypeForm () {
    document.getElementById("typeForm").submit();
}
function checkDateForm () {
    if (document.getElementById("startd").value != '' && document.getElementById("endd").value != '') document.getElementById("dateForm").submit();
}
function toggle(num) {
    $("#graph" + num).toggle();
}
</script>
<div class="grid_3 text-right" id="" style="float:right;position:relative;top:-20px;"> <span style="font-size: 22px; font-weight: bold;"><?php echo($type); ?> REPORT</span><br>
    <?php echo($from); ?>&nbsp;
    <?php echo($to); ?>
</div>
<div class="clear"></div>
<div class="grid_12">
    <div class="text-right"> </div>
    <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="height:54px;">
      <div class="grid_2">&nbsp;
        <!--Select Range:<br>
        <select>
          <option>Range 1</option>
          <option>Range 2</option>
          <option>Range 3</option>
        </select>-->
      </div>
        <div class="grid_2">Report Type:<br>
            <form id="typeForm" action="" method="post">
                <select name="type" onchange="submitTypeForm()">
                    <option value=""></option>
                    <option value="DAILY">Daily Report</option>
                    <option value="WEEKLY">Weekly Report</option>
                    <option value="MONTHLY">Monthly Report</option>
                    <option value="YEARLY">Yearly Report</option>
                </select>
            </form>
        </div>
        <div class="grid_5">Date Range:<br>
            <form id="dateForm" action="" method="post">
                From:
                <input id="startd" name="from" type="text" value="" oninput="checkDateForm()" onchange="checkDateForm()" style="width: 75px;">
                To:
                <input id="endd" name="to" type="text" value="" oninput="checkDateForm()" onchange="checkDateForm()" style="width: 75px;">
            </form>
        </div>
        <div class="grid_3">
            <input type="button" onclick="window.location='/xapp/reports/rangeUsageGraphLane'" value="SHOW ALL" style="padding: 15px;">
            <input type="button" onclick="window.print()" style="padding: 15px;" value="PRINT">
        </div>
    </div>
    <div class="grid_12 margt">
        <center> 
        <br><!--<h1 style="color: #fbb91f;">
          Range 1
          </h1>-->
        <div id="graph1">
            <h1>Lane Usage by Visit</h1>
            <canvas id="myChart" width="800" height="400"></canvas><br>
        </div>
        </center>
        <script>
            var data = {
                <?php echo ($lane_number); ?>,
                datasets: [
                    {
                    label: "My First dataset",
                    fillColor: "rgba(220,20,20,0.5)",
                    strokeColor: "rgba(220,20,20,0.8)",
                    highlightFill: "rgba(220,20,20,0.75)",
                    highlightStroke: "rgba(220,20,20,1)",
                    <?php echo ($lane_number_data); ?>
                    },			
                ]
            };
            // Get the context of the canvas element we want to select
            var ctx = document.getElementById("myChart").getContext("2d");
            var myNewChart = new Chart(ctx).Bar(data);
        </script>
        <?php ?>
        <center> 
        <br><br><br>
        <div id="graph1">
            <h1>Lane Usage by Hour</h1>
            <canvas id="myChart_2" width="800" height="400"></canvas><br>
        </div>        
        </center>
        <script>
            var data = {
                <?php echo ($lane_number); ?>,
                datasets: [
                    {
                    label: "My First dataset",
                    fillColor: "rgba(220,20,20,0.5)",
                    strokeColor: "rgba(220,20,20,0.8)",
                    highlightFill: "rgba(220,20,20,0.75)",
                    highlightStroke: "rgba(220,20,20,1)",
                    <?php echo ($lane_number_of_hour_used); ?>
                    },			
                ]
            };
            // Get the context of the canvas element we want to select
            var ctx = document.getElementById("myChart_2").getContext("2d");
            var myNewChart = new Chart(ctx).Bar(data);
        </script>
        <?php ?>
    </div>
</div>