<?php
use Rc\Services\DbHelper;
$link = DbHelper::getWrappedHandle($app['dbs']['main']);
$show_hide_sql = "";
$show_hide_sql = "SELECT point_of_sale, firearm_rentals FROM show_hide WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' "; 
if ($result = $link->query($show_hide_sql)) {   
while ($row = $result->fetch_assoc()) { 
$point_of_sale = $row['point_of_sale']; 
$firearm_rentals = $row['firearm_rentals']; 
}//END WHILE LOOP 
}//END IF
?>
<script src="https://www.rangecontroller.com/js/sorttable.js"></script>
<style>
@media print {
.no-print, .no-print * {
	display: none !important;
}
}
.text-center {
	text-align: center;
"
}
.padtb {
	padding-top: 1em;
	padding-bottom: 4em;
}
.padlr {
	padding-left: 1em;
	padding-right: 1em;
}
.margt {
	margin-top: 2em;
}
.lightblackbg {
	background: #666;
	height: 50px;
}
</style>
 <script>
 
	function filterMembershipType(type) {
	
	window.location = "/xapp/reports/memberaddress/type/" + type;
	
	}
	
	function filtershort(no) 
	{
		var types = '';
		var mnths = '';
		var yrs = '';
		var types = document.getElementById('h_by_membership_type');
		var mnths = document.getElementById('h_by_mnth');
		var yrs = document.getElementById('h_by_yr');
		
		var type = '';
		var m = '';
		var y = '';
		var type = types.value;
		var m = mnths.value;
		var y = yrs.value;
		if(type != "null")
		{
		window.location = "/xapp/reports/memberaddress/type/" + type + "/shortno/" + no;
		}
		else
		{
		window.location = "/xapp/reports/memberaddress/month/" + m + "/year/" + y + "/shortno/" + no;
		}
	}
	
	function filterByDate(m, y) {
	
	
	if (m == "blank" && y == "blank") alert("Please specify a month and a year!"); 
	
	else window.location = "/xapp/reports/memberaddress/month/" + m + "/year/" + y;
	
	}
	function memberaddressprintlable() 
	{
		var checkboxes = document.getElementsByName('membership_number');
		var all_checkboxes = document.getElementById('chk');
		var selected = [];
		for (var i=0; i<checkboxes.length; i++) 
		{
			if (checkboxes[i].checked) 
			{
				selected.push(checkboxes[i].value);
				checkboxes[i].checked = false;
				all_checkboxes.checked = false;
			}
		}
		if(selected == '')
		{
			alert('Please select any one checkbox to print lable');
		}
		else
		{
			window.open('/xapp/reports/memberAddressPrintLabel/' + selected,'_blank');
		}
	}
	
	function checkAll(ele) {
     var checkboxes = document.getElementsByName('membership_number');
	 
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }	
</script>
<div class="grid_12">
    <div class="text-right">
    </div>
</div>
    <div class="container_12">
    <div class="clear"></div>
          <div class="text-right"> </div>
          <br>
          <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="height:52px;">
			<!--<div class="grid_2">
			Shorting :<br>
            <select id="short" onChange="filtershort(this.value);">
              <option value="50" <?php //echo ($shortno == "50") ? 'selected=""' : ''; ?>>50</option>
              <option value="100" <?php //echo ($shortno == "100") ? 'selected=""' : ''; ?>>100</option>
              <option value="500" <?php //echo ($shortno == "500") ? 'selected=""' : ''; ?>>500</option>
              <option value="all" <?php //echo ($shortno == "all") ? 'selected=""' : ''; ?>>All</option>
            </select>
            </div>-->
            <div class="grid_4">Filter by Membership Type:<br>
			<?php 
			if($type)
			{
			?>
			<input type="hidden" id="h_by_membership_type" name="h_by_membership_type" value="<?php echo $type; ?>"/>
			<?php 
			}
			else
			{
			?>
			<input type="hidden" id="h_by_membership_type" name="h_by_membership_type" value="null"/>
			<?php 
			}
			?>
            <input type="hidden" id="h_by_membership" name="h_by_membership"/>
            <select class="type" id="by_membership type" title="Filter by Membership Type" onChange="filterMembershipType(this.value);">
              <option value="blank"></option>
              <option value="all" <?php echo ($type == 'all') ? 'selected=""' : ''; ?>>Show All</option>
            <?php
				$sql = "";
				$sql = "SELECT membership_name FROM editable_memberships WHERE acctnumberhashed = '" . $_SESSION['account_number_hashed'] . "' AND accountnumber = '" . $_SESSION['account_number'] . "' ORDER BY membership_name ASC";
				if ($result = $link->query($sql)) {  
					while ($row = $result->fetch_assoc()) {
					?>
						<option value="<?php echo $row['membership_name']; ?>" <?php echo ($row['membership_name']==$type) ? 'selected=""' : ''; ?>><?php echo $row['membership_name']; ?></option>
					<?php
					}
				}
			?>
            </select>
            </div>
			<?php 
			if($year)
			{
			?>
			<input type="hidden" id="h_by_yr" name="h_by_yr" value="<?php echo $year; ?>"/>
			<?php 
			}
			else
			{
			?>
			<input type="hidden" id="h_by_yr" name="h_by_yr" value="null"/>
			<?php 
			}
			if($month)
			{
			?>
			<input type="hidden" id="h_by_mnth" name="h_by_mnth" value="<?php echo $month; ?>"/>
			<?php 
			}
			else
			{
			?>
			<input type="hidden" id="h_by_mnth" name="h_by_mnth" value="null"/>
			<?php 
			}
			?>
            <div class="grid_5">
			Filter by Registration Date<br>
            Month:
            <select id="mnth">
              <option value="blank" <?php echo ($month == "blank") ? 'selected=""' : ''; ?>></option>
              <option value="1" <?php echo ($month == "1") ? 'selected=""' : ''; ?>>January</option>
              <option value="2" <?php echo ($month == "2") ? 'selected=""' : ''; ?>>February</option>
              <option value="3" <?php echo ($month == "3") ? 'selected=""' : ''; ?>>March</option>
              <option value="4" <?php echo ($month == "4") ? 'selected=""' : ''; ?>>April</option>
              <option value="5" <?php echo ($month == "5") ? 'selected=""' : ''; ?>>May</option>
              <option value="6" <?php echo ($month == "6") ? 'selected=""' : ''; ?>>June</option>
              <option value="7" <?php echo ($month == "7") ? 'selected=""' : ''; ?>>July</option>
              <option value="8" <?php echo ($month == "8") ? 'selected=""' : ''; ?>>August</option>
              <option value="9" <?php echo ($month == "9") ? 'selected=""' : ''; ?>>September</option>
              <option value="10" <?php echo ($month == "10") ? 'selected=""' : ''; ?>>October</option>
              <option value="11" <?php echo ($month == "11") ? 'selected=""' : ''; ?>>November</option>
              <option value="12" <?php echo ($month == "12") ? 'selected=""' : ''; ?>>December</option>
            </select>
            Year:
            <select id="yr">
              <option value="blank" <?php echo ($year == "blank") ? 'selected=""' : ''; ?>></option>
              <option value="2013" <?php echo ($year == "2013") ? 'selected=""' : ''; ?>>2013</option>
              <option value="2014" <?php echo ($year == "2014") ? 'selected=""' : ''; ?>>2014</option>
              <option value="2015" <?php echo ($year == "2015") ? 'selected=""' : ''; ?>>2015</option>
              <option value="2016" <?php echo ($year == "2016") ? 'selected=""' : ''; ?>>2016</option>
            </select>
            <input type="button" value="Go" onClick="filterByDate(document.getElementById('mnth').value, document.getElementById('yr').value)">
            </div>
            <div class="grid_3">
              <input type="button" onclick="memberaddressprintlable()" style="padding: 15px;" value="PRINT LABELS">
            </div>
          </div>
      <br>
      <br>
      <table style="width: 100%; font-size: 12px;" class="sortable">
        <thead>
          <tr>
            <td nowrap="">Membership Type</td>
            <td>Member #</td>
            <td nowrap="">Name</td>
            <td nowrap="">Address</td>
            <td>City</td>
            <td>State</td>
            <td>Zipcode</td>
            <td>Visits</td>
            <td>Reg. Date</td>
			<td class="sorttable_nosort"><INPUT type="checkbox" id="chk" onchange="checkAll(this)" name="chk[]" /></td>
          </tr>
        </thead>
        <tbody>
          <?php echo $output; ?>
            <tr id="norecords" <?php echo (empty($output)) ? "" : "style='display:none'"; ?>>
            	<td colspan="10" style="color:#F00"><center>Apply filter above to see results.</center></td>
            </tr>
        </tbody>
      </table>
      </
      <br>
      <br></td>
  </tr>
</table>