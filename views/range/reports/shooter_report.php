    <script src="https://www.rangecontroller.com/js/sorttable.js">
    </script>
    <script>

    if (history.pushState) {
    var newurl = window.location.protocol + "//" + window.location.host + '/xapp/reports/shooter/0/0/unfiltered/<?php echo ($shooter_id); ?>';
    window.history.pushState({path:newurl},'',newurl);
    }

    function hideToggle (myVar, myClass) {

    $("." + myClass).toggle();

    if (myVar.text == "Hide") myVar.text = "Show";

    else if (myVar.text == "Show") myVar.text = "Hide";

    }

    function customReport () {

    if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("CUSTOM");

    }

    function weeklyReport () {

    document.getElementById("startd").value = printDate(lastweek);

    document.getElementById("endd").value = printDate(today);

    if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("WEEKLY");

    }

    function dailyReport () {

    document.getElementById("startd").value = printDate(today);

    document.getElementById("endd").value = printDate(today);

    if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("DAILY");

    }

    function monthlyReport () {

    document.getElementById("startd").value = printDate(lastmonth);

    document.getElementById("endd").value = printDate(today);

    if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("MONTHLY");

    }

    function yearlyReport () {

    document.getElementById("startd").value = printDate(lastyear);

    document.getElementById("endd").value = printDate(today);

    if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("YEARLY");

    }

    function submitForm (name) {

    var buffer = document.getElementById("startd").value;

    for (var i = 0;i< 5;i++) buffer = buffer.replace("/", "%2F");

    document.getElementById("startd").value = buffer;

    buffer = document.getElementById("endd").value;

    for (var i = 0;i< 5;i++) buffer = buffer.replace("/", "%2F");

    document.getElementById("endd").value = buffer;

    var myurlstring = "" + (encodeURIComponent(document.getElementById("startd").value)).replace(".", "%2E") + "/" + (encodeURIComponent((document.getElementById("endd").value))).replace("\x0A", "%3Cbr%3E") + "/" + name;


    for (var i = 0;i< 20;i++) myurlstring = myurlstring.replace("%0A", "%3Cbr%3E");

    for (var i = 0;i< 20;i++) myurlstring = myurlstring.replace(".", "%2E");

    myurlstring = "/xapp/reports/shooter/" + myurlstring + "/<?php echo($shooter_id); ?>";

    window.location = myurlstring;

    }

    function printDate (tday) {

    //var tday = new Date();
    var dd = tday.getDate();
    var mm = tday.getMonth()+1; //January is 0!
    var yyyy = tday.getFullYear();

    if(dd<10) {
    dd='0'+dd
    } 

    if(mm<10) {
    mm='0'+mm
    } 

    tday = mm+'/'+dd+'/'+yyyy;

    return tday;

    }



    var today = new Date(); 

    var lastweek = new Date();

    var lastmonth = new Date();

    var lastyear = new Date();

    var d = new Date();

    lastweek.setTime(today.getTime() - (86400000 * 6));

    lastmonth.setTime(today.getTime() - (86400000 * 31));

    lastyear.setTime(today.getTime() - (86400000 * 365));

    //printDate(lastweek);

    //printDate(lastmonth);

    //printDate(lastyear);

    $(function(){
        $('#startd, #endd').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     
        
    });


    </script><?php
    if(!$isempty)
    {
    ?>
    <style>
    table.sortable tbody {
    counter-reset: sortabletablescope;
    }
    table.sortable thead tr::before {
    content: "Row";
    display: table-cell;
    padding: 0.38em 0.62em;
    border: 1px solid #333333;
    border-collapse: collapse;
     font-size: 14px;
    }
    table.sortable tbody tr::before {
    content: counter(sortabletablescope);
    counter-increment: sortabletablescope;
    display: table-cell;
    padding: 0.38em 0.62em;
    border: 1px solid #333333;
    border-collapse: collapse;
    }
    </style><?php
    }
    ?>
    <title></title>
</head>
<body>
    <div class="grid_3 text-right" id="" style=
    "float:right;position:relative;top:-20px;">
        <span style=
        "font-size: 22px; font-weight: bold;"><?php echo ($report_type); ?>
        REPORT</span><br>
        <?php if ($start_date != 0) echo ($start_date . " to " . $end_date); else echo ("Choose a report type."); ?>
    </div>
    <div class="clear"></div>
    <div class="grid_12">
        <div class="text-right"></div>
        <div class="text-center padtb padlr margt lightblackbg hideFromPrint"
        style="height:54px;">
            <div class="grid_4" style="float:left;">
                Report Type:<br>
                <select onchange=
                "if (this.value == 'custom') showDateRange(); else if (this.value == 'weekly') weeklyReport(); else if (this.value == 'monthly') monthlyReport(); else if (this.value == 'daily') dailyReport(); else if (this.value == 'yearly') yearlyReport();">
                <option>
                        </option>
                    <option value="daily">
                        Daily Report
                    </option>
                    <option value="weekly">
                        Weekly Report
                    </option>
                    <option value="monthly">
                        Monthly Report
                    </option>
                    <option value="yearly">
                        Yearly Report
                    </option>
                    <option value="custom">
                        Custom Report
                    </option>
                </select>
            </div>
            <div class="grid_4" id="rangee" style=
            "display:none;float:left;opacity:0.0;">
                Date Range:<br>
                From: <input id="startd" onchange="customReport()" style=
                "width: 75px;" type="text"> To: <input id="endd" onchange=
                "customReport()" style="width: 75px;" type="text">
            </div>
            <div class="grid_4" style="float:right;">
                <!--<input type="button" onclick="window.location = '/xapp/reports/shooter/0/0/unfiltered'" value="SHOW ALL" style="padding: 15px;">-->
                <input onclick="window.print()" style="padding: 15px;" type=
                "button" value="PRINT">
            </div>
        </div>
        <div class="grid_12 margt">
            <br>
            <table class="text-center sortable sorttable" style=
            "font-size:14px;">
                <thead>
                    <tr>
                        <!--<th style="width:10px;">Row</th>-->
                        <th>Lane</th>
                        <th nowrap="0">Membership Type</th>
                        <th nowrap="0">Member #</th>
                        <th nowrap="0">Name</th>
                        <th nowrap="0">Start Time</th>
                        <th nowrap="0">End Time</th>
                        <th>Duration</th>
                        <th>Date</th>
                        <th style="width:30px;">Emp.</th>
                    </tr>
                </thead>
                <tbody>
                    <?php echo $shooter_rows; ?>
                    <?php if(empty($shooter_rows)) { ?>
                    <tr id="norecords">
                        <td colspan="9" style="color:#F00">
                            <center>
                                No Records Found
                            </center>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        
        //$(".customer ").css("position", "absolute"); 
        
        
        
        function showDateRange () {$("#rangee").fadeTo(1200, 1.0);}
        
    </script>
