
<script>

var member_no = 0;

function sendRenewalMail (mem_no, name) {

//popup dialog then set mem_no

	member_no = mem_no;
	
	document.getElementById("popupContent").innerHTML = "Are you sure you want to send a renewal email to " + name + "?";
	
	$( "#dialog" ).dialog("open");
	
	$("#shadow").show();

}

function pushMail () {

	var xmlHttp = null;
	
	var myurlstring = "/xapp/sendRenewalMail/" + member_no;

    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", myurlstring, false );
    xmlHttp.send( null );
    
    //alert(xmlHttp.responseText);

	window.location = "/xapp/reports/membershipRenewal/<?php echo($month . '/' . $year); ?>";

}

function dateForm () {

	document.getElementById("dateForm").submit();

}

</script>



<script src="https://www.rangecontroller.com/js/sorttable.js"></script>

		  
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Really send a renewal email to this member?</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

		  
		  


          <div class="grid_3 text-right" style="float:right;position:relative;top:-20px;"> <span style="font-size: 22px; font-weight: bold;"><?php echo($month . " " . $year); ?></span><br>
             </div>
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"> </div>
            <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="height:55px;">
              <div class="grid_6">Filter Expiration Dates:<br>
              
              <form id="dateForm" action="" method="post">
              
                <select name="month2">
                  <option value="">Month</option>
                  <option value="January">January</option>
                  <option value="February">February</option>
                  <option value="March">March</option>
                  <option value="April">April</option>
                  <option value="May">May</option>
                  <option value="June">June</option>
                  <option value="July">July</option>
                  <option value="August">August</option>
                  <option value="September">September</option>
                  <option value="October">October</option>
                  <option value="November">November</option>
                  <option value="December">December</option>
                </select>
                <select name="year2" onchange="dateForm()">
                  <option value="">Year</option>
                  <option value="2013">2013</option>
                  <option value="2014">2014</option>
                  <option value="2015">2015</option>
                  <option value="2016">2016</option>
                </select>
                
                </form>
                
              </div>
              <div class="grid_6">
                <input type="button" onclick="window.location='/xapp/reports/membershipRenewal/January/2015'"  value="SHOW ALL" style="padding: 15px;">
                <input type="button" onclick="window.print()" style="padding: 15px;" value="PRINT">
              </div>
            </div>
            <div class="grid_12 margt"><br>
              <table class="text-center sortable">
              <thead>
                <tr style="font-size: 14px;">
                  <th></th>
                  <th>Membership Type</th>
                  <th>Name</th>
                  <th>Membership #</th>
                  <th>Visits</th>
                  <th><span style="font-size: 10px;">Sorted by</span><br>Exp Date</th>
                  <th>Action</th>
                  <th>Date Sent</th>
                </tr>
                </thead>
                <tbody>
                 <?php echo $rows; ?>
            <tr id="norecords" <?php echo (empty($rows)) ? "" : "style='display:none'"; ?>>
            	<td colspan="8" style="color:#F00"><center>No Records Found</center></td>
            </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
        <script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				pushMail();
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				
				$("#shadow").hide();
				
			}
		}
	]
});

</script>

