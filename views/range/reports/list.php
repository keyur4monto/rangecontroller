<?php
require('xapp/common/constants.php');
require('xapp/common/security.php');
$link = new mysqli(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE);
$show_hide_sql = "";
$show_hide_sql = "SELECT point_of_sale, firearm_rentals FROM show_hide WHERE acctnumberhashed = '".$_SESSION['account_number_hashed']."' AND accountnumber = '".$_SESSION['account_number']."' "; 
if ($result = $link->query($show_hide_sql)) {   
while ($row = $result->fetch_assoc()) { 
$point_of_sale = $row['point_of_sale']; 
$firearm_rentals = $row['firearm_rentals']; 
}//END WHILE LOOP 
}//END IF
?>
<script>

var nickType = 'sales';
var nickType = 'employee';
function submitForm (name) {

var buffer = document.getElementById("startd").value;

for (var i = 0;i< 5;i++) buffer = buffer.replace("/", "%2F");

document.getElementById("startd").value = buffer;

buffer = document.getElementById("endd").value;

for (var i = 0;i< 5;i++) buffer = buffer.replace("/", "%2F");

document.getElementById("endd").value = buffer;

var myurlstring = "" + (encodeURIComponent(document.getElementById("startd").value)).replace(".", "%2E") + "/" + (encodeURIComponent((document.getElementById("endd").value))).replace("\x0A", "%3Cbr%3E") + "/" + name;

var myurlstrings = "" + (encodeURIComponent(document.getElementById("startd").value)).replace(".", "%2E") + "/" + (encodeURIComponent((document.getElementById("endd").value))).replace("\x0A", "%3Cbr%3E") + "/" + name + "/" + 0;

for (var i = 0;i< 20;i++) myurlstring = myurlstring.replace("%0A", "%3Cbr%3E");

for (var i = 0;i< 20;i++) myurlstring = myurlstring.replace(".", "%2E");

if (nickType == 'sales') myurlstring = "/xapp/reports/sales/" + myurlstring;
else if (nickType == 'employee') myurlstring = "/xapp/reports/employee/" + myurlstrings;
else if (nickType == 'shooter') myurlstring = "/xapp/reports/shooter/" + myurlstring;

window.location = myurlstring;

}

function dailyReport (ntp) {

nickType = ntp;

document.getElementById("startd").value = printDate(today);

document.getElementById("endd").value = printDate(today);

if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("DAILY");

}

function printDate (tday) {

//var tday = new Date();
var dd = tday.getDate();
var mm = tday.getMonth()+1; //January is 0!
var yyyy = tday.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

tday = mm+'/'+dd+'/'+yyyy;

return tday;

}

var d = new Date();
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var n = month[d.getMonth()];
var thisyear = d.getFullYear();

var today = new Date(); 

</script>

<div class="grid_12">
  <div class="text-right"> <!--<span style="font-size: 22px;">REPORTS</span>--> </div>
</div>
<div class="grid_3">&nbsp;</div>
<div class="grid_6 margt">
  <table>
    <tr>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Complete Member List" class="navButton" onClick="window.location='reports/completeMembershipsReport';"/></td>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Membership Renewal" class="navButton" onClick="window.location='/xapp/reports/membershipRenewal/' + n + '/' + thisyear + ''"/></td>
    </tr>
    <tr>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Memberships" class="navButton" onClick="window.location='reports/membershipsReport';"/></td>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Sales Reports" class="navButton" onClick="dailyReport('sales')"/></td>
    </tr>
    <tr>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Range Usage" class="navButton" onClick="window.location='/xapp/reports/rangeUsageGraph';"/></td>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Shooter Reports" class="navButton" onClick="dailyReport('shooter')"/></td>
    </tr>
    <tr>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Member Address List" class="navButton" onClick="window.location='reports/memberaddress';"/></td>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Point of Sale" class="navButton" onClick="window.location='/xapp/reports/pos';"/></td>
    </tr>
    <tr>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Employee Reports" class="navButton" onClick="dailyReport('employee')"/></td>
      <td width="50%" align="center" class="noBorder"><input type="button" value="Lane Usage" class="navButton" onClick="window.location='reports/rangeUsageGraphLane';"/></td>
    </tr>
  </table>
  <input type="hidden" id="startd"/>
  <input type="hidden" id="endd"/>
</div>
<div class="grid_3">&nbsp;</div>
