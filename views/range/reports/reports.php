<script>

if (history.pushState) {
    var newurl = window.location.protocol + "//" + window.location.host + '/xapp/reports/sales/0/0/unfiltered';
    window.history.pushState({path:newurl},'',newurl);
}

function hideToggle (myVar, myClass) {

	$("." + myClass).toggle();
	
	if (myVar.text == "Hide") myVar.text = "Show";
	
	else if (myVar.text == "Show") myVar.text = "Hide";

}

function customReport () {

if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("CUSTOM");

}

function weeklyReport () {

document.getElementById("startd").value = printDate(lastweek);

document.getElementById("endd").value = printDate(today);

if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("WEEKLY");

}

function dailyReport () {

document.getElementById("startd").value = printDate(today);

document.getElementById("endd").value = printDate(today);

if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("DAILY");

}

function monthlyReport () {

document.getElementById("startd").value = printDate(lastmonth);

document.getElementById("endd").value = printDate(today);

if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("MONTHLY");

}

function yearlyReport () {

document.getElementById("startd").value = printDate(lastyear);

document.getElementById("endd").value = printDate(today);

if (document.getElementById("startd").value != "" && document.getElementById("endd").value) submitForm("YEARLY");

}

function submitForm (name) {

var buffer = document.getElementById("startd").value;

for (var i = 0;i< 5;i++) buffer = buffer.replace("/", "%2F");

document.getElementById("startd").value = buffer;

buffer = document.getElementById("endd").value;

for (var i = 0;i< 5;i++) buffer = buffer.replace("/", "%2F");

document.getElementById("endd").value = buffer;

var myurlstring = "" + (encodeURIComponent(document.getElementById("startd").value)).replace(".", "%2E") + "/" + (encodeURIComponent((document.getElementById("endd").value))).replace("\x0A", "%3Cbr%3E") + "/" + name;


for (var i = 0;i< 20;i++) myurlstring = myurlstring.replace("%0A", "%3Cbr%3E");

for (var i = 0;i< 20;i++) myurlstring = myurlstring.replace(".", "%2E");

myurlstring = "/xapp/reports/sales/" + myurlstring;

window.location = myurlstring;

}

function printDate (tday) {

//var tday = new Date();
var dd = tday.getDate();
var mm = tday.getMonth()+1; //January is 0!
var yyyy = tday.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

tday = mm+'/'+dd+'/'+yyyy;

return tday;

}



var today = new Date(); 

var lastweek = new Date();

var lastmonth = new Date();

var lastyear = new Date();

var d = new Date();

lastweek.setTime(today.getTime() - (86400000 * 6));

lastmonth.setTime(today.getTime() - (86400000 * 31));

lastyear.setTime(today.getTime() - (86400000 * 365));

//printDate(lastweek);

//printDate(lastmonth);

//printDate(lastyear);

$(function(){
        $('#startd, #endd').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     
		
    });

	
</script>

</div><div class="container_12 reports">
          <div class="grid_9">
            <div class="breadcrumbs" style="display:none;z-index:-1 !important;pointer-events:none;"> <a href="#">Home </a> / <a href="#">Reports </a> / <span>Sales Report </span> </div>
          </div>
          <div class="grid_3 text-right" id="breadcrumbsRight" style="float:right;"> <span style="font-size: 22px; font-weight: bold;text-transform:uppercase;"><?php echo ($report_type); ?> REPORT</span><br>
            <br><?php if ($start_date != 0) echo ($start_date . " to " . $end_date); else echo ("Choose a report type."); ?></div>
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"> </div>
            <div class="text-center padtb padlr margt lightblackbg hideFromPrint">
              <div class="grid_4" style="float:left;">Report Type:<br>
                <select onchange="if (this.value == 'custom') showDateRange(); else if (this.value == 'weekly') weeklyReport(); else if (this.value == 'monthly') monthlyReport(); else if (this.value == 'daily') dailyReport(); else if (this.value == 'yearly') yearlyReport();">
				  <option></option>
                  <option value="daily">Daily Report</option>
                  <option value="weekly">Weekly Report</option>
                  <option value="monthly">Monthly Report</option>
                  <option value="yearly">Yearly Report</option>
                  <option value="custom">Custom Report</option>
                </select>
              </div>
              <div class="grid_4" id="rangee" style="display:none;float:left;opacity:0.0;">Date Range:<br>
                From:
                <input onchange="customReport()" type="text" id="startd" style="width: 75px;">
                To: 
                <input onchange="customReport()" type="text" id="endd" style="width: 75px;">
              </div>
              <div class="grid_4" style="float:right;">
                <input type="button" onclick="window.location = '/xapp/reports/sales/0/0/unfiltered'" value="SHOW ALL" style="padding: 15px;">
                <input type="button" onclick="window.print()" style="padding: 15px;" value="PRINT">
              </div>
            </div>
            <div class="grid_12 margt text-center"><br>
              <table>
                <tr>
                  <th colspan="7" style="background-color: #666666;">POINT OF SALE (<a href="#afa" onclick="hideToggle(this, 'pos_class');">Hide</a>)</th>
                </tr>
                <tr class="pos_class">
                  <th>#</th>
                  <th>Qty</th>
                  <th>Item</th>
                  <th>I-Number</th>
                  <th>Price</th>
                  <th>Total + Taxes</th>
                  <th>Discount Applied</th>
                </tr>
                <?php echo($pos_transaction_rows); ?>
                <tr>
                  <td class="text-right" colspan="7"><span style="color: red;">Grand Total: $<?php echo ($pos_grand_total); ?></span></td>
                </tr>
              </table>
              <table>
                <tr>
                  <th colspan="3" style="background-color: #666666;">MEMBERSHIPS SOLD (<a href="#afa" onclick="hideToggle(this, 'memberships_class');">Hide</a>)</th>
                </tr>
                <tr class="memberships_class">
                  <th>Membership Type</th>
                  <th>Total Sold</th>
                  <th>Sub Total</th>
                </tr>
				<?php echo ($memberships_transaction_rows); ?>
                <tr>
                  <td></td>
                  <td><span style="color: red;"><?php echo ($memberships_total_count); ?></span></td>
                  <td class="text-right"><span style="color: red;">Grand Total: $<?php echo ($memberships_grand_total); ?></span></td>
                </tr>
              </table>
              <table>
                <tr>
                  <th colspan="7" style="background-color: #666666;">TRANSACTIONS BY TYPE (<a href="#afa" onclick="hideToggle(this, 'type_class');">Hide</a>)</th>
                </tr>
                <tr class="type_class">
                  <th>Cash</th>
                  <th>Check</th>
                  <th>Debit</th>
                  <th>Discover</th>
                  <th>Mastercard</th>
                  <th>Visa</th>
				  <th>PayPal</th>
                </tr>
                <tr class="type_class">
                  <td>$<?php echo (number_format($cash, 2)); ?></td>
                  <td>$<?php echo (number_format($check, 2)); ?></td>
                  <td>$<?php echo (number_format($debit, 2)); ?></td>
                  <td>$<?php echo (number_format($discover, 2)); ?></td>
                  <td>$<?php echo (number_format($mastercard, 2)); ?></td>
                  <td>$<?php echo (number_format($visa, 2)); ?></td>
				  <td>$<?php echo (number_format($paypal, 2)); ?></td>
                </tr>
                <tr>
                  <td colspan="7" style="text-align: left;">Total: $<?php echo (number_format($cash + $check + $debit + $discover + $mastercard + $visa + $paypal, 2)); ?></td>
                </tr>
              </table>
              <table>
                <tr>
                  <th style="background-color: #666666;">TOTALS</th>
                </tr>
                <tr>
                  <td class="text-right"><span style="color: red;">Drawer End: $<?php echo ($drawer_end); ?></span></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
		
		<script>
		
		//$(".customer ").css("position", "absolute"); 
		
		
		
		function showDateRange () {$("#rangee").fadeTo(1200, 1.0);}
		
		</script>
		