<div class="grid_3 text-right" id="" style="float:right;position:relative;top:-20px;"> <span style="font-size: 22px; font-weight: bold;">POS INVENTORY REORDERS <!--TODAYS DATE--><?php echo date('m/d/Y')?></span>
<br>
</div>
<div class="clear"></div>
<div class="text-right">
</div>

<div class="grid_12 margt">
	<br>
	<table>
		<tr>
			<th>Inv #</th>
			<th>Item Type, Brand, Desc and Model</th>
			<th style="font-size: 10px;">Qty<br>On Hand</th>
			<th style="font-size: 10px;">Reorder<br>Qty</th>
			<th>Cost</th>
			<th>Ext</th>
		</tr>
		<?php
		$ext_array = array();
		foreach($assoc_array as $row)
		{
		$reorder_qty = $row['min_quantity'] - $row['quantity'];
		if($row['min_quantity'] > $row['quantity'])
		{
		?>
		<tr>
			<td nowrap="0">I-<?php echo $row['inventory_number'];?>
			</td>
			<td nowrap="0" style="font-size: 14px;"><?php echo $row['type'];?>  <?php echo $row['brand'];?>  <?php echo $row['description'];?>  <?php echo $row['model'];?>
			</td>
			<td style="text-align: center;"><?php echo $row['quantity'];?>
			</td>
			<td style="text-align: center; color: #fbb91f"><?php echo $reorder_qty;?>
			</td>
			<td style="text-align: right;">$<?php echo $row['cost'];?>
			</td>
			<?php
			$ext = $row['cost'] * $reorder_qty;
			$ext_array[] = $ext;
			?>
			<td style="text-align: right;">$<?php echo $ext; ?>
			</td>
		</tr>
		<?php
		}
		}
		?>
		
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		<tr style="font-weight: bold;">
			<td colspan="5" class="text-right"><span style="color: red;">Total Reorder Cost</span>
			</td>
			<td colspan="1" style="text-align: right;">$<?php echo array_sum($ext_array); ?></td>
		</tr>
	</table>
	</div>
	<div class="clear">&nbsp;</div>
</div>
</td>
</tr>
</tbody>
</table>