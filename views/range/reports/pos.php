
<script>

$(function(){
        $('#startd, #endd').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     
		
    });

	
</script>

<script>

function submitTypeForm () {

	document.getElementById("typeForm").submit();

}


function checkDateForm () {

	if (document.getElementById("startd").value != '' && document.getElementById("endd").value != '') document.getElementById("dateForm").submit();

}

function toggle(num) {

$("#graph" + num).toggle();

}

</script>


          
          <div class="grid_3 text-right" id="" style="float:right;position:relative;top:-20px;"> <span style="font-size: 22px; font-weight: bold;"><?php echo($type); ?> REPORT</span><br>
            <?php echo($from); ?>&nbsp;
            <?php echo($to); ?></div>
          <div class="clear"></div>
          <div class="text-right">
          </div>
          
		  <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="height:54px;">
              <div class="grid_4">Report Type:<br>
                
              <form id="typeForm" action="" method="post">
              
              <select name="type" onchange="submitTypeForm()">
                <option value=""></option>
                <option value="DAILY">Daily Report</option>
                <option value="WEEKLY">Weekly Report</option>
                <option value="MONTHLY">Monthly Report</option>
                <option value="YEARLY">Yearly Report</option>
              </select>
              
            </form>
            
              </div>
              <div class="grid_4">Date Range:<br>
                
            <form id="dateForm" action="" method="post">
            
              From:
              <input id="startd" name="from" type="text" value="" oninput="checkDateForm()" onchange="checkDateForm()" style="width: 75px;">
              To:
              <input id="endd" name="to" type="text" value="" oninput="checkDateForm()" onchange="checkDateForm()" style="width: 75px;">
              
              </form>
              
              </div>
              <div class="grid_4">
                <input type="button" onclick="window.location='/xapp/reports/pos/sales'" value="SHOW ALL" style="padding: 15px;">
              <input type="button" onclick="window.print()" style="padding: 15px;" value="PRINT">
              </div>
            </div>
            
          <div class="grid_12 margt"><br>
            <table>
				<tr>
					<th>Date</th>
					<th>Name</th>
					<th>Membership Type</th>
					<th>Amount</th>
					<th>Sale #</th>
					<th colspan="2">Actions</th>
				</tr>
				<?php echo($rows); ?>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr style="font-weight: bold;">
					<td colspan="3" class="text-right"><span style="color: red;">Grand Total</span></td>
					<td colspan="4">$<?php echo ($total) ; ?></td>
				</tr>
			</table>	
          </div>
        