
<script>

$(function(){
        $('#startd, #endd').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     
		
    });

	
</script>

<script>

function submitTypeForm () {

	document.getElementById("typeForm").submit();

}


function checkDateForm () {

	if (document.getElementById("startd").value != '' && document.getElementById("endd").value != '') document.getElementById("dateForm").submit();

}

function toggle(num) {

$("#graph" + num).toggle();

}

</script>


          
          <div class="grid_3 text-right" id="" style="float:right;position:relative;top:-20px;"> <span style="font-size: 22px; font-weight: bold;"><?php echo($type); ?> REPORT</span><br>
            <?php echo($from); ?>&nbsp;
            <?php echo($to); ?></div>
          <div class="clear"></div>
          <div class="text-right">
          </div>
		  
		  <div class="text-center padtb padlr margt lightblackbg hideFromPrint" style="height:54px;">
              <div class="grid_4">Report Type:<br>
                
              <form id="typeForm" action="" method="post">
              
              <select name="type" onchange="submitTypeForm()">
                <option value=""></option>
                <option value="DAILY">Daily Report</option>
                <option value="WEEKLY">Weekly Report</option>
                <option value="MONTHLY">Monthly Report</option>
                <option value="YEARLY">Yearly Report</option>
              </select>
              
            </form>
            
              </div>
              <div class="grid_4">Date Range:<br>
                
            <form id="dateForm" action="" method="post">
            
              From:
              <input id="startd" name="from" type="text" value="" oninput="checkDateForm()" onchange="checkDateForm()" style="width: 75px;">
              To:
              <input id="endd" name="to" type="text" value="" oninput="checkDateForm()" onchange="checkDateForm()" style="width: 75px;">
              
              </form>
              
              </div>
              <div class="grid_4">
                <input type="button" onclick="window.location='/xapp/reports/pointOfSale/history/<?php echo($membershipNumber); ?>'" value="SHOW ALL" style="padding: 15px;">
              <input type="button" onclick="window.print()" style="padding: 15px;" value="PRINT">
              </div>
            </div>
            
          <div class="grid_12 margt"><br>
            <table style="width: 70%;">
				<tr>
					<td colspan="4" style="padding: 10px;">
						<div class="grid_6"><a href="/xapp/customers/<?php echo ($membershipNumber); ?>"><?php echo ($memberName); ?> (<?php echo ($membershipNumber); ?>)</a></div>
						<div class="grid_6 text-right"><?php echo ($membershipType); ?> Membership</div></td>
				</tr>
				<tr>
					<th>Date</th>
					<th>Amount</th>
					<th>Sale #</th>
					<th>Actions</th>
				</tr>
				<?php echo ($rows); ?>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr style="font-weight: bold;">
					<td class="text-right"><span style="color: red;">Grand Total</span></td>
					<td colspan="3">$<?php echo ($total); ?></td>
				</tr>
			</table>	
          </div>
        