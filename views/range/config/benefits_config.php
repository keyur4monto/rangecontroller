
<script>

function memTypeChanged () {

window.location = "/xapp/config/benefits/" + $("#memType").val(); 

}


function submitUpdate () {

	var showDiag = true;

	for (var i = 1;i <= 9; i++) {
	
		var quantity = document.getElementById("quantity-" + i).value;
		
		var timeframe = document.getElementById("timeframe-" + i).value;
		
		var benefit_name = document.getElementById("benefit-name-" + i).value;
		
		var benefit_id = document.getElementById("benefit-id-" + i).value;
	
		if ((quantity != '' && timeframe != '' && benefit_name != '') || (quantity == '' && timeframe == '' && benefit_name == '')) {
		
		if (i == 9 && showDiag) {
		
			$( "#dialog" ).dialog( "open" );
			
			document.getElementById("errid").style.display = "none";
			
		}
		
		//	alert(quantity); 
			//alert(timeframe);
			//alert(benefit_name);
			//alert(benefit_id);
			
			
			
	
			var myurlstring = "/xapp/config/benefits/edit/<?php echo ($memtypeid); ?>/" + quantity + "/" + timeframe + "/" + encodeURI(benefit_name) + "/" + benefit_id;
		
			var xmlHttp = null;

			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "GET", myurlstring, false );
			xmlHttp.send( null );
			//alert(xmlHttp.responseText);
	
			//ranger.members.edit.showSuccess("Successfully updated!");

			//$("#popupdiv").show();
	
			//window.location = "/xapp/config/benefits/<?php echo ($memtypeid); ?>";
			
		}
		
		else {
		
		
			document.getElementById("errid").style.display = "block";
			showDiag=false;
			//show that red notification thingy
		
		}
	
	}

}

</script>

<div class="container_12 benefits">

<div class="grid_12 margb" style="display:none;" id="errid">

<style>

.error {

padding:10px !important;

}

</style>

<br><br>
        <?php echo ($err); ?>
        </div>

          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right"><span style="color: red;">Select Membership Name:</span>
              <select id="memType" onchange="memTypeChanged()">
                <?php echo ($membershipOptions); ?>
				</select>
            </div>
          </div>
          <div class="margt"></div>
          <div id="Keystone">
            <div class="grid_9 margt" style="position:relative !important;left:-20px !important;">
              <table>
                <tr>
                  <th>Quantity</th>
                  <th>Duration</th>
                  <th><?php echo ($memtypename); ?> Benefits</th>
                </tr>
                <?php echo ($benefitsFields); ?>
                <tr>
                  <td id="addrows" style="display:none;" colspan="3" class="text-center"><a href="#" onclick="addRows()">Add 3 More Rows</a></td>
                </tr>
              </table>
            </div>
            <div class="grid_3 margt">
              <table class="benefits_duration">
                <tr>
                  <th colspan="2">Duration Key:</th>
                </tr>
                <tr>
                  <td>Expiration</td>
                  <td>Resets on Membership Expiration</td>
                </tr>
                <tr>
                  <td>Yearly</td>
                  <td>Resets on 1st of Year</td>
                </tr>
                <tr>
                  <td>Monthly</td>
                  <td>Resets on 1st of Month</td>
                </tr>
                <tr>
                  <td>Weekly</td>
                  <td>Resets on Sunday</td>
                </tr>
                <tr>
                  <td>Daily</td>
                  <td>Resets at midnight</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="grid_12 text-center">
            <input type="button" id="" value="Save" onclick="submitUpdate()">
          </div>
        </div>
		
		

<!-- ui-dialog -->
<div id="dialog" title="Edit Confirmation">
	<p>Your changes have been saved!</p>
</div>

		

<script src="external/jquery/jquery.js"></script>
<script src="jquery-ui.js"></script>
<script>






$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				//submitForm();
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});



function myConfirm (str) {

	var temp = $( "#dialog" ).dialog( "open" );
	
	alert(temp);
	
	return false;
	
}


// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

</script>

<script>

//nick's extra jquery

$( document ).ready(function() {

	for (var i = 1;i <= 6; i++) {
	
	$("#fieldno-" + i).show();
	
	if (<?php echo ($memtypeid); ?> != 0) $("#addrows").show();
	
	}
    
});

function addRows () {

for (var i = 7;i <= 9; i++) {
	
	$("#fieldno-" + i).show();
	
	$("#addrows").hide();
	
	}


}

</script>



        