
<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('xapp/css/jquery.timepicker.css');?>">
<!--<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>-->
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery-ui-timepicker-addon.js');?>"></script>
<script src="<?php echo $view['assets']->getUrl('xapp/js/jquery.timepicker.js');?>"></script>

<script>
$(function(){

	$('#sunday_from,#sunday_to,#monday_from,#monday_to,#tuesday_from,#tuesday_to,#wednesday_from,#wednesday_to,#thursday_from,#thursday_to,#friday_from,#friday_to,#saturday_from,#saturday_to').timepicker();
/*hourGrid: 1,
	 minuteGrid: 1,
	 timeFormat: 'h:mmTT'
	 */
	$("#company_state").val("<?php echo($company_state); ?>");
	
	});


	function submitForm ()
	{
	
		if (document.getElementById("email").value == "" || document.getElementById("password").value == "" || document.getElementById("companyname").value == "" || document.getElementById("taxrate").value == "" || document.getElementById("numberoflanes").value == "") document.getElementById("errid").style.display = "block";
		
		else 
		{

var myurlstringpart1 = encodeURIComponent(document.getElementById("email").value) + "/" + encodeURIComponent(document.getElementById("password").value) + "/" + encodeURIComponent(document.getElementById("companyname").value) + "/" + encodeURIComponent(document.getElementById("company_address").value) + "/" + encodeURIComponent(document.getElementById("company_city").value) + "/" + encodeURIComponent(document.getElementById("company_state").value) + "/" + encodeURIComponent(document.getElementById("company_zipcode").value) + "/" + encodeURIComponent(document.getElementById("company_phone").value) + "/" + (encodeURIComponent((document.getElementById("taxrate").value))).replace("\x0A", "%3Cbr%3E") + "/" + (encodeURIComponent((document.getElementById("numberoflanes").value))).replace("\x0A", "%3Cbr%3E") + "/" + encodeURIComponent(document.getElementById("sunday_from").value) + "/" + encodeURIComponent(document.getElementById("sunday_to").value) + "/" + encodeURIComponent(document.getElementById("monday_from").value) + "/" + encodeURIComponent(document.getElementById("monday_to").value) + "/" + encodeURIComponent(document.getElementById("tuesday_from").value) + "/" + encodeURIComponent(document.getElementById("tuesday_to").value) + "/" + encodeURIComponent(document.getElementById("wednesday_from").value) + "/" + encodeURIComponent(document.getElementById("wednesday_to").value) + "/" + encodeURIComponent(document.getElementById("thursday_from").value) + "/" + encodeURIComponent(document.getElementById("thursday_to").value) + "/" + encodeURIComponent(document.getElementById("friday_from").value) + "/" + encodeURIComponent(document.getElementById("friday_to").value) + "/" + encodeURIComponent(document.getElementById("saturday_from").value) + "/" + encodeURIComponent(document.getElementById("saturday_to").value) + "/" + encodeURIComponent(document.getElementById("time_zone").value);
		
		var myurlstringpart2 = "";//"/" + encodeURIComponent(document.getElementById("instructor_id").value + "/" + encodeURIComponent(document.getElementById("description").value)); 
		
		for (var i = 0;i< 20;i++) myurlstringpart2 = myurlstringpart2.replace("%2F", "/");
		
		var myurlstring = myurlstringpart1 + myurlstringpart2;
		
		//myurlstring = myurlstring.replace(/x0A/g, "%3Cbr%3E")
		
		for (var i = 0;i< 20;i++) myurlstring = myurlstring.replace("%0A", "%3Cbr%3E");
		
		for (var i = 0;i< 20;i++) myurlstring = myurlstring.replace(".", "%2E");
		
		myurlstring = "/xapp/configRC/update/" +  myurlstring ;
		//alert("Instructor added!");
		
		//alert(myurlstring);
		
		var xmlHttp = null;
		
			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "GET", myurlstring, false );
			xmlHttp.send( null );
			console.log(xmlHttp.responseText);
		
		document.getElementById("imgform").submit();
			
		//window.location = "/xapp/classes";
		window.location = "/xapp/configRC";
	}
}

</script>
<style>
.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}
.ui-timepicker-div dl {
	text-align: left;
}
.ui-timepicker-div dl dt {
	float: left;
	clear:left;
	padding: 0 0 0 5px;
}
.ui-timepicker-div dl dd {
	margin: 0 10px 10px 45%;
}
.ui-timepicker-div td {
	font-size: 90%;
}
.ui-tpicker-grid-label {
	background: none;
	border: none;
	margin: 0;
	padding: 0;
	display:none;
}
.ui-timepicker-rtl {
	direction: rtl;
}
.ui-timepicker-rtl dl {
	text-align: right;
	padding: 0 5px 0 0;
}
.ui-timepicker-rtl dl dt {
	float: right;
	clear: right;
}
.ui-timepicker-rtl dl dd {
	margin: 0 45% 10px 10px;
}
</style>

<br>
<div class="grid_12 margb" style="display:none;" id="errid"> <?php echo($errmsg);?> </div>
<div class="clear">&nbsp;</div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right"> <span style="color: red; font-size: 22px; font-weight: bold;">RANGE # <?php echo($accountno); ?></span> </div>
  <div class="grid_12 margt">
    <table style="width: 70%;">
      <tr>
        <th colspan="2">REGISTERED EMAIL ADDRESS</th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Email Address:</td>
        <td><input id="email" type="text" style="width: 200px;" value="<?php echo($email); ?>"></td>
      </tr>
    </table>
    <table style="width: 70%;">
      <tr>
        <th colspan="2">ACCOUNT NUMBER AND PASSWORD</th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Account Number:</td>
        <td><span style="color: red; font-weight: bold;"><?php echo($accountno); ?></span></td>
      </tr>
      <tr>
        <td class="text-right">Password:</td>
        <td><input id="password" type="password" style="width: 200px;" value="<?php echo($password); ?>"></td>
      </tr>
    </table>
    <table style="width: 70%;">
      <tr>
        <th colspan="2">COMPANY INFO AND LOGO</th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Company Name:</td>
        <td><input id="companyname" type="text" style="width: 200px;" value="<?php echo($companyname); ?>"></td>
      </tr>
      <tr>
        <td class="text-right">Address:</td>
        <td><input id="company_address" type="text" style="width: 200px;" value="<?php echo($company_address); ?>"></td>
      </tr>
      <tr>
        <td class="text-right">City:</td>
        <td><input id="company_city" type="text" value="<?php echo($company_city); ?>"></td>
      </tr>
      <tr>
        <td class="text-right">State:</td>
        <td><select id="company_state">
           			          <option value="">--Select State--</option>
	<option value="AL" <?php echo ($company_state == 'AL') ? 'selected=""' : ''; ?>>Alabama</option>
	<option value="AK" <?php echo ($company_state== 'AK') ? 'selected=""' : ''; ?>>Alaska</option>
	<option value="AZ" <?php echo ($company_state== 'AZ') ? 'selected=""' : ''; ?>>Arizona</option>
	<option value="AR" <?php echo ($company_state== 'AR') ? 'selected=""' : ''; ?>>Arkansas</option>
	<option value="CA" <?php echo ($company_state== 'CA') ? 'selected=""' : ''; ?>>California</option>
	<option value="CO" <?php echo ($company_state== 'CO') ? 'selected=""' : ''; ?>>Colorado</option>
	<option value="CT" <?php echo ($company_state== 'CT') ? 'selected=""' : ''; ?>>Connecticut</option>
	<option value="DE" <?php echo ($company_state== 'DE') ? 'selected=""' : ''; ?>>Delaware</option>
	<option value="DC" <?php echo ($company_state== 'DC') ? 'selected=""' : ''; ?>>District Of Columbia</option>
	<option value="FL" <?php echo ($company_state== 'FL') ? 'selected=""' : ''; ?>>Florida</option>
	<option value="GA" <?php echo ($company_state== 'GA') ? 'selected=""' : ''; ?>>Georgia</option>
	<option value="HI" <?php echo ($company_state== 'HI') ? 'selected=""' : ''; ?>>Hawaii</option>
	<option value="ID" <?php echo ($company_state== 'ID') ? 'selected=""' : ''; ?>>Idaho</option>
	<option value="IL" <?php echo ($company_state== 'IL') ? 'selected=""' : ''; ?>>Illinois</option>
	<option value="IN" <?php echo ($company_state== 'IN') ? 'selected=""' : ''; ?>>Indiana</option>
	<option value="IA" <?php echo ($company_state== 'IA') ? 'selected=""' : ''; ?>>Iowa</option>
	<option value="KS" <?php echo ($company_state== 'KS') ? 'selected=""' : ''; ?>>Kansas</option>
	<option value="KY" <?php echo ($company_state== 'KY') ? 'selected=""' : ''; ?>>Kentucky</option>
	<option value="LA" <?php echo ($company_state== 'LA') ? 'selected=""' : ''; ?>>Louisiana</option>
	<option value="ME" <?php echo ($company_state== 'ME') ? 'selected=""' : ''; ?>>Maine</option>
	<option value="MD" <?php echo ($company_state== 'MD') ? 'selected=""' : ''; ?>>Maryland</option>
	<option value="MA" <?php echo ($company_state== 'MA') ? 'selected=""' : ''; ?>>Massachusetts</option>
	<option value="MI" <?php echo ($company_state== 'MI') ? 'selected=""' : ''; ?>>Michigan</option>
	<option value="MN" <?php echo ($company_state== 'MN') ? 'selected=""' : ''; ?>>Minnesota</option>
	<option value="MS" <?php echo ($company_state== 'MS') ? 'selected=""' : ''; ?>>Mississippi</option>
	<option value="MO" <?php echo ($company_state== 'MO') ? 'selected=""' : ''; ?>>Missouri</option>
	<option value="MT" <?php echo ($company_state== 'MT') ? 'selected=""' : ''; ?>>Montana</option>
	<option value="NE" <?php echo ($company_state== 'NE') ? 'selected=""' : ''; ?>>Nebraska</option>
	<option value="NV" <?php echo ($company_state== 'NV') ? 'selected=""' : ''; ?>>Nevada</option>
	<option value="NH" <?php echo ($company_state== 'NH') ? 'selected=""' : ''; ?>>New Hampshire</option>
	<option value="NJ" <?php echo ($company_state== 'NJ') ? 'selected=""' : ''; ?>>New Jersey</option>
	<option value="NM" <?php echo ($company_state== 'NM') ? 'selected=""' : ''; ?>>New Mexico</option>
	<option value="NY" <?php echo ($company_state== 'NY') ? 'selected=""' : ''; ?>>New York</option>
	<option value="NC" <?php echo ($company_state== 'NC') ? 'selected=""' : ''; ?>>North Carolina</option>
	<option value="ND" <?php echo ($company_state== 'ND') ? 'selected=""' : ''; ?>>North Dakota</option>
	<option value="OH" <?php echo ($company_state== 'OH') ? 'selected=""' : ''; ?>>Ohio</option>
	<option value="OK" <?php echo ($company_state== 'OK') ? 'selected=""' : ''; ?>>Oklahoma</option>
	<option value="OR" <?php echo ($company_state== 'OR') ? 'selected=""' : ''; ?>>Oregon</option>
	<option value="PA" <?php echo ($company_state== 'PA') ? 'selected=""' : ''; ?>>Pennsylvania</option>
	<option value="RI" <?php echo ($company_state== 'RI') ? 'selected=""' : ''; ?>>Rhode Island</option>
	<option value="SC" <?php echo ($company_state== 'SC') ? 'selected=""' : ''; ?>>South Carolina</option>
	<option value="SD" <?php echo ($company_state== 'SD') ? 'selected=""' : ''; ?>>South Dakota</option>
	<option value="TN" <?php echo ($company_state== 'TN') ? 'selected=""' : ''; ?>>Tennessee</option>
	<option value="TX" <?php echo ($company_state== 'TX') ? 'selected=""' : ''; ?>>Texas</option>
	<option value="UT" <?php echo ($company_state== 'UT') ? 'selected=""' : ''; ?>>Utah</option>
	<option value="VT" <?php echo ($company_state== 'VT') ? 'selected=""' : ''; ?>>Vermont</option>
	<option value="VA" <?php echo ($company_state== 'VA') ? 'selected=""' : ''; ?>>Virginia</option>
	<option value="WA" <?php echo ($company_state== 'WA') ? 'selected=""' : ''; ?>>Washington</option>
	<option value="WV" <?php echo ($company_state== 'WV') ? 'selected=""' : ''; ?>>West Virginia</option>
	<option value="WI" <?php echo ($company_state== 'WT') ? 'selected=""' : ''; ?>>Wisconsin</option>
	<option value="WY" <?php echo ($company_state== 'WY') ? 'selected=""' : ''; ?>>Wyoming</option>
          </select></td>
      </tr>
      <tr>
        <td class="text-right">Zip Code:</td>
        <td><input id = "company_zipcode" type="text" style="width: 40px;" value="<?php echo($company_zipcode); ?>"></td>
      </tr>
      <tr>
        <td class="text-right">Phone Number:</td>
        <td><input id = "company_phone" type="text" style="width: 90px;" value="<?php echo($company_phone); ?>"></td>
      </tr>
      <tr>
        <td class="text-right">Change Company Logo:</td>
        <td><!--<input id="companylogo" type="text" style="width: 200px;" value="<?php// echo($companylogo); ?>">
										<input type="button" value="Upload">-->
          <form id="imgform" enctype="multipart/form-data" action="/xapp/upload/uploadPhoto.php" method="post" onsubmit="return imgSub()">
            <input type="file" name="uploadfile" id="uploadfile"/>
          </form></td>
      </tr>
    </table>
    <table style="width: 70%;">
      <tr>
        <th colspan="2">Time Zone</th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Time Zone:</td>
        <td>
        <select id="time_zone">
        	<option value="">Select Time Zone</option>
            <option value="-4" <?php echo ($time_zone == -4) ? 'selected=""' : ''; ?> >Atlantic Time Zone (UTC-04:00)</option>
            <option value="-5" <?php echo ($time_zone == -5) ? 'selected=""' : ''; ?> >Eastern Time Zone (UTC-05:00)</option>
            <option value="-6" <?php echo ($time_zone == -6) ? 'selected=""' : ''; ?> >Central Time Zone (UTC-06:00)</option>
            <option value="-7" <?php echo ($time_zone == -7) ? 'selected=""' : ''; ?> >Mountain Time Zone (UTC-07:00)</option>
            <option value="-8" <?php echo ($time_zone == -8) ? 'selected=""' : ''; ?> >Pacific Time Zone (UTC-08:00)</option>
            <option value="-9" <?php echo ($time_zone == -9) ? 'selected=""' : ''; ?> >Alaska Time Zone (UTC-09:00)</option>
            <option value="-10" <?php echo ($time_zone == -10) ? 'selected=""' : ''; ?> >Hawaii-Aleutian Time Zone (UTC-10:00)</option>
            <option value="-11" <?php echo ($time_zone == -11) ? 'selected=""' : ''; ?> >Samoa Time Zone (UTC-11:00)</option>
        </select>
        </td>
      </tr>
    </table>
    <table style="width: 70%;">
      <tr>
        <th colspan="2">TAX RATE</th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Tax Rate:<br>
          <span style="font-size: 14px;">(in decimal form) example: .0975</span></td>
        <td><input id="taxrate" type="text" style="width: 50px;" value="<?php echo($taxrate); ?>"></td>
      </tr>
    </table>
    <table style="width: 70%;">
      <tr>
        <th colspan="2">NUMBER OF LANES FOR RANGE 1</th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Number of Lanes:</td>
        <td><input id="numberoflanes" type="text" style="width: 50px;" value="<?php echo($numberoflanes); ?>"></td>
      </tr>
    </table>
    <table style="width: 70%;">
      <tr>
        <th colspan="4">HOURS OF OPERATION (For Scheduler)</th>
      </tr>
      <tr>
        <th class="text-right">Day</th>
        <th>From</th>
        <th>To</th>
        <th>Closed</th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Sunday</td>
        <td><input onchange="document.getElementById('sunday_checked').checked=0" id="sunday_from" type="text" style="width: 70px;" value="<?php echo($sunday_from); ?>"></td>
        <td><input id="sunday_to" type="text" style="width: 70px;" value="<?php echo($sunday_to); ?>"></td>
        <td><input id="sunday_checked" onclick="if (this.checked==1) document.getElementById('sunday_from').value = document.getElementById('sunday_to').value = 'closed'" type="checkbox" <?php if($sunday_from == $sunday_to) echo ('checked'); ?>></td>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Monday</td>
        <td><input onchange="document.getElementById('monday_checked').checked=0" id="monday_from" type="text" style="width: 70px;" value="<?php echo($monday_from); ?>"></td>
        <td><input id="monday_to" type="text" style="width: 70px;" value="<?php echo($monday_to); ?>"></td>
        <td><input id="monday_checked" onclick="if (this.checked==1) document.getElementById('monday_from').value = document.getElementById('monday_to').value = 'closed'" type="checkbox" <?php if($monday_from == $monday_to) echo ('checked'); ?>></td>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Tuesday</td>
        <td><input onchange="document.getElementById('tuesday_checked').checked=0" id="tuesday_from" type="text" style="width: 70px;" value="<?php echo($tuesday_from); ?>"></td>
        <td><input id="tuesday_to" type="text" style="width: 70px;" value="<?php echo($tuesday_to); ?>"></td>
        <td><input id="tuesday_checked" onclick="if (this.checked==1) document.getElementById('tuesday_from').value = document.getElementById('tuesday_to').value = 'closed'" type="checkbox" <?php if($tuesday_from == $tuesday_to) echo ('checked'); ?>></td>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Wednesday</td>
        <td><input onchange="document.getElementById('wednesday_checked').checked=0" id="wednesday_from" type="text" style="width: 70px;" value="<?php echo($wednesday_from); ?>"></td>
        <td><input id="wednesday_to" type="text" style="width: 70px;" value="<?php echo($wednesday_to); ?>"></td>
        <td><input id="wednesday_checked" onclick="if (this.checked==1) document.getElementById('wednesday_from').value = document.getElementById('wednesday_to').value = 'closed'" type="checkbox" <?php if($wednesday_from == $wednesday_to) echo ('checked'); ?>></td>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Thursday</td>
        <td><input onchange="document.getElementById('thursday_checked').checked=0" id="thursday_from" type="text" style="width: 70px;" value="<?php echo($thursday_from); ?>"></td>
        <td><input id="thursday_to" type="text" style="width: 70px;" value="<?php echo($thursday_to); ?>"></td>
        <td><input id="thursday_checked" onclick="if (this.checked==1) document.getElementById('thursday_from').value = document.getElementById('thursday_to').value = 'closed'" type="checkbox" <?php if($thursday_from == $thursday_to) echo ('checked'); ?>></td>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Friday</td>
        <td><input onchange="document.getElementById('friday_checked').checked=0" id="friday_from" type="text" style="width: 70px;" value="<?php echo($friday_from); ?>"></td>
        <td><input id="friday_to" type="text" style="width: 70px;" value="<?php echo($friday_to); ?>"></td>
        <td><input id="friday_checked" onclick="if (this.checked==1) document.getElementById('friday_from').value = document.getElementById('friday_to').value = 'closed'" type="checkbox" <?php if($friday_from == $friday_to) echo ('checked'); ?>></td>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Saturday</td>
        <td><input onchange="document.getElementById('saturday_checked').checked=0" id="saturday_from" type="text" style="width: 70px;" value="<?php echo($saturday_from); ?>"></td>
        <td><input id="saturday_to" type="text" style="width: 70px;" value="<?php echo($saturday_to); ?>"></td>
        <td><input id="saturday_checked" onclick="if (this.checked==1) document.getElementById('saturday_from').value = document.getElementById('saturday_to').value = 'closed'" type="checkbox" <?php if($saturday_from == $saturday_to) echo ('checked'); ?>></td>
      </tr>
    </table>
    <table style="width: 70%;">
      <tr>
        <th colspan="3">MEMBERSHIPS</th>
      </tr>
      <tr>
        <td colspan="3" class="text-center">Go to: <a href="/xapp/config/memberships">MANAGE MEMBERSHIPS</a></td>
      </tr>
    </table>
    <table style="width: 70%;">
      <tr>
        <th colspan="2">SET MEMBER NUMBER START COUNT</th>
      </tr>
      <tr>
        <td class="text-right" style="width: 50%;">Start Count:</td>
        <td><span style="color: red; font-weight: bold;"><?php echo ($startcount); ?></span></td>
      </tr>
    </table>
  </div>
  <div class="grid_12 text-center">
    <input id="dialog-link1" type="button" value="Update">
  </div>
</div>

<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
  <p>Your changes have been submitted!</p>
</div>
<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

<script>

$( "#company_phone" ).change(function() {
  $("#company_phone").mask("(999) 999-9999");
});

$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				submitForm();
				$( this ).dialog( "close" );
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});

function myConfirm (str) {

	var temp = $( "#dialog" ).dialog( "open" );
	
	alert(temp);
	
	return false;	
}

// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);

function imgSub() {

var frm = $('#imgform');
    frm.submit(function (ev) {
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                alert('ok');
            }
        });

        ev.preventDefault();
    });

	return false;
	
}
</script> 
