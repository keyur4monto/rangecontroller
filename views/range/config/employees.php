
					<div class="clear"></div>
					<div class="grid_12">
						<div class="text-right">
							<input type="button" value="Add" onClick="window.location='/xapp/config/employees/add'">
						</div>
						<div class="grid_12 margt">
							<table style="width: 50%;">
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Initials</th>
									<th colspan="2">Action</th>
								</tr>
                                 <?php echo $employeeRows; ?>
            <tr id="norecords" <?php echo (empty($employeeRows)) ? "" : "style='display:none'"; ?>>
            	<td colspan="8" style="color:#F00"><center>No Records Found</center></td>
            </tr>
							</table>
						</div>
					</div>
				
				

<!-- ui-dialog -->
<div id="dialog" title="Delete Confirmation">
	<p id="warningtext" >Are you sure you want to delete this employee?</p>
</div>



		  
		  
<script>
		  
var classid = -1;



$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Yes",
			click: function() {
				$( this ).dialog( "close" );
				//window.setTimeout(function () {
					window.location = "/xapp/config/employees/delete/" + classid;		
					//},500);
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});



// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
	);

function myConfirm (str, class_id) {

	classid = class_id;

	var temp = $( "#dialog" ).dialog( "open" );
	
	document.getElementById("warningtext").innerHTML = str;
	
	//alert(temp);
	
	return false;
	
}

</script>

