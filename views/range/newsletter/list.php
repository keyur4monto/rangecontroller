<style>
.customer .padtb {
    padding-bottom: 4em;
    padding-top: 1em;
}
</style>
<div class="grid_12">
    <div class="text-right">
      <input type="button" value="Create Newsletter" onClick="window.location='newsletters/addNew'">
    </div>
    <div class="text-center padtb padlr margt lightblackbg">
		<form name="filter_newsletter" action="" method="get">
		  <div class="grid_3">Filter by Date<br>
			<input type="text" name="filter_date" id="filter_date" value="<?php echo $dt; ?>" placeholder="datepicker" style="width: 75px;">
		  </div>
		  <div class="grid_3">Filter by Month & Year<br>
			<select name="filter_month" id="filter_month">
			  <option value="">Month</option>
			  <?php for($i=1;$i<=12;$i++){ ?>
				<option value="<?php echo $i; ?>" <?php echo ($fm==$i) ? "selected=''" : ''; ?>><?php echo $i; ?></option>
			  <?php } ?>
			</select>
			<select name="filter_year" id="filter_year">
			  <option value="">Year</option>
			  <?php for($i=2010;$i<=2020;$i++){ ?>
				<option value="<?php echo $i; ?>" <?php echo ($fy==$i) ? "selected=''" : ''; ?>><?php echo $i; ?></option>
			  <?php } ?>
			</select>
		  </div>
		  <div class="grid_3">Filter by Employee<br>
		   <select name="filter_employee" id="filter_employee">
				  <option value="">Choose an Employee:</option>
				  <?php while($row = $getemplists->fetch_assoc()) { ?>
						<option value="<?php echo $row['id']; ?>" <?php echo ($emp==$row['id']) ? "selected=''" : ''; ?>><?php echo $row['fname'].' '.$row['lname']; ?></option>
				  <?php } ?>
			</select>
		  </div>
		  <div class="grid_3">
			<!--input type="submit" value="Filter"-->
			<input type="button" style="padding: 15px;" value="Show All" onClick="javascript:window.location.href='/xapp/newsletters/viewall';">
		  </div>
	  </form>
    </div>
	<div class="grid_12 margt">
		<table>
			<thead>
			    <tr>
			      <th style="width: 15%;">Employee</th>
			      <th>Date</th>
			      <th>Subject</th>
			      <th colspan="2" style="width: 15%;">Actions</th>
			    </tr>
			</thead>
			<tbody class="inner">
            	<?php
				if($allnewsletter->num_rows == 0)
				{
				?>
                <tr>
                <td colspan="4" style="color:#F00"><center>No Records Found</center></td>
                </tr>
                <?php
				}
				else
				{
				?>
			    <?php 
				while($row=$allnewsletter->fetch_assoc()){?> 
					<tr>
				      <td><?php echo $row['emp_name'];?></td>
				      <!--<td style="width: 25%;"><?php echo date("D M j g:i:s Y",strtotime($row['timestamp']));?></td>-->
                      <!--Fri Mar 13 9:22:13 2015-->
                      <td style="width: 25%;"><?php echo date("D, m/d/Y",strtotime($row['timestamp']));?></td>
				      <td><?php echo $row['subject']?></td>
				      <td>
				      	<a href="/xapp/newsletters/edit/<?php echo $row['newsletter_id']?>">Manage</a></td>
				      <td>
				      	<a href="/xapp/newsletters/delete/<?php echo $row['newsletter_id'];?>" onclick="dels()">Delete</a>
					  </td>          
				    </tr>
				<?php }
				}
				?>
			</tbody>
		</table>
		<div class="text-center" id="pagerBottom"></div>   
		
	</div>
</div> 
<script>
$(document).ready(function(){
	
	
	var urlPrefix = '<?php echo $app['get_range_url']('newsletters/')?>';	
	$( "#filter_date" ).datepicker({
		dateFormat: "dd-mm-yy"		
	});
	$("#filter_month,#filter_year,#filter_employee,#filter_date").change(function() {
	    //get the selected value
	    var filter_date = $('#filter_date').val();
		var filter_month = $('#filter_month').val();
	    var filter_year = $('#filter_year').val();
	    var filter_employee = $('#filter_employee').val();		
	    //make the ajax call
	    $.ajax({
		      type: "Post",
		      url: urlPrefix + 'getfilters',
		      dataType: "json",
			  data: {filter_month:filter_month, filter_year:filter_year, filter_employee:filter_employee,filter_date:filter_date},
		      success: function(result){		          
				 option = '';
				 if(result == '')
				 {
					option += "<tr>";
				    option += "  <td colspan='5' style='color:#F00'><center>No Records Found</center></td>";  
				    option += "</tr>";
				 }
				 else
				 {
				 $.each(result, function(i, v) {
			        option += "<tr>";
				    option += "  <td>"+v.emp_name+"</td>";
				    option += '  <td style="width: 25%;">'+v.timestamp+'</td>';
				    option += "  <td>"+v.subject+"</td>";
				    option += "  <td>";
				    option += "  	<a href='/xapp/newsletters/edit/"+v.newsletter_id+"'>Resend</a></td>";
				    option += "  <td>";
				    option += "  	<a href='/xapp/newsletters/delete/"+v.newsletter_id+"' onclick='dels()' class='del'>Delete</a>";
					option += "  </td>";          
				    option += "</tr>";					
					//alert(v.emp_id);
					// For each record in the returned array
			        //alert(v.emp_name);					 
			    });
				 }
			    $( ".inner" ).html(option);
		      }
		});
	});
	
	/*$.ajax({
		  url: urlPrefix + 'getGroupMembersJson',
		  type:'post',
		  dataType: "json",
		  data: {
			q: request.term,
			m: $("#member_type").val(),
			e:$("#employeename").val(),
			task:'searchmember'
		  },
		  success: function( data ) {
				response($.map(data, function(v,i){
					var text = v.label;
					if ( text ) {
						return {
								label: v.label,
								value: v.id,
								id:v.id,
							   };
					}
				}));
		  }
		});*/
});

function dels(){
	if (!confirm('Are you sure want to delete this newsletter?')) {
		return false;
	}	
}
</script>