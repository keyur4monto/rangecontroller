<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script>
// global namespace for ranger functions 
var ranger = ranger || {};
// global namespace for ranger members/customers functions
ranger.members = ranger.members || {};
rc.disableNavigateBackOnBackspace();
tinymce.init({
    selector: "textarea",
	 theme: "modern",
                plugins: [
                    "advlist autolink lists link charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons paste textcolor colorpicker textpattern image"
                ],
                height: 250,
                menubar:false,
                toolbar1: "undo redo | styleselect fontsizeselect | link image print preview | fullscreen ",
                toolbar2: "bold italic underline | forecolor backcolor emoticons | bullist numlist | alignleft aligncenter alignright alignjustify | outdent indent",
});
var urlPrefix = '<?php echo $app['get_range_url']('newsletters/')?>';
</script>
<?php $data = $member->fetch_assoc();
/*echo '<pre>';
print_r($data);
echo '</pre>';*/
?>
<form name="newnewsletter" action="" method="post" id="newnewsletter" onsubmit="return myValidation()">
<div class="grid_3 pull-right" id="breadcrumbsRight"> 
	<select name="employeename" id="employeename" style="float: right;">
	  <option value="">Choose an Employee:</option>
	  <?php while($row = $getemplists->fetch_assoc()) { ?>
			<option value="<?php echo $row['id']; ?>" <?php echo ($data['emp_id'] == $row['id'])? 'selected=""': '';?> ><?php echo $row['fname'].' '.$row['lname']; ?></option>
	  <?php } ?>
	</select>
</div>
<div class="clear"></div>
<div class="grid_12">
		
		<div class="text-right"> </div>
		<div class="grid_12 margt"><br>
			<?php if ($errorMessage): ?>
				<div class="grid_12 margb">
					<?php echo $view->render('range/error-message.php', ['message'=>$errorMessage]);?>
				</div><div class="clear">&nbsp;</div>
			<?php endif; ?>			
			<table style="width: 80%;">
				<tr>
					<td class="text-right" nowrap=""><span class="requiredf">*</span> Select name or group:</td>
					<td>
						<input id="member" type="text" placeholder="Member name or #" style="width: 150px;">
						or
						<select name="member_type" id="member_type">
							<option value="">Membership Types</option>
							<?php while($row=$mrecordstype->fetch_assoc()){ ?>
							<option value="<?php echo $row['type'];?>" <?php echo ($data['membership_type'] == $row['type'])? 'selected=""': '';?>><?php echo $row['type'];?></option>
						<?php } ?>
						</select>
						or
						<input type="button" value="Everyone" id="everyone">
					</td>
				</tr>
				<tr>
					<td class="text-right">Send To:</td>
					<td>
						<div id="members" style="width:490px">
							
							<span class="snd" style="color: red;">&nbsp;
								<?php foreach($allmaillist as $nam){
										if(!empty($nam['label']))
											echo "<div class='memberlist' style='float:left'>".$nam['label'].'<input type="hidden" name="receivers[]" value="'.$nam['id'].'"/>&nbsp;<input type="button"  class="removefromlist" value="X"></div>';	
								}?>
								<?php
									$i = 1; 
									foreach($allmaillist as $nam){
										if(!empty($nam['m_type'])){											
											if($i == 1){
												echo "<div class='memberlist' style='float:left'>".$nam['m_type'].'<input type="button"  class="removefromlist" value="X"></div>';	
											}												
										}									
										if($nam['everyone'] == 'yes'){
											if($i == 1){
												echo "<div class='memberlist' style='float:left'>Everyone<input type='hidden' name='everyone' value='yes'/>&nbsp;<input type='button'  class='removefromlist' value='X'></div>";	
											}
										}
										$i++;			
								}?>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="text-right"><span class="requiredf">*</span> Choose Subject: </td>
					<td><input type="text" style="width: 300px;" name="subject" value="<?php echo $data['subject'];?>" id="subject"></td>
				</tr>
				<tr>
					<td colspan="2" class="text-center"><span class="requiredf">*</span> Compose Message</td>
				</tr>
				<tr>
					<td colspan="2" class="text-center"><textarea style="width: 550px; height: 275px;" name="message_body"><?php echo $data['content'];?></textarea></td>
				</tr>
			</table>
		</div>
		<div class="grid_12"><span class="requiredf">*</span> Denotes required fields</div>
		
		<div class="text-center">
			<input type="hidden" name="cmd" value="edit" />
			<input type="submit" value="Send" name="submit">
			<input type="hidden" value="<?php echo $data['newsletter_id'];?>" name="newsletter_id">
			
		</div>
</div>
</form>
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
<script src="<?php echo $view['assets']->getUrl('js/ranger.newsletter.js');?>"></script>
<!-- ui-dialog -->
<div id="dialog" title="Edit Confirmation">
	<p id="dialtext">Your changes have been saved!</p>
</div>
<script>
$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				if (document.getElementById("employeename").value != "") valid = true;
				var extraElement = document.getElementById("employeename");
				//var eCopy = extraElement.cloneNode(true);
				//extraElement.style.display = "none";
				if (document.getElementById("employeename").value != "") document.forms["newnewsletter"].appendChild(extraElement);
				if (document.getElementById("employeename").value != "") document.forms["newnewsletter"].submit();
				$( this ).dialog( "close" );
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	event.preventDefault();
});

function dialogBox () {
	$( "#dialog" ).dialog( "open" );
	}
// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
	function() {
		$( this ).addClass( "ui-state-hover" );
	},
	function() {
		$( this ).removeClass( "ui-state-hover" );
	}
);
</script>
<script>
function checkForValidations () {
			valid = false;
			if (document.getElementById("employeename").value != "") {
				valid = true;
				var val = document.getElementById("employeename").value;
				var extraElement = document.getElementById("employeename");
				var eCopy = extraElement.cloneNode(true);
				eCopy.style.display = "none";
				eCopy.value = val;
				document.forms["newnewsletter"].appendChild(eCopy);
				document.forms["newnewsletter"].submit();
				
				}
				
			else {
			
				valid = false;
	
				var errmsg = "Please select an employee to complete this registration!";
		
				document.getElementById("dialtext").innerHTML = errmsg;
		
				dialogBox();
				}
				
}
var valid = true;
function myValidation () {

	return valid;

}
</script>