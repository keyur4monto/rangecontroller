<?php
use Rc\Services\DbHelper;
use Rc\Services\ArrayHelper;
use Rc\Models\server_email_template_types;
use Rc\Models\server_email_templates;
use Rc\Models\range_owner;
use Rc\Models\range_member;
use Rc\Models\SessionModel;

$link = DbHelper::getWrappedHandle($app['dbs']['main']);
$roid = $app['range_owner_id'];
$accno = $roid->getAccountNo();	
$hash = $roid->getAccountHashKey();

if (!SessionModel::getAccHashKey($app) || !SessionModel::getAccNo($app)) {
    echo "You need to log in to use this page";
    exit();
}

$email_status_sql = "select email_status from customers where acctnumberhashed = '".$link->real_escape_string($hash) . "' and accountnumber = '".$link->real_escape_string($accno)."'";
$row = DbHelper::dbFetchRowAssoc($link, $email_status_sql);

echo "<pre>";
//print_r($row['email_status']);
echo "</pre>";

$setpl = new server_email_templates($link);
$setpl_types = new server_email_template_types($link);
$custom_tpls = $setpl->getTemplatesForAccount(
        SessionModel::getAccHashKey($app), SessionModel::getAccNo($app));
$custom_tpls_indexed = ArrayHelper::arrayIndexBy($custom_tpls, 'se_templatetype_fk');

//print_r($custom_tpls_indexed);

$tpl_types = $setpl_types->getTemplateTypes();
$tpl_types_indexed = ArrayHelper::arrayIndexBy($tpl_types, 'sett_id');
foreach($tpl_types as $k => &$tt){
    $tt['placeholders_'] = server_email_template_types::getVariablesForType($tt['sett_id']); 
}
?>
<!DOCTYPE html>
<html>
        <head>
        <meta http-equiv="content-type" content="text/html" />
        <meta name="author" content="carlos r garcia" />
        <meta name="contact" content="c.r.garcia68@gmail.com" />
        <title>Range Controller - Email Configuration</title>
        <link rel="stylesheet" type="text/css" href="../css/tooltip.css"/>
        <link rel="stylesheet" type="text/css" href="../css/site_layout.css"/>
        <style>
.plhtable {
	width:100%;
	border-collapse: collapse;
}
.plhtable td {
	border: 1px solid #ccc;
	padding: 3px 6px;
}
.tooltipadjust {
	margin-top: -70px !important;
	margin-left:-280px !important;
	width: 430px !important;
}
.tplnametd {
	padding:5px 0px 5px 5px;
}
</style>

        <!--START TOOLS-->
        <!--script type="text/javascript" src="../tools/protoplasm.js"></script-->
        <script src="https://tinymce.cachefly.net/4.1/tinymce.min.js"></script>
        <script src="../tools/jquery-1.11.0.min.js"></script>
        <script type="text/javascript">
            $.noConflict();
            var seco = {
                custom_templates: <?php echo json_encode($custom_tpls_indexed); ?>,
                template_types: <?php echo json_encode($tpl_types_indexed); ?>,
                active_template: null,
                placeholders_visible: false
            };
            tinymce.init({
                selector: "textarea.activeTplBody",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons paste textcolor colorpicker textpattern image"
                ],
                height: 250,
                menubar:false,
                toolbar1: "undo redo | styleselect fontsizeselect | link image print preview | fullscreen ",
                toolbar2: "bold italic underline | forecolor backcolor emoticons | bullist numlist | alignleft aligncenter alignright alignjustify | outdent indent",
                                
            });

        </script>
        <script type="text/javascript" src="/js/server_email_config.js"></script>
        <!--END TOOLS-->
        </head>
        <body>
        <div class="grid_12">
    <div class="text-right">
    </div>
</div>
  <!--<table width="950" align="center" bgcolor="#1E1E1E">
  <table>
  </table>
  </table> --><br><br><br>     
<table width="950" align="center" bgcolor="#1E1E1E">
          <tr>
    <td width="68%" align="center" valign="top"><div> Selected Email Template: <span id="activeTplName" style="color: #f00;font-weight:bold"> 
        <!-- --> 
        </span> </div>
              <hr color="#666666" size="1" />
              <div id="tplGetStarted" style="color:#FF0000">
        <h2>To get started, <br />
                  <span style="color:#fff">please click on any of the templates <br />
          located under "Email Templates" column <br />
          on the right </span> </h2>
      </div>
              <div id="tplControls" style="text-align: left; display: none">
        <div style="margin-top:1em;background: #666; padding:5px;">
                  <div style="text-align:center;"> Make your emails customized with these placeholders. <a href="" 
                               id="placeholderCtrl"
                               style="color: #fff; font-weight:bold;"
                               onclick="seco.showHidePlaceholders(this); return false;"
                               >Show placeholders</a> </div>
                  <?php foreach($tpl_types as $k=>$v) : 
                                $placeholders = $v['placeholders_'];
                                ?>
                  <div style="display:none" 
                                 id="placeholdersList<?php echo $v['sett_id']; ?>"
                                 class="placeholders">
            <table class="plhtable">
                      <tbody>
                <?php foreach($placeholders as $kk=>$vv) :?>
                <tr>
                          <td style="width:1%" nowrap><?php echo $kk; ?></td>
                          <td><?php echo $vv['desc']; ?></td>
                        </tr>
                <?php endforeach; ?>
              </tbody>
                    </table>
          </div>
                  <?php endforeach; ?>
                </div>
        <div style="margin-top:1em"> Subject: <br />
                  <input type="text" id="activeTplSubject" value="" 
                                   style="width:96%" />
                </div>
        <div style="margin-top:1em"> Message
                  <textarea id="activeTplBody" name="activeTplBody" 
                                  class="activeTplBody" 
                                  style="width:96%; min-height:200px;"></textarea>
                </div>
        <div style="padding:1em 0em; text-align: center">
                  <input type="button" value="Update" onClick="seco.saveTpl();"/>
                </div>
        <br />
        <hr />
        <br />
        <div style="text-align:center"> Send a test email for this template 
                  to this email address:
                  <input type="text" id="testEmail"value="" />
                  <button onClick="seco.sendTestEmail()">Go</button>
                  <br />
                  &nbsp; </div>
      </div></td>
    <td width="32%" align="center" valign="top"><!--<div style="height:20px;"></div>
              <hr color="#666666"/>-->
              <input type="hidden" id="h_template_id" name="h_template_id"/>
              <table width="100%" cellpadding="2"
                           style="border-collapse: collapse">
        <thead>
                  <tr>
            <td >&nbsp;</td>
            <td  align="center"
                                     colspan="2">Notify</td>
          </tr>
                  <tr>
            <td width="60%" align="center">Email Templates</td>
            <td width="20%" align="center" nowrap><a href="#" onClick="return false;" class="tooltip">Shooter <span class="tooltipadjust"> If checked, system sends the corresponding email to shooter</span></a></td>
            <td width="20%" align="center"
                                nowrap><a href="#" onClick="return false;" class="tooltip" 
                                   >Admin <span class="tooltipadjust">If checked, system sends the corresponding email to admin</span></a></td>
          </tr>
                </thead>
			<?php 
				foreach($tpl_types as $k => $v) 
				{ 
					$custom_tpl_id = $custom_tpls_indexed[$v['sett_id'] . '']['id'];
					$custom_tpl_se_templatetype_fk = $custom_tpls_indexed[$v['sett_id'] . '']['se_templatetype_fk'];
			?>
			<tr>
                  <td nowrap class="tplnametd">
					<a style="cursor:pointer;" title="Click to Edit" onclick="seco.loadTpl(<?php echo $v['sett_id']; ?>)"> <?php echo $v['sett_name']; ?></a>
				  </td>
                  <td  align="center">
					<input type="checkbox" id="notify_shooter-<?php  echo $custom_tpl_id; ?>" class="notifyCb"  <?php echo $custom_tpls_indexed[$v['sett_id'] . '']['notify_shooter'] ? 'checked' : '' ?>/>
				  </td>
                  <td  align="center">
					<input type="checkbox" class="notifyCb" id="notify_admin-<?php echo $custom_tpl_id; ?>" <?php echo ($custom_tpls_indexed[$v['sett_id'] . '']['notify_admin']) ? 'checked' : '' ?> />
				  </td>
            </tr>
			<?php 
				} 
			?>
      </table>
              <div style="padding: 1em 0; text-align:center;"> &nbsp;
        <button onClick="seco.updateNotifs()">Update Notification Settings</button>
      </div>
              <table width="100%" cellpadding="2" 
                           style="border-collapse:collapse;margin-bottom:1em">
        <tr>
                  <td width="100%" align="center" class="noBorder"><a href="#" 
                                   title="Restore Server Emails and Notifications to Defaults"
                                   onclick="seco.restoreToDeafults();return false;"> Restore Defaults </a>
            <form method="post" action="" id="frmRestoreDefaults" name="frmRestoreDefaults">
                      <input type="hidden" name="restore_templates_to_defaults" value="1" />
                    </form>
                    </td>
                </tr>
      </table></td>
  </tr>
        </table>
</body>
</html>
