
<link rel="stylesheet" type="text/css" href="<?php echo $view['assets']->getUrl('css/site_layout.css');?>" />
<script src='<?php echo $view['assets']->getUrl('js/editor/ckeditor.js');?>'></script>
<link rel="stylesheet" type="text/css" href="<?php echo $view['assets']->getUrl('js/editor/samples/sample.css');?>" />

<script>
window.onload = function() {
  createEditor();
};

var editor, html = '';
function createEditor(blnDirty) {
if ( editor )
return;
// Create a new editor inside the <div id="editor">, setting its value to html
var config = {};
editor = CKEDITOR.appendTo( 'editor', config, html );

var data = document.getElementById('txtContent').value;
if(data){
document.getElementById('editorcontents').innerHTML = editor.setData(data,1);
}//end if

}//end function

function removeEditor() {
if ( !editor )
return;

//if(document.getElementById('h_content').value==""){return;}
// Retrieve the editor contents. In an Ajax application, this data would be
// sent to the server or used in any other way.
document.getElementById('editorcontents').innerHTML = html = editor.getData();
//document.getElementById('contents').style.display = '';

//document.getElementById("h_save").value = "true";
//fill hidden input with content
document.getElementById('txtContent').value = html = editor.getData();//hidden ctrl

// Destroy the editor.
editor.destroy();
editor = null;
}//end function
</script>
<?php
 use Rc\Services\DbHelper;
$link2 = DbHelper::getWrappedHandle($app['dbs']['main']);
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<style>
#ui-id-2{
	width: 150px !important;
    list-style: none !important;
    padding-left: 0 !important;
}
#ui-id-2.ui-widget-content {
    background: none repeat scroll 0 0 white !important;
    border: 1px solid #666 !important;
    color: black !important;
}
#ui-id-2 .ui-state-focus{
	color:red !important;
	border:none !important;
	background:none !important;
	font-weight:normal !important;
}
</style>
<form name="newnewsletter" action="" method="post" id="newnewsletter">
<div class="grid_3" id="breadcrumbsRight"></div>
<div class="clear"></div>
<div class="grid_12">
<div class="text-right"></div>
</div>
<div class="grid_12 margt"> <br>
<?php if ($errorMessage): ?>
    <div class="grid_12 margb">
        <?php echo $view->render('admin/error-message.php', ['message'=>$errorMessage]);?>
    </div><div class="clear">&nbsp;</div>
<?php endif; ?>
<table style="width: 80%;">
  <tr>
    <td class="text-right" nowrap=""><span class="requiredf">*</span> Select name or group:</td>
    <td><input id="member" name="company_name" type="text" placeholder="Company Name" style="width: 150px;">
      or
      <input type="button" id="everyone" value="Everyone"></td>
  </tr>
  <tr>
    <td class="text-right">Send To:</td>
    <td>
        <div id="members" style="width:490px">
            <span class="snd" style="color: red;"></span>
        </div>
    </td>
  </tr>
  <tr>
    <td class="text-right"><span class="requiredf">*</span> Choose Subject:</td>
    <td><input type="text" style="width: 300px;" name="subject" value="" id="subject"></td>
  </tr>
  <tr>
    <td colspan="2" class="text-center"><span class="requiredf">*</span> Compose Message</td>
  </tr>
  <tr>
    <td colspan="2" class="text-center">
      <input type="hidden" id="txtContent" name="txtContent" value="<?=$content?>" />
      <!-- This div will hold the editor. -->
      <div id="editor"> </div>
      <div id="contents" style="display: none">
        <p> Blog Contents: </p>
        <!-- This div will be used to display the editor contents. -->
        <div id="editorcontents"> </div>
      </div>
   </td>
  </tr>
</table>
</div>
<div class="grid_12 text-center">
<input type="hidden" name="cmd" value="add" />
<input type="submit" value="Send" name="submit" onclick="removeEditor();">
</div>
</div>
</form>
<script>
var urlPrefix = '<?php echo $app['get_range_url']('admin/newsletters/')?>';
</script>
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
<script src="<?php echo $view['assets']->getUrl('js/ranger.admin_newsletter.js');?>"></script>
