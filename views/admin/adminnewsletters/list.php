
<div class="container_12 admin">
<div class="grid_12">
  <div class="text-right">
    <input type="button" value="Create Newsletter" onclick="window.location='newsletters/add'">
  </div>
  <div class="text-center padtb padlr margt lightblackbg">
    <form name="filter_newsletter" action="" method="get">
      <div class="grid_4">Filter by Date<br>
			<input type="text" name="filter_date" id="filter_date" placeholder="datepicker" style="width: 75px;">
		  </div>
      <div class="grid_4">Filter by Month & Year <br>
        <select name="filter_month" id="filter_month">
          <option value="">Month</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
        </select>
        <select name="filter_year" id="filter_year">
          <option value="">Year</option>
          <option value="2010">2010</option>
          <option value="2011">2011</option>
          <option value="2012">2012</option>
          <option value="2013">2013</option>
          <option value="2014">2014</option>
          <option value="2015">2015</option>
          <option value="2016">2016</option>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
        </select>
      </div>
      <div class="grid_4"> 
        <!--input type="submit" value="Filter"-->
        <input type="button" style="padding: 15px;" value="Show All" onClick="javascript:window.location.href='/xapp/admin/newsletters';">
      </div>
    </form>
  </div>
  <div class="grid_12 margt"> <br>
    <table>
      <tr>
        <th>Date</th>
        <th>Subject</th>
        <th colspan="2">Actions</th>
      </tr>
      <tbody class="inner">
      <?php
		if(empty($get_newsletter))
		{
		?>
		<tr>
		<td colspan="4" style="color:#F00"><center>No Record Found</center></td>
		</tr>
		<?php
		}
		else
		{
	  ?>
      <?php foreach($get_newsletter as $row) { ?>
      <tr>
        <td style="width: 35%;" class="text-right"><?php echo date("l, F j, Y",strtotime($row['timestamp']));?></td>	
        <td><?php echo $row['subject']; ?></td>
        <td style="width: 5%;"><a href="newsletters/manage/<?php echo $row['newsletter_id']?>">Resend</a></td>
        <td style="width: 5%;"><a href="#" onclick="dels('<?php echo $row['newsletter_id']; ?>')">Delete</a></td>
      </tr>
      <?php } ?>
      <?php } ?>
      </tbody>
    </table>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
	
	
	var urlPrefix = '<?php echo $app['get_range_url']('admin/newsletters/')?>';	
	$( "#filter_date" ).datepicker({
		dateFormat: "dd-mm-yy"		
	});
	$("#filter_month,#filter_year,#filter_date").change(function() {
	    //get the selected value
	    var filter_date = $('#filter_date').val();
		var filter_month = $('#filter_month').val();
	    var filter_year = $('#filter_year').val();		
	    //make the ajax call
	    $.ajax({
		      type: "Post",
		      url: urlPrefix + 'getfilters',
		      dataType: "json",
			  data: {filter_month:filter_month, filter_year:filter_year, filter_date:filter_date},
		      success: function(result){	          
				 option = '';
				 if(result == '')
				 {
					option += "<tr>";
				    option += "  <td colspan='5' style='color:#F00'><center>No Record Found</center></td>";  
				    option += "</tr>";
				 }
				 else
				 {
				 $.each(result, function(i, v) {
			        option += "<tr>";
				    option += "  <td style='width: 35%;' class='text-right'>"+v.timestamp+"</td>";
				    option += '  <td>'+v.subject+'</td>';
				    option += "  <td  style='width: 5%;'>";
				    option += "  	<a href='/xapp/admin/newsletters/manage/"+v.newsletter_id+"'>Resend</a></td>";
				    option += "  <td  style='width: 5%;'>";
				    option += "  	<a href='#' onclick='dels("+v.newsletter_id+")' class='del'>Delete</a>";
					option += "  </td>";          
				    option += "</tr>";					 
			    });
				 }
			    $( ".inner" ).html(option);
		      }
		});
	});
});

function dels(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/newsletters/delete/')?>';	
	if (confirm('Are you sure want to delete this newsletter?')) {
		window.location =  urlPrefix + id;	
	}
}
</script>