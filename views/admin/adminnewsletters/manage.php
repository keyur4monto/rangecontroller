
<link rel="stylesheet" type="text/css" href="<?php echo $view['assets']->getUrl('css/site_layout.css');?>" />
<script src='<?php echo $view['assets']->getUrl('js/editor/ckeditor.js');?>'></script>
<link rel="stylesheet" type="text/css" href="<?php echo $view['assets']->getUrl('js/editor/samples/sample.css');?>" />

<script>
window.onload = function() {
  createEditor();
};

var editor, html = '';
function createEditor(blnDirty) {
if ( editor )
return;
// Create a new editor inside the <div id="editor">, setting its value to html
var config = {};
editor = CKEDITOR.appendTo( 'editor', config, html );

var data = document.getElementById('txtContent').value;
if(data){
document.getElementById('editorcontents').innerHTML = editor.setData(data,1);
}//end if

}//end function

function removeEditor() {
if ( !editor )
return;

//if(document.getElementById('h_content').value==""){return;}
// Retrieve the editor contents. In an Ajax application, this data would be
// sent to the server or used in any other way.
document.getElementById('editorcontents').innerHTML = html = editor.getData();
//document.getElementById('contents').style.display = '';

//document.getElementById("h_save").value = "true";
//fill hidden input with content
document.getElementById('txtContent').value = html = editor.getData();//hidden ctrl

// Destroy the editor.
editor.destroy();
editor = null;
}//end function
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="grid_3" id="breadcrumbsRight"></div>
<div class="clear"></div>
<div class="grid_12">
<div class="text-right">
 	<input type="button" value="Delete" onclick="dels('<?php echo $get_newsletter['newsletter_id']; ?>')">
</div>
</div>
<form name="newnewsletter" action="" method="post" id="newnewsletter">
<div class="grid_12 margt"> <br>
<?php if ($errorMessage): ?>
    <div class="grid_12 margb">
        <?php echo $view->render('admin/error-message.php', ['message'=>$errorMessage]);?>
    </div>
    <div class="clear">&nbsp;</div>
<?php endif; ?>

<table style="width: 80%;">
  <tr>
    <td class="text-right" nowrap=""><span class="requiredf">*</span> Select name or group:</td>
    <td><input id="member" type="text" placeholder="Company Name" style="width: 150px;">
      or
      <input type="button" id="everyone" value="Everyone"></td>
  </tr>
  <tr>
    <td class="text-right">Send To:</td>
    <td><div id="members" style="width:490px"> <span class="snd" style="color: red;">
    <?php
	foreach($get_email_lists as $r)
	{
		foreach($email_name as $en)
		{
	?>
    <div class='memberlist' style='float:left'><?php echo $en['company_name']?><input type='hidden' name='receivers[]' value="<?php echo $en['accountnumber']?>"/>&nbsp;<input type='button'  class='removefromlist' value='X'></div>
    <?php
		}
	}
	?>
    </span> </div></td>
  </tr>
  <tr>
    <td class="text-right"><span class="requiredf">*</span> Choose Subject:</td>
    <td><input type="text" style="width: 300px;" name="subject" value="<?php echo $get_newsletter['subject']; ?>" id="subject"></td>
  </tr>
  <tr>
    <td colspan="2" class="text-center"><span class="requiredf">*</span> Compose Message</td>
  </tr>
  <tr>
    <td colspan="2" class="text-center">
      <input type="hidden" id="txtContent" name="txtContent" value="<?php echo $get_newsletter['content']; ?>" />
      <!-- This div will hold the editor. -->
      <div id="editor"> </div>
      <div id="contents" style="display: none">
        <p> Blog Contents: </p>
        <!-- This div will be used to display the editor contents. -->
        <div id="editorcontents"> </div>
      </div>
    </td>
  </tr>
</table>
</div>
<div class="grid_12 text-center">
<input type="hidden" name="cmd" value="edit" />
<input type="submit" value="Send" name="submit" onclick="removeEditor();">
<input type="hidden" value="<?php echo $get_newsletter['newsletter_id'];?>" name="newsletter_id">
</div>
</div>
</form>
<script>
function dels(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/newsletters/delete/')?>';	
	if (confirm('Are you sure want to delete this newsletter?')) {
		window.location =  urlPrefix + id;	
	}	
}
</script>
<script>
var urlPrefix = '<?php echo $app['get_range_url']('admin/newsletters/')?>';
</script>
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
<script src="<?php echo $view['assets']->getUrl('js/ranger.admin_newsletter.js');?>"></script>