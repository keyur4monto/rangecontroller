<style>
.navButton {
    font-family: arial;
    font-size: 16px;
    height: 60px;
    width: 185px;
}
</style>
<div class="grid_3" id="breadcrumbsRight"></div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right"></div>
  <div class="grid_12 margt">
    <table style="width: 500px;">
      <tr>
        <td style="width: 50%; text-align: center;" class="noBorder"><input type="button" value="Accounts" class="navButton" onclick="window.location='account'"></td>
        <td style="width: 50%; text-align: center;" class="noBorder"><input type="button" value="Reports" class="navButton" disabled></td>
      </tr>
      <tr>
        <td style="width: 50%; text-align: center;" class="noBorder"><input type="button" value="Blog" class="navButton" onclick="window.location='blog'"></td>
        <td style="width: 50%; text-align: center;" class="noBorder"><input type="button" value="Announcement Board" class="navButton" onclick="window.location='announcementBoard'"></td>
      </tr>
      <tr>
        <td style="width: 50%; text-align: center;" class="noBorder"><input type="button" value="Newsletters" class="navButton" onclick="window.location='newsletters'"></td>
        <td style="width: 50%; text-align: center;" class="noBorder" disabled>&nbsp;</td>
      </tr>
    </table>
  </div>
</div>
