<style>
a.disabled {
   pointer-events: none;
   cursor: default;
}
</style>
<script src="https://www.rangecontroller.com/js/sorttable.js"></script>
<div class="container_12 admin">
<div class="grid_12">
  <div class="text-right"></div>
  <div class="text-center padtb padlr margt lightblackbg">
    <div class="grid_4">Filter by Company Name: <br>
      <input type="text" name="filter_company_name" id="filter_company_name" onkeyup="showCompanyInfo()">
    </div>
    <div class="grid_4">Filter by Account Status: <br>
      <select name="account_status" id="account_status" onchange="showCompanyInfo()">
        <option></option>
        <option>Active</option>
        <option>Trial</option>
        <option>Suspended</option>
      </select>
    </div>
    <div class="grid_4">
      <input type="button" value="Show All" style="padding: 15px;" onClick="javascript:window.location.href='/xapp/admin/account';">
    </div>
  </div>
  <div class="grid_12 margt"> <br>
    <table class="text-center sortable sorttable" style="width: 100%;">
      <thead><tr>
        <th></th>
        <th nowrap="0">Company Name</th>
        <th nowrap="0">Account #</th>
        <th>Status</th>
        <th colspan="3">Action</th>
      </tr></thead>
      <tbody class="inner">
      <?php 
	  $count = 1;
	  foreach($get_accountdata as $row){ ?>
      <tr>
        <td><?php echo $count; ?></td>
        <td><?php echo $row['company_name']; ?></td>
        <td style="width: 15%;" class="text-center"><?php echo $row['accountnumber']; ?></td>
        <td style="width: 15%;" class="text-center"><?php echo $row['account_status']; ?></td>
        <td style="width: 5%;"><a href="account/manage/<?php echo $row['accountnumber']?>">Manage</a></td>
        <?php if($row['account_status'] == 'Suspended') { ?>
        			<?php 
					if(in_array($row['accountnumber'], $get_trial))
					{
					?>
        			<td style="width: 5%;"><a href="#" onClick="trial('<?php echo $row['accountnumber']; ?>')">Activate</a></td>
                    <?php
					}
					else
					{
					?>
                    <td style="width: 5%;"><a href="#" onClick="active('<?php echo $row['accountnumber']; ?>')">Activate</a></td>
                    <?php
					}
					?>
        <?php 
		} 
        else 
		{ ?>
        	 <?php if($row['accountnumber'] == 4420) { ?>
        		<td style="width: 5%;"></td>
              <?php } 
			  else {?>
              <td style="width: 5%;"><a href="#" onClick="suspend('<?php echo $row['accountnumber']; ?>')">Suspend</a></td>
              <?php } ?>
       <?php } ?>
        
       <?php if($row['accountnumber'] == 4420) { ?>
        <td style="width: 5%;"></td>
       <?php } 
	   else { ?>
       <td style="width: 5%;"><a href="#" onClick="dels('<?php echo $row['accountnumber']; ?>')">Delete</a></td>
       <?php } ?>
        
      </tr>
     <?php
	 $count++;
	  } ?>
     </tbody>
    </table>
  </div>
</div>
</div>
<script>

	
	
	var urlPrefix = '<?php echo $app['get_range_url']('admin/account/')?>';	
	
	function showCompanyInfo() {
	    //get the selected value
	    var filter_company_name = document.getElementById("filter_company_name").value;
		var account_status = document.getElementById("account_status").value;	
	    //make the ajax call
	    $.ajax({
		      type: "Post",
		      url: urlPrefix + 'getfilters',
		      dataType: "json",
			  data: {filter_company_name:filter_company_name, account_status:account_status},
		      success: function(result){	          
				 option = '';
				 if(result == '')
				 {
					option += "<tr>";
				    option += "  <td colspan='5' style='color:#F00'><center>No Record Found</center></td>";  
				    option += "</tr>";
				 }
				 else
				 {
			     var count=1;
				 $.each(result, function(i, v) {
			        option += "<tr>";
					option += "  <td>"+count+"</td>";
				    option += "  <td>"+v.company_name+"</td>";
				    option += '  <td style="width: 15%;" class="text-center">'+v.accountnumber+'</td>';
					option += '  <td style="width: 15%;" class="text-center">'+v.account_status+'</td>';
				    option += "  <td  style='width: 5%;'>";
				    option += "  	<a href='/xapp/admin/account/manage/"+v.accountnumber+"'>Manage</a></td>";
					
					if(v.account_status == 'Suspended')
					{
						if(jQuery.inArray(v.accountnumber, v.accountnumber_array)) 
						{
						option += "<td  style='width: 5%;'><a href='#' onclick='trial("+v.accountnumber+")'>Active</a></td>";
						}
						else
						{
						option += "<td  style='width: 5%;'><a href='#' onclick='active("+v.accountnumber+")'>Active</a></td>";	
						}
					}
					else
					{
						if(v.accountnumber == 4420 )
						{
						option += "<td  style='width: 5%;'> </td>";
						}
						else
						{
						option += "<td  style='width: 5%;'> <a href='#' onclick='suspend("+v.accountnumber+")'>Suspend</a></td>";	
						}
					}
				
					if(v.accountnumber == 4420 )
					{
				    option += "  <td  style='width: 5%;'> </td>"; 
					}    
					else
					{
					option += "  <td  style='width: 5%;'>";
				    option += "  	<a href='#' onclick='dels("+v.accountnumber+")' class='del'>Delete</a>";
					option += "  </td>";	
					}
				    option += "</tr>";	
				count++;				 
			    });
				 }
			    $( ".inner" ).html(option);
		      }
		});
	};


function dels(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/account/delete/')?>';	
	if (confirm('Are you sure want to delete this Account?')) {
		window.location =  urlPrefix + id;	
	}
}

function suspend(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/account/suspend/')?>';	
	if (confirm('Are you sure want to suspend this Account?')) {
		window.location =  urlPrefix + id;	
	}
}

function active(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/account/active/')?>';	
	if (confirm('Are you sure want to Active this Account?')) {
		window.location =  urlPrefix + id;	
	}
}

function trial(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/account/trial/')?>';	
	if (confirm('Are you sure want to Trial this Account?')) {
		window.location =  urlPrefix + id;	
	}
}
</script>