
<div class="grid_3" id="breadcrumbsRight"></div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right">
    <input type="button" value="Delete" onclick="dels('<?php echo $get_account_detail['accountnumber']; ?>')">
  </div>
    <form name="manageaccount" action="" method="post" id="manageaccount">
  <div class="grid_12 margt"> <br>
  <?php if ($errorMessage): ?>
    <div class="grid_12 margb">
        <?php echo $view->render('admin/error-message.php', ['message'=>$errorMessage]);?>
    </div>
    <div class="clear">&nbsp;</div>
<?php endif; ?>
    <table style="width: 60%;">
      <tr>
        <td class="text-right" style="width: 50%;">Registration Date:</td>
        <td><span style="color: #fbb91f; font-weight: bold;"><?php echo date('m/d/y', strtotime($get_account_detail['timestamp'])); ?></span></td>
      </tr>
      <tr>
        <td class="text-right">Status:</td>
        <td><?php echo $get_account_detail['account_status']; ?></td>
      </tr>
      <tr>
        <td class="text-right"><span class="requiredf">*</span>Company Name:</td>
        <td><input type="text" name="company_name" value="<?php echo $get_account_detail['company_name']; ?>" style="width: 200px;"></td>
      </tr>
      <tr>
        <td class="text-right">Account #:</td>
        <td> <?php echo $get_account_detail['accountnumber']; ?> </td>
      </tr>
      <tr>
        <td class="text-right"><span class="requiredf">*</span>Contact Name:</td>
        <td><input type="text" name="contact_name" value="<?php echo $get_account_detail['first_name'].' '.$get_account_detail['last_name']; ?>" style="width: 150px;"></td>
      </tr>
      <tr>
        <td class="text-right"><span class="requiredf">*</span>Address:</td>
        <td><input type="text" name="address" value="<?php echo $get_account_detail['street_address']; ?>"></td>
      </tr>
      <tr>
        <td class="text-right"><span class="requiredf">*</span>City:</td>
        <td><input type="text" name="city" value="<?php echo $get_account_detail['city']; ?>" style="width: 150px;"></td>
      </tr>
      <tr>
        <td class="text-right"><span class="requiredf">*</span>State:</td>
        <!--<td><input type="text" name="state" value="<?php echo $get_account_detail['state']; ?>" style="width: 40px;"></td>-->
        <td>
        
<select name="state" id="state">
    <option value="">--Select State--</option>
	<option value="AL" <?php echo ($get_account_detail['state']== 'AL') ? 'selected=""' : ''; ?>>Alabama</option>
	<option value="AK" <?php echo ($get_account_detail['state']== 'AK') ? 'selected=""' : ''; ?>>Alaska</option>
	<option value="AZ" <?php echo ($get_account_detail['state']== 'AZ') ? 'selected=""' : ''; ?>>Arizona</option>
	<option value="AR" <?php echo ($get_account_detail['state']== 'AR') ? 'selected=""' : ''; ?>>Arkansas</option>
	<option value="CA" <?php echo ($get_account_detail['state']== 'CA') ? 'selected=""' : ''; ?>>California</option>
	<option value="CO" <?php echo ($get_account_detail['state']== 'CO') ? 'selected=""' : ''; ?>>Colorado</option>
	<option value="CT" <?php echo ($get_account_detail['state']== 'CT') ? 'selected=""' : ''; ?>>Connecticut</option>
	<option value="DE" <?php echo ($get_account_detail['state']== 'DE') ? 'selected=""' : ''; ?>>Delaware</option>
	<option value="DC" <?php echo ($get_account_detail['state']== 'DC') ? 'selected=""' : ''; ?>>District Of Columbia</option>
	<option value="FL" <?php echo ($get_account_detail['state']== 'FL') ? 'selected=""' : ''; ?>>Florida</option>
	<option value="GA" <?php echo ($get_account_detail['state']== 'GA') ? 'selected=""' : ''; ?>>Georgia</option>
	<option value="HI" <?php echo ($get_account_detail['state']== 'HI') ? 'selected=""' : ''; ?>>Hawaii</option>
	<option value="ID" <?php echo ($get_account_detail['state']== 'ID') ? 'selected=""' : ''; ?>>Idaho</option>
	<option value="IL" <?php echo ($get_account_detail['state']== 'IL') ? 'selected=""' : ''; ?>>Illinois</option>
	<option value="IN" <?php echo ($get_account_detail['state']== 'IN') ? 'selected=""' : ''; ?>>Indiana</option>
	<option value="IA" <?php echo ($get_account_detail['state']== 'IA') ? 'selected=""' : ''; ?>>Iowa</option>
	<option value="KS" <?php echo ($get_account_detail['state']== 'KS') ? 'selected=""' : ''; ?>>Kansas</option>
	<option value="KY" <?php echo ($get_account_detail['state']== 'KY') ? 'selected=""' : ''; ?>>Kentucky</option>
	<option value="LA" <?php echo ($get_account_detail['state']== 'LA') ? 'selected=""' : ''; ?>>Louisiana</option>
	<option value="ME" <?php echo ($get_account_detail['state']== 'ME') ? 'selected=""' : ''; ?>>Maine</option>
	<option value="MD" <?php echo ($get_account_detail['state']== 'MD') ? 'selected=""' : ''; ?>>Maryland</option>
	<option value="MA" <?php echo ($get_account_detail['state']== 'MA') ? 'selected=""' : ''; ?>>Massachusetts</option>
	<option value="MI" <?php echo ($get_account_detail['state']== 'MI') ? 'selected=""' : ''; ?>>Michigan</option>
	<option value="MN" <?php echo ($get_account_detail['state']== 'MN') ? 'selected=""' : ''; ?>>Minnesota</option>
	<option value="MS" <?php echo ($get_account_detail['state']== 'MS') ? 'selected=""' : ''; ?>>Mississippi</option>
	<option value="MO" <?php echo ($get_account_detail['state']== 'MO') ? 'selected=""' : ''; ?>>Missouri</option>
	<option value="MT" <?php echo ($get_account_detail['state']== 'MT') ? 'selected=""' : ''; ?>>Montana</option>
	<option value="NE" <?php echo ($get_account_detail['state']== 'NE') ? 'selected=""' : ''; ?>>Nebraska</option>
	<option value="NV" <?php echo ($get_account_detail['state']== 'NV') ? 'selected=""' : ''; ?>>Nevada</option>
	<option value="NH" <?php echo ($get_account_detail['state']== 'NH') ? 'selected=""' : ''; ?>>New Hampshire</option>
	<option value="NJ" <?php echo ($get_account_detail['state']== 'NJ') ? 'selected=""' : ''; ?>>New Jersey</option>
	<option value="NM" <?php echo ($get_account_detail['state']== 'NM') ? 'selected=""' : ''; ?>>New Mexico</option>
	<option value="NY" <?php echo ($get_account_detail['state']== 'NY') ? 'selected=""' : ''; ?>>New York</option>
	<option value="NC" <?php echo ($get_account_detail['state']== 'NC') ? 'selected=""' : ''; ?>>North Carolina</option>
	<option value="ND" <?php echo ($get_account_detail['state']== 'ND') ? 'selected=""' : ''; ?>>North Dakota</option>
	<option value="OH" <?php echo ($get_account_detail['state']== 'OH') ? 'selected=""' : ''; ?>>Ohio</option>
	<option value="OK" <?php echo ($get_account_detail['state']== 'OK') ? 'selected=""' : ''; ?>>Oklahoma</option>
	<option value="OR" <?php echo ($get_account_detail['state']== 'OR') ? 'selected=""' : ''; ?>>Oregon</option>
	<option value="PA" <?php echo ($get_account_detail['state']== 'PA') ? 'selected=""' : ''; ?>>Pennsylvania</option>
	<option value="RI" <?php echo ($get_account_detail['state']== 'RI') ? 'selected=""' : ''; ?>>Rhode Island</option>
	<option value="SC" <?php echo ($get_account_detail['state']== 'SC') ? 'selected=""' : ''; ?>>South Carolina</option>
	<option value="SD" <?php echo ($get_account_detail['state']== 'SD') ? 'selected=""' : ''; ?>>South Dakota</option>
	<option value="TN" <?php echo ($get_account_detail['state']== 'TN') ? 'selected=""' : ''; ?>>Tennessee</option>
	<option value="TX" <?php echo ($get_account_detail['state']== 'TX') ? 'selected=""' : ''; ?>>Texas</option>
	<option value="UT" <?php echo ($get_account_detail['state']== 'UT') ? 'selected=""' : ''; ?>>Utah</option>
	<option value="VT" <?php echo ($get_account_detail['state']== 'VT') ? 'selected=""' : ''; ?>>Vermont</option>
	<option value="VA" <?php echo ($get_account_detail['state']== 'VA') ? 'selected=""' : ''; ?>>Virginia</option>
	<option value="WA" <?php echo ($get_account_detail['state']== 'WA') ? 'selected=""' : ''; ?>>Washington</option>
	<option value="WV" <?php echo ($get_account_detail['state']== 'WV') ? 'selected=""' : ''; ?>>West Virginia</option>
	<option value="WI" <?php echo ($get_account_detail['state']== 'WT') ? 'selected=""' : ''; ?>>Wisconsin</option>
	<option value="WY" <?php echo ($get_account_detail['state']== 'WY') ? 'selected=""' : ''; ?>>Wyoming</option>
</select>	
		</td>
      </tr>
      <tr>
        <td class="text-right"><span class="requiredf">*</span>Zip Code:</td>
        <td><input type="text" name="zip_code" value="<?php echo $get_account_detail['zipcode']; ?>" style="width: 60px;"></td>
      </tr>
      <tr>
        <td class="text-right"><span class="requiredf">*</span>Phone Number:</td>
        <td><input type="text" name="phone_number" value="<?php echo $get_account_detail['phone_number']; ?>" style="width: 100px;"></td>
      </tr>
      <tr>
        <td class="text-right"><span class="requiredf">*</span>Email Address:</td>
        <td><input type="text" name="email_address" value="<?php echo $get_account_detail['registered_email']; ?>" style="width: 200px;"></td>
      </tr>
      <tr>
        <td class="text-right">Notes:</td>
        <td><textarea style="width: 200px; height: 50px;" placeholder="Notes go here...." name="notes"><?php echo $get_account_detail['notes']; ?></textarea></td>
      </tr>
    </table>
  </div>

  <div class="grid_12 text-center">
    <input type="hidden" name="cmd" value="edit" />
    <input type="submit" value="Save" name="submit">
    <input type="hidden" value="<?php echo $get_account_detail['accountnumber'];?>" name="accountnumber" id="accountnumber">
  </div>
  </form>
</div>
<script>
function dels(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/account/delete/')?>';	
	if (confirm('Are you sure want to delete this Account?')) {
		window.location =  urlPrefix + id;	
	}
}
</script>