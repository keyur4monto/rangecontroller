<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="text/html">
<title><?php echo $pageTitle; ?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $view['assets']->getUrl('css/site_layout.css', 'rangepath');?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo $view['assets']->getUrl('css/grid-fluid.css', 'rangepath');?>" />
<link rel="stylesheet" type="text/css" href="<?php echo $view['assets']->getUrl('css/silex.css','rangepath');?>" />
<link rel="stylesheet" type='text/css' href='<?php echo $view['assets']->getUrl('tools/jquery-ui-1.11.2.custom/jquery-ui.min.css', 'rangepath');?>' />
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<script src="<?php echo $view['assets']->getUrl('tools/jquery-1.11.0.min.js', 'rangepath')?>"></script>
<script src='<?php echo $view['assets']->getUrl('js/vendor/ractive.min.js');?>'></script>
<script  src='<?php echo $view['assets']->getUrl('js/rclib.js');?>' ></script>
<script  src='<?php echo $view['assets']->getUrl('js/vendor/jquery.mask.js');?>' ></script>
<script src="<?php echo $view['assets']->getUrl('tools/jquery.simplePagination.js', 'rangepath')?>"></script>
<script src="<?php echo $view['assets']->getUrl('js/vendor/ejs/ejs_production.js')?>"></script>
<script src="<?php echo $view['assets']->getUrl('js/vendor/ejs/view.js')?>"></script>
<script src="<?php echo $view['assets']->getUrl('js/vendor/validator.min.js')?>"></script>
<script src="<?php echo $view['assets']->getUrl('js/vendor/numeral.min.js')?>"></script>
<script src="<?php echo $view['assets']->getUrl('tools/jquery-ui-1.11.2.custom/jquery-ui.min.js', 'rangepath')?>"></script>
<script src="<?php echo $view['assets']->getUrl('js/vendor/jquery.fileupload.js')?>"></script>
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script src="/xapp/js/angular.js"></script>
<?php /*<script>
	// global namespace for ranger functions 
	var ranger = ranger || {};
	 // global namespace for ranger members/customers functions
	ranger.members = ranger.members || {};
	rc.disableNavigateBackOnBackspace();
</script>*/ ?>

</head>

<body>
<div class="hideFromPrint marginCenter"> <img class="bg" src="<?php echo $view['assets']->getUrl('img/background.jpg', 'rangepath'); ?>"> </div>
<table id="headerTable" class="hideFromPrint marginCenter">
  <tbody>
    <tr>
      <td class="blackHeaderCell"><img src="<?php echo $view['assets']->getUrl('img/logo.png', 'rangepath') ?>" width="400"></td>
    </tr>
		</table>
	<table id="companyTable" class="hideFromPrint marginCenter">
    <tr>
      <td id="companyTableCell" class="textCenter borderTop"><font color="#fbb91f" size="6"> <b> Range Controller Admin </b> </font></td>
    </tr>
	</table>
<table id="contentTable">
          <tbody>
    <tr>
      <td id="contentTableCell"><div class="container_12 customer" > <?php echo $content ?: 'No content'; ?>
          <div class="clear">&nbsp;</div>
        </div></td>
    </tr>
  </tbody>
</table>
<table id="footer1Table" class="hideFromPrint textCenter">
  <tbody>
    <tr>
      <td style="border-top: 0px;"><div class="floatLeft padLeft"> <a href="/xapp/common/logout.php" class="text-nodeco">LOGOUT</a> </div></td>
    </tr>
  </tbody>
</table>
<table id="footerTable" class="hideFromPrint textCenter">
  <tbody>
    <tr>
      <td id="footerCell" class="textColor noBorder textCenter">Range Controller by Rockin' Guns <br>
        © 
        <script>
						document.write(new Date().getFullYear());
					</script></td>
    </tr>
  </tbody>
</table>
</body>
</html>