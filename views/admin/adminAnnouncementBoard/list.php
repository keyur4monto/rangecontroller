
<div class="grid_3" id="breadcrumbsRight"></div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right">
    <input type="button" value="Add" onclick="window.location='announcementBoard/add'">
  </div>
  <div class="grid_12 margt"> <br>
    <table style="width: 90%;">
      <tr>
        <th style="width: 5%;">Date</th>
        <th>Description</th>
        <th style="width: 10%;" colspan="2">Action</th>
      </tr>
      <?php
		if(empty($get_announcementboard))
		{
		?>
		<tr>
		<td colspan="3" style="color:#F00"><center>No Record Found</center></td>
		</tr>
		<?php
		}
		else
		{
	  ?>
      <?php foreach($get_announcementboard as $row) { ?>
      <tr>
        <td style="color: #fbb91f; text-align: right;"><?php echo $row['timestamp']; ?></td>
        <td><?php echo $row['description']; ?></td>
        <td><a href="announcementBoard/manage/<?php echo $row['announcementboard_id']?>">Manage</a></td>
        <td><a href="#" onclick="dels('<?php echo $row['announcementboard_id']; ?>')">Delete</a></td>
      </tr>
      <?php } ?>
      <?php } ?>
    </table>
  </div>
</div>
<script>
function dels(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/announcementBoard/delete/')?>';	
	if (confirm('Are you sure want to delete this announcementBoard?')) {
		window.location =  urlPrefix + id;	
	}
}
</script>