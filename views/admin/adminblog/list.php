
<div class="grid_3" id="breadcrumbsRight"></div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right">
    <input type="button" value="Add" onclick="window.location='blog/add'">
  </div>
  <div class="grid_12 margt"> <br>

  <?php foreach($get_blog as $row) { ?>
    <table style="width: 90%;">
      <tr>
        <td style="width: 5%; color: #fbb91f;"><?php echo $row['timestamp']; ?></td>
        <td style="font-weight: bold;"><?php echo $row['title']; ?></td>
        <td style="width: 5%;"><a href="blog/manage/<?php echo $row['blog_id']?>">Manage</a></td>
        <td style="width: 5%;"><a href="#" onclick="dels('<?php echo $row['blog_id']; ?>')">Delete</a></td>
      </tr>
      <tr>
        <td colspan="4"><p><?php echo $row['content']; ?></p></td>
      </tr>
    </table>
    <?php } ?>
  
  </div>
</div>
<script>
function dels(id){
	var urlPrefix = '<?php echo $app['get_admin_url']('admin/blog/delete/')?>';	
	if (confirm('Are you sure want to delete this Blog?')) {
		window.location =  urlPrefix + id;	
	}
}
</script>
