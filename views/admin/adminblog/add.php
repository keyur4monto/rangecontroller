
<link rel="stylesheet" type="text/css" href="<?php echo $view['assets']->getUrl('css/site_layout.css');?>" />
<script src='<?php echo $view['assets']->getUrl('js/editor/ckeditor.js');?>'></script>
<link rel="stylesheet" type="text/css" href="<?php echo $view['assets']->getUrl('js/editor/samples/sample.css');?>" />

<script>
window.onload = function() {
  createEditor();
};

var editor, html = '';
function createEditor(blnDirty) {
if ( editor )
return;
// Create a new editor inside the <div id="editor">, setting its value to html
var config = {};
editor = CKEDITOR.appendTo( 'editor', config, html );

var data = document.getElementById('txtContent').value;
if(data){
document.getElementById('editorcontents').innerHTML = editor.setData(data,1);
}//end if

}//end function

function removeEditor() {
if ( !editor )
return;

//if(document.getElementById('h_content').value==""){return;}
// Retrieve the editor contents. In an Ajax application, this data would be
// sent to the server or used in any other way.
document.getElementById('editorcontents').innerHTML = html = editor.getData();
//document.getElementById('contents').style.display = '';

//document.getElementById("h_save").value = "true";
//fill hidden input with content
document.getElementById('txtContent').value = html = editor.getData();//hidden ctrl

// Destroy the editor.
editor.destroy();
editor = null;
}//end function
</script>

<form name="newblog" action="" method="post" id="newblog">
<div class="grid_3" id="breadcrumbsRight"></div>
<div class="clear"></div>
<div class="grid_12">
  <div class="text-right"></div>
  <div class="grid_12 margt"> <br>
  <?php if ($errorMessage): ?>
    <div class="grid_12 margb">
        <?php echo $view->render('admin/error-message.php', ['message'=>$errorMessage]);?>
    </div><div class="clear">&nbsp;</div>
<?php endif; ?>
    <table style="width: 80%;">
      <tr>
        <td class="text-right" style="width: 25%;"><span class="requiredf">*</span>Subject:</td>
        <td><input type="text" style="width: 300px;" name="subject" id="subject"></td>
      </tr>
      <tr>
        <td colspan="2" class="text-center"><span class="requiredf">*</span>Message Body:</td>
      </tr>
      <tr>
        <td colspan="2" class="text-center">
          <input type="hidden" id="txtContent" name="txtContent" value="<?=$content?>" />
          <!-- This div will hold the editor. -->
          <div id="editor"> </div>
          <div id="contents" style="display: none">
            <p> Blog Contents: </p>
            <!-- This div will be used to display the editor contents. -->
            <div id="editorcontents"> </div>
          </div>
       </td>
     </tr>
    </table>
  </div>
  <div class="grid_12 text-center">
    <input type="hidden" name="cmd" value="add" />
	<input type="submit" value="Save" name="submit" onclick="removeEditor();">
  </div>
</div>
</form>
