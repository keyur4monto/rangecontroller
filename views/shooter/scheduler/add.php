<script>

var single = true;

$(function(){
        $('#scheduleDate').datepicker({
            'showAnim': 'slideDown',
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear:true
        });     
		
    });

	

		$(document).ready(function () {
		  
			//getEmployees();
		  
			//$("#breadcrumbsRigh").show();
			
		});
		

function setStudent (customer_id) {

//alert(customer_id);

	document.getElementById("namesearch").value = customer_id;

	//customer_name = customer_id.split(" ")[1] + customer_id.split("  ")[1];
	
	customer_id = customer_id.split(" ")[0];
	
	//alert(customer_id);

	//var myurlstring = "/xapp/classes/<?php echo($class_no); ?>/addCustomer/" + customer_id;

	//var xmlHttp = null;

    //xmlHttp = new XMLHttpRequest();
    //xmlHttp.open( "GET", myurlstring, false );
    //xmlHttp.send( null );
    //alert(xmlHttp.responseText);
	
	//window.location = "/xapp/classes/registrationList/<?php echo($class_no); ?>";
	
	
	
	$("#div_customer_list").hide();
	
}

function populateCustomerList(str){

if (window.XMLHttpRequest){
// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else{
// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function(){
if (xmlhttp.readyState==4 && xmlhttp.status==200){
document.getElementById("div_customer_list").innerHTML = xmlhttp.responseText;

if (str == "") $("#div_customer_list").hide(); 

else $("#div_customer_list").show();
//alert("Test!" + xmlhttp.responseText + str);
}
}//end onreadystatechange
xmlhttp.open("GET","../../xapp/ajax/getMembersOptions/"+str,true);
xmlhttp.send();
}//END AJAX TO FILL POS ITEMS LIST

function submitForm () {

			var xmlHttp = null;
			
			var myurlstring = "/xapp/rcScheduler/add/post";

			xmlHttp = new XMLHttpRequest();
			xmlHttp.open( "POST", myurlstring, true );
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			
			var numOfLanes = "";
			
			if (!single) numOfLanes = "number_of_lanes=" + document.getElementById("numOfLanes").value;
			
			xmlHttp.onload = function (e) {
			
				//alert(xmlHttp.responseText);
				
				window.location = "/xapp/members/rcScheduler";
			
			}
			
			if (document.getElementById("scheduleDate").value != '' && document.getElementById("startTime").value != '' && document.getElementById("endTime").value != '') xmlHttp.send(numOfLanes + "&scheduleDate=" + document.getElementById("scheduleDate").value + "&employee_id=24&shooter=" + "<?php echo($member_number . ' ' . $member_name); ?>" + "&startTime=" + document.getElementById("startTime").value + "&endTime=" + document.getElementById("endTime").value);
			
			else {
			
					$( "#dialog" ).dialog( "open" );
			
					$("#shadow").show();
			
			}
			
			//else document.getElementById("popupContent").innerHTML = "Please select an employee to continue!";
			
			//document.getElementById("popupContent").innerHTML = "Your note has been saved!";
			
			//if (document.getElementById("employee_id").value != '') xmlHttp.send("bug_content=" + document.getElementById("bug_content").value + "&employee_id=" + document.getElementById("employee_id").value + "&bug_date=" + tday);
			
			//else document.getElementById("popupContent").innerHTML = "Please select an employee to continue!";
			
			
			//$( "#dialog" ).dialog( "open" );
			
			//$("#shadow").show();
				

}

</script>



<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Edit Confirmation">
	<p id="popupContent">Validation Error : Please fill out all the fields!</p>
</div>

<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>

       

        <div class="clear"></div>
        <div class="grid_12">
          <div class="text-right"> </div>
          <div class="grid_12 margt text-center">
            <table style="width: 35%;">
              <tr>
                <td>Single Lane
                  <input selected checked type="radio" name="lanes" onclick="singleLane()"></td>
                <td>Multiple Lanes
                  <input type="radio" name="lanes" onclick="multipleLane()"></td>
              </tr>
            </table>
          </div>
          
          <center>
          
          <div class="grid_2 margt" style="position:relative;left:237px;width:400px;">
            <table>
              <tr>
                <th colspan="2"><span id="wordToggle">Single</span> Lanes</th>
              </tr>
              <tr id="laneAmount" style="display:none;">
                <td class="text-right">Amount of Lanes:</td>
                <td><select id="numOfLanes">
                    <option value="1"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select></td>
              </tr>
              <tr>
                <td class="text-right">Shooter:</td>
                <td><b><?php echo ($member_name); ?></b></td>
              </tr>
              <tr>
                <td class="text-right">Date:</td>
                <td><input id="scheduleDate" type="text" value="" style="width: 75px;"></td>
              </tr>
              <tr>
                <td class="text-right">Time from:</td>
                <td><select id="startTime" style="width: 90px;border-radius:5px;">
                <option value=""></option>
                <?php 
                
                	foreach ($hrs as $timeslot) { 
                	
                		echo '<option value="' . $timeslot . '">' . $timeslot . '</option>';
                		
                	}
                	
                ?>
                </td>
              </tr>
              <tr>
                <td class="text-right">Time to:</td>
                <td><select id="endTime" style="width: 90px;border-radius:5px;">
                <option value=""></option>
                <?php 
                
                	foreach ($hrs as $timeslot) { 
                	
                		echo '<option value="' . $timeslot . '">' . $timeslot . '</option>';
                		
                	}
                	
                ?>
                </td>
              </tr>
            </table>
          </div>
          
          </center>
          
          <div class="grid_12">
            <div>* all fields required</div>
            <div class="text-center">
              <input type="button" value="Save" onClick="submitForm()">
            </div>
          </div>
        </div></td>
        
        <script>
        
        function singleLane () {
        
        	$("#laneAmount").hide();
        	
        	$("#wordToggle").html("Single");
        	
        	single = true;
        
        }
        
        function multipleLane () {
        
        	$("#laneAmount").show();
        	
        	$("#wordToggle").html("Multiple");
        	
        	single = false;
        
        }
        
        
        </script>
        
        
		<script>
		
		$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				
				$("#shadow").hide();
				
				//window.location = "/xapp/rcScheduler";
				
			}
		},
		//{
		//	text: "Cancel",
		//	click: function() {
		//		$( this ).dialog( "close" );
		//	}
		//}
	]
});

// Link to open the dialog
$( "#dialog-link1" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});


</script>


          