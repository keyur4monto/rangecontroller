<script>
var del_id;
function deleteThis (id) {
del_id = id;
$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
}
</script>
<!-- ui-dialog -->
<div id="dialog" style="z-index: 101;" title="Delete Confirmation">
	<p id="popupContent">Are you sure you want to delete this booking?</p>
</div>
<div class="ui-widget-overlay ui-front" id="shadow" style="z-index: 100;display:none;"></div>
          <div class="clear"></div>
          <div class="grid_12">
            <div class="text-right">
              <input type="button" value="Add" onClick="window.location='rcScheduler/add';" >
            </div>
            <div class="margt"> </div>
            <div class="grid_12 text-center">
              <table style="width: 80%;">
                <tr>
                  <th colspan="6" style="background-color: #666;">Upcoming Scheduled Range Time</th>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>Time from</td>
                  <td>Time to</td>
                  <td>Lane Amount</td>
                  <td colspan="2">Action</td>
                </tr>
                <?php echo ($upcomingRows); ?>
              </table>
              <table style="width: 80%;">
                <tr>
                  <th colspan="6" style="background-color: #666;">Previous Scheduled Range Time</th>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>Time from</td>
                  <td>Time to</td>
                  <td>Lane Amount</td>
                  <td colspan="2">Action</td>
                </tr>
                <?php echo ($previousRows); ?>
              </table>
            </div>
          </div>
<script>
$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
				window.location="/xapp/members/rcScheduler/delete/" + del_id;
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
				$("#shadow").hide();
			}
		}
	]
});
// Link to open the dialog
$( "#dialog-link" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );
	$("#shadow").show();
	event.preventDefault();
});
</script>