<?php $data = $member->fetch_assoc();?>
<div class="text-right">
       <input type="button" value="Edit" onclick="window.location='editContactInfo'">
</div>
<div class="grid_12 margt">
              <table style="width: 50%;">
                <tr>
                  <td class="text-right">Email:</td>
                  <td><?php echo $data['email'];?></td>
                </tr>
                <tr>
                  <td class="text-right">Address:</td>
                  <td><?php echo $data['street_address'];?></td>
                </tr>
                <tr>
                  <td class="text-right">City:</td>
                  <td><?php echo $data['city'];?></td>
                </tr>
                <tr>
                  <td class="text-right">State:</td>
                  <td><?php echo $data['state'];?></td>
                </tr>
                <tr>
                  <td class="text-right">Zipcode:</td>
                  <td><?php echo $data['zipcode'];?></td>
                </tr>
                <tr>
                  <td class="text-right">Phone Number:</td>
                  <td><?php echo $data['home_phone'];?></td>
                </tr>
                <tr>
                  <td class="text-right">Do Not Call Status:</td>
                  <td><span style="color: yellow;">
				  <?php
				   $row = $do_not_call->fetch_assoc(); 
				
				   if($row['membership_number'] != $_SESSION['member_number'])
				   {
					   echo "<font color='red'>Do Not Call</font>";
				   }
				   else
				   {
					   echo "<font color='yellow'>It's OK to Call</font>";
				   }  
				   ?>
                   </span></td>
                </tr>
              </table>
</div>