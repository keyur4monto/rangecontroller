<div class="grid_12 margt">
     <table style="width: 70%;">
        <tr>
          <th colspan="3" style="background-color: #666;">Upcoming Classes</th>
        </tr>
         <?php
        if($getupcomingclasses->num_rows == 0 )
        {
            echo "<tr><td colspan='3'><span style='color: red;'><center>No records found</center></span></td></tr>";
        }
        else
        {
        while ($row = $getupcomingclasses->fetch_assoc()) { 
        ?>
        <tr style="font-size: 14px;">
          <td style="width: 10%;"><?php echo $row['start_date']; ?></td>
		   <?php 
					$starttime=$row['start_time'];
					$endtime=$row['end_time'];
					
					$starttime=str_replace("am"," AM",$starttime);
					$endtime=str_replace("am"," AM",$endtime);
					
					$starttime=str_replace("pm"," PM",$starttime);
					$endtime=str_replace("pm"," PM",$endtime);
			  ?>
          <td style="width: 27%;">(<?php echo $starttime .'&nbsp - '. $endtime; ?>)</td>
          <td nowrap=""><a href="/xapp/members/classDetails/<?php echo $row['id']?>"><?php echo $row['title']; ?></a></td>
        </tr>
         <?php
        }
        }
        ?>
      </table>
</div>