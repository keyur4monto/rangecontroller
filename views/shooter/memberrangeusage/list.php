<?php
require('xapp/common/constants.php');
require('xapp/common/security.php');
session_start();
?>
<div class="grid_12 margt">
              <table style="width: 50%;">
                <tr>
                  <th colspan="2" style="background-color: #666;">Registration Info</th>
                </tr>
                <tr>
                  <td class="text-right">Name:</td>
                  <td> <?=$_SESSION['member_name']?></td>
                </tr>
                <tr>
                  <td class="text-right">Member #:</td>
                  <td><?=$_SESSION['member_number']?></td>
                </tr>
                <?php
					$i=0; 
					/*while ($rowtype = $getmembertype->fetch_assoc()) {
					$i++;
					$mt = $rowtype['membership_type'];
					}*/
				?>
                <tr>
                  <td class="text-right">Membership Type:</td>
                  <td> <?php echo $membershiptype; ?></td>
                </tr>
              </table>
              <table style="width: 50%;">
                <tr>
                  <th colspan="2" style="background-color: #666;">Range Usage</th>
                </tr>
                <tr>
                  <td class="text-right">Total Visits:</td>
                  <td><label id="total_visits"><?php echo $total_visits['total_visits'];?></label></td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 52%;">Total Time Today:</td>
                  <td><label id="h_today"><?php echo $today['hour'].' hours '.$today['minute'].' minutes'; ?></label></td>
                </tr>
                <tr>
                  <td class="text-right">Total Time This Week to Date:</td>
                  <td><label id="h_week"><?php echo $week['hour'].' hours '.$week['minute'].' minutes'; ?></label></td>
                </tr>
                <tr>
                  <td class="text-right">Total Time This Month to Date:</td>
                  <td><label id="h_month"><?php echo $month['hour'].' hours '.$month['minute'].' minutes'; ?></label></td>
                </tr>
                <tr>
                  <td class="text-right">Total Time This Year to Date:</td>
                  <td><label id="h_year"><?php echo $year['hour'].' hours '.$year['minute'].' minutes'; ?></label></td>
                </tr>                
              </table>
              <table style="width: 50%;">
                <tr>
                  <th colspan="2" style="background-color: #666;">Benefits</th>
                </tr>
				<?php foreach($benifits as $bef) { ?>
					<tr>
					  <td><span style="color: red;"><?php echo (!empty($bef['used'])) ? $bef['used'] : 0; ?> of <?php echo $bef['quantity']; ?></span></td>
					  <td><?php echo $bef['name']; ?></td>
					</tr>					
				<?php } ?>
              </table>
</div>
<div class="grid_3">&nbsp;</div>