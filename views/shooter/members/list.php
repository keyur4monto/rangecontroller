<style>
.shooters .margt {
    margin-top: 2em;
}
.shooters table {
    margin-bottom: 2em;
    width: 100%;
}
.shooters table td, .shooters table th {
  padding: 0.38em 0.62em;
}
</style>
<!--<table style="margin:0 auto; width:950px;" border="1" cellspacing="0s">
  <tbody>
    <tr>
    
    <td style="background:#1e1e1e;padding:1.62em 0;">-->
    
    <!--<div class="container_12 customer">
      <div class="grid_9" style="background-color: yellow;">
        <div class="breadcrumbs"> <a href="/xapp/common/logout.php">Logout </a> / <span>Shooter Portal </span> </div>
      </div>
      <div class="grid_3" id="breadcrumbsRight"></div>
      <div class="clear"></div>-->
      <div class="grid_12">
        <div class="text-right"> </div>
        <div class="margt"> </div>
        <div class="grid_6">
          <table>
            <tr>
              <td class="noBorder"><input type="button" value="Scheduler" class="navButton" onclick="window.location='rcScheduler';" /></td>
              <td class="noBorder"><input type="button" value="My Range Usage" class="navButton" onclick="window.location = 'memberRangeUsage';" /></td>
            </tr>
            <tr>
              <td class="noBorder"><input type="button" value="Billing" class="navButton" onclick="window.location = 'billing';" /></td>
              <td class="noBorder"><input type="button" value="Change Contact Info" class="navButton" onclick="window.location = 'contactInfo';" /></td>
              <!--<input type="button" value="My Unused Benefits" style="width:175px; height:60px" onclick="window.location = 'member_used_benefits.php';" />-->
            </td>
            
            </tr>
            
            <tr>
              <td class="noBorder"><input type="button" value="Change Password" class="navButton" onclick="window.location = 'changePassword';" /></td>
              <td class="noBorder">&nbsp;</td>
            </tr>
          </table>
        </div>
        <div class="grid_6">
          <table>
            <tr>
              <th colspan="3" style="background-color: #666;">Upcoming Events</th>
            </tr>
            <?php
			if($getupcomingevents->num_rows == 0)
			{
				echo "<tr><td colspan='3'><span style='color: red;'><center>No records found</center></span></td></tr>";
			}
			else
			{
			while ($row = $getupcomingevents->fetch_assoc()) { 
			?>
			<tr style="font-size: 14px;">
			  <td><?php echo $row['date']; ?></td>
			  <?php 
					$starttime=$row['event_starttime'];
					$endtime=$row['event_endtime'];
					
					$starttime=str_replace("am"," AM",$starttime);
					$endtime=str_replace("am"," AM",$endtime);
					
					$starttime=str_replace("pm"," PM",$starttime);
					$endtime=str_replace("pm"," PM",$endtime);
			  ?>
			  <td>(<?php echo $starttime .'&nbsp'. $endtime; ?>)</td>
			  <td><a href="/xapp/members/eventDetails/<?php echo $row['event_id']?>"><?php echo $row['event_name']; ?></a></td>
			</tr>
			<?php
			}
			}
			?>
            <tr>
              <td colspan="3" class="text-center"><a href="/xapp/members/upcomingEvents">Show more</a></td>
            </tr>
          </table>
          <table>
            <tr>
              <th colspan="3" style="background-color: #666;">Upcoming Classes</th>
            </tr>
		  <?php
            if($getupcomingclasses->num_rows == 0 )
            {
                echo "<tr><td colspan='3'><span style='color: red;'><center>No records found</center></span></td></tr>";
            }
            else
            {
            while ($row = $getupcomingclasses->fetch_assoc()) { 
            ?>
            <tr style="font-size: 14px;">
				<?php $startdate=$row['start_date']; ?>
				<?php 
						$dates=explode('-',$startdate); 
				?>
              <td><?php echo $dates[1].'/'.$dates[2].'/'.$dates[0]; ?></td>
			   <?php 
					$starttime=$row['start_time'];
					$endtime=$row['end_time'];
					
					$starttime=str_replace("am"," AM",$starttime);
					$endtime=str_replace("am"," AM",$endtime);
					
					$starttime=str_replace("pm"," PM",$starttime);
					$endtime=str_replace("pm"," PM",$endtime);
			  ?>
              <td>(<?php echo $starttime .'&nbsp - '. $endtime; ?>)</td>
              <td><a href="/xapp/members/classDetails/<?php echo $row['id']?>"><?php echo $row['title']; ?></a></td>
            </tr>
             <?php
            }
            }
            ?>
            <tr>
              <td colspan="3" class="text-center"><a href="/xapp/members/upcomingClasses">Show more</a></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
   <!-- </td>
    
    </tr>
    
  </tbody>
</table>-->
