<div class="grid_12 text-center margt">
              <table style="width: 60%;">
                <tr>
                  <th colspan="3" style="background-color: #666;">MEMBERSHIP AND PAYMENTS</th>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>Method</td>
                  <td>Amount</td>
                </tr>
                <?php
				if($getbilling->num_rows == 0)
				{
					echo "<tr><td colspan='3'><span style='color: red;'><center>No records found</center></span></td></tr>";
				}
				else
				{
					while ($row = $getbilling->fetch_assoc()) 
					{  
						echo "<tr><td>".$row['date']."</td><td>".$row['payment_method']."</td><td>".$row['payment_amount']."</td></tr>";
					}
				}
				?>
              </table>
              <table style="width: 60%;">
                <tr>
                  <th colspan="5" style="background-color: #666;">POINT OF SALE PURCHASES</th>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>Sale #</td>
                  <td>Description</td>
                  <td>Total</td>
                  <td>Method</td>
                </tr>
                <?php
				if($get_sel_point->num_rows == 0)
				{
					echo "<tr><td colspan='5'><span style='color: red;'><center>No records found</center></span></td></tr>";
				}
				else
				{
					while ($row1 = $get_sel_point->fetch_assoc())
					{  
						echo "<tr><td>".$row1['date_stamp']."</td><td>".$row1['transaction_number']."</td><td>".$row1['description']."</td><td>".$row1['total']."</td><td>".$row1['payment1_method']."</td></tr>";
					}
				}
				?>
              </table>
</div>
<div class="grid_3">&nbsp;</div>