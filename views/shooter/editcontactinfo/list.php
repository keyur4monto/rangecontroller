<?php $data = $member->fetch_assoc();?>
<?php $row = $do_not_call->fetch_assoc();?>
<form name="chane" action="" method="post" id="change">
<div class="grid_12 margt">
	  <?php if ($errorMessage): ?>
      <div class="grid_12 margb">
        <div class="error padlr padtb">
            <?php echo $errorMessage; ?>
        </div>
      </div>
      <div class="clear">&nbsp;</div>
      <?php endif; ?>	
             <table style="width: 50%;">
                <tr>
                  <td class="text-right">Email:</td>
                  <td><input type="text" name="email" value="<?php echo $data['email'];?>"></td>
                </tr>
                <tr>
                  <td class="text-right">Address:</td>
                  <td><input type="text" name="street_address" value="<?php echo $data['street_address'];?>"></td>
                </tr>
                <tr>
                  <td class="text-right">City:</td>
                  <td><input type="text" name="city" value="<?php echo $data['city'];?>"></td>
                </tr>
                <tr>
                  <td class="text-right">State:</td>
                  <td><select name="state">
                      	<option value="AK" <?php echo ($data['state']== "AK") ? 'selected=""' : ''; ?>>AK</option>
                        <option value="AL" <?php echo ($data['state']== "AL") ? 'selected=""' : ''; ?>>AL</option>
                        <option value="AR" <?php echo ($data['state']== "AR") ? 'selected=""' : ''; ?>>AR</option>
                        <option value="AZ" <?php echo ($data['state']== "AZ") ? 'selected=""' : ''; ?>>AZ</option>
                    </select></td>
                </tr>
                <tr>
                  <td class="text-right">Zipcode:</td>
                  <td><input type="text" name="zipcode" value="<?php echo $data['zipcode'];?>"></td>
                </tr>
                <tr>
                  <td class="text-right">Phone Number:</td>
                  <td><input type="text" name="home_phone" value="<?php echo $data['home_phone'];?>"></td>
                </tr>
                <tr>
                  <td class="text-right">Do Not Call Status:</td>
                  <td><select name="call_status">
                  	  <?php $row = $do_not_call->fetch_assoc(); ?>
                      <?php echo $row['membership_number']; ?>
                      <option value="yes" <?php echo ($row['membership_number'] == $_SESSION['member_number']) ? 'selected=""' : ''; ?>>It's OK to Call</option>
                      <option value="no" <?php echo ($row['membership_number'] != $_SESSION['member_number']) ? 'selected=""' : ''; ?>>It's NOT Ok to Call</option>
                    </select></td>
                </tr>
              </table>
            
            <div class="grid_12 text-center">
              <input type="hidden" name="cmd" value="change" />
              <input type="submit" value="Save" name="submit">
            </div>
</div>
</form>
