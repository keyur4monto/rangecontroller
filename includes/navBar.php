<div id="navBar">
  <div id="socialMedia">
    <ul id="socialIcons">
      <a href="http://facebook.com/rangecontroller" target="_blank">
        <li class="fa fa-facebook" id="facebook"></li>
      </a>
      <a href="http://twitter.com/rangecontroller" target="_blank">
        <li class="fa fa-twitter" id="twitter"></li>
      </a>
      <a href="#">
        <li class="fa fa-instagram" id="instagram"></li>
      </a>
      <a href="https://www.youtube.com/channel/UCC3ii8t0kJsxwZhU5M8Gd4Q" target="_blank">
        <li class="fa fa-youtube" id="youtube"></li>
      </a>
    </ul>
  </div>
  <div id="navLinks"> <a href="checkout.php"><span id="signUp">Sign Up</span></a> | <a href="faq.php"><span id="faqLink">FAQ</span></a> </div>
</div>