<div align="center"><img src="images/background.jpg" id="bg" alt="" /></div>
		<div id="header">
			<div class="left"> <img src="/xapp/img/logoNew.png" id="rcLogo"></div>
			<div id="headerLeft"> <a href="/"><span id="rcLogoText"><span class="gold">Range</span> <span class="white">Controller</span></span></a> </div>
			<div id="headerRight">
				<table id="loginTable">
					<tr>
						<td colspan="3" class="center">Are you a
							<input type="hidden" id="h_login_type" name="h_login_type">
							<select id="selLoginType" onchange="setType(this.value);">
								<option>Range</option>
								<option>Shooter</option>
							</select>
						</td>
					</tr>
				</table>
				<div id="divRangeLogin">
					<table>
						<tr>
							<td class="right">Range #:</td>
							<td colspan="2">
								<input type="text" id="txtRangeNumber" name="txtRangeNumber" style="width:100px;" onkeypress="searchKeyPress1(event);">
							</td>
						</tr>
						<tr>
							<td class="right">Password:</td>
							<td>
								<input type="password" id="txtPassword" name="txtPassword" style="width:100px;" onkeypress="searchKeyPress1(event);">
							</td>
							<td>
								<input type="button" value="Go" onclick="rangeLogin();">
							</td>
						</tr>
					</table>
				</div>
				<div id="divShooterLogin">
					<table>
						<tr>
							<td class="right">Range #:</td>
							<td colspan="2">
								<input type="text" id="txtShooterRangeNumber" name="txtShooterRangeNumber" style="width:100px;" onkeypress="searchKeyPress2(event);">
							</td>
						</tr>
						<tr>
							<td class="right">Shooter #:</td>
							<td colspan="2">
								<input type="text" id="txtShooterNumber" name="txtShooterNumber" style="width:100px;" onkeypress="searchKeyPress2(event);">
							</td>
						</tr>
						<tr>
							<td class="right">Password:</td>
							<td>
								<input type="password" id="txtShooterPassword" name="txtShooterPassword" style="width:100px;" onkeypress="searchKeyPress2(event);">
							</td>
							<td>
								<input type="button" value="Go" onclick="shooterLogin();">
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>